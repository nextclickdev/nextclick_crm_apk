package com.nextclick.crm.faq

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.nextclick.crm.Config.Config.FAQ_LIST
import com.nextclick.crm.Constants.Constants
import com.nextclick.crm.Constants.Constants.AUTH_TOKEN
import com.nextclick.crm.R
import com.nextclick.crm.Utilities.PreferenceManager

class FaqActivity : Fragment() {
    private lateinit var faqContainer: LinearLayout
    private lateinit var preferenceManager: PreferenceManager

    //    override fun onCreate(savedInstanceState: Bundle?) {
//	  super.onCreate(savedInstanceState)
//	  setContentView(R.layout.activity_faq)
//	  faqContainer = findViewById(R.id.faqContainer)
//	  preferenceManager = PreferenceManager(this)
//	  getFaqFromServer()
//	  // Add FAQ items dynamically
//
//    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_faq, container, false)
        initializeUi(view)
        preferenceManager = PreferenceManager(requireContext())
        getFaqFromServer()
        return view
    }

    private fun initializeUi(view: View) {
        faqContainer = view.findViewById(R.id.faqContainer)

    }

    private fun addFaqItem(question: String, answer: String) {
        val itemView =
            LayoutInflater.from(requireContext()).inflate(R.layout.item_faq, faqContainer, false)
        val questionTextView = itemView.findViewById<TextView>(R.id.questionTextView)
        val answerTextView = itemView.findViewById<TextView>(R.id.answerTextView)
        val cardView = itemView.findViewById<MaterialCardView>(R.id.cardView)
        val imageArrow = itemView.findViewById<ImageView>(R.id.iv_image)

        questionTextView.text = question
        answerTextView.text = HtmlCompat.fromHtml(answer, HtmlCompat.FROM_HTML_MODE_LEGACY)

        cardView.setOnClickListener {
            // Toggle visibility of answerTextView when the cardView is clicked
            answerTextView.visibility = if (answerTextView.visibility == View.VISIBLE) {
                Glide.with(requireActivity()).load(R.drawable.arrow_down_24).into(imageArrow)
                View.GONE
            } else {
                Glide.with(requireActivity()).load(R.drawable.arrow_up_24).into(imageArrow)
                View.VISIBLE
            }
//		if (answerTextView.visibility == View.VISIBLE) {
//		    Glide.with(requireActivity()).load(R.drawable.arrow_down_24).into(imageArrow)
//		} else {
//		    Glide.with(requireActivity()).load(R.drawable.arrow_up_24).into(imageArrow)
//		}
        }

        faqContainer.addView(itemView)
    }

    private fun getFaqFromServer() {
        val requestQueue = Volley.newRequestQueue(requireContext())
        val jsonObjectRequest: StringRequest =
            object : StringRequest(Method.GET, FAQ_LIST, Response.Listener<String?> { response ->
                if (response != null) {
                    val parser = JsonParser()
                    val mJson = parser.parse(response)
                    val gson = Gson()
                    val faqModel: FAQModel = gson.fromJson(mJson, FAQModel::class.java)
                    for (data in faqModel.data) {
                        addFaqItem(
                            data.question,
                            data.answer
                        )
                    }
                    Log.d("TAG", "onResponse: ")
                }
            }, Response.ErrorListener {
                Log.d("TAG", "onErrorResponse: ")
            }) {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val map: MutableMap<String, String> = HashMap()
                    map["Content-Type"] = "application/json"
                    map[AUTH_TOKEN] = "Bearer " + preferenceManager.getString(Constants.USER_TOKEN)
                    return map
                }
            }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue.add(jsonObjectRequest)
    }
}