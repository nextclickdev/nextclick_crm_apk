package com.nextclick.crm.faq

data class FAQModel(
    val status: Boolean,
    val httpCode: Int,
    val message: String,
    val data: List<FaqData>
)

data class FaqData(
    val id: Int,
    val appId: Int,
    val question: String,
    val answer: String,
    val createdAt: String,
    val updatedAt: String?,
    val deletedAt: String?,
    val status: Int
)