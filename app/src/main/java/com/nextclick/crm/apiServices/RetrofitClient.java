package com.nextclick.crm.apiServices;

/**
 * Created by Arun Vegyas on 25-06-2023.
 */

import android.content.Context;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Utilities.PreferenceManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitClient {
    private static Retrofit retrofit;
    private static final String BASE_URL = "https://test.nextclick.in/";
//    public static final String BASE_URL ="https://app.nextclick.in/";
    private RetrofitClient() {
        // Private constructor to prevent instantiation
    }

    public static Retrofit getInstance(Context context) {
        if (retrofit == null) {
            // Create an OkHttpClient instance to add logging interceptor
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    PreferenceManager preferenceManager = new PreferenceManager(context);
                    Request originalClient = chain.request();
                    Request.Builder requestBuilder = originalClient.newBuilder()
                            .addHeader("Content-Type","application/json")
                            .addHeader("Authorization","Bearer " +preferenceManager.getString(Constants.USER_TOKEN));

                    return chain.proceed(requestBuilder.build());
                }
            });
            // Add logging interceptor to see request/response logs in the console
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(loggingInterceptor);

            // Build Retrofit instance
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }

    public static ApiService provideApi(Context context) {
        return getInstance(context).create(ApiService.class);
    }
}
