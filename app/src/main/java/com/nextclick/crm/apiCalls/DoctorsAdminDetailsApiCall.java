package com.nextclick.crm.apiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.interfaces.HttpReqResCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;

public class DoctorsAdminDetailsApiCall {

    private static HttpReqResCallBack callBack;

    public static void serviceCallForDoctorAdminDetails(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, final String token) {
        String url = Config.DOCTOR_ADMIN_DETAILS;
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("q", "");
            jsonBody.put("hosp_specialty_id", "1");
            StringRequest postRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response, 200, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put(AUTH_TOKEN, token);
                    return headers;

                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            postRequest.setRetryPolicy(policy);
            requestQueue.add(postRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
