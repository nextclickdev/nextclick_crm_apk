package com.nextclick.crm.apiCalls;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.interfaces.HttpReqResCallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;

public class AddServiceApiCall {
    private static HttpReqResCallBack callBack;

    public static void serviceCallForAddService(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String name, String description, /*String qualification, String experience, String languages,*/String serviceDuration, String fee, String discount, LinkedHashMap<Integer, JSONObject> mapOfTimings, LinkedList<Integer> listOfSelectedDayIds, int serviceTypeId, String base64, String token) {
        String url = Config.CREATE_SERVICE;
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("od_cat_id", serviceTypeId);
            jsonBody.put("name", name);
            jsonBody.put("desc", description);
           /* jsonBody.put("qualification", qualification);
            jsonBody.put("experience", experience);
            jsonBody.put("languages", languages);*/
            jsonBody.put("service_duration",serviceDuration);
            jsonBody.put("price", fee);
            jsonBody.put("discount", discount);
            jsonBody.put("image", base64);
            //jsonBody.put("image", base64);
            JSONArray timingsJsonArray = new JSONArray();

            LinkedList<JSONObject> listOfTimingsObject = new LinkedList<>(mapOfTimings.values());
            for (int timingsIndex = 0; timingsIndex < listOfTimingsObject.size(); timingsIndex++) {
                timingsJsonArray.put(listOfTimingsObject.get(timingsIndex));
            }
            jsonBody.put("timings", timingsJsonArray);

            JSONArray jsonArray = new JSONArray();
            for (int index = 0; index < listOfSelectedDayIds.size(); index++) {
                jsonArray.put(listOfSelectedDayIds.get(index));
            }
            jsonBody.put("holidays", jsonArray);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_ADD_SERVICE);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put(AUTH_TOKEN, token);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
