package com.nextclick.crm.apiCalls;

import android.content.Context;
import android.text.Editable;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.SettingsFragment;
import com.nextclick.crm.interfaces.HttpReqResCallBack;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;

public class UpdateVendorSettingsApiCall {
    private static HttpReqResCallBack callBack;

    public static void serviceCalToUpdateVendorSettings(final Context context, final Fragment fragment, final RecyclerView.Adapter adapter, String token, String vendorID, String minOrderPrice, String deliveryFreeRange, String minDeliveryFee, String extraDeliveryFee, String tax) {
        String url = Config.UPDATE_VENDOR_SETTINGS;
        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("vendor_id", vendorID);
            jsonBody.put("min_order_price", minOrderPrice);
            jsonBody.put("min_delivery_fee", minDeliveryFee);
            jsonBody.put("ext_delivery_fee", extraDeliveryFee);
            jsonBody.put("delivery_free_range", deliveryFreeRange);
            jsonBody.put("tax", tax);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(response.toString(), 200, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (fragment != null) {
                        callBack = (HttpReqResCallBack) fragment;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    } else if (adapter != null) {
                        callBack = (HttpReqResCallBack) adapter;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    } else {
                        callBack = (HttpReqResCallBack) context;
                        callBack.jsonResponseReceived(null, 500, Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    final Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put(AUTH_TOKEN, token);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            RetryPolicy policy = new DefaultRetryPolicy(Constants.TIME_OUT_THIRTY_SECONDS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            jsonObjectRequest.setRetryPolicy(policy);
            requestQueue.add(jsonObjectRequest);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
