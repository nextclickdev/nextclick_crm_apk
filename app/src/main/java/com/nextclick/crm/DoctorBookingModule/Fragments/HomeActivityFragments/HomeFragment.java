package com.nextclick.crm.DoctorBookingModule.Fragments.HomeActivityFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.nextclick.crm.R;
import com.bumptech.glide.Glide;

public class HomeFragment extends Fragment {

    private ImageView doc_gif;
    private View rootView;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView=inflater.inflate(R.layout.fragment_home_doctors_booking, container, false);
        doc_gif = rootView.findViewById(R.id.doc_gif);

        Glide.with(getActivity())
                .load(R.drawable.docgiffy)
                .into(doc_gif);

        return rootView;
    }
}