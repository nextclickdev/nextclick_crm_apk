package com.nextclick.crm.DoctorBookingModule.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.nextclick.crm.Common.Activities.LoginActivity;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.navigation.NavigationView;

public class DoctorBookingHomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private Context mContext;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    NavController navController;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_booking_home);
        init();
        setSupportActionBar(toolbar);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.doc_nav_home, R.id.doc_nav_speciality, R.id.doc_nav_doctors)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.doctors_booking_nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.doctors_booking_nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        /* case R.id.action_services:
                startActivity(new Intent(mContext, ServicesActivity.class));
                finish();
                return true;*/
        if (id == R.id.action_logout) {
            new AlertDialog.Builder(mContext)
                    .setTitle("Alert")
                    .setMessage("Are you sure to logout..?")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation

                            preferenceManager.clear();
                            UIMsgs.showToast(mContext, "Logout successfull");
                            startActivity(new Intent(mContext, LoginActivity.class));
                            finish();
                        }
                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
       /* new AlertDialog.Builder(mContext)
                .setTitle("Alert")
                .setMessage("Are you sure to exit..?")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation

                        finish();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();*/
       super.onBackPressed();
    }

    private void init() {
        toolbar = findViewById(R.id.doctors_booking_toolbar);
        drawer = findViewById(R.id.doctors_booking_drawer_layout);
        navigationView = findViewById(R.id.doctors_booking_nav_view);
        mContext = DoctorBookingHomeActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        navigationView.setNavigationItemSelectedListener(DoctorBookingHomeActivity.this);

    }
}