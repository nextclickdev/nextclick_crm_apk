package com.nextclick.crm.DoctorBookingModule.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.DOCTOR_C;
import static com.nextclick.crm.Config.Config.DOCTOR_R;
import static com.nextclick.crm.Config.Config.DOCTOR_U;
import static com.nextclick.crm.Config.Config.SPECIALITY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateDoctorActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private ImageView back_image, doc_image;
    private EditText doc_name, doc_desc,doc_qualification,doc_experience,doc_languages,doc_fee,doc_discount;
    private Spinner speciality_spinner;
    private TextView doc_status_type;
    private Button submit;
    private int status_type = 1;//1- create, 2-update
    private String doc_name_str, doc_desc_str,doc_qualification_str, doc_experience_str,
            doc_languages_str,doc_fee_str,doc_discount_Str,doc_image_str, token;

    public int flag = 0;
    private String userChoosenTask;
    private static final int CAMERA_REQUEST = 8;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 15;
    private final int SELECT_MULTIPLE_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;

    private ArrayList<String> specalityList;
    private ArrayList<String> specalityIDList;
    private String speciality_id=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_update_doctor);
        getSupportActionBar().hide();
        init();
        try {
            String statustext = getIntent().getStringExtra("type");
            if (statustext.equalsIgnoreCase("u")) {
                doc_status_type.setText("Update doctor");
                submit.setText("UPDATE");
                status_type = 2;
                /*availabilty.setVisibility(View.VISIBLE);*/
                Log.d("doc_id", getIntent().getStringExtra("doc_id")+"----ok");
                getDoctor(getIntent().getStringExtra("doc_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        back_image.setOnClickListener(this);
        doc_image.setOnClickListener(this);
        submit.setOnClickListener(this);
        speciality_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!speciality_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    speciality_id = specalityIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getDoctor(String doc_id) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DOCTOR_R + doc_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    LoadingDialog.dialog.dismiss();
                    try{

                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if(status){
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            doc_name.setText(dataObject.getString("name"));
                            doc_desc.setText(dataObject.getString("desc"));
                            doc_fee.setText(dataObject.getString("fee"));
                            doc_discount.setText(dataObject.getString("discount"));
                            doc_qualification.setText(dataObject.getString("qualification"));
                            doc_experience.setText(dataObject.getString("experience"));
                            String lng_str="";
                            JSONArray lang_array = dataObject.getJSONArray("languages");
                            if(lang_array.length()>0) {
                                for(int i=0; i<lang_array.length(); i++){
                                    lng_str = lng_str+lang_array.get(i)+",";
                                }
                                doc_languages.setText(lng_str);
                            }
                            Picasso.get()
                                    .load(dataObject.getString("image"))
                                    /*.networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                                    .placeholder(R.drawable.loader_gif)
                                    .into(doc_image);

                            try{
                                Thread thread = new Thread();
                                Thread.sleep(1000);
                                for(int i=0;i<specalityIDList.size();i++){
                                    if(dataObject.getJSONObject("speciality").getString("id").equalsIgnoreCase(specalityIDList.get(i))){
                                        speciality_spinner.setSelection((i+1),true);
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }catch (Exception e){
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,"Unable to get details");
                        e.printStackTrace();
                    }
                }else{
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext,MAINTENANCE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext,OOPS);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void init() {
        mContext = CreateAndUpdateDoctorActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        back_image = findViewById(R.id.back_image);
        doc_image = findViewById(R.id.doc_image);
        doc_name = findViewById(R.id.doc_name);
        doc_desc = findViewById(R.id.doc_desc);
        doc_qualification = findViewById(R.id.doc_qualification);
        doc_experience = findViewById(R.id.doc_experience);
        doc_languages = findViewById(R.id.doc_languages);
        doc_fee = findViewById(R.id.doc_fee);
        doc_discount = findViewById(R.id.doc_discount);
        speciality_spinner = findViewById(R.id.speciality_spinner);
        doc_status_type = findViewById(R.id.doc_status_type);
        submit = findViewById(R.id.submit);
        //availabilty = findViewById(R.id.availabilty);
        getSpecialities();
    }

    private void getSpecialities() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", " ");
        final String data = new JSONObject(searchMap).toString();

        //LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SPECIALITY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("spe_res", response);

                        if (response != null) {
                            //LoadingDialog.dialog.dismiss();
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        specalityList = new ArrayList<>();
                                        specalityIDList = new ArrayList<>();
                                        specalityList.add("select");
                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject specialityObject = dataArray.getJSONObject(i);
                                            specalityList.add(specialityObject.getString("name"));
                                            specalityIDList.add(specialityObject.getString("id"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, specalityList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        speciality_spinner.setAdapter(adapter);


                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.back_image:
                onBackPressed();
                break;

            case R.id.doc_image:
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;

            case R.id.submit:
                if (isValid()) {

                    String url = "";
                    String data = "";


                    if (status_type == 1) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("speciality_id", speciality_id);
                        uploadMap.put("name", doc_name_str);
                        uploadMap.put("desc", doc_desc_str);
                        uploadMap.put("qualification", doc_qualification_str);
                        uploadMap.put("experience", doc_experience_str);
                        uploadMap.put("languages", doc_languages_str);
                        uploadMap.put("fee", doc_fee_str);
                        uploadMap.put("discount", doc_discount_Str);
                        uploadMap.put("image", doc_image_str);
                        //uploadMap.put("status", "1");
                        url = DOCTOR_C;
                        data = new JSONObject(uploadMap).toString();
                    }
                    if (status_type == 2) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("id", getIntent().getStringExtra("doc_id"));
                        uploadMap.put("speciality_id", speciality_id);
                        uploadMap.put("name", doc_name_str);
                        uploadMap.put("desc", doc_desc_str);
                        uploadMap.put("qualification", doc_qualification_str);
                        uploadMap.put("experience", doc_experience_str);
                        uploadMap.put("languages", doc_languages_str);
                        uploadMap.put("fee", doc_fee_str);
                        uploadMap.put("discount", doc_discount_Str);
                        uploadMap.put("image", doc_image_str);
                        /*if (availabilty.isChecked()) {//1 = Active, 2 = In-active
                            uploadMap.put("status", "1");
                        } else {
                            uploadMap.put("status", "2");
                        }*/
                        url = DOCTOR_U;
                        data = new JSONObject(uploadMap).toString();
                    }

                    createOrUpdateSpeciality(url, data);
                }
                break;

        }

    }

    //Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAndUpdateDoctorActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateAndUpdateDoctorActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            doc_image.setImageBitmap(photo);
            Bitmap bitmap = ((BitmapDrawable) doc_image.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();

            flag = 1;
        }

    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);


        doc_image.setImageBitmap(bm);

        Bitmap bitmap = ((BitmapDrawable) doc_image.getDrawable()).getBitmap();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArray = baos.toByteArray();

        flag = 1;


    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private boolean isValid() {
        boolean valid = true;
        doc_name_str = doc_name.getText().toString().trim();
        doc_desc_str = doc_desc.getText().toString().trim();
        doc_qualification_str = doc_qualification.getText().toString().trim();
        doc_experience_str = doc_experience.getText().toString().trim();
        doc_fee_str = doc_fee.getText().toString().trim();
        doc_discount_Str = doc_discount.getText().toString().trim();
        doc_languages_str = doc_languages.getText().toString().trim();
        doc_image_str = basse64Converter(doc_image);
        int name_length, desc_length, image_length,doc_qual_length,
        doc_exp_length,doc_fee_length,doc_disc_length,doc_lang_length;
        name_length = doc_name_str.length();
        desc_length = doc_desc_str.length();
        image_length = doc_image_str.length();
        doc_qual_length = doc_qualification_str.length();
        doc_exp_length = doc_experience_str.length();
        doc_fee_length = doc_fee_str.length();
        doc_disc_length = doc_discount_Str.length();
        doc_lang_length = doc_image_str.length();

        if (name_length == 0) {
            setEditTextErrorMethod(doc_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (name_length < 1) {
            setEditTextErrorMethod(doc_name, "Please provide valid data");
            valid = false;
        } else if(speciality_id==null){
            UIMsgs.showToast(mContext,"Please select specialization");
            valid = false;
        }else if (desc_length == 0) {
            setEditTextErrorMethod(doc_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (desc_length < 3) {
            setEditTextErrorMethod(doc_desc, "Please provide valid data");
            valid = false;
        } else if (image_length < 1) {
            UIMsgs.showToast(mContext, "Please provide image");
            valid = false;
        } else if(doc_qual_length<2){
            setEditTextErrorMethod(doc_qualification, "Please provide valid data");
            valid = false;
        }else if(doc_exp_length<1 || Double.parseDouble(doc_experience_str)==0){
            setEditTextErrorMethod(doc_experience, "Please provide valid data");
            valid = false;
        }else if(doc_fee_length<1|| Integer.parseInt(doc_fee_str)==0 || doc_fee_length>6){
            setEditTextErrorMethod(doc_fee, "Please provide valid amount");
            valid = false;
        }else if(doc_lang_length<3){
            setEditTextErrorMethod(doc_qualification, "Please provide valid data");
            valid = false;
        }

        return valid;

    }

    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }

    private void createOrUpdateSpeciality(String url, final String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            Log.d("cc_res",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    recreate();
                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}