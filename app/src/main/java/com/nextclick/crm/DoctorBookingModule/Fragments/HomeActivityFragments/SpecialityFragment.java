package com.nextclick.crm.DoctorBookingModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.DoctorBookingModule.Activities.CreateAndUpdateSpeciality;
import com.nextclick.crm.DoctorBookingModule.Adapters.SpecialitiesAdapter;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.DoctorBookingModule.Models.SpecialityModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SPECIALITY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class SpecialityFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private RecyclerView speciality_recycler;
    private PreferenceManager preferenceManager;
    private FloatingActionButton add_speciality_fab;
    private ArrayList<SpecialityModel> specialityModelArrayList;

    private SearchView speciality_searchview;
    private SpecialitiesAdapter specialitiesAdapter;

    private String token = "";
    private final String searchString = "";

    public SpecialityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_speciality, container, false);
        initializeUi(view);
        initializeListeners();
        getSpecialities(searchString);
        return view;
    }

    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);


        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        add_speciality_fab = view.findViewById(R.id.add_speciality_fab);
        speciality_recycler = view.findViewById(R.id.speciality_recycler);

        preferenceManager.putBoolean(getString(R.string.refresh_speciality), false);
    }

    private void initializeListeners() {
        add_speciality_fab.setOnClickListener(this);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String enteredText = charSequence.toString();
                if (specialitiesAdapter != null) {
                    specialitiesAdapter.getFilter().filter(enteredText);
                }
                //getSpecialities(query);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getSpecialities(String s) {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", s);
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SPECIALITY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            listIsFull();
                            Log.d("spe_res", response);
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        specialityModelArrayList = new ArrayList<>();
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            SpecialityModel specialityModel = new SpecialityModel();
                                            JSONObject specialityObject = dataArray.getJSONObject(i);
                                            specialityModel.setId(specialityObject.getString("id"));
                                            specialityModel.setName(specialityObject.getString("name"));
                                            specialityModel.setStatus(specialityObject.getInt("status"));
                                            specialityModel.setImage(specialityObject.getString("image"));
                                            specialityModel.setDesc(specialityObject.getString("desc"));
                                            specialityModelArrayList.add(specialityModel);
                                        }

                                        specialitiesAdapter = new SpecialitiesAdapter(mContext, specialityModelArrayList);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }

                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }
                                        };
                                        speciality_recycler.setLayoutManager(layoutManager);
                                        speciality_recycler.setAdapter(specialitiesAdapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listIsEmpty();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            listIsEmpty();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshSpeciality = preferenceManager.getBoolean(getString(R.string.refresh_speciality));
        if (refreshSpeciality) {
            preferenceManager.putBoolean(getString(R.string.refresh_speciality), false);
            getSpecialities("");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_speciality_fab) {
            startActivity(new Intent(mContext, CreateAndUpdateSpeciality.class));
        }
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        speciality_recycler.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        speciality_recycler.setVisibility(View.VISIBLE);
    }
}