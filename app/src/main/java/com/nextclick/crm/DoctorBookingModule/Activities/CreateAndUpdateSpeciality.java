package com.nextclick.crm.DoctorBookingModule.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SPECIALITY_C;
import static com.nextclick.crm.Config.Config.SPECIALITY_R;
import static com.nextclick.crm.Config.Config.SPECIALITY_U;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateSpeciality extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private ImageView back_image, speciality_image;
    private EditText speciality_name, speciality_desc;
    private TextView speciality_status_type;
    private Button submit;
    private int status_type = 1;//1- create, 2-update
    private String speciality_name_str, speciality_desc_str, speciality_image_str, token;
    private SwitchMaterial availabilty;

    public int flag = 0;
    private String userChoosenTask;
    private static final int CAMERA_REQUEST = 8;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 15;
    private final int SELECT_MULTIPLE_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_update_speciality);
        getSupportActionBar().hide();
        init();
        try {
            String statustext = getIntent().getStringExtra("type");
            if (statustext.equalsIgnoreCase("u")) {
                speciality_status_type.setText("Update Speciality");
                submit.setText("UPDATE");
                status_type = 2;
                availabilty.setVisibility(View.VISIBLE);
                Log.d("spec_id", getIntent().getStringExtra("spec_id") + "----ok");
                getSpeciality(getIntent().getStringExtra("spec_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        back_image.setOnClickListener(this);
        speciality_image.setOnClickListener(this);
        submit.setOnClickListener(this);
    }

    private void getSpeciality(String spec_id) {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SPECIALITY_R + spec_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        LoadingDialog.dialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            speciality_name.setText(dataObject.getString("name"));
                            speciality_desc.setText(dataObject.getString("desc"));
                            Picasso.get()
                                    .load(dataObject.getString("image"))
                                    /*.networkPolicy(NetworkPolicy.NO_CACHE)
                                    .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                                    .placeholder(R.drawable.loader_gif)
                                    .into(speciality_image);
                        }
                    } catch (Exception e) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, "Unable to get details");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void init() {
        mContext = CreateAndUpdateSpeciality.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        back_image = findViewById(R.id.back_image);
        speciality_image = findViewById(R.id.speciality_image);
        speciality_name = findViewById(R.id.speciality_name);
        speciality_desc = findViewById(R.id.speciality_desc);
        speciality_status_type = findViewById(R.id.speciality_status_type);
        submit = findViewById(R.id.submit);
        availabilty = findViewById(R.id.availabilty);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_image:
                onBackPressed();
                break;
            case R.id.speciality_image:
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;
            case R.id.submit:
                if (isValid()) {
                    String url = "";
                    String data = "";
                    if (status_type == 1) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("name", speciality_name_str);
                        uploadMap.put("desc", speciality_desc_str);
                        uploadMap.put("image", speciality_image_str);
                        //uploadMap.put("status", "1");
                        url = SPECIALITY_C;
                        data = new JSONObject(uploadMap).toString();
                    }
                    if (status_type == 2) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("id", getIntent().getStringExtra("spec_id"));
                        uploadMap.put("name", speciality_name_str);
                        uploadMap.put("desc", speciality_desc_str);
                        uploadMap.put("image", speciality_image_str);
                        /*if (availabilty.isChecked()) {//1 = Active, 2 = In-active
                            uploadMap.put("status", "1");
                        } else {
                            uploadMap.put("status", "2");
                        }*/
                        url = SPECIALITY_U;
                        data = new JSONObject(uploadMap).toString();
                    }

                    createOrUpdateSpeciality(url, data);
                }
                break;
            default:
                break;
        }
    }

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAndUpdateSpeciality.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateAndUpdateSpeciality.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            speciality_image.setImageBitmap(photo);
            Bitmap bitmap = ((BitmapDrawable) speciality_image.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] byteArray = baos.toByteArray();
            flag = 1;
        }
    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);
        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);
        speciality_image.setImageBitmap(bm);
        Bitmap bitmap = ((BitmapDrawable) speciality_image.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        flag = 1;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private boolean isValid() {
        boolean valid = true;
        speciality_name_str = speciality_name.getText().toString().trim();
        speciality_desc_str = speciality_desc.getText().toString().trim();
        speciality_image_str = basse64Converter(speciality_image);
        int name_length, desc_length, image_length;
        name_length = speciality_name_str.length();
        desc_length = speciality_desc_str.length();
        image_length = speciality_image_str.length();

        if (name_length == 0) {
            setEditTextErrorMethod(speciality_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (name_length < 1) {
            setEditTextErrorMethod(speciality_name, "Please provide valid data");
            valid = false;
        } else if (desc_length == 0) {
            setEditTextErrorMethod(speciality_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (desc_length < 3) {
            setEditTextErrorMethod(speciality_desc, "Please provide valid data");
            valid = false;
        } else if (image_length < 1) {
            UIMsgs.showToast(mContext, "Please provide image");
            valid = false;
        }

        return valid;

    }

    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }

    private void createOrUpdateSpeciality(String url, final String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    //recreate();
                                    preferenceManager.putBoolean(getString(R.string.refresh_speciality), true);
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}