package com.nextclick.crm.DoctorBookingModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.DoctorBookingModule.Activities.CreateAndUpdateDoctorActivity;
import com.nextclick.crm.DoctorBookingModule.Adapters.DoctorsAdapter;
import com.nextclick.crm.DoctorBookingModule.Models.DoctorModel;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.DOCTOR_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class DoctorsFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private RecyclerView doctors_recycler;
    private DoctorsAdapter doctorsAdapter;
    private PreferenceManager preferenceManager;
    private FloatingActionButton add_doctor_fab;

    private ArrayList<DoctorModel> doctorModelArrayList;

    private String token = "";
    private final String searchString = "";

    public DoctorsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctors, container, false);
        initializeUi(view);
        initializeListeners();
        getDoctors(searchString);
        return view;
    }

    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);

        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        add_doctor_fab = view.findViewById(R.id.add_doctor_fab);
        doctors_recycler = view.findViewById(R.id.doctors_recycler);
    }

    private void initializeListeners() {
        add_doctor_fab.setOnClickListener(this);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String enteredText = charSequence.toString();
                if (doctorsAdapter != null) {
                    doctorsAdapter.getFilter().filter(enteredText);
                }
                //getDoctors(enteredText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getDoctors(String s) {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", s);
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DOCTOR_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("doc_res", response);
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        listIsFull();
                                        doctorModelArrayList = new ArrayList<>();
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            DoctorModel doctorModel = new DoctorModel();
                                            JSONObject specialityObject = dataArray.getJSONObject(i);
                                            doctorModel.setId(specialityObject.getString("id"));
                                            doctorModel.setName(specialityObject.getString("name"));
                                            doctorModel.setStatus(specialityObject.getInt("status"));
                                            doctorModel.setImage(specialityObject.getString("image"));
                                            doctorModel.setDesc(specialityObject.getString("desc"));
                                            doctorModel.setSpeciality_id(specialityObject.getInt("speciality_id"));
                                            doctorModel.setQualification(specialityObject.getString("qualification"));
                                            doctorModel.setExperience(specialityObject.getString("experience"));
                                            doctorModel.setLanguages(specialityObject.getString("languages"));
                                            doctorModel.setFee(specialityObject.getString("fee"));
                                            doctorModel.setDiscount(specialityObject.getString("discount"));
                                            doctorModelArrayList.add(doctorModel);
                                        }

                                        doctorsAdapter = new DoctorsAdapter(mContext, doctorModelArrayList);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }

                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }
                                        };
                                        doctors_recycler.setLayoutManager(layoutManager);
                                        doctors_recycler.setAdapter(doctorsAdapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listIsEmpty();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            listIsEmpty();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_doctor_fab) {
            startActivity(new Intent(mContext, CreateAndUpdateDoctorActivity.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //getDoctors(" ");
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        doctors_recycler.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        doctors_recycler.setVisibility(View.VISIBLE);
    }
}