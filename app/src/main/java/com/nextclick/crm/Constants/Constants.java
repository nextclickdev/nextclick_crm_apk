package com.nextclick.crm.Constants;

import android.text.TextUtils;

public interface Constants {

    String USER_TOKEN = "user_token";
    String FCM_TOKEN = "fcm_token";
    String VENDOR_ID = "vendor_id";
    String AUTH_TOKEN = "Authorization";
    String APP_ID = "APP_ID";
    String APP_ID_VALUE = "TWc9PQ==";
    String USER_ID = "user_id";
    String UNIQUE_ID = "unique_id";
    String USERNAME = "user_name";
    String MAIL = "mail";
    String MOBILE = "mobile";
    String PROFILE_PIC = "profile_image";
    String SOCIAL_IMAGE = "social_image_url";
    String GMAIL_LOGIN = "gmail_login";
    String FACEBOOK_LOGIN = "facebook_login";
    String LOGIN_TYPE = "login_type";
    String SERVICE = "SERVICE";
    String SERVICE_SELECTED = "YES";
    String SERVICE_NOT_SELECTED = "YES";

    String CATALOGUESTATUS="1";
    String APPROVEDSTATUS="2";
    String PENDINGSTATUS="3";

    int TIME_OUT_THIRTY_SECONDS = 30000;

    int REQUEST_CODE_FOR_PHONE = 1;
    int REQUEST_CODE_FOR_LOCATION_PERMISSION = 2;
    int REQUEST_CODE_FOR_CAMERA = 3;
    int REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION = 4;
    int PICK_GALLERY = 5;
    int REQUEST_CODE_CAPTURE_IMAGE = 6;



    int SERVICE_CALL_TO_GET_NOTIFICATIONS = 1;
    int SERVICE_CALL_TO_VERIFY_SUBSCRIPTION = 2;
    int SERVICE_CALL_TO_GET_SUBSCRIPTIONS = 3;
    int SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS = 4;
    int SERVICE_CALL_TO_GET_MY_DOCTOR_DETAILS = 5;
    int SERVICE_CALL_TO_GET_APPROVED_DOCTOR_DETAILS = 6;
    int SERVICE_CALL_TO_GET_PENDING_DOCTOR_DETAILS = 7;
    int SERVICE_CALL_TO_GET_SPECIALITIES_DETAILS = 8;
    int SERVICE_CALL_TO_ADD_DOCTOR = 9;
    int SERVICE_CALL_TO_GET_DOCTOR_DETAILS = 10;
    int SERVICE_CALL_TO_ADD_AS_YOUR_DOCTOR = 11;
    int SERVICE_CALL_TO_UPDATE_DOCTOR_DEAILS = 12;
    int SERVICE_CALL_TO_DELETE_DOCTOR = 13;
    int SERVICE_CALL_TO_GET_SERVICE_ADMIN_DETAILS = 14;
    int SERVICE_CALL_TO_GET_PENDING_SERVICE_DETAILS = 15;
    int SERVICE_CALL_TO_DELETE_SERVICE = 16;
    int SERVICE_CALL_TO_GET_APPROVED_SERVICE_DETAILS = 17;
    int SERVICE_CALL_TO_GET_MY_SERVICE_DETAILS = 18;
    int SERVICE_CALL_TO_ADD_SERVICE = 19;
    int SERVICE_CALL_TO_GET_SERVICE_TYPES = 20;
    int SERVICE_CALL_TO_GET_SERVICE_DETAILS = 21;
    int SERVICE_CALL_TO_UPDATE_SERVICE = 22;
    int SERVICE_CALL_TO_ADD_AS_YOUR_SERVICE = 23;
    int SERVICE_CALL_TO_GET_DASHBOARD_COUNTS = 24;
    int SERVICE_CALL_TO_GET_VENDOR_SETTINGS = 25;
    int SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS = 26;
    int SERVICE_CALL_TO_GET_NOTIFICATIONS_LIST = 27;
    int SERVICE_CALL_GET_BOOKINGS = 28;
    int SERVICE_CALL_TO_GET_BOOKING_DETAILS = 29;
    int SERVICE_CALL_TO_REJECT_BOOKING = 30;
    int SERVICE_CALL_TO_ACCEPT_BOOKING = 31;
    int SERVICE_CALL_TO_COMPLETE_BOOKING = 32;

    boolean Use_ActivityDesign = false;

    static String isNull(String str) {
        if(TextUtils.isEmpty(str)||str.equalsIgnoreCase("null"))
            return "";
        else
            return str;
    }
    static boolean isnullboolean(String str) {
        return !TextUtils.isEmpty(str) && !str.equalsIgnoreCase("null");
    }
}
