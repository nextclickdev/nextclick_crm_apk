package com.nextclick.crm.Constants;

/*
* @Author : Sarath
* Created At : 01/07/2020
* Description : Provides error messages whenever required.
* */

public interface ValidationMessages {

    String INVALID = "Invalid";
    String INVALID_USERID = "Invalid user id";
    String NOT_ALLOWED = "Not allowed";
    String EMPTY = "Empty";
    String EMPTY_NOT_ALLOWED = "Empty field not allowed";
    String INVALID_MOBILE = "Invalid mobile";
    String INVALID_WHATSAPP = "Invalid whatsapp number";
    String INVALID_PASSWORD = "Minimum four characters required";
    String NOT_MATCHED = "Password not matched";
    String INVALID_MAIL = "Invalid mail";
    String LOGIN_SUCCESS = "Login successful";
    String AVAILABLE = "Will be available soon";
    String OOPS = "Oops! Something went wrong";
    String INVALID_LOGIN = "Login Failed";
    String MAINTENANCE = "Oops! Sorry for inconvenience. Server is under maintenance";
    String AVAILABLE_SOON = "Will be available soon";
    String UPDATED_PROFILE="Profile has been updated succesfully";
    String TIMEINAVLID ="Please select valid time";

}
