package com.nextclick.crm.models.responseModels;

import java.util.List;

/**
 * Created by Arun Vegyas on 27-06-2023.
 */
public class HistoryModel {
    private boolean status;
    private int http_code;
    private String message;
    private List<DataItem> data;

    // Getter and Setter methods for the fields
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getHttpCode() {
        return http_code;
    }

    public void setHttpCode(int http_code) {
        this.http_code = http_code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }
}

