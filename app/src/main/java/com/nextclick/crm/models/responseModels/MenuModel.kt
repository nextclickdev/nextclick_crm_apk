package com.nextclick.crm.models.responseModels

/**
 * Created by Arun Vegyas on 18-07-2023.
 */
data class MenuModel(val status: Boolean,
			   val http_code: Int,
			   val message: String,
			   val data: Category)
data class Menu( val sub_cat_id: Int,
		     val id: Int,
		     val name: String,
		     val desc: String,
		     val status: Int,
		     val image: String)
