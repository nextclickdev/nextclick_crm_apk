package com.nextclick.crm.models.responseModels.VendorSettingsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorSettingsResponse {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("http_code")
    @Expose
    public Integer httpCode;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;

    public class Data {
        @SerializedName("shop_settings")
        @Expose
        public ShopSettings shopSettings;

        public class ShopSettings {
            @SerializedName("vendor_id")
            @Expose
            public Double vendorId;
            @SerializedName("min_order_price")
            @Expose
            public Double minOrderPrice;
            @SerializedName("delivery_free_range")
            @Expose
            public Double deliveryFreeRange;
            @SerializedName("min_delivery_fee")
            @Expose
            public Double minDeliveryFee;
            @SerializedName("ext_delivery_fee")
            @Expose
            public Double extraDeliveryFee;
            @SerializedName("tax")
            @Expose
            public Double tax;
        }

    }


}
