package com.nextclick.crm.models.responseModels.getDoctorDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class DoctorDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hosp_specialty_id")
    @Expose
    private Integer hospSpecialtyId;
    @SerializedName("hosp_doctor_id")
    @Expose
    private Integer hospDoctorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("experience")
    @Expose
    private Double experience;
    @SerializedName("languages")
    @Expose
    private LinkedList<String> listOfLanguages;
    @SerializedName("fee")
    @Expose
    private Integer fee;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("qualification")
    @Expose
    private String qualification;
    @SerializedName("holidays")
    @Expose
    private LinkedList<Integer> listOfHolidays;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("speciality")
    @Expose
    private Speciality speciality;
    @SerializedName("service_timings")
    @Expose
    private LinkedList<ServiceTiming> listOfServiceTimings;
    @SerializedName("image")
    @Expose
    private String image;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHospSpecialtyId() {
        return hospSpecialtyId;
    }

    public void setHospSpecialtyId(Integer hospSpecialtyId) {
        this.hospSpecialtyId = hospSpecialtyId;
    }

    public Integer getHospDoctorId() {
        return hospDoctorId;
    }

    public void setHospDoctorId(Integer hospDoctorId) {
        this.hospDoctorId = hospDoctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Double getExperience() {
        return experience;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public LinkedList<String> getListOfLanguages() {
        return listOfLanguages;
    }

    public void setListOfLanguages(LinkedList<String> listOfLanguages) {
        this.listOfLanguages = listOfLanguages;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public LinkedList<Integer> getListOfHolidays() {
        return listOfHolidays;
    }

    public void setListOfHolidays(LinkedList<Integer> listOfHolidays) {
        this.listOfHolidays = listOfHolidays;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public LinkedList<ServiceTiming> getListOfServiceTimings() {
        return listOfServiceTimings;
    }

    public void setListOfServiceTimings(LinkedList<ServiceTiming> listOfServiceTimings) {
        this.listOfServiceTimings = listOfServiceTimings;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
