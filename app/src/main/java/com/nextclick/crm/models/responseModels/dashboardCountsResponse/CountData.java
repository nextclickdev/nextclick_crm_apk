package com.nextclick.crm.models.responseModels.dashboardCountsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountData {
    @SerializedName("active_count")
    @Expose
    private Integer activeCount;
    @SerializedName("in_active_count")
    @Expose
    private Integer inActiveCount;
    @SerializedName("pending_bookings_count")
    @Expose
    private Integer pendingBookingsCount;
    @SerializedName("accepted_bookings_count")
    @Expose
    private Integer acceptedBookingsCount;

    public Integer getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(Integer activeCount) {
        this.activeCount = activeCount;
    }

    public Integer getInActiveCount() {
        return inActiveCount;
    }

    public void setInActiveCount(Integer inActiveCount) {
        this.inActiveCount = inActiveCount;
    }

    public Integer getPendingBookingsCount() {
        return pendingBookingsCount;
    }

    public void setPendingBookingsCount(Integer pendingBookingsCount) {
        this.pendingBookingsCount = pendingBookingsCount;
    }

    public Integer getAcceptedBookingsCount() {
        return acceptedBookingsCount;
    }

    public void setAcceptedBookingsCount(Integer acceptedBookingsCount) {
        this.acceptedBookingsCount = acceptedBookingsCount;
    }
}
