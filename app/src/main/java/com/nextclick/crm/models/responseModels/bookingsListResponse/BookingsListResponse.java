package com.nextclick.crm.models.responseModels.bookingsListResponse;

import com.nextclick.crm.models.responseModels.doctorsAdminResponse.DoctorAdminDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class BookingsListResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;



    @SerializedName("data")
    @Expose
    private LinkedList<BookingDetails> listOfBookings;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public LinkedList<BookingDetails> getListOfBookings() {
        return listOfBookings;
    }

    public void setListOfBookings(LinkedList<BookingDetails> listOfBookings) {
        this.listOfBookings = listOfBookings;
    }

}
