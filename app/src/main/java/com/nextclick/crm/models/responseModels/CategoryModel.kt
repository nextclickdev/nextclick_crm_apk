package com.nextclick.crm.models.responseModels

/**
 * Created by Arun Vegyas on 18-07-2023.
 */
data class CategoryModel( val status: Boolean,
				  val http_code: Int,
				  val message: String,
				  val data: List<Category>)
data class Category( val id: Int,
			   val cat_id: Int,
			   val type: Int,
			   val name: String,
			   val desc: String,
			   val product_type_widget_status: Int,
			   val menus: List<Menu>,
			   val image: String)