package com.nextclick.crm.models.responseModels.notificationDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationDetails {


    private String id,title,message,notification_code,notification_name,status,ecom_order_id;
    private int ticket_id;

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getEcom_order_id() {
        return ecom_order_id;
    }

    public void setEcom_order_id(String ecom_order_id) {
        this.ecom_order_id = ecom_order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNotification_code() {
        return notification_code;
    }

    public void setNotification_code(String notification_code) {
        this.notification_code = notification_code;
    }

    public String getNotification_name() {
        return notification_name;
    }

    public void setNotification_name(String notification_name) {
        this.notification_name = notification_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
