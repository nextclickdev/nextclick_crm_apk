package com.nextclick.crm.models.responseModels.bookingsListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class BookingDetailsResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private BookingsData bookingsData;

    public BookingsData getBookingsData() {
        return bookingsData;
    }

    public void setBookingsData(BookingsData bookingsData) {
        this.bookingsData = bookingsData;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class BookingsData {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("track_id")
        @Expose
        private String trackId;
        @SerializedName("vendor_id")
        @Expose
        private String vendorId;
        @SerializedName("sub_total")
        @Expose
        private String subTotal;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("booking_status")
        @Expose
        private String bookingStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("created_user_id")
        @Expose
        private String createdUserId;
        @SerializedName("booking_items")
        @Expose
        public LinkedList<BookingItemsDetails> listOfBookingItemDetails;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTrackId() {
            return trackId;
        }

        public void setTrackId(String trackId) {
            this.trackId = trackId;
        }

        public String getVendorId() {
            return vendorId;
        }

        public void setVendorId(String vendorId) {
            this.vendorId = vendorId;
        }

        public String getSubTotal() {
            return subTotal;
        }

        public void setSubTotal(String subTotal) {
            this.subTotal = subTotal;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getBookingStatus() {
            return bookingStatus;
        }

        public void setBookingStatus(String bookingStatus) {
            this.bookingStatus = bookingStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getCreatedUserId() {
            return createdUserId;
        }

        public void setCreatedUserId(String createdUserId) {
            this.createdUserId = createdUserId;
        }

        public LinkedList<BookingItemsDetails> getListOfBookingItemDetails() {
            return listOfBookingItemDetails;
        }

        public void setListOfBookingItemDetails(LinkedList<BookingItemsDetails> listOfBookingItemDetails) {
            this.listOfBookingItemDetails = listOfBookingItemDetails;
        }
    }

    public class BookingItemsDetails {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("booking_id")
        @Expose
        private String bookingId;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("discount")
        @Expose
        private String discount;
        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("booking_date")
        @Expose
        private String bookingDate;
        @SerializedName("service_timing_id")
        @Expose
        private String serviceTimingId;
        @SerializedName("service_item")
        @Expose
        private ServiceItem serviceItem;
        @SerializedName("service_timings")
        @Expose
        private ServiceTimings serviceTimings;

        public ServiceTimings getServiceTimings() {
            return serviceTimings;
        }

        public void setServiceTimings(ServiceTimings serviceTimings) {
            this.serviceTimings = serviceTimings;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getBookingDate() {
            return bookingDate;
        }

        public void setBookingDate(String bookingDate) {
            this.bookingDate = bookingDate;
        }

        public String getServiceTimingId() {
            return serviceTimingId;
        }

        public void setServiceTimingId(String serviceTimingId) {
            this.serviceTimingId = serviceTimingId;
        }

        public ServiceItem getServiceItem() {
            return serviceItem;
        }

        public void setServiceItem(ServiceItem serviceItem) {
            this.serviceItem = serviceItem;
        }
    }

    public class ServiceItem {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("od_service_id")
        @Expose
        private String odServiceId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("desc")
        @Expose
        private String description;
        private String image;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOdServiceId() {
            return odServiceId;
        }

        public void setOdServiceId(String odServiceId) {
            this.odServiceId = odServiceId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }


    }

    public class ServiceTimings {
        @SerializedName("start_time")
        @Expose
        private String startTime;
        @SerializedName("end_time")
        @Expose
        private String endTime;

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }
    }
}
