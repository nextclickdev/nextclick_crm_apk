package com.nextclick.crm.models.responseModels

/**
 * Created by Arun Vegyas on 18-07-2023.
 */
data class BrandsModel(
    val status: Boolean,
    val http_code: Int,
    val message: String,
    val data: CategoryData
)

data class CategoryData(
    val id: Int,
    val name: String,
    val desc: String,
    val terms: String,
    val created_user_id: Int,
    val updated_user_id: Int?,
    val created_at: String,
    val updated_at: String?,
    val deleted_at: String?,
    val is_having_lead_managemet: Int,
    val is_pickup_allowed: Int,
    val status: Int,
    val amenities: String?,
    val brands: List<Brand>,
    val sub_categories: List<SubCategory>,
    val services: Map<String, Service>,
    val image: String,
    val fields: List<String>
)

data class Brand(
    val id: Int,
    val cat_id: Int,
    val name: String,
    val image: String
)

data class SubCategory(
    val cat_id: Int,
    val name: String,
    val id: Int,
    val image: String
)

data class Service(
    val id: Int,
    val cat_id: Int,
    val name: String
)
