package com.nextclick.crm.models.responseModels;

public class DataItem {
    private int id;
    private int created_user_id;
    private String product_search;
    private String created_at;

    // Getter and Setter methods for the fields
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCreatedUserId() {
        return created_user_id;
    }

    public void setCreatedUserId(int created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getProductSearch() {
        return product_search;
    }

    public void setProductSearch(String product_search) {
        this.product_search = product_search;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }
}
