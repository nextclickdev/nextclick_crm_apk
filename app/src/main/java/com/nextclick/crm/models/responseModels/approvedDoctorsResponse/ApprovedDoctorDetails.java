package com.nextclick.crm.models.responseModels.approvedDoctorsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ApprovedDoctorDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hosp_doctor_id")
    @Expose
    private Integer hospDoctorId;
    @SerializedName("hosp_specialty_id")
    @Expose
    private Integer hospSpecialtyId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("qualification")
    @Expose
    private String qualification;
    @SerializedName("experience")
    @Expose
    private Double experience;
    @SerializedName("languages")
    @Expose
    private String languages;
    @SerializedName("fee")
    @Expose
    private Integer fee;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("holidays")
    @Expose
    private LinkedList<Integer> listOfHolidays;
    @SerializedName("created_user_id")
    @Expose
    private Integer createdUserId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHospDoctorId() {
        return hospDoctorId;
    }

    public void setHospDoctorId(Integer hospDoctorId) {
        this.hospDoctorId = hospDoctorId;
    }

    public Integer getHospSpecialtyId() {
        return hospSpecialtyId;
    }

    public void setHospSpecialtyId(Integer hospSpecialtyId) {
        this.hospSpecialtyId = hospSpecialtyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public Double getExperience() {
        return experience;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public Integer getFee() {
        return fee;
    }

    public void setFee(Integer fee) {
        this.fee = fee;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public LinkedList<Integer> getListOfHolidays() {
        return listOfHolidays;
    }

    public void setListOfHolidays(LinkedList<Integer> listOfHolidays) {
        this.listOfHolidays = listOfHolidays;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
