package com.nextclick.crm.models.responseModels.VendorSettingsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VendorSettingsUpdateResponse {
    @SerializedName("status")
    @Expose
    public Boolean status;
    @SerializedName("http_code")
    @Expose
    public Integer httpCode;
    @SerializedName("message")
    @Expose
    public String message;
}
