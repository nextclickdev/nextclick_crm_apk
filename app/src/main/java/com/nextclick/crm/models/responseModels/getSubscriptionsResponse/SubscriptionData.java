package com.nextclick.crm.models.responseModels.getSubscriptionsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class SubscriptionData {

    @SerializedName("all_plans")
    @Expose
    private LinkedList<SubscriptionPlans> listOfSubscriptionPlans;
    @SerializedName("active_plan")
    @Expose
    private Boolean activePlan;


    public LinkedList<SubscriptionPlans> getListOfSubscriptionPlans() {
        return listOfSubscriptionPlans;
    }

    public void setListOfSubscriptionPlans(LinkedList<SubscriptionPlans> listOfSubscriptionPlans) {
        this.listOfSubscriptionPlans = listOfSubscriptionPlans;
    }

    public Boolean getActivePlan() {
        return activePlan;
    }

    public void setActivePlan(Boolean activePlan) {
        this.activePlan = activePlan;
    }
}
