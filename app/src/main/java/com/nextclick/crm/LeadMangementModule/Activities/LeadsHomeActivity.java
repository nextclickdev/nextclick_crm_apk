package com.nextclick.crm.LeadMangementModule.Activities;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.LeadMangementModule.Adapters.LeadsAdapter;
import com.nextclick.crm.LeadMangementModule.Model.LeadModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.LEADS_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class LeadsHomeActivity extends AppCompatActivity {

    private Context mContext;
    PreferenceManager preferenceManager;
    private String token;
    private RecyclerView leads_recycler;
    private ImageView back_image;
    private ArrayList<LeadModel> leadModelArrayList;
    LeadsAdapter leadsAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leads_home);
        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
        init();
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void init() {
        mContext = LeadsHomeActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        leads_recycler = findViewById(R.id.leads_recycler);
        back_image = findViewById(R.id.back_image);
        getLeads();
    }

    private void getLeads() {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, LEADS_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            leadModelArrayList = new ArrayList<>();
                                            for (int i = 0; i < dataArray.length(); i++) {

                                                JSONObject leadObject = dataArray.getJSONObject(i);
                                                LeadModel leadModel = new LeadModel();

                                                leadModel.setId(leadObject.getString("id"));
                                                leadModel.setVendor_id(leadObject.getString("vendor_id"));
                                                leadModel.setLead_id(leadObject.getString("lead_id"));
                                                leadModel.setLead_status(leadObject.getInt("lead_status"));
                                                leadModel.setCreated_at(leadObject.getString("created_at"));
                                                leadModel.setUser_email(leadObject.getJSONObject("lead").getJSONObject("user").getString("email"));
                                                leadModel.setUser_first_name(leadObject.getJSONObject("lead").getJSONObject("user").getString("first_name"));
                                                leadModel.setPhone(leadObject.getJSONObject("lead").getJSONObject("user").getString("phone"));



                                                leadModelArrayList.add(leadModel);
                                            }

                                            leadsAdapter = new LeadsAdapter(mContext, leadModelArrayList);

                                            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                                @Override
                                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                                        @Override
                                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                            return SPEED / displayMetrics.densityDpi;
                                                        }

                                                    };
                                                    smoothScroller.setTargetPosition(position);
                                                    startSmoothScroll(smoothScroller);
                                                }

                                            };
                                            leads_recycler.setLayoutManager(layoutManager);
                                            leads_recycler.setAdapter(leadsAdapter);

                                        }


                                    } catch (Exception e) {
                                        UIMsgs.showToast(mContext,"Leads not available");
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(mContext,"Leads not available");
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        /*startActivity(new Intent(mContext, ServicesActivity.class));
        finish();*/
        super.onBackPressed();
    }
}