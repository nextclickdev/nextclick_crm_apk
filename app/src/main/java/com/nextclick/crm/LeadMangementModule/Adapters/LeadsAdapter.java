package com.nextclick.crm.LeadMangementModule.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.LeadMangementModule.Model.LeadModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;

import java.util.List;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class LeadsAdapter extends RecyclerView.Adapter<LeadsAdapter.ViewHolder>  {

    List<LeadModel> data;


    LayoutInflater inflter;
    Context context;
    private String token;




    public LeadsAdapter(Context activity, List<LeadModel> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
        //data_full = new ArrayList<>(itemPojos);

    }


    @NonNull
    @Override
    public LeadsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_leads_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        token = new PreferenceManager(context).getString(USER_TOKEN);
        return new LeadsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LeadsAdapter.ViewHolder holder, final int position) {
        final LeadModel leadModel = data.get(position);

        holder.customer_name.setText(leadModel.getUser_first_name());
        holder.customer_date.setText(leadModel.getCreated_at());

        holder.mail_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{

                    Intent intent=new Intent(Intent.ACTION_SEND);
                    String[] recipients={leadModel.getUser_email()};
                    intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                    intent.putExtra(Intent.EXTRA_SUBJECT,"Nextclick");
                    intent.putExtra(Intent.EXTRA_TEXT,"");
                    intent.setType("text/html");
                    intent.setPackage("com.google.android.gm");
                    context.startActivity(Intent.createChooser(intent, "Send mail"));

                }catch (Exception e){
                    UIMsgs.showToast(context,"Unable to open mail");
                    e.printStackTrace();
                }
            }
        });

        holder.call_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(/*leadModel.getPhone()*/"tel:"+leadModel.getPhone()));
                    context.startActivity(intent);
                }catch (Exception e){
                    UIMsgs.showToast(context,"Number does not exist");
                    e.printStackTrace();
                }

            }
        });

    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView customer_date,customer_name;
        CardView call_card,mail_card;
        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            customer_date = itemView.findViewById(R.id.customer_date);
            customer_name = itemView.findViewById(R.id.customer_name);
            call_card = itemView.findViewById(R.id.call_card);
            mail_card = itemView.findViewById(R.id.mail_card);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }





}

