package com.nextclick.crm.activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.adapters.NotificationsAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.notificationDetailsResponse.NotificationDetails;
import com.nextclick.crm.models.responseModels.notificationDetailsResponse.NotificationDetailsResponse;
import com.google.gson.Gson;
import com.nextclick.crm.subcriptions.FeaturesModel;
import com.nextclick.crm.subcriptions.PackageFeatureModel;
import com.nextclick.crm.subcriptions.SubscriptionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.nextclick.crm.Config.Config.NOTIFICATIONS;
import static com.nextclick.crm.Config.Config.OLD_SUBCRIPTIONS;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener,
        HttpReqResCallBack {
    PreferenceManager preferenceManager;

    private TextView tvError;
    private ImageView ivBackArrow;
    private RecyclerView rvNotificationDetails;
    private SubscriptionModel subscriptionModel;

    private LinkedList<NotificationDetails> listOFNotificationDetails;
    private boolean isPlanExists;

    private String token = "";
    private CustomDialog mCustomDialog;
    private Context mContext;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_notifications);
        initializeUi();
        initializeListeners();
        prepareDetails();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to notification activity");
        }
    }

    private void initializeUi() {
        mContext = NotificationActivity.this;
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvNotificationDetails = findViewById(R.id.rvNotificationDetails);
        preferenceManager = new PreferenceManager(mContext);

        mCustomDialog = new CustomDialog(NotificationActivity.this);
        subscriptionModel = new SubscriptionModel();

        listOFNotificationDetails = new LinkedList<>();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getoldSubscriptions();
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        //   LoadingDialog.loadDialog(this);
        token = "Bearer " +new PreferenceManager(this).getString(USER_TOKEN);
        //  NotificationApiCall.serviceCallToGetNotifications(this, null, null, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_NOTIFICATIONS) {
            if (jsonResponse != null) {
                NotificationDetailsResponse notificationDetailsResponse = new Gson().fromJson(jsonResponse, NotificationDetailsResponse.class);
                if (notificationDetailsResponse != null) {
                    listOFNotificationDetails = notificationDetailsResponse.getListOfNotificationDetails();
                    if (listOFNotificationDetails != null && listOFNotificationDetails.size() > 0) {
                        listIsFull();
                        initializeAdapter();
                    } else {
                        listIsEmpty();
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvNotificationDetails.setVisibility(View.VISIBLE);
    }

    private void initializeAdapter() {
        NotificationsAdapter notificationsAdapter = new NotificationsAdapter(this, listOFNotificationDetails, isPlanExists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvNotificationDetails.setLayoutManager(layoutManager);
        rvNotificationDetails.setItemAnimator(new DefaultItemAnimator());
        rvNotificationDetails.setAdapter(notificationsAdapter);
    }
    private void getoldSubscriptions() {
        mCustomDialog.show();
        String url = OLD_SUBCRIPTIONS + "/?service_id=2";
        System.out.println("aaaaaaaa oldsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response != null) {
                                mCustomDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response OLD_SUBCRIPTIONS  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObject1 = jsonArray.getJSONObject(i);
                                        if (dataObject1.getString("status").equalsIgnoreCase("active")) {
                                            isPlanExists = true;
                                            String days = dataObject1.getJSONObject("packages").getString("days");
                                            String dateStop = dataObject1.getString("created_at");
                                            JSONArray packageFeaturesArray = dataObject1.getJSONArray("package_features");
//                                        JsonParser parser = new JsonParser();
//                                        JsonElement mJson = parser.parse(response);
//                                        Gson gson = new Gson();
//                                        CouponModel couponModel = gson.fromJson(mJson, CouponModel.class);
                                            ArrayList<PackageFeatureModel> packageFeatureModelArrayList = new ArrayList<>();
                                            for (int j = 0; j < packageFeaturesArray.length(); j++) {
                                                JSONObject packageFeatureObject = packageFeaturesArray.getJSONObject(j);
                                                PackageFeatureModel packageFeatureModel = new PackageFeatureModel();
                                                packageFeatureModel.setTitle(packageFeatureObject.getString("title"));
                                                packageFeatureModel.setIs_active(packageFeatureObject.getInt("is_active"));
                                                JSONArray featuresArray = packageFeatureObject.getJSONArray("features");
                                                ArrayList<FeaturesModel> featuresModelArrayList = new ArrayList<>();
                                                for (int k = 0; k < featuresArray.length(); k++) {
                                                    JSONObject featuresObject = featuresArray.getJSONObject(k);
                                                    FeaturesModel featuresModel = new FeaturesModel();
                                                    featuresModel.setDescription(featuresObject.getString("description"));
                                                    featuresModel.setStatus(featuresObject.getInt("status"));
                                                    featuresModelArrayList.add(featuresModel);
                                                    packageFeatureModel.setFeaturesModelArrayList(featuresModelArrayList);
                                                }
                                                packageFeatureModelArrayList.add(packageFeatureModel);
                                                subscriptionModel.setPackageFeatureModelArrayList(packageFeatureModelArrayList);
                                            }
                                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                            Date d1 = null;
                                            Date d2 = null;

                                            d2 = format.parse(dateStop);

                                            long diff = d2.getTime() - d1.getTime();

                                            long diffSeconds = diff / 1000 % 60;
                                            long diffMinutes = diff / (60 * 1000) % 60;
                                            long diffHours = diff / (60 * 60 * 1000) % 24;
                                            long diffDays = diff / (24 * 60 * 60 * 1000);

                                            System.out.print(diffDays + " days, ");
                                            System.out.print(diffHours + " hours, ");
                                            System.out.print(diffMinutes + " minutes, ");
                                            System.out.print(diffSeconds + " seconds.");

                                            Long remainingdays = Long.parseLong(days) + diffDays;
                                            System.out.println("aaaaaaa days  " + diffDays + " " + remainingdays);

                                            break;
                                        }
                                    }
                                } else {
//                                    showBottomSheet();
                                }

                            } else {
                                mCustomDialog.dismiss();
                            }
                        } catch (Exception e) {
                            mCustomDialog.dismiss();
                            e.printStackTrace();
                        } finally {
                            getNotifications();

                            if (!isPlanExists) {

                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("aaaaaaa error  " + error.getMessage());
                        mCustomDialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                //  map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjExIiwidGltZSI6MTYyNTExNjY3OX0.hGHKRItuzSimfyH_awycj58enHX_xncMhJWEfQpIjrg");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvNotificationDetails.setVisibility(View.GONE);
    }

    public void getNotifications() {
        listOFNotificationDetails.clear();
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NOTIFICATIONS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject notification  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject dataobj = jsonArray.getJSONObject(i);
                                    String id = dataobj.getString("id");
                                    String title = dataobj.getString("title");
                                    Integer ticket = dataobj.isNull("ticket_id") ? null : dataobj.getInt("ticket_id");
                                    NotificationDetails notificationDetails = new NotificationDetails();
                                    notificationDetails.setId(dataobj.getString("id"));
                                    notificationDetails.setTitle(dataobj.getString("title"));
                                    notificationDetails.setEcom_order_id(dataobj.getString("ecom_order_id"));
                                    notificationDetails.setMessage(dataobj.getString("message"));
                                    notificationDetails.setNotification_code(dataobj.getString("notification_code"));
                                    notificationDetails.setNotification_name(dataobj.getString("notification_name"));
                                    if (ticket != null) {
                                        notificationDetails.setTicket_id(dataobj.getInt("ticket_id"));
                                    }
                                    notificationDetails.setStatus(dataobj.getString("status"));
                                    if (!listOFNotificationDetails.contains(notificationDetails.getId())) {
                                        listOFNotificationDetails.add(notificationDetails);
                                    }
                                }
                                Set set = new TreeSet(new Comparator() {
                                    @Override
                                    public int compare(Object o1, Object o2) {
                                        if (((NotificationDetails) o1).getId().equalsIgnoreCase(((NotificationDetails) o2).getId())) {
                                            return 0;
                                        }
                                        return 1;
                                    }
                                });
                                set.addAll(listOFNotificationDetails);
                                final List newList = new ArrayList(set);
                                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                int count = 0;
                                for (int i = 0; i < listOFNotificationDetails.size(); i++) {
                                    if (listOFNotificationDetails.get(i).getStatus().equalsIgnoreCase("1")) {
                                        count++;

                                    }
                                }
                                System.out.println("aaaaaaaaa " + count);
                                rvNotificationDetails.setLayoutManager(layoutManager);
                                NotificationsAdapter notificationsAdapter = new NotificationsAdapter(mContext, listOFNotificationDetails,isPlanExists);
                                rvNotificationDetails.setAdapter(notificationsAdapter);
                                if (newList.isEmpty()) {
                                    tvError.setVisibility(View.VISIBLE);
                                    rvNotificationDetails.setVisibility(View.GONE);
                                } else {
                                    tvError.setVisibility(View.GONE);
                                    rvNotificationDetails.setVisibility(View.VISIBLE);
                                }

                            }

                        } catch (JSONException e) {
                            tvError.setVisibility(View.VISIBLE);
                            rvNotificationDetails.setVisibility(View.GONE);
                            Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                tvError.setVisibility(View.VISIBLE);
                rvNotificationDetails.setVisibility(View.GONE);
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  HashMap<String, String> headers = new HashMap<>();
                map.put(AUTH_TOKEN,"Bearer " + new PreferenceManager(mContext).getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                System.out.println("aaaaaaaa headers  " + map);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}
