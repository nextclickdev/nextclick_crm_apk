package com.nextclick.crm.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.adapters.SupportListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SUPPORTLIST;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SupportListActivity extends AppCompatActivity {

    private ArrayList<SupportModel> supportlist;
    CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private RecyclerView recycle_support;
    private TextView tv_nodata;
    private SupportListAdapter supportListAdapter;
    private FloatingActionButton add_product_fab;
    private ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_support_list);
        recycle_support=findViewById(R.id.recycle_support);
        tv_nodata=findViewById(R.id.tv_nodata);
        add_product_fab=findViewById(R.id.add_product_fab);
        img_back=findViewById(R.id.img_back);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to support activity");
        }

        mContext=SupportListActivity.this;
        supportlist=new ArrayList<>();
        mCustomDialog=new CustomDialog(SupportListActivity.this);
        preferenceManager=new PreferenceManager(mContext);
        recycle_support.setLayoutManager(new GridLayoutManager(mContext,1));
        supportListAdapter=new SupportListAdapter(mContext,supportlist);
        recycle_support.setAdapter(supportListAdapter);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        add_product_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(mContext,SupportActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportList();
    }

    public void getSupportList(){
        supportlist.clear();

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SUPPORTLIST,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e){

                                    }

                                }
                            }
                            //  mCustomDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");

                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray.length();i++){
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    SupportModel supportModel=new SupportModel();
                                    supportModel.setId(jsonObject1.getString("id"));
                                    supportModel.setToken_no(jsonObject1.getString("token_no"));
                                    supportModel.setApp_details_id(jsonObject1.getString("app_details_id"));
                                    supportModel.setRequest_type_id(jsonObject1.getString("request_type_id"));
                                    supportModel.setMobile(jsonObject1.getString("mobile"));
                                    supportModel.setEmail(jsonObject1.getString("email"));
                                    supportModel.setSubject(jsonObject1.getString("subject"));
                                    supportModel.setMessage(jsonObject1.getString("message"));
                                    supportModel.setQuery_owner_id(jsonObject1.getString("query_owner_id"));
                                    supportModel.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    supportModel.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    supportModel.setCreated_at(jsonObject1.getString("created_at"));
                                    supportModel.setUpdated_at(jsonObject1.getString("updated_at"));
                                    supportModel.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    supportModel.setStaus(jsonObject1.getString("staus"));
                                    supportModel.setApp_id(jsonObject1.getString("app_id"));
                                    supportModel.setApp_name(jsonObject1.getString("app_name"));
                                    supportModel.setTitle(jsonObject1.getString("title"));

                                    supportlist.add(supportModel);
                                }

                                if (supportlist.size()!=0){
                                    tv_nodata.setVisibility(View.GONE);
                                    recycle_support.setVisibility(View.VISIBLE);
                                    supportListAdapter.setrefresh(supportlist);
                                }else {
                                    tv_nodata.setVisibility(View.VISIBLE);
                                    recycle_support.setVisibility(View.GONE);
                                }
                            }

                        } catch (JSONException e) {
                            if (mCustomDialog!=null)
                            {
                                if (mCustomDialog.isShowing())
                                {
                                    try{
                                        mCustomDialog.dismiss();
                                    }catch (IllegalArgumentException e1){

                                    }
                                }
                            }

                            tv_nodata.setVisibility(View.VISIBLE);
                            recycle_support.setVisibility(View.GONE);
                            //   mCustomDialog.dismiss();
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mCustomDialog!=null)
                {
                    if (mCustomDialog.isShowing())
                    {
                        try{
                            mCustomDialog.dismiss();
                        }catch (IllegalArgumentException e){

                        }
                    }
                }
                tv_nodata.setVisibility(View.VISIBLE);
                recycle_support.setVisibility(View.GONE);
                //  mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
               // map.put("APP_ID",APP_ID_VALUE);

                return map;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}