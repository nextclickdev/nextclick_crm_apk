package com.nextclick.crm.activities.customer_support

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nextclick.crm.R

class CustomerSupportAdapter(val customerList: ArrayList<CustomerSupportItem>,val context: Context) :
    RecyclerView.Adapter<CustomerSupportAdapter.Viewholder>() {
    class Viewholder(itemview: View) : RecyclerView.ViewHolder(itemview) {
	  val title = itemview.findViewById<TextView>(R.id.tvTitle)
	  val des = itemview.findViewById<TextView>(R.id.tvDescription)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
	  val view = LayoutInflater.from(parent.context)
		.inflate(R.layout.customer_support_item, parent, false)

	  return Viewholder(view)
    }

    override fun getItemCount(): Int = customerList.size

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
	  val data = customerList[position]
	  holder.title.text = data.title
	  holder.des.text = data.description
	  holder.itemView.setOnClickListener {
		context.startActivity(Intent(context,TicketsActivity::class.java).putExtra("id",data.id))
	  }
    }
}