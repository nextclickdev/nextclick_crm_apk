package com.nextclick.crm.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Activities.RegisterVendorActivity;
import com.nextclick.crm.Common.Models.TermsandConditions;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class TermsAndConditionsActivity extends AppCompatActivity {

    private Context mContext;
    private WebView webView;
    private TextView tv_terms_id;
    private CustomDialog customDialog;
    private ImageView image_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_conditions);
        mContext=TermsAndConditionsActivity.this;
        webView = findViewById(R.id.ss_web);
        tv_terms_id = findViewById(R.id.tv_terms_id);
        image_back = findViewById(R.id.image_back);
        customDialog=new CustomDialog(mContext);
        gettermsandconditions();

        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void gettermsandconditions() {
        customDialog.show();
        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id=", "4");
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS+"?page_id=4",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);
                        customDialog.dismiss();

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try{
                                        JSONArray dataarray = jsonObject.getJSONArray("data");
                                        JSONObject dataobject = dataarray.getJSONObject(0);

                                        webView.loadDataWithBaseURL(null, dataobject.getString("desc"), "text/html; charset=UTF-8", "utf-8", null);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                    }catch (Exception e){
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        webView.loadDataWithBaseURL(null, dataobject.getString("desc"), "text/html; charset=UTF-8", "utf-8", null);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, "Not Found");
                                finish();
                            }
                        } else {
                            UIMsgs.showToast(mContext, "Not Found");
                            finish();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        UIMsgs.showToast(mContext, "Not Found");
                        finish();
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}