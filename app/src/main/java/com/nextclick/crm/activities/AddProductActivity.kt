package com.nextclick.crm.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Html
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.nextclick.crm.Config.Config
import com.nextclick.crm.Constants.Constants
import com.nextclick.crm.Constants.ValidationMessages
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs
import com.nextclick.crm.Helpers.UIHelpers.Utility
import com.nextclick.crm.R
import com.nextclick.crm.ShopNowModule.Adapters.OptionsAdapter
import com.nextclick.crm.ShopNowModule.Models.ImagesModel
import com.nextclick.crm.ShopNowModule.Models.ProductAdd
import com.nextclick.crm.Utilities.PreferenceManager
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel
import com.nextclick.crm.apiServices.RetrofitClient
import com.nextclick.crm.imagesselect.ImagesActivity
import com.nextclick.crm.productDetails.activities.AddNewBrandActivity
import com.nextclick.crm.productDetails.adapters.ImageSelectionAdapter
import com.nextclick.crm.productDetails.adapters.SelectedImageAdapter
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL

/**
 * Created by Arun Vegyas on 18-07-2023.
 */
class AddProductActivity : AppCompatActivity(), View.OnClickListener,
    CompoundButton.OnCheckedChangeListener {
    private lateinit var img_add: ImageView
    private lateinit var back_image: ImageView
    private lateinit var banner_image: ImageView
    private lateinit var upload_image: ImageView
    private lateinit var tv_addimage: ImageView
    private lateinit var layout_addproduct: LinearLayout
    private lateinit var layout_itemtype: LinearLayout
    lateinit var product_name: TextInputEditText
    lateinit var product_desc: TextInputEditText
    lateinit var product_price: TextInputEditText
    lateinit var product_quantity: TextInputEditText
    lateinit var product_discount: TextInputEditText
    lateinit var product_option: EditText
    lateinit var tv_addnewbrand: TextView
    lateinit var category_spinner: Spinner
    lateinit var menu_spinner: Spinner
    lateinit var brands_spinner: Spinner
    lateinit var item_type_spinner: Spinner

    //    private lateinit var layout_add: LinearLayout
    lateinit var listedittext: ArrayList<String>
    lateinit var productaddlist: ArrayList<ProductAdd>
    private var countoptions = 0
    private lateinit var submit: Button
    private val allViews: MutableList<View?> = ArrayList()
    private lateinit var multiselecttext: Array<EditText?>
    lateinit var optionsAdapter: OptionsAdapter
    private lateinit var recycle_views: RecyclerView
    private lateinit var userChoosenTask: String
    private val REQUEST_CAMERA = 0
    private val SELECT_FILE = 1
    lateinit var disposable: Disposable

    private lateinit var product_name_str: String
    private lateinit var product_desc_str: String
    private var base64image = ""
    private lateinit var token: String
    private var sub_cat_id: String = ""
    private var cat_id: String = ""
    private var menu_id: String = ""
    private var brand_id: String = ""
    private var item_type_id = "3"
    private var product_price_str: String = ""
    private var product_quantity_str: String = ""
    private var product_discount_str: String = ""
    private var product_option_str: String = ""
    private lateinit var imageslist: ArrayList<ImagesModel>

    // private ArrayList<Shopbycategeries> categoriesList;
    private lateinit var categoriesList: ArrayList<String>
    private lateinit var categoryIDList: ArrayList<String>
    private lateinit var catidlist: ArrayList<String>
    private lateinit var producttypelist: ArrayList<String>
    private var menusList = ArrayList<String>()
    private var menuIDList = ArrayList<String>()
    private var brandidlist = ArrayList<String>()
    private var brandlist = ArrayList<String>()
    private var itemtypelist = ArrayList<String>()
    private lateinit var product_status_type: TextView
    private var status_type = 1
    private lateinit var mContext: Context
    private lateinit var switchCompact: SwitchCompat

    //    private lateinit var veg_radio_btn: RadioButton
//    private lateinit var non_veg_radio_btn: RadioButton
    lateinit var preferenceManager: PreferenceManager
    private var productAvailability = 1
    private val item_type = 0
    private val veg = 1
    private val nonveg = 2
    private var isImageSelected = false
    private var currentposition = 0
    private var sectionid = 0
    private lateinit var images_recycle: RecyclerView
    private lateinit var selectedImageAdapter: ImageSelectionAdapter
    private lateinit var imagelist: ArrayList<String>
    private lateinit var base64imglist: ArrayList<String>
    var product_id: String = ""
    lateinit var gson: Gson
    private lateinit var customDialog: CustomDialog
    var checkpermisstiona = false
    override fun onCreate(savedInstanceState: Bundle?) {
	  super.onCreate(savedInstanceState)
	  setContentView(R.layout.activity_add_product)
	  checkpermisstiona = Utility.checkPermission(this)
	  init()
	  prepareDetails()
	  prepareSpinnerDetails()
	  itemtypelist = ArrayList()
	  itemtypelist.add("Veg")
	  itemtypelist.add("Non Veg")
	  itemtypelist.add("Others")
	  val adapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_item, itemtypelist)
	  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
	  item_type_spinner.adapter = adapter
	  item_type_spinner.setSelection(2)
	  if (MyMixPanel.isMixPanelSupport) {
		MyMixPanel.logEvent("Vendor navigated to product add or update activity")
	  }
    }

    fun init() {
	  mContext = this
	  preferenceManager = PreferenceManager(mContext)
	  token = "Bearer " + preferenceManager.getString(Constants.USER_TOKEN)
	  customDialog = CustomDialog(mContext)
	  img_add = findViewById(R.id.img_add)
	  upload_image = findViewById(R.id.upload_image)
	  layout_addproduct = findViewById(R.id.layout_addproduct)
	  product_status_type = findViewById(R.id.product_status_type)
//	  layout_add = findViewById(R.id.layout_add)
	  submit = findViewById(R.id.submit)
	  recycle_views = findViewById(R.id.recycle_views)
	  back_image = findViewById(R.id.back_image)
	  banner_image = findViewById(R.id.banner_image)
	  tv_addimage = findViewById(R.id.tv_addimage)
	  product_name = findViewById(R.id.product_name)
	  product_desc = findViewById(R.id.product_desc)
	  category_spinner = findViewById(R.id.category_spinner)
	  brands_spinner = findViewById(R.id.brands_spinner)
	  switchCompact = findViewById(R.id.switchCompact)
//	  veg_radio_btn = findViewById(R.id.veg_radio_btn)
//	  non_veg_radio_btn = findViewById(R.id.non_veg_radio_btn)
	  menu_spinner = findViewById(R.id.menu_spinner)
	  product_price = findViewById(R.id.product_price)
	  product_option = findViewById(R.id.product_option)
	  product_quantity = findViewById(R.id.product_quantity)
	  product_discount = findViewById(R.id.product_discount)
	  images_recycle = findViewById(R.id.images_recycle)
	  item_type_spinner = findViewById(R.id.item_type_spinner)
	  layout_itemtype = findViewById(R.id.layout_itemtype)
	  tv_addnewbrand = findViewById(R.id.tv_addnewbrand)

	  // product_name.setFilters(new InputFilter[]{filter});
	  /*    product_name.addTextChangedListener(new TextWatcher() {
            CharSequence previous;
            public void afterTextChanged(Editable s) {
                if(s.toString().contains("&^%$#*@&(")){
                    s.clear();
                    s.append(previous.toString());
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previous = s;
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });*/gson = Gson()
	  base64imglist = ArrayList()
	  imageslist = ArrayList()
	  imagelist = ArrayList()
	  listedittext = ArrayList()
	  productaddlist = ArrayList()
	  img_add.setOnClickListener(this)
	  layout_addproduct.setOnClickListener(this)
	  back_image.setOnClickListener(this)
	  banner_image.setOnClickListener(this)
	  tv_addimage.setOnClickListener(this)
	  submit.setOnClickListener(this)
	  upload_image.setOnClickListener(this)
	  switchCompact.setOnCheckedChangeListener(this)
	  multiselecttext = arrayOfNulls(3)
	  currentposition = intent.getIntExtra("currentposition", 0)
        recycle_views.layoutManager = GridLayoutManager(this, 1)
	  optionsAdapter = OptionsAdapter(this, productaddlist)
        recycle_views.adapter = optionsAdapter
	  selectedImageAdapter = ImageSelectionAdapter(this, imageslist)
        images_recycle.adapter = selectedImageAdapter
	  try {
		categories
//		val thread = Thread()
//		categories
//		/*thread.sleep(1000);
//            getMenus();*/Thread.sleep(1000)
	  } catch (e: Exception) {
		e.printStackTrace()
	  }
	  val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        images_recycle.layoutManager = layoutManager
	  tv_addnewbrand.setOnClickListener(View.OnClickListener {
		val i = Intent(this, AddNewBrandActivity::class.java)
		startActivity(i)
	  })
    }

    private fun prepareDetails() {
	  try {
		val productAdd = ProductAdd()
		productAdd.stock = "0"
		productAdd.price = ""
		productAdd.sizecolor = ""
		productAdd.discount = ""
		productAdd.item_id = ""
		productAdd.id = "0"
		productaddlist.add(productAdd)
		optionsAdapter.setrefresh(productaddlist, 0)
		if (imageslist.size == 0) {
		    imageslist.add(ImagesModel())
		    imageslist.add(ImagesModel())
		}
		upload_image.visibility = View.GONE
		images_recycle.visibility = View.VISIBLE
		selectedImageAdapter.refresh(imageslist)

	  } catch (e: Exception) {
		println("aaaaaaaa cathc " + e.message)
		e.printStackTrace()
	  }
    }

    private fun prepareSpinnerDetails() {
	  category_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    if (!category_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  sub_cat_id = categoryIDList[position - 1]
			  cat_id = catidlist[position - 1]
			  LoadingDialog.loadDialog(mContext)
			  getMenus(sub_cat_id)
			  getBrands(cat_id)
			  LoadingDialog.dialog.dismiss()
			  if (producttypelist[position - 1].equals("1", ignoreCase = true)) {
				layout_itemtype.visibility = View.VISIBLE
			  } else {
				layout_itemtype.visibility = View.GONE
			  }
		    } else {
			  sub_cat_id = ""
			  menu_id = ""
			  menuIDList.clear()
			  menusList.clear()
			  menu_spinner.adapter = null
		    }
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  menu_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    menu_id = if (!menu_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  menuIDList[position - 1]
		    } else ({
			  null
		    }).toString()
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  brands_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    brand_id = if (!brands_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  brandidlist[position - 1]
		    } else ({
			  null
		    }).toString()
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  item_type_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    item_type_id = "" + (position + 1)
		    println("aaaaaaaaaa item typemid  $item_type_id")
		    /* if (!item_type_spinner.getSelectedItem().toString().equalsIgnoreCase("Others")) {
                    item_type_id =""+position;
                } else {
                    item_type_id = "3";
                }*/
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
    }

    private val categories: Unit
	  private get() {

		val stringObjectHashMap = JsonObject()
          stringObjectHashMap.addProperty("q", "")
          val requestBody =
		    RequestBody.create(
			  MediaType.parse("application/json"),
			  Gson().toJson(stringObjectHashMap)
		    )
		val apiService = RetrofitClient.provideApi(this)
		disposable = apiService.getCategories(requestBody)
		    .subscribeOn(Schedulers.io())
		    .observeOn(AndroidSchedulers.mainThread())
		    .subscribe({
			  if (it.status) {
				val dataArray = it.data
				if (dataArray.isNotEmpty()) {
				    categoriesList = ArrayList()
				    categoryIDList = ArrayList()
				    catidlist = ArrayList()
				    producttypelist = ArrayList()
				    categoriesList.add("Select")
				    for (i in 0 until dataArray.size) {
					  val categoryObject = dataArray[i]
					  categoryIDList.add(categoryObject.id.toString())
					  categoriesList.add("" + Html.fromHtml(categoryObject.name))
					  producttypelist.add(categoryObject.product_type_widget_status.toString())
					  catidlist.add(categoryObject.cat_id.toString())
				    }
				    val adapter = ArrayAdapter(
                        mContext,
					  android.R.layout.simple_spinner_item, categoriesList
				    )
				    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
				    category_spinner.adapter = adapter
				}

			  } else {
				Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
			  }
		    }, {
			  Log.d("TAG", "submitFormToServer: ${it.localizedMessage}")
		    })

	  }

    private fun getBrands(sub_cat_id: String?) {
	  val apiService = RetrofitClient.provideApi(this)
	  disposable = apiService.getBrands(sub_cat_id)
		.subscribeOn(Schedulers.io())
		.observeOn(AndroidSchedulers.mainThread())
		.subscribe({
		    if (it.status) {
			  val dataArray = it.data.brands
			  if (dataArray.isNotEmpty()) {
				brandlist = ArrayList()
				brandidlist = ArrayList()
				brandlist.add("Select")
				for (i in 0 until dataArray.size) {
				    val categoryObject = dataArray[i]
				    brandlist.add("" + Html.fromHtml(categoryObject.name))
				    brandidlist.add(categoryObject.id.toString())
				}
				val adapter = ArrayAdapter(
                    mContext, android.R.layout.simple_spinner_item, brandlist
				)
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
				brands_spinner.adapter = adapter

				// Set selected brand if brand_id is available
				try {
				    if (brand_id != null) {
					  for (i in brandidlist.indices) {
						if (brand_id.equals(brandidlist[i], ignoreCase = true)) {
						    brands_spinner.setSelection(i + 1, true)
						    break
						}
					  }
				    }
				} catch (e: Exception) {
				    e.printStackTrace()
				}
			  }
		    }
		}, {
		    Log.d("TAG", "getbrands: ${it.localizedMessage}")
		})
    }

    private fun getMenus(cat_id: String) {
	  val apiService = RetrofitClient.provideApi(this)
	  disposable = apiService.getMenus(sub_cat_id)
		.subscribeOn(Schedulers.io())
		.observeOn(AndroidSchedulers.mainThread())
		.subscribe({
		    if (it.status) {
			  val dataArray = it.data.menus
			  if (dataArray.isNotEmpty()) {
				menusList = ArrayList()
				menuIDList = ArrayList()
				menusList.add("Select")
				for (i in 0 until dataArray.size) {
				    val categoryObject = dataArray[i]
				    menusList.add("" + Html.fromHtml(categoryObject.name))
				    menuIDList.add(categoryObject.id.toString())
				}
				val adapter = ArrayAdapter(
                    mContext, android.R.layout.simple_spinner_item, menusList
				)
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
				menu_spinner.adapter = adapter

				// Set selected menu if menu_id is available
				try {
				    if (menu_id != null) {
					  for (i in menuIDList.indices) {
						if (menu_id.equals(menuIDList[i], ignoreCase = true)) {
						    menu_spinner.setSelection(i + 1, true)
						    break
						}
					  }
				    }
				} catch (e: Exception) {
				    println("aaaaaaaaaa  catch3  " + e.message)
				    e.printStackTrace()
				}
			  }
		    }
		},{
		    Log.d("TAG", "getMenus: ${it.localizedMessage}")
		})
    }

    override fun onClick(p0: View?) {

    }
    fun uploadNewImage() {
	  val intent2 = Intent(this, ImagesActivity::class.java)
	  startActivityForResult(intent2, 2)
    }
    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
	  super.onActivityResult(requestCode, resultCode, data)
	  println("aaaaaaaaa resultcode  $resultCode")
	  if (resultCode == RESULT_OK) {
		if (requestCode == SELECT_FILE) onSelectFromGalleryResult(data) else if (requestCode == REQUEST_CAMERA) onCaptureImageResult(
		    data
		)
	  }
	  if (requestCode == 2) {
		try {
		    val carListAsString = data!!.getStringExtra("jsonCars")
		    val type = object : TypeToken<List<String?>?>() {}.type
		    imagelist = gson.fromJson(carListAsString, type)
		    println("aaaaaaaaa imaelistsize  " + imagelist.size)
		    for (i in imagelist.indices) {
			  try {
				val bm = BitmapFactory.decodeFile("" + imagelist.get(i))
				val base64 = convert(bm)
				base64imglist.add(base64)
				val imagesModel = ImagesModel()
				// imagesModel.setId("0");
				imagesModel.image = imagelist.get(i)
				imagesModel.base64 = base64
				imageslist.add(imageInsertIndex, imagesModel)
			  } catch (e: NullPointerException) {
				var image: Bitmap? =null
				try {
				    image =
					  BitmapFactory.decodeStream(URL(imagelist.get(i)).content as InputStream)
				    println("aaaaaaaaaaaa image  $image")
				} catch (e1: MalformedURLException) {
				    e.printStackTrace()
				    try {
					  image =
						BitmapFactory.decodeStream(URL("file:///" + imagelist.get(i)).content as InputStream)
				    } catch (s1: MalformedURLException) {
					  println("aaaaaaaaaaaaa image  11  " + e1.message)
				    } catch (ioException: IOException) {
					  ioException.printStackTrace()
					  println("aaaaaaaaaaaaa image  33  " + ioException.message)
				    }
				} catch (e2: IOException) {
				    println("aaaaaaaaaaaaa image 222  " + e2.message)
				    e.printStackTrace()
				}
				if (image != null) {
				    val base64 = convert(image)
				    base64imglist.add(base64)
				    val imagesModel = ImagesModel()
				    // imagesModel.setId("0");
				    imagesModel.image = imagelist.get(i)
				    imagesModel.base64 = base64
				    imageslist.add(imageInsertIndex, imagesModel)
				}
			  }
		    }
		    println("aaaaaaaa  imagelist sixe " + imageslist.size)
		    if (imageslist.size == 0) {
			  // upload_image.setVisibility(View.VISIBLE);
			  //  images_recycle.setVisibility(View.GONE);
			  //  }else {
			  imageslist.add(ImagesModel())
			  imageslist.add(ImagesModel())
		    }
		    upload_image.visibility = View.GONE
		    images_recycle.visibility = View.VISIBLE
		    selectedImageAdapter.refresh(imageslist)
		} catch (e: NullPointerException) {

		    // upload_image.setVisibility(View.VISIBLE);
		    //  images_recycle.setVisibility(View.GONE);
		    println("aaaaaaa catch " + e.message)
		}
	  }
    }
    fun convert(bitmap: Bitmap?): String {
	  val outputStream = ByteArrayOutputStream()
	  bitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, outputStream)
	  return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    private val imageInsertIndex: Int
	  private get() {
		var index = 0
		if (imageslist.size > 0) {
		    for (i in imageslist.indices.reversed()) {
			  if (imageslist[i].image != null) {
				index = i + 1
				break
			  }
		    }
		}
		return index
	  }
    private fun onSelectFromGalleryResult(data: Intent?) {
	  var bm: Bitmap? =null
	  if (data != null) {
		try {
		    bm =
			  MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
		    base64image = convert(bm)
		} catch (e: IOException) {
		    e.printStackTrace()
		}
	  }
	  val stream = ByteArrayOutputStream()
	  bm!!.compress(Bitmap.CompressFormat.JPEG, 50, stream)
	  val imageInByte = stream.toByteArray()
	  val lengthbmp = (imageInByte.size / 1024).toLong()
	  isImageSelected = true
	  banner_image.setImageBitmap(bm)
	  base64image = convert(bm)
    }

    private fun onCaptureImageResult(data: Intent?) {
	  val thumbnail = data!!.extras!!["data"] as Bitmap?
	  val bytes = ByteArrayOutputStream()
	  thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
	  val destination = File(
		Environment.getExternalStorageDirectory(),
		System.currentTimeMillis().toString() + ".jpg"
	  )
	  val fo: FileOutputStream
	  try {
		destination.createNewFile()
		fo = FileOutputStream(destination)
		fo.write(bytes.toByteArray())
		fo.close()
	  } catch (e: FileNotFoundException) {
		e.printStackTrace()
	  } catch (e: IOException) {
		e.printStackTrace()
	  }
	  isImageSelected = true
	  banner_image.setImageBitmap(thumbnail)
	  base64image = convert(thumbnail)
	  val stream = ByteArrayOutputStream()
	  val imageInByte = stream.toByteArray()
	  val lengthbmp = (imageInByte.size / 1024).toLong()
    }
}