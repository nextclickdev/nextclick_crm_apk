package com.nextclick.crm.subcriptions;

import static com.nextclick.crm.Config.Config.Buy_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.WALLETHISTORY;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Utilities.PreferenceManager;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;

import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.crm.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class BankTransferActivity extends AppCompatActivity {

    private String service_id,upgrade,payableAmount;
    SubscriptionModel subscriptionModel;
    private CustomDialog customDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    LinearLayout layout_upi;

    TextView tv_upi,tv_bank_name,tv_account_no,tv_account_name,tv_isfc_code,tv_totalamount;
    TextInputEditText tv_transaction_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transfer);
        mContext=BankTransferActivity.this;
        customDialog=new CustomDialog(BankTransferActivity.this);
        preferenceManager=new PreferenceManager(mContext);

        layout_upi=findViewById(R.id.layout_upi);
        tv_totalamount=findViewById(R.id.tv_totalamount);

        service_id=getIntent().getStringExtra("service_id");

        if(getIntent().hasExtra("upgrade"))
            upgrade=getIntent().getStringExtra("upgrade");

        if(service_id!=null && !service_id.isEmpty()) {
            subscriptionModel = (SubscriptionModel) getIntent().getSerializableExtra("data");

            if(subscriptionModel.getDifferenceAmount() > 0) {
                payableAmount = subscriptionModel.getDifferenceAmount().toString();
                tv_totalamount.setText(subscriptionModel.getDifferenceAmount().toString());
            }
            else
            {
                payableAmount = subscriptionModel.getPrice();
                /*String display_price = subscriptionModel.getDisplayPrice();
                if (display_price != null && !display_price.equals("null"))
                    payableAmount = display_price;*/
                tv_totalamount.setText(payableAmount);
            }
        }

        tv_transaction_no=findViewById(R.id.tv_transaction_no);
        tv_bank_name=findViewById(R.id.tv_bank_name);
        tv_account_no=findViewById(R.id.tv_account_no);
        tv_account_name=findViewById(R.id.tv_account_name);
        tv_isfc_code=findViewById(R.id.tv_isfc_code);
        tv_upi=findViewById(R.id.tv_upi);
        getBankDetails();
    }

    private void getBankDetails() {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_BANK_DETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject GET_BANK_DETAILS "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONArray dataobj=jsonObject.getJSONArray("data");
                                try{
                                    String isfc=dataobj.getJSONObject(0).getString("value");
                                    if(isfc!=null && !isfc.isEmpty() && !isfc.equals("null"))
                                        tv_upi.setText(isfc);
                                    else
                                        layout_upi.setVisibility(View.GONE);
                                    tv_bank_name.setText(dataobj.getJSONObject(1).getString("value"));
                                    tv_account_no.setText(dataobj.getJSONObject(2).getString("value"));
                                    tv_isfc_code.setText(dataobj.getJSONObject(3).getString("value"));

                                    tv_account_name.setText("Nextclick Info Solutions private limited");
                                    dataobj.getJSONObject(4).getString("value");

                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void CopyAction(View view) {
        String text="";
        switch (view.getId())
        {
            case R.id.tv_copy_upi: text=tv_upi.getText().toString();break;
            case R.id.tv_copy_bank_name: text=tv_bank_name.getText().toString();break;
            case R.id.tv_copy_account_no: text=tv_account_no.getText().toString();break;
            case R.id.tv_copy_account_name: text=tv_account_name.getText().toString();break;
            case R.id.tv_copy_ifsc_code: text=tv_isfc_code.getText().toString();break;
            case R.id.tv_copy_amount: text=tv_totalamount.getText().toString();break;
        }
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("text", text);
        clipboard.setPrimaryClip(clip);

        Toast.makeText(getApplicationContext(), text+" is copied" , Toast.LENGTH_SHORT).show();


        /*//paste
        ClipData pasteData = clipboard.getPrimaryClip();
        ClipData.Item item = pasteData.getItemAt(0);
        tv_transaction_no.setText(item.getText().toString());*/
    }

    public void goBackAction(View view) {
        finish();
    }public void SubmitAction(View view) {
        if (tv_transaction_no.getText().toString().isEmpty()) {
            tv_transaction_no.setError("Empty");
            tv_transaction_no.requestFocus();
        } else
            submitConfirmationAction(tv_transaction_no.getText().toString());
    }


    private void submitConfirmationAction(String paymentId) {
        Map<String, Object> mainData = new HashMap<>();
        mainData.put("payment_intent", "Subscription");//or promotion
        mainData.put("payment_txn_id", ""+paymentId);//upi/utr reference
        mainData.put("amount", ""+payableAmount);//actual amount

        Map<String, String> otherInfo = new HashMap<>();//business_address
        otherInfo.put("service_id", ""+service_id);//subscription id or promotion id--record_id
        otherInfo.put("package_id", ""+subscriptionModel.getId());//only for subscription
        if(upgrade!=null && upgrade.equals("1")) {
            otherInfo.put("upgrade", "1");//in case of subscription upgrade
            mainData.put("message", "Subscription Upgrade");
        }
        else
            mainData.put("message", "Buy Subscription");

        mainData.put("info", otherInfo);

        JSONObject json = new JSONObject(mainData);

        System.out.println("aaaaaaaaa request "+ json);
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Config.POST_MANUAL_PAYMENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Successfully submitted your payment information, please wait for admin approval.");
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, "Payment error");
                                }
                            } catch (Exception e) {
                                UIMsgs.showToast(mContext, "Payment error"+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UIMsgs.showToast(mContext, "Payment error");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  "+error.getMessage());
                        customDialog.dismiss();
                        UIMsgs.showToast(mContext, "Payment error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}