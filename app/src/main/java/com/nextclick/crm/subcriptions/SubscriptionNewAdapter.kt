package com.nextclick.crm.subcriptions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


/**
 * Created by Arun Vegyas on 20-06-2023.
 */
class SubscriptionNewAdapter(
    fm: FragmentManager,
    val subscriptionloist: ArrayList<SubscriptionModel>
) : FragmentPagerAdapter(
    fm,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {
    override fun getItem(position: Int): Fragment {
	  return SubscriptionPlanFragment(subscriptionloist[position],
		position)
    }

    override fun getCount(): Int {
	  val name = if (subscriptionloist.isEmpty()) 0 else subscriptionloist.size

	  return name
    }
}