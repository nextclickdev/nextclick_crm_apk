package com.nextclick.crm.subcriptions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class SubcriptionsActivity extends AppCompatActivity {
    private ImageView img_back;
    private TabLayout foodiemenutablayout;
    private ViewPager viewpager;
    private CustomDialog customDialog;
    private PreferenceManager preferenceManager;
    LinearLayoutManager layoutManager;
    com.nextclick.crm.subcriptions.ServicesAdapter servicesAdapter;

    private Context mContext;
    private ArrayList<ServicesModel> serviceModelArrayList;
    private int shopInIndex;
    private String isPlanExists ="false";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_subcriptions);

        //getSupportActionBar().hide();
       /* Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(),
                    R.color.Iconblue));
        }
*/

        if(getIntent().hasExtra("isPlanExists"))
            isPlanExists =getIntent().getStringExtra("isPlanExists");
        init();

    }
    public void init(){
        mContext=SubcriptionsActivity.this;
        viewpager=findViewById(R.id.viewpager);
        img_back=findViewById(R.id.img_back);
        foodiemenutablayout=findViewById(R.id.foodiemenutablayout);
        layoutManager = new LinearLayoutManager(this);
        preferenceManager = new PreferenceManager(mContext);
        foodiemenutablayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        customDialog=new CustomDialog(mContext);

        if(isPlanExists.equals("true")) {
           /* LinearLayout bottom_layout = findViewById(R.id.bottom_layout);
            bottom_layout.setVisibility(View.VISIBLE);

            TextView tv_upgrade = findViewById(R.id.tv_upgrade);
            tv_upgrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });*/
        }
      /*  serviceModelArrayList = (ArrayList<ServicesModel>) getIntent().getSerializableExtra("mylist");

        System.out.println("aaaaaaaa sizeee  "+serviceModelArrayList.size());

        servicesAdapter = new com.nextclick.crm.subcriptions.ServicesAdapter(mContext, getSupportFragmentManager(),serviceModelArrayList);
        viewpager.setAdapter(servicesAdapter);
        foodiemenutablayout.setupWithViewPager(viewpager);*/

        getServices();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
   /* @Override
    protected void onResume() {
        super.onResume();
        try{
            servicesAdapter = new com.nextclick.crm.subcriptions.ServicesAdapter(mContext, getSupportFragmentManager(),serviceModelArrayList);
            viewpager.setAdapter(servicesAdapter);
            foodiemenutablayout.setupWithViewPager(viewpager);
        }catch (NullPointerException e){

        }
    }*/


    private void getServices() {
        System.out.println("aaaaaaa services ");
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    JSONObject servicesObject = dataObject.getJSONObject("services");
                                    System.out.println("aaaaaaaaa  data  "+ dataObject);
                                    String is_admin=dataObject.getString("is_admin");
                                    String email=dataObject.getString("email");
                                    preferenceManager.putString("is_admin",is_admin);
                                    preferenceManager.putString("email",email);
                                    Iterator x = servicesObject.keys();
                                    JSONArray servicesjsonArray = new JSONArray();

                                    /*try {

                                        JSONArray jsonArray=dataObject.getJSONArray("contacts");
                                        for (int i=0;i<jsonArray.length();i++){
                                            JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                            if (i==0){
                                                String number = jsonObject1.getString("number");
                                                preferenceManager.putString("number",number);
                                            }
                                        }
                                    }catch (NullPointerException e){

                                    }*/
                                    while (x.hasNext()) {
                                        String key = (String) x.next();
                                        servicesjsonArray.put(servicesObject.get(key));
                                    }

                                    if (servicesjsonArray.length() > 0) {
                                        serviceModelArrayList = new ArrayList<>();
                                        for (int i = 0; i < servicesjsonArray.length(); i++) {
                                            ServicesModel serviceModel = new ServicesModel();
                                            JSONObject serviceObject = servicesjsonArray.getJSONObject(i);
                                            Log.d("service_object", serviceObject + "");
                                            String id = serviceObject.getString("id");
                                            String name = serviceObject.getString("name");
                                            String description = serviceObject.getString("desc");
                                            String languages = serviceObject.getString("languages");

                                            if(id.equals("2"))
                                                shopInIndex=i;

                                            String image = null;
                                            try {
                                                image = serviceObject.getString("image");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            serviceModel.setId(id);
                                            serviceModel.setName(name);
                                            serviceModel.setImage(image);
                                            serviceModel.setDesc(description);
                                            serviceModel.setLanguages(languages);
                                            serviceModelArrayList.add(serviceModel);
                                        }

                                        servicesAdapter = new com.nextclick.crm.subcriptions.ServicesAdapter(mContext, getSupportFragmentManager(),serviceModelArrayList,isPlanExists);
                                        viewpager.setAdapter(servicesAdapter);
                                        foodiemenutablayout.setupWithViewPager(viewpager);

                                        if(shopInIndex!=-1)
                                            viewpager.setCurrentItem(shopInIndex);
                                    }
                                } else {
                                    UIMsgs.showToast(mContext, "Service not available");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}