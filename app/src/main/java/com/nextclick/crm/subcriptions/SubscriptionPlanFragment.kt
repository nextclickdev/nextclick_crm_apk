package com.nextclick.crm.subcriptions

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.nextclick.crm.Config.Config
import com.nextclick.crm.Constants.Constants
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs
import com.nextclick.crm.R
import com.nextclick.crm.ShopNowModule.Activities.SubscriptionPlan
import com.nextclick.crm.ShopNowModule.Adapters.SubscriptionSettingAdapter2
import com.nextclick.crm.Utilities.PreferenceManager
import com.nextclick.crm.promocodes.adatpetrs.SubscriptionSettingAdapter
import com.nextclick.crm.promocodes.model.SubscriptionSetting
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

/**
 * Created by Arun Vegyas on 20-06-2023.
 */
class SubscriptionPlanFragment(val data: SubscriptionModel, val position: Int) : Fragment() {
    lateinit var tv_title: TextView
    lateinit var tv_prive: TextView
    lateinit var tv_original_price: TextView
    lateinit var tv_description: TextView
    lateinit var tv_error: TextView
    lateinit var recycle_status: RecyclerView
    lateinit var tv_days: TextView
    lateinit var tv_buynow: Button
    private var settingModalArrayList = ArrayList<SubscriptionSetting>()
    private var totalPackages = ArrayList<SubscriptionPlan>()
    private lateinit var recycle_settings: RecyclerView
    var whichclick = 0
    private val service_id: String = ""
    private lateinit var customDialog: CustomDialog
    private lateinit var preferenceManager: PreferenceManager

    lateinit var img_background: ImageView
    override fun onCreateView(
	  inflater: LayoutInflater,
	  container: ViewGroup?,
	  savedInstanceState: Bundle?
    ): View? {
	  return inflater.inflate(R.layout.subscription_layout, container, false)
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(itemView: View, savedInstanceState: Bundle?) {
	  super.onViewCreated(itemView, savedInstanceState)
	  customDialog = CustomDialog(requireContext())
	  recycle_settings = itemView.findViewById(R.id.recycle_settings)
	  recycle_status = itemView.findViewById(R.id.recycle_status)
	  tv_error = itemView.findViewById(R.id.tv_error)
	  tv_title = itemView.findViewById<TextView>(R.id.tv_title)
	  tv_prive = itemView.findViewById<TextView>(R.id.tv_prive)
	  tv_original_price = itemView.findViewById<TextView>(R.id.tv_original_price)
	  tv_description = itemView.findViewById<TextView>(R.id.tv_description)
	  tv_days = itemView.findViewById<TextView>(R.id.tv_days)
	  tv_buynow = itemView.findViewById<Button>(R.id.tv_buynow)
	  img_background = itemView.findViewById<ImageView>(R.id.img_background)
	  preferenceManager = PreferenceManager(requireContext())
        tv_title.text = data.getTitle() + " "
	  tv_days.text = requireContext().getString(R.string.plan_support) + " :" + data
		.getDays() + " " + requireContext().getString(R.string.days)
	  if (data != null && data.packageFeatureModelArrayList.isNotEmpty()) {
		recycle_settings.layoutManager = GridLayoutManager(
		    requireContext(),
		    1
		)
		val adapter = SubscriptionSettingAdapter(
		    requireContext(),
		    data.packageFeatureModelArrayList[0].featuresModelArrayList,
		    true
		)
		recycle_settings.adapter = adapter
		recycle_status.layoutManager = GridLayoutManager(
		    requireContext(),
		    1
		)

	  } else {
          recycle_settings.visibility = View.GONE
          recycle_status.visibility = View.GONE
          tv_error.visibility = View.VISIBLE
	  }
        tv_description.text = data.getDesc() + " "
	  img_background.setOnClickListener(View.OnClickListener {
//		val i = Intent(context, ViewSubscriptionPrivileges::class.java)
//		i.putExtra("serviceID", data.getService_id())
//		requireContext().startActivity(i)
	  })

//getSubscriptionServicesList()
	  if (whichclick == 0) {
          tv_buynow.text = requireContext().getString(R.string.buy_now)
	  } else {
          tv_buynow.text = "" + data.getStatus() + " "
	  }

	  if (data.getDifferenceAmount() > 0) {
          tv_buynow.text = requireContext().getString(R.string.upgrade_subsription)
	  }


	  val originalPrice: String = data.getPrice()
	  val display_price: String = data.getDisplayPrice()
        tv_original_price.visibility = View.GONE

	  //  if ((tv_prive.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0)
	  //    tv_prive.setPaintFlags( tv_prive.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));


	  //  if ((tv_prive.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0)
	  //    tv_prive.setPaintFlags( tv_prive.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
	  if ( /*whichclick==0 && */display_price != null && display_price != "null" && originalPrice != display_price && !originalPrice.equals(
		    "0",
		    ignoreCase = true
		)
	  ) {
          tv_original_price.visibility = View.VISIBLE
          tv_original_price.paintFlags = tv_original_price.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
          tv_original_price.text = "₹ $display_price "
		// price = display_price;
	  }


	  if (originalPrice.equals("0", ignoreCase = true)) {
          tv_prive.text = requireContext().getString(R.string.free)
		// if (whichclick == 0)
		//   tv_buynow.setText(context.getString(R.string.try_now));
	  } else {
          tv_prive.text = "₹ $originalPrice " //add space to avoid clipping at last workd
	  }

	  //

	  //
	  Glide.with(requireContext())
		.load(data.getImage())
		.error(R.drawable.ic_default_place_holder)
		.placeholder(R.drawable.ic_default_place_holder)
		.into(img_background)

	  tv_buynow.setOnClickListener(View.OnClickListener {
		if (whichclick == 0) {
		    if (data.getDifferenceAmount() > 0) {
			  val intent = Intent(context, PaymentActivityActivity::class.java)
			  intent.putExtra("data", data as Serializable?)
			  intent.putExtra("service_id", service_id)
			  intent.putExtra("upgrade", "1")
			  requireContext().startActivity(intent)
		    } else checkSubscription(data)
		} else {
		    // Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
		}

		/*  Intent intent=new Intent(context, PaymentActivityActivity.class);
			    intent.putExtra("data", (Serializable) data);
			    intent.putExtra("service_id",service_id);
			    context.startActivity(intent);*/
	  })
	  // Customize the layout and functionality of the subscription plan fragment
	  // Update the views and add any required logic
    }

    private fun getSubscriptionServicesList() {
	  settingModalArrayList = ArrayList<SubscriptionSetting>()
	  totalPackages = ArrayList<SubscriptionPlan>()
	  customDialog.show()
	  val url = Config.URL_GET_SUBSCRIPTION_PACKAGES + "/?service_id=2"
	  println("aaaaaaaaaa   URL_GET_SUBSCRIPTION_PACKAGES url $url")
	  val requestQueue = Volley.newRequestQueue(requireContext())
	  val stringRequest: StringRequest = object : StringRequest(
		Method.GET, url,
		Response.Listener<String> { response ->
		    try {
			  println("aaaaaaaaaa   URL_GET_SUBSCRIPTION_PACKAGES $response")
			  val jsonObject = JSONObject(response)
			  val status = jsonObject.getBoolean("status")
			  val http_code = jsonObject.getInt("http_code")
			  if (status && http_code == 200) {
				val responsearray = jsonObject.getJSONArray("data")
				for (k in 0 until responsearray.length()) {
				    val packageSetting = ArrayList<SubscriptionSetting>()
				    val featuresArray =
					  responsearray.getJSONObject(k).getJSONArray("features")
				    for (i in 0 until featuresArray.length()) {
					  val jsonObject1 = featuresArray.getJSONObject(i)
					  val key = jsonObject1.getString("description")
					  val setting_description = jsonObject1.getString("description")
					  val setting_status = jsonObject1.getString("status")
					  val subscriptionSetting = SubscriptionSetting(
						key,
						setting_description,
                          setting_status == "1"
					  )
					  if (k == 0) {
						settingModalArrayList.add(subscriptionSetting)
						if (i == 0) //for custom title
						    settingModalArrayList.add(subscriptionSetting)
					  }
					  if (i == 0) //for custom title
						packageSetting.add(subscriptionSetting)
					  packageSetting.add(subscriptionSetting)
				    }
				    val plan = SubscriptionPlan()
				    plan.subscriptionSetting = packageSetting
				    plan.title = responsearray.getJSONObject(k).getString("title")
				    plan.isActive = responsearray.getJSONObject(k).getString("is_active")
				    totalPackages.add(plan)
				}
			  }
		    } catch (e: JSONException) {
			  println("SUBSCRIPTION_SETTINGS   catch $e")
		    } finally {
			  customDialog.dismiss()
			  val settings: ArrayList<SubscriptionSetting> = settingModalArrayList
			  if (settings != null && settings.size > 0) {
                  recycle_settings.layoutManager = GridLayoutManager(
                      requireContext(),
                      1
                  )
//				val adapter = SubscriptionSettingAdapter(
//				    requireContext(),
//				    settings,
//				    true
//				)
//				recycle_settings.setAdapter(adapter)
                  recycle_status.layoutManager = GridLayoutManager(
                      requireContext(),
                      1
                  )
				val adapter1 = SubscriptionSettingAdapter2(
				    requireContext(), settings,
				    totalPackages, false
				)
                  recycle_status.adapter = adapter1
			  } else {
                  recycle_settings.visibility = View.GONE
                  recycle_status.visibility = View.GONE
                  tv_error.visibility = View.VISIBLE
			  }
		    }
		},
		Response.ErrorListener { error ->
		    println("SUBSCRIPTION_SETTINGS  settings error  " + error.message)
		    customDialog.dismiss()
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = java.util.HashMap()
		    map["Content-Type"] = "application/json"
		    println("aaaaaaaaaa token " + preferenceManager.getString(Constants.USER_TOKEN))
		    map[Constants.AUTH_TOKEN] = preferenceManager.getString(Constants.USER_TOKEN)
		    return map
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    private fun checkSubscription(subscriptionModel: SubscriptionModel) {
	  customDialog.show()
	  val mainData: MutableMap<String?, Any?> = HashMap()
	  mainData["service_id"] = "" + service_id
	  mainData["package_id"] = "" + subscriptionModel.getId()
	  val json = JSONObject(mainData)
	  val requestQueue = Volley.newRequestQueue(context)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.POST, Config.CHECK_SUBCRIPTIONS,
		Response.Listener<String?> { response ->
		    if (response != null) {
			  customDialog.dismiss()
			  try {
				val jsonObject = JSONObject(response)
				println("aaaaaaa response  $jsonObject")
				val status = jsonObject.getBoolean("status")
				if (!status) {
				    if (subscriptionModel.getPrice()
						.equals("0", ignoreCase = true)
				    ) { //subscriptionModel.getId().equalsIgnoreCase("4")
					  sendPaymentstatus(
						"", "", subscriptionModel.getPrice(), "",
						subscriptionModel.getId()
					  )
				    } else {
					  val intent = Intent(context, PaymentActivityActivity::class.java)
					  intent.putExtra("data", subscriptionModel as Serializable)
					  intent.putExtra("service_id", service_id)
					  requireContext().startActivity(intent)
				    }
				} else {
				    UIMsgs.showToast(context, "You have subscription")
				}
			  } catch (e: Exception) {
				UIMsgs.showToast(context, "You have subscription")
				e.printStackTrace()
			  }
		    } else {
			  customDialog.dismiss()
			  UIMsgs.showToast(context, "You have subscription")
		    }
		},
		Response.ErrorListener { error ->
		    println("aaaaaaa error  " + error.message)
		    customDialog.dismiss()
		    UIMsgs.showToast(context, "You have subscription")
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = preferenceManager.getString(Constants.USER_TOKEN)
		    return map
		}

		@RequiresApi(api = Build.VERSION_CODES.KITKAT)
		@Throws(AuthFailureError::class)
		override fun getBody(): ByteArray {
		    return json.toString()
			  .toByteArray(charset("utf-8"))
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    private fun sendPaymentstatus(
	  paymentmethod: String, paymentId: String, totalamt: String,
	  status: String, id: String
    ) {
	  val mainData: MutableMap<String?, Any?> = java.util.HashMap()
	  mainData["service_id"] = "" + service_id
	  mainData["package_id"] = "" + id
	  mainData["payment_method_id"] = "FREE"
	  //mainData.put("payment_gw_txn_id", ""+paymentId);
	  mainData["amount"] = "0"
	  mainData["status"] = "2"
	  val json = JSONObject(mainData)
	  println("aaaaaaaaa request $json")
	  customDialog.show()
	  val requestQueue = Volley.newRequestQueue(context)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.POST, Config.Buy_SUBCRIPTIONS,
		Response.Listener<String?> { response ->
		    if (response != null) {
			  customDialog.dismiss()
			  try {
				val jsonObject = JSONObject(response)
				println("aaaaaaa response  $jsonObject")
				val status = jsonObject.getBoolean("status")
				if (status) {
				    Toast.makeText(
					  context,
					  "You have been successfully subscribed",
					  Toast.LENGTH_SHORT
				    ).show()
				} else {
				    UIMsgs.showToast(context, "Payment error")
				}
			  } catch (e: java.lang.Exception) {
				UIMsgs.showToast(context, "Payment error" + e.message)
				e.printStackTrace()
			  }
		    } else {
			  customDialog.dismiss()
			  UIMsgs.showToast(context, "Payment error")
		    }
		},
		Response.ErrorListener { error ->
		    println("aaaaaaa error  " + error.message)
		    customDialog.dismiss()
		    UIMsgs.showToast(context, "Payment error")
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = java.util.HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = preferenceManager.getString(Constants.USER_TOKEN)
		    println("aaaaaaaa headers map  $map")
		    return map
		}

		@RequiresApi(api = Build.VERSION_CODES.KITKAT)
		@Throws(AuthFailureError::class)
		override fun getBody(): ByteArray {
		    return json.toString()
			  .toByteArray(charset("utf-8"))
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }
}
