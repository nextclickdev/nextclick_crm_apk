package com.nextclick.crm.subcriptions;

import java.io.Serializable;

public class FeaturesModel implements Serializable {
    String description, setting_key;
    int status;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSetting_key() {
        return setting_key;
    }

    public void setSetting_key(String setting_key) {
        this.setting_key = setting_key;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
