package com.nextclick.crm.subcriptions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ViewSubscriptionPrivileges;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.nextclick.crm.promocodes.adatpetrs.SubscriptionSettingAdapter;

import org.json.JSONObject;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.Buy_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.CHECK_SUBCRIPTIONS;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SubscriptionAdapter extends RecyclerView.Adapter<SubscriptionAdapter.ViewHolder> {

    private final Context context;
    private List<SubscriptionModel> data;
    SubCriptionsFragment subCriptionsFragment;
    private final CustomDialog customDialog;
    private final PreferenceManager preferenceManager;
    private final String service_id;
    int whichclick = 0;

    public SubscriptionAdapter(Context mContext, ArrayList<SubscriptionModel> serviceslist,
                               SubCriptionsFragment subCriptionsFragment, String service_id) {
        this.context = mContext;
        this.data = serviceslist;
        this.subCriptionsFragment = subCriptionsFragment;
        this.service_id = service_id;
        this.customDialog = new CustomDialog(context);
        this.preferenceManager = new PreferenceManager(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.subscription_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        holder.tv_title.setText(data.get(position).getTitle() + " ");
        holder.tv_days.setText(context.getString(R.string.plan_support) + " :" + data.get(position).getDays() + " " + context.getString(R.string.days));
        holder.tv_description.setText(data.get(position).getDesc() + " ");

//        holder.img_background.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(context, ViewSubscriptionPrivileges.class);
//                i.putExtra("serviceID",data.get(position).getService_id());
//                context.startActivity(i);
//            }
//        });
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (data.get(position).getStart_date() != null) {
                Date date2 = formatter2.parse(data.get(position).getStart_date());
                SimpleDateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
                String date = formatter1.format(date2);
                Date date1 = formatter2.parse(data.get(position).getEnd_date());
                SimpleDateFormat formatter3 = new SimpleDateFormat("dd/MM/yyyy");
                String date3 = formatter3.format(date1);

                holder.tv_duration.setVisibility(View.VISIBLE);
                if (data.get(position).status.equalsIgnoreCase("active")) {
                    holder.tv_duration.setText("Validity : " + date + " - " + date3);
                } else {
                    holder.tv_duration.setText("Validity : Expired");
                }
            } else {
                holder.tv_duration.setVisibility(View.GONE);
            }

        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        if (data != null && data.get(position).packageFeatureModelArrayList != null) {
            holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
            SubscriptionSettingAdapter adapter = new SubscriptionSettingAdapter(
                    context,
                    data.get(position).packageFeatureModelArrayList.get(0).featuresModelArrayList,
                    true
            );
            holder.recyclerView.setAdapter(adapter);
            holder.recyclerView.setVisibility(View.VISIBLE);

        } else {
            holder.recyclerView.setVisibility(View.GONE);
        }


        if (whichclick == 0) {
            holder.tv_buynow.setText(context.getString(R.string.buy_now));
        } else {
            holder.tv_buynow.setText("" + data.get(position).getStatus() + " ");
        }

        if (data.get(position).getDifferenceAmount() > 0) {
            holder.tv_buynow.setText(context.getString(R.string.upgrade_subsription));
        }
        if (data.get(position).isPending_payment_status()){
            holder.tv_buynow.setText("Awaiting approval");
        }

        String originalPrice = data.get(position).getPrice();
        String display_price = data.get(position).getDisplayPrice();
        String price = originalPrice;
        holder.tv_original_price.setVisibility(View.GONE);

        //  if ((holder.tv_prive.getPaintFlags() & Paint.STRIKE_THRU_TEXT_FLAG) > 0)
        //    holder.tv_prive.setPaintFlags( holder.tv_prive.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));

        if (/*whichclick==0 && */display_price != null && !display_price.equals("null") && !price.equals(display_price) && !price.equalsIgnoreCase("0")) {
            holder.tv_original_price.setVisibility(View.VISIBLE);
            holder.tv_original_price.setPaintFlags(holder.tv_original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tv_original_price.setText("₹ " + display_price + " ");
            // price = display_price;
        }


        if (price.equalsIgnoreCase("0")) {
            holder.tv_prive.setText(context.getString(R.string.free));
            // if (whichclick == 0)
            //   holder.tv_buynow.setText(context.getString(R.string.try_now));
        } else {
            holder.tv_prive.setText("₹ " + price + " ");//add space to avoid clipping at last workd
        }

        //
        Glide.with(context)
                .load(data.get(position).getImage())
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(holder.img_background);

        holder.tv_buynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (whichclick == 0) {
                    if (data.get(position).getDifferenceAmount() > 0) {
                        if (data.get(position).isPending_payment_status()){
                            Toast.makeText(context,"Your request is being reviewed. Approval pending.",Toast.LENGTH_SHORT).show();
                        } else {
                            subCriptionsFragment.doaction(data.get(position).getDifferenceAmount().toString(),data.get(position),service_id);
                        }

                    } else {
                        checkSubscription(data.get(position));
                    }
                } else {
                    // Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                }

              /*  Intent intent=new Intent(context, PaymentActivityActivity.class);
                intent.putExtra("data", (Serializable) data.get(position));
                intent.putExtra("service_id",service_id);
                context.startActivity(intent);*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setchange(ArrayList<SubscriptionModel> subscriptionloist, int whichclick) {
        this.data = subscriptionloist;
        this.whichclick = whichclick;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img_background;
        private final RecyclerView recyclerView;
        private final TextView tv_title;
        private final TextView tv_prive;
        private final TextView tv_original_price;
        private final TextView tv_description;
        private final TextView tv_days;
        private final TextView tv_buynow;
        private final TextView tv_duration;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_duration = itemView.findViewById(R.id.tvduration);
            tv_prive = itemView.findViewById(R.id.tv_prive);
            tv_original_price = itemView.findViewById(R.id.tv_original_price);
            tv_description = itemView.findViewById(R.id.tv_description);
            tv_days = itemView.findViewById(R.id.tv_days);

            tv_buynow = itemView.findViewById(R.id.tv_buynow);
            recyclerView = itemView.findViewById(R.id.recycle_settings);
            img_background = itemView.findViewById(R.id.img_background);

        }

    }

    private void sendPaymentstatus(String paymentmethod, String paymentId, String totalamt,
                                   String status, String id) {

        Map<String, Object> mainData = new HashMap<>();
        mainData.put("service_id", "" + service_id);
        mainData.put("package_id", "" + id);
        mainData.put("payment_method_id", "FREE");
        //mainData.put("payment_gw_txn_id", ""+paymentId);
        mainData.put("amount", "0");
        mainData.put("status", "2");

        JSONObject json = new JSONObject(mainData);

        System.out.println("aaaaaaaaa request " + json);
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Buy_SUBCRIPTIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    Toast.makeText(context, "You have been successfully subscribed", Toast.LENGTH_SHORT).show();
                                } else {
                                    UIMsgs.showToast(context, "Payment error");
                                }
                            } catch (Exception e) {
                                UIMsgs.showToast(context, "Payment error" + e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UIMsgs.showToast(context, "Payment error");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        UIMsgs.showToast(context, "Payment error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaaaa headers map  " + map);
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void checkSubscription(SubscriptionModel subscriptionModel) {
        customDialog.show();
        Map<String, Object> mainData = new HashMap<>();
        mainData.put("service_id", "" + service_id);
        mainData.put("package_id", "" + subscriptionModel.getId());
        JSONObject json = new JSONObject(mainData);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECK_SUBCRIPTIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (!status) {
                                    if (subscriptionModel.getPrice().equalsIgnoreCase("0")) {//subscriptionModel.getId().equalsIgnoreCase("4")
                                        sendPaymentstatus("", "", subscriptionModel.getPrice(), "",
                                                subscriptionModel.getId());
                                    } else {
                                        Intent intent = new Intent(context, PaymentActivityActivity.class);
                                        intent.putExtra("data", subscriptionModel);
                                        intent.putExtra("service_id", service_id);
                                        context.startActivity(intent);
                                    }
                                } else {
                                    UIMsgs.showToast(context, "You have subscription");
                                }
                            } catch (Exception e) {
                                UIMsgs.showToast(context, "You have subscription");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UIMsgs.showToast(context, "You have subscription");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        UIMsgs.showToast(context, "You have subscription");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}

