package com.nextclick.crm.subcriptions;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.OnDemandsModule.Fragments.HomeActivityFragments.ServicesFragment;
import com.nextclick.crm.R;

import java.util.ArrayList;


public class ServicesAdapter extends FragmentPagerAdapter {

    @StringRes
    //private static final int[] TAB_TITLES = new int[]{R.string.tab_text_1, R.string.tab_text_2, R.string.tab_text_3, R.string.tab_text_4};
    //private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    private static final int[] TAB_TITLES = new int[]{1, 2, 3, 4};
    public  ArrayList<ServicesModel> serviceslist = new ArrayList<>();
    private final Context mContext;
    String isPlanExists;

    private boolean doNotifyDataSetChangedOnce = false;

    public ServicesAdapter(Context mContext, FragmentManager fm,ArrayList<ServicesModel> serviceslist,String isPlanExists ) {
        super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.mContext = mContext;
        this.serviceslist = serviceslist;
        this.isPlanExists=isPlanExists;
        // System.out.println("aaaaaaaaaa menu create");
    }

    @Override
    public SubCriptionsFragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if (doNotifyDataSetChangedOnce) {
            doNotifyDataSetChangedOnce = false;
            notifyDataSetChanged();
        }
        ServicesModel foodMenuPojo = serviceslist.get(position);
        //foodMenuPojo.getId();
         return new SubCriptionsFragment(mContext,position + 1,foodMenuPojo,serviceslist,isPlanExists);
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        //return mContext.getResources().getString(TAB_TITLES[position]);
        //return String.valueOf(TAB_TITLES[position]);
        ServicesModel foodMenuPojo = serviceslist.get(position);
        return String.valueOf(foodMenuPojo.getName());
    }

    @Override
    public int getCount() {
        //return TAB_TITLES.length;
        return serviceslist.size();
    }
}

