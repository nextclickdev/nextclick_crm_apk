package com.nextclick.crm.subcriptions;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.LIST_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.OLD_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.UPGRADABLE_VENDOR_SUBCRIPTIONS;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SubCriptionsFragment extends Fragment {


    private TextView tv_old, tv_new, tv_history;
    private final Context mContext;
    private final int service_id;
    ServicesModel servicesModel;
    SubscriptionAdapter subscriptionAdapter;
    RecyclerView recycle_subscriptions;
    ArrayList<ServicesModel> serviceslist;
    ArrayList<SubscriptionModel> subscriptionloist;
    private PreferenceManager preferenceManager;
    private CustomDialog customDialog;
    private RelativeLayout layout_nodata;
    String isPlanExists;

    public SubCriptionsFragment(Context mContext, int i, ServicesModel servicesModel,
                                ArrayList<ServicesModel> serviceslist,
                                String isPlanExists) {
        this.mContext = mContext;
        this.service_id = i;
        this.servicesModel = servicesModel;
        this.serviceslist = serviceslist;
        this.isPlanExists = isPlanExists;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_criptions, container, false);
        tv_old = view.findViewById(R.id.tv_old);
        tv_new = view.findViewById(R.id.tv_new);
        tv_history = view.findViewById(R.id.tv_history);
        layout_nodata = view.findViewById(R.id.layout_nodata);
        recycle_subscriptions = view.findViewById(R.id.recycle_subscriptions);


        customDialog = new CustomDialog(getContext());
        preferenceManager = new PreferenceManager(mContext);
        subscriptionloist = new ArrayList<>();
        recycle_subscriptions.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        subscriptionAdapter = new SubscriptionAdapter(mContext, subscriptionloist, SubCriptionsFragment.this, servicesModel.getId());
        recycle_subscriptions.setAdapter(subscriptionAdapter);
        //  getSubscriptions();
        tv_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_new.setTypeface(Typeface.DEFAULT_BOLD);
                tv_old.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_history.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_new.setBackground(getContext().getResources().getDrawable(R.drawable.background_blue_full));
                tv_old.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_history.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_new.setTextColor(getContext().getResources().getColor(R.color.white));
                tv_old.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
                tv_history.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
                if (servicesModel.getId().equals("2") && isPlanExists.equals("true")) {
                    getUpgradeSubscriptions();
                } else
                    getSubscriptions();
            }
        });
        tv_old.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getoldSubscriptions();
                tv_old.setTypeface(Typeface.DEFAULT_BOLD);
                tv_new.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_history.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_old.setBackground(getContext().getResources().getDrawable(R.drawable.background_blue_full));
                tv_new.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_history.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_old.setTextColor(getContext().getResources().getColor(R.color.white));
                tv_new.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
                tv_history.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
            }
        });
        tv_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_history.setTypeface(Typeface.DEFAULT_BOLD);
                tv_old.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_new.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
                tv_history.setBackground(getContext().getResources().getDrawable(R.drawable.background_blue_full));
                tv_old.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_new.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
                tv_history.setTextColor(getContext().getResources().getColor(R.color.white));
                tv_old.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
                tv_new.setTextColor(getContext().getResources().getColor(R.color.Iconblue));

                getHistory();
            }
        });

        return view;
    }

    public void doaction(String difference, SubscriptionModel subscriptionModel, String s) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Upgrade Subscription");
        builder.setMessage("You already have an active subscription. To upgrade, a payment of " + difference + "/- is required");
        builder.setPositiveButton("Upgrade", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(requireContext(), PaymentActivityActivity.class);
                intent.putExtra("data", subscriptionModel);
                intent.putExtra("service_id", s);
                intent.putExtra("upgrade", "1");
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        subscriptionloist.clear();
        tv_new.setTypeface(Typeface.DEFAULT_BOLD);
        tv_old.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        tv_history.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        tv_new.setBackground(getContext().getResources().getDrawable(R.drawable.background_blue_full));
        tv_old.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
        tv_history.setBackground(getContext().getResources().getDrawable(R.drawable.oldnewsubscriptions));
        tv_new.setTextColor(getContext().getResources().getColor(R.color.white));
        tv_old.setTextColor(getContext().getResources().getColor(R.color.Iconblue));
        tv_history.setTextColor(getContext().getResources().getColor(R.color.Iconblue));

        //   getSubscriptions();

        if (servicesModel.getId().equals("2") && isPlanExists.equals("true")) {
            getUpgradeSubscriptions();
        } else
            getSubscriptions();
    }

    private void getUpgradeSubscriptions() {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("service_id", "" + servicesModel.getId());
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        subscriptionloist.clear();
        customDialog.show();
        String url = UPGRADABLE_VENDOR_SUBCRIPTIONS;//+"/?service_id="+servicesModel.getId()
        System.out.println("aaaaaaaa UPGRADABLE_VENDOR_SUBCRIPTIONS  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa UPGRADABLE_VENDOR_SUBCRIPTIONS  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObject = jsonArray.getJSONObject(i);
                                        SubscriptionModel subscriptionModel = new SubscriptionModel();
                                        subscriptionModel.setId(dataObject.getString("id"));
                                        subscriptionModel.setService_id(dataObject.getString("service_id"));
                                        subscriptionModel.setTitle(dataObject.getString("title"));
                                        subscriptionModel.setDesc(dataObject.getString("desc"));
                                        subscriptionModel.setDays(dataObject.getString("days"));
                                        subscriptionModel.setPrice(dataObject.getString("price"));
                                        if (dataObject.has("display_price"))
                                            subscriptionModel.setDisplayPrice(dataObject.getString("display_price"));
                                        subscriptionModel.setCreated_at(dataObject.getString("created_at"));
                                        subscriptionModel.setStatus(dataObject.getString("status"));

                                        if (dataObject.has("image"))
                                            subscriptionModel.setImage(dataObject.getString("image"));

                                         /* JSONObject service0bj=dataObject.getJSONObject("services");
                                        subscriptionModel.setServicesid(service0bj.getString("id"));
                                        subscriptionModel.setServicesname(service0bj.getString("name"));
                                        subscriptionModel.setServicesdesc(service0bj.getString("desc"));*/
                                        JSONArray packageFeaturesArray = dataObject.getJSONArray("package_features");
//                                        JsonParser parser = new JsonParser();
//                                        JsonElement mJson = parser.parse(response);
//                                        Gson gson = new Gson();
//                                        CouponModel couponModel = gson.fromJson(mJson, CouponModel.class);
                                        ArrayList<PackageFeatureModel> packageFeatureModelArrayList = new ArrayList<>();
                                        for (int j = 0; j < packageFeaturesArray.length(); j++) {
                                            JSONObject packageFeatureObject = packageFeaturesArray.getJSONObject(j);
                                            PackageFeatureModel packageFeatureModel = new PackageFeatureModel();
                                            packageFeatureModel.setTitle(packageFeatureObject.getString("title"));
                                            packageFeatureModel.setIs_active(packageFeatureObject.getInt("is_active"));
                                            JSONArray featuresArray = packageFeatureObject.getJSONArray("features");
                                            ArrayList<FeaturesModel> featuresModelArrayList = new ArrayList<>();
                                            for (int k = 0; k < featuresArray.length(); k++) {
                                                JSONObject featuresObject = featuresArray.getJSONObject(k);
                                                FeaturesModel featuresModel = new FeaturesModel();
                                                featuresModel.setDescription(featuresObject.getString("description"));
                                                featuresModel.setSetting_key(featuresObject.getString("setting_key"));
                                                featuresModel.setStatus(featuresObject.getInt("status"));
                                                featuresModelArrayList.add(featuresModel);
                                                packageFeatureModel.setFeaturesModelArrayList(featuresModelArrayList);
                                            }
                                            packageFeatureModelArrayList.add(packageFeatureModel);
                                            subscriptionModel.setPackageFeatureModelArrayList(packageFeatureModelArrayList);
                                        }
                                        if (dataObject.has("pending_payment_status")) {
                                            subscriptionModel.setPending_payment_status(dataObject.getBoolean("pending_payment_status"));
                                        }
                                        if (dataObject.has("differential"))
                                            subscriptionModel.setDifferenceAmount(Double.parseDouble(dataObject.getString("differential").replace(",", "")));

                                        subscriptionloist.add(subscriptionModel);

                                    }
                                    if (subscriptionloist.size() == 0) {
                                        layout_nodata.setVisibility(View.VISIBLE);
                                        recycle_subscriptions.setVisibility(View.GONE);
                                    } else {
                                        subscriptionAdapter.setchange(subscriptionloist, 0);
                                        layout_nodata.setVisibility(View.GONE);
                                        recycle_subscriptions.setVisibility(View.VISIBLE);
                                    }


                                } else {
                                    layout_nodata.setVisibility(View.VISIBLE);
                                    recycle_subscriptions.setVisibility(View.GONE);
                                    //  UIMsgs.showToast(mContext, "Subscriptions not available");
                                }
                            } catch (Exception e) {
                                layout_nodata.setVisibility(View.VISIBLE);
                                recycle_subscriptions.setVisibility(View.GONE);
                                System.out.println("aaaaaaa catch  " + e.getMessage());
                                // UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            layout_nodata.setVisibility(View.VISIBLE);
                            recycle_subscriptions.setVisibility(View.GONE);
                            //  UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        layout_nodata.setVisibility(View.VISIBLE);
                        recycle_subscriptions.setVisibility(View.GONE);
                        // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getSubscriptions() {
        subscriptionloist.clear();
        customDialog.show();
        String url = LIST_SUBCRIPTIONS + "/?service_id=" + servicesModel.getId();
        System.out.println("aaaaaaaa listsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObject = jsonArray.getJSONObject(i);
                                        SubscriptionModel subscriptionModel = new SubscriptionModel();
                                        subscriptionModel.setId(dataObject.getString("id"));
                                        subscriptionModel.setService_id(dataObject.getString("service_id"));
                                        subscriptionModel.setTitle(dataObject.getString("title"));
                                        subscriptionModel.setDesc(dataObject.getString("desc"));
                                        subscriptionModel.setDays(dataObject.getString("days"));
                                        subscriptionModel.setPrice(dataObject.getString("price"));
                                        if (dataObject.has("display_price"))
                                            subscriptionModel.setDisplayPrice(dataObject.getString("display_price"));
                                        //else
                                        //  subscriptionModel.setDisplayPrice("4222");
                                        subscriptionModel.setCreated_at(dataObject.getString("created_at"));
                                        subscriptionModel.setStatus(dataObject.getString("status"));
                                        subscriptionModel.setImage(dataObject.getString("image"));
                                        JSONObject service0bj = dataObject.getJSONObject("services");
                                        JSONArray packageFeaturesArray = dataObject.getJSONArray("package_features");
//                                        JsonParser parser = new JsonParser();
//                                        JsonElement mJson = parser.parse(response);
//                                        Gson gson = new Gson();
//                                        CouponModel couponModel = gson.fromJson(mJson, CouponModel.class);
                                        ArrayList<PackageFeatureModel> packageFeatureModelArrayList = new ArrayList<>();
                                        for (int j = 0; j < packageFeaturesArray.length(); j++) {
                                            JSONObject packageFeatureObject = packageFeaturesArray.getJSONObject(j);
                                            PackageFeatureModel packageFeatureModel = new PackageFeatureModel();
                                            packageFeatureModel.setTitle(packageFeatureObject.getString("title"));
                                            packageFeatureModel.setIs_active(packageFeatureObject.getInt("is_active"));
                                            JSONArray featuresArray = packageFeatureObject.getJSONArray("features");
                                            ArrayList<FeaturesModel> featuresModelArrayList = new ArrayList<>();
                                            for (int k = 0; k < featuresArray.length(); k++) {
                                                JSONObject featuresObject = featuresArray.getJSONObject(k);
                                                FeaturesModel featuresModel = new FeaturesModel();
                                                featuresModel.setDescription(featuresObject.getString("description"));
                                                featuresModel.setSetting_key(featuresObject.getString("setting_key"));
                                                featuresModel.setStatus(featuresObject.getInt("status"));
                                                featuresModelArrayList.add(featuresModel);
                                                packageFeatureModel.setFeaturesModelArrayList(featuresModelArrayList);
                                            }
                                            packageFeatureModelArrayList.add(packageFeatureModel);
                                            subscriptionModel.setPackageFeatureModelArrayList(packageFeatureModelArrayList);
                                        }
                                        // subscriptionModel.setDifferenceAmount(Double.parseDouble(subscriptionModel.getPrice()));

                                        subscriptionModel.setServicesid(service0bj.getString("id"));
                                        subscriptionModel.setServicesname(service0bj.getString("name"));
                                        subscriptionModel.setServicesdesc(service0bj.getString("desc"));

                                        subscriptionloist.add(subscriptionModel);

                                    }
                                    if (subscriptionloist.size() == 0) {
                                        layout_nodata.setVisibility(View.VISIBLE);
                                        recycle_subscriptions.setVisibility(View.GONE);
                                    } else {
                                        subscriptionAdapter.setchange(subscriptionloist, 0);
                                        layout_nodata.setVisibility(View.GONE);
                                        recycle_subscriptions.setVisibility(View.VISIBLE);
                                    }


                                } else {
                                    layout_nodata.setVisibility(View.VISIBLE);
                                    recycle_subscriptions.setVisibility(View.GONE);
                                    //  UIMsgs.showToast(mContext, "Subscriptions not available");
                                }
                            } catch (Exception e) {
                                layout_nodata.setVisibility(View.VISIBLE);
                                recycle_subscriptions.setVisibility(View.GONE);
                                System.out.println("aaaaaaa catch  " + e.getMessage());
                                // UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            layout_nodata.setVisibility(View.VISIBLE);
                            recycle_subscriptions.setVisibility(View.GONE);
                            //  UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        layout_nodata.setVisibility(View.VISIBLE);
                        recycle_subscriptions.setVisibility(View.GONE);
                        // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getoldSubscriptions() {
        subscriptionloist.clear();
        customDialog.show();
        String url = OLD_SUBCRIPTIONS + "/?service_id=" + servicesModel.getId();
        System.out.println("aaaaaaaa oldsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubscriptionModel subscriptionModel = new SubscriptionModel();
                                        JSONObject dataObject1 = jsonArray.getJSONObject(i);
                                        subscriptionModel.setService_id(dataObject1.getString("service_id"));
                                        subscriptionModel.setStatus(dataObject1.getString("status"));

                                        if (dataObject1.has("packages")) {
                                            JSONObject dataObject = dataObject1.getJSONObject("packages");
                                            subscriptionModel.setId(dataObject.getString("id"));
                                            subscriptionModel.setTitle(dataObject.getString("title"));
                                            subscriptionModel.setDesc(dataObject.getString("desc"));
                                            subscriptionModel.setDays(dataObject.getString("days"));
                                            subscriptionModel.setPrice(dataObject.getString("price"));

                                            if (dataObject.has("display_price"))
                                                subscriptionModel.setDisplayPrice(dataObject.getString("display_price"));
                                            if (dataObject.has("image"))
                                                subscriptionModel.setImage(dataObject.getString("image"));
                                        }
                                        subscriptionModel.setStart_date(dataObject1.getString("start_date"));
                                        subscriptionModel.setEnd_date(dataObject1.getString("end_date"));
                                        JSONObject service0bj = dataObject1.getJSONObject("services");

                                        subscriptionModel.setServicesid(service0bj.getString("id"));
                                        subscriptionModel.setServicesname(service0bj.getString("name"));
                                        subscriptionModel.setServicesdesc(service0bj.getString("desc"));

                                        if (dataObject1.getString("status").equalsIgnoreCase("active")) {
//                                            isPlanExists = "true";
                                            subscriptionloist.add(subscriptionModel);
                                        }


                                    }
                                    if (subscriptionloist.size() == 0) {
                                        layout_nodata.setVisibility(View.VISIBLE);
                                        recycle_subscriptions.setVisibility(View.GONE);
                                    } else {
                                        Collections.reverse(subscriptionloist);
                                        subscriptionAdapter.setchange(subscriptionloist, 1);
                                        layout_nodata.setVisibility(View.GONE);
                                        recycle_subscriptions.setVisibility(View.VISIBLE);
                                    }


                                } else {
                                    layout_nodata.setVisibility(View.VISIBLE);
                                    recycle_subscriptions.setVisibility(View.GONE);
                                    //  UIMsgs.showToast(mContext, "Subscriptions not available");
                                }
                            } catch (Exception e) {
                                layout_nodata.setVisibility(View.VISIBLE);
                                recycle_subscriptions.setVisibility(View.GONE);
                                System.out.println("aaaaaaa catch  " + e.getMessage());
                                // UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            layout_nodata.setVisibility(View.VISIBLE);
                            recycle_subscriptions.setVisibility(View.GONE);
                            //  UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        layout_nodata.setVisibility(View.VISIBLE);
                        recycle_subscriptions.setVisibility(View.GONE);
                        // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                // map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjExIiwidGltZSI6MTYyNTExNjY3OX0.hGHKRItuzSimfyH_awycj58enHX_xncMhJWEfQpIjrg");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getHistory() {
        subscriptionloist.clear();
        customDialog.show();
        String url = OLD_SUBCRIPTIONS + "/?service_id=" + servicesModel.getId();
        System.out.println("aaaaaaaa oldsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        SubscriptionModel subscriptionModel = new SubscriptionModel();
                                        JSONObject dataObject1 = jsonArray.getJSONObject(i);
                                        subscriptionModel.setService_id(dataObject1.getString("service_id"));
                                        subscriptionModel.setStatus(dataObject1.getString("status"));

                                        if (dataObject1.has("packages")) {
                                            JSONObject dataObject = dataObject1.getJSONObject("packages");
                                            subscriptionModel.setId(dataObject.getString("id"));
                                            subscriptionModel.setTitle(dataObject.getString("title"));
                                            subscriptionModel.setDesc(dataObject.getString("desc"));
                                            subscriptionModel.setDays(dataObject.getString("days"));
                                            subscriptionModel.setPrice(dataObject.getString("price"));

                                            if (dataObject.has("display_price"))
                                                subscriptionModel.setDisplayPrice(dataObject.getString("display_price"));
                                            if (dataObject.has("image"))
                                                subscriptionModel.setImage(dataObject.getString("image"));
                                        }
                                        subscriptionModel.setStart_date(dataObject1.getString("start_date"));
                                        subscriptionModel.setEnd_date(dataObject1.getString("end_date"));
                                        JSONObject service0bj = dataObject1.getJSONObject("services");

                                        subscriptionModel.setServicesid(service0bj.getString("id"));
                                        subscriptionModel.setServicesname(service0bj.getString("name"));
                                        subscriptionModel.setServicesdesc(service0bj.getString("desc"));

                                        if (!dataObject1.getString("status").equalsIgnoreCase("active")) {
//                                            isPlanExists = "true";
                                            subscriptionloist.add(subscriptionModel);
                                        }


                                    }
                                    if (subscriptionloist.size() == 0) {
                                        layout_nodata.setVisibility(View.VISIBLE);
                                        recycle_subscriptions.setVisibility(View.GONE);
                                    } else {
                                        Collections.reverse(subscriptionloist);
                                        subscriptionAdapter.setchange(subscriptionloist, 1);
                                        layout_nodata.setVisibility(View.GONE);
                                        recycle_subscriptions.setVisibility(View.VISIBLE);
                                    }


                                } else {
                                    layout_nodata.setVisibility(View.VISIBLE);
                                    recycle_subscriptions.setVisibility(View.GONE);
                                    //  UIMsgs.showToast(mContext, "Subscriptions not available");
                                }
                            } catch (Exception e) {
                                layout_nodata.setVisibility(View.VISIBLE);
                                recycle_subscriptions.setVisibility(View.GONE);
                                System.out.println("aaaaaaa catch  " + e.getMessage());
                                // UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            layout_nodata.setVisibility(View.VISIBLE);
                            recycle_subscriptions.setVisibility(View.GONE);
                            //  UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        customDialog.dismiss();
                        layout_nodata.setVisibility(View.VISIBLE);
                        recycle_subscriptions.setVisibility(View.GONE);
                        // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                // map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjExIiwidGltZSI6MTYyNTExNjY3OX0.hGHKRItuzSimfyH_awycj58enHX_xncMhJWEfQpIjrg");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


}