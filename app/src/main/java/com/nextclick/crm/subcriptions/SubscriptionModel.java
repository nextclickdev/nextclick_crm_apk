package com.nextclick.crm.subcriptions;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class SubscriptionModel implements Serializable {
    String id;
    String service_id;
    String title;
    String desc;
    String days;
    String price;
    String created_user_id;
    String updated_user_id;
    String created_at;
    String updated_at;
    String start_date;
    String end_date;

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    String deleted_at;
    String status;
    String servicesid;
    String servicesname;
    String servicesdesc;
    String image;
    String displayPrice;
    Double differenceAmount = -1d;
    boolean pending_payment_status;

    public boolean isPending_payment_status() {
        return pending_payment_status;
    }

    public void setPending_payment_status(boolean pending_payment_status) {
        this.pending_payment_status = pending_payment_status;
    }

    public ArrayList<PackageFeatureModel> getPackageFeatureModelArrayList() {
        return packageFeatureModelArrayList;
    }

    public void setPackageFeatureModelArrayList(ArrayList<PackageFeatureModel> packageFeatureModelArrayList) {
        this.packageFeatureModelArrayList = packageFeatureModelArrayList;
    }

    ArrayList<PackageFeatureModel> packageFeatureModelArrayList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(String updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServicesid() {
        return servicesid;
    }

    public void setServicesid(String servicesid) {
        this.servicesid = servicesid;
    }

    public String getServicesname() {
        return servicesname;
    }

    public void setServicesname(String servicesname) {
        this.servicesname = servicesname;
    }

    public String getServicesdesc() {
        return servicesdesc;
    }

    public void setServicesdesc(String servicesdesc) {
        this.servicesdesc = servicesdesc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getDifferenceAmount() {
        return differenceAmount;
    }

    public void setDifferenceAmount(Double differenceAmount) {
        this.differenceAmount = differenceAmount;
    }


}

