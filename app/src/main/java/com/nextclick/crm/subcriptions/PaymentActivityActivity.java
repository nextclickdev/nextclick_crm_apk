package com.nextclick.crm.subcriptions;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.Buy_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.WALLETHISTORY;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class PaymentActivityActivity extends AppCompatActivity implements PaymentResultWithDataListener {

    private CheckBox cb_walet;
    private TextView tv_availbal,tv_online,tv_totalamount,tv_bank_transfer;
    CardView card_wallet,card_pay_online,card_bank_transfer;
    private ImageView img_back;
    SubscriptionModel subscriptionModel;
    private CustomDialog customDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private boolean checkprice=false;
    private LinearLayout layout_totalbuy,layout_wallet;
    String year,month,day,start_date_str,end_date_str,walletamount,totalamount,usedWalletamount,service_id;
    String payableAmount,upgrade;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_payment_activity);
        mContext=PaymentActivityActivity.this;
        card_wallet=findViewById(R.id.card_wallet);
        card_pay_online=findViewById(R.id.card_pay_online);
        card_bank_transfer=findViewById(R.id.card_bank_transfer);

        cb_walet=findViewById(R.id.cb_walet);
        tv_availbal=findViewById(R.id.tv_availbal);
        tv_online=findViewById(R.id.tv_online);
        tv_bank_transfer=findViewById(R.id.tv_bank_transfer);
        layout_totalbuy=findViewById(R.id.layout_totalbuy);
        tv_totalamount=findViewById(R.id.tv_totalamount);
        img_back=findViewById(R.id.img_back);
        layout_wallet=findViewById(R.id.layout_wallet);
        customDialog=new CustomDialog(PaymentActivityActivity.this);
        preferenceManager=new PreferenceManager(mContext);


        service_id=getIntent().getStringExtra("service_id");

        if(getIntent().hasExtra("upgrade"))
            upgrade=getIntent().getStringExtra("upgrade");

        if(service_id!=null && !service_id.isEmpty()) {
            subscriptionModel = (SubscriptionModel) getIntent().getSerializableExtra("data");

            if(subscriptionModel.getDifferenceAmount() > 0) {
                payableAmount = subscriptionModel.getDifferenceAmount().toString();
                tv_totalamount.setText(subscriptionModel.getDifferenceAmount().toString());
            }
            else {
                payableAmount = subscriptionModel.getPrice();
                /*String display_price = subscriptionModel.getDisplayPrice();
                if (display_price != null && !display_price.equals("null"))
                    payableAmount = display_price;*/

                tv_totalamount.setText(payableAmount);
            }

            if (subscriptionModel.getPrice().equalsIgnoreCase("0")){
                sendPaymentstatus(""+0,"",""+0,""+2);
            }

        }
        if(getIntent().hasExtra("totalAmount")) {
            layout_wallet.setVisibility(View.GONE);
            payableAmount = getIntent().getStringExtra("totalAmount");
        }

        totalamount  = payableAmount;

        getDate();
        getWallet(start_date_str,end_date_str,"","");


        card_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Double.parseDouble(walletamount) <= (Double.parseDouble(payableAmount))) {
                        usedWalletamount = "";
                        cb_walet.setChecked(false);
                        Toast.makeText(mContext, "You don't have enough balance to pay", Toast.LENGTH_SHORT).show();
                    } else {
                        checkprice = true;
                        usedWalletamount = payableAmount;
                        setwalletsend("Wallet");
                    }
                }catch (Exception ex)
                {
                    usedWalletamount = "";
                    cb_walet.setChecked(false);
                    Toast.makeText(mContext, "You don't have enough balance to pay", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cb_walet.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                   // String amount=totalamount.toString().replace(getString(R.string.Rs),"").trim();
                    try {
                        if (Double.parseDouble(walletamount) <= (Double.parseDouble(payableAmount))) {
                            usedWalletamount = "";
                            cb_walet.setChecked(false);
                            Toast.makeText(mContext, "You don't have enough balance to pay", Toast.LENGTH_SHORT).show();
                        } else {
                            checkprice = true;
                            usedWalletamount = payableAmount;
                            setwalletsend("Wallet");
                        }
                    }catch (Exception ex)
                    {
                        usedWalletamount = "";
                        cb_walet.setChecked(false);
                        Toast.makeText(mContext, "You don't have enough balance to pay", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    totalamount=getIntent().getStringExtra("totalamount");
                }
            }
        });
        card_bank_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BankTransferActivity.class);
                intent.putExtra("data", subscriptionModel);
                intent.putExtra("service_id", service_id);
                intent.putExtra("upgrade", upgrade);
                mContext.startActivity(intent);
                finish();
            }
        });
        card_pay_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setTitle("Razorpay");
                alertDialogBuilder
                        .setMessage("Do You Want To Procced to razorpay")
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                razorPayPayment(""+totalamount);
                            }

                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                cb_walet.setChecked(false);
                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        layout_totalbuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkprice){
                    razorPayPayment(""+totalamount);
                }else {
                    Toast.makeText(mContext, "Select paymentmode", Toast.LENGTH_SHORT).show();
                }
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

    }
    public void getWallet(String start_date_str, String end_date_str, String s, String s1){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+ json);

        tv_availbal.setText("₹0");

        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");

                                try{
                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    walletamount=paymentobj.getString("wallet");
                                    tv_availbal.setText("₹"+paymentobj.getString("wallet"));
                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    public void setwalletsend(String title) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(""+title);
        alertDialogBuilder
                .setMessage("Do You Want To Procced")
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(subscriptionModel!=null)
                        sendPaymentstatus(""+3,"",""+usedWalletamount,""+2);
                        else
                        {
                            Intent intent = new Intent();
                            intent.putExtra("PaymentMethod","3");//wallet
                            intent.putExtra("PaymentStatus","2");//success
                            intent.putExtra("paymentId","");
                            setResult(AddPromotionActivity.FLAG_PAYMENT, intent);
                        }

                      /* if (paymentModes.getId().equalsIgnoreCase("1")){
                           dialog.cancel();
                           sendPaymentstatus(""+1,"",""+totalamount,""+1,"");
                       }else {
                           dialog.cancel();
                           razorPayPayment(""+totalamount);
                           Intent intent=new Intent(mContext,PaymentGateways.class);
                           intent.putExtra("totatlpayamount", Math.round(1) + "");
                           startActivity(intent);
                       }*/
                    }

                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        cb_walet.setChecked(false);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        co.setImage(R.mipmap.ic_launcher_round);
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            if(Utility.Skip_Final_PAYMENT)
                orderRequest.put("amount", "100"); // amount in the smallest currency unit
            else
                orderRequest.put("amount", ""+(Double.parseDouble(totatlAmount)*100));  // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image",R.mipmap.ic_launcher_round);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open(activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
           // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
           // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("aaaaaaaa  msg "+e.getMessage());
        }


    }
    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        View v = getWindow().getDecorView().getRootView();

        System.out.println("aaaaaaaaaa  data sucess  "+s+paymentData.toString());

        System.out.println("aaaaaaaaaa sucess  "+paymentData.getOrderId()+" "+paymentData.getPaymentId()+" "+paymentData.getSignature()+" "+
                paymentData.getUserContact()+"  "+paymentData.getData().toString()+" "+paymentData.getUserEmail());



        if(subscriptionModel!=null)
            sendPaymentstatus(""+2,paymentData.getPaymentId(),""+totalamount,""+2);//online
        else
        {
            Intent intent = new Intent();
            intent.putExtra("PaymentMethod","2");
            intent.putExtra("paymentId",paymentData.getPaymentId());
            intent.putExtra("PaymentStatus",true);
            setResult(AddPromotionActivity.FLAG_PAYMENT, intent);
        }

    }

    private void sendPaymentstatus(String paymentmethod, String paymentId, String totalamt,
                                   String status) {

        Map<String, Object> mainData = new HashMap<>();
        mainData.put("service_id", ""+service_id);
        mainData.put("package_id", ""+subscriptionModel.getId());
        mainData.put("payment_method_id", ""+paymentmethod);
        mainData.put("payment_gw_txn_id", ""+paymentId);
        mainData.put("amount", ""+totalamt);
        mainData.put("status", ""+status);
        if(upgrade!=null && upgrade.equals("1")) {
            mainData.put("upgrade", "1");
            mainData.put("message", "Subscription Upgrade");
        }
        else
            mainData.put("message", "Buy Subscription");

        JSONObject json = new JSONObject(mainData);

        System.out.println("aaaaaaaaa request "+ json);
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Buy_SUBCRIPTIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, "Payment error");
                                }
                            } catch (Exception e) {
                                UIMsgs.showToast(mContext, "Payment error"+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                            UIMsgs.showToast(mContext, "Payment error");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa error  "+error.getMessage());
                        customDialog.dismiss();
                        UIMsgs.showToast(mContext, "Payment error");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try{
            System.out.println("aaaaaaaaaa  payment  error  "+paymentData.toString()+"  "+s);
        }catch (NullPointerException e){

        }
        System.out.println("aaaaaaaaaa  payment  error  "+s);
        Toast.makeText(mContext, "Payment Cancelled", Toast.LENGTH_SHORT).show();
        // UImsgs.showToast(mContext, String.valueOf("Error " + "  " + i + " " + s));
       /* try {
            JSONObject jsonObject1=new JSONObject(s);
            JSONObject errorobj=jsonObject1.getJSONObject("error");
            String description=errorobj.getString("description");
            if(subscriptionModel!=null)
                sendPaymentstatus(""+3,"1",""+totalamount,""+3);
            else
            {
                Intent intent = new Intent();
                intent.putExtra("PaymentMethod","3");
                intent.putExtra("paymentId","1");
                intent.putExtra("PaymentStatus","3");//fail
                setResult(AddPromotionActivity.FLAG_PAYMENT, intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/


    }
}