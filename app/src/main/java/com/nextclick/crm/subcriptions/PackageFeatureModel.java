package com.nextclick.crm.subcriptions;

import java.io.Serializable;
import java.util.ArrayList;

public class PackageFeatureModel implements Serializable {
    String title;
    int is_active;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public ArrayList<FeaturesModel> getFeaturesModelArrayList() {
        return featuresModelArrayList;
    }

    public void setFeaturesModelArrayList(ArrayList<FeaturesModel> featuresModelArrayList) {
        this.featuresModelArrayList = featuresModelArrayList;
    }

    ArrayList<FeaturesModel> featuresModelArrayList;
}
