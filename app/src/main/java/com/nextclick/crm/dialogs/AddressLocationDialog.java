package com.nextclick.crm.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.ShopNowModule.Activities.BannerPromotionDashboard;
import com.nextclick.crm.ShopNowModule.Activities.MyPromotionsActivity;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsListActivity;
import com.nextclick.crm.ShopNowModule.Models.Constiuencies;
import com.nextclick.crm.ShopNowModule.Models.Districts;
import com.nextclick.crm.ShopNowModule.Models.States;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AddressLocationDialog {

    private final PromotionsListActivity activity;
    private final Context context;
    private android.app.AlertDialog.Builder builder;


    private ArrayList<States> statelist;
    private ArrayList<Districts> districtlist;
    private ArrayList<Constiuencies> constiencieslist;
    private String stateid="",districtid="",constitueid="";
    String constituency_id,district_id,state_id,state_name,distict_name,consti_name;
    Spinner spinner_state,spinner_distict,spinner_constitunesy;


    public AddressLocationDialog(Context ctx, PromotionsListActivity activity) {
        this.context = ctx;
        this.activity = activity;
    }

    public void showDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.select_constituency);

        Button applyButton = dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);
        spinner_state = dialog.findViewById(R.id.spinner_state);
        spinner_distict = dialog.findViewById(R.id.spinner_distict);
        spinner_constitunesy = dialog.findViewById(R.id.spinner_constitunesy);


        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(stateid==null || stateid.isEmpty())
                {
                    Toast.makeText(context, "Please select the State",
                            Toast.LENGTH_SHORT).show();
                }
                else if(districtid==null || districtid.isEmpty())
                {
                    Toast.makeText(context, "Please select the District",
                            Toast.LENGTH_SHORT).show();
                }
                else if(constitueid==null || constitueid.isEmpty())
                {
                    Toast.makeText(context, "Please select the Constituency",
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    dialog.dismiss();
                    activity.setConstituency(constitueid);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        statelist=new ArrayList<States>();
        districtlist=new ArrayList<Districts>();
        constiencieslist=new ArrayList<Constiuencies>();
        getStates();


        //hidden api, and see if data not exists while setting content type and also plase list inside inside layout not dilog\

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size()==0){

                }else {
                    if (position!=0){
                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        stateid=statelist.get(position-1).getId();
                        districtlist.clear();
                        constitueid=null;
                        getDistricts(statelist.get(position-1).getId());
                    }else {
                        spinner_distict.setEnabled(false);
                        spinner_distict.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(context,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_distict.setAdapter(ad);
                        spinner_constitunesy.setAdapter(ad);
                        constitueid=null;

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_distict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtlist.size()==0){
                    // Toast.makeText(PlacesAddActivity.this, "Please Select District", Toast.LENGTH_SHORT).show();
                }else {
                    if (position!=0){

                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setClickable(true);
                        spinner_constitunesy.setEnabled(true);
                        districtid=districtlist.get(position-1).getId();
                        constiencieslist.clear();
                        // getConstituencis(districtlist.get(position-1).getDistrictID());
                        constitueid=null;
                        getConstituencis(stateid,districtlist.get(position-1).getId());

                    }else {
                        spinner_constitunesy.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(context,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner_constitunesy.setAdapter(ad);
                        constitueid=null;
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_constitunesy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()){
                    //  Toast.makeText(context, "Please select District", Toast.LENGTH_SHORT).show();
                }else {
                    try{
                        if (position!=0){
                            constitueid=constiencieslist.get(position-1).getId();
                        }else{
                            //  Toast.makeText(context, "Please Select Constiency", Toast.LENGTH_SHORT).show();
                        }
                    }catch (IndexOutOfBoundsException e){

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

    }


    private void getStates() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    States states=new States();
                                    states.setId(jsonObject1.getString("id"));
                                    states.setName(jsonObject1.getString("name"));
                                    states.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    states.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    states.setCreated_at(jsonObject1.getString("created_at"));
                                    states.setUpdated_at(jsonObject1.getString("updated_at"));
                                    states.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    states.setStatus(jsonObject1.getString("status"));

                                    statelist.add(states);
                                }
                                ArrayList<String> statenames=new ArrayList<String>();
                                statenames.add("Select");
                                int stateid=0;
                                for (int k=0;k<statelist.size();k++){
                                    statenames.add(statelist.get(k).getName());
                                    System.out.println("aaaaaa state "+statelist.get(k).getId()+"  "+statelist.get(k).getName());
                                   /* if (isupdate==1){
                                        if (saveAddress.getState_id().equals(statelist.get(k).getId())){
                                            stateid=k;
                                        }
                                    }*/
                                }

                                ArrayAdapter ad = new ArrayAdapter(context,
                                        android.R.layout.simple_spinner_item, statenames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setAdapter(ad);
                                try{
                                    if (state_id != null) {
                                        int spinnerPosition = ad.getPosition(state_name);
                                        spinner_state.setSelection(spinnerPosition);
                                        getDistricts(state_id);
                                    }
                                }catch (NullPointerException e){

                                }

                            }

                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getDistricts(String stateID) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States+stateID,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("districts");
                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);
                                    Districts districts=new Districts();
                                    districts.setId(jsonObject.getString("id"));
                                    districts.setName(jsonObject.getString("name"));
                                    districts.setState_id(jsonObject.getString("state_id"));
                                    districtlist.add(districts);
                                }
                                ArrayList<String> districtnames=new ArrayList<String>();
                                districtnames.add("Select");
                                int distictid=0;
                                System.out.println("aaaaaaa districtlist size "+districtlist.size());
                                for (int k=0;k<districtlist.size();k++){
                                    districtnames.add(districtlist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            distictid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(context,
                                        android.R.layout.simple_spinner_item, districtnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_distict.setAdapter(ad);
                                try{
                                    if (district_id != null) {
                                        int spinnerPosition = ad.getPosition(distict_name);
                                        spinner_distict.setSelection(spinnerPosition);
                                        getConstituencis(state_id,district_id);
                                    }
                                }catch (NullPointerException e){

                                }
                            }

                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                
                Toast.makeText(context, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getConstituencis(String stateID,String districtid) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States+stateID+"/"+districtid,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        
                        System.out.println("aaaaaaa response const  "+ response);
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("constituenceis");

                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);

                                    Constiuencies constiuencies=new Constiuencies();
                                    constiuencies.setId(jsonObject.getString("id"));
                                    constiuencies.setDistrict_id(jsonObject.getString("district_id"));
                                    constiuencies.setName(jsonObject.getString("name"));

                                    constiencieslist.add(constiuencies);
                                }
                                ArrayList<String> constnames=new ArrayList<String>();
                                constnames.add("Select");
                                int conid=0;
                                for (int k=0;k<constiencieslist.size();k++){
                                    constnames.add(constiencieslist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            conid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(context,
                                        android.R.layout.simple_spinner_item, constnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_constitunesy.setAdapter(ad);
                                try{
                                    if (consti_name != null) {
                                        int spinnerPosition = ad.getPosition(consti_name);
                                        spinner_constitunesy.setSelection(spinnerPosition);
                                    }
                                }catch (NullPointerException e){

                                }

                            }

                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                
                Toast.makeText(context, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
