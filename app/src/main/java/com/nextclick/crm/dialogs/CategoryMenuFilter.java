package com.nextclick.crm.dialogs;

import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.interfaces.CategoryMenuSelection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CategoryMenuFilter {


    private CategoryMenuSelection categoryMenuSelection;
    private Context context;

    private String token = "";

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    Spinner filetr_menu_spinner,filter_category_spinner;

    private String menu_id = "";
    private String sub_cat_id = "";

    public void showMenuCategoryFilter(Context ctx, CategoryMenuSelection activity) {
        this.context = ctx;
        this.categoryMenuSelection = activity;
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.open_category_dialog);

        LinearLayout layout_root= dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels;
        layout_root.setLayoutParams(layoutParams);
        Button reset= dialog.findViewById(R.id.reset);
        filetr_menu_spinner = dialog.findViewById(R.id.filetr_menu_spinner);
        filter_category_spinner = dialog.findViewById(R.id.filter_category_spinner);
        TextView tv_error=dialog.findViewById(R.id.tv_error);

        getCategories();

        Button applyButton = dialog.findViewById(R.id.apply_p);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub_cat_id=menu_id=null;
                dialog.dismiss();
                categoryMenuSelection.onCategoryMenuSelection(sub_cat_id, menu_id);
            }
        });

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sub_cat_id.equalsIgnoreCase("select") || sub_cat_id.isEmpty()) {
                    // Toast.makeText(getContext(), "Please select Category", Toast.LENGTH_SHORT).show();
                    tv_error.setText("Please select "+context.getString(R.string.Shopbycategory));
                    tv_error.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tv_error.setVisibility(View.GONE);
                        }
                    },2000);
                } else if (menu_id.equalsIgnoreCase("") || menu_id.isEmpty()) {
                    tv_error.setText("Please select menu");
                    tv_error.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tv_error.setVisibility(View.GONE);
                        }
                    },2000);
                } else {
                    dialog.dismiss();
                    categoryMenuSelection.onCategoryMenuSelection(sub_cat_id, menu_id);
                }
            }
        });

        filter_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filter_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    menusList.clear();
                    menuIDList.clear();
                    filetr_menu_spinner.setAdapter(null);
                    getMenus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();


    }

    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filter_category_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaa catch cat "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error cat "+error.getMessage());
                        UIMsgs.showToast(context, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                System.out.println("aaaaaaaa  cat "+data);
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("menu_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filetr_menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("aaaaaa catch manu "+e.getMessage());
                                UIMsgs.showToast(context, "No menus available for the selected menu");
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error menu "+error.getMessage());
                        UIMsgs.showToast(context, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
