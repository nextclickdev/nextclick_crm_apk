package com.nextclick.crm.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextclick.crm.Common.Activities.ServicesActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;


public class AddNewBrand {
    private final Context context;
    private final Dialog dialog;
    private final EditText brand_name;
    private final EditText brand_desc;
    private final ImageView brand_image;
    private final ImageView tv_addbannerimage;


    public AddNewBrand(Context ctx, int whichactivity) {
        this.context = ctx;


        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        brand_name=dialog.findViewById(R.id.brand_name);
        brand_desc=dialog.findViewById(R.id.brand_desc);
        brand_image=dialog.findViewById(R.id.brand_image);
        tv_addbannerimage=dialog.findViewById(R.id.tv_addbannerimage);

        tv_addbannerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }
    public void showDialog() {
        dialog.show();
        dialog.setCancelable(false);
    }
    public void cancle(){
        dialog.dismiss();
    }

}
