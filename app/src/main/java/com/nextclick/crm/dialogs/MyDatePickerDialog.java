package com.nextclick.crm.dialogs;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;

import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.nextclick.crm.R;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.CompositeDateValidator;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MyDatePickerDialog {

    public void showDialog(FragmentManager fragmentManager, DatePickerResponse interfaceListener) {
        showDialog(fragmentManager,interfaceListener,interfaceListener.getActivityContext().getString(R.string.date_range));//select_date
    }

    public void showDialog(FragmentManager fragmentManager, DatePickerResponse interfaceListener,String title)
    {
        MaterialDatePicker.Builder<Pair<Long, Long>> materialDateBuilder = MaterialDatePicker.Builder.dateRangePicker();
        materialDateBuilder.setTitleText(title);
        materialDateBuilder.setCalendarConstraints(limitRange().build());
        materialDateBuilder.build();

        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();
        materialDatePicker.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        //holder.start_date.setText(materialDatePicker.getHeaderText());

                        Pair selectedDates = (Pair) materialDatePicker.getSelection();
                        final Pair<Date, Date> rangeDate = new Pair<>(new Date((Long) selectedDates.first), new Date((Long) selectedDates.second));
                        Date startDate = rangeDate.first;
                        Date endDate = rangeDate.second;
                        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

                       // holder.start_date.setText(simpleFormat.format(startDate)+"\n"+simpleFormat.format(endDate));

                        interfaceListener.onDateSelected(simpleFormat.format(startDate),simpleFormat.format(endDate));
                    }
                });

        materialDatePicker.show(fragmentManager, "date");
    }

    private CalendarConstraints.Builder limitRange() {

        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

       // Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();
        //calendarStart.add(Calendar.YEAR,-1);


       // long minDate = calendarStart.getTimeInMillis();
        long maxDate = calendarEnd.getTimeInMillis();


        //constraintsBuilderRange.setStart(minDate);
        constraintsBuilderRange.setEnd(maxDate);

        ArrayList<CalendarConstraints.DateValidator> validators= new ArrayList<>();
       // validators.add(DateValidatorPointForward.from(minDate));
        validators.add(DateValidatorPointBackward.before(maxDate));
        constraintsBuilderRange.setValidator(CompositeDateValidator.allOf(validators));

        return constraintsBuilderRange;
    }

    DatePickerResponse interfaceListener;
    Context mContext;
    String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";

    public void showStartAndEndDatePickers(Context mContext, DatePickerResponse interfaceListener)
    {
        this.mContext=mContext;
        this.interfaceListener=interfaceListener;
        datepicker(0,"");
    }

    public void datepicker(int i,final String start_date_str) {
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i == 0) {
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        } else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear), (Integer.parseInt(frommonth) - 1), Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext, /*R.style.DialogTheme,*/ new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i == 0) {
                    if ((monthOfYear + 1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear + 1);
                    } else {
                        frommonth = "" + (monthOfYear + 1);
                    }
                    if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear = "" + year;
                    datepicker(1, fromyear + "-" + frommonth + "-" + fromday);
                } else {
                    if ((monthOfYear + 1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear + 1);
                    } else {
                        tomonth = "" + (monthOfYear + 1);
                    }
                    if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear = "" + year;
                    String end_date_str = toyear + "-" + tomonth + "-" + today;
                    interfaceListener.onDateSelected(start_date_str, end_date_str);
                }
            }
        }, mYear, mMonth, mDay);
        if (i == 0) {
            Calendar c1 = Calendar.getInstance();
            c1.set((mYear), mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
        } else {
            Calendar c1 = Calendar.getInstance();
            Calendar c2 = Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);
            c2.set((mYear1), mMonth1, mDay1);
            c1.set(Integer.parseInt(fromyear), (Integer.parseInt(frommonth) - 1), Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());

        }
        datePickerDialog.setTitle(i == 0 ? "Select Start Date" : "Select End Date");
        //datePickerDialog.setCustomTitle();
        datePickerDialog.show();
    }
}
