package com.nextclick.crm.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nextclick.crm.Common.Activities.ServicesActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;


public class Logout_Dialog {
    private final Context context;
    private final Dialog dialog;
    private RelativeLayout main;
    private final TextView stay;
    private final TextView logout;
    private final TextView company_name;
    private final int whichactivity;


    public Logout_Dialog(Context ctx,int whichactivity) {
        this.context = ctx;
        this.whichactivity=whichactivity;

        dialog = new Dialog(context);
        //note_list=new ArrayList<>();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);

        logout=dialog.findViewById(R.id.logout);
        stay=dialog.findViewById(R.id.stay);
        company_name=dialog.findViewById(R.id.company_name);

       // company_name.setText("You might miss updates from "+companyname);
        stay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (whichactivity==0){
                    ((ServicesActivity)context).setLogout();
                }else {
                    ((ShopNowHomeActivity)context).setlogout();
                }
            }
        });
    }
    public void showDialog() {
        dialog.show();
        dialog.setCancelable(false);
    }
    public void cancle(){
        dialog.dismiss();
    }

}
