package com.nextclick.crm.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.Common.Activities.ServicesActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;


public class Language_Dialog {
    private final AppCompatActivity activity;
    private final Context context;
    private android.app.AlertDialog.Builder builder;


    public Language_Dialog(Context ctx, AppCompatActivity activity) {
        this.context = ctx;
        this.activity = activity;
    }

    public void showDialog() {
        final CharSequence[] items = {context.getString(R.string.english),context.getString(R.string.telugu)};
        builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(R.string.choose_language));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    setLocale("en");
                    //activity.recreate();
                    restartCurrentActivity();

                } else if (item == 1) {
                    setLocale("te");
                   // activity.recreate();
                    restartCurrentActivity();
                }
            }

            private void restartCurrentActivity() {
                Intent intent = activity.getIntent();
                activity.finish();
                activity.startActivity(intent);
            }
        });
        builder.show();
    }

    private void setLocale(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.setLocale(locale);

        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = context.getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", lang);
        editor.apply();
    }

    //load lang form shared prefrences
    public void LoadLanguage() {
        SharedPreferences pref = context.getSharedPreferences("Settings", MODE_PRIVATE);
        if (pref != null && pref.contains("My_Lang")) {
            String lang = pref.getString("My_Lang", "");
            setLocale(lang);
        }
    }
}