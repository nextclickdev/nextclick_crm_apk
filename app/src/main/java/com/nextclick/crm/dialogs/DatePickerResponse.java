package com.nextclick.crm.dialogs;

import android.content.Context;

import java.util.ResourceBundle;

public interface DatePickerResponse {
    void onDateSelected(String startDate,String endDate);
    Context getActivityContext();
}
