package com.nextclick.crm.ShopNowModule.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.PromotionViewFragment;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsListActivity;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.Utilities.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class PromotionCategoryAdapter  extends FragmentPagerAdapter {

    private final Boolean is_admin;
    PromotionsListActivity promotionsListActivity;
    List<PromotionObject> listOwnerPromotion;
    List<PromotionObject> listMyPromotions;

    PromotionViewFragment OwnerFragment,MyFragment;
    private PreferenceManager preferenceManager;

    public PromotionCategoryAdapter(
            @NonNull androidx.fragment.app.FragmentManager fm, PromotionsListActivity promotionsListActivity,String is_admin)
    {
        super(fm);
        this.is_admin= is_admin != null && is_admin.equalsIgnoreCase("true");
        this.promotionsListActivity=promotionsListActivity;
        listMyPromotions = new ArrayList<>();
        listOwnerPromotion = new ArrayList<>();
            //  if (preferenceManager.getString("is_admin").equalsIgnoreCase("true")){
    }



    public void setData(List<PromotionObject> listOwnerPromotion,List<PromotionObject> listMyPromotions) {
        this.listOwnerPromotion = listOwnerPromotion;
        this.listMyPromotions = listMyPromotions;
        if(OwnerFragment!=null)
        {
            OwnerFragment.refreshData(listOwnerPromotion);
        }
        if(MyFragment!=null)
        {
            MyFragment.refreshData(listMyPromotions);
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            OwnerFragment = new PromotionViewFragment(promotionsListActivity, false,listOwnerPromotion,is_admin);
            fragment = OwnerFragment;
        } else if (position == 1) {
            MyFragment = new PromotionViewFragment(promotionsListActivity, true,listMyPromotions,is_admin);
            fragment = MyFragment;
        }
        return fragment;
    }

    @Override
    public int getCount()
    {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if (position == 0)
            title = promotionsListActivity.getString(R.string.NextClickPromotions);
        else if (position == 1)
            title = promotionsListActivity.getString(R.string.my_promotions);
        return title;
    }
}
