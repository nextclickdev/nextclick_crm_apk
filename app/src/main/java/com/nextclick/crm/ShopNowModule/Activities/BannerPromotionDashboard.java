package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.BannerGalleryAdapter;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;
import com.nextclick.crm.Utilities.PreferenceManager;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class BannerPromotionDashboard extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    Boolean isMyPromotions=false;
    List<SliderItems> sliderItems;
    private TabLayout tabLayout;
    ViewPager viewPager;
    BannerGalleryAdapter viewPagerAdapter;
    private int currentposition;
    String constitueid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banner_promotion_dashboard);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        mContext=this;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(BannerPromotionDashboard.this);


        if(getIntent().hasExtra("constitueid")) {
            constitueid=getIntent().getStringExtra("constitueid");
        }

        init();

        if (com.nextclick.crm.Utilities.mixpanel.MyMixPanel.isMixPanelSupport) {
            com.nextclick.crm.Utilities.mixpanel.MyMixPanel.logEvent("Clicked on New Promotion");
        }
        getBanners();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void init() {

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new BannerGalleryAdapter(getSupportFragmentManager(), this,
                preferenceManager.getString("is_admin"));
        viewPager.setAdapter(viewPagerAdapter);
        currentposition=0;
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(currentposition);

        ImageView img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {
            finish();
        }
    }

    private void getBanners() {

        sliderItems = new ArrayList<>();

        mCustomDialog.show();
    String be= preferenceManager.getString(USER_TOKEN);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_BANNERS_TEMPLATES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_BANNERS_TEMPLATES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");
                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject transobj = dataobj.getJSONObject(i);
                                    SliderItems sliderItem = new SliderItems(transobj.getString("image"));
                                    sliderItem.setID(transobj.getString("id"));
                                    sliderItems.add(sliderItem);
                                }
                            }

                        } catch (JSONException e) {
                          //  Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            mCustomDialog.dismiss();
                            viewPagerAdapter.setData(sliderItems);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "onErrorResponse " + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void onBannerSelect(SliderItems sliderItems) {
        Intent intent=new Intent(this,AddPromotionActivity.class);
        intent.putExtra("constitueid", constitueid);
        intent.putExtra("SelectedImage", sliderItems.getImage());
        intent.putExtra("SelectedID", sliderItems.getID());
        startActivity(intent);
        finish();
    }

    public void onBannerCreate(String UploadedImage) {
        Intent intent=new Intent(this,AddPromotionActivity.class);
        intent.putExtra("constitueid", constitueid);
        preferenceManager.putString("UploadedImage", UploadedImage);
       // intent.putExtra("UploadedImage", UploadedImage);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            viewPagerAdapter.setActivityResult(requestCode,data);
        }
    }

}