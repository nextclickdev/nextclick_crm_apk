package com.nextclick.crm.ShopNowModule.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.util.Pair;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.ShopNowModule.Activities.SelectBannerImage;
import com.nextclick.crm.ShopNowModule.Models.BannerAvailability;
import com.nextclick.crm.ShopNowModule.Models.BannerPositionIndex;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PositionsAdapter extends RecyclerView.Adapter<PositionsAdapter.SliderViewHolder> {

    private final AddPromotionActivity parentActivity;
    private final List<BannerAvailability> bannerPositions;
    private int selectedSlotID;
    FragmentManager fm;
    /*private List<SliderItems> sliderItems;

    public PositionsAdapter(AddPromotionActivity addPromotionActivity) {
        this.parentActivity=addPromotionActivity;
        this.sliderItems = new ArrayList<>();

        SliderItems sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_1);
        sliderItem.setID("1");
        sliderItems.add(sliderItem);

        sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_2);
        sliderItem.setID("2");
        sliderItems.add(sliderItem);

        sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_3);
        sliderItem.setID("3");
        sliderItems.add(sliderItem);

        sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_4);
        sliderItem.setID("4");
        sliderItems.add(sliderItem);

        sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_5);
        sliderItem.setID("5");
        sliderItems.add(sliderItem);

        sliderItem=new SliderItems();
        sliderItem.setIcon(R.drawable.posi_`6);
        sliderItem.setID("6");
        sliderItems.add(sliderItem);
    }*/

    public PositionsAdapter(AddPromotionActivity addPromotionActivity, List<BannerAvailability> bannerPositions) {
        this.parentActivity=addPromotionActivity;
        this.bannerPositions =bannerPositions;
    }



    @NonNull
    @Override
    public PositionsAdapter.SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PositionsAdapter.SliderViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.layout_view_banner_slots, parent, false
                )
        );//position_layout_view
    }

    Integer selectedPosition =-1;
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull PositionsAdapter.SliderViewHolder holder, int position) {
        holder.setImage(bannerPositions.get(position));

        if(selectedPosition == position)
            holder.cardView.setBackgroundColor(Color.parseColor("#E49B83"));//DC746C
        else
            holder.cardView.setBackgroundColor(Color.parseColor("#efeae2"));//E49B83

        BannerAvailability selectedBanner = bannerPositions.get(position);
        holder.tv_slot.setText("Position - "+selectedBanner.getName());//selectedPosition

        holder.start_date.setText(bannerPositions.get(position).getstartDate()+"\n"+bannerPositions.get(position).getendDate());


        int filledPos=selectedBanner.getFilled_positions();
        if(filledPos>selectedBanner.getBanners_limit())
            filledPos=selectedBanner.getBanners_limit();

        int slotsRemaining=selectedBanner.getBanners_limit()-filledPos;
        holder.tv_slots_remaining.setText(slotsRemaining+" Slots left");
        if(slotsRemaining<=0)
        {
            holder.tv_slots_remaining.setText("0 Slots left");
            holder.tv_next_availability.setText("Next available slot: "+selectedBanner.getNext_available_date());
            holder.tv_next_availability.setVisibility(View.VISIBLE);
        }

        holder.radio_group.clearCheck();

        holder.radio_group.setOnCheckedChangeListener (null);
       // if(filledPos>0) {
            Integer slotID=bannerPositions.get(position).getSelectedSlotID();
            for (int i = 0; i < 6 ; i++) {//filledPos
                View view = holder.radio_group.getChildAt(i);
                view.setEnabled(filledPos <= i);
                if (slotID == i)
                    ((RadioButton) view).setChecked(true);
            }
        //}
        holder.tv_price.setText("Price : "+selectedBanner.getPrice()+ " Rs / Day");

        selectedSlotID=-1;


        holder.radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    if(checkedId==-1)
                        return;
                    View radioButton = group.findViewById(checkedId);
                    if(((RadioButton)radioButton).isChecked() && selectedPosition!= position && selectedSlotID!=group.indexOfChild(radioButton)) {
                        selectedSlotID = group.indexOfChild(radioButton);
                        if (selectedPosition != -1) {
                            //previous selection
                            bannerPositions.get(selectedPosition).setSelectedSlotID(-1);
                        }
                        selectedPosition = position;
                        bannerPositions.get(position).setSelectedSlotID(selectedSlotID);
                        parentActivity.onPositionSelected(bannerPositions.get(position));
                        notifyDataSetChanged();
                    }
                }
                catch (Exception ex)
                {
                    Log.e("Radiobutton selection",ex.getLocalizedMessage());
                }
            }
        });


        /*applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(selectedSlotID >=0) {
                    makePayment(selectedSlotID,selectedBanner.getPrice());
                }
                else
                    Toast.makeText(mContext, "Please select the slot position.", Toast.LENGTH_SHORT).show();
            }
        });


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition=position;
                parentActivity.onPositionSelected(bannerPositions.get(position));
                notifyDataSetChanged();
            }
        });*/

        //MaterialDatePicker.Builder materialDateBuilder = MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker.Builder<Pair<Long, Long>> materialDateBuilder = MaterialDatePicker.Builder.dateRangePicker();
        materialDateBuilder.setTitleText(parentActivity.getString(R.string.select_date));
        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();
        Calendar c1=Calendar.getInstance();
        materialDateBuilder.setCalendarConstraints(limitRange().build());


        materialDatePicker.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        holder.start_date.setText(materialDatePicker.getHeaderText());

                        Pair selectedDates = (Pair) materialDatePicker.getSelection();
                        final Pair<Date, Date> rangeDate = new Pair<>(new Date((Long) selectedDates.first), new Date((Long) selectedDates.second));
                        Date startDate = rangeDate.first;
                        Date endDate = rangeDate.second;
                        SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

                        holder.start_date.setText(simpleFormat.format(startDate)+"\n"+simpleFormat.format(endDate));

                        parentActivity.changeDate(simpleFormat.format(startDate),simpleFormat.format(endDate));
                    }
                });

        holder.start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(parentActivity.getSupportFragmentManager(), "date");
            }
        });
    }
    private CalendarConstraints.Builder limitRange() {

        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        Calendar calendarStart = Calendar.getInstance();
        //Calendar calendarEnd = Calendar.getInstance();


        long minDate = calendarStart.getTimeInMillis();
      //  long maxDate = calendarEnd.getTimeInMillis();


        constraintsBuilderRange.setStart(minDate);
       // constraintsBuilderRange.setEnd(maxDate);
        return constraintsBuilderRange;
    }

    @Override
    public int getItemCount() {
        return bannerPositions.size();
    }


    public void setSelectedIndex(int position) {
        selectedPosition=position;
        if(bannerPositions!=null && bannerPositions.size()>position)
        {
            parentActivity.onPositionSelected(bannerPositions.get(position));
            notifyDataSetChanged();
        }
    }

    class SliderViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout layout_panel;
        ImageView imageView;
        RadioGroup radio_group;
        TextView tv_slot,tv_slots_remaining,tv_next_availability,tv_price,start_date;
        LinearLayout cardView;

        SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            layout_panel = itemView.findViewById(R.id.layout_panel);

            cardView= itemView.findViewById(R.id.cardView);
            start_date = itemView.findViewById(R.id.start_date);
            radio_group = itemView.findViewById(R.id.radio_group);
            tv_slot = itemView.findViewById(R.id.tv_slot);
            tv_slots_remaining = itemView.findViewById(R.id.tv_slots_remaining);
            tv_next_availability = itemView.findViewById(R.id.tv_next_availability);
            tv_price = itemView.findViewById(R.id.tv_price);
        }

        void setImage(BannerAvailability sliderItems) {
            Picasso.get()
                    .load(sliderItems.getIcon())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(imageView);
        }
    }
}