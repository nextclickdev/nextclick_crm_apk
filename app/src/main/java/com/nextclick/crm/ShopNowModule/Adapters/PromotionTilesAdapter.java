package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsActivity;

public class PromotionTilesAdapter extends RecyclerView.Adapter<PromotionTilesAdapter.ViewHolder> {

    Context context;
    String[] data;
    private final View.OnClickListener mOnClickListener;

    public PromotionTilesAdapter(PromotionsActivity promotionsActivity, PromotionsActivity.MyOnClickListener myOnClickListener) {
        this.context = promotionsActivity;
        data = new String[]{context.getResources().getString(R.string.Promotions), context.getResources().getString(R.string.promotions_history)/*, context.getResources().getString(R.string.PROFILE)*/};
        this.mOnClickListener=myOnClickListener;
    }

    @NonNull
    @Override
    public PromotionTilesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.promotion_list_tile, parent, false);
        itemView.setOnClickListener(mOnClickListener);
        return new PromotionTilesAdapter.ViewHolder(itemView);
    }

    TextView tv_header;
    ImageView img_icon;
    LinearLayout layout_panel;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull PromotionTilesAdapter.ViewHolder holder, final int position) {
        final String promotionObject = data[position];

        if(position%2 == 0)
            holder.layout_panel.setBackgroundColor(Color.parseColor("#5E97F6"));
        else
            holder.layout_panel.setBackgroundColor(Color.parseColor("#0B9D58"));
        if(position==2)
        {
            holder.layout_panel.setBackgroundColor(Color.parseColor("#783BE4"));
            holder.img_icon.setImageDrawable(context.getDrawable(R.drawable.ic_promotion_profile_tile));
        }
        holder.tv_header.setText(promotionObject);

       /* holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                String promotionObjectString = gson.toJson(promotionObject);
                Intent intent = new Intent(context, PromotionDetailsActivity.class);
                intent.putExtra("promotionObject",promotionObjectString);
                context.startActivity(intent);
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return data.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_header;
        ImageView img_icon;
        LinearLayout layout_panel;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_icon= itemView.findViewById(R.id.img_icon);
            tv_header = itemView.findViewById(R.id.tv_header);
            layout_panel = itemView.findViewById(R.id.layout_panel);
        }
    }

}


