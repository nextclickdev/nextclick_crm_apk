package com.nextclick.crm.ShopNowModule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.nextclick.crm.Common.Activities.ProfileActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.PromotionTilesAdapter;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;


public class PromotionsActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recycler_promotions_tabs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions);
       // getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        ImageView img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        recycler_promotions_tabs = findViewById(R.id.recycler_promotions_tabs);
        PromotionTilesAdapter promotionTilesAdapter = new PromotionTilesAdapter(PromotionsActivity.this,new MyOnClickListener());
        recycler_promotions_tabs.setAdapter(promotionTilesAdapter);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,
                LinearLayoutManager.VERTICAL);
        recycler_promotions_tabs.setLayoutManager(staggeredGridLayoutManager);

        //if (preferenceManager.getBoolean(MIXPANEL_INTEGRATED)) {
            MyMixPanel.logEvent("Clicked on Promotions service");
        //}
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {
            finish();
        }
    }


    public class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            int itemPosition = recycler_promotions_tabs.getChildLayoutPosition(v);
            if (itemPosition == 0) {
                Intent intent1 = new Intent(getApplicationContext(), PromotionsListActivity.class);
                intent1.putExtra("isMyPromotions", itemPosition != 0);
                startActivity(intent1);
            }
            if (itemPosition == 1) {
                Intent intent1 = new Intent(getApplicationContext(), MyPromotionsActivity.class);
                startActivity(intent1);
            }
            if (itemPosition == 2) {
                Intent intent1 = new Intent(getApplicationContext(), ProfileActivity.class);
               // intent1.putExtra("id", "5");
                startActivity(intent1);
            }
        }
    }
}