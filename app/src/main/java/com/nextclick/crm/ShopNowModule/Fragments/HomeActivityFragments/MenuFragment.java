package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.CreateAndUpdateMenuActivity;
import com.nextclick.crm.ShopNowModule.Adapters.MenusAdapter;
import com.nextclick.crm.ShopNowModule.Models.MenuModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MENU_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class MenuFragment extends Fragment implements View.OnClickListener {


    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private MenusAdapter menusAdapter;
    private RecyclerView menu_recycler;
    private FloatingActionButton add_menu_fab;
    private PreferenceManager preferenceManager;

    private ArrayList<MenuModel> menuModelsList;

    private String token = "";
    private final String searchString = "";

    public static int count = 0;

    public MenuFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);

        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        add_menu_fab = view.findViewById(R.id.add_menu_fab);
        menu_recycler = view.findViewById(R.id.menu_recycler);
    }

    private void initializeListeners() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String enteredText = charSequence.toString();
                try {
                    menusAdapter.getFilter().filter(enteredText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //getMenus(enteredText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        add_menu_fab.setOnClickListener(this);
    }

    private void prepareDetails() {
        getMenus(searchString);
    }

    private void getMenus(String s) {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", s);
        final String data = new JSONObject(searchMap).toString();
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MENU_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            listIsFull();
                                            menuModelsList = new ArrayList<>();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject menuObject = dataArray.getJSONObject(i);
                                                MenuModel menuModel = new MenuModel();
                                                menuModel.setId(menuObject.getString("id"));
                                                menuModel.setName(menuObject.getString("name"));
                                                menuModel.setDesc(menuObject.getString("desc"));
                                                try {
                                                    menuModel.setCat_id(menuObject.getJSONObject("subcat").getString("id"));
                                                    menuModel.setSub_cat_name(menuObject.getJSONObject("subcat").getString("name"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                try {
                                                    menuModel.setImage(menuObject.getString("image"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                menuModelsList.add(menuModel);
                                            }

                                            menusAdapter = new MenusAdapter(mContext, menuModelsList);
                                            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                                                @Override
                                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                                        @Override
                                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                            return SPEED / displayMetrics.densityDpi;
                                                        }
                                                    };
                                                    smoothScroller.setTargetPosition(position);
                                                    startSmoothScroll(smoothScroller);
                                                }
                                            };
                                            menu_recycler.setLayoutManager(layoutManager);
                                            menu_recycler.setAdapter(menusAdapter);
                                        }
                                    } catch (Exception e) {
                                        UIMsgs.showToast(mContext, "Menu not available");
                                        listIsEmpty();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, "Menu not available");
                                listIsEmpty();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            listIsEmpty();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        menu_recycler.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        menu_recycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_menu_fab) {
            mContext.startActivity(new Intent(mContext, CreateAndUpdateMenuActivity.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (count == 1) {
            getMenus("");
            count = 0;
        }
    }
}