package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.OrderItems;
import com.nextclick.crm.Utilities.PreferenceManager;

import java.util.List;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder>  {

    List<OrderItems> data;

    LayoutInflater inflter;
    Context context;
    private String token;




    public OrderItemsAdapter(Context activity, List<OrderItems> itemPojos) {
        this.context = activity;
        this.data = itemPojos;


    }


    @NonNull
    @Override
    public OrderItemsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_individual_order_items, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        token ="Bearer " + new PreferenceManager(context).getString(USER_TOKEN);
        return new OrderItemsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemsAdapter.ViewHolder holder, final int position) {

        OrderItems orderItems = data.get(position);
        holder.item_sno.setText(orderItems.getSno()+"");
        holder.item_name.setText(orderItems.getItemName());
        holder.item_value.setText(orderItems.getPrice()+"");
        holder.item_qty.setText(orderItems.getQuantity()+"");


    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}



    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView item_sno,item_name,item_qty,item_value;
        CardView order_status_card,view_details;
        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_sno = itemView.findViewById(R.id.item_sno);
            item_name = itemView.findViewById(R.id.item_name);
            item_qty = itemView.findViewById(R.id.item_qty);
            item_value = itemView.findViewById(R.id.item_value);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }


}
