package com.nextclick.crm.ShopNowModule.Models;

import com.nextclick.crm.productDetails.model.VendorVarients;

import java.util.ArrayList;

public class ProductModel {

    String id,myinventory;
    String sub_cat_id;
    String menu_id;
    String product_code;
    String name;
    String desc;
    int quantity;
    int price;
    int discount;
    String item_type;
    String menu_name;
    String product_image;
    int status;
    String categoryName;
    String availabilityStatus;
    String product_variant_id;
    ArrayList<ImagesModel> imagelist;
    boolean setpromocode,checkproductselect;
    ArrayList<ProductAdd> selectedvarientlist;
    ArrayList<VendorVarients> vendorVarientslist;

    public boolean isCheckproductselect() {
        return checkproductselect;
    }

    public void setCheckproductselect(boolean checkproductselect) {
        this.checkproductselect = checkproductselect;
    }

    public String getMyinventory() {
        return myinventory;
    }

    public void setMyinventory(String myinventory) {
        this.myinventory = myinventory;
    }

    public ArrayList<VendorVarients> getVendorVarientslist() {
        return vendorVarientslist;
    }

    public void setVendorVarientslist(ArrayList<VendorVarients> vendorVarientslist) {
        this.vendorVarientslist = vendorVarientslist;
    }

    public ArrayList<ProductAdd> getSelectedvarientlist() {
        return selectedvarientlist;
    }

    public void setSelectedvarientlist(ArrayList<ProductAdd> selectedvarientlist) {
        this.selectedvarientlist = selectedvarientlist;
    }

    public boolean isSetpromocode() {
        return setpromocode;
    }

    public void setSetpromocode(boolean setpromocode) {
        this.setpromocode = setpromocode;
    }

    public ArrayList<ImagesModel> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ImagesModel> imagelist) {
        this.imagelist = imagelist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAvailabilityStatus() {
        return availabilityStatus;
    }

    public void setAvailabilityStatus(String availabilityStatus) {
        this.availabilityStatus = availabilityStatus;
    }

    public String getProduct_variant_id() {
        return product_variant_id;
    }

    public void setProduct_variant_id(String product_variant_id) {
        this.product_variant_id = product_variant_id;
    }
}