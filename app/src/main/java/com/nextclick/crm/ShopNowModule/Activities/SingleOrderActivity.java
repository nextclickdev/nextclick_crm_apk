package com.nextclick.crm.ShopNowModule.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.OrderItemsAdapter;
import com.nextclick.crm.ShopNowModule.Models.OrderItems;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ACCEPT_ORDER;
import static com.nextclick.crm.Config.Config.OUT_FOR_DELVERY;
import static com.nextclick.crm.Config.Config.REJECT_ORDER;
import static com.nextclick.crm.Config.Config.SINGLE_ORDER;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class SingleOrderActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private String token;
    private RecyclerView items_recycler;
    private TextView order_no,ordered_from,grand_total,timing,otp,payment_type,status;
    private ImageView refresh_order_,order_back_image_;
    private String orderId;
    private LinearLayout otp_layout,status_layout;
    private ArrayList<OrderItems> orderItemsArrayList;
    private CardView accept_order_card,reject_order_card,out_card,status_card;
    int orderStatus;
    String sts_str;
    String otp_str;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_order);
        getSupportActionBar().hide();
        init();
        getOrder();
        accept_order_card.setOnClickListener(this);
        reject_order_card.setOnClickListener(this);
        out_card.setOnClickListener(this);
        order_back_image_.setOnClickListener(this);
        refresh_order_.setOnClickListener(this);

    }

    private void getOrder() {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SINGLE_ORDER + orderId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    LoadingDialog.dialog.dismiss();
                    try{
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        timing.setText(dataObject.getString("created_at"));
                        grand_total.setText(dataObject.getString("total"));
                        order_no.setText(dataObject.getString("order_track"));
                        orderStatus = dataObject.getInt("order_status");
                        if(orderStatus==0){
                            sts_str= "Order Rejected";
                            status_card.setCardBackgroundColor(Color.RED);
                            status_layout.setVisibility(View.GONE);
                        }else if(orderStatus==1){
                            sts_str= "Order Received";
                            status_card.setCardBackgroundColor(Color.parseColor("#F26B35"));
                        }else if(orderStatus==2){
                            sts_str= "Order Accepted";
                            accept_order_card.setVisibility(View.GONE);
                            reject_order_card.setVisibility(View.GONE);
                            out_card.setVisibility(View.VISIBLE);
                            status_card.setCardBackgroundColor(Color.GREEN);
                        }else if(orderStatus==3){
                            sts_str= "Order Preparing";
                            status_card.setCardBackgroundColor(Color.GRAY);
                            accept_order_card.setVisibility(View.GONE);
                            reject_order_card.setVisibility(View.GONE);
                            out_card.setVisibility(View.VISIBLE);
                        }else if(orderStatus==4){
                            sts_str= "Out For Delivery";
                            status_card.setCardBackgroundColor(Color.BLUE);
                        }else if(orderStatus==5){
                            sts_str= "On the way";
                            status_card.setCardBackgroundColor(Color.parseColor("#B8860B"));
                            status_layout.setVisibility(View.GONE);
                        }else if(orderStatus==6){
                            sts_str= "Order Completed";
                            status_card.setCardBackgroundColor(Color.parseColor("#228B22"));
                            status_layout.setVisibility(View.GONE);
                        }else if(orderStatus==7){
                            sts_str= "Order Cancelled";
                            status_card.setCardBackgroundColor(Color.RED);
                            status_layout.setVisibility(View.GONE);
                        }
                        status.setText(sts_str);
                        payment_type.setText(dataObject.getJSONObject("payment_method").getString("name"));
                        ordered_from.setText(dataObject.getJSONObject("user").getString("first_name"));
                        try{
                            if(!dataObject.getString("otp").equalsIgnoreCase("null") && !dataObject.getString("otp").equalsIgnoreCase(""))
                            otp.setText(dataObject.getString("otp"));
                            otp_str = dataObject.getString("otp");
                            //otp_layout.setVisibility(View.VISIBLE);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try{
                            orderItemsArrayList = new ArrayList<>();
                            JSONArray orderItemsArray = dataObject.getJSONArray("order_items");
                            if(orderItemsArray.length()>0){
                                for(int i=0; i<orderItemsArray.length(); i++){
                                    JSONObject orderItemObject = orderItemsArray.getJSONObject(i);
                                    OrderItems orderItems = new OrderItems();
                                    orderItems.setItemName(orderItemObject.getJSONObject("item").getString("name"));
                                    orderItems.setPrice(orderItemObject.getInt("price"));
                                    orderItems.setQuantity(orderItemObject.getInt("quantity"));
                                    orderItems.setSno(i+1);
                                    orderItemsArrayList.add(orderItems);
                                }
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        try{
                            JSONArray suborderItemsArray = dataObject.getJSONArray("sub_order_items");
                            if(suborderItemsArray.length()>0){
                                for(int i=0; i<suborderItemsArray.length(); i++){
                                    JSONObject orderItemObject = suborderItemsArray.getJSONObject(i);
                                    OrderItems orderItems = new OrderItems();
                                    orderItems.setItemName(orderItemObject.getJSONObject("section_item").getString("name"));
                                    orderItems.setPrice(orderItemObject.getInt("price"));
                                    orderItems.setQuantity(orderItemObject.getInt("quantity"));
                                    orderItems.setSno(orderItemsArrayList.size()+1);
                                    orderItemsArrayList.add(orderItems);
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if(orderItemsArrayList.size()>0) {
                            OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(mContext, orderItemsArrayList);

                            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                @Override
                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                        @Override
                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                            return SPEED / displayMetrics.densityDpi;
                                        }

                                    };
                                    smoothScroller.setTargetPosition(position);
                                    startSmoothScroll(smoothScroller);
                                }

                            };
                            items_recycler.setLayoutManager(layoutManager);
                            items_recycler.setAdapter(orderItemsAdapter);

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void init() {
        mContext = SingleOrderActivity.this;
        orderId = getIntent().getStringExtra("order_id");
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        items_recycler = findViewById(R.id.items_recycler);
        refresh_order_ = findViewById(R.id.refresh_order_);
        order_back_image_ = findViewById(R.id.order_back_image_);
        order_no = findViewById(R.id.order_no);
        ordered_from = findViewById(R.id.ordered_from);
        grand_total = findViewById(R.id.grand_total);
        timing = findViewById(R.id.timing);
        otp = findViewById(R.id.otp);
        otp_layout = findViewById(R.id.otp_layout);
        status_layout = findViewById(R.id.status_layout);
        payment_type = findViewById(R.id.payment_type);
        accept_order_card = findViewById(R.id.accept_order_card);
        reject_order_card = findViewById(R.id.reject_order_card);
        out_card = findViewById(R.id.out_card);
        status_card = findViewById(R.id.status_card);
        status = findViewById(R.id.status);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.accept_order_card:
                acceptOrder();
                break;

            case R.id.reject_order_card:

                LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_reject_order, null);

                final EditText reason = alertLayout.findViewById(R.id.reason);
                final Button submit = alertLayout.findViewById(R.id.submit);

                AlertDialog.Builder alert = new AlertDialog.Builder(SingleOrderActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Reason");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String res = reason.getText().toString().trim();
                        if(res.length()==0){
                            reason.setError("Should Not Be Empty");
                            reason.requestFocus();
                        }
                        else if(res.length()<10){
                            reason.setError("Minimum 10 characters");
                            reason.requestFocus();
                        }
                        else{

                            rejectOrder(res,dialog);
                        }

                    }
                });


                break;

            case R.id.out_card:

                LayoutInflater inflater1 = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout1 = inflater1.inflate(R.layout.supporter_otp_input, null);

                final EditText otp = alertLayout1.findViewById(R.id.order_otp);
                final Button submit1 = alertLayout1.findViewById(R.id.submit);

                AlertDialog.Builder alert1 = new AlertDialog.Builder(SingleOrderActivity.this);
                alert1.setIcon(R.mipmap.ic_launcher);
                alert1.setTitle("ENTER OTP");
                // this is set the view from XML inside AlertDialog
                alert1.setView(alertLayout1);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert1.setCancelable(true);
                final AlertDialog dialog1 = alert1.create();
                dialog1.show();
                submit1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String otp_str = otp.getText().toString().trim();
                        if(otp_str.length()==0){
                            otp.setError("Should Not Be Empty");
                            otp.requestFocus();
                        }
                        else if(otp_str.length()<4){
                            otp.setError("Minimum 4 characters");
                            otp.requestFocus();
                        }
                        else{

                            outDelivery(otp_str,dialog1);
                        }

                    }
                });



                break;

            case R.id.refresh_order_:
                getOrder();
                break;

            case R.id.order_back_image_:
                onBackPressed();
                break;
        }

    }

    private void outDelivery(String otp_str, final AlertDialog dialog1) {

        dialog1.dismiss();
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, OUT_FOR_DELVERY + orderId+"&otp="+ otp_str, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    Log.d("out_resp",response);
                    LoadingDialog.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if(status){
                            UIMsgs.showToast(mContext,"Status changed");
                            recreate();
                        }
                        else{
                            UIMsgs.showToast(mContext,jsonObject.getString("data"));
                            dialog1.show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,"Unable to change status. Please try later");
                    }

                }else{
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext,MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext,OOPS);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void rejectOrder(String res, AlertDialog dialog) {
        dialog.dismiss();
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, REJECT_ORDER + orderId +"&reason="+res, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    Log.d("rej_resp",response);
                    LoadingDialog.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if(status){
                            UIMsgs.showToast(mContext,"Order Rejected");
                            recreate();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,"Unable to reject. Please try later");
                    }

                }else{
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext,MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext,OOPS);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void acceptOrder() {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ACCEPT_ORDER + orderId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    Log.d("ac_resp",response);
                    LoadingDialog.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if(status){
                            UIMsgs.showToast(mContext,"Order Accepted");
                            recreate();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,"Unable to accept. Please try later");
                    }

                }else{
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext,MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext,OOPS);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}