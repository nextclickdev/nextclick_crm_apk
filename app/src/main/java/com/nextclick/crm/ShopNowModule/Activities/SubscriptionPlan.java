package com.nextclick.crm.ShopNowModule.Activities;

import com.nextclick.crm.promocodes.model.SubscriptionSetting;

import java.util.ArrayList;

public class SubscriptionPlan {
    private String title;
    private String isActive;
    private ArrayList<SubscriptionSetting> subscriptionSetting;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }


    public ArrayList<SubscriptionSetting> getSubscriptionSetting() {
        return subscriptionSetting;
    }

    public void setSubscriptionSetting(ArrayList<SubscriptionSetting> subscriptionSetting) {
        this.subscriptionSetting = subscriptionSetting;
    }

}
