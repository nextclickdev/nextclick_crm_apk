package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.BannerPromotionDashboard;
import com.nextclick.crm.ShopNowModule.Fragments.TemplatePickerFragment;
import com.nextclick.crm.ShopNowModule.Fragments.UploadTemplateBanner;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;

import java.util.ArrayList;
import java.util.List;

public class BannerGalleryAdapter  extends FragmentPagerAdapter {

    private final boolean is_admin;
    boolean restrictOnlyOnePromotionForAdmin =true;
    BannerPromotionDashboard bannerPromotionDashboard;

    TemplatePickerFragment templatePickerFragment;
    private List<SliderItems> sliderItems;
    private UploadTemplateBanner uploadTemplateBanner;

    public BannerGalleryAdapter(
            @NonNull androidx.fragment.app.FragmentManager fm, BannerPromotionDashboard bannerPromotionDashboard, String isAdmin)
    {
        super(fm);
        this.is_admin= isAdmin != null && isAdmin.equalsIgnoreCase("true");
        this.bannerPromotionDashboard =bannerPromotionDashboard;
        sliderItems = new ArrayList<>();
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        /*if (position == 0) {
            templatePickerFragment = new TemplatePickerFragment(bannerPromotionDashboard);
            fragment = templatePickerFragment;
        } else if (position == 1) {
            uploadTemplateBanner=  new UploadTemplateBanner(bannerPromotionDashboard);
            fragment = uploadTemplateBanner;
        }*/

        if (is_admin && !restrictOnlyOnePromotionForAdmin) {
            if (position == 0) {
                templatePickerFragment = new TemplatePickerFragment(bannerPromotionDashboard);
                fragment = templatePickerFragment;
            } else if (position == 1) {
                uploadTemplateBanner = new UploadTemplateBanner(bannerPromotionDashboard);
                fragment = uploadTemplateBanner;
            }
        } else {
            if (!is_admin) {
                templatePickerFragment = new TemplatePickerFragment(bannerPromotionDashboard);
                fragment = templatePickerFragment;
            } else {
                uploadTemplateBanner = new UploadTemplateBanner(bannerPromotionDashboard);
                fragment = uploadTemplateBanner;
            }
        }
        return fragment;
    }

    @Override
    public int getCount()
    {

        return is_admin && !restrictOnlyOnePromotionForAdmin ?2:1;
        //return 2;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if(is_admin && !restrictOnlyOnePromotionForAdmin)
        {
            if (position==0)
                title = bannerPromotionDashboard.getString(R.string.ProfilePromotions);
            else
                title = bannerPromotionDashboard.getString(R.string.OfferPromotions);
        }
        else {
            if (!is_admin)
                title = bannerPromotionDashboard.getString(R.string.ProfilePromotions);
            else
                title = bannerPromotionDashboard.getString(R.string.OfferPromotions);
        }
        return title;
    }

    public void setData(List<SliderItems> sliderItems) {
        this.sliderItems = sliderItems;
        if(templatePickerFragment!=null)
        {
            templatePickerFragment.refreshData(sliderItems);
        }
    }

    public void setActivityResult(int requestCode, Intent data) {
        if(uploadTemplateBanner!=null) {
            uploadTemplateBanner.setActivityResult(requestCode,data);
        }
    }
}
