package com.nextclick.crm.ShopNowModule.Models;

import java.util.ArrayList;

public class CategoryObject {

    String categoryId,categoryName, categoryImage;

    public void setCatID(String id) {
        this.categoryId=id;
    }
    public void setCatName(String name) {
        this.categoryName=name;
    }
    public void setCatImage(String img) {
        this.categoryImage=img;
    }

    public String getCatID() {
        return categoryId;
    }
    public String getCatName() {
        return categoryName;
    }
    public String getCatImage() {
        return categoryImage;
    }
}
