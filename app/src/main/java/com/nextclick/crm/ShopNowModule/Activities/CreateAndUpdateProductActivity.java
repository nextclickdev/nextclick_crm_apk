package com.nextclick.crm.ShopNowModule.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.MyProductsFragment;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.PRODUCT_C;
import static com.nextclick.crm.Config.Config.PRODUCT_R;
import static com.nextclick.crm.Config.Config.PRODUCT_U;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateProductActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private Button submit;
    private Switch switch_;
    private Context mContext;
    private SwitchCompat switchCompact;
    private TextView product_status_type;
    private PreferenceManager preferenceManager;
    private ImageView back_image, product_image;
    private Spinner category_spinner, menu_spinner;
    private RadioButton veg_radio_btn, non_veg_radio_btn;
    private EditText product_name, product_desc, product_price, product_quantity, product_discount;

    private int status_type = 1;//1- create, 2-update
    private int item_type = 0;//1- create, 2-update
    private final int veg = 1;
    private final int nonveg = 2;
    private String product_name_str, product_desc_str, product_image_str,
            token, sub_cat_id = null, menu_id = null, product_price_str, product_quantity_str, product_discount_str;

    public int flag = 0;
    private String userChoosenTask;
    private static final int CAMERA_REQUEST = 8;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 15;
    private final int SELECT_MULTIPLE_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();

    private int productAvailability = 1;
    private boolean isImageSelected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_create_and_update_product);
        getSupportActionBar().hide();
        initializeUi();
        initializeListeners();
        prepareDetails();
        prepareSpinnerDetails();
    }

    private void initializeUi() {
        mContext = CreateAndUpdateProductActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);

        submit = findViewById(R.id.submit);
        back_image = findViewById(R.id.back_image);
        product_image = findViewById(R.id.banner_image);
        product_name = findViewById(R.id.product_name);
        product_desc = findViewById(R.id.product_desc);
        menu_spinner = findViewById(R.id.menu_spinner);
        veg_radio_btn = findViewById(R.id.veg_radio_btn);
        switchCompact = findViewById(R.id.switchCompact);
        product_price = findViewById(R.id.product_price);
        category_spinner = findViewById(R.id.category_spinner);
        product_quantity = findViewById(R.id.product_quantity);
        product_discount = findViewById(R.id.product_discount);
        non_veg_radio_btn = findViewById(R.id.non_veg_radio_btn);
        product_status_type = findViewById(R.id.product_status_type);

        try {
            Thread thread = new Thread();
            getCategories();
            /*thread.sleep(1000);
            getMenus();*/
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeListeners() {
        submit.setOnClickListener(this);
        back_image.setOnClickListener(this);
        product_image.setOnClickListener(this);
        switchCompact.setOnCheckedChangeListener(this);
    }

    private void prepareDetails() {
        try {
            String statusText = getIntent().getStringExtra("type");
            if (statusText != null) {
                if (statusText.equalsIgnoreCase("u")) {
                    product_status_type.setText("Update product");
                    submit.setText("UPDATE");
                    status_type = 2;
                    LoadingDialog.loadDialog(mContext);
                    getProduct(getIntent().getStringExtra("product_id"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareSpinnerDetails() {

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    LoadingDialog.loadDialog(mContext);
                    getMenus(sub_cat_id);
                    LoadingDialog.dialog.dismiss();
                } else {
                    sub_cat_id = null;
                    menu_id = null;
                    menuIDList.clear();
                    menusList.clear();
                    menu_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                } else {
                    menu_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getProduct(String product_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PRODUCT_R + product_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            int productStatus = dataObject.getInt("status");
                            switchCompact.setChecked(productStatus == 1);
                            product_name.setText(dataObject.getString("name"));
                            product_desc.setText(dataObject.getString("desc"));
                            Picasso.get()
                                    .load(dataObject.getString("product_image"))
                                    .placeholder(R.drawable.loader_gif)
                                    .into(product_image);
                            product_quantity.setText(dataObject.getString("quantity"));
                            product_desc.setText(Html.fromHtml(dataObject.getString("desc")).toString());
                            product_price.setText(dataObject.getString("price"));
                            product_discount.setText(dataObject.getString("discount"));
                            int item_type = dataObject.getInt("item_type");
                            if (item_type == 1) {
                                veg_radio_btn.setChecked(true);
                            }
                            if (item_type == 2) {
                                non_veg_radio_btn.setChecked(true);
                            }
                            try {
                                menu_id = dataObject.getJSONObject("menu").getString("id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                Thread thread = new Thread();
                                Thread.sleep(1000);
                                for (int i = 0; i < categoryIDList.size(); i++) {
                                    if (dataObject.getJSONObject("sub_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                                        sub_cat_id = dataObject.getJSONObject("sub_category").getString("id");
                                        category_spinner.setSelection((i + 1), true);
                                        getMenus(sub_cat_id);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            UIMsgs.showToast(mContext, OOPS);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        UIMsgs.showToast(mContext, OOPS);
                    }

                } else {
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }
                LoadingDialog.dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getMenus(String sub_cat_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("menu_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        menu_spinner.setAdapter(adapter);

                                        try {
                                            if (menu_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    Thread.sleep(1000);
                                                    for (int i = 0; i < menuIDList.size(); i++) {
                                                        if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                            menu_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getCategories() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        category_spinner.setAdapter(adapter);


                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_image:
                onBackPressed();
                break;
            case R.id.banner_image:
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;
            case R.id.submit:
                if (isValid()) {
                    String url = "";
                    String data = "";
                    if (status_type == 1) {
                        if (isImageSelected) {
                            Map<String, String> uploadMap = new HashMap<>();
                            uploadMap.put("name", product_name_str);
                            uploadMap.put("desc", product_desc_str);
                            uploadMap.put("shop_by_cat_id", sub_cat_id);
                            uploadMap.put("menu_id", menu_id);
                            uploadMap.put("price", product_price_str);
                            uploadMap.put("quantity", product_quantity_str);
                            uploadMap.put("item_type", item_type + "");
                            uploadMap.put("discount", product_discount_str);
                            uploadMap.put("image", product_image_str);
                            uploadMap.put("status", String.valueOf(productAvailability));
                            url = PRODUCT_C;
                            data = new JSONObject(uploadMap).toString();
                        } else {
                            Toast.makeText(mContext, getString(R.string.please_select_product_image), Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (status_type == 2) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("id", getIntent().getStringExtra("product_id"));
                        uploadMap.put("name", product_name_str);
                        uploadMap.put("desc", product_desc_str);
                        uploadMap.put("shop_by_cat_id", sub_cat_id);
                        uploadMap.put("menu_id", menu_id);
                        uploadMap.put("price", product_price_str);
                        uploadMap.put("quantity", product_quantity_str);
                        uploadMap.put("item_type", item_type + "");
                        uploadMap.put("discount", product_discount_str);
                        uploadMap.put("image", product_image_str);
                        uploadMap.put("status", String.valueOf(productAvailability));
                        url = PRODUCT_U;
                        data = new JSONObject(uploadMap).toString();
                    }
                    createOrUpdateProduct(url, data);
                }
                break;
            default:
                break;
        }
    }

    private void createOrUpdateProduct(String url, final String data) {
        System.out.println("aaaaaaa  url  "+url+"   "+data);
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            Log.d("pr_u_c_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    MyProductsFragment.status_count = 1;
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, "Something went wrong");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private boolean isValid() {
        boolean valid = true;
      //  product_image_str = basse64Converter(product_image);
        Log.v("image",product_image_str);
        product_name_str = product_name.getText().toString().trim();
        product_desc_str = product_desc.getText().toString().trim();
        product_price_str = product_price.getText().toString().trim();
        product_quantity_str = product_quantity.getText().toString().trim();
        product_discount_str = product_discount.getText().toString().trim();
        if (veg_radio_btn.isChecked()) {
            item_type = veg;
        }
        if (non_veg_radio_btn.isChecked()) {
            item_type = nonveg;
        }
        int pr_image_length = product_image_str.length();
        int pr_name_length = product_name_str.length();
        int pr_desc_length = product_desc_str.length();
        int pr_price_length = product_price_str.length();
        int pr_quantity_length = product_quantity_str.length();
        int pr_discount_length = product_discount_str.length();

        if (pr_image_length == 0) {
            UIMsgs.showToast(mContext, "Please provide product image");
            valid = false;
        } else if (pr_name_length == 0) {
            setEditTextErrorMethod(product_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_desc_length == 0) {
            setEditTextErrorMethod(product_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_price_length == 0) {
            setEditTextErrorMethod(product_price, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (Integer.parseInt(product_price_str) <= 0) {
            setEditTextErrorMethod(product_price, "INVALID AMOUNT");
            valid = false;
        } else if (product_price_str.equalsIgnoreCase("0")) {
            setEditTextErrorMethod(product_price, INVALID);
            valid = false;
        } else if (pr_quantity_length == 0) {
            setEditTextErrorMethod(product_quantity, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (sub_cat_id == null) {
            UIMsgs.showToast(mContext, "Please provide category");
            valid = false;
        } else if (menu_id == null) {
            UIMsgs.showToast(mContext, "Please provide menu");
            valid = false;
        }

        return valid;
    }


    //Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAndUpdateProductActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateAndUpdateProductActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            product_image.setImageBitmap(photo);

            isImageSelected = true;
            Bitmap bitmap = ((BitmapDrawable) product_image.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            product_image_str=convert(bitmap);
            byte[] byteArray = baos.toByteArray();

            flag = 1;
        }

    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
      //  bm = Bitmap.createScaledBitmap(bm, 512, 512, false);


        product_image.setImageBitmap(bm);
        isImageSelected = true;
        Bitmap bitmap = ((BitmapDrawable) product_image.getDrawable()).getBitmap();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        product_image_str=convert(bitmap);
        byte[] byteArray = baos.toByteArray();

        flag = 1;


    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            productAvailability = 1;
        } else {
            productAvailability = 2;
        }
    }
}