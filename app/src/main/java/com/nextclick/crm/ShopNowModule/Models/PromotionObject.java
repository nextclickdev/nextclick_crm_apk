package com.nextclick.crm.ShopNowModule.Models;

public class PromotionObject implements Comparable<PromotionObject> {
    String id,header,subHeader,description,icon,accessibility;

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setHeader(String header) {
        this.header=header;
    }
    public String getHeader() {
        return this.header;
    }
    public void setSubHeader(String subHeader) {
        this.subHeader=subHeader;
    }
    public String getSubHeader() {
        return this.subHeader;
    }
    public void setDescription(String description) {
        this.description=description;
    }
    public String getDescription() {
        return this.description;
    }
    public void setIcon(String icon) {
        this.icon=icon;
    }
    public String getIcon() {
        return this.icon;
    }



    String cat_id,sub_cat_id,brand_id,constituency_id,promotion_banner_position_id,
            content_type,published_on,expired_on,owner,status,joined_user_id,promotion_banner_discount_type_id;

    public void setJoined_user_id(String joined_user_id) {
        this.joined_user_id=joined_user_id;
    }
    public String getJoined_user_id() {
        return this.joined_user_id;
    }

    public void setcat_id(String cat_id) {
        this.cat_id=cat_id;
    }
    public String getcat_id() {
        return this.cat_id;
    }
    public void setsub_cat_id(String sub_cat_id) {
        this.sub_cat_id=sub_cat_id;
    }
    public String getsub_cat_id() {
        return this.sub_cat_id;
    }
    public void setbrand_id(String brand_id) {
        this.brand_id=brand_id;
    }
    public String getbrand_id() {
        return this.brand_id;
    }
    public void setconstituency_id(String constituency_id) {
        this.constituency_id=constituency_id;
    }
    public String getconstituency_id() {
        return this.constituency_id;
    }
    public void setpromotion_banner_position_id(String promotion_banner_position_id) {
        this.promotion_banner_position_id=promotion_banner_position_id;
    }
    public String getpromotion_banner_position_id() {
        return this.promotion_banner_position_id;
    }
    public void setcontent_type(String content_type) {
        this.content_type=content_type;
    }
    public String getcontent_type() {
        return this.content_type;
    }
    public void setpublished_on(String published_on) {
        this.published_on=published_on;
    }
    public String getpublished_on() {
        return this.published_on;
    }
    public void setexpired_on(String expired_on) {
        this.expired_on=expired_on;
    }
    public String getexpired_on() {
        return this.expired_on;
    }
    public void setowner(String owner) {
        this.owner=owner;
    }
    public String getowner() {
        return this.owner;
    }
    public void setstatus(String status) {
        this.status=status;
    }
    public String getstatus() {
        return this.status;
    }

    public void setAccessibility(String accessibility) {
        this.accessibility=accessibility;
    }
    public String getAccessibility() {
        return this.accessibility;
    }


    public void setpromotion_banner_discount_type_id(String promotion_banner_discount_type_id) {
        this.promotion_banner_discount_type_id=promotion_banner_discount_type_id;
    }
    public String getpromotion_banner_discount_type_id() {
        return this.promotion_banner_discount_type_id;
    }


    String maxOrderQty,discount;
    public void setDiscount(String discount) {
        this.discount=discount;
    }
    public String getDiscount() {
        return this.discount;
    }

    public void setMaxOrderQty(String maxOrderQty) {
        this.maxOrderQty=maxOrderQty;
    }
    public String getMaxOrderQty() {
        return this.maxOrderQty;
    }

    String offer_title,offer_details;
    public void setOfferTile(String offer_title) {
        this.offer_title=offer_title;
    }
    public String getOfferTile() {
        return this.offer_title;
    }

    public void setOfferDetails(String offer_details) {
        this.offer_details=offer_details;
    }
    public String getOfferDetails() {
        return this.offer_details;
    }

    @Override
    public int compareTo(PromotionObject d) {
        return this.content_type.equals("3")?1:0;
    }
}
