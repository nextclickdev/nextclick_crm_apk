package com.nextclick.crm.ShopNowModule.Models;

public class SectionModel {

    String id;
    String menu_id;
    String menu_name;
    String item_id;
    String name;
    int required;
    int item_field;
    int sec_price;
    String productName;
    String categoryName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRequired() {
        return required;
    }

    public void setRequired(int required) {
        this.required = required;
    }

    public int getItem_field() {
        return item_field;
    }

    public void setItem_field(int item_field) {
        this.item_field = item_field;
    }

    public int getSec_price() {
        return sec_price;
    }

    public void setSec_price(int sec_price) {
        this.sec_price = sec_price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
