package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.MultiImageModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.REMOVE_BANNER;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.Profilefragment.base64ImageArray;


public class MultiImagesAdapter extends RecyclerView.Adapter<MultiImagesAdapter.ViewHolder> {

    List<MultiImageModel> data;

    LayoutInflater inflter;
    Context context;
    View itemView;
    PreferenceManager preferenceManager;
    private final String token;

    public MultiImagesAdapter(Context activity, List<MultiImageModel> completData) {
        this.context = activity;
        this.data = completData;
        preferenceManager = new PreferenceManager(context);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
    }


    @NonNull
    @Override
    public MultiImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(context).inflate(R.layout.supporter_multi_image_recycler, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new MultiImagesAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MultiImagesAdapter.ViewHolder holder, final int position) {
        final MultiImageModel multiImageModel = data.get(position);

        if(multiImageModel.getBitmap()!=null) {
            holder.banner_image.setImageBitmap(multiImageModel.getBitmap());
        }else{
            Glide.with(context)
                    .load(multiImageModel.getImage())
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.banner_image);
        }
        holder.remove_banner_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(multiImageModel.getId()==null) {
                    data.remove(position);
                    base64ImageArray.remove(position);

                    notifyDataSetChanged();
                }else{
                    new AlertDialog.Builder(context)
                            .setTitle("Alert")
                            .setMessage("Are you sure to remove..?")

                            // Specifying a listener allows you to take an action before dismissing the dialog.
                            // The dialog is automatically dismissed when a dialog button is clicked.
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    removeBanner(multiImageModel.getId(),position,data);
                                }
                            })

                            // A null listener allows the button to dismiss the dialog and take no further action.
                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                }
            }
        });

    }

    private void removeBanner(String id, final int position, final List<MultiImageModel> data) {
        Map<String,String> deleteMap=new HashMap<>();
        deleteMap.put("id",id);
        final String delString = new JSONObject(deleteMap).toString();
        LoadingDialog.loadDialog(context);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, REMOVE_BANNER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    data.remove(position);
                                    notifyDataSetChanged();
                                    UIMsgs.showToast(context, "Deleted Successfully");

                                } else {
                                    UIMsgs.showToast(context, Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(context, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(context, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return delString == null ? null : delString.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView banner_image, remove_banner_image;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            banner_image = itemView.findViewById(R.id.banner_image);
            remove_banner_image = itemView.findViewById(R.id.remove_product_image);


        }
    }
}


