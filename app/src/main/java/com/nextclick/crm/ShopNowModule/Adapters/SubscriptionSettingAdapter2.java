package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.SubscriptionPlan;
import com.nextclick.crm.ShopNowModule.Activities.ViewSubscriptionPrivileges;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;

import java.util.ArrayList;

public class SubscriptionSettingAdapter2 extends RecyclerView.Adapter<SubscriptionSettingAdapter2.ViewHolder> {
    private final Boolean isKeyName;
    private final ArrayList<SubscriptionPlan> subscriptionPlans;
    Context context;
    ArrayList<SubscriptionSetting> listOFNotificationDetails;
    public MediaPlayer mp;
    SharedPreferences preferenceManager;

    public SubscriptionSettingAdapter2(Context context, ArrayList<SubscriptionSetting> listOFNotificationDetails,
                                       ArrayList<SubscriptionPlan> subscriptionPlans, Boolean isKeyName) {
        this.context =context;
        this.subscriptionPlans =subscriptionPlans;
        this.listOFNotificationDetails =listOFNotificationDetails;
        preferenceManager=PreferenceManager.getDefaultSharedPreferences(context);
        this.isKeyName=isKeyName;
    }

    @NonNull
    @Override
    public SubscriptionSettingAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View  itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_privilege_value, parent, false);
        return new SubscriptionSettingAdapter2.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionSettingAdapter2.ViewHolder holder, int position) {


        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(subscriptionPlans.size(), LinearLayoutManager.VERTICAL);
        holder.recycle_images.setLayoutManager(staggeredGridLayoutManager);

        //holder.recycle_images.setLayoutManager(new GridLayoutManager(context, 1));
        SettingPackageAdapter adapter = new SettingPackageAdapter(context,subscriptionPlans, position);
        holder.recycle_images.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return listOFNotificationDetails!=null? listOFNotificationDetails.size(): 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        RecyclerView recycle_images;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recycle_images= itemView.findViewById(R.id.recycle_images);
        }
    }
}

