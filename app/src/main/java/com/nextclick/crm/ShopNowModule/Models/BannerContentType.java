package com.nextclick.crm.ShopNowModule.Models;

import java.util.List;

public class BannerContentType {
    String id,name;
    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setName(String name) {
        this.name=name;
    }
    public String getName() {
        return this.name;
    }
}
