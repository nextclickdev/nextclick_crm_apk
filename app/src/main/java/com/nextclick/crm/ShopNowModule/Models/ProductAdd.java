package com.nextclick.crm.ShopNowModule.Models;

import com.nextclick.crm.Helpers.UIHelpers.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class ProductAdd {
    String price,sizecolor,discount,id,item_id,sku,stock,weight,weight_type,selectedTaxID,selectedTaxTypeID,
            return_id,return_days,return_terms_conditions,item_type;
    int status,section_id,section_item_id,vendor_user_id,return_available;
    ArrayList<String> optionlist;
    Double discountInDouble = -1d, priceInDouble = -1d;

    private ArrayList<String> taxCategoryList;
    private ArrayList<String> taxCategoryIDList;

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public int getReturn_available() {
        return return_available;
    }

    public void setReturn_available(int return_available) {
        this.return_available = return_available;
    }

    public String getReturn_id() {
        return return_id;
    }

    public void setReturn_id(String return_id) {
        this.return_id = return_id;
    }

    public String getReturn_days() {
        return return_days;
    }

    public void setReturn_days(String return_days) {
        this.return_days = return_days;
    }

    public String getReturn_terms_conditions() {
        return return_terms_conditions;
    }

    public void setReturn_terms_conditions(String return_terms_conditions) {
        this.return_terms_conditions = return_terms_conditions;
    }

    private boolean isselect;
    private int activecheck,returncheck;

    public int getReturncheck() {
        return returncheck;
    }

    public void setReturncheck(int returncheck) {
        this.returncheck = returncheck;
    }

    public int getActivecheck() {
        return activecheck;
    }

    public void setActivecheck(int activecheck) {
        this.activecheck = activecheck;
    }

    public boolean isIsselect() {
        return isselect;
    }

    public void setIsselect(boolean isselect) {
        this.isselect = isselect;
    }

    public String getPrice() {
        return price;
    }
    public Double getPriceInDouble() {
        if (priceInDouble == -1)
            priceInDouble = Utility.getDiscount(price);
        return priceInDouble;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public ArrayList<String> getOptionlist() {
        return optionlist;
    }

    public void setOptionlist(ArrayList<String> optionlist) {
        this.optionlist = optionlist;
    }
    public void setOptionListFromString(String commaSeparatedString) {
        if(!commaSeparatedString.isEmpty() && commaSeparatedString.length()>0) {
            String[] elements = commaSeparatedString.split(",");
            this.optionlist = new ArrayList<String>(Arrays.asList(elements));
        }
    }

    public void addOptionToList(String option) {
        if (this.optionlist != null)
            this.optionlist.add(option);
    }

    public String getSizecolor() {
        return sizecolor;
    }

    public void setSizecolor(String sizecolor) {
        this.sizecolor = sizecolor;
    }

    public String getDiscount() {
        return discount;
    }

    public Double getDiscountInDouble() {
        if (discountInDouble == -1)
            discountInDouble = Utility.getDiscount(discount);
        return discountInDouble;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSection_id() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }

    public int getSection_item_id() {
        return section_item_id;
    }

    public void setSection_item_id(int section_item_id) {
        this.section_item_id = section_item_id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public int getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(int vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getWeight_type() {
        return weight_type;
    }

    public void setWeight_type(String weight_type) {
        this.weight_type = weight_type;
    }

    public void setTaxesList(ArrayList<String> taxCategoryList, ArrayList<String> taxCategoryIDList) {
        this.taxCategoryList = taxCategoryList;
        this.taxCategoryIDList = taxCategoryIDList;
    }
    public ArrayList<String> getTaxCategoryList() {
        return taxCategoryList;
    }
    public ArrayList<String> getTaxCategoryIDList() {
        return taxCategoryIDList;
    }

    public void setSelectedTaxID(String selectedTaxID) {
        this.selectedTaxID = selectedTaxID;
    }
    public String getSelectedTaxID() {
        return selectedTaxID;
    }

    public void setSelectedTaxTypeID(String selectedTaxID) {
        this.selectedTaxTypeID = selectedTaxID;
    }
    public String getSelectedTaxTypeID() {
        return selectedTaxTypeID;
    }

    public JSONObject getJSONObject() {
        String finalweight;
        if (weight_type.equalsIgnoreCase("2") ||weight_type.equalsIgnoreCase("3") ){
            finalweight=""+(Integer.parseInt(weight)*1000);
        }else{
            finalweight=weight;
        }
        JSONObject obj = new JSONObject();
        try {
            obj.put("variant_id", id);

            if (getOptionlist() != null) {
                StringBuilder str = new StringBuilder();
                for (String option : getOptionlist()) {
                    if(!option.isEmpty())
                        str.append(option).append(",");
                }
                if (str.length() > 0) {
                    obj.put("option_name",  str.substring(0, str.length() - 1));
                }
            }
          //  obj.put("discount", discount);
            obj.put("price", price);
            obj.put("weight", finalweight);
            obj.put("status", status);
          //  obj.put("stock", qunatity);

        } catch (JSONException e) {
            System.out.println("aaaaaa  catch "+e.getMessage());
           // trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

}
