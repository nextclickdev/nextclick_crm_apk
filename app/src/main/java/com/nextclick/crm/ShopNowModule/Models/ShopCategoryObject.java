package com.nextclick.crm.ShopNowModule.Models;

public class ShopCategoryObject {

    String id,cat_id,type,description,name,status,vendor_id,image;
    private boolean selected;

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setCatID(String cat_id) {
        this.cat_id=cat_id;
    }
    public String getCatID() {
        return this.cat_id;
    }
    public void setType(String type) {
        this.type=type;
    }
    public String getType() {
        return this.type;
    }
    public void setDescription(String description) {
        this.description=description;
    }
    public String getDescription() {
        return this.description;
    }
    public void setName(String name) {
        this.name=name;
    }
    public String getName() {
        return this.name;
    }


    public void setStatus(String status) {
        this.status=status;
    }
    public String getStatus() {
        return this.status;
    }
    public void setVendorID(String vendor_id) {
        this.vendor_id=vendor_id;
    }
    public String getVendorID() {
        return this.vendor_id;
    }
    public void setImage(String image) {
        this.image=image;
    }
    public String getImage() {
        return this.image;
    }

    public void setSelected(Boolean selected) {
        this.selected=selected;
    }
    public Boolean getSelected() {
        return this.selected;
    }

}
