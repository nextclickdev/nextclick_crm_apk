package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.nextclick.crm.R;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.nextclick.crm.Config.Config.ALL_ORDERS;
import static com.nextclick.crm.Config.Config.CANCELLED_ORDERS;
import static com.nextclick.crm.Config.Config.PAST_ORDERS;
import static com.nextclick.crm.Config.Config.REJECTED_ORDERS;
import static com.nextclick.crm.Config.Config.UPCOMING_ORDERS;


public class OrdersFragment extends Fragment {

    private View root;
    private TabLayout tabLayout;
    private ViewPager orders_view_pager;
    private Context mContext;
    private EditText start_date, end_date;
    private TextView get;
    final Calendar myCalendar = Calendar.getInstance();

    public OrdersFragment() {

    }

    public OrdersFragment(Context mContext) {

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_orders, container, false);
        initView();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(start_date);
            }
        };
        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(end_date);
            }
        };


        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mContext, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mContext, date1, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTabsToFragments();
            }
        });
        return root;
    }

    private void updateLabel(EditText editText) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editText.setText(sdf.format(myCalendar.getTime()));
    }

    private void initView() {
        mContext = getActivity();
        tabLayout = root.findViewById(R.id.tabLayout);
        orders_view_pager = root.findViewById(R.id.orders_view_pager);
        start_date = root.findViewById(R.id.start_date);
        end_date = root.findViewById(R.id.end_date);
        get = root.findViewById(R.id.apply);
        AddTabsToFragments();
    }

    private void AddTabsToFragments() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        String start_date_str = start_date.getText().toString().trim();
        String end_date_str = end_date.getText().toString().trim();
        /*start_date_str = "2020-08-10";
        end_date_str = "2020-08-28";*/
        viewPagerAdapter.addFrag(new OrderDataFragment(ALL_ORDERS, start_date_str, end_date_str), "All Orders");
        viewPagerAdapter.addFrag(new OrderDataFragment(UPCOMING_ORDERS, start_date_str, end_date_str), "Upcoming Orders");
        viewPagerAdapter.addFrag(new OrderDataFragment(PAST_ORDERS, start_date_str, end_date_str), "Past Orders");
        viewPagerAdapter.addFrag(new OrderDataFragment(CANCELLED_ORDERS, start_date_str, end_date_str), "Cancelled Orders");
        viewPagerAdapter.addFrag(new OrderDataFragment(REJECTED_ORDERS, start_date_str, end_date_str), "Rejected Orders");
        orders_view_pager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(orders_view_pager);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        orders_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                tabLayout.setScrollPosition(position,positionOffset,true);
            }

            @Override
            public void onPageSelected(int position) {
                viewPagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm/*, int behavior*/) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}