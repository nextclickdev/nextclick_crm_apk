package com.nextclick.crm.ShopNowModule.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PromotionDetailsActivity extends AppCompatActivity {

    PromotionObject promotionObject;
    TextView tv_header,tv_subHeader,tv_description;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_details2);

        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        Gson gson=new Gson();
        String promotionObjectAsString = getIntent().getStringExtra("promotionObject");
        Type type = new TypeToken<PromotionObject>(){}.getType();
        promotionObject = gson.fromJson(promotionObjectAsString, type);

        tv_header=findViewById(R.id.tv_header);
        tv_subHeader=findViewById(R.id.tv_subHeader);
        tv_description=findViewById(R.id.tv_description);
        tv_header.setText(promotionObject.getHeader());
        tv_subHeader.setText(promotionObject.getSubHeader());
        tv_description.setText(promotionObject.getDescription());
    }
}