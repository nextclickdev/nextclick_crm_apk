package com.nextclick.crm.ShopNowModule.Models;

public class SlidersModelClass {
    private String id;
    private String banners;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBanners() {
        return banners;
    }

    public void setBanners(String banners) {
        this.banners = banners;
    }
}
