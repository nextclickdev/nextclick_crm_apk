package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.JoinOfferPromotion;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MYPRODUCTLIST;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class JoinPromotionAdapter   extends RecyclerView.Adapter<JoinPromotionAdapter.ViewHolder> {


    private List<ShopByCategoryObject> data;
    private final Context context;
    int checkupdate=0;
    private final CustomDialog mCustomDialog;
    private final PreferenceManager preferenceManager;
    private Integer promotionOfferType=0;
    Double promotionDiscount;

    public JoinPromotionAdapter(JoinOfferPromotion addPromotionActivity,
                                     List<ShopByCategoryObject> data) {
        this.context = addPromotionActivity;
        this.data = data;
        preferenceManager=new PreferenceManager(context);
        mCustomDialog=new CustomDialog(context);
    }


    @Override
    public JoinPromotionAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_promotion_add_products, parent, false);
        return new JoinPromotionAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull JoinPromotionAdapter.ViewHolder holder, int position) {
        String selectedID=data.get(position).getID();
        holder.tv_category_name.setText(data.get(position).getTitle());
        holder.selectedData=data.get(position);
        holder.promotionOfferType=promotionOfferType;
        holder.promotionDiscount= promotionDiscount;

        Log.e("discount","In promotion "+holder.promotionDiscount);
        Log.e("discount","promotionDiscountType "+promotionOfferType);

        Picasso.get()
                .load(data.get(position).getImage())
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(holder.upload_image);

        holder.et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                data.get(position).setDiscount(holder.et_discount.getText().toString());
            }
        });

        holder.et_max_offer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                data.get(position).setMaxOrderQty(holder.et_max_offer.getText().toString());
            }
        });

        //holder.layout_x.setVisibility(View.VISIBLE);
        holder.layout_y.setVisibility(View.VISIBLE);
        if(promotionOfferType == 5) {
            //Buy X get Y free(with optional discount amount
            //holder.layout_discount.setVisibility(View.VISIBLE);
        }
        else {
           // holder.layout_discount.setVisibility(View.GONE);
            if (promotionOfferType <= 3) {
                // holder.layout_x.setVisibility(View.GONE);
                holder.tv_header_X.setText(context.getString(R.string.selectProducts));
                holder.layout_y.setVisibility(View.GONE);
            }
            else
            {
                if(promotionOfferType == 6) {
                    holder.tv_header_Y.setText(context.getString(R.string.selectYZProduct));
                }
            }
        }

        holder.tv_addproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.getProducts(data.get(position),true);
            }
        });

        holder.img_add_x_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.sub_cat_id=data.get(position).getShopByCategoryID();
                holder.getProducts(data.get(position),true);
            }
        });
        holder.img_add_y_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.sub_cat_id=data.get(position).getShopByCategoryID();
                holder.getProducts(data.get(position),false);
            }
        });
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void refresh(List<ShopByCategoryObject> listShopByCategoryObject, PromotionObject promotion) {
        this.data = listShopByCategoryObject;
        promotionOfferType = 0; // IsSelectedXProductType?1:0;
        try
        {
            promotionDiscount =Utility.getDiscount(promotion.getDiscount());
            promotionOfferType=Integer.parseInt(promotion.getpromotion_banner_discount_type_id());
        }
        catch (Exception ex){}


        notifyDataSetChanged();
    }
    public List<ShopByCategoryObject> getSelectedData()
    {
        return data;
    }


    public class ViewHolder extends RecyclerView.ViewHolder  {

        public ShopByCategoryObject selectedData;
        public Integer promotionOfferType = 0;
        Double promotionDiscount;
        private final ImageView img_add;
        private final ImageView upload_image;
        private final ImageView img_minus;
        TextView tv_addproduct,tv_category_name,tv_no_data;
        RecyclerView recycle_products,recycle_selectedproducts;
        PromotionCategoryProductsAdapter productSelectAdapter;
        private LinearLayoutManager layoutManager;
        private LinearLayoutManager layoutManagerselect;

        private ArrayList<String> menusList = new ArrayList<>();
        private ArrayList<String> menuIDList = new ArrayList<>();

        private ArrayList<ProductModel> productModelsList = new ArrayList<>();
        private final ArrayList<ProductModel> promocodesList = new ArrayList<>();


        boolean IsSelectedXProductType=false;
        private ArrayList<ProductModel> selectedXProducts = new ArrayList<>();
        private ArrayList<ProductModel> selectedYProducts = new ArrayList<>();

        private EditText etSearch;
        private Spinner filetr_menu_spinner;
        private int totalItems;
        private int page_no = 1;
        private int count;
        private final String stock_type="instock";
        private String sub_cat_id = null;
        private String menu_id = null;
        private int currentItems;
        private int scrollOutItems;
        private boolean isScrolled;
        private boolean isScrolling = false;
        private ShopByCategoryObject shopByCategoryObject;

        ImageView img_add_x_product,img_add_y_product;
        TextView tv_X_Products,tv_Y_Products,tv_header_X,tv_header_Y;
        private final TextInputEditText et_discount;
        private final TextInputEditText et_max_offer;
        LinearLayout layout_y,layout_x;
        TextInputLayout layout_discount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_add = itemView.findViewById(R.id.img_add);
            tv_category_name = itemView.findViewById(R.id.tv_category_name);
            upload_image = itemView.findViewById(R.id.upload_image);
            img_minus= itemView.findViewById(R.id.img_minus);
            tv_addproduct = itemView.findViewById(R.id.tv_addproduct);

            layout_x = itemView.findViewById(R.id.layout_y);
            layout_y = itemView.findViewById(R.id.layout_y);
            img_add_x_product = itemView.findViewById(R.id.img_add_x_product);
            img_add_y_product = itemView.findViewById(R.id.img_add_y_product);
            tv_X_Products = itemView.findViewById(R.id.tv_X_Products);
            tv_Y_Products = itemView.findViewById(R.id.tv_Y_Products);
            tv_header_X = itemView.findViewById(R.id.tv_header_X);
            tv_header_Y = itemView.findViewById(R.id.tv_header_Y);
            et_discount= itemView.findViewById(R.id.et_discount);
            layout_discount= itemView.findViewById(R.id.layout_discount);
            et_max_offer= itemView.findViewById(R.id.et_max_offer);

            recycle_selectedproducts=itemView.findViewById(R.id.recycle_products);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            recycle_selectedproducts.setLayoutManager(layoutManager1);
        }


        private void adapterSetter() {

            Integer selectionMode=1;//multi selction

            if(!IsSelectedXProductType)//Y
                selectionMode =promotionOfferType==6?2:0;

            productSelectAdapter = new PromotionCategoryProductsAdapter(context, productModelsList,0,checkupdate,
                    promocodesList,IsSelectedXProductType?selectedXProducts:selectedYProducts,selectionMode,promotionOfferType,promotionDiscount);
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false) {

                @Override
                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(context) {

                        private static final float SPEED = 300f;// Change this value (default=25f)

                        @Override
                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                            return SPEED / displayMetrics.densityDpi;
                        }

                    };
                    smoothScroller.setTargetPosition(position);
                    startSmoothScroll(smoothScroller);
                }

            };
            recycle_products.setLayoutManager(layoutManager);
            recycle_products.setAdapter(productSelectAdapter);
        }

        private void getProducts(ShopByCategoryObject shopByCategoryObject, boolean isXItemType){
            this.shopByCategoryObject=shopByCategoryObject;
            IsSelectedXProductType=isXItemType;
            LayoutInflater inflater =  LayoutInflater.from(context);
            View alertLayout = inflater.inflate(R.layout.select_products_dialog, null);
            recycle_products=alertLayout.findViewById(R.id.recycle_products);
            tv_no_data=alertLayout.findViewById(R.id.tv_no_data);
            etSearch=alertLayout.findViewById(R.id.etSearch);

            filetr_menu_spinner=alertLayout.findViewById(R.id.filetr_menu_spinner);
            ImageView ivSearch=alertLayout.findViewById(R.id.ivSearch);
            Button apply_p=alertLayout.findViewById(R.id.apply_p);
            Button reset=alertLayout.findViewById(R.id.reset);
            recycle_products.setLayoutManager(new GridLayoutManager(context,1));

           TextView tvCategory=alertLayout.findViewById(R.id.tvCategory);
           tvCategory.setText(sub_cat_id);
            AlertDialog.Builder alert = new AlertDialog.Builder(context);
            // alert.setIcon(R.mipmap.ic_launcher);
           // alert.setTitle(context.getString(R.string.products));
            alert.setView(alertLayout);
            alert.setCancelable(true);

            String ButtonTile="";
            if(isXItemType)
            {
                ButtonTile=context.getString(R.string.add_products);
            }
            else {
                ButtonTile=context.getString(R.string.add_product);
            }

            alert.setPositiveButton(ButtonTile, new DialogInterface.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                public void onClick(DialogInterface dialog, int id) {
                    captureUpdatedProducts(isXItemType);
                }
            });
            AlertDialog dialog = alert.create();
            dialog.show();
            adapterSetter();
            //getCategories();
            getMenus(sub_cat_id);
            page_no=1;
            menu_id=null;
            getProducts(1,"instock");

            apply_p.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    page_no = 1;
                    count = 0;
                    recycle_products.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    getProducts(1,stock_type);
                    adapterSetter();
                }
            });
            reset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    page_no = 1;
                    count = 0;
                    menu_id=null;
                    etSearch.setText("");
                    recycle_products.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    getProducts(1,stock_type);
                    adapterSetter();
                }
            });
            ivSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    page_no = 1;
                    count = 0;
                    recycle_products.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    adapterSetter();
                    getProducts(1,stock_type);
                }
            });

            filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        menu_id = menuIDList.get(position - 1);
                        page_no = 1;
                        count = 0;
                        recycle_products.setAdapter(null);
                        productModelsList = new ArrayList<>();
                        adapterSetter();
                        getProducts(1,stock_type);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            recycle_products.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                        isScrolling = true;
                    }
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    currentItems = layoutManager.getChildCount();
                    totalItems = layoutManager.getItemCount();
                    scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                    System.out.println("aaaaaaaaa currentitem  "+currentItems + scrollOutItems+"   "+totalItems);
                    if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                        isScrolled = true;
                        isScrolling = false;
                        count++;
                        boolean isContinue = true;
                        if (dy > previousOffset)
                            ++page_no;
                        else if (page_no > 1)//min range
                        {
                            //--page_no;
                            isContinue = false;
                        }
                        else {
                            isContinue = false;//comment this if you encounter any problem
                        }
                        previousOffset = dy;
                        if (isContinue)
                            getProducts(0,"instock");
                    }
                }
            });
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        private void captureUpdatedProducts(Boolean isXItemType) {
            etSearch.setText("");
            String text = "";
            ArrayList<ProductModel> selectedProducts = new ArrayList<>();
            for (int i = 0; i < productModelsList.size(); i++) {
                if (productModelsList.get(i).isSetpromocode()) {
                    promocodesList.add(productModelsList.get(i));

                    ArrayList<ProductAdd> vairants =  productModelsList.get(i).getSelectedvarientlist();
                    if(vairants!=null && vairants.size()>0) {
                        String selectedVariants = "";
                        for (int v = 0; v < vairants.size(); v++) {
                            ProductAdd varientmodel = vairants.get(v);
                            if (varientmodel.isIsselect()) {
                                try {
                                    ProductModel productModel =new ProductModel();
                                    productModel.setId(productModelsList.get(i).getId());
                                    productModel.setProduct_variant_id(varientmodel.getId());
                                    String name = varientmodel.getSizecolor().substring(0, 1).toUpperCase() +
                                            varientmodel.getSizecolor().substring(1)+",";
                                    selectedVariants += name;
                                    selectedProducts.add(productModel);
                                } catch (IndexOutOfBoundsException e) {
                                }
                            }
                        }

                        //UMA- DISABLED FIRST ITEM SELECT       `
                       /* if (selectedVariants.isEmpty()) {
                            try {
                                ProductModel productModel =new ProductModel();
                                productModel.setId(productModelsList.get(i).getId());
                                productModel.setProduct_variant_id(vairants.get(0).getId());
                                selectedVariants = vairants.get(0).getSizecolor().substring(0, 1).toUpperCase() +
                                        vairants.get(0).getSizecolor().substring(1) + ",";

                                selectedProducts.add(productModel);
                            } catch (IndexOutOfBoundsException e) {
                            }
                        }*/
                        text += selectedVariants;
                    }
                }
            }
            if(text.endsWith(",") && text.length()>0) {
                text = text.substring(0, text.length() - 1);
                if (isXItemType) {
                    tv_X_Products.setText(text);
                    selectedXProducts=selectedProducts;
                    img_add_x_product.setImageDrawable(context.getDrawable(R.drawable.ic_edit));
                } else {
                    tv_Y_Products.setText(text);
                    selectedYProducts=selectedProducts;
                    img_add_y_product.setImageDrawable(context.getDrawable(R.drawable.ic_edit));
                }
            }
            else
            {
                if (isXItemType) {
                    tv_X_Products.setText("");
                    selectedXProducts.clear();
                    img_add_x_product.setImageDrawable(context.getDrawable(R.drawable.ic_add_img));
                } else {
                    tv_Y_Products.setText("");
                    selectedYProducts.clear();
                    img_add_y_product.setImageDrawable(context.getDrawable(R.drawable.ic_add_img));
                }
            }
            shopByCategoryObject.setselectedXProducts(selectedXProducts);
            shopByCategoryObject.setselectedYProducts(selectedYProducts);
            // promocodeadapterSetter();
            // shopByCategoryObject.setSelectedProducts(promocodesList);
        }

        private int previousOffset;
        static final String randomString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom rnd = new SecureRandom();
        String randomString(int len){
            StringBuilder sb = new StringBuilder(len);
            for(int i = 0; i < len; i++)
                sb.append(randomString.charAt(rnd.nextInt(randomString.length())));
            return sb.toString();
        }


        public void getProducts(int i,String stock_type) {
            if (i==1){
                productModelsList.clear();
            }
            mCustomDialog.show();
            int limit =10;
            int offset =((page_no-1)* limit);
            Map<String, String> dataMap = new HashMap<>();
            //dataMap.put("page_no", page_no + "");
            dataMap.put("limit",  limit+ "");
            dataMap.put("offset", offset  + "");
            dataMap.put("q", ""+etSearch.getText().toString().trim());
            dataMap.put("menu_id", menu_id);
            dataMap.put("shop_by_cat_id", sub_cat_id);
            dataMap.put("stock_type", stock_type);
            // dataMap.put("status", APPROVEDSTATUS);
            final String data = new JSONObject(dataMap).toString();
            System.out.println("aaaaaaaa  data "+data+" api "+MYPRODUCTLIST);
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, MYPRODUCTLIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            //  LoadingDialog.dialog.dismiss();
                            mCustomDialog.dismiss();
                            if (response != null) {
                                Log.d("Product_resp", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    System.out.println("aaaaaaaaaaaa  myproducts "+ jsonObject);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        Object aObj=null;
                                        try {
                                            try {
                                                JSONObject dataObject = jsonObject.getJSONObject("data");
                                                aObj = dataObject.get("result");
                                            }
                                            catch (Exception ex){}
                                            if (!(aObj instanceof JSONArray) || aObj instanceof JSONObject) {
                                                if (page_no > 1)
                                                    page_no--;
                                                System.out.println("getProducts responsed failed - MyProducts");
                                                return;
                                            }
                                            JSONArray dataArray =(JSONArray) aObj;
                                            if (dataArray.length() > 0) {
                                                listIsFull();
                                                for (int i = 0; i < dataArray.length(); i++) {
                                                    JSONObject productObject = dataArray.getJSONObject(i);

                                                    ProductModel productModel = new ProductModel();
                                                    productModel.setId(productObject.getString("id"));
                                                    productModel.setSub_cat_id(productObject.getString("sub_cat_id"));
                                                    productModel.setMenu_id(productObject.getString("menu_id"));
                                                    productModel.setProduct_code(productObject.getString("product_code"));
                                                    productModel.setName(productObject.getString("name"));
                                                    productModel.setDesc(Html.fromHtml(productObject.getString("desc")).toString());
                                                    productModel.setItem_type(productObject.getString("item_type"));
                                                    productModel.setStatus(productObject.getInt("status"));
                                                    //  productModel.setProduct_image(productObject.getString("product_image"));
                                                    ArrayList<ImagesModel> imageslist=new ArrayList<>();
                                                    try {

                                                        String productImage = productObject.getString("image");
                                                        if(productImage!=null)
                                                        {
                                                            ImagesModel imagesModel=new ImagesModel();
                                                            imagesModel.setImage(productImage);
                                                            imageslist.add(imagesModel);
                                                        }

                                                        productModel.setImagelist(imageslist);
                                                    }catch (Exception e){

                                                    }

                                                    try {
                                                        productModel.setAvailabilityStatus(productObject.getString("availability"));
                                                        productModel.setCategoryName(productObject.getJSONObject("sub_category").getString("name"));
                                                        productModel.setMenu_name(productObject.getJSONObject("menu").getString("name"));
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    productModel.setSetpromocode(false);
                                                    if (checkupdate==1){
                                                        // System.out.println("aaaaaaaaaaa listsize "+promocodesList.size());
                                                        for (int j=0;j<promocodesList.size();j++){
                                                            System.out.println("aaaaaaa  procut equal  "+promocodesList.get(j).getId()+"   "+productModel.getId());
                                                            if (promocodesList.get(j).getId().equalsIgnoreCase(productModel.getId())){
                                                                productModel.setSetpromocode(true);

                                                            }
                                                        }
                                                    }


                                                    productModelsList.add(productModel);
                                                    productSelectAdapter.setchanged(productModelsList);

                                                }


                                                if(productModelsList!=null && productModelsList.size()>0) {
                                                    tv_no_data.setVisibility(View.GONE);
                                                    recycle_products.setVisibility(View.VISIBLE);
                                                    recycle_products.setVisibility(View.VISIBLE);
                                                }
                                                else {
                                                    tv_no_data.setVisibility(View.VISIBLE);
                                                    recycle_products.setVisibility(View.GONE);
                                                }
                                            }
                                            else{

                                                tv_no_data.setVisibility(View.VISIBLE);
                                                recycle_products.setVisibility(View.GONE);
                                            }
                                        } catch (Exception e) {
                                            System.out.println("aaaaaaa catch "+e.getMessage());
                                            e.printStackTrace();

                                            tv_no_data.setVisibility(View.VISIBLE);
                                            recycle_products.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (Exception e) {
                                    System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(context, MAINTENANCE);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            mCustomDialog.dismiss();
                            System.out.println("aaaaaaa error 111  "+error.getMessage());
                            UIMsgs.showToast(context, OOPS);

                            tv_no_data.setVisibility(View.VISIBLE);
                            recycle_products.setVisibility(View.GONE);
                        }
                    }) {

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }

        private void listIsFull() {
            // tvError.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            recycle_products.setVisibility(View.VISIBLE);
        }


        private void getMenus(String sub_cat_id) {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("menu_res", response);

                            if (response != null) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                        if (dataArray.length() > 0) {
                                            System.out.println("aaaaaaaaa  menu spinner");
                                            menusList = new ArrayList<>();
                                            menuIDList = new ArrayList<>();
                                            menusList.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject categoryObject = dataArray.getJSONObject(i);
                                                menusList.add(categoryObject.getString("name"));
                                                menuIDList.add(categoryObject.getString("id"));
                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, menusList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            filetr_menu_spinner.setAdapter(adapter);
                                        }
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(context, MAINTENANCE);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("aaaaaaa  444 "+error.getMessage());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }

    }

}
