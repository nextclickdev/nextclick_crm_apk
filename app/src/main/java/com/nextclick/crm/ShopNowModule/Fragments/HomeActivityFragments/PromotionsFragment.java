package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.MyPromotionsActivity;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsListActivity;
import com.nextclick.crm.ShopNowModule.Adapters.DashboardCartAdapter;

public class PromotionsFragment  extends Fragment {

    RecyclerView recycler_dashobard;
    Context mContext;

    public PromotionsFragment(Context mContext) {
        this.mContext=mContext;
    }

    public PromotionsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.promotions_fragment, container, false);
        initializeUi(view);
        return view;
    }


    private void initializeUi(View root) {
        mContext = getActivity();
        recycler_dashobard = root.findViewById(R.id.recycler_dashobard);
        DashboardCartAdapter dashboardCartAdapter=new DashboardCartAdapter(mContext,this);
        recycler_dashobard.setAdapter(dashboardCartAdapter);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recycler_dashobard.setLayoutManager(staggeredGridLayoutManager);
    }
    public void doAction(Integer itemPosition) {
        if (itemPosition == 0) {
            Intent intent = new Intent(mContext, PromotionsListActivity.class);
            intent.putExtra("isMyPromotions", itemPosition != 0);
            startActivity(intent);

        } else if (itemPosition == 1) {
            Intent intent1 = new Intent(mContext, MyPromotionsActivity.class);
            startActivity(intent1);
        }
    }

}
