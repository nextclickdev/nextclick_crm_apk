package com.nextclick.crm.ShopNowModule.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.OptionsAdapter;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.imagesselect.ImagesActivity;
import com.nextclick.crm.productDetails.activities.AddNewBrandActivity;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.adapters.SelectedImageAdapter;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nextclick.crm.productDetails.model.Shopbycategeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.BRANDS;
import static com.nextclick.crm.Config.Config.DELETEIMAGE;
import static com.nextclick.crm.Config.Config.PRODUCTDETAILS;
import static com.nextclick.crm.Config.Config.PRODUCT_C;
import static com.nextclick.crm.Config.Config.PRODUCT_U;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class ProductAddOrUpdate  extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private ImageView img_add,back_image,banner_image,upload_image,tv_addimage;
    private LinearLayout layout_addproduct,layout_itemtype;
    TextInputEditText product_name,product_desc, product_price, product_quantity, product_discount;
    EditText product_option;
    TextView tv_addnewbrand;
    Spinner category_spinner,menu_spinner,brands_spinner,item_type_spinner;
    private LinearLayout layout_add;
    ArrayList<String> listedittext;
     ArrayList<ProductAdd> productaddlist;
    private int countoptions=0;
    private Button submit;
    private List<View> allViews = new ArrayList<View>();
    private  EditText[] multiselecttext;
    OptionsAdapter optionsAdapter;
    private RecyclerView recycle_views;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String product_name_str, product_desc_str, base64image="",
            token, sub_cat_id = null,cat_id=null, menu_id = null,brand_id=null,item_type_id="3", product_price_str, product_quantity_str, product_discount_str,product_option_str;
   private ArrayList<ImagesModel> imageslist;

   // private ArrayList<Shopbycategeries> categoriesList;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> catidlist;
    private ArrayList<String> producttypelist;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<String> brandidlist = new ArrayList<>();
    private ArrayList<String> brandlist = new ArrayList<>();
    private ArrayList<String> itemtypelist = new ArrayList<>();
    private TextView product_status_type;
    private int status_type = 1;
    private Context mContext;
    private SwitchCompat switchCompact;
    private RadioButton veg_radio_btn, non_veg_radio_btn;
    PreferenceManager preferenceManager;
    private int productAvailability = 1;
    private int item_type = 0;
    private int veg = 1, nonveg = 2;
    private boolean isImageSelected;
    private int currentposition, sectionid;
    private RecyclerView images_recycle;
    private SelectedImageAdapter selectedImageAdapter;
    private ArrayList<String> imagelist;
    private ArrayList<String> base64imglist;
    String product_id;
    Gson gson;
    private CustomDialog customDialog;
    boolean checkpermisstiona;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_add_new);
         checkpermisstiona= Utility.checkPermission(ProductAddOrUpdate.this);
     //  Objects.requireNonNull(getSupportActionBar()).hide();
        init();
        prepareDetails();
        prepareSpinnerDetails();

        itemtypelist=new ArrayList<>();
        itemtypelist.add("Veg");
        itemtypelist.add("Non Veg");
        itemtypelist.add("Others");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, itemtypelist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        item_type_spinner.setAdapter(adapter);
        item_type_spinner.setSelection(2);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to product add or update activity");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA}, 101);
        }
    }
    private String blockCharacterSet = "~#^|$%&*!";

    private InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
    public void init(){
        mContext=ProductAddOrUpdate.this;
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(USER_TOKEN);
        customDialog=new CustomDialog(mContext);

        img_add=findViewById(R.id.img_add);
        upload_image=findViewById(R.id.upload_image);
        layout_addproduct=findViewById(R.id.layout_addproduct);
        product_status_type=findViewById(R.id.product_status_type);
        layout_add=findViewById(R.id.layout_add);
        submit=findViewById(R.id.submit);
        recycle_views=findViewById(R.id.recycle_views);
        back_image=findViewById(R.id.back_image);
        banner_image=findViewById(R.id.banner_image);
        tv_addimage=findViewById(R.id.tv_addimage);
        product_name=findViewById(R.id.product_name);
        product_desc=findViewById(R.id.product_desc);
        category_spinner=findViewById(R.id.category_spinner);
        brands_spinner=findViewById(R.id.brands_spinner);
        switchCompact = findViewById(R.id.switchCompact);
        veg_radio_btn = findViewById(R.id.veg_radio_btn);
        non_veg_radio_btn = findViewById(R.id.non_veg_radio_btn);
        menu_spinner = findViewById(R.id.menu_spinner);
        product_price = findViewById(R.id.product_price);
        product_option = findViewById(R.id.product_option);
        product_quantity = findViewById(R.id.product_quantity);
        product_discount = findViewById(R.id.product_discount);
        images_recycle = findViewById(R.id.images_recycle);
        item_type_spinner = findViewById(R.id.item_type_spinner);
        layout_itemtype = findViewById(R.id.layout_itemtype);
        tv_addnewbrand = findViewById(R.id.tv_addnewbrand);

       // product_name.setFilters(new InputFilter[]{filter});
        product_name.addTextChangedListener(new TextWatcher() {
            CharSequence previous;
            public void afterTextChanged(Editable s) {
                if(s.toString().contains("&^%$#*@&(")){
                    s.clear();
                    s.append(previous.toString());
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previous = s;
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
        gson = new Gson();
        base64imglist=new ArrayList<>();
        imageslist=new ArrayList<>();
        imagelist=new ArrayList<>();
        listedittext=new ArrayList<>();
        productaddlist=new ArrayList<>();
        img_add.setOnClickListener(this::onClick);
        layout_addproduct.setOnClickListener(this::onClick);
        back_image.setOnClickListener(this::onClick);
        banner_image.setOnClickListener(this::onClick);
        tv_addimage.setOnClickListener(this::onClick);
        submit.setOnClickListener(this::onClick);
        upload_image.setOnClickListener(this::onClick);
        switchCompact.setOnCheckedChangeListener(this);
        multiselecttext=new EditText[3];
        currentposition=getIntent().getIntExtra("currentposition",0);
        recycle_views.setLayoutManager(new GridLayoutManager(this,1));
        optionsAdapter=new OptionsAdapter(ProductAddOrUpdate.this,productaddlist);
        recycle_views.setAdapter(optionsAdapter);

        selectedImageAdapter = new SelectedImageAdapter(this, imageslist);
        images_recycle.setAdapter(selectedImageAdapter);

        try {
            Thread thread = new Thread();
            getCategories();
            /*thread.sleep(1000);
            getMenus();*/
            thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        images_recycle.setLayoutManager(layoutManager);

        tv_addnewbrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(ProductAddOrUpdate.this, AddNewBrandActivity.class);
                startActivity(i);
            }
        });
    }
    private void prepareDetails() {
        try {
            String statusText = getIntent().getStringExtra("type");
            System.out.println("aaaaaaaa type "+statusText);
            if (statusText != null) {
                if (statusText.equalsIgnoreCase("u")) {
                    product_status_type.setText("Update product");
                    submit.setText("UPDATE");
                    status_type = 2;
                    LoadingDialog.loadDialog(mContext);
                     product_id=getIntent().getStringExtra("product_id");
                    getProductToUpdate(product_id);
                  //  getProduct(getIntent().getStringExtra("product_id"));
                }
            }else {
                ProductAdd productAdd=new ProductAdd();
                productAdd.setStock("0");
                productAdd.setPrice("");
                productAdd.setSizecolor("");
                productAdd.setDiscount("");
                productAdd.setItem_id("");
                productAdd.setId("0");
                productaddlist.add(productAdd);
                optionsAdapter.setrefresh(productaddlist,0);

                if (imageslist.size()==0) {

                    imageslist.add(new ImagesModel());
                    imageslist.add(new ImagesModel());
                }


                upload_image.setVisibility(View.GONE);
                images_recycle.setVisibility(View.VISIBLE);
                selectedImageAdapter.refresh(imageslist);
            }
        } catch (Exception e) {
            System.out.println("aaaaaaaa cathc "+e.getMessage());
            e.printStackTrace();
        }
    }
    private void prepareSpinnerDetails() {

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    cat_id = catidlist.get(position - 1);
                    LoadingDialog.loadDialog(mContext);
                    getMenus(sub_cat_id);
                    getBrands(cat_id);
                    LoadingDialog.dialog.dismiss();
                    if (producttypelist.get(position-1).equalsIgnoreCase("1")){
                        layout_itemtype.setVisibility(View.VISIBLE);
                    }else{
                        layout_itemtype.setVisibility(View.GONE);
                    }
                } else {
                    sub_cat_id = null;
                    menu_id = null;
                    menuIDList.clear();
                    menusList.clear();
                    menu_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);

                } else {
                    menu_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }); brands_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!brands_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    brand_id = brandidlist.get(position - 1);
                } else {
                    brand_id = null;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        }); item_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item_type_id =""+(position+1);
                System.out.println("aaaaaaaaaa item typemid  "+item_type_id);
               /* if (!item_type_spinner.getSelectedItem().toString().equalsIgnoreCase("Others")) {
                    item_type_id =""+position;
                } else {
                    item_type_id = "3";
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_add:
            case R.id.layout_addproduct:
                countoptions=countoptions+1;

              //  threeSwitchLayout(countoptions);
                ProductAdd productAdd=new ProductAdd();
                productAdd.setStock("0");
                productAdd.setPrice("");
                productAdd.setSizecolor("");
                productAdd.setDiscount("");
                productAdd.setItem_id("");
                productAdd.setId("0");
                productAdd.setActivecheck(1);
                productaddlist.add(productAdd);
                optionsAdapter.setrefresh(productaddlist,0);
               // recycle_views.setAdapter(optionsAdapter);
                break;
            // recycle_views.setAdapter(optionsAdapter);
            case R.id.back_image:
                if(!Constants.Use_ActivityDesign) {
                    if (currentposition==1){
                        finish();
                    }else{
                        Intent intent = new Intent(ProductAddOrUpdate.this, MainProductpage.class);
                        intent.putExtra("currentposition", currentposition);
                        startActivity(intent);
                    }

                }
                else
                {
                    getFragmentManager().popBackStack();
                }
                finish();
                break;
            case R.id.tv_addimage:

               /* if (imagelist.size()==0){
                    Intent intent1=new Intent(ProductAddOrUpdate.this, ImagesActivity.class);
                    intent1.putExtra("whichposition",0);
                    startActivityForResult(intent1, 2);
                }else {*/
                 //   String jsonCars = gson.toJson(imagelist);
                    Intent intent1=new Intent(ProductAddOrUpdate.this, ImagesActivity.class);
                  //  intent1.putExtra("whichposition",1);
                  //  intent1.putExtra("jsonCars",jsonCars);
                    startActivityForResult(intent1, 2);
               // }

              //  selectImage();
                break;
                case R.id.upload_image:
                   uploadNewImage();
               // }

              //  selectImage();
                break;
            case R.id.submit:
               /* for (int k=0;k<productaddlist.size();k++){
                 //   System.out.println("aaaaaa  "+productaddlist.get(k).getPrice()+"  "+productaddlist.get(k).getQunatity());
                    System.out.println("aaaaaa  "+productaddlist.get(k).getSize()+"  "+productaddlist.get(k).getColr()+"  "+
                            productaddlist.get(k).getOptional()+"  "+productaddlist.get(k).getPrice()+" "+
                            productaddlist.get(k).getQunatity());
                }*/

                if (isValid()) {
                    JSONArray jsonArray = new JSONArray();
                    ProductAdd productAdd1=new ProductAdd();
                   // productAdd1.setSizecolor(product_option.getText().toString().trim());
                  //  productAdd1.setPrice(product_price.getText().toString().trim());
                  //  productaddlist.add(productAdd1);
                    if (productaddlist.size()==0){
                        Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT).show();
                    }else {

                        boolean checkproduct=false;
                        for(int k=0;k<productaddlist.size();k++){
                            if (productaddlist.get(k).getWeight().isEmpty()|| productaddlist.get(k).getWeight().equalsIgnoreCase("")||
                                    productaddlist.get(k).getPrice().isEmpty()||productaddlist.get(k).getPrice().equalsIgnoreCase("")||
                                    productaddlist.get(k).getOptionlist().get(0).isEmpty()||productaddlist.get(k).getOptionlist().get(0).equalsIgnoreCase("")){
                              //  Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT).show();
                                checkproduct=false;
                                break;
                            }else {
                                checkproduct=true;
                            }
                        }
                        if (!checkproduct){
                            Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT).show();
                        }else {

                            for (int i = 0; i < productaddlist.size(); i++) {
                                jsonArray.put(productaddlist.get(i).getJSONObject());
                            }
                            String url = "";
                            String data = "";
                            if (status_type == 1) {
                                if (imageslist.size() != 0) {
                                    System.out.println("aaaaaaaaa  atatus type  " + status_type);
                                    Map<String, String> uploadMap = new HashMap<>();
                                    uploadMap.put("name", product_name_str);
                                    uploadMap.put("desc", product_desc_str);
                                    uploadMap.put("shop_by_cat_id", sub_cat_id);
                                    uploadMap.put("menu_id", menu_id);
                                    uploadMap.put("brand_id", "" + brand_id);
                                    //  uploadMap.put("price", product_price_str);
                                    //  uploadMap.put("quantity", product_quantity_str);
                                     uploadMap.put("item_type",  item_type_id + "");
                                    //  uploadMap.put("discount", product_discount_str);

                                    // uploadMap.put("image", base64image);

                                    uploadMap.put("status", String.valueOf(productAvailability));
                                    //  uploadMap.put("options", ""+jsonArray);

                                    url = PRODUCT_C;

                                    JSONArray jsonimagesArray = new JSONArray();
                                    for (int im = 0; im < imageslist.size(); im++) {//base64imglist
                                        if (imageslist.get(im).getBase64() != null)
                                            jsonimagesArray.put(imageslist.get(im).getBase64());
                                    }


                                    try {
                                        JSONObject json = new JSONObject(uploadMap);
                                        //  json.putOpt( "",data );
                                        json.put("variants", jsonArray);
                                        //  System.out.println("aaaaaaaa  withoutimage "+json.toString());
                                        json.put("item_images", jsonimagesArray);

                                        data = json.toString();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    // data = new JSONObject(uploadMap).toString();

                                    createOrUpdateCategory(url, data);
                                    //  data = new JSONObject(uploadMap).toString();
                                    System.out.println("aaaaaaaa  url " + url + "  data " + data);
                                } else {
                                    Toast.makeText(mContext, getString(R.string.please_select_product_image), Toast.LENGTH_SHORT).show();
                                }
                            }
                            else if (status_type == 2) {
                                // if (isImageSelected) {
                                // System.out.println("aaaaaaaaa  atatus type  "+imageslist.size());
                                Map<String, String> uploadMap = new HashMap<>();
                                uploadMap.put("item_id", getIntent().getStringExtra("product_id"));
                                uploadMap.put("name", product_name_str);
                                uploadMap.put("desc", product_desc_str);
                                uploadMap.put("shop_by_cat_id", sub_cat_id);
                                uploadMap.put("menu_id", menu_id);
                                uploadMap.put("section_id", "" + sectionid);
                                uploadMap.put("brand_id", "" + brand_id);
                                uploadMap.put("item_type", "" + item_type_id);
                                url = PRODUCT_U;
                                JSONObject json = new JSONObject(uploadMap);
                                JSONArray jsonimagesArray = new JSONArray();

                                for (int k = 0; k < imageslist.size(); k++) {

                                    if(imageslist.get(k).getBase64()!=null) {
                                        System.out.println("aaaaaaaaa id " + imageslist.get(k).getId());
                                        try {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("id", imageslist.get(k).getId());
                                            jsonObject.put("image", imageslist.get(k).getBase64());
                                            jsonimagesArray.put(jsonObject);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }


                                try {
                                    //    json.putOpt( "",uploadMap );
                                    json.put("variants", jsonArray);
                                    System.out.println("aaaaaaaa  json " + json.toString());
                                    json.put("item_images", jsonimagesArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                // data = new JSONObject(uploadMap).toString();
                                data = json.toString();
                                createOrUpdateCategory(url, data);
                                System.out.println("aaaaaaaa  url " + url + "  data " + data);
                       /* }else {
                            System.out.println("aaaaaaaaa  atatus type  "+status_type);
                            Map<String, String> uploadMap = new HashMap<>();
                            uploadMap.put("item_id", getIntent().getStringExtra("product_id"));
                            uploadMap.put("name", product_name_str);
                            uploadMap.put("desc", product_desc_str);
                            uploadMap.put("shop_by_cat_id", sub_cat_id);
                            uploadMap.put("menu_id", menu_id);
                            uploadMap.put("section_id", ""+sectionid);
                            uploadMap.put("brand_id", ""+brand_id);

                            url = PRODUCT_U;
                            JSONObject json = new JSONObject(uploadMap);
                            JSONArray jsonimagesArray = new JSONArray();
                            JSONObject jsonObject=new JSONObject();
                            for (int k=0;k<imageslist.size();k++){
                                try {
                                    jsonObject.put("id",imageslist.get(k).getId());
                                    jsonObject.put("image",imageslist.get(k).getBase64());
                                    jsonimagesArray.put(jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }


                            try {
                                //    json.putOpt( "",uploadMap );
                                json.put("variants",jsonArray);
                                json.put("item_images",jsonimagesArray);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // data = new JSONObject(uploadMap).toString();
                            data = json.toString();
                            createOrUpdateCategory(url,data);
                            System.out.println("aaaaaaaa  url "+url+"  data "+data);
                        }
*/
                            }
                        }
                    }


                  /*  banner_image.buildDrawingCache();
                    Bitmap bitmap = banner_image.getDrawingCache();

                    ByteArrayOutputStream stream=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
                    byte[] image=stream.toByteArray();


                    base64image = Base64.encodeToString(image, 0);
                    Log.v("Image", ""+base64image);
*/

                }

              /*  for (int i =0;i<layout_add.getChildCount();i++)
                {
                    RelativeLayout secondLayout= (RelativeLayout) layout_add.getChildAt(i);
                    //   and then:

                    EditText ed_item = (EditText) secondLayout
                            .findViewById(R.id.et_size);
                    EditText ed_value = (EditText) secondLayout
                            .findViewById(R.id.et_color);
                    EditText ed_value1 = (EditText) secondLayout
                            .findViewById(R.id.et_option);
                    System.out.println("aaaaaaa value "+ed_item.getText().toString()+"  "+ed_value.getText().toString()+"  "+
                            ed_value1.getText().toString());
                }*/

                break;
        }
    }

    public void uploadNewImage() {
        Intent intent2=new Intent(ProductAddOrUpdate.this, ImagesActivity.class);
        startActivityForResult(intent2, 2);
    }

    public void threeSwitchLayout(int countoption) {

        if (listedittext.size()!=0){
            Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_SHORT).show();
        }else {
            // Generate dynamic layout
            LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // Use demo.xml to create layout
            final View addView = layoutInflater.inflate(R.layout.dynamic_addlayout, null);


           // final LinearLayout layout_addedittext = (LinearLayout)addView.findViewById(R.id.layout_addedittext);
            final TextView tv_option = (TextView) addView.findViewById(R.id.tv_option);
            final ImageView img_cancel = (ImageView) addView.findViewById(R.id.img_cancel);
            final EditText et_size = (EditText)addView.findViewById(R.id.et_size);
            final EditText et_color = (EditText)addView.findViewById(R.id.et_color);
            final EditText et_option = (EditText)addView.findViewById(R.id.et_option);
            final EditText et_price = (EditText)addView.findViewById(R.id.et_price);
            final EditText et_qty = (EditText)addView.findViewById(R.id.et_qty);

            tv_option.setText(mContext.getString(R.string.option)+ " "+countoption);
          //  addEdittext(layout_addedittext,1);
          /*  img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  addEdittext(layout_addedittext,1);
                }
            });*/
            img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    layout_add.removeView(addView);
                    countoptions=countoptions-1;
                    tv_option.setText(mContext.getString(R.string.option)+ " "+countoptions);
                }
            });
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 15);
            addView.setLayoutParams(layoutParams);


            layout_add.addView(addView);
        }

    }
    public void addEdittext(LinearLayout layout_addedittext,int count) {


        // Generate dynamic layout
        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Use demo.xml to create layout
        final View addView = layoutInflater.inflate(R.layout.dynamic_edittext, null);

        final ImageView img_add = (ImageView) addView.findViewById(R.id.img_add);
        multiselecttext[count] = (EditText)addView.findViewById(R.id.edittext);
        img_add.setImageResource(R.drawable.ic_add_circle_outline_black_24dp);
        img_add.setTag("add");
        //  cardIdentifier.setText(card_identifier);

        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (img_add.getTag().equals("add")){
                    if (multiselecttext[count].getText().toString().trim().isEmpty()){
                        multiselecttext[count].setError("Empty");
                    }else {
                     //   listedittext.add(edittext.getText().toString().trim());

                        int countadd=count+1;
                        addEdittext(layout_addedittext,countadd);
                        img_add.setImageResource(R.drawable.ic_cancel);
                        img_add.setTag("delete");
                    }

                }else if (img_add.getTag().equals("delete")){
                    layout_addedittext.removeView(addView);
                  //  listedittext.remove(multiselecttext[count].getText().toString().trim());
                }

            }
        });
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 15);
        addView.setLayoutParams(layoutParams);
        allViews.add(multiselecttext[count]);
        layout_addedittext.addView(addView);
    }
    public static String getClassName(Class c) {
        String className = c.getName();
        int firstChar;
        firstChar = className.lastIndexOf('.') + 1;
        if (firstChar > 0) {
            className = className.substring(firstChar);
        }
        return className;
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductAddOrUpdate.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(ProductAddOrUpdate.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("aaaaaaaaa resultcode  "+resultCode);
        if (resultCode == Activity.RESULT_OK) {
             if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        if (requestCode==2){
            try{
                String carListAsString = data.getStringExtra("jsonCars");
                Type type = new TypeToken<List<String>>(){}.getType();
                imagelist = gson.fromJson(carListAsString, type);
                System.out.println("aaaaaaaaa imaelistsize  "+imagelist.size());
                for (int i=0;i<imagelist.size();i++){
                    try{
                        Bitmap bm = BitmapFactory.decodeFile(""+imagelist.get(i));
                        String base64=convert(bm);
                        base64imglist.add(base64);
                        ImagesModel imagesModel=new ImagesModel();
                        // imagesModel.setId("0");
                        imagesModel.setImage(imagelist.get(i));
                        imagesModel.setBase64(base64);



                        imageslist.add(getImageInsertIndex(),imagesModel);
                    }catch (NullPointerException e){
                        Bitmap image = null;
                        try {
                            image = BitmapFactory.decodeStream((InputStream) new URL(imagelist.get(i)).getContent());
                            System.out.println("aaaaaaaaaaaa image  "+image);
                        } catch (MalformedURLException e1) {  e.printStackTrace();

                            try{
                                image = BitmapFactory.decodeStream((InputStream) new URL("file:///"+imagelist.get(i)).getContent());

                            }catch (MalformedURLException s1){
                                System.out.println("aaaaaaaaaaaaa image  11  "+e1.getMessage());
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                                System.out.println("aaaaaaaaaaaaa image  33  "+ioException.getMessage());
                            }
                        } catch (IOException e2) {
                            System.out.println("aaaaaaaaaaaaa image 222  "+e2.getMessage());
                            e.printStackTrace();
                        }
                        if(image!=null) {
                            String base64 = convert(image);
                            base64imglist.add(base64);
                            ImagesModel imagesModel = new ImagesModel();
                            // imagesModel.setId("0");
                            imagesModel.setImage(imagelist.get(i));
                            imagesModel.setBase64(base64);
                            imageslist.add(getImageInsertIndex(), imagesModel);
                        }
                    }
                }
                System.out.println("aaaaaaaa  imagelist sixe "+imageslist.size());
                if (imageslist.size()==0){
                   // upload_image.setVisibility(View.VISIBLE);
                  //  images_recycle.setVisibility(View.GONE);
              //  }else {

                    imageslist.add(new ImagesModel());
                    imageslist.add(new ImagesModel());
                }
                upload_image.setVisibility(View.GONE);
                images_recycle.setVisibility(View.VISIBLE);

               selectedImageAdapter.refresh(imageslist);

            }catch (NullPointerException e){

               // upload_image.setVisibility(View.VISIBLE);
              //  images_recycle.setVisibility(View.GONE);
                System.out.println("aaaaaaa catch "+e.getMessage());
            }

        }

    }

    private int getImageInsertIndex() {
        int index =0;
        if(imageslist.size()>0)
        {
            for(int i = imageslist.size()-1; i>=0 ; i--)
            {
                if(imageslist.get(i).getImage() != null)
                {
                    index = i + 1;
                    break;
                }
            }
        }
        return index;
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }
        catch(Exception e){
            e.getMessage();
            System.out.println("aaaaaaaa catch e  "+e.getMessage());
            return null;
        }
    }
    public String convert(Bitmap bitmap) {
         ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        isImageSelected=true;
        banner_image.setImageBitmap(bm);
        base64image=convert(bm);

    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isImageSelected=true;
        banner_image.setImageBitmap(thumbnail);
        base64image=convert(thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

    }

    private void getProductToUpdate(String product_id) {
        System.out.println("aaaaa productid  "+product_id);
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PRODUCTDETAILS + product_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        customDialog.cancel();
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  product details "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    int productStatus = dataObject.getInt("status");
                                    if (productStatus == 1) {
                                        switchCompact.setChecked(true);
                                    } else {
                                        switchCompact.setChecked(false);
                                    }
                                    product_name.setText(dataObject.getString("name"));
                                    product_desc.setText(dataObject.getString("desc"));
                                   /* base64image=dataObject.getString("product_image");
                                    Picasso.get()
                                            .load(dataObject.getString("product_image"))
                                            .placeholder(R.drawable.loader_gif)
                                            .into(banner_image);*/
                                    //  product_quantity.setText(dataObject.getString("quantity"));
                                    product_desc.setText(Html.fromHtml(dataObject.getString("desc")).toString());
                                    product_price.setText(dataObject.getString("price"));

                                    JSONArray sectionsarray=dataObject.getJSONArray("sections");
                                    JSONObject sectionobject=sectionsarray.getJSONObject(0);
                                     sectionid=sectionobject.getInt("id");


                                    JSONArray sectionarray=dataObject.getJSONArray("section_items");
                                    productaddlist.clear();
                                    for (int i=0;i<sectionarray.length();i++){
                                        JSONObject jsonObject1=sectionarray.getJSONObject(i);

                                       /* if (i==0){
                                            product_price.setText(jsonObject1.getString("price"));
                                            product_option.setText(jsonObject1.getString("name"));
                                            product_option.setText(jsonObject1.getString("variant_id"));
                                        }else {*/
                                            ProductAdd productAdd=new ProductAdd();
                                            productAdd.setPrice(jsonObject1.getString("price"));
                                            //productAdd.setSizecolor(jsonObject1.getString("name"));
                                             productAdd.setOptionListFromString(jsonObject1.getString("name"));
                                            productAdd.setId(jsonObject1.getString("id"));
                                            productAdd.setItem_id(jsonObject1.getString("item_id"));
                                            productAdd.setStatus(jsonObject1.getInt("status"));
                                            productAdd.setWeight(jsonObject1.getString("weight"));
                                            try {

                                                if (jsonObject1.getString("tax_id") != null)
                                                    productAdd.setSelectedTaxID(jsonObject1.getString("tax_id"));
                                            }
                                            catch(Exception ex) {}
                                        productAdd.setActivecheck(1);
                                            productaddlist.add(productAdd);
                                     //   }
                                    }
                                    product_price.setText(productaddlist.get(0).getPrice());

                                    product_option.setText(productaddlist.get(0).getSizecolor());

                                    optionsAdapter.setrefresh(productaddlist,1);


                                    try {
                                        JSONArray imagesArray=dataObject.getJSONArray("item_images");
                                        System.out.println("aaaaaaaa imagesArray  "+imagesArray.toString());
                                        for (int i=0;i<imagesArray.length();i++){
                                            JSONObject jsonObject1=imagesArray.getJSONObject(i);
                                            System.out.println("aaaaaaaa jsonarray  "+jsonObject1.toString());
                                            String id=jsonObject1.getString("id");
                                           // String image=jsonObject1.getString("image");
                                          //  System.out.println("aaaaaaa image "+id+" "+jsonObject1.getString("image"));
                                            ImagesModel imagesModel=new ImagesModel();
                                            imagesModel.setId(id);
                                            imagesModel.setImage(jsonObject1.getString("image"));

                                            String imageDataString = convertUrlToBase64(jsonObject1.getString("image"));
                                          //  Log.v("imagebase64",imageDataString);
                                            imagesModel.setBase64(imageDataString);

                                            imageslist.add(imagesModel);
                                           /* Picasso.get()
                                                    .load(jsonObject1.getString("image"))
                                                    .placeholder(R.drawable.loader_gif)
                                                    .into(banner_image);*/
                                          //  base64image=jsonObject1.getString("image");
                                        }
                                        System.out.println("aaaaaaaa imagelist size  "+imageslist.size());
                                       // if (imageslist.size()==0){
                                         //   upload_image.setVisibility(View.VISIBLE);
                                          //  images_recycle.setVisibility(View.GONE);
                                       // }else {

                                            imageslist.add(new ImagesModel());
                                            imageslist.add(new ImagesModel());
                                       // }


                                        upload_image.setVisibility(View.GONE);
                                        images_recycle.setVisibility(View.VISIBLE);
                                        selectedImageAdapter.refresh(imageslist);

                                    }catch (Exception e){
                                        upload_image.setVisibility(View.VISIBLE);
                                        images_recycle.setVisibility(View.GONE);
                                        System.out.println("aaaaaa catch img "+e.getMessage());
                                    }

                                    //  product_discount.setText(dataObject.getString("discount"));
                                    int item_type = dataObject.getInt("item_type");
                                    if (item_type == 1) {
                                        veg_radio_btn.setChecked(true);
                                    }
                                    if (item_type == 2) {http://test.nextclick.in/vendor/api/ecom/vendor_products/my_products
                                        non_veg_radio_btn.setChecked(true);
                                    }

                                    try {
                                        menu_id = dataObject.getJSONObject("menu").getString("id");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Thread thread = new Thread();
                                        thread.sleep(1000);
                                        for (int i = 0; i < categoryIDList.size(); i++) {
                                            if (dataObject.getJSONObject("sub_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                                                sub_cat_id = dataObject.getJSONObject("sub_category").getString("id");
                                                category_spinner.setSelection((i + 1), true);
                                                getMenus(sub_cat_id);
                                            }
                                        }
                                    } catch (Exception e) {
                                        System.out.println("aaaaaa catch  "+e.getMessage());
                                        e.printStackTrace();
                                    }
                                    try {
                                        brand_id=dataObject.getString("brand_id");
                                        Thread thread = new Thread();
                                        thread.sleep(1000);
                                        for (int i = 0; i < catidlist.size(); i++) {
                                         //   System.out.println("aaaaaaaaaa brandid  "+catidlist.get(i)+"  "+dataObject.getJSONObject("brand").getString("id"));
                                            if (dataObject.getJSONObject("brand").getString("id").equalsIgnoreCase(catidlist.get(i))) {
                                                brand_id = dataObject.getJSONObject("brand").getString("id");
                                                brands_spinner.setSelection(( 1), true);
                                                getBrands(catidlist.get(i));
                                                // getMenus(sub_cat_id);
                                            }
                                        }
                                    } catch (Exception e) {
                                        System.out.println("aaaaaa catch  "+e.getMessage());
                                        e.printStackTrace();
                                    }

                                } else {
                                    UIMsgs.showToast(mContext, OOPS);
                                }


                            } catch (Exception e) {
                                System.out.println("aaaaaa catch  "+e.getMessage());
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, OOPS);
                            }

                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                        LoadingDialog.dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
                LoadingDialog.dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public String convertUrlToBase64(String url) {
        URL newurl;
        Bitmap bitmap;
        String base64 = "";
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            newurl = new URL(url);
            bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64;
    }

    private void getMenus(String sub_cat_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        System.out.println("aaaaaaaaaa  menus url  "+SHOP_BY_CATEGORY_R+sub_cat_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("menu_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaa  gretmenus  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(""+Html.fromHtml(categoryObject.getString("name")));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        menu_spinner.setAdapter(adapter);

                                        try {
                                            if (menu_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    thread.sleep(1000);
                                                    for (int i = 0; i < menuIDList.size(); i++) {
                                                        if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                            menu_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    System.out.println("aaaaaaaaaa  catch3  "+e.getMessage());
                                                    e.printStackTrace();
                                                }
                                            }


                                        } catch (Exception e) {
                                            System.out.println("aaaaaaaaaa  catch22  "+e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                menusList.clear();
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                menu_spinner.setAdapter(adapter);
                                System.out.println("aaaaaaaaaa  catch11  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getBrands(String sub_cat_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        System.out.println("aaaaaaaaa  brandsurl  "+BRANDS+sub_cat_id);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BRANDS + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("brands_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                System.out.println("aaaaaaaaa  respose brands  "+jsonObject);
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("brands");
                                    if (dataArray.length() > 0) {
                                        brandlist = new ArrayList<>();
                                        brandidlist = new ArrayList<>();
                                        brandlist.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            brandlist.add(""+Html.fromHtml(categoryObject.getString("name")));
                                            brandidlist.add(categoryObject.getString("id"));
                                        }
                                       // System.out.println("aaaaaaaa  brandidlist  "+brandidlist.size()+"   "+brandlist.size()+" brand_id "+brand_id);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, brandlist);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        brands_spinner.setAdapter(adapter);

                                        try {
                                            if (brand_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    thread.sleep(1000);
                                                    for (int i = 0; i < brandidlist.size(); i++) {
                                                        if (brand_id.equalsIgnoreCase(brandidlist.get(i))) {
                                                            brands_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa  catch  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa erro  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            productAvailability = 1;
        } else {
            productAvailability = 2;
        }
    }
    private void getCategories() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaa jsonobject  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        catidlist = new ArrayList<>();
                                        producttypelist = new ArrayList<>();
                                      /*  Shopbycategeries shopbycategeries=new Shopbycategeries();
                                        shopbycategeries.setCat_id("");
                                        shopbycategeries.setDesc("");
                                        shopbycategeries.setId("");
                                        shopbycategeries.setName("Select");
                                        shopbycategeries.setProduct_type_widget_status("");
                                        shopbycategeries.setType("");

                                        categoriesList.add(shopbycategeries);*/
                                        categoriesList.add("Select");

                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(""+Html.fromHtml(categoryObject.getString("name")));
                                            producttypelist.add(categoryObject.getString("product_type_widget_status"));
                                            catidlist.add(categoryObject.getString("cat_id"));
                                         //   System.out.println("aaaaaaaaa  catid  "+categoryObject.getString("cat_id"));
                                           /* Shopbycategeries shopbycategeries1=new Shopbycategeries();
                                            shopbycategeries1.setCat_id(categoryObject.getString("cat_id"));
                                            shopbycategeries1.setDesc(categoryObject.getString("name"));
                                            shopbycategeries1.setId(categoryObject.getString("id"));
                                            shopbycategeries1.setName(categoryObject.getString("name"));
                                            shopbycategeries1.setProduct_type_widget_status(categoryObject.getString("product_type_widget_status"));
                                            shopbycategeries1.setType(categoryObject.getString("type"));
                                            shopbycategeries1.setDesc(categoryObject.getString("desc"));
                                            shopbycategeries1.setImage(categoryObject.getString("image"));

                                            categoriesList.add(shopbycategeries);*/
                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        category_spinner.setAdapter(adapter);


                                    }
                                }


                            } catch (Exception e) {
                                System.out.println("aaaaaaaaa catch "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private boolean isValid() {
        boolean valid = true;
        //  base64image = basse64Converter(product_image);
     //   Log.v("image",base64image);
        product_name_str = product_name.getText().toString().trim();
        product_desc_str = product_desc.getText().toString().trim();
        product_price_str = product_price.getText().toString().trim();
        product_option_str = product_option.getText().toString().trim();
        product_quantity_str = product_quantity.getText().toString().trim();
        product_discount_str = product_discount.getText().toString().trim();
      /*  if (veg_radio_btn.isChecked()) {
            item_type = veg;
        }
        if (non_veg_radio_btn.isChecked()) {
            item_type = nonveg;
        }*/

        int pr_image_length = imageslist.size();
        if(pr_image_length>0) {
            pr_image_length = 0;
            for (int i = 0; i < imageslist.size(); i++)
                if (imageslist.get(i).getBase64() != null) {
                    pr_image_length = 1;
                    continue;
                }
        }

        int pr_name_length = product_name_str.length();
        int pr_desc_length = product_desc_str.length();
        int pr_price_length = product_price_str.length();
        int pr_product_option_length = product_option_str.length();
        int pr_quantity_length = product_quantity_str.length();
        int pr_discount_length = product_discount_str.length();

        if (pr_image_length == 0) {
            UIMsgs.showToast(mContext, "Please provide product image");
            valid = false;
        } else if (pr_name_length == 0) {
            setEditTextErrorMethod(product_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_name_length <= 3) {
            setEditTextErrorMethod(product_name, "Minimum 3 characters required");
            valid = false;
        }
        else if (pr_desc_length == 0) {
            setEditTextErrorMethod(product_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } /*else if (pr_product_option_length == 0) {
            setEditTextErrorMethod(product_option, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_price_length == 0) {
            setEditTextErrorMethod(product_price, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (Double.parseDouble(product_price_str) <= 0) {
            setEditTextErrorMethod(product_price, "INVALID AMOUNT");
            valid = false;
        } else if (product_price_str.equalsIgnoreCase("0")) {
            setEditTextErrorMethod(product_price, INVALID);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }else if (pr_quantity_length == 0) {
            setEditTextErrorMethod(product_quantity, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }*/ else if (sub_cat_id == null) {
            UIMsgs.showToast(mContext, "Please provide category");
            valid = false;
        } else if (menu_id == null) {
            UIMsgs.showToast(mContext, "Please provide menu");
            valid = false;
        }else if (brand_id == null) {
            UIMsgs.showToast(mContext, "Please provide brand");
            valid = false;
        }

        return valid;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkpermisstiona=true;
                    /* if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();*/
                } else {
                    //code for deny
                }
                break;
        }
    }

    private void createOrUpdateCategory(String url, final String data) {

      //  System.out.println("aaaaaaa data "+url+"  "+data);
        Log.v("update",data);
      //  LoadingDialog.loadDialog(mContext);
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.dismiss();
                            //LoadingDialog.dialog.dismiss();
                            Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  jsonobject  "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    if(Constants.Use_ActivityDesign) {
                                        if (status_type==2){
                                            UIMsgs.showToast(mContext, "Updated Successfully");
                                            MainProductpage.viewpagercurrentposion=0;
                                            Intent i=new Intent(ProductAddOrUpdate.this, MainProductpage.class);
                                            i.putExtra("currentposition",3);
                                            startActivity(i);
                                            finish();
                                        }else {
                                            UIMsgs.showToast(mContext, "Created Successfully");
                                            MainProductpage.viewpagercurrentposion=3;
                                            Intent i=new Intent(ProductAddOrUpdate.this, MainProductpage.class);
                                            i.putExtra("currentposition",3);
                                            startActivity(i);
                                            finish();
                                        }
                                    }
                                    else {
                                        if (status_type == 2)
                                            UIMsgs.showToast(mContext, "Updated Successfully");
                                        else
                                            UIMsgs.showToast(mContext, "Created Successfully");
                                        Utility.ShowPendingProuctsItem = true;
                                        getFragmentManager().popBackStack();
                                        finish();
                                    }

                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                LoadingDialog.dialog.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.dismiss();
                           // LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
               // LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
                customDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setremoveProduct(int position) {
        System.out.println("aaaaaa size  "+productaddlist.size()+"  "+position);
        productaddlist.remove(position);
        optionsAdapter.setrefresh(productaddlist,1);
        optionsAdapter. notifyItemRemoved(position);
        optionsAdapter.notifyDataSetChanged();
    }


    public ArrayList<String> addOptionToProduct(int position) {
        ArrayList<String> optionList =null;
        if(productaddlist!=null && productaddlist.size()>position) {
            productaddlist.get(position).addOptionToList("");
            optionList=  productaddlist.get(position).getOptionlist();
        }
        return optionList;
    }
    public ArrayList<String> removeOptionFromProduct(int productIndex, int position) {
        ArrayList<String> optionList =null;
        if(productaddlist!=null && productaddlist.size()>productIndex) {
            optionList=  productaddlist.get(productIndex).getOptionlist();
            if (optionList != null && optionList.size() > position) {
                optionList.remove(position);
            }
        }
        return optionList;
    }

    public void deleteimage(ImagesModel s) {
       // imageslist.remove(s);
       // selectedImageAdapter.refresh(imageslist);

        if (TextUtils.isEmpty(s.getId())){
            imageslist.remove(s);
            selectedImageAdapter.refresh(imageslist);
            selectedImageAdapter.notifyDataSetChanged();
            if (imageslist.size()==0)
            {
               // upload_image.setVisibility(View.VISIBLE);
               // images_recycle.setVisibility(View.GONE);
           // }else {

                imageslist.add(new ImagesModel());
                imageslist.add(new ImagesModel());
            }


            upload_image.setVisibility(View.GONE);
            images_recycle.setVisibility(View.VISIBLE);
        }else {
            JSONObject jsonObject=new JSONObject();
            try {
                jsonObject.put("image_id",s.getId());
                String data=jsonObject.toString();
                getDeleteImage(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void getDeleteImage(String data) {
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DELETEIMAGE ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  product details "+jsonObject.toString());
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    imageslist.clear();
                                    getProductToUpdate(product_id);
                                } else {
                                    UIMsgs.showToast(mContext, OOPS);
                                }


                            } catch (Exception e) {
                                System.out.println("aaaaaa catch  "+e.getMessage());
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, OOPS);
                            }

                        } else {
                            customDialog.cancel();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                       // LoadingDialog.dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
               // LoadingDialog.dialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setradiocheck(int position,int i) {
        productaddlist.get(position).setActivecheck(i);
    }
}
