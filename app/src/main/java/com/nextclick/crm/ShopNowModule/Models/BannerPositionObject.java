package com.nextclick.crm.ShopNowModule.Models;

import java.util.List;

public class BannerPositionObject {
    String image;
    List<BannerPositionIndex> bannerPositons;
    public void setImage(String image) {
        this.image=image;
    }
    public String getImage() {
        return this.image;
    }

    public void setBannerPositions(List<BannerPositionIndex> bannerPositons) {
        this.bannerPositons=bannerPositons;
    }
    public List<BannerPositionIndex> getBannerPositions() {
        return this.bannerPositons;
    }
}
