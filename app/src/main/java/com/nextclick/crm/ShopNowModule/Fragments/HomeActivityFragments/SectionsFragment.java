package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.CreateAndUpdateSectionActivity;
import com.nextclick.crm.ShopNowModule.Adapters.SectionsAdapter;
import com.nextclick.crm.ShopNowModule.Models.SectionModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SECTION_R;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class SectionsFragment extends Fragment implements View.OnClickListener {


    public static int status_count = 0;


    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private Button apply_sections;
    private SectionsAdapter sectionsAdapter;
    private PreferenceManager preferenceManager;
    private FloatingActionButton add_section_fab;
    private RecyclerView seller_sections_recycler;
    private Spinner filetr_menu_spinner, filter_category_spinner;

    private String token = "";
    private String menu_id = "";
    private String sub_cat_id = "";
    private final String search_string = "";

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<SectionModel> sectionModelArrayList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();


    public SectionsFragment() {
        // Required empty public constructor
    }

    public SectionsFragment(Context mContext) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sections, container, false);
        initializeUi(view);
        initializeListeners();
        getSections();
        getCategories();
        prepareSpinnerDetails();
        return view;
    }


    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);


        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        apply_sections = view.findViewById(R.id.apply_sections);
        add_section_fab = view.findViewById(R.id.add_section_fab);
        filetr_menu_spinner = view.findViewById(R.id.filetr_menu_spinner);
        filter_category_spinner = view.findViewById(R.id.filter_category_spinner);
        seller_sections_recycler = view.findViewById(R.id.seller_sections_recycler);
    }

    private void initializeListeners() {
        apply_sections.setOnClickListener(this);
        add_section_fab.setOnClickListener(this);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String enteredText = charSequence.toString();
                sectionsAdapter.getFilter().filter(enteredText);
                /*search_string = enteredText;
                getSections();*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void prepareSpinnerDetails() {
        filter_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filter_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    menuIDList.clear();
                    menusList.clear();
                    filetr_menu_spinner.setAdapter(null);
                    getMenus();
                } else {
                    sub_cat_id = null;
                    menu_id = null;
                    filetr_menu_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("menu_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filetr_menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, "No menus available for the selected menu");
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filter_category_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getSections() {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("shop_by_cat_id", sub_cat_id);
        dataMap.put("q", search_string);
        dataMap.put("menu_id", menu_id);
        final String data = new JSONObject(dataMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SECTION_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            listIsFull();
                                            sectionModelArrayList = new ArrayList<>();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject sectionObject = dataArray.getJSONObject(i);
                                                SectionModel sectionModel = new SectionModel();
                                                sectionModel.setId(sectionObject.getString("id"));
                                                sectionModel.setName(sectionObject.getString("name"));
                                                sectionModel.setMenu_id(sectionObject.getString("menu_id"));
                                                sectionModel.setItem_id(sectionObject.getString("item_id"));
                                                sectionModel.setRequired(sectionObject.getInt("required"));
                                                sectionModel.setItem_field(sectionObject.getInt("item_field"));
                                                sectionModel.setSec_price(sectionObject.getInt("sec_price"));
                                                sectionModel.setMenu_name(sectionObject.getJSONObject("menu").getString("name"));

                                                try {
                                                    sectionModel.setProductName(sectionObject.getJSONObject("item").getString("name"));
                                                    //sectionModel.setCategoryName(sectionObject.getJSONObject("subcat").getString("name"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                sectionModelArrayList.add(sectionModel);
                                            }
                                            sectionsAdapter = new SectionsAdapter(mContext, sectionModelArrayList);
                                            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                                @Override
                                                public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                    LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                        private static final float SPEED = 300f;// Change this value (default=25f)

                                                        @Override
                                                        protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                            return SPEED / displayMetrics.densityDpi;
                                                        }

                                                    };
                                                    smoothScroller.setTargetPosition(position);
                                                    startSmoothScroll(smoothScroller);
                                                }

                                            };
                                            seller_sections_recycler.setLayoutManager(layoutManager);
                                            seller_sections_recycler.setAdapter(sectionsAdapter);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        UIMsgs.showToast(mContext, "Sections Not Available");
                                        listIsEmpty();
                                    }
                                } else {
                                    listIsEmpty();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, "Sections Not Available");
                                listIsEmpty();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            listIsEmpty();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        seller_sections_recycler.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        seller_sections_recycler.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_section_fab:
                mContext.startActivity(new Intent(mContext, CreateAndUpdateSectionActivity.class));
                break;
            case R.id.apply_sections:
                getSections();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (status_count == 1) {
            getSections();
            status_count = 0;
        }
    }
}