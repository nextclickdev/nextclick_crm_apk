package com.nextclick.crm.ShopNowModule.Activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.SectionsFragment;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MENU_R;
import static com.nextclick.crm.Config.Config.SECTION_C;
import static com.nextclick.crm.Config.Config.SECTION_R;
import static com.nextclick.crm.Config.Config.SECTION_U;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateSectionActivity extends AppCompatActivity implements View.OnClickListener {


    private Button submit;
    private Context mContext;
    private ImageView back_image;
    private EditText section_name;
    private TextView section_status_type;
    private PreferenceManager preferenceManager;
    private Spinner products_spinner, menu_spinner,category_spinner;
    private RadioButton radio_type, check_box_type, add, replace, no_change, yes, no;

    private int status_type = 1;//1- create, 2-update
    private final int item_type = 0;//1- create, 2-update
    private final int radio = 1;
    private final int checkbox = 2;
    private final int add_s = 1;
    private final int replace_s = 2;
    private final int nochange_s = 3;
    private int item_field_typ_int = 0;
    private int section_price_type = 0;
    private String section_name_str;
    private String section_require_items_str = null;
    private String token;
    private String menu_id = null;
    private String product_id = null;
    private String sub_cat_id=null;
    private final String item_id=null;

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<String> productsList=new ArrayList<>();
    private ArrayList<String> productsIDList = new ArrayList<>();

    private int page_no = 1, count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_update_section);
        getSupportActionBar().hide();
        init();

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    productsIDList.clear();
                    productsList.clear();
                    menusList.clear();
                    menuIDList.clear();
                    menu_spinner.setAdapter(null);
                    products_spinner.setAdapter(null);
                    getMenus();
                }else{
                    sub_cat_id=null;
                    productsIDList.clear();
                    productsList.clear();
                    menusList.clear();
                    menuIDList.clear();
                    menu_spinner.setAdapter(null);
                    products_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                    productsIDList.clear();
                    productsList.clear();
                    products_spinner.setAdapter(null);
                    getProducts();
                }else{
                    menu_id=null;
                    productsIDList.clear();
                    productsList.clear();
                    products_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        products_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!products_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    try {
                        product_id = productsIDList.get(position - 1);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        submit.setOnClickListener(this);
        back_image.setOnClickListener(this);
    }


    private void init() {
        mContext = CreateAndUpdateSectionActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        back_image = findViewById(R.id.back_image);

        section_name = findViewById(R.id.section_name);
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);

        products_spinner = findViewById(R.id.products_spinner);
        menu_spinner = findViewById(R.id.menu_spinner);
        category_spinner = findViewById(R.id.category_spinner);
        submit = findViewById(R.id.submit);
        section_status_type = findViewById(R.id.section_status_type);
        radio_type = findViewById(R.id.radio_type);
        check_box_type = findViewById(R.id.check_box_type);
        add = findViewById(R.id.add);
        replace = findViewById(R.id.replace);
        no_change = findViewById(R.id.no_change);
        productsList = new ArrayList<>();
        productsList.add("select");
        productsIDList = new ArrayList<>();

        try {
            Thread thread = new Thread();
            getCategories();
            Thread.sleep(1000);
            //getProducts();
            //thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String statustext = getIntent().getStringExtra("type");
            if (statustext.equalsIgnoreCase("u")) {
                section_status_type.setText("Update Section");
                submit.setText("UPDATE");
                status_type = 2;
                getSection(getIntent().getStringExtra("section_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R+sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("menu_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                        if (dataArray.length() > 0) {
                                            menusList = new ArrayList<>();
                                            menuIDList = new ArrayList<>();
                                            menusList.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {

                                                JSONObject categoryObject = dataArray.getJSONObject(i);
                                                menusList.add(categoryObject.getString("name"));
                                                menuIDList.add(categoryObject.getString("id"));

                                            }

                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                    android.R.layout.simple_spinner_item, menusList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            menu_spinner.setAdapter(adapter);


                                            try{
                                                if(menu_id!=null){
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < menuIDList.size(); i++) {
                                                            if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                                menu_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }

                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        UIMsgs.showToast(mContext,"Menus not available for selected category");
                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getProducts() {
        /*Map<String, String> dataMap = new HashMap<>();
        dataMap.put("page_no", page_no + "");
        dataMap.put("q", "");
        dataMap.put("menu_id", menu_id);
        dataMap.put("shop_by_cat_id", "");*/
        /*final String data = new JSONObject(dataMap).toString();*/
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MENU_R+menu_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ressssssssssss",response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("items");
                                        if (dataArray.length() > 0) {
                                            productsList.add("select");
                                            for (int i = 0; i < dataArray.length(); i++) {

                                                JSONObject productObject = dataArray.getJSONObject(i);
                                                productsList.add(productObject.getString("name"));
                                                productsIDList.add(productObject.getString("id"));

                                            }
                                            count++;
                                            ++page_no;
                                            try {
                                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                        android.R.layout.simple_spinner_item, productsList);
                                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                products_spinner.setAdapter(adapter);
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }

                                            try{
                                                if(product_id!=null){
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < productsIDList.size(); i++) {
                                                            if (product_id.equalsIgnoreCase(productsIDList.get(i))) {
                                                                products_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            ///getProducts();


                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        UIMsgs.showToast(mContext, "Products Not Available");
                                        products_spinner.setAdapter(null);
                                        /*if (count == 0) {

                                        }
                                        try {
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                    android.R.layout.simple_spinner_item, productsList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            products_spinner.setAdapter(adapter);
                                        } catch (Exception e1) {
                                            e.printStackTrace();
                                        }*/
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {


           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getSection(String section_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SECTION_R + section_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            section_name.setText(dataObject.getString("name"));
                            int item_field = 0;
                            item_field = dataObject.getInt("item_field");
                            if (item_field == 1) {
                                radio_type.setChecked(true);
                            }
                            if (item_field == 2) {
                                check_box_type.setChecked(true);
                            }
                            int sec_price = 0;
                            sec_price = dataObject.getInt("sec_price");
                            if (sec_price == 1) {
                                add.setChecked(true);
                            }
                            if (sec_price == 2) {
                                replace.setChecked(true);
                            }
                            if (sec_price == 3) {
                                no_change.setChecked(true);
                            }
                            //section_require_items.setText(dataObject.getString());
                            int req = dataObject.getInt("required");
                            if (req == 1) {
                                yes.setChecked(true);
                            }
                            if (req == 0) {
                                no.setChecked(true);
                            }

                            try{
                                Thread thread = new Thread();
                                Thread.sleep(1000);
                                for (int i = 0; i < categoryIDList.size(); i++) {
                                    if (dataObject.getJSONObject("shop_by_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                                        category_spinner.setSelection((i + 1), true);
                                        sub_cat_id=dataObject.getJSONObject("shop_by_category").getString("id");
                                        getMenus();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try{

                                menu_id=dataObject.getJSONObject("menu").getString("id");
                                getProducts();

                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try{

                                product_id=dataObject.getJSONObject("item").getString("id");

                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            try {
                                Thread thread = new Thread();
                                Thread.sleep(1000);
                                for (int i = 0; i < menuIDList.size(); i++) {
                                    if (dataObject.getJSONObject("menu").getString("id").equalsIgnoreCase(menuIDList.get(i))) {
                                        menu_spinner.setSelection((i + 1), true);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getCategories() {

        Map<String,String> searchMap = new HashMap<>();
        searchMap.put("q","");
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res",response);

                        if(response!=null){
                            LoadingDialog.dialog.dismiss();
                            try{

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if(dataArray.length()>0){
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for(int i=0; i<dataArray.length(); i++){

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        category_spinner.setAdapter(adapter);


                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back_image:
                onBackPressed();
                break;
            case R.id.submit:
                if (isValid()) {
                    String url = "";
                    String data = "";


                    if (status_type == 1) {
                        Map<String, String> uploadMap = new HashMap<>();

                        uploadMap.put("item_field", item_field_typ_int + "");
                        uploadMap.put("sec_price", section_price_type + "");
                        uploadMap.put("menu_id", menu_id);
                        uploadMap.put("item_id", product_id);
                        uploadMap.put("require_items", section_require_items_str);
                        uploadMap.put("name", section_name_str);
                        url = SECTION_C;
                        data = new JSONObject(uploadMap).toString();
                    }
                    if (status_type == 2) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("id", getIntent().getStringExtra("section_id"));
                        uploadMap.put("item_field", item_field_typ_int + "");
                        uploadMap.put("sec_price", section_price_type + "");
                        uploadMap.put("menu_id", menu_id);
                        uploadMap.put("item_id", product_id);
                        uploadMap.put("require_items", section_require_items_str);
                        uploadMap.put("name", section_name_str);

                        url = SECTION_U;
                        data = new JSONObject(uploadMap).toString();
                    }

                    createOrUpdateSection(url, data);

                    break;
                }

        }

    }

    private boolean isValid() {
        boolean valid = true;
        section_name_str = section_name.getText().toString().trim();
        if (yes.isChecked()) {
            section_require_items_str = "1";
        }
        if (no.isChecked()) {
            section_require_items_str = "0";
        }
        if (radio_type.isChecked()) {
            item_field_typ_int = radio;
        }
        if (check_box_type.isChecked()) {
            item_field_typ_int = checkbox;
        }
        if (add.isChecked()) {
            section_price_type = add_s;
        }
        if (replace.isChecked()) {
            section_price_type = replace_s;
        }
        if (no_change.isChecked()) {
            section_price_type = nochange_s;
        }
        int sec_name_length = section_name_str.length();


        if (sec_name_length == 0) {
            setEditTextErrorMethod(section_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (item_field_typ_int == 0) {
            UIMsgs.showToast(mContext, "Item field is required");
            valid = false;
        } else if (section_price_type == 0) {
            UIMsgs.showToast(mContext, "Section price field is required");
            valid = false;
        } else if (section_require_items_str.length() < 1) {
            UIMsgs.showToast(mContext, "Please select require items");
            valid = false;
        } else if (menu_id == null) {
            UIMsgs.showToast(mContext, "Please provide menu");
            valid = false;
        } else if (product_id == null) {
            UIMsgs.showToast(mContext, "Please provide product");
            valid = false;
        }

        return valid;

    }

    private void createOrUpdateSection(String url, final String data) {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    SectionsFragment.status_count=1;
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, "Something went wrong");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}