package com.nextclick.crm.ShopNowModule.Models;

public class SliderItems {
    //set to String, if you want to add image url from internet
    private String image,id;
    Integer Icon;
    public SliderItems(String image) {
        this.image = image;
    }

    public SliderItems() {
    }

    public String getImage() {
        return image;
    }

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return id;
    }


    public void setIcon(Integer Icon) {
        this.Icon=Icon;
    }
    public Integer getIcon() {
        return Icon;
    }

}
