package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.CategoryModelAdapter;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.PositionsAdapter;
import com.nextclick.crm.ShopNowModule.Adapters.ShopByCategoryListAdapter;
import com.nextclick.crm.ShopNowModule.Models.BannerAvailability;
import com.nextclick.crm.ShopNowModule.Models.BannerContentType;
import com.nextclick.crm.ShopNowModule.Models.BannerPositionIndex;
import com.nextclick.crm.ShopNowModule.Models.BannerPositionObject;
import com.nextclick.crm.ShopNowModule.Models.Constiuencies;
import com.nextclick.crm.ShopNowModule.Models.Districts;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryObject;
import com.nextclick.crm.ShopNowModule.Models.ShopCategoryObject;
import com.nextclick.crm.ShopNowModule.Models.States;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.CompositeDateValidator;
import com.google.android.material.datepicker.DateValidatorPointBackward;
import com.google.android.material.datepicker.DateValidatorPointForward;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.GET_SHOP_BY_CATEGORIES;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class AddPromotionActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener ,
        PaymentResultWithDataListener {

    public static final int FLAG_GET_BANNER = 199;
    public static final int FLAG_PAYMENT = 200;
    ImageView banner_img,banner_position_preview,bannerAvailability;
    TextView  start_date_header, end_date_header,tv_chooseDate,banner_pos_info;
    LinearLayout layout_position_details;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int SELECT_MULTIPLE_FILE = 1;
    public static String bannerImageBase64 = "";
    private TextView start_date,end_date;
    TextInputEditText chooseDate;
    private CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    TextInputEditText et_title,et_description,et_offer_name,et_offer_value;
    TextInputEditText et_discount,et_max_offer;
    TextView tv_header1,tv_offer_tile,tv_offer_description;

    String BannerPosition="";
    String BannerContentType="";
    String OfferPromotionType="";
    List<ShopCategoryObject> listShops;
    List<ShopByCategoryObject> listShopByCategoryObject;
    List<PromotionObject> selectedVendors;
    List<PromotionObject> selectedShopByCategories;
    List<BannerContentType> listBannerContentType;
    List<BannerContentType> listOfferPromotions;
    List<BannerAvailability> listBannerAvailability;

    double amount=0;
    Boolean disablePayment =false;


    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    //String[] positions = { "1-Top", "2-After Categories", "3-At Brands", "4-After brands", "5-Bottom"};
    private ArrayList<String> requestPositions;
    //private ArrayList<String> bannerTypes;
    ArrayList<String> spinnerCategories=new ArrayList<>();
    ArrayList<String> bannerTypes=new ArrayList<>();
    ArrayList<String> offerTypes=new ArrayList<>();/// = { "Content", "Vendors", "Profile", "Offers"};
    LinearLayout layout_banner_content,layout_banner_vendors,layout_banner_profile,layout_banner_offers,layout_discount;
    private String promotionID;
    private PromotionObject promotionObject;
    private BannerPositionObject bannerPositionObject;
    Spinner spinner_banner_type,spinner_banner_positions,spinner_offer_promotion_type;
    RecyclerView recyclerView_shopBycategory,recycler_positions;
    ShopByCategoryListAdapter shopByCategoryListAdapter;
    private boolean isBannerImage;
    private ImageView catUploadImage=null;
    private ShopByCategoryObject selectedCategoryObject;
    private Boolean IsViewPromotion=false;
    private int selectedSlotID;
    LinearLayout layout_location,layout_img_upload;
    TextView tv_header,banner_pos_title;
    Button btn_add_promotion;
    PositionsAdapter positionsAdapter;
    private String SelectedBannerID;
    private BannerAvailability selectedBanner;
    LinearLayout vendor_title_layout;
    TextView offer_category_other;
    RelativeLayout layout_category,banner_slot_info;
    private View profile_banner_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_add_promotion);

      //  getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext= AddPromotionActivity.this;
        mCustomDialog=new CustomDialog(AddPromotionActivity.this);
        preferenceManager=new PreferenceManager(mContext);


        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor Clicked on Add Promotion");
        }

        profile_banner_layout= findViewById(R.id.profile_banner_layout);
        layout_category=findViewById(R.id.layout_category);
        banner_slot_info=findViewById(R.id.banner_slot_info);
        offer_category_other=findViewById(R.id.offer_category_other);
        layout_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSubCategory();
            }
        });

        layout_position_details=findViewById(R.id.layout_position_details);
        start_date_header=findViewById(R.id.start_date_header);
        end_date_header=findViewById(R.id.end_date_header);
        recycler_positions=findViewById(R.id.recycler_positions);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(AddPromotionActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);//horizontal
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_positions.getContext(), linearLayoutManager.getOrientation());
        recycler_positions.setLayoutManager(linearLayoutManager);
        recycler_positions.addItemDecoration(dividerItemDecoration);

        banner_pos_title= findViewById(R.id.banner_pos_title);
        spinner_state = findViewById(R.id.spinner_state);
        spinner_distict = findViewById(R.id.spinner_distict);
        spinner_constitunesy = findViewById(R.id.spinner_constitunesy);

        listShopByCategoryObject=new ArrayList<>();
        listShops=new ArrayList<>();
        requestPositions=new ArrayList<>();
        et_title=findViewById(R.id.et_title);
        et_description=findViewById(R.id.et_description);
        et_offer_name=findViewById(R.id.et_offer_name);
        et_offer_value=findViewById(R.id.et_offer_value);
        et_discount=findViewById(R.id.et_discount);
        et_max_offer=findViewById(R.id.et_max_offer);
        banner_img=findViewById(R.id.banner_img);
        bannerAvailability=findViewById(R.id.bannerAvailability);
        ImageView btn_upload=findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(this);
        start_date = findViewById(R.id.start_date);
        end_date = findViewById(R.id.end_date);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);

        vendor_title_layout=findViewById(R.id.vendor_title_layout);
        //getDate();

        recyclerView_shopBycategory=findViewById(R.id.recyclerView_shopBycategory);
        btn_add_promotion = findViewById(R.id.btn_add_promotion);


        btn_add_promotion.setOnClickListener(this);

        spinner_banner_positions = findViewById(R.id.spinner);
        spinner_banner_positions.setOnItemSelectedListener(this);

        spinner_banner_type = findViewById(R.id.spinner_banner_type);
        spinner_banner_type.setOnItemSelectedListener(this);

        spinner_offer_promotion_type= findViewById(R.id.spinner_offer_promotion_type);
        spinner_offer_promotion_type.setOnItemSelectedListener(this);

        tv_header= findViewById(R.id.tv_header);

        tv_header1= findViewById(R.id.tv_header1);
        tv_offer_tile= findViewById(R.id.tv_offer_tile);
        tv_offer_description= findViewById(R.id.tv_offer_description);


        layout_img_upload = findViewById(R.id.layout_img_upload);
        layout_location = findViewById(R.id.layout_location);
        layout_location.setVisibility(View.GONE);//always
        if(getIntent().hasExtra("IsViewPromotion"))
        {
            IsViewPromotion=getIntent().getBooleanExtra("IsViewPromotion",false);
        }
        if(getIntent().hasExtra("promotionID"))
        {
            vendor_title_layout.setVisibility(View.GONE);

            promotionID=getIntent().getStringExtra("promotionID");
            btn_add_promotion.setText(getString(R.string.update_details));
            getPromotionDetails(promotionID);
        }
        else
        {
            vendor_title_layout.setVisibility(View.VISIBLE);

            if(preferenceManager.getString("VendorName")!=null) {
                et_title.setText(preferenceManager.getString("VendorName"));
                tv_header1.setText(preferenceManager.getString("VendorName"));
            }

            et_title.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    tv_header1.setText(s.toString());
                }
            });
            et_offer_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    tv_offer_tile.setText(s.toString());
                }
            });

        }

        if(getIntent().hasExtra("constitueid")) {
            constitueid=getIntent().getStringExtra("constitueid");
        }



        if(preferenceManager.getString("UploadedImage")!=null)
        {
            BannerContentType="4";

            vendor_title_layout.setVisibility(View.GONE);

            bannerImageBase64=preferenceManager.getString("UploadedImage");
            preferenceManager.putString("UploadedImage",null);

            byte[] decodedString = Base64.decode(bannerImageBase64, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            banner_img.setImageBitmap(decodedByte);
        }
        if(getIntent().hasExtra("UploadedImage")) {
            try {
               //offers
                BannerContentType="4";

                vendor_title_layout.setVisibility(View.GONE);

                bannerImageBase64=getIntent().getStringExtra("UploadedImage");

                byte[] decodedString = Base64.decode(bannerImageBase64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                banner_img.setImageBitmap(decodedByte);

            } catch (Exception ex) {

            }
        }
        if(BannerContentType.equals("4")) {
            ViewGroup.LayoutParams params = banner_img.getLayoutParams();
            float factor = banner_img.getContext().getResources().getDisplayMetrics().density;
            params.width = (int) (200 * factor);
            banner_img.setLayoutParams(params);
            profile_banner_layout.setVisibility(View.GONE);
        }
        else {
            // if (promotionID == null || promotionID.isEmpty())
            profile_banner_layout.setVisibility(View.VISIBLE);
        }

        if(getIntent().hasExtra("SelectedImage")) {
            //profile - default banner
            BannerContentType="3";
            SelectedBannerID = getIntent().getStringExtra("SelectedID");
            Picasso.get()
                    .load(getIntent().getStringExtra("SelectedImage"))
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(banner_img);
        }


        recyclerView_shopBycategory.setLayoutManager(new GridLayoutManager(this,1));
        shopByCategoryListAdapter=new ShopByCategoryListAdapter(AddPromotionActivity.this,listShopByCategoryObject,IsViewPromotion);
        recyclerView_shopBycategory.setAdapter(shopByCategoryListAdapter);


        bannerTypes.add("Select");
        requestPositions.add("Select");
        offerTypes.add("Select");

        ImageView img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        layout_banner_content = findViewById(R.id.layout_banner_content);
        layout_banner_vendors = findViewById(R.id.layout_banner_vendors);
        layout_banner_profile = findViewById(R.id.layout_banner_profile);
        layout_banner_offers = findViewById(R.id.layout_banner_offers);
        layout_discount= findViewById(R.id.layout_discount);
        getShopByCategories();
        getContentTypes();
        getOfferPromotions();
        getPromotionBannerPositions();
        
        if(BannerContentType=="4")
        {
            collapseAllBannerViews();
            layout_banner_offers.setVisibility(View.VISIBLE);
        }

        MaterialDatePicker.Builder<Pair<Long, Long>> materialDateBuilder = MaterialDatePicker.Builder.dateRangePicker();
        materialDateBuilder.setTitleText(getString(R.string.book_a_slot));



        Calendar c1=Calendar.getInstance();
        Long todayTime= Calendar.getInstance().getTime().getTime();
        //CalendarConstraints.DateValidator dateValidator = DateValidatorPointForward.from(Calendar.getInstance().getTimeInMillis());
        //(new CalendarConstraints.Builder()).setValidator(dateValidator);

        materialDateBuilder.setCalendarConstraints(limitRange().build());
        materialDateBuilder.build();

        final MaterialDatePicker materialDatePicker = materialDateBuilder.build();

        //materialDatePicker.confirmButton.setText("");

        chooseDate=findViewById(R.id.chooseDate);
        tv_chooseDate=findViewById(R.id.tv_chooseDate);

        materialDatePicker.addOnPositiveButtonClickListener(
                new MaterialPickerOnPositiveButtonClickListener() {

                    @Override
                    public void onPositiveButtonClick(Object selection) {
                        //  chooseDate.setText(materialDatePicker.getHeaderText());

                        Pair selectedDates = (Pair) materialDatePicker.getSelection();
                        final Pair<Date, Date> rangeDate = new Pair<>(new Date((Long) selectedDates.first), new Date((Long) selectedDates.second));
                        Date startDate = rangeDate.first;
                        Date endDate = rangeDate.second;

                        //if (startDate.getTime() >=todayTime) {

                            SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
                            //  chooseDate.setText(simpleFormat.format(startDate)+"\n"+simpleFormat.format(endDate));
                            changeDate(simpleFormat.format(startDate), simpleFormat.format(endDate));
                       //v }
                        //else
                        //    Toast.makeText(mContext, "Please select future dates", Toast.LENGTH_SHORT).show();
                    }
                });

        chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(getSupportFragmentManager(), "date");
            }
        });
        tv_chooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                materialDatePicker.show(getSupportFragmentManager(), "date");
            }
        });

       /*
       Note: Earlier we shown current date as default date, but as per QA Team we are not setiting default date
       getDate();
        changeDate(start_date.getText().toString().replace("-", "/"), end_date.getText().toString().replace("-", "/"));
*/
        statelist=new ArrayList<States>();
        districtlist=new ArrayList<Districts>();
        constiencieslist=new ArrayList<Constiuencies>();
       // getStates();


        //hidden api, and see if data not exists while setting content type and also plase list inside inside layout not dilog\

        spinner_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (statelist.size()==0){

                }else {
                    if (position!=0){
                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        stateid=statelist.get(position-1).getId();
                        districtlist.clear();
                        constitueid=null;
                        getDistricts(statelist.get(position-1).getId());
                    }else {
                        spinner_distict.setEnabled(false);
                        spinner_distict.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        spinner_constitunesy.setClickable(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(AddPromotionActivity.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_distict.setAdapter(ad);
                        spinner_constitunesy.setAdapter(ad);
                        constitueid=null;

                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_distict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtlist.size()==0){
                    // Toast.makeText(PlacesAddActivity.this, "Please Select District", Toast.LENGTH_SHORT).show();
                }else {
                    if (position!=0){

                        spinner_distict.setEnabled(true);
                        spinner_distict.setClickable(true);
                        spinner_constitunesy.setClickable(true);
                        spinner_constitunesy.setEnabled(true);
                        districtid=districtlist.get(position-1).getId();
                        constiencieslist.clear();
                        // getConstituencis(districtlist.get(position-1).getDistrictID());
                        constitueid=null;
                        getConstituencis(stateid,districtlist.get(position-1).getId());

                    }else {
                        spinner_constitunesy.setClickable(false);
                        spinner_constitunesy.setEnabled(false);
                        ArrayList<String> selectlist=new ArrayList<>();
                        selectlist.add("Select");
                        ArrayAdapter ad = new ArrayAdapter(AddPromotionActivity.this,
                                android.R.layout.simple_spinner_item, selectlist);
                        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner_constitunesy.setAdapter(ad);
                        constitueid=null;
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_constitunesy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()){
                    //  Toast.makeText(context, "Please select District", Toast.LENGTH_SHORT).show();
                }else {
                    try{
                        if (position!=0){
                            constitueid=constiencieslist.get(position-1).getId();
                        }else{
                            //  Toast.makeText(context, "Please Select Constiency", Toast.LENGTH_SHORT).show();
                        }
                    }catch (IndexOutOfBoundsException e){

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //`if(promotionID!=null && !promotionID.isEmpty())
        //setActionsDisable();


        if(IsViewPromotion)
        {
            setActionsDisable();
        }
    }

    private void setActionsDisable() {
        tv_header.setText("Promotion");
        chooseDate.setEnabled(false);
        spinner_banner_type.setEnabled(false);
        layout_category.setEnabled(false);
        offer_category_other.setEnabled(false);
        offer_category_other.setAlpha(0.5f);
        spinner_offer_promotion_type.setEnabled(false);
        spinner_banner_positions.setEnabled(false);
        start_date.setEnabled(false);
        end_date.setEnabled(false);
        et_title.setEnabled(false);
        et_discount.setEnabled(false);
        et_offer_name.setEnabled(false);
        et_offer_value.setEnabled(false);
        //chooseDate.setEnabled(false);
        et_max_offer.setEnabled(false);
        et_description.setEnabled(false);
        btn_add_promotion.setVisibility(View.GONE);
        layout_img_upload.setVisibility(View.GONE);
        layout_location.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_upload:
                catUploadImage=null;
                isBannerImage=true;
                selectImage();
                /*Intent intent=new Intent(this,SelectBannerImage.class);
                intent.putExtra("promotionType",BannerContentType);
                startActivityForResult(intent,FLAG_GET_BANNER);*/
                break;
            case R.id.spinner:
                selectImage();
                break;
            case R.id.start_date:
                datepicker(0);
                break;
            case R.id.end_date:
                if (start_date.getText().toString()==null || start_date.getText().toString().isEmpty()){
                    Toast.makeText(this, "Please enter the start date.", Toast.LENGTH_SHORT).show();
                }else {
                    datepicker(1);
                }
                break;
            case R.id.img_back:
                finish();
                break;

            case R.id.btn_add_promotion:
                addPromotion();
                break;
        }
    }


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddPromotionActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(AddPromotionActivity.this);

                if (items[item].equals("Choose from Library")) {
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == FLAG_GET_BANNER) {
            bannerImageBase64 = data.getStringExtra("SelectedImage");

            Picasso.get()
                    .load(bannerImageBase64)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(banner_img);
        }
       else if (resultCode == FLAG_PAYMENT) {
            String PaymentMethod = data.getStringExtra("PaymentMethod");//wallet-3, online -2
            String PaymentStatus = data.getStringExtra("PaymentStatus");//2-success
            String transactionID = data.getStringExtra("paymentId");//transactionID

            if (PaymentStatus.equals("2")) {
                //success
                createorUpdatePromotion(transactionID, "" + amount);
            }
            else
            {
                //fail
                Toast.makeText(this, "Payment has been failed", Toast.LENGTH_SHORT).show();
            }
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);

        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            if(isBannerImage) {
                banner_img.setImageBitmap(photo);
                bannerImageBase64 = getBase64String(banner_img);
                isBannerImage=false;
            }
            else if( catUploadImage!=null) {
                catUploadImage.setImageBitmap(photo);
                if(selectedCategoryObject!=null)
                    selectedCategoryObject.setImage(getBase64String(catUploadImage));
                catUploadImage=null;
            }
        }
    }

    private String getBase64String(ImageView img) {
        Bitmap bitmap = ((BitmapDrawable) img.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);
        if (isBannerImage) {
            banner_img.setImageBitmap(bm);
            bannerImageBase64 = getBase64String(banner_img);
            isBannerImage = false;
        } else if (catUploadImage != null) {
            catUploadImage.setImageBitmap(bm);
            if(selectedCategoryObject!=null)
                selectedCategoryObject.setImage(getBase64String(catUploadImage));
            catUploadImage = null;
        }
    }
    //Image Selection End

    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="",startdate="",enddate="";
    String year,month,day;
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        startdate=year+"-"+month+"-"+day;
        enddate=year+"-"+month+"-"+day;
        start_date.setText(startdate);
        end_date.setText(enddate);

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //  System.out.println("aaaaaa currentdate  "+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+"  "+c.get(Calendar.YEAR));
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    startdate=fromyear+"-"+frommonth+"-"+fromday;
                    //   System.out.println("aaaaaaaa  startdate   "+startdate);
                    //start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    start_date.setText(""+startdate);

                    enddate="";
                    end_date.setText(enddate);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    enddate=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+enddate);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
           // datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {

        switch (parent.getId())
        {
            case R.id.spinner:
                if (position1==0){
                    BannerPosition=null;
                    return;
                }
                int position=position1-1;
                if(bannerPositionObject!=null &
                        bannerPositionObject.getBannerPositions()!=null &&
                        bannerPositionObject.getBannerPositions().size()>0) {
                    BannerPosition=bannerPositionObject.getBannerPositions().get(position).getID();
                }

              //  Toast.makeText(getApplicationContext(), requestPositions.get(position) , Toast.LENGTH_LONG).show();
                break;
            case R.id.spinner_offer_promotion_type:
                if (position1==0){
                    OfferPromotionType=null;
                    return;
                }
                position=position1-1;
                if(listOfferPromotions!=null &
                        listOfferPromotions.size()>0) {
                    OfferPromotionType=listOfferPromotions.get(position).getID();
                }
                switch (OfferPromotionType)
                {
                    case "1":
                    case "2":
                    case "5":
                        layout_discount.setVisibility(View.VISIBLE);
                        break;
                    default:
                        layout_discount.setVisibility(View.GONE);
                        break;

                }
                break;
            case R.id.layout_category:
                 openSubCategory();
                break;
            case R.id.spinner_banner_type:
              //  BannerContentType=position+1;
                if (position1==0){
                    BannerContentType=null;
                    return;
                }

                position=position1-1;
                if(listBannerContentType!=null &
                        listBannerContentType.size()>0) {
                        BannerContentType=listBannerContentType.get(position).getID();

                     //  if(BannerContentType.equals("3"))
                     //   BannerContentType="4";//testing
                }

               // Toast.makeText(getApplicationContext(), bannerTypes.get(position) , Toast.LENGTH_LONG).show();
                switch (BannerContentType)
                {
                    case "1":
                        collapseAllBannerViews();
                        layout_banner_content.setVisibility(View.VISIBLE);
                        break;
                    case "2":
                        collapseAllBannerViews();
                        layout_banner_vendors.setVisibility(View.VISIBLE);
                        //if(listVendors.size()>0)
                          //  showVendorsSelection();
                        break;
                    case "3":
                        collapseAllBannerViews();
                        layout_banner_profile.setVisibility(View.VISIBLE);
                        break;
                    case "4":
                        collapseAllBannerViews();
                        layout_banner_offers.setVisibility(View.VISIBLE);
                        //if(listShops.size()>0)
                       // showAddShopByCategory();
                        break;
                }
                break;
        }
    }


    private void collapseAllBannerViews() {
        layout_banner_content.setVisibility(View.GONE);
        layout_banner_vendors.setVisibility(View.GONE);
        layout_banner_profile.setVisibility(View.GONE);
        layout_banner_offers.setVisibility(View.GONE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void addPromotion() {

        if (validate()) {
//            if (BannerContentType.equals("3")) {
//                if(selectedBanner!=null) {
//                    makePayment(selectedSlotID,selectedBanner.getPrice());
//                }
//                return;
//            }
            createorUpdatePromotion("", "");
        }
    }

    private void createorUpdatePromotion(String Payment_gateway_id,String amount) {


        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("txn_id", Payment_gateway_id);
        uploadMap.put("amount", amount);
        uploadMap.put("title", et_title.getText().toString());
        uploadMap.put("offer_title", et_offer_name.getText().toString());
        uploadMap.put("offer_details", offer_category_other.getText().toString());
        if(BannerContentType.equals("1")) {
            uploadMap.put("desc", et_description.getText().toString());
        }
        uploadMap.put("constituency_id",  constitueid);

        //if(BannerPosition.equals("6")) {
            if (preferenceManager.getString("VendorCategory") != null)
                uploadMap.put("cat_id", preferenceManager.getString("VendorCategory"));
        //}

        uploadMap.put("promotion_banner_position_id", BannerPosition);//1-5
        uploadMap.put("content_type", BannerContentType); // 1 = Content, 2 = Vendors, 3 = Profile, 4 = Offers
        uploadMap.put("published_on",start_date.getText().toString().replace("-","/"));// "21/06/2021"
        uploadMap.put("expired_on", end_date.getText().toString().replace("-","/"));
        if(promotionID!=null && !promotionID.isEmpty()  && !promotionID.equals("null")) {
            if (bannerImageBase64 != null && !bannerImageBase64.isEmpty())
                uploadMap.put("image", bannerImageBase64);
            else if (SelectedBannerID != null && !SelectedBannerID.isEmpty())
                uploadMap.put("image_id", bannerImageBase64);
            else if(promotionObject.getIcon()!=null)
                uploadMap.put("image", promotionObject.getIcon());
        }
        else {
            if (bannerImageBase64 != null && !bannerImageBase64.isEmpty())
                uploadMap.put("image", bannerImageBase64);
            else if (SelectedBannerID != null && !SelectedBannerID.isEmpty())
                uploadMap.put("image_id", SelectedBannerID);
        }

        String URL=Config.CREATE_PROMOTION_BANNERS;
        if(promotionID!=null && !promotionID.isEmpty()  && !promotionID.equals("null")) {
            uploadMap.put("id", promotionID);
            URL = Config.UPDATE_PROMOTION_BANNERS;

            if (MyMixPanel.isMixPanelSupport) {
                MyMixPanel.logEvent("Vendor updated promotion of "+promotionID);
            }
        }
        else {
            if (MyMixPanel.isMixPanelSupport) {
                MyMixPanel.logEvent("Vendor created new promotion");
            }
        }

        JSONObject json = new JSONObject(uploadMap);
        if(BannerContentType.equals("4")) {
            try {
                json.put("discount_type_id", OfferPromotionType);
                if (OfferPromotionType != null && (OfferPromotionType.equals("1") || OfferPromotionType.equals("2") || OfferPromotionType.equals("5"))) {
                    json.put("discount", et_discount.getText().toString());
                }
                json.put("max_offer_steps", et_max_offer.getText().toString());

                listShopByCategoryObject = shopByCategoryListAdapter.getSelectedData();
                if (listShopByCategoryObject != null) {
                    JSONArray productsArray = new JSONArray();
                    try {
                        for (int i = 0; i < listShopByCategoryObject.size(); i++) {
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("id", listShopByCategoryObject.get(i).getID());
                            jsonObject1.put("image", listShopByCategoryObject.get(i).getImage());
                            productsArray.put(jsonObject1);
                        }
                        json.put("shop_by_categories", productsArray);
                    } catch (JSONException e) {
                        System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }
        }

        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                if (promotionID != null && !promotionID.isEmpty() && !promotionID.equals("null"))
                                    Toast.makeText(mContext, "Promotion updated successfully.", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(mContext, "Promotion created successfully.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else
                                Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
               // Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private boolean validate() {

       /* if(bannerImageBase64==null || bannerImageBase64.isEmpty())
        {
            if(promotionID==null || promotionID.isEmpty()) {
                Toast.makeText(mContext, "Please upload banner image", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(SelectedBannerID==null || SelectedBannerID.isEmpty())
        {
            if(promotionID==null || promotionID.isEmpty()) {
                Toast.makeText(mContext, "Please select the banner image", Toast.LENGTH_SHORT).show();
                return false;
            }
        }*/
        if(vendor_title_layout.getVisibility()==View.VISIBLE) {
            if (et_title.getText().toString() == null || et_title.getText().toString().isEmpty()) {
                Toast.makeText(mContext, "Please enter banner title", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (et_offer_name.getText().toString() == null || et_offer_name.getText().toString().isEmpty()) {
                Toast.makeText(mContext, "Please enter offer title", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (offer_category_other.getText().toString() == null || offer_category_other.getText().toString().isEmpty()) {
                Toast.makeText(mContext, "Please enter offer sub categories", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if(BannerContentType=="1" && (et_description.getText().toString()==null || et_description.getText().toString().isEmpty()))
        {
            Toast.makeText(mContext, "Please enter banner description", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(BannerContentType=="4")
        {
            listShopByCategoryObject=shopByCategoryListAdapter.getSelectedData();
            if(listShopByCategoryObject==null) {
                Toast.makeText(mContext, "Please enter offer products", Toast.LENGTH_SHORT).show();
                return false;
            }
            if(OfferPromotionType==null || OfferPromotionType.isEmpty())
            {
                Toast.makeText(mContext, "Please select offer promotion type ", Toast.LENGTH_SHORT).show();
                return false;
            }

            if(OfferPromotionType.equals("1") || OfferPromotionType.equals("2") || OfferPromotionType.equals("5")) {
                if(et_discount.getText().toString()==null || et_discount.getText().toString().isEmpty())
                {
                    Toast.makeText(mContext, "Please enter the discount", Toast.LENGTH_SHORT).show();
                    return false;
                }
               else if(OfferPromotionType.equals("1") &&  Float.parseFloat(et_discount.getText().toString())>= 100)
                {
                    Toast.makeText(mContext, "Discount value should be less than 100% ", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        if(BannerContentType==null || BannerContentType.isEmpty())
        {
            Toast.makeText(mContext, "Please select banner type ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(start_date.getText().toString()==null || start_date.getText().toString().isEmpty())
        {
            Toast.makeText(mContext, "Please enter banner validity ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(end_date.getText().toString()==null || end_date.getText().toString().isEmpty())
        {
            Toast.makeText(mContext, "Please enter end date", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(BannerPosition==null || BannerPosition.isEmpty())
        {
            Toast.makeText(mContext, "Please select banner position ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(constitueid==null || constitueid.isEmpty())
        {
            Toast.makeText(mContext, "Please select the Constituency", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void showAddShopByCategory() {
        if(listShops.size()==0) {
        }



       /* final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_add_shop_category);
        Button applyButton = (Button) dialog.findViewById(R.id.btn_add_promotion);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();*/
    }
    private void showVendorsSelection() {
       /* final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_show_vendor_selection);
        Button applyButton = (Button) dialog.findViewById(R.id.btn_add_promotion);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.closeButton);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();*/
    }
    private void showDescriptionAlert() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_add_shop_category);
        Button applyButton = dialog.findViewById(R.id.btn_add_promotion);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }


    private void getShopByCategories() {
        //mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_SHOP_BY_CATEGORIES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_SHOP_BY_CATEGORIES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");

                             //   spinnerCategories.add("Select All");

                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject shopObj = dataobj.getJSONObject(i);

                                    ShopCategoryObject shopCategoryObject = new ShopCategoryObject();
                                    shopCategoryObject.setID(shopObj.getString("id"));
                                    shopCategoryObject.setCatID(shopObj.getString("cat_id"));
                                    shopCategoryObject.setType(shopObj.getString("type"));
                                    shopCategoryObject.setName(shopObj.getString("name"));
                                    shopCategoryObject.setDescription(shopObj.getString("desc"));
                                    if(shopObj.has("status"))
                                    shopCategoryObject.setStatus(shopObj.getString("status"));
                                    if(shopObj.has("vendor_id"))
                                    shopCategoryObject.setVendorID(shopObj.getString("vendor_id"));
                                    if(shopObj.has("image"))
                                    shopCategoryObject.setImage(shopObj.getString("image"));

                                    listShops.add(shopCategoryObject);

                                    spinnerCategories.add(shopCategoryObject.getName());
                                }
                                if(listShops!=null) {
                                    if(listShopByCategoryObject.size()==0)
                                        listShopByCategoryObject.add(new ShopByCategoryObject());
                                    shopByCategoryListAdapter.refresh(listShopByCategoryObject,listShops);

                                  //  spinnerCategories.add("Others");
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                        finally {
                            //mCustomDialog.dismiss();
                            showAddShopByCategory();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mCustomDialog.dismiss();
               // Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void getPromotionDetails(String promotionID) {
        mCustomDialog.show();
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("id", promotionID);
        JSONObject json = new JSONObject(uploadMap);
        System.out.println("GET_PROMOTION_BANNERS request" + json);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_PROMOTION_BANNERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_SHOP_BY_CATEGORIES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                JSONObject transobj = dataObj.getJSONObject("banner");

                                promotionObject = new PromotionObject();
                                promotionObject.setID(transobj.getString("id"));
                                promotionObject.setHeader(transobj.getString("title"));
                                promotionObject.setDescription(transobj.getString("desc"));

                                if(transobj.has("offer_title"))
                                    promotionObject.setOfferTile(transobj.getString("offer_title"));
                                if(transobj.has("offer_details"))
                                    promotionObject.setOfferDetails(transobj.getString("offer_details"));

                                if(transobj.has("image"))
                                    promotionObject.setIcon(transobj.getString("image"));

                                promotionObject.setcat_id(transobj.getString("cat_id"));
                                promotionObject.setsub_cat_id(transobj.getString("sub_cat_id"));
                                promotionObject.setbrand_id(transobj.getString("brand_id"));
                                promotionObject.setconstituency_id(transobj.getString("constituency_id"));
                                promotionObject.setpromotion_banner_position_id(transobj.getString("promotion_banner_position_id"));
                                promotionObject.setcontent_type(transobj.getString("content_type"));
                                // promotionObject.setDescription(transobj.getString("created_user_id"));
                                //  promotionObject.setHeader(transobj.getString("updated_user_id"));
                                promotionObject.setpublished_on(transobj.getString("published_on"));
                                promotionObject.setexpired_on(transobj.getString("expired_on"));
                                promotionObject.setowner(transobj.getString("owner"));
                                promotionObject.setstatus(transobj.getString("status"));
                                promotionObject.setAccessibility(transobj.getString("accessibility"));

                                if(transobj.has("promotion_banner_discount_type_id"))
                                    promotionObject.setpromotion_banner_discount_type_id(transobj.getString("promotion_banner_discount_type_id"));
                                if(transobj.has("discount"))
                                promotionObject.setDiscount(transobj.getString("discount"));
                                if(transobj.has("max_offer_steps"))
                                promotionObject.setMaxOrderQty(transobj.getString("max_offer_steps"));

                                try {
                                    listShopByCategoryObject.clear();

                                    JSONArray OffersObj = dataObj.getJSONArray("offers_on_shop_by_categories");
                                    for (int i = 0; i < OffersObj.length(); i++) {
                                        JSONObject offer = OffersObj.getJSONObject(i);

                                        ShopByCategoryObject shopByCategoryObject = new ShopByCategoryObject();
                                        shopByCategoryObject.setID(offer.getString("id"));
                                       shopByCategoryObject.setShopByCategoryID(offer.getString("sub_cat_id"));
                                        if (offer.has("image"))
                                            shopByCategoryObject.setImage(offer.getString("image"));
                                        if(offer.has("details")) {
                                            JSONObject details = offer.getJSONObject("details");
                                            shopByCategoryObject.setTitle(details.getString("name"));
                                        }


                                        listShopByCategoryObject.add(shopByCategoryObject);
                                    }

                                    if(listShops!=null && listShopByCategoryObject.size()> 0) {
                                        shopByCategoryListAdapter.refresh(listShopByCategoryObject,listShops);
                                    }
                                }
                                catch (Exception dd){}


                                try {

                                    if(transobj.has("position"))
                                    {
                                        layout_position_details.setVisibility(View.VISIBLE);
                                        recycler_positions.setVisibility(View.GONE);
                                        banner_pos_title.setVisibility(View.VISIBLE);
                                        if(transobj.getJSONObject("position").has("id"))
                                        {
                                            String position=transobj.getJSONObject("position").getString("id");
                                            if(Integer.parseInt(position)<= 5)//home banners
                                                banner_pos_title.setText(banner_pos_title.getText() +" - "+position);//custom name
                                            else
                                                banner_pos_title.setText(banner_pos_title.getText() +" - "+transobj.getJSONObject("position").getString("title"));

                                            Integer icon=-1;
                                            switch (position) {
                                                case "1":
                                                    icon=R.drawable.posi_1;
                                                    break;
                                                case "2":
                                                    icon=R.drawable.posi_2;
                                                    break;
                                                case "3":
                                                    icon=R.drawable.posi_3;
                                                    break;
                                                case "4":
                                                    icon=R.drawable.posi_4;
                                                    break;
                                                case "5":
                                                    icon=R.drawable.posi_5;
                                                    break;
                                                default:
                                                    icon=R.drawable.posi_6;
                                                    break;
                                            }


                                            Picasso.get()
                                                    .load(icon)
                                                    .error(R.drawable.ic_default_place_holder)
                                                    .placeholder(R.drawable.ic_default_place_holder)
                                                    .into(bannerAvailability);
                                        }
                                    }
                                }
                                catch (Exception ex){}

                                setData(promotionObject);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            mCustomDialog.dismiss();
                            showAddShopByCategory();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void setData(PromotionObject promotionObject) {
        if (promotionObject != null) {
            et_title.setText(promotionObject.getHeader());
            et_description.setText(promotionObject.getDescription());

            et_offer_name.setText(promotionObject.getOfferTile());
            offer_category_other.setText(promotionObject.getOfferDetails());

            tv_header1.setText(promotionObject.getHeader());
                if(promotionObject.getOfferTile()!=null && !promotionObject.getOfferTile().equals("null"))
                    tv_offer_tile.setText(promotionObject.getOfferTile());
                if(promotionObject.getOfferDetails()!=null && !promotionObject.getOfferDetails().equals("null"))
                    tv_offer_description.setText(promotionObject.getOfferDetails());

            if (promotionObject.getIcon() != null) {
                Picasso.get().load(promotionObject.getIcon())
                        .placeholder(R.drawable.no_image)
                        .into(banner_img);


                if(promotionObject.getcontent_type().equals("4"))
                {
                    ViewGroup.LayoutParams params = banner_img.getLayoutParams();
                    float factor = banner_img.getContext().getResources().getDisplayMetrics().density;
                    params.width =(int)( 200* factor);
                    banner_img.setLayoutParams(params);
                    profile_banner_layout.setVisibility(View.GONE);
                }
                else {
                    profile_banner_layout.setVisibility(View.VISIBLE);
                }
            }

            vendor_title_layout.setVisibility(View.GONE);

            try {
                if (promotionObject.getcontent_type().equals("4")) {
                    try {
                        //offers
                        BannerContentType = "4";
                        vendor_title_layout.setVisibility(View.GONE);
                    } catch (Exception ex) {

                    }
                }
                if (promotionObject.getcontent_type().equals("3")) {
                    //profile - default banner
                    BannerContentType = "3";
                }

                if(BannerContentType=="4")
                {
                    collapseAllBannerViews();
                    layout_banner_offers.setVisibility(View.VISIBLE);
                }
                //
            }
            catch (Exception ex){}

            try {

                String publishedON=promotionObject.getpublished_on().replace("/", "-");
                String expiry=promotionObject.getexpired_on().replace("/", "-");
                SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
                Date publishedDate=null,expiryDate=null;
                try {
                    publishedDate = mdformat.parse(publishedON);
                    expiryDate = mdformat.parse(expiry);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar cal = Calendar.getInstance();
                cal.setTime(publishedDate);
                int year = cal.get(Calendar.YEAR);
                int monthOfYear = cal.get(Calendar.MONTH);
                int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

                if ((monthOfYear+1) <= 9) {
                    frommonth = 0 + "" + (monthOfYear+1);
                } else {
                    frommonth = "" + (monthOfYear+1);
                }  if (dayOfMonth <= 9) {
                    fromday = 0 + "" + dayOfMonth;
                } else {
                    fromday = "" + dayOfMonth;
                }
                fromyear=""+year;
                startdate=fromyear+"-"+frommonth+"-"+fromday;
                start_date.setText(""+startdate);

                cal = Calendar.getInstance();
                cal.setTime(expiryDate);
                year = cal.get(Calendar.YEAR);
                monthOfYear = cal.get(Calendar.MONTH);
                dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

                if ((monthOfYear+1) <= 9) {
                    tomonth = 0 + "" + (monthOfYear+1);
                } else {
                    tomonth = "" + (monthOfYear+1);
                }  if (dayOfMonth <= 9) {
                    today = 0 + "" + dayOfMonth;
                } else {
                    today = "" + dayOfMonth;
                }
                toyear=""+year;
                enddate=toyear+"-"+tomonth+"-"+today;
                //   System.out.println("aaaaaaaa  enddate   "+enddate);

                // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                end_date.setText(""+enddate);

                chooseDate.setText(start_date.getText().toString().replace("-", "/")+" To \n"+end_date.getText().toString().replace("-", "/"));
            }
            catch (Exception ex){}

            if(promotionObject.getpromotion_banner_discount_type_id()!=null &&
                    !promotionObject.getpromotion_banner_discount_type_id().isEmpty() &&
                    !promotionObject.getpromotion_banner_discount_type_id().equals("null"))
            {
                if(listOfferPromotions!=null &&
                        listOfferPromotions.size()>0) {

                    for (int i = 0; i < listOfferPromotions.size(); i++) {
                        if(listOfferPromotions.get(i).getID().equals(promotionObject.getpromotion_banner_discount_type_id())) {
                            spinner_offer_promotion_type.setSelection(i+1);
                            break;
                        }
                    }
                }
            }
            if(promotionObject.getDiscount()!=null &&!promotionObject.getDiscount().isEmpty()&&
                    !promotionObject.getDiscount().equals("null"))
            {
                et_discount.setText(promotionObject.getDiscount());
            }
            if(promotionObject.getMaxOrderQty()!=null &&!promotionObject.getMaxOrderQty().isEmpty()&&
                    !promotionObject.getMaxOrderQty().equals("null"))
            {
                et_max_offer.setText(promotionObject.getMaxOrderQty());
            }

            if(promotionObject.getcontent_type()!=null &&!promotionObject.getcontent_type().isEmpty())
            {
                if(listBannerContentType!=null &&
                        listBannerContentType.size()>0) {

                    for (int i = 0; i < listBannerContentType.size(); i++) {
                        if(listBannerContentType.get(i).getID().equals(promotionObject.getcontent_type())) {
                            spinner_banner_type.setSelection(i+1);
                            break;
                        }
                    }
                }

            }
            if(positionsAdapter!=null && promotionObject.getpromotion_banner_position_id()!=null &&!promotionObject.getpromotion_banner_position_id().isEmpty())
            {

                if(bannerPositionObject!=null && bannerPositionObject.getBannerPositions()!=null &&
                        bannerPositionObject.getBannerPositions().size()>0) {

                    for (int i = 0; i < bannerPositionObject.getBannerPositions().size(); i++) {
                        if(bannerPositionObject.getBannerPositions().get(i).getID().equals(promotionObject.getpromotion_banner_position_id())) {
                            spinner_banner_positions.setSelection(i+1);
                            positionsAdapter.setSelectedIndex(i);
                            break;
                        }
                    }
                }
            }

            //To-Do:for offers
        }
    }

    private void getContentTypes() {
        //mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_CONTENT_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_CONTENT_TYPES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");
                                listBannerContentType=new ArrayList<>();


                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject transobj = dataobj.getJSONObject(i);

                                    bannerTypes.add(transobj.getString("name"));

                                    com.nextclick.crm.ShopNowModule.Models.BannerContentType
                                            bannerContentType=new BannerContentType();
                                    bannerContentType.setID(transobj.getString("id"));
                                    bannerContentType.setName(transobj.getString("name"));

                                    listBannerContentType.add(bannerContentType);
                                }

                                ArrayAdapter aaBannertypes = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,bannerTypes);
                                aaBannertypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_banner_type.setAdapter(aaBannertypes);


                                if(promotionObject!=null && promotionObject.getcontent_type()!=null &&!promotionObject.getcontent_type().isEmpty())
                                {
                                    if(listBannerContentType!=null &&
                                            listBannerContentType.size()>0) {

                                        for (int i = 0; i < listBannerContentType.size(); i++) {
                                            if(listBannerContentType.get(i).getID().equals(promotionObject.getcontent_type())) {
                                                spinner_banner_type.setSelection(i+1);
                                                break;
                                            }
                                        }
                                    }

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                        finally {
                            //mCustomDialog.dismiss();
                            showVendorsSelection();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void getPromotionBannerPositions() {
        //mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_BANNER_POSITIONS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_BANNER_POSITIONS response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                bannerPositionObject = new BannerPositionObject();
                                bannerPositionObject.setImage(dataobj.getString("image"));


                                List<BannerPositionIndex> list = new ArrayList<>();
                                BannerPositionIndex bannerPositionIndex = new BannerPositionIndex();



                                JSONObject positionObj = dataobj.getJSONObject("0");
                                bannerPositionIndex.setID(positionObj.getString("id"));
                                bannerPositionIndex.setTitle(positionObj.getString("title"));
                                requestPositions.add(bannerPositionIndex.getTitle());
                                list.add(bannerPositionIndex);

                                bannerPositionIndex = new BannerPositionIndex();
                                positionObj = dataobj.getJSONObject("1");
                                bannerPositionIndex.setID(positionObj.getString("id"));
                                bannerPositionIndex.setTitle(positionObj.getString("title"));
                                requestPositions.add(bannerPositionIndex.getTitle());
                                list.add(bannerPositionIndex);

                                bannerPositionIndex = new BannerPositionIndex();
                                positionObj = dataobj.getJSONObject("2");
                                bannerPositionIndex.setID(positionObj.getString("id"));
                                bannerPositionIndex.setTitle(positionObj.getString("title"));
                                requestPositions.add(bannerPositionIndex.getTitle());
                                list.add(bannerPositionIndex);

                                bannerPositionIndex = new BannerPositionIndex();
                                positionObj = dataobj.getJSONObject("3");
                                bannerPositionIndex.setID(positionObj.getString("id"));
                                bannerPositionIndex.setTitle(positionObj.getString("title"));
                                requestPositions.add(bannerPositionIndex.getTitle());
                                list.add(bannerPositionIndex);

                                bannerPositionIndex = new BannerPositionIndex();
                                positionObj = dataobj.getJSONObject("4");
                                bannerPositionIndex.setID(positionObj.getString("id"));
                                bannerPositionIndex.setTitle(positionObj.getString("title"));
                                requestPositions.add(bannerPositionIndex.getTitle());
                                list.add(bannerPositionIndex);


                                bannerPositionObject.setBannerPositions(list);

                                setBanners();

                                // bannerPositionObject.add(bannerPositionObject);
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                        finally {
                            //mCustomDialog.dismiss();
                            showAddShopByCategory();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void setBanners() {
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,requestPositions);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_banner_positions.setAdapter(aa);







        if(positionsAdapter!=null && promotionObject!=null && promotionObject.getpromotion_banner_position_id()!=null &&!promotionObject.getpromotion_banner_position_id().isEmpty())
        {

            if(bannerPositionObject!=null &&bannerPositionObject.getBannerPositions()!=null &&
                    bannerPositionObject.getBannerPositions().size()>0) {

                for (int i = 0; i < bannerPositionObject.getBannerPositions().size(); i++) {
                    if(bannerPositionObject.getBannerPositions().get(i).getID().equals(promotionObject.getpromotion_banner_position_id())) {
                        spinner_banner_positions.setSelection(i+1);
                        positionsAdapter.setSelectedIndex(i);
                        break;
                    }
                }
            }
        }
    }


    private ArrayList<States> statelist;
    private ArrayList<Districts> districtlist;
    private ArrayList<Constiuencies> constiencieslist;
    private String stateid="",districtid="",constitueid="";
    String constituency_id,district_id,state_id,state_name,distict_name,consti_name;
    Spinner spinner_state,spinner_distict,spinner_constitunesy;

    private void getStates() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");
                            if (status && http_code==200){
                                JSONArray responsearray=jsonObject.getJSONArray("data");
                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject1=responsearray.getJSONObject(i);
                                    States states=new States();
                                    states.setId(jsonObject1.getString("id"));
                                    states.setName(jsonObject1.getString("name"));
                                    states.setCreated_user_id(jsonObject1.getString("created_user_id"));
                                    states.setUpdated_user_id(jsonObject1.getString("updated_user_id"));
                                    states.setCreated_at(jsonObject1.getString("created_at"));
                                    states.setUpdated_at(jsonObject1.getString("updated_at"));
                                    states.setDeleted_at(jsonObject1.getString("deleted_at"));
                                    states.setStatus(jsonObject1.getString("status"));

                                    statelist.add(states);
                                }
                                ArrayList<String> statenames=new ArrayList<String>();
                                statenames.add("Select");
                                int stateid=0;
                                for (int k=0;k<statelist.size();k++){
                                    statenames.add(statelist.get(k).getName());
                                    System.out.println("aaaaaa state "+statelist.get(k).getId()+"  "+statelist.get(k).getName());
                                   /* if (isupdate==1){
                                        if (saveAddress.getState_id().equals(statelist.get(k).getId())){
                                            stateid=k;
                                        }
                                    }*/
                                }

                                ArrayAdapter ad = new ArrayAdapter(AddPromotionActivity.this,
                                        android.R.layout.simple_spinner_item, statenames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_state.setAdapter(ad);
                                try{
                                    if (state_id != null) {
                                        int spinnerPosition = ad.getPosition(state_name);
                                        spinner_state.setSelection(spinnerPosition);
                                        getDistricts(state_id);
                                    }
                                }catch (NullPointerException e){

                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(AddPromotionActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(AddPromotionActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getDistricts(String stateID) {
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States+stateID,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("districts");
                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);
                                    Districts districts=new Districts();
                                    districts.setId(jsonObject.getString("id"));
                                    districts.setName(jsonObject.getString("name"));
                                    districts.setState_id(jsonObject.getString("state_id"));
                                    districtlist.add(districts);
                                }
                                ArrayList<String> districtnames=new ArrayList<String>();
                                districtnames.add("Select");
                                int distictid=0;
                                System.out.println("aaaaaaa districtlist size "+districtlist.size());
                                for (int k=0;k<districtlist.size();k++){
                                    districtnames.add(districtlist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            distictid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(getApplicationContext(),
                                        android.R.layout.simple_spinner_item, districtnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_distict.setAdapter(ad);
                                try{
                                    if (district_id != null) {
                                        int spinnerPosition = ad.getPosition(distict_name);
                                        spinner_distict.setSelection(spinnerPosition);
                                        getConstituencis(state_id,district_id);
                                    }
                                }catch (NullPointerException e){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(AddPromotionActivity.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getConstituencis(String stateID,String districtid) {
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.URL_States+stateID+"/"+districtid,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        System.out.println("aaaaaaa response const  "+ response);
                        try {
                            JSONObject jsonObject1=new JSONObject(response);
                            boolean status=jsonObject1.getBoolean("status");
                            int http_code=jsonObject1.getInt("http_code");
                            if (status && http_code==200){

                                JSONArray responsearray = jsonObject1.getJSONObject("data").getJSONArray("constituenceis");

                                System.out.println("aaaaaaaaaa   sucess " + response);
                                for (int i=0;i<responsearray.length();i++){
                                    JSONObject jsonObject=responsearray.getJSONObject(i);

                                    Constiuencies constiuencies=new Constiuencies();
                                    constiuencies.setId(jsonObject.getString("id"));
                                    constiuencies.setDistrict_id(jsonObject.getString("district_id"));
                                    constiuencies.setName(jsonObject.getString("name"));

                                    constiencieslist.add(constiuencies);
                                }
                                ArrayList<String> constnames=new ArrayList<String>();
                                constnames.add("Select");
                                int conid=0;
                                for (int k=0;k<constiencieslist.size();k++){
                                    constnames.add(constiencieslist.get(k).getName());
                                    /*if (isupdate==1){
                                        if (saveAddress.getDistrict_id().equals(districtlist.get(k).getId())){
                                            conid=k;
                                        }
                                    }*/
                                }
                                ArrayAdapter ad = new ArrayAdapter(getApplicationContext(),
                                        android.R.layout.simple_spinner_item, constnames);
                                ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_constitunesy.setAdapter(ad);
                                try{
                                    if (consti_name != null) {
                                        int spinnerPosition = ad.getPosition(consti_name);
                                        spinner_constitunesy.setSelection(spinnerPosition);
                                    }
                                }catch (NullPointerException e){

                                }

                            }

                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(getApplicationContext(), ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void upLoadImage(ImageView upload_image, ShopByCategoryObject shopByCategoryObject) {
        this.selectedCategoryObject=shopByCategoryObject;
        catUploadImage=upload_image;
        selectImage();
    }

    private void getOfferPromotions() {

        listOfferPromotions = new ArrayList<>();


        /*String[] staticTypes=new String[]{"Percent of product price discount","Fixed amount discount",
        "Buy 1 get 1 free","Buy X get Y free",
        "Buy X get Y free(with optional discount amount)","Buy X get Y or Z free"};

        for (int i = 0; i < staticTypes.length; i++) {

            com.nextclick.crm.ShopNowModule.Models.BannerContentType
                    bannerContentType = new BannerContentType();

            bannerContentType.setID(""+(i+1));
            bannerContentType.setName(staticTypes[i]);

            offerTypes.add(bannerContentType.getName());

            listOfferPromotions.add(bannerContentType);
        }
        ArrayAdapter aaBannertypes = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_spinner_item,offerTypes);
        aaBannertypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_offer_promotion_type.setAdapter(aaBannertypes);

        */


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_DISCOUNT_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_DISCOUNT_TYPES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");
                                listOfferPromotions = new ArrayList<>();


                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject transobj = dataobj.getJSONObject(i);

                                    offerTypes.add(transobj.getString("name"));

                                    com.nextclick.crm.ShopNowModule.Models.BannerContentType
                                            bannerContentType = new BannerContentType();
                                    bannerContentType.setID(transobj.getString("id"));
                                    bannerContentType.setName(transobj.getString("name"));

                                    listOfferPromotions.add(bannerContentType);
                                }

                                ArrayAdapter aaBannertypes = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, offerTypes);
                                aaBannertypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_offer_promotion_type.setAdapter(aaBannertypes);


                                if (promotionObject != null && promotionObject.getpromotion_banner_discount_type_id() != null && !promotionObject.getpromotion_banner_discount_type_id().isEmpty() &&
                                        !promotionObject.getpromotion_banner_discount_type_id().equals("null")) {
                                    if (listOfferPromotions != null &&
                                            listOfferPromotions.size() > 0) {

                                        for (int i = 0; i < listOfferPromotions.size(); i++) {
                                            if (listOfferPromotions.get(i).getID().equals(promotionObject.getpromotion_banner_discount_type_id())) {
                                                spinner_offer_promotion_type.setSelection(i + 1);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            //mCustomDialog.dismiss();
                            showVendorsSelection();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getBannersAvailability(String publish_date, String constituency_id) {

        listBannerAvailability = new ArrayList<>();
        List<BannerAvailability> listLocalBannerAvailability = new ArrayList<>();

        String URL =Config.GET_BANNER_AVAILABILITY+"?constituency_id="+constituency_id+"&publish_date="+publish_date;

        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_BANNER_AVAILABILITY response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");

                                for (int i = 0; i < dataobj.length(); i++) {
                                    BannerAvailability bannerAvailability = new BannerAvailability();
                                    JSONObject transobj = dataobj.getJSONObject(i);
                                    bannerAvailability.setID(transobj.getString("id"));

                                    banner_pos_title.setVisibility(View.VISIBLE);

                                    // bannerAvailability.setName(transobj.getString("title"));// 6
                                    if (Integer.parseInt(bannerAvailability.getID()) <= 5)//home banners
                                        bannerAvailability.setName("" + bannerAvailability.getID());//custom name
                                    else
                                        bannerAvailability.setName(transobj.getString("title"));

                                    bannerAvailability.setBanners_limit(transobj.getInt("banners_limit"));//6
                                    bannerAvailability.setFilled_positions(transobj.getInt("filled_positions"));//2
                                    bannerAvailability.setNext_available_date(transobj.getString("next_available_date"));//"2021-07-16"

                                    bannerAvailability.setDate(publish_date, end_date.getText().toString().replace("-", "/"));

                                    switch (bannerAvailability.getID()) {
                                        case "1":
                                            bannerAvailability.setIcon(R.drawable.posi_1);
                                            break;
                                        case "2":
                                            bannerAvailability.setIcon(R.drawable.posi_2);
                                            break;
                                        case "3":
                                            bannerAvailability.setIcon(R.drawable.posi_3);
                                            break;
                                        case "4":
                                            bannerAvailability.setIcon(R.drawable.posi_4);
                                            break;
                                        case "5":
                                            bannerAvailability.setIcon(R.drawable.posi_5);
                                            break;
                                        default:
                                            bannerAvailability.setIcon(R.drawable.posi_6);
                                            break;
                                    }

                                    if (transobj.has("per_day_charge"))
                                        bannerAvailability.setPrice(transobj.getString("per_day_charge"));// 6

                                    if (BannerContentType.equals("4")) {
                                        //we have to show offer position related layout and disable all

                                        if (bannerAvailability.getID().equals("4"))
                                            listLocalBannerAvailability.add(bannerAvailability);
                                    } else if (!bannerAvailability.getID().equals("4"))
                                        listLocalBannerAvailability.add(bannerAvailability);

                                    listBannerAvailability.add(bannerAvailability);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            mCustomDialog.dismiss();
                           // selectBannerPosition();

                            positionsAdapter = new PositionsAdapter(AddPromotionActivity.this,listLocalBannerAvailability);
                            recycler_positions.setAdapter(positionsAdapter);

                            banner_slot_info.setVisibility(View.VISIBLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                banner_slot_info.setVisibility(View.GONE);
              //  Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void selectBannerPosition()
    {
        if(listBannerAvailability==null || listBannerAvailability.size()==0 || listBannerAvailability.size() < 6)
        {
            Toast.makeText(mContext, "Banners slots are not available at this moments, please try again later.", Toast.LENGTH_SHORT).show();
        }

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_banner_single_slot_selection);
        Button applyButton = dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);
        RadioGroup radio_group = dialog.findViewById(R.id.radio_group);
        TextView tv_slot = dialog.findViewById(R.id.tv_slot);
        TextView  tv_slots_remaining= dialog.findViewById(R.id.tv_slots_remaining);
        TextView tv_next_availability = dialog.findViewById(R.id.tv_next_availability);
        TextView tv_price = dialog.findViewById(R.id.tv_price);

        int selectedPosition=0;
        if(BannerPosition!=null) {
            selectedPosition = Integer.parseInt(BannerPosition) - 1;
        }

        Log.i("selectedPosition","selectedPosition "+selectedPosition);

        BannerAvailability selectedBanner = listBannerAvailability.get(selectedPosition);
        tv_slot.setText("Position - "+selectedBanner.getName());//selectedPosition


        int filledPos=selectedBanner.getFilled_positions();
        if(filledPos>selectedBanner.getBanners_limit())
            filledPos=selectedBanner.getBanners_limit();

        int slotsRemaining=selectedBanner.getBanners_limit()-filledPos;
        tv_slots_remaining.setText(slotsRemaining+" Slots left");
        if(slotsRemaining<=0)
        {
            tv_slots_remaining.setText("0 Slots left");
            tv_next_availability.setText("Next available slot: "+selectedBanner.getNext_available_date());
        }

        if(filledPos>0) {
            for (int i = 0; i < filledPos; i++) {
                radio_group.getChildAt(i).setEnabled(false);
            }
        }
        tv_price.setText("Price : "+selectedBanner.getPrice()+ " Rs / Day");

        selectedSlotID=-1;
        radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    View radioButton = group.findViewById(checkedId);
                    selectedSlotID=group.indexOfChild(radioButton);
                }
                catch (Exception ex)
                {
                    Log.e("Radiobutton selection",ex.getLocalizedMessage());
                }
            }
        });


        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(selectedSlotID >=0) {
                    makePayment(selectedSlotID,selectedBanner.getPrice());
                }
                else
                    Toast.makeText(mContext, "Please select the slot position.", Toast.LENGTH_SHORT).show();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private  void makePayment(Integer selectedSlotID ,String per_day_charge) {

        Long duration = getTimeLeft(start_date.getText().toString().replace("-", "/"), end_date.getText().toString().replace("-", "/"));
        if (duration <= 0)
            duration = 0L;

        amount = Double.parseDouble(per_day_charge) * (duration + 1);
        if (disablePayment) {
            createorUpdatePromotion("", "");
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            alertDialogBuilder.setTitle("Razorpay");
            alertDialogBuilder
                    .setMessage("Do You Want To Procced with razorpay payment of Rs/-" + amount)
                    .setCancelable(false)
                    .setPositiveButton(R.string.pay, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            razorPayPayment("" + amount);
                        }

                    })
                   /* .setNegativeButton(R.string.pay_later, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            createorUpdatePromotion("", "");
                        }
                    })*/;
         /*   if (Utility.enable_Pay_Later) {
                alertDialogBuilder.setNegativeButton(R.string.pay_later, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        createorUpdatePromotion("", "");
                    }
                });
            }*/
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setCanceledOnTouchOutside(true);
            alertDialog.show();
        }
    }


    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        co.setImage(R.mipmap.ic_launcher_round);
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            if(Utility.Skip_Final_PAYMENT)
                orderRequest.put("amount", 100); //totatlAmount * 100
            else
             orderRequest.put("amount", ""+(Double.parseDouble(totatlAmount)*100)); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image",R.mipmap.ic_launcher_round);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open(activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
            // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("razorpay  JSONException "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
            // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("razorpay  Exception "+e.getMessage());
        }


    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        System.out.println("razorpay  data sucess  " + s + paymentData.toString());
        createorUpdatePromotion(paymentData.getPaymentId(), "" + amount);
    }
    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try{
            Toast.makeText(this, "Payment has been failed with error: "+paymentData.toString(), Toast.LENGTH_SHORT).show();
        }catch (NullPointerException e){

        }
        System.out.println("razorpay  payment  error  "+s);
    }


    public long getTimeLeft(String startdate,String enddate) {
        long elapsedDays =0;
        try {

            SimpleDateFormat mdformat = new SimpleDateFormat("yyyy/MM/dd");
            Date startDate = null, endDate = null;
            startDate = mdformat.parse(startdate);
            endDate = mdformat.parse(enddate);


            //milliseconds
            long different = endDate.getTime() - startDate.getTime();

            System.out.println("startDate : " + startDate);
            System.out.println("endDate : " + endDate);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            elapsedDays = different / daysInMilli;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elapsedDays;
    }

    public void onPositionSelected(BannerAvailability sliderItem) {
        BannerPosition=sliderItem.getID();
        selectedSlotID=sliderItem.getSelectedSlotID();
        selectedBanner=sliderItem;
    }

    public void changeDate(String startDate, String endDate) {

        start_date_header.setText(startDate);
        end_date_header.setText(endDate);

        this.startdate = startDate;
        this.start_date.setText(startdate);
        this.enddate = endDate;
        this.end_date.setText(enddate);
        chooseDate.setText(startDate + " To  " + endDate);
        BannerPosition="";
        if (constitueid != null && !constitueid.isEmpty())
            getBannersAvailability(startdate, constitueid);
        else
            banner_pos_title.setVisibility(View.GONE);
    }
    private CalendarConstraints.Builder limitRange() {

        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();
        calendarStart.add(Calendar.DATE,-1);
        calendarEnd.add(Calendar.YEAR,1);


        long minDate = calendarStart.getTimeInMillis();
        long maxDate = calendarEnd.getTimeInMillis();


        constraintsBuilderRange.setStart(minDate);
         constraintsBuilderRange.setEnd(maxDate);

        //constraintsBuilderRange.setValidator(new RangeValidator(minDate,maxDate));

        ArrayList<CalendarConstraints.DateValidator> validators= new ArrayList<>();
        validators.add(DateValidatorPointForward.from(minDate));
        validators.add(DateValidatorPointBackward.before(maxDate));
        constraintsBuilderRange.setValidator(CompositeDateValidator.allOf(validators));

        return constraintsBuilderRange;
    }

    private void openSubCategory() {
        if (listShops != null && listShops.size() > 0) {
            Dialog dialog = new Dialog(this);

            View contentView = View.inflate(mContext, R.layout.select_sub_categories, null);
            //context = contentView.getContext();
            dialog.setContentView(contentView);


            RelativeLayout layout_root= dialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels;
            layout_root.setLayoutParams(layoutParams);

            TextView tv_error=dialog.findViewById(R.id.tv_error);
            RecyclerView recyclerView=dialog.findViewById(R.id.recyclerView_shopBycategory);
            CheckBox chk_select_all = dialog.findViewById(R.id.chk_select_all);

            Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = dialog.findViewById(R.id.closeButton);
            TextInputEditText other_value = dialog.findViewById(R.id.other_value);


            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            CategoryModelAdapter mAdapter = new CategoryModelAdapter(listShops);
            recyclerView.setAdapter(mAdapter);

            chk_select_all.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (chk_select_all.isChecked()) {

                        for (ShopCategoryObject model : listShops) {
                            model.setSelected(true);
                        }
                    } else {

                        for (ShopCategoryObject model : listShops) {
                            model.setSelected(false);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }
            });
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String SelectOfferCategory="";
                    for (ShopCategoryObject model : listShops) {
                        if (model.getSelected())
                            SelectOfferCategory += model.getName() + ",";
                    }

                    if (SelectOfferCategory !=null && SelectOfferCategory.length() > 0) {

                        if(other_value.getText().toString() !=null && other_value.getText().toString() .length()> 0)
                          SelectOfferCategory +=other_value.getText().toString();
                        else if(SelectOfferCategory.endsWith(","))
                            SelectOfferCategory=SelectOfferCategory.substring(0,SelectOfferCategory.length()-1);

                        offer_category_other.setText(SelectOfferCategory);
                        tv_offer_description.setText(SelectOfferCategory);
                        dialog.dismiss();
                    }
                    else if(other_value.getText().toString() !=null && other_value.getText().toString() .length()> 0) {
                        offer_category_other.setText(other_value.getText().toString());
                        tv_offer_description.setText(other_value.getText().toString());
                        dialog.dismiss();
                    }
                    else {
                        tv_error.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tv_error.setVisibility(View.GONE);
                            }
                        },2000);
                    }
                }
            });

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            //window.setGravity(Gravity.BOTTOM);

            dialog.show();
        }


    }

}
