package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.dialogs.DatePickerResponse;
import com.nextclick.crm.dialogs.MyDatePickerDialog;
import com.nextclick.crm.reports.MonthlyReportObject;
import com.nextclick.crm.reports.OrderReportObject;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;


public class ReportsFragment extends Fragment implements View.OnClickListener, DatePickerResponse {

    // variable for our bar chart
    BarChart monthlyBarChart,weeklyBarChart;
    PieChart pieChart;

    // array list for storing entries.
    ArrayList barEntriesArrayList;
    List<PieEntry> pieEntriesList;

    String[] info = {"received", "accepted", "You order has been preparing", "Out for delivery", "Rejected", "Cancelled"};

    String SelectedPaymentType= CATERORY2;
    public static final String CATERORY1 = "CATERORY1";
    public static final String CATERORY2 = "CATERORY2";
    private TextView start_date,end_date,tv_get;
    ImageView order_back_image_;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    LinearLayout layout_report1,layout_report2;

    public ReportsFragment() {

    }
    public ReportsFragment(Context mContext) {
        this.mContext=mContext;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(mContext);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_reports, container, false);


        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to reportss activity");
        }
        monthlyReports = new ArrayList<>();
        weekelyReports = new ArrayList<>();

        monthlyBarChart= root.findViewById(R.id.monthlyBarChart);
        weeklyBarChart= root.findViewById(R.id.weeklyBarChart);
        pieChart = root.findViewById(R.id.piechart);

      //  initPieChart();
        //intBarchart();

        start_date = root.findViewById(R.id.start_date);
        end_date = root.findViewById(R.id.end_date);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        tv_get= root.findViewById(R.id.tv_get);
        tv_get.setOnClickListener(this);

        layout_report1= root.findViewById(R.id.layout_report1);
        layout_report2= root.findViewById(R.id.layout_report2);

        TextView tv_header1 = root.findViewById(R.id.tv_header1);
        TextView tv_header2 = root.findViewById(R.id.tv_header2);
        LinearLayout layout_header1= root.findViewById(R.id.layout_header1);
        LinearLayout  layout_header2= root.findViewById(R.id.layout_header2);
        layout_header1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= CATERORY2)
                {
                    SelectedPaymentType= CATERORY2;
                    layout_header1.setBackground(getResources().getDrawable(R.drawable.border_set));
                    layout_header2.setBackground(null);
                    //tv_header1.setTextColor(0XFFFFFFFF);
                  // tv_header2.setTextColor(0XFF000000);

                    layout_report2.setVisibility(View.GONE);
                    layout_report1.setVisibility(View.VISIBLE);

                }
            }
        });
        layout_header2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= CATERORY1)
                {
                    SelectedPaymentType= CATERORY1;
                    layout_header2.setBackground(getResources().getDrawable(R.drawable.border_set));
                    layout_header1.setBackground(null);
                    //tv_header2.setTextColor(0XFFFFFFFF);
                   // tv_header1.setTextColor(0XFF000000);

                    layout_report2.setVisibility(View.VISIBLE);
                    layout_report1.setVisibility(View.GONE);
                }
            }
        });

        getReports("2021-04-01","2021-07-08");
        return root;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_get:
                if (start_date.getText().toString() == null || start_date.getText().equals(getString(R.string.StartDate))
                        || start_date.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please enter the start date.", Toast.LENGTH_SHORT).show();
                }
                else if (end_date.getText().toString() == null || end_date.getText().equals(getString(R.string.EndDate))
                        || end_date.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please enter the end date.", Toast.LENGTH_SHORT).show();
                } else
                    getReports(startdate, enddate);

               /* barChart.clearValues();
                barChart.clear();
                pieChart.clearValues();
                pieChart.clear();

                barEntriesArrayList.add(new BarEntry(7f, 1));
                barDataSet = new BarDataSet(barEntriesArrayList, "Monthly Sales");
                barData = new BarData(barDataSet);
                barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
                barChart.setScaleEnabled(false);//disable zoom
                barChart.setData(barData);

                pieEntriesList.get(pieEntriesList.size()-1).setX(10f);
                pieEntriesList.add(new PieEntry(20f, "July"));
                PieDataSet dataSet = new PieDataSet(pieEntriesList, "");
                dataSet.setSliceSpace(3f);
                PieData data = new PieData(dataSet);
                data.setValueFormatter(new PercentFormatter());
                pieChart.setData(data);

                int[] colors = {Color.RED, Color.rgb(255, 128, 0), Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};
                dataSet.setColors(ColorTemplate.createColors(colors));
                pieChart.setTransparentCircleRadius(30f);
                pieChart.setHoleRadius(30f);
                data.setValueTextSize(13f);
                data.setValueTextColor(Color.DKGRAY);
                pieChart.animateXY(1500, 1500);

                dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
                dataSet.setValueLinePart1OffsetPercentage(80.f);
                dataSet.setValueLinePart1Length(1.2f);
                dataSet.setValueLinePart2Length(0.4f);*/

                break;
            case R.id.start_date:
                datepicker(0);
                break;
            case R.id.end_date:
                if (start_date.getText().toString() == null || start_date.getText().equals(getString(R.string.StartDate))
                        || start_date.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Please enter the start date.", Toast.LENGTH_SHORT).show();
                } else {
                    datepicker(1);
                }
        }
    }

    List<MonthlyReportObject> monthlyReports;
    List<MonthlyReportObject> weekelyReports;
    private void getReports(String startdate, String enddate) {
        monthlyReports.clear();
        weekelyReports.clear();
        //startdate="2021-01-01";
        //enddate="2021-05-20";
        String URL=Config.GET_REPORTS+"?start_date="+startdate+"&end_date="+enddate;
        System.out.println("GET_REPORTS request url "+URL);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URL,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status) {
                                JSONObject dataObject = jsonObject.getJSONObject("data");

                                try {
                                    JSONArray mothly_reports = dataObject.getJSONArray("mothly_reports");
                                    for (int i = 0; i < mothly_reports.length(); i++) {
                                        JSONObject reportObject = mothly_reports.getJSONObject(i);

                                        MonthlyReportObject month = new MonthlyReportObject();
                                        month.setMonth(reportObject.getString("m"));//int
                                        //month.setMonth(reportObject.getString("month"));
                                        month.setYear(reportObject.getString("year"));
                                        month.setAmount(reportObject.getString("amount"));

                                        monthlyReports.add(month);
                                    }
                                } catch (Exception ex) {
                                }
                                try {
                                    JSONArray weekly_reports = dataObject.getJSONArray("weekly_reports");
                                    for (int i = 0; i < weekly_reports.length(); i++) {
                                        JSONObject reportObject = weekly_reports.getJSONObject(i);
                                        MonthlyReportObject month = new MonthlyReportObject();
                                        month.setMonth(reportObject.getString("month"));
                                        month.setWeek(reportObject.getString("week"));
                                        month.setAmount(reportObject.getString("amount"));

                                        weekelyReports.add(month);
                                    }
                                } catch (Exception ex) {
                                }
                                try {
                                    JSONObject reportObject = dataObject.getJSONObject("orders_count_by_statuses");

                                    OrderReportObject orderReportObject = new OrderReportObject();
                                    orderReportObject.setReceived(reportObject.getString("received"));
                                    orderReportObject.setAccepted(reportObject.getString("accepted"));
                                    orderReportObject.setPreparing(reportObject.getString("You order has been preparing"));
                                    orderReportObject.setOutForDelivery(reportObject.getString("Out for delivery"));
                                    orderReportObject.setRejected(reportObject.getString("Rejected"));
                                    orderReportObject.setCancelled(reportObject.getString("Cancelled"));

                                    initPieChart(orderReportObject);

                                } catch (Exception ex) {
                                }
                            }

                        } catch (JSONException exx) {
                            //  Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + exx);
                        }
                        finally {
                            mCustomDialog.dismiss();
                            showSalesdata();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
                showSalesdata();
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void showSalesdata() {

        if (weekelyReports.size() > 0 && weekelyReports.size() > monthlyReports.size())
            intBarchart(weekelyReports,false);
        else
            weeklyBarChart.setNoDataText("No weekly sales available");
        if (monthlyReports.size() > 0)
            intBarchart(monthlyReports,true);
        else
            monthlyBarChart.setNoDataText("No monthly sales available");
    }

    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="",startdate="",enddate="";
    String year,month,day;
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        start_date.setText(day+"-"+month+"-"+year);
        end_date.setText(day+"-"+month+"-"+year);
        startdate=year+"-"+month+"-"+day;
        enddate=year+"-"+month+"-"+day;

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //  System.out.println("aaaaaa currentdate  "+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+"  "+c.get(Calendar.YEAR));
        }else {
            c = Calendar.getInstance();
           // c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    startdate=fromyear+"-"+frommonth+"-"+fromday;
                    //   System.out.println("aaaaaaaa  startdate   "+startdate);
                    //start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    start_date.setText(""+startdate);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    enddate=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+enddate);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    private void intBarchart(List<MonthlyReportObject>  reports,Boolean isMonthlyReport) {

        BarChart barChart =isMonthlyReport?monthlyBarChart:weeklyBarChart;
        if(barChart!=null) {
            if(barChart.getBarData()!=null)
                barChart.clearValues();
            barChart.clear();
        }

        barEntriesArrayList = new ArrayList<>();
        if(reports==null) {
            //testing
            barEntriesArrayList.add(new BarEntry(1f, 4));
            barEntriesArrayList.add(new BarEntry(2f, 6));
            barEntriesArrayList.add(new BarEntry(3f, 8));
            barEntriesArrayList.add(new BarEntry(4f, 2));
            barEntriesArrayList.add(new BarEntry(5f, 4));
            barEntriesArrayList.add(new BarEntry(6f, 1));
        }
        else
        {
            for(int i=0; i<reports.size();i++) {
                if (isMonthlyReport)
                    barEntriesArrayList.add(new BarEntry(Float.parseFloat(reports.get(i).getMonth()), Float.parseFloat(reports.get(i).getAmount())));//Float.parseFloat(reports.get(i).getYear()
                else
                    barEntriesArrayList.add(new BarEntry(Float.parseFloat(reports.get(i).getWeek()), Float.parseFloat(reports.get(i).getAmount())));
            }
        }

        BarDataSet barDataSet = new BarDataSet(barEntriesArrayList, isMonthlyReport?"Monthly Sales":"Weekly Sales");
        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);
        barChart.getDescription().setEnabled(false);

        barChart.setScaleEnabled(false);//disable zoom

        setDesign(barChart);
    }

    private void setDesign(BarChart severityBarChart) {
        severityBarChart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        severityBarChart.setMaxVisibleValueCount(4);
        severityBarChart.getXAxis().setDrawGridLines(false);
        // scaling can now only be done on x- and y-axis separately
        severityBarChart.setPinchZoom(false);

        severityBarChart.setDrawBarShadow(false);
        severityBarChart.setDrawGridBackground(false);

        XAxis xAxis = severityBarChart.getXAxis();
        xAxis.setDrawGridLines(false);

        severityBarChart.getAxisLeft().setDrawGridLines(false);
        severityBarChart.getAxisRight().setDrawGridLines(false);
        severityBarChart.getAxisRight().setEnabled(false);
        severityBarChart.getAxisLeft().setEnabled(true);
        severityBarChart.getXAxis().setDrawGridLines(false);
        // add a nice and smooth animation
        severityBarChart.animateY(1500);


        severityBarChart.getLegend().setEnabled(false);

        severityBarChart.getAxisRight().setDrawLabels(false);
        severityBarChart.getAxisLeft().setDrawLabels(true);
        severityBarChart.setTouchEnabled(false);
        severityBarChart.setDoubleTapToZoomEnabled(false);
        severityBarChart.getXAxis().setEnabled(true);
        severityBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        severityBarChart.invalidate();
    }

    private void initPieChart(OrderReportObject orderReportObject) {

        if(pieChart!=null) {
            if(pieChart.getData()!=null)
                pieChart.clearValues();
            pieChart.clear();
            pieChart.invalidate();
        }

        pieChart.setUsePercentValues(true);
        pieChart.setExtraOffsets(25, 5, 25, 0);
        pieChart.setCenterText("Vendor Order Statuses");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);
        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(30f);
        pieChart.setEntryLabelColor(Color.BLACK);

        pieEntriesList = new ArrayList<>();
        if(orderReportObject==null) {
            //test
            pieEntriesList.add(new PieEntry(10f, info[0]));
            pieEntriesList.add(new PieEntry(10f, info[1]));
            pieEntriesList.add(new PieEntry(10f, info[2]));
            pieEntriesList.add(new PieEntry(10f, info[3]));
            pieEntriesList.add(new PieEntry(30f, info[4]));
            pieEntriesList.add(new PieEntry(30f, info[5]));
        }
        else
        {
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getReceived()), info[0]));
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getAccepted()), info[1]));
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getPreparing()), info[2]));
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getOutForDelivery()), info[3]));
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getRejected()), info[4]));
            pieEntriesList.add(new PieEntry(Float.parseFloat(orderReportObject.getCancelled()), info[5]));
        }

        PieDataSet dataSet = new PieDataSet(pieEntriesList, "");
        dataSet.setSliceSpace(3f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        pieChart.setData(data);
        pieChart.invalidate();

        pieChart.setEntryLabelTextSize(13);
        Description d = new Description();
        d.setTextSize(16);
        d.setPosition(65, 50);
        d.setTextAlign(Paint.Align.LEFT);
        d.setText("");//Orders
        pieChart.setDescription(d);

        int[] colors = {Color.RED, Color.rgb(255, 128, 0), Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};
        dataSet.setColors(ColorTemplate.createColors(colors));
        pieChart.setTransparentCircleRadius(30f);
        pieChart.setHoleRadius(30f);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);
        pieChart.animateXY(1500, 1500);

        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(1.2f);
        dataSet.setValueLinePart2Length(0.4f);

        //dataSet.setValueLineWidth(0);//to disalbe line

    }

    public void doAction(String id) {
        switch (id)
        {
            case "1801":
                SelectedPaymentType= CATERORY2;
                layout_report2.setVisibility(View.GONE);
                layout_report1.setVisibility(View.VISIBLE);
                break;
            case "1802":
                SelectedPaymentType= CATERORY1;
                layout_report2.setVisibility(View.VISIBLE);
                layout_report1.setVisibility(View.GONE);
                break;
        }
    }
    public void onCalendarAction(FragmentManager supportFragmentManager) {
        MyDatePickerDialog dialog=new MyDatePickerDialog();
     //   dialog.showDialog(supportFragmentManager,this);
        dialog.showStartAndEndDatePickers(mContext,this);
    }

    @Override
    public Context getActivityContext() {
        return mContext;
    }

    @Override
    public void onDateSelected(String startDate, String endDate) {
        startdate =startDate;
        start_date.setText(startDate);
        endDate =endDate;
        end_date.setText(endDate);
        getReports(startdate, enddate);
    }
}