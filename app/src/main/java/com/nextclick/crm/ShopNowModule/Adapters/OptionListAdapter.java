package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class OptionListAdapter extends RecyclerView.Adapter<OptionListAdapter.ViewHolder> {

    ArrayList<String> data;
    Context context;
    int productIndex;

    public OptionListAdapter(Context activity, ArrayList<String> optionlist, int productIndex) {
        this.context = activity;
        this.data = optionlist;
        this.productIndex=productIndex;
    }

    @NonNull
    @Override
    public OptionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.dynamic_layout_add_option, parent, false);
        return new OptionListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionListAdapter.ViewHolder holder, int position) {

        if (position==0){
            holder.img_add_option.setVisibility(View.VISIBLE);
            holder.img_remove_option.setVisibility(View.GONE);
        }else {
            holder.img_remove_option.setVisibility(View.VISIBLE);
            holder.img_add_option.setVisibility(View.GONE);
        }

        holder.img_remove_option.setVisibility(View.GONE);
        holder.img_add_option.setVisibility(View.GONE);

        holder.tv_option.setText(data.get(position));

        holder.img_add_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(data.get(data.size()-1).isEmpty())
                {
                    UIMsgs.showToast(context, "Please enter the previous option details.");
                }
                else {
                    setrefresh(((ProductAddOrUpdate) context).addOptionToProduct(productIndex));
                }
            }
        });
        holder.img_remove_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setrefresh(((ProductAddOrUpdate)context).removeOptionFromProduct(productIndex,position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setrefresh(ArrayList<String> data) {
        this.data=data;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_option;
        ImageView img_add_option,img_remove_option;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_option = itemView.findViewById(R.id.et_input_option);
            img_add_option = itemView.findViewById(R.id.img_add_option);
            img_remove_option = itemView.findViewById(R.id.img_remove_option);

            tv_option.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    data.set(getAdapterPosition(),tv_option.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }
}
