package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryObject;
import com.nextclick.crm.ShopNowModule.Models.ShopCategoryObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ShopByCategoryListAdapter  extends RecyclerView.Adapter<ShopByCategoryListAdapter.ViewHolder> {


    private final AddPromotionActivity activity;
    private Boolean IsViewPromotion=false;
    private List<ShopByCategoryObject> data;
    private final Context context;
    private List<ShopCategoryObject> listShops;
    private final ArrayList<String> categoryNames = new ArrayList<>();
    public static String bannerImageBase64 = "";
    int checkupdate=0;
    private final CustomDialog mCustomDialog;
    private final PreferenceManager preferenceManager;

    public ShopByCategoryListAdapter(AddPromotionActivity addPromotionActivity, List<ShopByCategoryObject> data,Boolean IsViewPromotion) {
        this.context = addPromotionActivity;
        this.activity=addPromotionActivity;
        this.data = data;
        this.IsViewPromotion=IsViewPromotion;

        preferenceManager=new PreferenceManager(context);
        mCustomDialog=new CustomDialog(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_add_shop_category, parent, false);
        return new ShopByCategoryListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        ArrayAdapter aaBannertypes = new ArrayAdapter(context, android.R.layout.simple_spinner_item, categoryNames);
        aaBannertypes.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spinner_banner_type.setAdapter(aaBannertypes);



        if(data.get(position).getImage()!=null && !data.get(position).getImage().startsWith("http")) {
            try {
                byte[] decodedString = Base64.decode(data.get(position).getImage(), Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                holder.upload_image.setImageBitmap(decodedByte);

            } catch (Exception ex) {

            }
        }
        else
        {
            Picasso.get()
                    .load(data.get(position).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.upload_image);
        }


        String selectedID=data.get(position).getID();
        if(selectedID!=null &&!selectedID.isEmpty())
        {
            try {
                if(IsViewPromotion)
                {
                    holder.spinner_banner_type.setVisibility(View.GONE);
                    holder.tv_Sub_category.setVisibility(View.VISIBLE);
                    holder.tv_Sub_category.setText(data.get(position).getTitle());
                }
                else {
                    for (int i = 0; i < listShops.size(); i++) {
                        if (listShops.get(i).getID().equals(selectedID)) {
                            holder.spinner_banner_type.setSelection(i);
                            break;
                        }
                    }
                }
            }catch (Exception ex)
            {

            }

        }

        if(position>=1)
            holder.img_minus.setVisibility(View.VISIBLE);


        if(IsViewPromotion)
        {
            holder.img_minus.setVisibility(View.GONE);
            holder.img_add.setVisibility(View.GONE);
            holder.spinner_banner_type.setEnabled(false);
        }
        else {

            if(position== data.size()-1)
                holder.img_add.setVisibility(View.VISIBLE);
            else
                holder.img_add.setVisibility(View.GONE);

            holder.upload_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //selectImage();
                    //data.get(position).setImage(bannerImageBase64);

                    activity.upLoadImage(holder.upload_image, data.get(position));

                }
            });

            holder.img_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.remove(position);
                    notifyDataSetChanged();
                }
            });


            holder.spinner_banner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                    data.get(position).setID(listShops.get(position1).getID());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void refresh(List<ShopByCategoryObject> listShopByCategoryObject, List<ShopCategoryObject> listShops) {
        this.data = listShopByCategoryObject;
        this.listShops = listShops;
        if (listShops != null && listShops.size() > 0) {
            for (int i = 0; i < listShops.size(); i++)
                categoryNames.add(listShops.get(i).getName());
        }
        notifyDataSetChanged();
    }
    public List<ShopByCategoryObject> getSelectedData()
    {
     return data;
    }


     public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final Spinner spinner_banner_type;
        private final ImageView img_add;
         private final ImageView upload_image;
         private final ImageView img_minus;
        TextView tv_Sub_category;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_add = itemView.findViewById(R.id.img_add);
            spinner_banner_type = itemView.findViewById(R.id.spinner_banner_type);
            tv_Sub_category = itemView.findViewById(R.id.tv_Sub_category);
            upload_image = itemView.findViewById(R.id.upload_image);
            img_minus= itemView.findViewById(R.id.img_minus);
            img_add.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.img_add) {
                //showDeleteAlertDialog(getLayoutPosition());

                ShopByCategoryObject newData = new ShopByCategoryObject();
                data.add(newData);
                notifyDataSetChanged();
            }
        }
    }

}
