package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.ViewHolder> {

    List<ProductAdd> data;
    Context context;
    int whrecome;
    ArrayList<String> list=new ArrayList();


    public OptionsAdapter(Context activity, List<ProductAdd> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
        list.add("ML");
        list.add("Gms");
        list.add("Kgs");
        list.add("Liters");

    }


    @NonNull
    @Override
    public OptionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.dynamic_addlayout, parent, false);

        return new OptionsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionsAdapter.ViewHolder holder, final int position) {
       /* final ProductAdd orderModel = data.get(position);
        orderModel.setSize(holder.et_size.getText().toString().trim());
        orderModel.setColr(holder.et_color.getText().toString().trim());
        orderModel.setPrice(holder.et_price.getText().toString().trim());
        orderModel.setQunatity(holder.et_qty.getText().toString().trim());
        orderModel.setOptional(holder.et_option.getText().toString().trim());*/

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.weight_spinner.setAdapter(adapter);
        holder.weight_spinner.setSelection(1);


        if (data.get(position).getId().equalsIgnoreCase("0")){
            holder.img_cancel.setVisibility(View.VISIBLE);
        }else {
            holder.img_cancel.setVisibility(View.GONE);
        }
        if (position==0){
            holder.img_cancel.setVisibility(View.GONE);
        }else {
           // holder.img_cancel.setVisibility(View.VISIBLE);
        }

       /* if (data.get(position).getStatus()==1){
            holder.sw_status.setChecked(true);
            holder.status_radio.check(R.id.active);
        }else {
            holder.sw_status.setChecked(false);
            holder.status_radio.check(R.id.inactive);
        }*/
        holder.status_radio.setVisibility(View.GONE);
        if (data.get(position).getActivecheck()==2){
            holder.sw_status.setChecked(true);
            holder.status_radio.check(R.id.active);
        }else {
            holder.sw_status.setChecked(false);
            holder.status_radio.check(R.id.inactive);
        }


            holder.tv_option.setText(context.getString(R.string.option)+ " "+(position+1));
           // holder.et_size.setText(data.get(position).getSizecolor());
            //   holder.et_option.setText(data.get(position).getOptional());
            holder.et_qty.setText(data.get(position).getStock());
            holder.et_price.setText(data.get(position).getPrice());
            holder.et_weight.setText(data.get(position).getWeight());
            holder.et_discount.setText(data.get(position).getDiscount());

            if(data.get(position).getOptionlist() ==null)
            {
                ArrayList<String> optionlist =new ArrayList<>();
                optionlist.add("");
                data.get(position).setOptionlist(optionlist);
            }

            holder.recyclerView_add_option.setLayoutManager(new GridLayoutManager(context,1));
            holder.optionListAdapter=new OptionListAdapter(context,data.get(position).getOptionlist(),position);
            holder.recyclerView_add_option.setAdapter(holder.optionListAdapter);

            holder.img_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ProductAddOrUpdate)context).setremoveProduct(position);
                  //  data.remove(data.get(position));
                 //   notifyItemRemoved(position);

                    //  notifyItemRangeChanged(position, data.size());
                  //  notifyDataSetChanged();

                }
            });



    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setrefresh(ArrayList<ProductAdd> productaddlist,int whrecome) {
        this.data=productaddlist;
        this.whrecome=whrecome;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextInputEditText /*et_size,*/et_price,et_qty,et_discount,et_weight;
        TextView tv_option;
        ImageView img_cancel;
        Switch sw_status;
        RadioGroup status_radio;
        RecyclerView recyclerView_add_option;
        OptionListAdapter optionListAdapter;
        Spinner weight_spinner;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
           // et_size = itemView.findViewById(R.id.et_size);
            et_discount = itemView.findViewById(R.id.et_discount);
           // et_option = itemView.findViewById(R.id.et_option);
            et_price = itemView.findViewById(R.id.et_price);
            et_qty = itemView.findViewById(R.id.et_qty);
            tv_option = itemView.findViewById(R.id.tv_option);
            img_cancel = itemView.findViewById(R.id.img_cancel);
            sw_status = itemView.findViewById(R.id.sw_status);
            status_radio = itemView.findViewById(R.id.status_radio);
            et_weight = itemView.findViewById(R.id.et_weight);
            recyclerView_add_option = itemView.findViewById(R.id.recycle_options);
            weight_spinner = itemView.findViewById(R.id.weight_spinner);


            status_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch(checkedId){
                        case R.id.active:
                            System.out.println("aaaaaaaaaa status_radio");
                            ((ProductAddOrUpdate)context).setradiocheck(getAdapterPosition(),2);
                            data.get(getAdapterPosition()).setStatus(2);
                            // do operations specific to this selection
                            break;
                        case R.id.inactive:
                            ((ProductAddOrUpdate)context).setradiocheck(getAdapterPosition(),1);
                            data.get(getAdapterPosition()).setStatus(1);
                            // do operations specific to this selection
                            break;

                    }

                }
            });

            sw_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    System.out.println("aaaaaaaaaa sw_status");
                    if (b){
                        ((ProductAddOrUpdate)context).setradiocheck(getAdapterPosition(),1);
                        data.get(getAdapterPosition()).setStatus(1);
                    }else {
                        ((ProductAddOrUpdate)context).setradiocheck(getAdapterPosition(),2);
                        data.get(getAdapterPosition()).setStatus(2);
                    }

                }
            });
           /* et_size.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setSizecolor(et_size.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });*/
            et_discount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setDiscount(et_discount.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            }); /*et_option.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setOptional(et_option.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });*/
            et_price.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    /*if (i==0){
                        if (charSequence.equals("0")){
                        }else {
                            data.get(getAdapterPosition()).setPrice(et_price.getText().toString());
                        }
                    }else {
                        data.get(getAdapterPosition()).setPrice(et_price.getText().toString());
                    }*/
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().startsWith("0"))
                    {
                        et_price.setText("");
                        Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                    }else {
                        data.get(getAdapterPosition()).setPrice(et_price.getText().toString());
                    }
                }
            });  et_weight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                   // data.get(getAdapterPosition()).setWeight(et_weight.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().startsWith("0"))
                    {
                        et_weight.setText("");
                        Toast.makeText(context, "Please enter valid weight", Toast.LENGTH_SHORT).show();
                    }else {
                        data.get(getAdapterPosition()).setWeight(et_weight.getText().toString());
                    }
                }
            });
            weight_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    data.get(getAdapterPosition()).setWeight_type(""+i);
                   /* if (i==2 ||i==3 ){
                       String weight=""+(Integer.parseInt(data.get(getAdapterPosition()).getWeight())*1000);
                        Toast.makeText(context, ""+weight, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(context, ""+ data.get(getAdapterPosition()).getWeight(), Toast.LENGTH_SHORT).show();
                    }*/
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            et_qty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setStock(et_qty.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

}
