package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Adapters.ShopByCategoryAdapter;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class ShopByCategoryFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private PreferenceManager preferenceManager;
    private FloatingActionButton add_category_fab;
    private RecyclerView shop_by_category_recycler;
    private ShopByCategoryAdapter shopByCategoryAdapter;

    private ArrayList<ShopByCategoryModel> categoryModelsList;

    private String token = "";
    private final String searchString = "";


    public static int count = 0;

    public ShopByCategoryFragment() {
    }


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_by_category, container, false);
        initializeUi(view);
        initializeListeners();
        return view;
    }

    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);

        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        add_category_fab = view.findViewById(R.id.add_category_fab);
        shop_by_category_recycler = view.findViewById(R.id.shop_by_category_recycler);

        getCategories(searchString);
    }

    private void initializeListeners() {
        add_category_fab.setOnClickListener(this);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String enteredText = charSequence.toString();
                try {
                    shopByCategoryAdapter.getFilter().filter(enteredText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //getCategories(enteredText);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getCategories(String s) {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", s);
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        listIsFull();
                                        categoryModelsList = new ArrayList<>();
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            ShopByCategoryModel categoryModel = new ShopByCategoryModel();
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryModel.setId(categoryObject.getString("id"));
                                            categoryModel.setName(categoryObject.getString("name"));
                                            categoryModel.setCat_id(categoryObject.getString("cat_id"));
                                            categoryModel.setType(categoryObject.getInt("type"));
                                            categoryModel.setDescription(categoryObject.getString("desc"));
                                            categoryModel.setVendorId(categoryObject.getString("vendor_id"));
                                            categoryModel.setImage(categoryObject.getString("image"));
                                            categoryModel.setStatus(categoryObject.getInt("status"));
                                            categoryModelsList.add(categoryModel);
                                        }

                                        shopByCategoryAdapter = new ShopByCategoryAdapter(mContext, categoryModelsList);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {
                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }
                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }
                                        };
                                        shop_by_category_recycler.setLayoutManager(layoutManager);
                                        shop_by_category_recycler.setAdapter(shopByCategoryAdapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listIsEmpty();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            listIsEmpty();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        shop_by_category_recycler.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        shop_by_category_recycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add_category_fab) {// mContext.startActivity(new Intent(mContext, CreateAndUpdateShopByCategoryActivity.class));
            mContext.startActivity(new Intent(mContext, ProductAddOrUpdate.class));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (count == 1) {
            getCategories("");
            count = 0;
        }
    }
}