package com.nextclick.crm.ShopNowModule.Models;

public class BannerAvailability {
    String id,name,next_available_date,price="20";
    Integer banners_limit,filled_positions,Icon;
    private String startDate,endDate;

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setName(String name) {
        this.name=name;
    }
    public String getName() {
        return this.name;
    }


    public void setBanners_limit(Integer banners_limit) {
        this.banners_limit=banners_limit;
    }
    public Integer getBanners_limit() {
        return this.banners_limit;
    }
    public void setFilled_positions(Integer filled_positions) {
        this.filled_positions=filled_positions;
    }
    public Integer getFilled_positions() {
        return this.filled_positions;
    }

    public void setNext_available_date(String next_available_date) {
        this.next_available_date=next_available_date;
    }
    public String getNext_available_date() {
        return this.next_available_date;
    }

    public void setPrice(String price) {
        this.price=price;
    }
    public String getPrice() {
        return  price;
    }


    public void setIcon(Integer Icon) {
        this.Icon=Icon;
    }
    public Integer getIcon() {
        return Icon;
    }

    Integer selectedSlotID=-1;
    public void setSelectedSlotID(int selectedSlotID) {
        this.selectedSlotID=selectedSlotID;
    }
    public Integer getSelectedSlotID()
    {
        return  selectedSlotID;
    }

    public void setDate(String startDate, String endDate) {
        this.startDate=startDate;
        this.endDate=endDate;
    }

    public String getstartDate() {
        return startDate;
    }
    public String getendDate() {
        return this.endDate;
    }
}
