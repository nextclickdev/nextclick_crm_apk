package com.nextclick.crm.ShopNowModule.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.BannerPromotionDashboard;
import com.nextclick.crm.ShopNowModule.Adapters.SliderAdapter;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;

import java.util.List;

public class TemplatePickerFragment  extends Fragment {

    private ViewPager2 viewPager2;
    List<SliderItems> sliderItems;
    private final Handler sliderHandler = new Handler();
    private Runnable sliderRunnable;
    LinearLayout layout_position;
    TextView tv_current_index,tv_sliders_count,tv_no_images;
    Button btn_edit;

    private final BannerPromotionDashboard bannerPromotionDashboard;
    private SliderItems sliderItem;

    public TemplatePickerFragment(BannerPromotionDashboard bannerPromotionDashboard) {
        this.bannerPromotionDashboard=bannerPromotionDashboard;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pick_template, container, false);

        tv_no_images=view.findViewById(R.id.tv_no_images);
        btn_edit=view.findViewById(R.id.btn_edit);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sliderItem!=null)
                    bannerPromotionDashboard.onBannerSelect(sliderItem);
            }
        });

        layout_position = view.findViewById(R.id.layout_position);
        tv_sliders_count = view.findViewById(R.id.tv_sliders_count);
        tv_current_index = view.findViewById(R.id.tv_current_index);
        viewPager2 = view.findViewById(R.id.viewPagerImageSlider);
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(3);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float r = 1 - Math.abs(position);
                page.setScaleY(0.85f + r * 0.15f);
            }
        });

        viewPager2.setPageTransformer(compositePageTransformer);

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                if(sliderItems!=null && sliderItems.size()>position)
                    sliderItem = sliderItems.get(position);
            }
        });

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                tv_current_index.setText("" + (position + 1));
            }
        });
        return view;
    }


    public void onBannerSelect(SliderItems sliderItems) {
       this.sliderItem=sliderItems;
    }

    public void refreshData(List<SliderItems> sliderItems) {
        if(sliderItems!=null && sliderItems.size()>0) {
            this.sliderItems = sliderItems;
            viewPager2.setAdapter(new SliderAdapter(this, sliderItems));
            layout_position.setVisibility(View.VISIBLE);
            tv_sliders_count.setText("" + sliderItems.size());
            tv_current_index.setText("1");
            tv_no_images.setVisibility(View.GONE);
        }
        else {
            layout_position.setVisibility(View.GONE);
            tv_no_images.setVisibility(View.VISIBLE);
            btn_edit.setVisibility(View.GONE);
        }
    }

}