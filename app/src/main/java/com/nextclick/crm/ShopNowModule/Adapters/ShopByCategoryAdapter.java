package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.CreateAndUpdateShopByCategoryActivity;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_D;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;

public class ShopByCategoryAdapter extends RecyclerView.Adapter<ShopByCategoryAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private final Transformation transformation;

    private final List<ShopByCategoryModel> data;
    private final List<ShopByCategoryModel> data_full;

    private String token = "";


    public ShopByCategoryAdapter(Context activity, List<ShopByCategoryModel> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
        data_full = new ArrayList<>(itemPojos);
        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }


    @NonNull
    @Override
    public ShopByCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_shop_by_category_and_speciality, parent, false);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new ShopByCategoryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopByCategoryAdapter.ViewHolder holder, final int position) {
        final ShopByCategoryModel categoryModel = data.get(position);

        String categoryName = categoryModel.getName();
        String categoryImageUrl = categoryModel.getImage();
        String categoryDescription = categoryModel.getDescription();

        holder.sub_cat_name.setText(categoryName);
        holder.tvCategoryDescription.setText(categoryDescription);
        if (categoryImageUrl != null) {
            if (!categoryImageUrl.isEmpty()) {
                Picasso.get()
                        .load(categoryImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.sub_cat_image);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.sub_cat_image);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(transformation)
                    .into(holder.sub_cat_image);
        }

        if (categoryModel.getStatus() == 1) {
            holder.switch_.setVisibility(View.VISIBLE);
            holder.switch_.setChecked(true);
        } else if (categoryModel.getStatus() == 2) {
            holder.switch_.setVisibility(View.VISIBLE);
            holder.switch_.setChecked(false);
        }
    }

    private void deleteCategory(final List<ShopByCategoryModel> data, final int position, String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_D + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(context, "Deleted");
                                    data.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    UIMsgs.showToast(context, "Unable to delete. Please try later");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ShopByCategoryModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ShopByCategoryModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final Switch switch_;
        private final ImageView sub_cat_image;
        private final ImageView delete;
        private final TextView sub_cat_name;
        private final TextView tvCategoryDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            delete = itemView.findViewById(R.id.delete);
            switch_ = itemView.findViewById(R.id.switchCompat);
            sub_cat_name = itemView.findViewById(R.id.sub_cat_name);
            sub_cat_image = itemView.findViewById(R.id.sub_cat_image);
            tvCategoryDescription = itemView.findViewById(R.id.tvCategoryDescription);

            delete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.delete) {
                showDeleteAlertDialog(getLayoutPosition());
            } else {
                ShopByCategoryModel categoryModel = data.get(getLayoutPosition());
                Intent intent = new Intent(context, CreateAndUpdateShopByCategoryActivity.class);
                intent.putExtra("cat_id", categoryModel.getId());
                intent.putExtra("type", "u");
                context.startActivity(intent);
            }
        }
    }

    private void showDeleteAlertDialog(final int layoutPosition) {
        final ShopByCategoryModel categoryModel = data.get(layoutPosition);
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Are you sure to remove..?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteCategory(data, layoutPosition, categoryModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
