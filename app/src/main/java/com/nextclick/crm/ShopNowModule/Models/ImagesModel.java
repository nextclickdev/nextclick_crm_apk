package com.nextclick.crm.ShopNowModule.Models;

import android.graphics.Bitmap;

public class ImagesModel {

    String base64;
    String image;
    String id;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
