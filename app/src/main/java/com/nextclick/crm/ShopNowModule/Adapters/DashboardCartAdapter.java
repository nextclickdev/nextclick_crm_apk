package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.HomeFragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.PromotionsFragment;
import com.nextclick.crm.ShopNowModule.Models.DashboardObject;
import com.nextclick.crm.subcriptions.SubscriptionModel;

import java.util.ArrayList;

public class DashboardCartAdapter extends RecyclerView.Adapter<DashboardCartAdapter.ViewHolder>
{

    private PromotionsFragment promotionsFragment;
    private HomeFragment fragment;
    ArrayList<DashboardObject> dashboardList;
    boolean isPlanExisits;
    Context context;
    SubscriptionModel subscriptionModel;

    public DashboardCartAdapter(Context context, HomeFragment fragment, boolean isPlanExisits, SubscriptionModel subscriptionModel) {
        this.context = context;
        this.fragment=fragment;
        subscriptionModel = subscriptionModel;
        dashboardList=new ArrayList<>();
        setStaticData();
    }
    public DashboardCartAdapter(Context context, PromotionsFragment promotionsFragment) {
        this.context = context;
        this.promotionsFragment=promotionsFragment;

        dashboardList=new ArrayList<>();

        DashboardObject obj=new DashboardObject();
        obj.setImage(R.drawable.ic_promotion_tile);
        obj.setTitle(context.getString(R.string.NextClickPromotions));
        obj.setColor(0XFF31a4f5);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_promotion_tile);
        obj.setTitle(context.getString(R.string.promotions_history));
        obj.setColor(0XFFfd54e3);
        dashboardList.add(obj);
    }

    private void setStaticData() {
        DashboardObject obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_order);
        obj.setTitle(context.getString(R.string.Orders));
        obj.setColor(0XFF31a4f5);
        obj.setAction(R.id.orders_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_wallet);
        obj.setTitle(context.getString(R.string.wallet));
        obj.setColor(0XFF815bff);
        obj.setAction(R.id.wallet_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_product);
        obj.setTitle(context.getString(R.string.product));
        obj.setColor(0XFFff9650);
        obj.setAction(R.id.product_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_promo_code);
        obj.setTitle(context.getString(R.string.PromoCodes));
        obj.setColor(0XFFfd54e3);
        obj.setAction(R.id.promocard_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.promotions_icon_24);
        obj.setTitle(context.getString(R.string.Promotions));
        obj.setColor(0XFF08C7CE);
        obj.setAction(101);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_profile);
        obj.setTitle(context.getString(R.string.PROFILE));
        obj.setColor(0XFF507bfe);
        obj.setAction(R.id.profile_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_reports);
        obj.setTitle(context.getString(R.string.reports));
        obj.setColor(0XFFe53935);
        obj.setAction(R.id.report_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.ic_dashboard_settings);
        obj.setTitle(context.getString(R.string.Settings));
        obj.setColor(0XFF1bc74d);
        obj.setAction(R.id.settings_card);
        dashboardList.add(obj);

        obj=new DashboardObject();
        obj.setImage(R.drawable.customer_support);
        obj.setTitle(context.getString(R.string.support));
        obj.setColor(0XFFe53935);
        obj.setAction(R.id.support_card);
        dashboardList.add(obj);
    }

    @NonNull
    @Override
    public DashboardCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.dashboard_item, parent, false);
        return new DashboardCartAdapter.ViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull DashboardCartAdapter.ViewHolder holder, int position) {

        DashboardObject dashboardObject = dashboardList.get(position);
        holder.img_icon.setImageDrawable(context.getDrawable(dashboardObject.getImage()));
        holder.tv_item_name.setText(dashboardObject.getTitle());
       // holder.tv_item_name.setTextColor(dashboardObject.getColor());

        Drawable buttonDrawable = holder.layout_icon.getBackground();
        buttonDrawable = DrawableCompat.wrap(buttonDrawable);
        DrawableCompat.setTint(buttonDrawable, dashboardObject.getColor());
        holder.layout_icon.setBackground(buttonDrawable);
        Log.d("TAG", "onBindViewHolder: "+subscriptionModel);
        holder.card_item_adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment != null)
                    fragment.doAction(dashboardObject.getAction());
                else if (promotionsFragment != null)
                    promotionsFragment.doAction(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dashboardList.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_icon;
        TextView tv_item_name;
        RelativeLayout layout_icon;

        androidx.cardview.widget.CardView card_item_adapter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            card_item_adapter=itemView.findViewById(R.id.card_item_adapter);
            img_icon=itemView.findViewById(R.id.img_icon);
            tv_item_name=itemView.findViewById(R.id.tv_item_name);
            layout_icon=itemView.findViewById(R.id.layout_icon);
        }
    }
}
