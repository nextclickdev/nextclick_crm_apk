package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.SingleOrderActivity;
import com.nextclick.crm.ShopNowModule.Models.OrderModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.model.OrderHistoryModel;

import java.util.ArrayList;
import java.util.List;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    List<OrderModel> data;

    LayoutInflater inflter;
    Context context;
    private String token;
    ArrayList<OrderHistoryModel> orderslist;

    public OrdersAdapter(Context activity, List<OrderModel> itemPojos) {
        this.context = activity;
        this.data = itemPojos;


    }

    public OrdersAdapter(Context activity, ArrayList<OrderHistoryModel> orderslist) {
        this.context = activity;
        this.orderslist = orderslist;
    }


    @NonNull
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_order_item, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new OrdersAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.ViewHolder holder, final int position) {
        final OrderModel orderModel = data.get(position);

        holder.ordered_person_name.setText(orderModel.getFirst_name());
        holder.order_payment_type.setText(orderModel.getPayment_name());

        holder.order_sub_items.setText(orderModel.getSub_order_items());
        holder.order_items.setText(orderModel.getOrder_items());
        holder.order_total.setText(orderModel.getTotal() + "");

        int sts = orderModel.getOrder_status();
        String sts_str = "";
        if (sts == 0) {
            sts_str = "Order Rejected";
            holder.order_status_card.setCardBackgroundColor(Color.RED);
        } else if (sts == 1) {
            sts_str = "Order Received";

        } else if (sts == 2) {
            sts_str = "Order Accepted";
            holder.order_status_card.setCardBackgroundColor(Color.GREEN);
        } else if (sts == 3) {
            sts_str = "Order Preparing";
            holder.order_status_card.setCardBackgroundColor(Color.GRAY);
        } else if (sts == 4) {
            sts_str = "Out For Delivery";
            holder.order_status_card.setCardBackgroundColor(Color.BLUE);
        } else if (sts == 5) {
            sts_str = "On the way";
            holder.order_status_card.setCardBackgroundColor(Color.parseColor("#B8860B"));
        } else if (sts == 6) {
            sts_str = "Order Completed";
            holder.order_status_card.setCardBackgroundColor(Color.parseColor("#228B22"));
        } else if (sts == 7) {
            sts_str = "Order Cancelled";
            holder.order_status_card.setCardBackgroundColor(Color.RED);
        }


        holder.order_status.setText(sts_str);
        holder.tvViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SingleOrderActivity.class);
                intent.putExtra("order_id", orderModel.getId());
                context.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return orderslist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView ordered_person_name, order_payment_type, order_sub_items, order_items, order_total, order_status, tvViewDetails;
        CardView order_status_card;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ordered_person_name = itemView.findViewById(R.id.ordered_person_name);
            order_payment_type = itemView.findViewById(R.id.order_payment_type);
            order_sub_items = itemView.findViewById(R.id.order_sub_items);
            order_items = itemView.findViewById(R.id.order_items);
            order_total = itemView.findViewById(R.id.order_total);
            order_status = itemView.findViewById(R.id.order_status);
            order_status_card = itemView.findViewById(R.id.order_status_card);
            tvViewDetails = itemView.findViewById(R.id.tvViewDetails);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }


}
