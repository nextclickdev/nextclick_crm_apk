package com.nextclick.crm.ShopNowModule.Models;

import android.graphics.Bitmap;

public class MultiImageModel {

    Bitmap bitmap;
    String image;
    String id;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
