package com.nextclick.crm.ShopNowModule.Models;

import android.content.Intent;

public class DashboardObject {

    Integer image,color,actiontype;
    String title;
    public void setImage(Integer image) {
        this.image=image;
    }
    public Integer getImage()
    {
        return image;
    }

    public void setTitle(String title) {
        this.title=title;
    }
    public String getTitle()
    {
        return title;
    }

    public void setColor(Integer color) {
        this.color=color;
    }
    public Integer getColor()
    {
        return color;
    }

    public void setAction(int actiontype)  {
        this.actiontype=actiontype;
    }
    public Integer getAction()
    {
        return actiontype;
    }
}
