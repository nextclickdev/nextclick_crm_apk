package com.nextclick.crm.ShopNowModule.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.SelectBannerImage;
import com.nextclick.crm.ShopNowModule.Fragments.TemplatePickerFragment;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {

    private SelectBannerImage parentActivity;
    private final List<SliderItems> sliderItems;
    TemplatePickerFragment templatePickerFragment;

    public SliderAdapter(SelectBannerImage selectBannerImage, List<SliderItems> sliderItems) {
        this.parentActivity=selectBannerImage;
        this.sliderItems = sliderItems;
    }
    public SliderAdapter(TemplatePickerFragment templatePickerFragment, List<SliderItems> sliderItems) {
        this.templatePickerFragment=templatePickerFragment;
        this.sliderItems = sliderItems;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SliderViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.slide_item_container, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        holder.setImage(sliderItems.get(position));
        //  holder.tv_current_index.setText(""+position);
        //  holder.tv_sliders_count.setText(""+sliderItems.size());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parentActivity != null)
                    parentActivity.onBannerSelect(sliderItems.get(position));
                if (templatePickerFragment != null)
                    templatePickerFragment.onBannerSelect(sliderItems.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return sliderItems.size();
    }

    class SliderViewHolder extends RecyclerView.ViewHolder {
        private final RoundedImageView imageView;
        TextView tv_current_index,tv_sliders_count;

        SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageSlide);
            tv_sliders_count= itemView.findViewById(R.id.tv_sliders_count);
            tv_current_index= itemView.findViewById(R.id.tv_current_index);
        }

        void setImage(SliderItems sliderItems) {
            Picasso.get()
                    .load(sliderItems.getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(imageView);
        }
    }
}