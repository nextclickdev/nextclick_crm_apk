package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.PromotionCategoryAdapter;
import com.nextclick.crm.ShopNowModule.Adapters.PromotionsAdapter;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.dialogs.AddressLocationDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.GET_PROMOTION_BANNERS;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class PromotionsListActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etSearch;
    private ImageView ivSearch;
    private String searchString;
    TextView tv_no_data,tv_no_data2;
    private  RecyclerView recyclerview_promotions,recyclerview_my_promotions;
    private PromotionsAdapter promotionsAdapter,myPromotionsAdapter;
    private FloatingActionButton add_promotions;
    private Context mContext;
    LinearLayout layout_promotions,layout_my_promotions;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    Boolean isMyPromotions=false;
    List<PromotionObject> listOwnerPromotion;
    List<PromotionObject> listMyPromotions;

    String SelectedPaymentType= TYPE_OWNER;
    public static final String TYPE_SELF = "Self";
    public static final String TYPE_OWNER = "Owner";

    private TabLayout tabLayout;
    ViewPager viewPager;
    PromotionCategoryAdapter viewPagerAdapter;
    private int currentposition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_promotions_list);
        //getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        if(getIntent().hasExtra("isMyPromotions"))
        {
            isMyPromotions=getIntent().getBooleanExtra("isMyPromotions",false);
        }
        mContext=this;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(PromotionsListActivity.this);
        listMyPromotions = new ArrayList<>();
        listOwnerPromotion = new ArrayList<>();
        init();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Clicked on Promotions");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPromotions();
    }

    private void init() {

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new PromotionCategoryAdapter(getSupportFragmentManager(), this,preferenceManager.getString("is_admin"));
        viewPager.setAdapter(viewPagerAdapter);
        currentposition=0;
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(currentposition);

        recyclerview_promotions = findViewById(R.id.recyclerview_promotions);
        recyclerview_my_promotions = findViewById(R.id.recyclerview_my_promotions);
        tv_no_data = findViewById(R.id.tv_no_data);
        tv_no_data2 = findViewById(R.id.tv_no_data2);
        //List<PromotionObject> promotions = getPromotions(searchString);
        promotionsAdapter = new PromotionsAdapter(this,listOwnerPromotion,true,false);
        myPromotionsAdapter= new PromotionsAdapter(this,listMyPromotions,true,true);

        recyclerview_promotions.setLayoutManager(new LinearLayoutManager(this));
        recyclerview_promotions.setAdapter(promotionsAdapter);

        recyclerview_my_promotions.setLayoutManager(new LinearLayoutManager(this));
        recyclerview_my_promotions.setAdapter(myPromotionsAdapter);

        layout_promotions= findViewById(R.id.layout_promotions);
        layout_my_promotions= findViewById(R.id.layout_my_promotions);

        TextView tv_promotion_header=findViewById(R.id.tv_promotion_header);
        TextView tv_my_promotion_header=findViewById(R.id.tv_my_promotion_header);

        layout_promotions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= TYPE_OWNER)
                {
                    SelectedPaymentType= TYPE_OWNER;
                    layout_promotions.setBackgroundColor(Color.parseColor("#660033"));
                    tv_promotion_header.setTextColor(0XFFFFFFFF);
                    layout_my_promotions.setBackgroundColor(0XFFCDCDCD);
                    tv_my_promotion_header.setTextColor(0XFF000000);

                    recyclerview_my_promotions.setVisibility(View.GONE);
                    tv_no_data2.setVisibility(View.GONE);

                    if (listOwnerPromotion.size() > 0) {
                        promotionsAdapter.setRefresh(listOwnerPromotion);
                        recyclerview_promotions.setVisibility(View.VISIBLE);
                        tv_no_data.setVisibility(View.GONE);
                    }
                    else
                    {
                        recyclerview_promotions.setVisibility(View.GONE);
                        tv_no_data.setVisibility(View.VISIBLE);
                    }

                }
            }
        });
        layout_my_promotions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= TYPE_SELF)
                {
                    SelectedPaymentType= TYPE_SELF;
                    layout_my_promotions.setBackgroundColor(Color.parseColor("#660033"));
                    tv_my_promotion_header.setTextColor(0XFFFFFFFF);
                    layout_promotions.setBackgroundColor(0XFFCDCDCD);
                    tv_promotion_header.setTextColor(0XFF000000);

                    recyclerview_promotions.setVisibility(View.GONE);
                    tv_no_data.setVisibility(View.GONE);
                    if (listMyPromotions.size() > 0) {
                        myPromotionsAdapter.setRefresh(listMyPromotions);
                        recyclerview_my_promotions.setVisibility(View.VISIBLE);
                        tv_no_data2.setVisibility(View.GONE);
                    }
                    else
                    {
                        recyclerview_my_promotions.setVisibility(View.GONE);
                        tv_no_data2.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


        add_promotions = findViewById(R.id.add_promotions);
        add_promotions.setOnClickListener(this);

        TextView tv_add_promotion = findViewById(R.id.tv_add_promotion);
        tv_add_promotion.setOnClickListener(this);


        ImageView img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(this);

        etSearch = findViewById(R.id.etSearch);
        ivSearch = findViewById(R.id.ivSearch);
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchString = etSearch.getText().toString();
                startSearchView();
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                searchString = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    startSearchView();
                    return true;
                }
                return false;
            }
        });
    }

    private void startSearchView() {
        getPromotions();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_promotions:
            case R.id.tv_add_promotion:
              /*  Intent intent=new Intent(this,BannerPromotionDashboard.class);
                intent.putExtra("constitueid", "1");
                startActivity(intent);*/
                if (!preferenceManager.getString("is_admin").equalsIgnoreCase("true") &&
                        preferenceManager.getString("VendorConstituency") != null) {
                    //vendor profile constitu details we are sending here in case of non admin
                    Intent intent=new Intent(this,BannerPromotionDashboard.class);
                    intent.putExtra("constitueid", preferenceManager.getString("VendorConstituency"));
                    startActivity(intent);
                }
                else {
                    AddressLocationDialog addressLocationDialog = new AddressLocationDialog(mContext, PromotionsListActivity.this);
                    addressLocationDialog.showDialog();
                }
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }


    public void setConstituency(String constitueid) {
        Intent intent=new Intent(this,BannerPromotionDashboard.class);
        intent.putExtra("constitueid", constitueid);
        startActivity(intent);
    }

    private void getPromotions() {
        listMyPromotions.clear();
        listOwnerPromotion.clear();
        mCustomDialog.show();
        JSONObject requestBody =null;
        if (searchString != null && !searchString.isEmpty()) {
            Map<String, String> uploadMap = new HashMap<>();
            uploadMap.put("q", searchString);
            requestBody  = new JSONObject(uploadMap);
            System.out.println("aaaaaaaaa request" + requestBody);
        }



        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JSONObject finalRequestBody = requestBody;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_PROMOTION_BANNERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_PROMOTION_BANNERS response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                JSONArray bannersArray = dataObj.getJSONArray("banners");
                                for (int i = 0; i < bannersArray.length(); i++) {
                                    try {
                                        JSONObject transobj = bannersArray.getJSONObject(i);

                                        PromotionObject promotionObject = new PromotionObject();
                                        promotionObject.setID(transobj.getString("id"));
                                        promotionObject.setHeader(transobj.getString("title"));
                                        promotionObject.setDescription(transobj.getString("desc"));
                                        promotionObject.setIcon(transobj.getString("banner_image"));


                                        if (transobj.has("is_joined")) {
                                            try {
                                                if (transobj.getJSONObject("is_joined").has("joined_user_id"))
                                                    promotionObject.setJoined_user_id(transobj.getJSONObject("is_joined").getString("joined_user_id"));
                                            } catch (Exception ex) {
                                            }
                                        }

                                        if (transobj.has("offer_title"))
                                            promotionObject.setOfferTile(transobj.getString("offer_title"));
                                        if (transobj.has("offer_details"))
                                            promotionObject.setOfferDetails(transobj.getString("offer_details"));

                                        promotionObject.setcat_id(transobj.getString("cat_id"));
                                        promotionObject.setsub_cat_id(transobj.getString("sub_cat_id"));
                                        promotionObject.setbrand_id(transobj.getString("brand_id"));
                                        promotionObject.setconstituency_id(transobj.getString("constituency_id"));
                                        promotionObject.setpromotion_banner_position_id(transobj.getString("promotion_banner_position_id"));
                                        promotionObject.setcontent_type(transobj.getString("content_type"));
                                        // promotionObject.setDescription(transobj.getString("created_user_id"));
                                        //  promotionObject.setHeader(transobj.getString("updated_user_id"));
                                        promotionObject.setpublished_on(transobj.getString("published_on"));
                                        promotionObject.setexpired_on(transobj.getString("expired_on"));
                                        promotionObject.setowner(transobj.getString("owner"));
                                        promotionObject.setstatus(transobj.getString("status"));
                                        promotionObject.setAccessibility(transobj.getString("accessibility"));

                                        if (promotionObject.getowner().equals("1"))
                                            listOwnerPromotion.add(promotionObject);
                                        else
                                            listMyPromotions.add(promotionObject);
                                    }
                                    catch (Exception ex){}
                                }
                                promotionsAdapter.setRefresh(listOwnerPromotion);
                                myPromotionsAdapter.setRefresh(listMyPromotions);

                                if(viewPagerAdapter!=null)
                                    viewPagerAdapter.setData(listOwnerPromotion,listMyPromotions);

                                if(SelectedPaymentType == TYPE_OWNER) {
                                    if (listOwnerPromotion.size() > 0) {
                                        recyclerview_promotions.setVisibility(View.VISIBLE);
                                        tv_no_data.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        recyclerview_promotions.setVisibility(View.GONE);
                                        tv_no_data.setVisibility(View.VISIBLE);
                                    }
                                }
                                else
                                {
                                    recyclerview_promotions.setVisibility(View.GONE);
                                    tv_no_data.setVisibility(View.GONE);
                                }
                                if(SelectedPaymentType != TYPE_OWNER) {
                                    if (listMyPromotions.size() > 0) {
                                        recyclerview_my_promotions.setVisibility(View.VISIBLE);
                                        tv_no_data2.setVisibility(View.GONE);
                                    }
                                    else
                                    {
                                        recyclerview_my_promotions.setVisibility(View.GONE);
                                        tv_no_data2.setVisibility(View.VISIBLE);
                                    }
                                }
                                else
                                {
                                    recyclerview_my_promotions.setVisibility(View.GONE);
                                    tv_no_data2.setVisibility(View.GONE);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                        finally {
                            mCustomDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return finalRequestBody == null ? null : finalRequestBody.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void deletePrmotion(String promotionID) {
        Map<String, String> uploadMap = new HashMap<>();
        if (promotionID != null && !promotionID.isEmpty()) {

            uploadMap.put("id", promotionID);
        }
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor deleted the promotion of "+promotionID);
        }

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.DELETE_PROMOTION_BANNERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                Toast.makeText(mContext, "Promotion Deleted successfully.", Toast.LENGTH_SHORT).show();
                                getPromotions();
                            }
                            else {
                                Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "delete Promotion API is failed" , Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void joinPromotin(String promotionID) {
        Map<String, String> uploadMap = new HashMap<>();
        if (promotionID != null && !promotionID.isEmpty()) {

            uploadMap.put("promotion_banner_id", promotionID);
        }
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor joined the promotion of "+promotionID);
        }

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.JOIN_PROMOTION,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                Toast.makeText(mContext, "Promotion is successfully activated.", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}