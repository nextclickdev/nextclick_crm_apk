package com.nextclick.crm.ShopNowModule.Fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.ShopNowModule.Activities.BannerPromotionDashboard;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class UploadTemplateBanner extends Fragment {

    Button btn_edit;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    ImageView banner_img,banner_position_preview;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int SELECT_MULTIPLE_FILE = 1;
    public static String bannerImageBase64 = "";
    private Context mContext;
    LinearLayout layout_img_upload;

    private final BannerPromotionDashboard bannerPromotionDashboard;
    public UploadTemplateBanner(BannerPromotionDashboard bannerPromotionDashboard) {
        this.bannerPromotionDashboard=bannerPromotionDashboard;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.upload_banner, container, false);
        bannerImageBase64="";
        mContext=getActivity();
        banner_img=view.findViewById(R.id.banner_img);
        layout_img_upload=view.findViewById(R.id.layout_img_upload);
        btn_edit=view.findViewById(R.id.btn_edit);
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bannerImageBase64 != null && !bannerImageBase64.isEmpty())
                    bannerPromotionDashboard.onBannerCreate(bannerImageBase64);
                else
                    Toast.makeText(mContext, "Please upload banner image", Toast.LENGTH_SHORT).show();
            }
        });
        layout_img_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
            }
        });
        return view;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    public void setActivityResult(int requestCode, Intent data) {
        //  if (requestCode == SELECT_FILE)
        onSelectFromGalleryResult(data);
    }

    private String getBase64String(ImageView img) {
        Bitmap bitmap = ((BitmapDrawable) img.getDrawable()).getBitmap();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArray = baos.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bm != null) {
            bm = Bitmap.createScaledBitmap(bm, 512, 512, false);
            banner_img.setImageBitmap(bm);
            bannerImageBase64 = getBase64String(banner_img);
        } else
            Toast.makeText(mContext, "Please upload valid image", Toast.LENGTH_SHORT).show();
    }
}
