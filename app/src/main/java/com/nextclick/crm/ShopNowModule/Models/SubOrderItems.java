package com.nextclick.crm.ShopNowModule.Models;

public class SubOrderItems {

    String order_id;
    String sec_item_id;
    String item_id;
    int price ;
    int quantity;
    String name;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSec_item_id() {
        return sec_item_id;
    }

    public void setSec_item_id(String sec_item_id) {
        this.sec_item_id = sec_item_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
