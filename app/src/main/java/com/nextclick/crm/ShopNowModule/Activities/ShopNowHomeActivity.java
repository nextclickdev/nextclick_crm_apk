package com.nextclick.crm.ShopNowModule.Activities;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Activities.LoginActivity;
import com.nextclick.crm.Common.Adapters.ServicesAdapter;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.HomeFragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.Profilefragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.PromotionsFragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.ReportsFragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.SettingsFragment;
import com.nextclick.crm.ShopNowModule.Fragments.MyProductFragments.ProductsHomeFragment;
import com.nextclick.crm.Utilities.NetworkChangeReceiver;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.activities.NotificationActivity;
import com.nextclick.crm.authentication.SigninActivity;
import com.nextclick.crm.dialogs.Language_Dialog;
import com.nextclick.crm.dialogs.Logout_Dialog;
import com.nextclick.crm.faq.FaqActivity;
import com.nextclick.crm.orders.fragments.OrdersHistoryFragment;
import com.nextclick.crm.orders.fragments.WalletFragment;
import com.nextclick.crm.promocodes.PromocodelistFragment;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;
import com.nextclick.crm.subcriptions.SubcriptionsActivity;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.nextclick.crm.activities.customer_support.CustomerSupportActivity;
import com.zoho.desk.asap.api.ZohoDeskPortalSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.nextclick.crm.Config.Config.NOTIFICATIONS;
import static com.nextclick.crm.Config.Config.OLD_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.REMOVEtoken;
import static com.nextclick.crm.Config.Config.SHOP_AVAILABLE;
import static com.nextclick.crm.Config.Config.VENDOR_PRODUCTS_COUNT;
import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.FCM_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Services.InAppMessagingService.mp;

public class ShopNowHomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private Context mContext;
    private Toolbar toolbar;
    private DrawerLayout drawer;
    private boolean isPlanExists;

    private NavigationView navigationView;
    NavController navController;
    PreferenceManager preferenceManager;
    private int position = 0;
    private CustomDialog customDialog;
    RelativeLayout layout_dashboard_header;
    ImageView img_cal;
    LabeledSwitch labeledSwitch;
    RecyclerView recycler_services;
    TextView tv_no_services;
    private ServicesAdapter servicesAdapter;
    AppBarLayout appbar_layout;
    private ArrayList<ServicesModel> serviceModelArrayList;
    Integer shopInIndex = -1;
    private MenuItem languageItem, refreshItem;
    private final boolean removeLang = false;
    private Profilefragment profilefragment;
    private String vendorTitle;
    private boolean isServicesAPICalled = false;
    public ZohoDeskPortalSDK apiProvider;
    int availability;
    String address;
    ProductsHomeFragment productsHomeFragment;
    TextView item_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (UserData.isNewDesign)
            setContentView(R.layout.activity_home_new);
        else
            setContentView(R.layout.activity_home);
        init();
        setSupportActionBar(toolbar);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to shop now activity");
        }
        /*mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.s_n_nav_home, R.id.s_n_nav_shop_by_category, R.id.s_n_nav_orders,R.id.s_n_nav_menu,R.id.s_n_nav_sections,
                R.id.s_n_nav_section_items, R.id.s_n_nav_my_products, R.id.nav_profile,R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.shop_now_nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);*/

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Clicked on Shop now module");
        }

        if (UserData.isNewDesign) {
            getServices();
            getNotifications();
            getoldSubscriptions();
        }


       /* setToolbarTile(getString(R.string.menu_home));
        loadFragment(new HomeFragment(isServicesAPICalled));
        navigationView.setCheckedItem(R.id.s_n_nav_home);*/

        position = getIntent().getIntExtra("position", 0);
        if (position == 1) {
            setToolbarTile(getString(R.string.Orders));
            loadFragment(new OrdersHistoryFragment());
            //   loadFragment(new OrdersHomeFragment());
        }


        Menu menu = navigationView.getMenu();

        // find MenuItem you want to change
        MenuItem nav_camara = menu.findItem(R.id.nav_logout);
        nav_camara.setVisible(preferenceManager.getString("VendorName") != null);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (preferenceManager.getString("VendorName") == null) {
                    askLogin();
                } else if (!isServicesAPICalled) {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                    int id = menuItem.getItemId();
                    if (id == R.id.nav_logout) {
                        Logout_Dialog logout_dialog = new Logout_Dialog(ShopNowHomeActivity.this, 1);
                        logout_dialog.showDialog();
                    }
                } else {
                    int id = menuItem.getItemId();
                    if (id == R.id.s_n_nav_home) {
                        setToolbarTile(vendorTitle);
                        loadFragment(new HomeFragment(isServicesAPICalled));
                    } else if (id == R.id.s_n_nav_my_products) {
                        System.out.println("aaaaaaaa isServicesAPICalled nav " + isServicesAPICalled);
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.catalogue_products))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                getProductscount();

                            }
                        }
                    } else if (id == R.id.s_n_nav_orders) {
                        System.out.println("aaaaaaaa isServicesAPICalled nav " + isServicesAPICalled);
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.orders_listing))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                setToolbarTile(getString(R.string.Orders));
                                Fragment ordersFragment = new OrdersHistoryFragment();
                                loadFragment(ordersFragment);
                                setOrderFragmentHeaders(ordersFragment);
                            }
                        }
                    } else if (id == R.id.nav_profile) {
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.vendor_shop_profile))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                setToolbarTile(getString(R.string.PROFILE));
                                Fragment profilefragment = new Profilefragment();
                                loadFragment(profilefragment);
                                setProfileFragmentHeaders(profilefragment);
                            }
                        }

                    } else if (id == R.id.nav_settings) {
                        if (isServicesAPICalled) {
                            setToolbarTile(getString(R.string.action_settings));
                            loadFragment(new SettingsFragment(Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.catalogue_products)));
                        }
                    } else if (id == R.id.nav_wallet) {
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.wallet_listing))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                setToolbarTile(getString(R.string.wallet));
                                Fragment walletFragment = new WalletFragment(mContext);
                                loadFragment(walletFragment);
                                setWalletFragmentHeaders(walletFragment);
                            }
                        }
                    } else if (id == R.id.s_n_nav_subscription) {
                        if (isServicesAPICalled) {
                            Intent intent = new Intent(mContext, SubcriptionsActivity.class);
                            intent.putExtra("isPlanExists", "" + isPlanExists);
                            startActivity(intent);
                        }
                    } else if (id == R.id.s_n_nav_promotion) {
                        if (isServicesAPICalled) {
                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.promotions))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else
                                showPromotionFragment();
                        }
                    } else if (id == R.id.nav_faq) {
                        setToolbarTile(getString(R.string.faq));

                        Fragment profilefragment = new FaqActivity();
                        loadFragment(profilefragment);

//                        Intent intent = new Intent(ShopNowHomeActivity.this, FaqActivity.class);
//                        startActivity(intent);
                    } else if (id == R.id.nav_support) {
                        if (isServicesAPICalled) {
                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.customer_feedback))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else
                                startActivity(new Intent(ShopNowHomeActivity.this, CustomerSupportActivity.class));
                            //support related action
              /*              ZohoDeskPortalSDK.Logger.enableLogs();
                            ZDPortalChatUser chatUser = new ZDPortalChatUser();
                            chatUser.setName(preferenceManager.getString("VendorName"));
                            chatUser.setEmail(preferenceManager.getString("email"));
                            // chatUser.setPhone(phone);
                            ZDPortalChat.setGuestUserDetails(chatUser);

                            apiProvider = ZohoDeskPortalSDK.getInstance(getApplicationContext());
                            apiProvider.initDesk(Long.parseLong(getResources().getString(R.string.orgid)), "edbsndc215d3e00c2eb6a3ef71cd0050c1a052c0a4eb43c04324cefc8273de7a28cc9",
                                    ZohoDeskPortalSDK.DataCenter.IN);
                            ZDPHomeConfiguration homeConfiguration = new ZDPHomeConfiguration.Builder()
                                    .showKB(true)
                                    .showMyTickets(true)
                                    .showCommunity(true)
                                    .showLiveChat(true)
                                    .showNavDrawer(true).build();
                            ZDPortalHome.show(ShopNowHomeActivity.this, homeConfiguration);
                            ZDPortalTickets.show(ShopNowHomeActivity.this);
                            ZDPortalCommunity.show(ShopNowHomeActivity.this);*/
                        }
                    } else if (id == R.id.nav_logout) {
                        Logout_Dialog logout_dialog = new Logout_Dialog(ShopNowHomeActivity.this, 1);
                        logout_dialog.showDialog();
                    } else if (id == R.id.s_n_nav_Reports) {
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.reports))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                                Fragment reportFragment = new ReportsFragment(mContext);
                                ft.replace(R.id.shop_now_nav_host_fragment, reportFragment);
                                // getActivity().setTitle(getString(R.string.reports));
                                setToolbarTile(getString(R.string.reports));
                                ft.addToBackStack(null);
                                ft.commit();
                                setToolbarTile(getString(R.string.reports));
                                setReportFragmentHeaders(reportFragment);
                            }
                        }
                    } else if (id == R.id.s_n_nav_promocode) {
                        if (isServicesAPICalled) {

                            if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.promocodes))
                                Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                            else {
                                setToolbarTile(getString(R.string.PromoCodes));
                                loadFragment(new PromocodelistFragment(mContext));
                            }
                        }
                    }
                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
    }

    private void getoldSubscriptions() {
        String url = OLD_SUBCRIPTIONS + "/?service_id=2";
        System.out.println("aaaaaaaa oldsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response != null) {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response OLD_SUBCRIPTIONS  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObject1 = jsonArray.getJSONObject(i);
                                        if (dataObject1.getString("status").equalsIgnoreCase("active")) {
                                            isPlanExists = true;
                                            String days = dataObject1.getJSONObject("packages").getString("days");
                                            String dateStop = dataObject1.getString("created_at");

                                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                            Date d1 = null;
                                            Date d2 = null;

                                            d2 = format.parse(dateStop);

                                            long diff = d2.getTime() - d1.getTime();

                                            long diffSeconds = diff / 1000 % 60;
                                            long diffMinutes = diff / (60 * 1000) % 60;
                                            long diffHours = diff / (60 * 60 * 1000) % 24;
                                            long diffDays = diff / (24 * 60 * 60 * 1000);

                                            System.out.print(diffDays + " days, ");
                                            System.out.print(diffHours + " hours, ");
                                            System.out.print(diffMinutes + " minutes, ");
                                            System.out.print(diffSeconds + " seconds.");

                                            Long remainingdays = Long.parseLong(days) + diffDays;
                                            System.out.println("aaaaaaa days  " + diffDays + " " + remainingdays);

                                            break;
                                        }
                                    }
                                } else {
//                                    showBottomSheet();
                                }
                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (!isPlanExists) {

                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("aaaaaaa error  " + error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                //  map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjExIiwidGltZSI6MTYyNTExNjY3OX0.hGHKRItuzSimfyH_awycj58enHX_xncMhJWEfQpIjrg");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void getProductscount() {

        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_PRODUCTS_COUNT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        customDialog.dismiss();
                        if (response != null) {
                            Log.d("Productscount_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  myproducts " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");

                                if (status) {
                                    try {
                                        //JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        String catalogue_count = dataObject.getString("catalogue_count");
                                        int inventory_instock_count = dataObject.getInt("inventory_instock_count");
                                        int inventory_outofstock_count = dataObject.getInt("inventory_outofstock_count");
                                        String pendig_count = dataObject.getString("pendig_count");
                                        String approved_count = dataObject.getString("approved_count");

                                        setToolbarTile(getString(R.string.my_products));
                                        productsHomeFragment = new ProductsHomeFragment();
                                        loadFragment(productsHomeFragment);
                                        setProductFragmentHeaders(productsHomeFragment, (inventory_instock_count + inventory_outofstock_count), catalogue_count, pendig_count, approved_count);

                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch myproduct  " + e.getMessage());
                                        e.printStackTrace();
                                    }
                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  " + e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        System.out.println("aaaaaaa error 111  " + error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaaaaa token  " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("aaaaaaaaaa onresume call ");
        registerReceiver(getNetworkChangeReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if (servicesAdapter != null && shopInIndex != -1 && serviceModelArrayList != null &&
                serviceModelArrayList.size() > shopInIndex &&
                servicesAdapter.isEqual(serviceModelArrayList) &&
                servicesAdapter.getSelectedIndex() != shopInIndex) {
            servicesAdapter.selectedIndex(shopInIndex);
            servicesAdapter.notifyDataSetChanged();
        }
        getSubscriptionServicesList();
        getNotifications();

        if (Utility.check_product_page) {
            Utility.check_product_page = false;
            getProductscount();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (networkChangeReceiver != null)
            unregisterReceiver(networkChangeReceiver);
    }

    private NetworkChangeReceiver networkChangeReceiver = null;

    private BroadcastReceiver getNetworkChangeReceiver() {
        if (networkChangeReceiver == null)
            networkChangeReceiver = new NetworkChangeReceiver();
        return networkChangeReceiver;
    }


    public void askLogin() {
        new AlertDialog.Builder(mContext)
                .setTitle(getString(R.string.app_login))
                .setMessage(getString(R.string.app_login_info))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = null;
                        if (Utility.isNewAuthentication)
                            intent = new Intent(mContext, SigninActivity.class);
                        else
                            intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    Fragment prevFragment;

    public void loadFragment(Fragment fragment) {
       /* if(prevFragment!=null) {
            if (!(prevFragment instanceof HomeFragment))
                getSupportFragmentManager().beginTransaction().remove(prevFragment).commit();
        }*/

        customDialog.show();
        clearFragments();

        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {

            customDialog.dismiss();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.shop_now_nav_host_fragment, fragment);
            transaction.commitAllowingStateLoss();

            if (fragment != null && fragment.getView() != null) {
                /*if (!(fragment instanceof PromocodelistFragment)
                        && !(fragment instanceof PromotionsFragment))*/
                fragment.getView().setBackground(getDrawable(R.drawable.dashboard_content_panel));

                //else
                //  fragment.getView().setBackground(getDrawable(R.drawable.white_background));
            }
        }, 100);//100
    }

    private void clearFragments() {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            /*if (fragment instanceof HomeFragment) {
                continue;
            }
            else*/
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }

    public void setToolbarTile(String title) {
        if (UserData.isNewDesign) {
            if (title.equals(vendorTitle) || title.equals(getString(R.string.menu_home))) {
                showDashboardToolbar();
                if (preferenceManager != null)
                    getSupportActionBar().setTitle(vendorTitle);
                else
                    getSupportActionBar().setTitle("");
            } else {
                showOtherToolbar();
                getSupportActionBar().setTitle(title);
            }
        } else
            getSupportActionBar().setTitle(title);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showOtherToolbar() {
        // appbar_layout.setAlpha(1f);
        // appbar_layout.setBackgroundColor(getColor(R.color.Iconblue));
        layout_dashboard_header.setVisibility(View.GONE);
        img_cal.setVisibility(View.GONE);
        labeledSwitch.setVisibility(View.GONE);
        // drawer.setBackgroundColor(getColor(R.color.white));
        if (removeLang && languageItem != null)
            languageItem.setVisible(false);
        if (refreshItem != null)
            refreshItem.setVisible(false);
    }

    //boolean isServicesAPICalled= false;//by default no services are showing so after api call only we have to show
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void showDashboardToolbar() {
        //  appbar_layout.setAlpha(0.9f);
        // appbar_layout.setBackground(getDrawable(R.drawable.dashboard_header_panel));
        // drawer.setBackground(getDrawable(R.drawable.image_header_panel_bg));

        if (layout_dashboard_header.getVisibility() == View.VISIBLE && recycler_services != null) {
            recycler_services.setAdapter(null);
            if ((serviceModelArrayList == null || serviceModelArrayList.size() == 0)) {
                if (isServicesAPICalled)
                    tv_no_services.setVisibility(View.VISIBLE);
            } else {
                System.out.println("aaaaaaaaa isServicesAPICalled " + isServicesAPICalled);
                tv_no_services.setVisibility(View.GONE);
                labeledSwitch.setVisibility(View.GONE);
                servicesAdapter = new ServicesAdapter(mContext, serviceModelArrayList, shopInIndex);
                servicesAdapter.setParentActivity(ShopNowHomeActivity.this);
                recycler_services.setAdapter(servicesAdapter);
                //if (shopInIndex != -1)
                // recycler_services.scrollToPosition(shopInIndex);
                if (shopInIndex != -1 && serviceModelArrayList.size() > shopInIndex && servicesAdapter.getSelectedIndex() != shopInIndex) {
                    servicesAdapter.selectedIndex(shopInIndex);
                    servicesAdapter.notifyDataSetChanged();
                }
            }
        } else {
            if (isServicesAPICalled) {
                labeledSwitch.setVisibility(View.GONE);
                if (shopInIndex != -1 && serviceModelArrayList.size() > shopInIndex && servicesAdapter.getSelectedIndex() != shopInIndex) {
                    servicesAdapter.selectedIndex(shopInIndex);
                    servicesAdapter.notifyDataSetChanged();
                }
            }
        }

        layout_dashboard_header.setVisibility(View.VISIBLE);
        img_cal.setVisibility(View.GONE);
        if (removeLang && languageItem != null)
            languageItem.setVisible(false);

        if (refreshItem != null)
            refreshItem.setVisible(true);
    }

    private void init() {

        toolbar = findViewById(R.id.shop_now_toolbar);
        if (UserData.isNewDesign) {
            layout_dashboard_header = findViewById(R.id.layout_dashboard_header);
            img_cal = findViewById(R.id.img_cal);
            appbar_layout = findViewById(R.id.appbar_layout);
            recycler_services = findViewById(R.id.recycler_services);
            tv_no_services = findViewById(R.id.tv_no_services);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ShopNowHomeActivity.this);
            linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
            recycler_services.setLayoutManager(linearLayoutManager);

            labeledSwitch = findViewById(R.id.service_availability_status);
            labeledSwitch.setLabelOff(getString(R.string.close));
            labeledSwitch.setLabelOn(getString(R.string.open));
            labeledSwitch.setColorOn(getResources().getColor(R.color.orange));
            labeledSwitch.setColorOff(getResources().getColor(R.color.white));
            labeledSwitch.setOnToggledListener(new OnToggledListener() {
                @Override
                public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                    String text = "";
                    // labeledSwitch.setOn(!isOn);
                    if (availability == 1) {
                        text = getString(R.string.close);
                    } else {
                        text = getString(R.string.open);
                    }
                    new AlertDialog.Builder(mContext)
                            .setTitle(getString(R.string.shop_status))
                            .setPositiveButton("" + text, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (isOn) {
                                        Map<String, Object> mainData = new HashMap<>();
                                        mainData.put("availability", 1);
                                        String data = new JSONObject(mainData).toString();
                                        updateDetails(data, 1);
                                    } else {
                                        Map<String, Object> mainData = new HashMap<>();
                                        mainData.put("availability", 0);
                                        String data = new JSONObject(mainData).toString();
                                        updateDetails(data, 0);
                                    }

                                }
                            })
                            .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                    labeledSwitch.setOn(!labeledSwitch.isOn());
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();

                    //UIMsgs.showToast(mContext,"onSwitched "+isOn);
                }
            });
        }
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        LinearLayout layout_img_cancel = findViewById(R.id.layout_img_cancel);
        layout_img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        });

        mContext = ShopNowHomeActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        vendorTitle = preferenceManager.getString("VendorName");
        if (vendorTitle == null || vendorTitle.isEmpty()) {
            vendorTitle = getString(R.string.menu_home);
        }

        customDialog = new CustomDialog(ShopNowHomeActivity.this);
        View nav_header_main_view = navigationView.getHeaderView(0);
        //nav_header_image = navigationView.inflateHeaderView(R.layout.shop_now_nav_header_main).findViewById(R.id.nav_header_image);
        //profilelayout=(LinearLayout)navigationView.inflateHeaderView(R.layout.shop_now_nav_header_main).findViewById(R.id.profilelayout);
        //TextView titlehead = (TextView)navigationView.inflateHeaderView(R.layout.shop_now_nav_header_main).findViewById(R.id.titlehead);
        navigationView.setNavigationItemSelectedListener(ShopNowHomeActivity.this);

    }

    private void updateDetails(final String data, int whichposition) {

        Log.d("updateDetails", data);
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(ShopNowHomeActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_AVAILABLE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(ShopNowHomeActivity.this, "Updated Successfully");

                                    if (whichposition == 0) {
                                        labeledSwitch.setOn(false);
                                        availability = 0;
                                    } else if (whichposition == 2) {
                                        preferenceManager.clear();
                                        UIMsgs.showToast(mContext, "Logout successfull");
                                        if (mp != null) {
                                            mp.stop();
                                            mp = null;
                                            //  mp.reset();
                                            // mp.release();
                                            System.out.println("aaaaaaaaa position  mpp ");
                                        }
                                        if (MyMixPanel.isMixPanelSupport) {
                                            MyMixPanel.logEvent("Vendor Logged out from the CRM application");
                                            MyMixPanel.logOutCurrentUser();
                                        }

                                        if (Utility.isNewAuthentication)
                                            startActivity(new Intent(mContext, SigninActivity.class));
                                        else
                                            startActivity(new Intent(mContext, LoginActivity.class));
                                        finish();
                                    } else {
                                        labeledSwitch.setOn(true);
                                        availability = 1;
                                    }
                                } else {
                                    UIMsgs.showToast(ShopNowHomeActivity.this, Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaaa error " + e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(ShopNowHomeActivity.this, MAINTENANCE);
                        }

                        customDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaaa error " + error.getMessage());
                        customDialog.dismiss();
                        UIMsgs.showToast(ShopNowHomeActivity.this, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaaaa token " + preferenceManager.getString(USER_TOKEN));
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void getServices() {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaa  jsonObject services " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject dataObject = jsonObject.getJSONObject("data");


                                    System.out.println("aaaaaaaaa  data services " + dataObject);
                                    String is_admin = dataObject.getString("is_admin");
                                    String email = dataObject.getString("email");
                                    preferenceManager.putString("is_admin", is_admin);
                                    preferenceManager.putString("email", email);


                                   /* if(dataObject.has("address"))
                                    {
                                        JSONObject addressObj=  dataObject.getJSONObject("address");
                                        String constitueid=addressObj.getString("constituency");
                                        preferenceManager.putString("VendorConstituency", constitueid);
                                    }*/
                                    if (dataObject.has("category")) {
                                        preferenceManager.putString("VendorCategory", dataObject.getJSONObject("category").getString("id"));
                                        preferenceManager.putString("VendorCategoryname", dataObject.getJSONObject("category").getString("name"));
                                    }


                                    availability = dataObject.getInt("availability");
                                    preferenceManager.putString("availability", "" + availability);
                                    address = dataObject.getString("address");
                                    System.out.println("aaaaaaaa address  " + address);
                                    labeledSwitch.setVisibility(View.GONE);
                                    labeledSwitch.setOn(availability == 1);
                                   /* try {

                                        JSONArray jsonArray=dataObject.getJSONArray("contacts");
                                        for (int i=0;i<jsonArray.length();i++){
                                            JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                            if (i==0){
                                                String number = jsonObject1.getString("number");
                                                preferenceManager.putString("number",number);
                                            }
                                        }
                                    }catch (NullPointerException e){

                                    }*/
                                    try {
                                        JSONObject servicesObject = dataObject.getJSONObject("services");
                                        isServicesAPICalled = true;
                                        setToolbarTile(vendorTitle);
                                        loadFragment(new HomeFragment(isServicesAPICalled));
                                        navigationView.setCheckedItem(R.id.s_n_nav_home);


                                        Iterator x = servicesObject.keys();
                                        JSONArray servicesjsonArray = new JSONArray();
                                        while (x.hasNext()) {
                                            String key = (String) x.next();
                                            servicesjsonArray.put(servicesObject.get(key));
                                        }
                                        if (servicesjsonArray.length() > 0) {

                                            serviceModelArrayList = new ArrayList<>();
                                            for (int i = 0; i < servicesjsonArray.length(); i++) {
                                                ServicesModel serviceModel = new ServicesModel();
                                                JSONObject serviceObject = servicesjsonArray.getJSONObject(i);
                                                Log.d("service_object", serviceObject + "");
                                                String id = serviceObject.getString("id");

                                                if (id.equals("2"))
                                                    shopInIndex = i;
                                                String name = serviceObject.getString("name");
                                                String description = serviceObject.getString("desc");
                                                String languages = serviceObject.getString("languages");

                                                String image = null;
                                                try {
                                                    image = serviceObject.getString("image");
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                serviceModel.setId(id);
                                                serviceModel.setName(name);
                                                serviceModel.setImage(image);
                                                serviceModel.setDesc(description);
                                                serviceModel.setLanguages(languages);
                                                serviceModelArrayList.add(serviceModel);
                                            }
                                           /* ServicesModel model = new ServicesModel();
                                            model.setName(getString(R.string.PromotionsAds));
                                            model.setDesc(getString(R.string.PromotionsAds));
                                            model.setId("12");//1234
                                            serviceModelArrayList.add(model);*/

                                            servicesAdapter = new ServicesAdapter(mContext, serviceModelArrayList, shopInIndex);
                                            servicesAdapter.setParentActivity(ShopNowHomeActivity.this);
                                            recycler_services.setAdapter(servicesAdapter);
                                            if (shopInIndex != -1)
                                                recycler_services.scrollToPosition(shopInIndex);

                                        }
                                    } catch (Exception e) {
                                        isServicesAPICalled = false;
                                        setToolbarTile(vendorTitle);
                                        loadFragment(new HomeFragment(isServicesAPICalled));
                                        navigationView.setCheckedItem(R.id.s_n_nav_home);
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  " + e.getMessage());
                                e.printStackTrace();
                            } finally {
                                if (serviceModelArrayList == null || serviceModelArrayList.size() == 0)
                                    tv_no_services.setVisibility(View.VISIBLE);
                                else
                                    tv_no_services.setVisibility(View.GONE);
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();

                        isServicesAPICalled = false;

                        labeledSwitch.setVisibility(View.GONE);
                        tv_no_services.setVisibility(View.VISIBLE);
                        tv_no_services.setText(R.string.app_login_info);

                        setToolbarTile(vendorTitle);
                        loadFragment(new HomeFragment(isServicesAPICalled));
                        navigationView.setCheckedItem(R.id.s_n_nav_home);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (vendorTitle != null && !vendorTitle.equals(getString(R.string.menu_home))) {
            getMenuInflater().inflate(R.menu.main, menu);
            languageItem = menu.findItem(R.id.action_change_language);
            refreshItem = menu.findItem(R.id.action_refresh);

            MenuItem item = menu.findItem(R.id.action_notifications);

            FrameLayout rootView = (FrameLayout)
                    MenuItemCompat.getActionView(item);
            item_count = rootView.findViewById(R.id.item_count);
            LinearLayout layout_notification = rootView.findViewById(R.id.layout_notification);

            layout_notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferenceManager.getString("VendorName") == null) {
                        askLogin();
                    } else if (!isServicesAPICalled) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(ShopNowHomeActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        //throw new RuntimeException("Test Crash"); // Force a crash
                    }
                }
            });
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.shop_now_nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void getNotifications() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NOTIFICATIONS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject notification  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                // String message=jsonObject.getString("message");
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                int count = 0;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject dataobj = jsonArray.getJSONObject(i);
                                    if (dataobj.getString("status").equalsIgnoreCase("1")) {
                                        count++;
                                    }
                                }
                                if (count == 0) {
                                    item_count.setVisibility(View.GONE);
                                } else {
                                    item_count.setText("" + count);
                                    item_count.setVisibility(View.VISIBLE);
                                }

                                System.out.println("aaaaaaaaaaa notificationcount " + count);
                            }

                        } catch (JSONException e) {
                            item_count.setVisibility(View.GONE);
                            // Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    item_count.setVisibility(View.GONE);
                } catch (NullPointerException e) {

                }

                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //  HashMap<String, String> headers = new HashMap<>();
                map.put(APP_ID, APP_ID_VALUE);
                map.put(AUTH_TOKEN, "Bearer " + new PreferenceManager(mContext).getString(USER_TOKEN));
                System.out.println("aaaaaaaa headers  " + map);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportActionBar() != null && !getSupportActionBar().getTitle().equals(vendorTitle)) {
            setToolbarTile(vendorTitle);
            loadFragment(new HomeFragment(isServicesAPICalled));
            navigationView.setCheckedItem(R.id.s_n_nav_home);
        } else {
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.app_exit_header))
                    .setMessage(getString(R.string.app_exit_info))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        /**/

        /*if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        setToolbarTile(getString(R.string.menu_home));
        navigationView.setCheckedItem(R.id.s_n_nav_home);
        super.onBackPressed();*/
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            /*case R.id.action_services:
                startActivity(new Intent(mContext, ServicesActivity.class));
                finish();
                return true;*/
            case R.id.action_notifications:
                if (preferenceManager.getString("VendorName") == null) {
                    askLogin();
                } else if (!isServicesAPICalled) {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(ShopNowHomeActivity.this, NotificationActivity.class);
                    startActivity(intent);
                    //throw new RuntimeException("Test Crash"); // Force a crash
                }
                return true;
            case R.id.action_change_language:
                Language_Dialog language_dialog = new Language_Dialog(ShopNowHomeActivity.this, ShopNowHomeActivity.this);
                language_dialog.showDialog();
                return true;
            case R.id.action_refresh:
                refreshSettings(ShopNowHomeActivity.this);
                return true;
            /*  case R.id.action_change_notification:
                Intent i=new Intent(ShopNowHomeActivity.this, NotificationSoundsActivity.class);
                startActivity(i);
                return true;
            case R.id.action_logout:
                Logout_Dialog logout_dialog=new Logout_Dialog(ShopNowHomeActivity.this,1);
                logout_dialog.showDialog();
                *//*new AlertDialog.Builder(mContext)
                        .setTitle("Alert")
                        .setMessage("Are you sure to logout..?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation

                                preferenceManager.clear();
                                UIMsgs.showToast(mContext,"Logout successfull");
                                startActivity(new Intent(mContext, LoginActivity.class));
                                finish();
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();*//*
                return true;*/

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void setlogout() {
        grantFCMPermission(preferenceManager.getString(FCM_TOKEN));

    }

    private void grantFCMPermission(final String msg) {
        customDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                REMOVEtoken, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        customDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa response  removetoken " + jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            Map<String, Object> mainData = new HashMap<>();
                            mainData.put("availability", 0);
                            String data = new JSONObject(mainData).toString();
                            updateDetails(data, 2);

                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        customDialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    customDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.dismiss();
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void showPromotionFragment() {
        setToolbarTile(getString(R.string.PromotionsAds));
        loadFragment(new PromotionsFragment());
       /* layout_dashboard_header.setVisibility(View.VISIBLE);

        ArrayList<ServicesModel> promotionList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("1601");
        serviceModel.setName(getString(R.string.NextClickPromotions));
        promotionList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1602");
        serviceModel.setName(getString(R.string.promotions_history));
        promotionList.add(serviceModel);

        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, promotionList);
        servicesAdapter.setParentActivity(ShopNowHomeActivity.this);
        recycler_services.setAdapter(servicesAdapter);*/
    }

    public void setProfileFragmentHeaders(Fragment profilefragment) {
        layout_dashboard_header.setVisibility(View.VISIBLE);

        ArrayList<ServicesModel> otherServicesList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("1701");
        serviceModel.setName(getString(R.string.PROFILE));
        otherServicesList.add(serviceModel);


        serviceModel = new ServicesModel();
        serviceModel.setId("1702");
        serviceModel.setName(getString(R.string.Business_Profile));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1703");
        serviceModel.setName(getString(R.string.BANKDETAILS));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1704");
        serviceModel.setName(getString(R.string.SOCIAL));
        //otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1705");
        serviceModel.setName(getString(R.string.BANNERIMAGES));
        otherServicesList.add(serviceModel);


        tv_no_services.setVisibility(View.GONE);

        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, otherServicesList, 0);
        servicesAdapter.setParentFragment(profilefragment);
        recycler_services.setAdapter(servicesAdapter);

        this.profilefragment = (Profilefragment) profilefragment;
    }

    public void setReportFragmentHeaders(Fragment profilefragment) {
        layout_dashboard_header.setVisibility(View.VISIBLE);
        img_cal.setVisibility(View.VISIBLE);
        img_cal.setOnClickListener(null);
        img_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilefragment instanceof ReportsFragment)
                    ((ReportsFragment) profilefragment).onCalendarAction(getSupportFragmentManager());
            }
        });

        ArrayList<ServicesModel> otherServicesList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("1801");
        serviceModel.setName(getString(R.string.sales_header1));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1802");
        serviceModel.setName(getString(R.string.sales_header2));
        otherServicesList.add(serviceModel);

        tv_no_services.setVisibility(View.GONE);
        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, otherServicesList, 0);
        servicesAdapter.setParentFragment(profilefragment);
        recycler_services.setAdapter(servicesAdapter);
    }

    public void setWalletFragmentHeaders(Fragment profilefragment) {
        layout_dashboard_header.setVisibility(View.VISIBLE);
        img_cal.setVisibility(View.VISIBLE);
        img_cal.setOnClickListener(null);
        img_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilefragment instanceof WalletFragment)
                    ((WalletFragment) profilefragment).onCalendarAction(getSupportFragmentManager());
            }
        });

        ArrayList<ServicesModel> otherServicesList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("1901");
        serviceModel.setName(getString(R.string.today));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1902");
        serviceModel.setName(getString(R.string.weekly));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("1903");
        serviceModel.setName(getString(R.string.monthly));
        otherServicesList.add(serviceModel);

        /*serviceModel = new ServicesModel();
        serviceModel.setId("1904");
        serviceModel.setName(getString(R.string.custom));
        otherServicesList.add(serviceModel);*/

        tv_no_services.setVisibility(View.GONE);
        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, otherServicesList, 0);
        servicesAdapter.setParentFragment(profilefragment);
        recycler_services.setAdapter(servicesAdapter);
    }

    public void setOrderFragmentHeaders(Fragment profilefragment) {
        layout_dashboard_header.setVisibility(View.VISIBLE);
        img_cal.setVisibility(View.VISIBLE);
        img_cal.setOnClickListener(null);
        img_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profilefragment instanceof OrdersHistoryFragment)
                    ((OrdersHistoryFragment) profilefragment).onCalendarAction(getSupportFragmentManager());
            }
        });

        ArrayList<ServicesModel> otherServicesList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("2001");
        serviceModel.setName(getString(R.string.all_orders));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2002");
        serviceModel.setName(getString(R.string.ongoing_orders));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2003");
        serviceModel.setName(getString(R.string.out_for_delivery));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2004");
        serviceModel.setName(getString(R.string.rejected_orders));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2005");
        serviceModel.setName(getString(R.string.cancelled_orders));
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2006");
        serviceModel.setName(getString(R.string.deliveryboy_rejected));
        otherServicesList.add(serviceModel);

        tv_no_services.setVisibility(View.GONE);
        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, otherServicesList, 0);
        servicesAdapter.setParentFragment(profilefragment);
        recycler_services.setAdapter(servicesAdapter);
    }

    public void setProductFragmentHeaders(Fragment profilefragment, int inventorystock, String catalogue_count,
                                          String pendig_count, String approved_count) {
        layout_dashboard_header.setVisibility(View.VISIBLE);

        ArrayList<ServicesModel> otherServicesList = new ArrayList<>();

        ServicesModel serviceModel = new ServicesModel();
        serviceModel.setId("2101");
        serviceModel.setName(getString(R.string.my_inventory) + "(" + inventorystock + ")");
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2102");
        serviceModel.setName(getString(R.string.catalogue) + "(" + catalogue_count + ")");
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2103");
        serviceModel.setName(getString(R.string.approved) + "(" + approved_count + ")");
        otherServicesList.add(serviceModel);

        serviceModel = new ServicesModel();
        serviceModel.setId("2104");
        serviceModel.setName(getString(R.string.Pending) + "(" + pendig_count + ")");
        otherServicesList.add(serviceModel);

        tv_no_services.setVisibility(View.GONE);
        recycler_services.setAdapter(null);
        servicesAdapter = new ServicesAdapter(mContext, otherServicesList, 0);
        servicesAdapter.setParentFragment(profilefragment);
        recycler_services.setAdapter(servicesAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == Profilefragment.UPDATE_LOCATION) {
            if (profilefragment != null)
                profilefragment.updateLocation(intent);
        }
    }


    private ArrayList<SubscriptionSetting> settingModalArrayList;

    private void getSubscriptionServicesList() {
        //preferenceManager.putString("is_admin","true");//testing
        /*if(preferenceManager.getString("SubscriptionSettings")!=null) {
            Gson gson = new Gson();
            String json = preferenceManager.getString("SubscriptionSettings");
            Type type = new TypeToken<ArrayList<SubscriptionSetting>>() {
            }.getType();
            settingModalArrayList = gson.fromJson(json, type);
        }
        else
        {
            settingModalArrayList =new ArrayList<>();
            settingModalArrayList.add(new SubscriptionSetting("listing","listing",true));
            settingModalArrayList.add(new SubscriptionSetting("vendor_shop_profile","vendor_shop_profile",true));
            settingModalArrayList.add(new SubscriptionSetting("products_listing","products_listing",false));
            settingModalArrayList.add(new SubscriptionSetting("wallet_listing","wallet_listing",false));
            settingModalArrayList.add(new SubscriptionSetting("promotions","promotions",false));

            Gson gson = new Gson();
            String json = gson.toJson(settingModalArrayList);
            preferenceManager.putString("SubscriptionSettings",json);
        }*/
        settingModalArrayList = new ArrayList<>();


        String url = Config.URL_GET_SUBSCRIPTION_SETTINGS + "/?service_id=2";
        System.out.println("aaaaaaaaaa   SUBSCRIPTION_SETTINGS url " + url);


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("aaaaaaaaaa   SUBSCRIPTION_SETTINGS " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");
                            if (status && http_code == 200) {
                                JSONObject dataObj = jsonObject.getJSONObject("data");

                                JSONArray responsearray = dataObj.getJSONArray("settings");
                                for (int i = 0; i < responsearray.length(); i++) {
                                    JSONObject jsonObject1 = responsearray.getJSONObject(i);

                                    String key = jsonObject1.getString("setting_key");
                                    String setting_description = "";
                                    if (jsonObject1.has("master_setting"))
                                        setting_description = jsonObject1.getJSONObject("master_setting").getString("description");
                                    String setting_status = jsonObject1.getString("status");

                                    SubscriptionSetting subscriptionSetting = new SubscriptionSetting(key, setting_description, setting_status.equals("1"));
                                    settingModalArrayList.add(subscriptionSetting);
                                }
                            }

                        } catch (JSONException e) {
                            System.out.println("SUBSCRIPTION_SETTINGS   catch " + e);
                        } finally {
                            Gson gson = new Gson();
                            String json = gson.toJson(settingModalArrayList);
                            preferenceManager.putString("SubscriptionSettings", json);
                            Utility.single_instance = settingModalArrayList;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("SUBSCRIPTION_SETTINGS  settings error  " + error.getMessage());

                Gson gson = new Gson();
                String json = gson.toJson(settingModalArrayList);
                preferenceManager.putString("SubscriptionSettings", json);
                Utility.single_instance = settingModalArrayList;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaaaa token " + preferenceManager.getString(USER_TOKEN));
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void refreshSettings(AppCompatActivity activity) {
        Intent intent = activity.getIntent();
        activity.finish();
        activity.startActivity(intent);
    }
}