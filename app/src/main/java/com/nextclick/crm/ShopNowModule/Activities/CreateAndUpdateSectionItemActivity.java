package com.nextclick.crm.ShopNowModule.Activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.SectionItemsFragment;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MENU_R;
import static com.nextclick.crm.Config.Config.SECTION_ITEM_C;
import static com.nextclick.crm.Config.Config.SECTION_ITEM_R;
import static com.nextclick.crm.Config.Config.SECTION_ITEM_U;
import static com.nextclick.crm.Config.Config.SECTION_R;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateSectionItemActivity extends AppCompatActivity implements View.OnClickListener {

    private Button submit;
    private Context mContext;
    private ImageView back_image;
    private TextView section_item_status_type;
    private PreferenceManager preferenceManager;
    private EditText section_item_name, section_item_desc, section_item_price;
    private Spinner products_spinner, menu_spinner, sections_spinner, category_spinner;


    private int status_type = 1;//1- create, 2-update
    private int page_no = 1, count = 0;
    private String section_item_name_str, section_item_desc_str, section_item_price_str,
            token, menu_id = null, product_id = null, section_id = null, sub_cat_id = null;

    private ArrayList<String> sectionsList;
    private ArrayList<String> sectionsIDList;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<String> productsList = new ArrayList<>();
    private ArrayList<String> productsIDList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_update_section_item);
        getSupportActionBar().hide();
        init();

        sections_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!sections_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    section_id = sectionsIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    productsIDList.clear();
                    productsList.clear();
                    menusList.clear();
                    menuIDList.clear();
                    menu_spinner.setAdapter(null);
                    products_spinner.setAdapter(null);
                    getMenus();
                } else {
                    sub_cat_id = null;
                    productsIDList.clear();
                    productsList.clear();
                    menusList.clear();
                    menuIDList.clear();
                    menu_spinner.setAdapter(null);
                    products_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                    /*sectionsIDList.clear();
                    sectionsList.clear();
                    sectionsList.add("Select");*/
                    productsIDList.clear();
                    productsList.clear();
                    products_spinner.setAdapter(null);
                    Thread thread = new Thread();
                    try {
                        Thread.sleep(1000);
                        getSections();
                        Thread.sleep(1000);
                        getProducts();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        products_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!products_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    product_id = productsIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        submit.setOnClickListener(this);
        back_image.setOnClickListener(this);
    }

    private void getSectionItem(String section_item_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SECTION_ITEM_R + section_item_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    Log.d("secItem_resp", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            section_item_name.setText(dataObject.getString("name"));
                            try {

                                section_item_desc.setText(dataObject.getString("desc"));
                                section_item_price.setText(dataObject.getString("price"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                menu_id = dataObject.getJSONObject("menu").getString("id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {

                                product_id = dataObject.getJSONObject("item").getString("id");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                Thread thread = new Thread();
                                Thread.sleep(1000);
                                for (int i = 0; i < categoryIDList.size(); i++) {
                                    if (dataObject.getJSONObject("shop_by_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                                        category_spinner.setSelection((i + 1), true);
                                        sub_cat_id = dataObject.getJSONObject("shop_by_category").getString("id");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {

                                section_id = dataObject.getJSONObject("sec").getString("id");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            /*try{
                                Thread thread = new Thread();
                                thread.sleep(1000);
                                for(int i=0;i<productsIDList.size();i++){
                                    if(dataObject.getJSONObject("item").getString("id").equalsIgnoreCase(productsIDList.get(i))){
                                        products_spinner.setSelection((i+1),true);
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }*/

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void init() {
        mContext = CreateAndUpdateSectionItemActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        back_image = findViewById(R.id.back_image);

        section_item_name = findViewById(R.id.section_item_name);
        section_item_desc = findViewById(R.id.section_item_desc);
        section_item_price = findViewById(R.id.section_item_price);
        products_spinner = findViewById(R.id.products_spinner);
        menu_spinner = findViewById(R.id.menu_spinner);
        sections_spinner = findViewById(R.id.sections_spinner);
        category_spinner = findViewById(R.id.category_spinner);
        submit = findViewById(R.id.submit);
        section_item_status_type = findViewById(R.id.section_item_status_type);
        productsList = new ArrayList<>();

        productsIDList = new ArrayList<>();
        sectionsList = new ArrayList<>();
        sectionsIDList = new ArrayList<>();

        getCategories();
        //getProducts();

        Thread thread = new Thread();
        try {
            LoadingDialog.loadDialog(mContext);
            Thread.sleep(1000);

            try {
                String statustext = getIntent().getStringExtra("type");
                if (statustext.equalsIgnoreCase("u")) {
                    section_item_status_type.setText("Update Section Item");
                    submit.setText("UPDATE");
                    status_type = 2;
                    getSectionItem(getIntent().getStringExtra("section_item_id"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoadingDialog.dialog.dismiss();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void getSections() {
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("shop_by_cat_id", sub_cat_id);
        dataMap.put("q", "");
        dataMap.put("menu_id", menu_id);
        final String data = new JSONObject(dataMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SECTION_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            sectionsList.add("select");
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject sectionObject = dataArray.getJSONObject(i);
                                                sectionsIDList.add(sectionObject.getString("id"));
                                                sectionsList.add(sectionObject.getString("name"));
                                                Log.d("array_sec_id", sectionObject.getString("id"));
                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, sectionsList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            sections_spinner.setAdapter(adapter);
                                            try {
                                                if (section_id != null) {
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < sectionsIDList.size(); i++) {
                                                            if (section_id.equalsIgnoreCase(sectionsIDList.get(i))) {
                                                                sections_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        UIMsgs.showToast(mContext, "Sections Not Available");
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("menu_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        menu_spinner.setAdapter(adapter);


                                        try {
                                            if (menu_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    Thread.sleep(1000);
                                                    for (int i = 0; i < menuIDList.size(); i++) {
                                                        if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                            menu_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        //LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            //LoadingDialog.dialog.dismiss();
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        category_spinner.setAdapter(adapter);


                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            //LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getProducts() {
        /*Map<String, String> dataMap = new HashMap<>();
        dataMap.put("page_no", page_no + "");
        dataMap.put("q", "");
        dataMap.put("menu_id", menu_id);
        dataMap.put("shop_by_cat_id", "");
        final String data = new JSONObject(dataMap).toString();*/
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, /*PRODUCT_R,*/MENU_R + menu_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("items");
                                        if (dataArray.length() > 0) {
                                            productsList.add("select");
                                            for (int i = 0; i < dataArray.length(); i++) {

                                                JSONObject productObject = dataArray.getJSONObject(i);
                                                productsList.add(productObject.getString("name"));
                                                productsIDList.add(productObject.getString("id"));

                                            }
                                            count++;
                                            ++page_no;
                                            try {
                                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                        android.R.layout.simple_spinner_item, productsList);
                                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                products_spinner.setAdapter(adapter);
                                            } catch (Exception e1) {
                                                e1.printStackTrace();
                                            }

                                            try {
                                                if (product_id != null) {
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < productsIDList.size(); i++) {
                                                            if (product_id.equalsIgnoreCase(productsIDList.get(i))) {
                                                                products_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            ///getProducts();


                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        UIMsgs.showToast(mContext, "Products Not Available");
                                        /*if (count == 0) {

                                        }
                                        try {
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                    android.R.layout.simple_spinner_item, productsList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            products_spinner.setAdapter(adapter);
                                        } catch (Exception e1) {
                                            e.printStackTrace();
                                        }*/
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {


            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back_image:
                onBackPressed();
                break;
            case R.id.submit:
                if (isValid()) {
                    String url = "";
                    String data = "";


                    if (status_type == 1) {
                        Map<String, String> uploadMap = new HashMap<>();

                        uploadMap.put("price", section_item_price_str);
                        uploadMap.put("sec_id", section_id);
                        uploadMap.put("menu_id", menu_id);
                        uploadMap.put("item_id", product_id);
                        uploadMap.put("desc", section_item_desc_str);
                        uploadMap.put("name", section_item_name_str);
                        url = SECTION_ITEM_C;
                        data = new JSONObject(uploadMap).toString();
                    }
                    if (status_type == 2) {
                        Map<String, String> uploadMap = new HashMap<>();
                        uploadMap.put("id", getIntent().getStringExtra("section_item_id"));
                        uploadMap.put("price", section_item_price_str);
                        uploadMap.put("sec_id", section_id);
                        uploadMap.put("menu_id", menu_id);
                        uploadMap.put("item_id", product_id);
                        uploadMap.put("desc", section_item_desc_str);
                        uploadMap.put("name", section_item_name_str);

                        url = SECTION_ITEM_U;
                        data = new JSONObject(uploadMap).toString();
                    }

                    createOrUpdateSectionItem(url, data);

                    break;
                }


        }
    }

    private void createOrUpdateSectionItem(String url, final String data) {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    SectionItemsFragment.status_count = 1;
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, "Something went wrong");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    private boolean isValid() {
        boolean valid = true;
        section_item_name_str = section_item_name.getText().toString().trim();
        section_item_desc_str = section_item_desc.getText().toString().trim();
        section_item_price_str = section_item_price.getText().toString().trim();

        if (section_item_name_str.length() == 0) {
            setEditTextErrorMethod(section_item_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (section_item_desc_str.length() == 0) {
            setEditTextErrorMethod(section_item_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (section_item_price_str.length() == 0) {
            setEditTextErrorMethod(section_item_price, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (menu_id == null) {
            UIMsgs.showToast(mContext, "Please Provide menu");
            valid = false;
        } else if (product_id == null) {
            UIMsgs.showToast(mContext, "Please Provide product");
            valid = false;
        } else if (section_id == null) {
            UIMsgs.showToast(mContext, "Please Provide section");
            valid = false;
        }


        return valid;
    }
}