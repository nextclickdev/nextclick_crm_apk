package com.nextclick.crm.ShopNowModule.Activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.text.Html
import android.text.InputFilter
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nextclick.crm.Config.Config
import com.nextclick.crm.Constants.Constants
import com.nextclick.crm.Constants.ValidationMessages
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs
import com.nextclick.crm.Helpers.UIHelpers.Utility
import com.nextclick.crm.R
import com.nextclick.crm.ShopNowModule.Adapters.OptionsAdapter
import com.nextclick.crm.ShopNowModule.Models.ImagesModel
import com.nextclick.crm.ShopNowModule.Models.ProductAdd
import com.nextclick.crm.Utilities.PreferenceManager
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel
import com.nextclick.crm.imagesselect.ImagesActivity
import com.nextclick.crm.productDetails.activities.AddNewBrandActivity
import com.nextclick.crm.productDetails.activities.MainProductpage
import com.nextclick.crm.productDetails.adapters.SelectedImageAdapter
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL

class ProductAddOrUpdate : AppCompatActivity(), View.OnClickListener,
    CompoundButton.OnCheckedChangeListener {
    private lateinit var img_add: ImageView
    private lateinit var back_image: ImageView
    private lateinit var banner_image: ImageView
    private lateinit var upload_image: ImageView
    private lateinit var tv_addimage: ImageView
    private lateinit var layout_addproduct: LinearLayout
    private lateinit var layout_itemtype: LinearLayout
    lateinit var product_name: TextInputEditText
    lateinit var product_desc: TextInputEditText
    lateinit var product_price: TextInputEditText
    lateinit var product_quantity: TextInputEditText
    lateinit var product_discount: TextInputEditText
    lateinit var product_option: EditText
    lateinit var tv_addnewbrand: TextView
    lateinit var category_spinner: Spinner
    lateinit var menu_spinner: Spinner
    lateinit var brands_spinner: Spinner
    lateinit var item_type_spinner: Spinner
//    private lateinit var layout_add: LinearLayout
    lateinit var listedittext: ArrayList<String>
    lateinit var productaddlist: ArrayList<ProductAdd>
    private var countoptions = 0
    private lateinit var submit: Button
    private val allViews: MutableList<View?> = ArrayList()
    private lateinit var multiselecttext: Array<EditText?>
    lateinit var optionsAdapter: OptionsAdapter
    private lateinit var recycle_views: RecyclerView
    private lateinit var userChoosenTask: String
    private val REQUEST_CAMERA = 0
    private val SELECT_FILE = 1
    private lateinit var product_name_str: String
    private lateinit var product_desc_str: String
    private var base64image = ""
    private lateinit var token: String
    private var sub_cat_id: String=""
    private var cat_id: String=""
    private var menu_id: String=""
    private var brand_id: String=""
    private var item_type_id = "3"
    private var product_price_str: String=""
    private var product_quantity_str: String=""
    private var product_discount_str: String=""
    private var product_option_str: String=""
    private lateinit var imageslist: ArrayList<ImagesModel>

    // private ArrayList<Shopbycategeries> categoriesList;
    private lateinit var categoriesList: ArrayList<String>
    private lateinit var categoryIDList: ArrayList<String>
    private lateinit var catidlist: ArrayList<String>
    private lateinit var producttypelist: ArrayList<String>
    private var menusList = ArrayList<String>()
    private var menuIDList = ArrayList<String>()
    private var brandidlist = ArrayList<String>()
    private var brandlist = ArrayList<String>()
    private var itemtypelist = ArrayList<String>()
    private lateinit var product_status_type: TextView
    private var status_type = 1
    private lateinit var mContext: Context
    private lateinit var switchCompact: SwitchCompat
//    private lateinit var veg_radio_btn: RadioButton
//    private lateinit var non_veg_radio_btn: RadioButton
    lateinit var preferenceManager: PreferenceManager
    private var productAvailability = 1
    private val item_type = 0
    private val veg = 1
    private val nonveg = 2
    private var isImageSelected = false
    private var currentposition = 0
    private var sectionid = 0
    private lateinit var images_recycle: RecyclerView
    private lateinit var selectedImageAdapter: SelectedImageAdapter
    private lateinit var imagelist: ArrayList<String>
    private lateinit var base64imglist: ArrayList<String>
    var product_id: String=""
    lateinit var gson: Gson
    private lateinit var customDialog: CustomDialog
    var checkpermisstiona = false
    override fun onCreate(savedInstanceState: Bundle?) {
	  super.onCreate(savedInstanceState)
	  supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
	  setContentView(R.layout.product_add_new)
	  checkpermisstiona = Utility.checkPermission(this@ProductAddOrUpdate)
	  //  Objects.requireNonNull(getSupportActionBar()).hide();
	  init()
	  prepareDetails()
	  prepareSpinnerDetails()
	  itemtypelist = ArrayList()
	  itemtypelist.add("Veg")
	  itemtypelist.add("Non Veg")
	  itemtypelist.add("Others")
	  val adapter = ArrayAdapter(mContext, android.R.layout.simple_spinner_item, itemtypelist)
	  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
	  item_type_spinner.adapter = adapter
	  item_type_spinner.setSelection(2)
	  if (MyMixPanel.isMixPanelSupport) {
		MyMixPanel.logEvent("Vendor navigated to product add or update activity")
	  }
    }

//    override fun onResume() {
//	  super.onResume()
//	  if (ContextCompat.checkSelfPermission(
//		    applicationContext,
//		    Manifest.permission.READ_EXTERNAL_STORAGE
//		) != PackageManager.PERMISSION_GRANTED
//		|| ContextCompat.checkSelfPermission(
//		    applicationContext,
//		    Manifest.permission.CAMERA
//		) != PackageManager.PERMISSION_GRANTED
//	  ) {
//		ActivityCompat.requestPermissions(
//		    this, arrayOf(
//			  Manifest.permission.ACCESS_FINE_LOCATION,
//			  Manifest.permission.ACCESS_COARSE_LOCATION,
//			  Manifest.permission.READ_EXTERNAL_STORAGE,
//			  Manifest.permission.CAMERA
//		    ), 101
//		)
//	  }
//    }

    private val blockCharacterSet = "~#^|$%&*!"
    private val filter = InputFilter { source, start, end, dest, dstart, dend ->
	  if (source != null && blockCharacterSet.contains("" + source)) {
		""
	  } else null
    }

    fun init() {
	  mContext = this@ProductAddOrUpdate
	  preferenceManager = PreferenceManager(mContext)
	  token = "Bearer " + preferenceManager.getString(Constants.USER_TOKEN)
	  customDialog = CustomDialog(mContext)
	  img_add = findViewById(R.id.img_add)
	  upload_image = findViewById(R.id.upload_image)
	  layout_addproduct = findViewById(R.id.layout_addproduct)
	  product_status_type = findViewById(R.id.product_status_type)
//	  layout_add = findViewById(R.id.layout_add)
	  submit = findViewById(R.id.submit)
	  recycle_views = findViewById(R.id.recycle_views)
	  back_image = findViewById(R.id.back_image)
	  banner_image = findViewById(R.id.banner_image)
	  tv_addimage = findViewById(R.id.tv_addimage)
	  product_name = findViewById(R.id.product_name)
	  product_desc = findViewById(R.id.product_desc)
	  category_spinner = findViewById(R.id.category_spinner)
	  brands_spinner = findViewById(R.id.brands_spinner)
	  switchCompact = findViewById(R.id.switchCompact)
//	  veg_radio_btn = findViewById(R.id.veg_radio_btn)
//	  non_veg_radio_btn = findViewById(R.id.non_veg_radio_btn)
	  menu_spinner = findViewById(R.id.menu_spinner)
	  product_price = findViewById(R.id.product_price)
	  product_option = findViewById(R.id.product_option)
	  product_quantity = findViewById(R.id.product_quantity)
	  product_discount = findViewById(R.id.product_discount)
	  images_recycle = findViewById(R.id.images_recycle)
	  item_type_spinner = findViewById(R.id.item_type_spinner)
	  layout_itemtype = findViewById(R.id.layout_itemtype)
	  tv_addnewbrand = findViewById(R.id.tv_addnewbrand)

	  // product_name.setFilters(new InputFilter[]{filter});
	  /*    product_name.addTextChangedListener(new TextWatcher() {
            CharSequence previous;
            public void afterTextChanged(Editable s) {
                if(s.toString().contains("&^%$#*@&(")){
                    s.clear();
                    s.append(previous.toString());
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previous = s;
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });*/gson = Gson()
	  base64imglist = ArrayList()
	  imageslist = ArrayList()
	  imagelist = ArrayList()
	  listedittext = ArrayList()
	  productaddlist = ArrayList()
	  img_add.setOnClickListener(this)
	  layout_addproduct.setOnClickListener(this)
	  back_image.setOnClickListener(this)
	  banner_image.setOnClickListener(this)
	  tv_addimage.setOnClickListener(this)
	  submit.setOnClickListener(this)
	  upload_image.setOnClickListener(this)
	  switchCompact.setOnCheckedChangeListener(this)
	  multiselecttext = arrayOfNulls(3)
	  currentposition = intent.getIntExtra("currentposition", 0)
        recycle_views.layoutManager = GridLayoutManager(this, 1)
	  optionsAdapter = OptionsAdapter(this@ProductAddOrUpdate, productaddlist)
        recycle_views.adapter = optionsAdapter
	  selectedImageAdapter = SelectedImageAdapter(this, imageslist)
        images_recycle.adapter = selectedImageAdapter
	  try {
		categories
//		val thread = Thread()
//		categories
//		/*thread.sleep(1000);
//            getMenus();*/Thread.sleep(1000)
	  } catch (e: Exception) {
		e.printStackTrace()
	  }
	  val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        images_recycle.layoutManager = layoutManager
	  tv_addnewbrand.setOnClickListener(View.OnClickListener {
		val i = Intent(this@ProductAddOrUpdate, AddNewBrandActivity::class.java)
		startActivity(i)
	  })
    }

    private fun prepareDetails() {
	  try {
		val statusText = intent.getStringExtra("type")
		println("aaaaaaaa type $statusText")
		if (statusText != null) {
		    if (statusText.equals("u", ignoreCase = true)) {
			  product_status_type.text = "Update product"
			  submit.text = "UPDATE"
			  status_type = 2
			  LoadingDialog.loadDialog(mContext)
			  product_id = intent.getStringExtra("product_id")!!
			  getProductToUpdate(product_id)
			  //  getProduct(getIntent().getStringExtra("product_id"));
		    }
		} else {
		    val productAdd = ProductAdd()
		    productAdd.stock = "0"
		    productAdd.price = ""
		    productAdd.sizecolor = ""
		    productAdd.discount = ""
		    productAdd.item_id = ""
		    productAdd.id = "0"
		    productaddlist.add(productAdd)
		    optionsAdapter.setrefresh(productaddlist, 0)
		    if (imageslist.size == 0) {
			  imageslist.add(ImagesModel())
			  imageslist.add(ImagesModel())
		    }
		    upload_image.visibility = View.GONE
		    images_recycle.visibility = View.VISIBLE
		    selectedImageAdapter.refresh(imageslist)
		}
	  } catch (e: Exception) {
		println("aaaaaaaa cathc " + e.message)
		e.printStackTrace()
	  }
    }

    private fun prepareSpinnerDetails() {
	  category_spinner.onItemSelectedListener = object : OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    if (!category_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  sub_cat_id = categoryIDList[position - 1]
			  cat_id = catidlist[position - 1]
			  LoadingDialog.loadDialog(mContext)
			  getMenus(sub_cat_id)
			  getBrands(cat_id)
			  LoadingDialog.dialog.dismiss()
			  if (producttypelist[position - 1].equals("1", ignoreCase = true)) {
				layout_itemtype.visibility = View.VISIBLE
			  } else {
				layout_itemtype.visibility = View.GONE
			  }
		    } else {
			  sub_cat_id = ""
			  menu_id = ""
			  menuIDList.clear()
			  menusList.clear()
			  menu_spinner.adapter = null
		    }
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  menu_spinner.onItemSelectedListener = object : OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    menu_id = if (!menu_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  menuIDList[position - 1]
		    } else ({
			  null
		    }).toString()
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  brands_spinner.onItemSelectedListener = object : OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    brand_id = if (!brands_spinner.selectedItem.toString()
				.equals("select", ignoreCase = true)
		    ) {
			  brandidlist[position - 1]
		    } else ({
			  null
		    }).toString()
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
	  item_type_spinner.onItemSelectedListener = object : OnItemSelectedListener {
		override fun onItemSelected(
		    parent: AdapterView<*>?,
		    view: View,
		    position: Int,
		    id: Long
		) {
		    item_type_id = "" + (position + 1)
		    println("aaaaaaaaaa item typemid  $item_type_id")
		    /* if (!item_type_spinner.getSelectedItem().toString().equalsIgnoreCase("Others")) {
                    item_type_id =""+position;
                } else {
                    item_type_id = "3";
                }*/
		}

		override fun onNothingSelected(parent: AdapterView<*>?) {}
	  }
    }

    override fun onClick(view: View) {
	  when (view.id) {
		R.id.img_add, R.id.layout_addproduct -> {
		    countoptions = countoptions + 1

		    //  threeSwitchLayout(countoptions);
		    val productAdd = ProductAdd()
		    productAdd.stock = "0"
		    productAdd.price = ""
		    productAdd.sizecolor = ""
		    productAdd.discount = ""
		    productAdd.item_id = ""
		    productAdd.id = "0"
		    productAdd.activecheck = 1
		    productaddlist.add(productAdd)
		    optionsAdapter.setrefresh(productaddlist, 0)
		}

		R.id.back_image -> {
		    if (!Constants.Use_ActivityDesign) {
			  if (currentposition == 1) {
				finish()
			  } else {
				val intent = Intent(this@ProductAddOrUpdate, MainProductpage::class.java)
				intent.putExtra("currentposition", currentposition)
				startActivity(intent)
			  }
		    } else {
			  fragmentManager.popBackStack()
		    }
		    finish()
		}

		R.id.tv_addimage -> {

		    /* if (imagelist.size()==0){
                    Intent intent1=new Intent(ProductAddOrUpdate.this, ImagesActivity.class);
                    intent1.putExtra("whichposition",0);
                    startActivityForResult(intent1, 2);
                }else {*/
		    //   String jsonCars = gson.toJson(imagelist);
		    val intent1 = Intent(this@ProductAddOrUpdate, ImagesActivity::class.java)
		    //  intent1.putExtra("whichposition",1);
		    //  intent1.putExtra("jsonCars",jsonCars);
		    startActivityForResult(intent1, 2)
		}

		R.id.upload_image -> uploadNewImage()
		R.id.submit ->                /* for (int k=0;k<productaddlist.size();k++){
                 //   System.out.println("aaaaaa  "+productaddlist.get(k).getPrice()+"  "+productaddlist.get(k).getQunatity());
                    System.out.println("aaaaaa  "+productaddlist.get(k).getSize()+"  "+productaddlist.get(k).getColr()+"  "+
                            productaddlist.get(k).getOptional()+"  "+productaddlist.get(k).getPrice()+" "+
                            productaddlist.get(k).getQunatity());
                }*/if (isValid) {
		    val jsonArray = JSONArray()
		    val productAdd1 = ProductAdd()
		    // productAdd1.setSizecolor(product_option.getText().toString().trim());
		    //  productAdd1.setPrice(product_price.getText().toString().trim());
		    //  productaddlist.add(productAdd1);
		    if (productaddlist.size == 0) {
			  Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT).show()
		    } else {
			  var checkproduct = false
			  var k = 0
			  while (k < productaddlist.size) {
				if (productaddlist[k].weight.isEmpty() || productaddlist[k].weight.equals(
					  "",
					  ignoreCase = true
				    ) ||
				    productaddlist[k].price.isEmpty() || productaddlist[k].price.equals(
					  "",
					  ignoreCase = true
				    ) ||
				    productaddlist[k].optionlist[0].isEmpty() || productaddlist[k].optionlist[0].equals(
					  "",
					  ignoreCase = true
				    )
				) {
				    //  Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT).show();
				    checkproduct = false
				    break
				} else {
				    checkproduct = true
				}
				k++
			  }
			  if (!checkproduct) {
				Toast.makeText(mContext, "Please enter all fileds", Toast.LENGTH_SHORT)
				    .show()
			  } else {
				var i = 0
				while (i < productaddlist.size) {
				    jsonArray.put(productaddlist[i].jsonObject)
				    i++
				}
				var url = ""
				var data = ""
				if (status_type == 1) {
				    if (imageslist.size != 0) {
					  println("aaaaaaaaa  atatus type  $status_type")
					  val uploadMap: MutableMap<String?, Any?> = HashMap()
					  uploadMap["name"] = product_name_str
					  uploadMap["desc"] = product_desc_str
					  uploadMap["shop_by_cat_id"] = sub_cat_id
					  uploadMap["menu_id"] = menu_id
					  uploadMap["brand_id"] = "" + brand_id
					  //  uploadMap.put("price", product_price_str);
					  //  uploadMap.put("quantity", product_quantity_str);
					  uploadMap["item_type"] = item_type_id + ""
					  //  uploadMap.put("discount", product_discount_str);

					  // uploadMap.put("image", base64image);
					  uploadMap["status"] = productAvailability.toString()
					  //  uploadMap.put("options", ""+jsonArray);
					  url = Config.PRODUCT_C
					  val jsonimagesArray = JSONArray()
					  var im = 0
					  while (im < imageslist.size) {
						//base64imglist
						if (imageslist[im].base64 != null) jsonimagesArray.put(
						    imageslist[im].base64
						)
						im++
					  }
					  try {
						val json = JSONObject(uploadMap)
						//  json.putOpt( "",data );
						json.put("variants", jsonArray)
						//  System.out.println("aaaaaaaa  withoutimage "+json.toString());
						json.put("item_images", jsonimagesArray)
						data = json.toString()
					  } catch (e: JSONException) {
						e.printStackTrace()
					  }

					  // data = new JSONObject(uploadMap).toString();
					  createOrUpdateCategory(url, data)
					  //  data = new JSONObject(uploadMap).toString();
					  println("aaaaaaaa  url $url  data $data")
				    } else {
					  Toast.makeText(
						mContext,
						getString(R.string.please_select_product_image),
						Toast.LENGTH_SHORT
					  ).show()
				    }
				} else if (status_type == 2) {
				    // if (isImageSelected) {
				    // System.out.println("aaaaaaaaa  atatus type  "+imageslist.size());
				    val uploadMap: MutableMap<String?, Any?> = HashMap()
				    uploadMap["item_id"] = intent.getStringExtra("product_id")
				    uploadMap["name"] = product_name_str
				    uploadMap["desc"] = product_desc_str
				    uploadMap["shop_by_cat_id"] = sub_cat_id
				    uploadMap["menu_id"] = menu_id
				    uploadMap["section_id"] = "" + sectionid
				    uploadMap["brand_id"] = "" + brand_id
				    uploadMap["item_type"] = "" + item_type_id
				    url = Config.PRODUCT_U
				    val json = JSONObject(uploadMap)
				    val jsonimagesArray = JSONArray()
				    var k = 0
				    while (k < imageslist.size) {
					  if (imageslist[k].base64 != null) {
						println("aaaaaaaaa id " + imageslist[k].id)
						try {
						    val jsonObject = JSONObject()
						    jsonObject.put("id", imageslist[k].id)
						    jsonObject.put("image", imageslist[k].base64)
						    jsonimagesArray.put(jsonObject)
						} catch (e: JSONException) {
						    e.printStackTrace()
						}
					  }
					  k++
				    }
				    try {
					  //    json.putOpt( "",uploadMap );
					  json.put("variants", jsonArray)
					  println("aaaaaaaa  json $json")
					  json.put("item_images", jsonimagesArray)
				    } catch (e: JSONException) {
					  e.printStackTrace()
				    }

				    // data = new JSONObject(uploadMap).toString();
				    data = json.toString()
				    createOrUpdateCategory(url, data)
				    println("aaaaaaaa  url $url  data $data")
				    /* }else {
                            System.out.println("aaaaaaaaa  atatus type  "+status_type);
                            Map<String, String> uploadMap = new HashMap<>();
                            uploadMap.put("item_id", getIntent().getStringExtra("product_id"));
                            uploadMap.put("name", product_name_str);
                            uploadMap.put("desc", product_desc_str);
                            uploadMap.put("shop_by_cat_id", sub_cat_id);
                            uploadMap.put("menu_id", menu_id);
                            uploadMap.put("section_id", ""+sectionid);
                            uploadMap.put("brand_id", ""+brand_id);

                            url = PRODUCT_U;
                            JSONObject json = new JSONObject(uploadMap);
                            JSONArray jsonimagesArray = new JSONArray();
                            JSONObject jsonObject=new JSONObject();
                            for (int k=0;k<imageslist.size();k++){
                                try {
                                    jsonObject.put("id",imageslist.get(k).getId());
                                    jsonObject.put("image",imageslist.get(k).getBase64());
                                    jsonimagesArray.put(jsonObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }


                            try {
                                //    json.putOpt( "",uploadMap );
                                json.put("variants",jsonArray);
                                json.put("item_images",jsonimagesArray);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // data = new JSONObject(uploadMap).toString();
                            data = json.toString();
                            createOrUpdateCategory(url,data);
                            System.out.println("aaaaaaaa  url "+url+"  data "+data);
                        }
*/
				}
			  }
		    }


		    /*  banner_image.buildDrawingCache();
                    Bitmap bitmap = banner_image.getDrawingCache();

                    ByteArrayOutputStream stream=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
                    byte[] image=stream.toByteArray();


                    base64image = Base64.encodeToString(image, 0);
                    Log.v("Image", ""+base64image);
*/
		}
	  }
    }

    fun uploadNewImage() {
	  val intent2 = Intent(this@ProductAddOrUpdate, ImagesActivity::class.java)
	  startActivityForResult(intent2, 2)
    }

    fun threeSwitchLayout(countoption: Int) {
	  if (listedittext.size != 0) {
		Toast.makeText(this, "Please Fill All Fields", Toast.LENGTH_SHORT).show()
	  } else {
		// Generate dynamic layout
		val layoutInflater =
		    baseContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

		// Use demo.xml to create layout
		val addView = layoutInflater.inflate(R.layout.dynamic_addlayout, null)


		// final LinearLayout layout_addedittext = (LinearLayout)addView.findViewById(R.id.layout_addedittext);
		val tv_option = addView.findViewById<View>(R.id.tv_option) as TextView
		val img_cancel = addView.findViewById<View>(R.id.img_cancel) as ImageView
		//            final EditText et_size = (EditText)addView.findViewById(R.id.et_size);
		val et_color = addView.findViewById<View>(R.id.et_color) as EditText
		val et_option = addView.findViewById<View>(R.id.et_option) as EditText
		val et_price = addView.findViewById<View>(R.id.et_price) as EditText
		val et_qty = addView.findViewById<View>(R.id.et_qty) as EditText
		tv_option.text = mContext.getString(R.string.option) + " " + countoption
		//  addEdittext(layout_addedittext,1);
		/*  img_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  //  addEdittext(layout_addedittext,1);
                }
            });*/img_cancel.setOnClickListener {
//		    layout_add!!.removeView(addView)
		    countoptions = countoptions - 1
		    tv_option.text = mContext.getString(R.string.option) + " " + countoptions
		}
		val layoutParams = LinearLayout.LayoutParams(
		    LinearLayout.LayoutParams.MATCH_PARENT,
		    LinearLayout.LayoutParams.WRAP_CONTENT
		)
		layoutParams.setMargins(0, 0, 0, 15)
		addView.layoutParams = layoutParams
//		layout_add!!.addView(addView)
	  }
    }

    fun addEdittext(layout_addedittext: LinearLayout, count: Int) {


	  // Generate dynamic layout
	  val layoutInflater = baseContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

	  // Use demo.xml to create layout
	  val addView = layoutInflater.inflate(R.layout.dynamic_edittext, null)
	  val img_add = addView.findViewById<View>(R.id.img_add) as ImageView
	  multiselecttext[count] = addView.findViewById<View>(R.id.edittext) as EditText
	  img_add.setImageResource(R.drawable.ic_add_circle_outline_black_24dp)
	  img_add.tag = "add"
	  //  cardIdentifier.setText(card_identifier);
	  img_add.setOnClickListener {
		if (img_add.tag == "add") {
		    if (multiselecttext[count]!!.text.toString().trim { it <= ' ' }.isEmpty()) {
			  multiselecttext[count]!!.error = "Empty"
		    } else {
			  //   listedittext.add(edittext.getText().toString().trim());
			  val countadd = count + 1
			  addEdittext(layout_addedittext, countadd)
			  img_add.setImageResource(R.drawable.ic_cancel)
			  img_add.tag = "delete"
		    }
		} else if (img_add.tag == "delete") {
		    layout_addedittext.removeView(addView)
		    //  listedittext.remove(multiselecttext[count].getText().toString().trim());
		}
	  }
	  val layoutParams = LinearLayout.LayoutParams(
		LinearLayout.LayoutParams.MATCH_PARENT,
		LinearLayout.LayoutParams.WRAP_CONTENT
	  )
	  layoutParams.setMargins(0, 0, 0, 15)
	  addView.layoutParams = layoutParams
	  allViews.add(multiselecttext[count])
	  layout_addedittext.addView(addView)
    }

    private fun selectImage() {
	  val items = arrayOf<CharSequence>(
		"Take Photo", "Choose from Library",
		"Cancel"
	  )
	  val builder = AlertDialog.Builder(this@ProductAddOrUpdate)
	  builder.setTitle("Add Photo!")
	  builder.setItems(items) { dialog, item ->
		val result = Utility.checkPermission(this@ProductAddOrUpdate)
		if (items[item] == "Take Photo") {
		    userChoosenTask = "Take Photo"
		    if (result) cameraIntent()
		} else if (items[item] == "Choose from Library") {
		    userChoosenTask = "Choose from Library"
		    if (result) galleryIntent()
		} else if (items[item] == "Cancel") {
		    dialog.dismiss()
		}
	  }
	  builder.show()
    }

    private fun galleryIntent() {
	  val intent = Intent()
	  intent.type = "image/*"
	  intent.action = Intent.ACTION_GET_CONTENT //
	  startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE)
    }

    private fun cameraIntent() {
	  val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
	  startActivityForResult(intent, REQUEST_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
	  super.onActivityResult(requestCode, resultCode, data)
	  println("aaaaaaaaa resultcode  $resultCode")
	  if (resultCode == RESULT_OK) {
		if (requestCode == SELECT_FILE) onSelectFromGalleryResult(data) else if (requestCode == REQUEST_CAMERA) onCaptureImageResult(
		    data
		)
	  }
	  if (requestCode == 2) {
		try {
		    val carListAsString = data!!.getStringExtra("jsonCars")
		    val type = object : TypeToken<List<String?>?>() {}.type
		    imagelist = gson.fromJson(carListAsString, type)
		    println("aaaaaaaaa imaelistsize  " + imagelist.size)
		    for (i in imagelist.indices) {
			  try {
				val bm = BitmapFactory.decodeFile("" + imagelist.get(i))
				val base64 = convert(bm)
				base64imglist.add(base64)
				val imagesModel = ImagesModel()
				// imagesModel.setId("0");
				imagesModel.image = imagelist.get(i)
				imagesModel.base64 = base64
				imageslist.add(imageInsertIndex, imagesModel)
			  } catch (e: NullPointerException) {
				var image: Bitmap? =null
				try {
				    image =
					  BitmapFactory.decodeStream(URL(imagelist.get(i)).content as InputStream)
				    println("aaaaaaaaaaaa image  $image")
				} catch (e1: MalformedURLException) {
				    e.printStackTrace()
				    try {
					  image =
						BitmapFactory.decodeStream(URL("file:///" + imagelist.get(i)).content as InputStream)
				    } catch (s1: MalformedURLException) {
					  println("aaaaaaaaaaaaa image  11  " + e1.message)
				    } catch (ioException: IOException) {
					  ioException.printStackTrace()
					  println("aaaaaaaaaaaaa image  33  " + ioException.message)
				    }
				} catch (e2: IOException) {
				    println("aaaaaaaaaaaaa image 222  " + e2.message)
				    e.printStackTrace()
				}
				if (image != null) {
				    val base64 = convert(image)
				    base64imglist.add(base64)
				    val imagesModel = ImagesModel()
				    // imagesModel.setId("0");
				    imagesModel.image = imagelist.get(i)
				    imagesModel.base64 = base64
				    imageslist.add(imageInsertIndex, imagesModel)
				}
			  }
		    }
		    println("aaaaaaaa  imagelist sixe " + imageslist.size)
		    if (imageslist.size == 0) {
			  // upload_image.setVisibility(View.VISIBLE);
			  //  images_recycle.setVisibility(View.GONE);
			  //  }else {
			  imageslist.add(ImagesModel())
			  imageslist.add(ImagesModel())
		    }
		    upload_image.visibility = View.GONE
		    images_recycle.visibility = View.VISIBLE
		    selectedImageAdapter.refresh(imageslist)
		} catch (e: NullPointerException) {

		    // upload_image.setVisibility(View.VISIBLE);
		    //  images_recycle.setVisibility(View.GONE);
		    println("aaaaaaa catch " + e.message)
		}
	  }
    }

    private val imageInsertIndex: Int
	  private get() {
		var index = 0
		if (imageslist.size > 0) {
		    for (i in imageslist.indices.reversed()) {
			  if (imageslist[i].image != null) {
				index = i + 1
				break
			  }
		    }
		}
		return index
	  }

    fun StringToBitMap(encodedString: String?): Bitmap? {
	  return try {
		val encodeByte =
		    Base64.decode(encodedString, Base64.DEFAULT)
		BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
	  } catch (e: Exception) {
		e.message
		println("aaaaaaaa catch e  " + e.message)
		null
	  }
    }

    fun convert(bitmap: Bitmap?): String {
	  val outputStream = ByteArrayOutputStream()
	  bitmap!!.compress(Bitmap.CompressFormat.JPEG, 70, outputStream)
	  return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    private fun onSelectFromGalleryResult(data: Intent?) {
	  var bm: Bitmap? =null
	  if (data != null) {
		try {
		    bm =
			  MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
		    base64image = convert(bm)
		} catch (e: IOException) {
		    e.printStackTrace()
		}
	  }
	  val stream = ByteArrayOutputStream()
	  bm!!.compress(Bitmap.CompressFormat.JPEG, 50, stream)
	  val imageInByte = stream.toByteArray()
	  val lengthbmp = (imageInByte.size / 1024).toLong()
	  isImageSelected = true
	  banner_image.setImageBitmap(bm)
	  base64image = convert(bm)
    }

    private fun onCaptureImageResult(data: Intent?) {
	  val thumbnail = data!!.extras!!["data"] as Bitmap?
	  val bytes = ByteArrayOutputStream()
	  thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 50, bytes)
	  val destination = File(
		Environment.getExternalStorageDirectory(),
		System.currentTimeMillis().toString() + ".jpg"
	  )
	  val fo: FileOutputStream
	  try {
		destination.createNewFile()
		fo = FileOutputStream(destination)
		fo.write(bytes.toByteArray())
		fo.close()
	  } catch (e: FileNotFoundException) {
		e.printStackTrace()
	  } catch (e: IOException) {
		e.printStackTrace()
	  }
	  isImageSelected = true
	  banner_image.setImageBitmap(thumbnail)
	  base64image = convert(thumbnail)
	  val stream = ByteArrayOutputStream()
	  val imageInByte = stream.toByteArray()
	  val lengthbmp = (imageInByte.size / 1024).toLong()
    }

    private fun getProductToUpdate(product_id: String?) {
	  println("aaaaa productid  $product_id")
	  customDialog.show()
	  val url = Config.PRODUCTDETAILS + product_id
	  val request: JsonObjectRequest = object : JsonObjectRequest(
		Method.GET, url, null,
		Response.Listener { response ->
		    customDialog.cancel()
		    try {
			  val status = response.getBoolean("status")
			  if (status) {
				val dataObject = response.getJSONObject("data")
				val productStatus = dataObject.getInt("status")
                  switchCompact.isChecked = productStatus == 1
				product_name.setText(dataObject.getString("name"))
				product_desc.setText(dataObject.getString("desc"))

				// ... Continue populating other UI elements
				val sectionsarray = dataObject.getJSONArray("sections")
				val sectionobject = sectionsarray.getJSONObject(0)
				sectionid = sectionobject.getInt("id")
				val sectionarray = dataObject.getJSONArray("section_items")
				productaddlist.clear()
				for (i in 0 until sectionarray.length()) {
				    val jsonObject1 = sectionarray.getJSONObject(i)

				    // ... Populate productaddlist with data
				}

				// ... Continue processing and updating UI elements
				val imagesArray = dataObject.getJSONArray("item_images")
				for (i in 0 until imagesArray.length()) {
				    val jsonObject1 = imagesArray.getJSONObject(i)

				    // ... Process and add images to imageslist
				}

				// ... Continue processing and updating UI elements
			  } else {
				UIMsgs.showToast(mContext, ValidationMessages.OOPS)
			  }
		    } catch (e: JSONException) {
			  e.printStackTrace()
			  UIMsgs.showToast(mContext, ValidationMessages.OOPS)
		    }
		    LoadingDialog.dialog.dismiss()
		},
		Response.ErrorListener {
		    customDialog.cancel()
		    LoadingDialog.dialog.dismiss()
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val headers: MutableMap<String, String> = HashMap()
		    headers["Content-Type"] = "application/json"
		    headers[Constants.AUTH_TOKEN] = token
		    return headers
		}
	  }

	  // Set request timeout and retries
	  request.retryPolicy = DefaultRetryPolicy(
		DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )

	  // Add the request to the RequestQueue
	  val requestQueue = Volley.newRequestQueue(mContext)
	  requestQueue.add(request)
    }

    fun convertUrlToBase64(url: String?): String {
	  val newurl: URL
	  val bitmap: Bitmap
	  var base64 = ""
	  try {
		val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
		StrictMode.setThreadPolicy(policy)
		newurl = URL(url)
		bitmap = BitmapFactory.decodeStream(newurl.openConnection().getInputStream())
		val outputStream = ByteArrayOutputStream()
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
		base64 = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
	  } catch (e: Exception) {
		e.printStackTrace()
	  }
	  return base64
    }

    private fun getMenus(sub_cat_id: String?) {
	  val requestQueue = Volley.newRequestQueue(mContext)
	  println("aaaaaaaaaa  menus url  " + Config.SHOP_BY_CATEGORY_R + sub_cat_id)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.POST, Config.SHOP_BY_CATEGORY_R + sub_cat_id,
		Response.Listener { response ->
		    Log.d("menu_res", response!!)
		    if (response != null) {
			  try {
				val jsonObject = JSONObject(response)
				println("aaaaaaaaaa  gretmenus  $jsonObject")
				val status = jsonObject.getBoolean("status")
				if (status) {
				    val dataArray = jsonObject.getJSONObject("data").getJSONArray("menus")
				    if (dataArray.length() > 0) {
					  menusList = ArrayList()
					  menuIDList = ArrayList()
					  menusList.add("Select")
					  for (i in 0 until dataArray.length()) {
						val categoryObject = dataArray.getJSONObject(i)
						menusList.add("" + Html.fromHtml(categoryObject.getString("name")))
						menuIDList.add(categoryObject.getString("id"))
					  }
					  val adapter = ArrayAdapter(
                          mContext, android.R.layout.simple_spinner_item, menusList
					  )
					  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
					  menu_spinner.adapter = adapter
					  try {
						if (menu_id != null) {
						    try {
							  for (i in menuIDList.indices) {
								if (menu_id.equals(
									  menuIDList[i],
									  ignoreCase = true
								    )
								) {
								    menu_spinner.setSelection(i + 1, true)
								}
							  }
						    } catch (e: Exception) {
							  println("aaaaaaaaaa  catch3  " + e.message)
							  e.printStackTrace()
						    }
						}
					  } catch (e: Exception) {
						println("aaaaaaaaaa  catch22  " + e.message)
						e.printStackTrace()
					  }
				    }
				}
			  } catch (e: Exception) {
				menusList.clear()
				val adapter = ArrayAdapter(
                    mContext,
				    android.R.layout.simple_spinner_item,
				    menusList
				)
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
				menu_spinner.adapter = adapter
				println("aaaaaaaaaa  catch11  " + e.message)
				e.printStackTrace()
			  }
		    } else {
			  UIMsgs.showToast(mContext, ValidationMessages.MAINTENANCE)
		    }
		},
		Response.ErrorListener { UIMsgs.showToast(mContext, ValidationMessages.OOPS) }) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = token
		    return map
		} /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    private fun getBrands(sub_cat_id: String?) {
	  val requestQueue = Volley.newRequestQueue(mContext)
	  println("aaaaaaaaa  brandsurl  " + Config.BRANDS + sub_cat_id)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.GET, Config.BRANDS + sub_cat_id,
		Response.Listener { response ->
		    Log.d("brands_res", response!!)
		    if (response != null) {
			  try {
				val jsonObject = JSONObject(response)
				val status = jsonObject.getBoolean("status")
				println("aaaaaaaaa  respose brands  $jsonObject")
				if (status) {
				    val dataArray = jsonObject.getJSONObject("data").getJSONArray("brands")
				    if (dataArray.length() > 0) {
					  brandlist = ArrayList()
					  brandidlist = ArrayList()
					  brandlist.add("Select")
					  for (i in 0 until dataArray.length()) {
						val categoryObject = dataArray.getJSONObject(i)
						brandlist.add("" + Html.fromHtml(categoryObject.getString("name")))
						brandidlist.add(categoryObject.getString("id"))
					  }
					  // System.out.println("aaaaaaaa  brandidlist  "+brandidlist.size()+"   "+brandlist.size()+" brand_id "+brand_id);
					  val adapter = ArrayAdapter(
                          mContext, android.R.layout.simple_spinner_item, brandlist
					  )
					  adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
					  brands_spinner.adapter = adapter
					  try {
						if (brand_id != null) {
						    try {

							  for (i in brandidlist.indices) {
								if (brand_id.equals(
									  brandidlist[i],
									  ignoreCase = true
								    )
								) {
								    brands_spinner.setSelection(i + 1, true)
								}
							  }
						    } catch (e: Exception) {
							  e.printStackTrace()
						    }
						}
					  } catch (e: Exception) {
						e.printStackTrace()
					  }
				    }
				}
			  } catch (e: Exception) {
				println("aaaaaaa  catch  " + e.message)
				e.printStackTrace()
			  }
		    } else {
			  UIMsgs.showToast(mContext, ValidationMessages.MAINTENANCE)
		    }
		},
		Response.ErrorListener { error ->
		    println("aaaaaa erro  " + error.message)
		    UIMsgs.showToast(mContext, ValidationMessages.OOPS)
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = token
		    return map
		} /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    override fun onCheckedChanged(compoundButton: CompoundButton, b: Boolean) {
	  productAvailability = if (b) {
		1
	  } else {
		2
	  }
    }//   System.out.println("aaaaaaaaa  catid  "+categoryObject.getString("cat_id"));

    /* Shopbycategeries shopbycategeries1=new Shopbycategeries();
	  shopbycategeries1.setCat_id(categoryObject.getString("cat_id"));
	  shopbycategeries1.setDesc(categoryObject.getString("name"));
	  shopbycategeries1.setId(categoryObject.getString("id"));
	  shopbycategeries1.setName(categoryObject.getString("name"));
	  shopbycategeries1.setProduct_type_widget_status(categoryObject.getString("product_type_widget_status"));
	  shopbycategeries1.setType(categoryObject.getString("type"));
	  shopbycategeries1.setDesc(categoryObject.getString("desc"));
	  shopbycategeries1.setImage(categoryObject.getString("image"));

	  categoriesList.add(shopbycategeries);*/
    /*  Shopbycategeries shopbycategeries=new Shopbycategeries();
							 shopbycategeries.setCat_id("");
							 shopbycategeries.setDesc("");
							 shopbycategeries.setId("");
							 shopbycategeries.setName("Select");
							 shopbycategeries.setProduct_type_widget_status("");
							 shopbycategeries.setType("");
   
							 categoriesList.add(shopbycategeries);*/
    private val categories: Unit
	  private get() {
		val searchMap: MutableMap<String?, Any?> = HashMap()
		searchMap["q"] = ""
		val data = JSONObject(searchMap).toString()
		val requestQueue = Volley.newRequestQueue(mContext)
		val stringRequest: StringRequest = object : StringRequest(
		    Method.POST, Config.SHOP_BY_CATEGORY_R,
		    Response.Listener<String?> { response ->
			  Log.d("cat_res", response!!)
			  if (response != null) {
				try {
				    val jsonObject = JSONObject(response)
				    println("aaaaaaaaaa jsonobject  $jsonObject")
				    val status = jsonObject.getBoolean("status")
				    if (status) {
					  val dataArray = jsonObject.getJSONArray("data")
					  if (dataArray.length() > 0) {
						categoriesList = ArrayList()
						categoryIDList = ArrayList()
						catidlist = ArrayList()
						producttypelist = ArrayList()
						/*  Shopbycategeries shopbycategeries=new Shopbycategeries();
				    shopbycategeries.setCat_id("");
				    shopbycategeries.setDesc("");
				    shopbycategeries.setId("");
				    shopbycategeries.setName("Select");
				    shopbycategeries.setProduct_type_widget_status("");
				    shopbycategeries.setType("");
		   
				    categoriesList.add(shopbycategeries);*/
						categoriesList.add("Select")
						for (i in 0 until dataArray.length()) {
						    val categoryObject = dataArray.getJSONObject(i)
						    categoryIDList.add(categoryObject.getString("id"))
						    categoriesList.add(
							  "" + Html.fromHtml(
								categoryObject.getString(
								    "name"
								)
							  )
						    )
						    producttypelist.add(categoryObject.getString("product_type_widget_status"))
						    catidlist.add(categoryObject.getString("cat_id"))
						    //   System.out.println("aaaaaaaaa  catid  "+categoryObject.getString("cat_id"));
						    /* Shopbycategeries shopbycategeries1=new Shopbycategeries();
										   shopbycategeries1.setCat_id(categoryObject.getString("cat_id"));
										   shopbycategeries1.setDesc(categoryObject.getString("name"));
										   shopbycategeries1.setId(categoryObject.getString("id"));
										   shopbycategeries1.setName(categoryObject.getString("name"));
										   shopbycategeries1.setProduct_type_widget_status(categoryObject.getString("product_type_widget_status"));
										   shopbycategeries1.setType(categoryObject.getString("type"));
										   shopbycategeries1.setDesc(categoryObject.getString("desc"));
										   shopbycategeries1.setImage(categoryObject.getString("image"));
			 
										   categoriesList.add(shopbycategeries);*/
						}
						val adapter = ArrayAdapter(
                            mContext,
						    android.R.layout.simple_spinner_item, categoriesList
						)
						adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
						category_spinner.adapter = adapter
					  }
				    }
				} catch (e: Exception) {
				    println("aaaaaaaaa catch " + e.message)
				    e.printStackTrace()
				}
			  } else {
				UIMsgs.showToast(mContext, ValidationMessages.MAINTENANCE)
			  }
		    },
		    Response.ErrorListener { UIMsgs.showToast(mContext, ValidationMessages.OOPS) }) {
		    @Throws(AuthFailureError::class)
		    override fun getHeaders(): Map<String, String> {
			  val map: MutableMap<String, String> = HashMap()
			  map["Content-Type"] = "application/json"
			  map[Constants.AUTH_TOKEN] = token
			  return map
		    }

		    @Throws(AuthFailureError::class)
		    override fun getBody(): ByteArray? {
			  return try {
				if (data == null) null else data.toByteArray(charset("utf-8"))
			  } catch (e: Exception) {
				e.printStackTrace()
				null
			  }
		    }
		}
		stringRequest.retryPolicy = DefaultRetryPolicy(
		    0,
		    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
		)
		requestQueue.add(stringRequest)
	  }
    /*else if (pr_product_option_length == 0) {
            setEditTextErrorMethod(product_option, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_price_length == 0) {
            setEditTextErrorMethod(product_price, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (Double.parseDouble(product_price_str) <= 0) {
            setEditTextErrorMethod(product_price, "INVALID AMOUNT");
            valid = false;
        } else if (product_price_str.equalsIgnoreCase("0")) {
            setEditTextErrorMethod(product_price, INVALID);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }else if (pr_quantity_length == 0) {
            setEditTextErrorMethod(product_quantity, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }*/

    //  base64image = basse64Converter(product_image);
    //   Log.v("image",base64image);
    /*  if (veg_radio_btn.isChecked()) {
     item_type = veg;
 }
 if (non_veg_radio_btn.isChecked()) {
     item_type = nonveg;
 }*/
    private val isValid: Boolean
	  private get() {
		var valid = true
		//  base64image = basse64Converter(product_image);
		//   Log.v("image",base64image);
		product_name_str = product_name.text.toString().trim { it <= ' ' }
		product_desc_str = product_desc.text.toString().trim { it <= ' ' }
		product_price_str = product_price.text.toString().trim { it <= ' ' }
		product_option_str = product_option.text.toString().trim { it <= ' ' }
		product_quantity_str = product_quantity.text.toString().trim { it <= ' ' }
		product_discount_str = product_discount.text.toString().trim { it <= ' ' }
		/*  if (veg_radio_btn.isChecked()) {
		 item_type = veg;
	   }
	   if (non_veg_radio_btn.isChecked()) {
		 item_type = nonveg;
	   }*/
		var pr_image_length = imageslist.size
		if (pr_image_length > 0) {
		    pr_image_length = 0
		    for (i in imageslist.indices) if (imageslist[i].base64 != null) {
			  pr_image_length = 1
			  continue
		    }
		}
		val pr_name_length = product_name_str.length
		val pr_desc_length = product_desc_str.length
		val pr_price_length = product_price_str.length
		val pr_product_option_length = product_option_str.length
		val pr_quantity_length = product_quantity_str.length
		val pr_discount_length = product_discount_str.length
		if (pr_image_length == 0) {
		    UIMsgs.showToast(mContext, "Please provide product image")
		    valid = false
		} else if (pr_name_length == 0) {
		    UIMsgs.setEditTextErrorMethod(product_name, ValidationMessages.EMPTY_NOT_ALLOWED)
		    valid = false
		} else if (pr_name_length <= 3) {
		    UIMsgs.setEditTextErrorMethod(product_name, "Minimum 3 characters required")
		    valid = false
		} else if (pr_desc_length == 0) {
		    UIMsgs.setEditTextErrorMethod(product_desc, ValidationMessages.EMPTY_NOT_ALLOWED)
		    valid = false
		} /*else if (pr_product_option_length == 0) {
            setEditTextErrorMethod(product_option, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_price_length == 0) {
            setEditTextErrorMethod(product_price, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (Double.parseDouble(product_price_str) <= 0) {
            setEditTextErrorMethod(product_price, "INVALID AMOUNT");
            valid = false;
        } else if (product_price_str.equalsIgnoreCase("0")) {
            setEditTextErrorMethod(product_price, INVALID);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }else if (pr_quantity_length == 0) {
            setEditTextErrorMethod(product_quantity, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (pr_discount_length == 0) {
            setEditTextErrorMethod(product_discount, EMPTY_NOT_ALLOWED);
            valid = false;
        }*/ else if (sub_cat_id.isEmpty()) {
		    UIMsgs.showToast(mContext, "Please provide category")
		    valid = false
		} else if (menu_id.isEmpty()) {
		    UIMsgs.showToast(mContext, "Please provide menu")
		    valid = false
		} else if (brand_id == null) {
		    UIMsgs.showToast(mContext, "Please provide brand")
		    valid = false
		}
		return valid
	  }

    override fun onRequestPermissionsResult(
	  requestCode: Int, permissions: Array<String>,
	  grantResults: IntArray
    ) {
	  super.onRequestPermissionsResult(requestCode, permissions, grantResults)
	  when (requestCode) {
		Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
		    checkpermisstiona = true
		    /* if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();*/
		} else {
		    //code for deny
		}

	  }
    }

    private fun createOrUpdateCategory(url: String, data: String?) {

	  //  System.out.println("aaaaaaa data "+url+"  "+data);
	  Log.v("update", data!!)
	  //  LoadingDialog.loadDialog(mContext);
	  customDialog.show()
	  val requestQueue = Volley.newRequestQueue(mContext)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.POST, url,
		Response.Listener { response ->
		    if (response != null) {
			  customDialog.dismiss()
			  //LoadingDialog.dialog.dismiss();
			  Log.d("cc_res", response)
			  try {
				val jsonObject = JSONObject(response)
				println("aaaaaaa  jsonobject  $jsonObject")
				val status = jsonObject.getBoolean("status")
				if (status) {
				    if (Constants.Use_ActivityDesign) {
					  if (status_type == 2) {
						UIMsgs.showToast(mContext, "Updated Successfully")
						MainProductpage.viewpagercurrentposion = 0
						val i =
						    Intent(this@ProductAddOrUpdate, MainProductpage::class.java)
						i.putExtra("currentposition", 3)
						startActivity(i)
						finish()
					  } else {
						UIMsgs.showToast(mContext, "Created Successfully")
						MainProductpage.viewpagercurrentposion = 3
						val i =
						    Intent(this@ProductAddOrUpdate, MainProductpage::class.java)
						i.putExtra("currentposition", 3)
						startActivity(i)
						finish()
					  }
				    } else {
					  if (status_type == 2) UIMsgs.showToast(
						mContext,
						"Updated Successfully"
					  ) else UIMsgs.showToast(mContext, "Created Successfully")
					  Utility.ShowPendingProuctsItem = true
					  fragmentManager.popBackStack()
					  finish()
				    }
				} else {
				    UIMsgs.showToast(mContext, jsonObject.getString("data"))
				}
			  } catch (e: Exception) {
				println("aaaaaaa catch  " + e.message)
				LoadingDialog.dialog.cancel()
				e.printStackTrace()
			  }
		    } else {
			  customDialog.dismiss()
			  // LoadingDialog.dialog.dismiss();
			  UIMsgs.showToast(mContext, ValidationMessages.MAINTENANCE)
		    }
		}, Response.ErrorListener { error ->
		    println("aaaaaaa error  " + error.message)
		    // LoadingDialog.dialog.dismiss();
		    UIMsgs.showToast(mContext, ValidationMessages.OOPS)
		    customDialog.dismiss()
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = token
		    return map
		}

		@Throws(AuthFailureError::class)
		override fun getBody(): ByteArray? {
		    return try {
			  if (data == null) null else data.toByteArray(charset("utf-8"))
		    } catch (e: Exception) {
			  e.printStackTrace()
			  null
		    }
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    fun setremoveProduct(position: Int) {
	  println("aaaaaa size  " + productaddlist.size + "  " + position)
	  productaddlist.removeAt(position)
	  optionsAdapter.setrefresh(productaddlist, 1)
	  optionsAdapter.notifyItemRemoved(position)
	  optionsAdapter.notifyDataSetChanged()
    }

    fun addOptionToProduct(position: Int): ArrayList<String> {
	  var optionList= ArrayList<String>()
	  if (productaddlist != null && productaddlist.size > position) {
		productaddlist[position].addOptionToList("")
		optionList = productaddlist[position].optionlist
	  }
	  return optionList
    }

    fun removeOptionFromProduct(productIndex: Int, position: Int): ArrayList<String?> {
	  var optionList= ArrayList<String?>()
	  if (productaddlist != null && productaddlist.size > productIndex) {
		optionList = productaddlist[productIndex].optionlist
		if (optionList != null && optionList.size > position) {
		    optionList.removeAt(position)
		}
	  }
	  return optionList
    }

    fun deleteimage(s: ImagesModel) {
	  // imageslist.remove(s);
	  // selectedImageAdapter.refresh(imageslist);
	  if (TextUtils.isEmpty(s.id)) {
		imageslist.remove(s)
		selectedImageAdapter.refresh(imageslist)
		selectedImageAdapter.notifyDataSetChanged()
		if (imageslist.size == 0) {
		    // upload_image.setVisibility(View.VISIBLE);
		    // images_recycle.setVisibility(View.GONE);
		    // }else {
		    imageslist.add(ImagesModel())
		    imageslist.add(ImagesModel())
		}
		upload_image.visibility = View.GONE
		images_recycle.visibility = View.VISIBLE
	  } else {
		val jsonObject = JSONObject()
		try {
		    jsonObject.put("image_id", s.id)
		    val data = jsonObject.toString()
		    getDeleteImage(data)
		} catch (e: JSONException) {
		    e.printStackTrace()
		}
	  }
    }

    private fun getDeleteImage(data: String?) {
	  customDialog.show()
	  val requestQueue = Volley.newRequestQueue(mContext)
	  val stringRequest: StringRequest = object : StringRequest(
		Method.POST, Config.DELETEIMAGE,
		Response.Listener { response ->
		    if (response != null) {
			  customDialog.cancel()
			  try {
				val jsonObject = JSONObject(response)
				println("aaaaaaa  product details $jsonObject")
				val status = jsonObject.getBoolean("status")
				if (status) {
				    imageslist.clear()
				    getProductToUpdate(product_id)
				} else {
				    UIMsgs.showToast(mContext, ValidationMessages.OOPS)
				}
			  } catch (e: Exception) {
				println("aaaaaa catch  " + e.message)
				e.printStackTrace()
				UIMsgs.showToast(mContext, ValidationMessages.OOPS)
			  }
		    } else {
			  customDialog.cancel()
			  UIMsgs.showToast(mContext, ValidationMessages.MAINTENANCE)
		    }
		    // LoadingDialog.dialog.dismiss();
		}, Response.ErrorListener {
		    customDialog.cancel()
		    // LoadingDialog.dialog.dismiss();
		}) {
		@Throws(AuthFailureError::class)
		override fun getHeaders(): Map<String, String> {
		    val map: MutableMap<String, String> = HashMap()
		    map["Content-Type"] = "application/json"
		    map[Constants.AUTH_TOKEN] = token
		    return map
		}

		@Throws(AuthFailureError::class)
		override fun getBody(): ByteArray? {
		    return try {
			  data?.toByteArray(charset("utf-8"))
		    } catch (e: Exception) {
			  e.printStackTrace()
			  null
		    }
		}
	  }
	  stringRequest.retryPolicy = DefaultRetryPolicy(
		0,
		DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
		DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
	  )
	  requestQueue.add(stringRequest)
    }

    fun setradiocheck(position: Int, i: Int) {
	  productaddlist[position].activecheck = i
    }

    companion object {
	  fun getClassName(c: Class<*>): String {
		var className = c.name
		val firstChar: Int
		firstChar = className.lastIndexOf('.') + 1
		if (firstChar > 0) {
		    className = className.substring(firstChar)
		}
		return className
	  }
    }
}