package com.nextclick.crm.ShopNowModule.Models;

public class BannerPositionIndex {

    String id,title;

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setTitle(String title) {
        this.title=title;
    }
    public String getTitle() {
        return this.title;
    }

}
