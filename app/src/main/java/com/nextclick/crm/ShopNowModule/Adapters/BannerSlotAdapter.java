package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.BannerAvailability;

import java.util.List;

public class BannerSlotAdapter extends RecyclerView.Adapter<BannerSlotAdapter.ViewHolder> {


    private final List<BannerAvailability> data;
    private final Context context;

    public BannerSlotAdapter(Context addPromotionActivity, List<BannerAvailability> listBannerAvailability) {
        this.context = addPromotionActivity;
        this.data = listBannerAvailability;
    }

    @NonNull
    @Override
    public BannerSlotAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.banner_slot, parent, false);
        return new BannerSlotAdapter.ViewHolder(itemView);
    }

    RadioButton mCurrentlyCheckedRB =null;
    @Override
    public void onBindViewHolder(@NonNull BannerSlotAdapter.ViewHolder holder, final int position) {
        BannerAvailability bannerAvailability = data.get(position);

        holder.radio_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                try {
                    //int radioButtonID = group.getCheckedRadioButtonId();
                    View radioButton = group.findViewById(checkedId);
                    int idx = group.indexOfChild(radioButton);
                    RadioButton r = (RadioButton) group.getChildAt(idx);

                    if(r.isChecked()) {
                        if (mCurrentlyCheckedRB == null) {
                            mCurrentlyCheckedRB = r;
                            mCurrentlyCheckedRB.setChecked(true);
                        } else {
                            mCurrentlyCheckedRB.setChecked(false);
                        }
                        if (mCurrentlyCheckedRB == group.getChildAt(checkedId))
                            return;
                        mCurrentlyCheckedRB = r;
                        mCurrentlyCheckedRB.setChecked(true);
                    }
                }
                catch (Exception ex)
                {
                    Log.e("Radiobutton selection",ex.getLocalizedMessage());
                }
            }
        });

        (holder.radio_group.getChildAt(position)).setEnabled(false);

        // holder.tv_slot.setText(bannerAvailability.getHeader());
        //  holder.tv_next_availability.setText(promotionObject.getSubHeader());

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_slot,tv_next_availability;
        RadioGroup radio_group;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_slot = itemView.findViewById(R.id.tv_slot);
            tv_next_availability = itemView.findViewById(R.id.tv_next_availability);
            radio_group=itemView.findViewById(R.id.radio_group);
        }
    }

}

