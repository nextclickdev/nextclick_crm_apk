package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.JoinPromotionAdapter;
import com.nextclick.crm.ShopNowModule.Models.BannerContentType;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.ShopNowModule.Models.ShopByCategoryObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

import es.dmoral.toasty.Toasty;

public class JoinOfferPromotion extends AppCompatActivity implements PaymentResultWithDataListener {

    RecyclerView recyclerView_shopBycategory;
    private ImageView img_back;
    JoinPromotionAdapter shopByCategoryListAdapter;
    private String promotionID;
    private PromotionObject promotionObject;
    private CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    TextView tv_title,tv_offer_type;
    List<ShopByCategoryObject> listShopByCategoryObject;
    String amount="0";
    Integer promotionOfferType=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_join_offer_promotion);

        img_back=findViewById(R.id.img_back);

        //getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext= JoinOfferPromotion.this;
        mCustomDialog=new CustomDialog(JoinOfferPromotion.this);

        preferenceManager=new PreferenceManager(mContext);
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor Clicked on Join promotion of promotion type -Offer");
        }
        listShopByCategoryObject=new ArrayList<>();
        recyclerView_shopBycategory=findViewById(R.id.recyclerView_shopBycategory);
        recyclerView_shopBycategory.setLayoutManager(new GridLayoutManager(this,1));
        shopByCategoryListAdapter=new JoinPromotionAdapter(JoinOfferPromotion.this,listShopByCategoryObject);
        recyclerView_shopBycategory.setAdapter(shopByCategoryListAdapter);

        tv_title=findViewById(R.id.tv_title);
        tv_offer_type=findViewById(R.id.tv_offer_type);
        Button btn_add_promotion = findViewById(R.id.btn_add_promotion);
        if(getIntent().hasExtra("promotionID")) {
            promotionID = getIntent().getStringExtra("promotionID");
            getPromotionDetails(promotionID);
            getOfferPromotions();
        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_add_promotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                    alertDialogBuilder.setTitle("Razorpay");
                    alertDialogBuilder
                            .setMessage("Do You Want To Procced")
                            .setCancelable(false)
                            .setPositiveButton(R.string.pay, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
//                                    razorPayPayment("" + amount);
                                    joinPromotin(promotionID, "", "");

                                }

                            })
                            /*.setNegativeButton(R.string.pay_later, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    joinPromotin(promotionID, "", "");
                                }
                            })*/;

                   /*         if(Utility.enable_Pay_Later) {
                                alertDialogBuilder.setNegativeButton(R.string.pay_later, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        joinPromotin(promotionID, "", "");
                                    }
                                });
                            }*/

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                }
            }
        });
    }
    JSONArray productsArray,offersArray;
    private boolean  validate() {

        if (listShopByCategoryObject == null || listShopByCategoryObject.size() == 0) {
            Toasty.error(mContext, "Please select the products", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            productsArray = new JSONArray();
            offersArray = new JSONArray();

            try {

                for (int i = 0; i < listShopByCategoryObject.size(); i++) {
                    ShopByCategoryObject cat = listShopByCategoryObject.get(i);

                    if (cat != null && cat.getselectedXProducts() != null
                            && cat.getselectedXProducts().size() > 0) {
                        for (int j = 0; j < cat.getselectedXProducts().size(); j++) {
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("shop_by_category_id", cat.getID());
                            jsonObject1.put("product_id", cat.getselectedXProducts().get(j).getId());
                            jsonObject1.put("product_variant_id", cat.getselectedXProducts().get(j).getProduct_variant_id());
                            productsArray.put(jsonObject1);
                        }
                    }
                    if (cat != null && cat.getselectedYProducts() != null
                            && cat.getselectedYProducts().size() > 0) {
                        for (int j = 0; j < cat.getselectedYProducts().size(); j++) {
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("shop_by_category_id", cat.getID());
                            jsonObject1.put("product_id", cat.getselectedYProducts().get(j).getId());
                            jsonObject1.put("product_variant_id", cat.getselectedYProducts().get(j).getProduct_variant_id());
                            offersArray.put(jsonObject1);
                        }
                    }
                }
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }

            if (productsArray.length() == 0) {
                String error = mContext.getString(R.string.selectXProducts);
                if (promotionOfferType <= 3) {
                     error = mContext.getString(R.string.selectProducts);
                }
                Toasty.error(mContext, error, Toast.LENGTH_SHORT).show();
                return  false;
            }
            if (promotionOfferType > 3) {
                if (offersArray.length() == 0) {
                    String error = mContext.getString(R.string.selectYProducts);
                    if (promotionOfferType == 6) {
                        error = mContext.getString(R.string.selectYZProduct);
                    }
                    Toasty.error(mContext, error, Toast.LENGTH_SHORT).show();
                    return  false;
                }
                else if (promotionOfferType == 6 && offersArray.length() > 2) {
                    Toasty.error(mContext, mContext.getString(R.string.selectMaxYZProduct), Toast.LENGTH_SHORT).show();
                    return  false;
                }
            }
        }
        return true;
    }

    private void getPromotionDetails(String promotionID) {
        mCustomDialog.show();
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("id", promotionID);
        JSONObject json = new JSONObject(uploadMap);
        System.out.println("GET_PROMOTION_BANNERS request" + json);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_PROMOTION_BANNERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_SHOP_BY_CATEGORIES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                JSONObject transobj = dataObj.getJSONObject("banner");

                                promotionObject = new PromotionObject();
                                promotionObject.setID(transobj.getString("id"));
                                promotionObject.setHeader(transobj.getString("title"));
                                tv_title.setText(promotionObject.getHeader());
                                promotionObject.setDescription(transobj.getString("desc"));
                                if(transobj.has("image"))
                                    promotionObject.setIcon(transobj.getString("image"));


                                promotionObject.setcat_id(transobj.getString("cat_id"));
                                promotionObject.setsub_cat_id(transobj.getString("sub_cat_id"));
                                promotionObject.setbrand_id(transobj.getString("brand_id"));
                                promotionObject.setconstituency_id(transobj.getString("constituency_id"));
                                promotionObject.setpromotion_banner_position_id(transobj.getString("promotion_banner_position_id"));
                                if(transobj.has("promotion_banner_discount_type_id")) {
                                    promotionObject.setpromotion_banner_discount_type_id(transobj.getString("promotion_banner_discount_type_id"));

                                    try {
                                        promotionOfferType = transobj.getInt("promotion_banner_discount_type_id");
                                    } catch (Exception ex) {
                                    }
                                }
                                promotionObject.setcontent_type(transobj.getString("content_type"));
                                // promotionObject.setDescription(transobj.getString("created_user_id"));
                                //  promotionObject.setHeader(transobj.getString("updated_user_id"));
                                promotionObject.setpublished_on(transobj.getString("published_on"));
                                promotionObject.setexpired_on(transobj.getString("expired_on"));
                                promotionObject.setowner(transobj.getString("owner"));
                                promotionObject.setstatus(transobj.getString("status"));
                                promotionObject.setAccessibility(transobj.getString("accessibility"));
                                promotionObject.setDiscount(transobj.getString("discount"));

                                try {

                                    if(transobj.has("position"))
                                    {
                                        if(transobj.getJSONObject("position").has("per_day_charge"))
                                        {
                                            amount=transobj.getJSONObject("position").getString("per_day_charge");

                                            Long duration = getTimeLeft(promotionObject.getpublished_on().replace("-", "/"),
                                                    promotionObject.getexpired_on().replace("-", "/"));
                                            if (duration <= 0)
                                                duration = 0L;

                                            amount =""+( Double.parseDouble(amount) * (duration + 1));
                                        }
                                    }
                                }
                                catch (Exception ex){}

                                try {
                                    JSONArray OffersObj = dataObj.getJSONArray("offers_on_shop_by_categories");
                                    for (int i = 0; i < OffersObj.length(); i++) {
                                        JSONObject offer = OffersObj.getJSONObject(i);

                                        ShopByCategoryObject shopByCategoryObject = new ShopByCategoryObject();
                                        shopByCategoryObject.setShopByCategoryID(offer.getString("id"));
                                        shopByCategoryObject.setID(offer.getString("id"));
                                        shopByCategoryObject.setShopByCategoryID(offer.getString("sub_cat_id"));

                                        if(offer.has("details")) {
                                            JSONObject details = offer.getJSONObject("details");

                                            //shopByCategoryObject.setID(details.getString("id"));
                                            shopByCategoryObject.setTitle(details.getString("name"));
                                        }

                                        if (offer.has("image"))
                                            shopByCategoryObject.setImage(offer.getString("image"));
                                        listShopByCategoryObject.add(shopByCategoryObject);
                                    }

                                    if(listShopByCategoryObject.size()> 0) {
                                        shopByCategoryListAdapter.refresh(listShopByCategoryObject,promotionObject);
                                    }
                                }
                                catch (Exception dd){}

                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            mCustomDialog.dismiss();
                            setSelection();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    public void joinPromotin(String promotionID,String Payment_gateway_id,String amount) {
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("txn_id", Payment_gateway_id);
        uploadMap.put("amount", amount);

        if (promotionID != null && !promotionID.isEmpty()) {
            uploadMap.put("promotion_banner_id", promotionID);
        }


        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor joined the promotion of "+promotionID);
        }

        JSONObject json = new JSONObject(uploadMap);
        if (listShopByCategoryObject != null && listShopByCategoryObject.size() > 0) {
            try {

                if(productsArray.length()>0)
                json.put("products", productsArray);
                if(offersArray.length()>0)
                json.put("offer_products", offersArray);
                //json.put("products", productsArray);
            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }
        }

        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.JOIN_PROMOTION,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                Toast.makeText(mContext, "Promotion is successfully activated.", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else {
                                String error=jsonObject.getString("message");
                                if(error!=null && !error.isEmpty())
                                Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
                                else{
                                    Toast.makeText(mContext, "Promotion is successfully activated.", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void razorPayPayment(String totatlAmount) {
        final Checkout co = new Checkout();
        co.setImage(R.mipmap.ic_launcher_round);
        Activity activity = this;
        try {
            JSONObject orderRequest = new JSONObject();
            if(Utility.Skip_Final_PAYMENT)
                orderRequest.put("amount", 100); //totatlAmount * 100
            else
                orderRequest.put("amount", ""+(Double.parseDouble(totatlAmount)*100)); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", "order_rcptid_11");
            orderRequest.put("payment_capture", false);
            orderRequest.put("image",R.mipmap.ic_launcher_round);

            JSONObject readOnly = new JSONObject();
            readOnly.put("email",true);
            readOnly.put("contact",true);
            orderRequest.put("readOnly", readOnly);

            co.open(activity, orderRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
            // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("razorpay  JSONException "+e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, ""+ e, Toast.LENGTH_SHORT).show();
            // UImsgs.showToast(mContext, String.valueOf(e));
            System.out.println("razorpay  Exception "+e.getMessage());
        }


    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        System.out.println("razorpay  data sucess  " + s + paymentData.toString());
        joinPromotin(promotionID,paymentData.getPaymentId(), "" + amount);
    }
    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        try{
            Toast.makeText(this, "Payment has been failed with error: "+paymentData.toString(), Toast.LENGTH_SHORT).show();
        }catch (NullPointerException e){

        }
        System.out.println("razorpay  payment  error  "+s);
    }
    public long getTimeLeft(String startdate,String enddate) {
        long elapsedDays =0;
        try {

            SimpleDateFormat mdformat = new SimpleDateFormat("yyyy/MM/dd");
            Date startDate = null, endDate = null;
            startDate = mdformat.parse(startdate);
            endDate = mdformat.parse(enddate);


            //milliseconds
            long different = endDate.getTime() - startDate.getTime();

            System.out.println("startDate : " + startDate);
            System.out.println("endDate : " + endDate);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            elapsedDays = different / daysInMilli;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return elapsedDays;
    }

    List<BannerContentType> listOfferPromotions;
    private void getOfferPromotions() {

        listOfferPromotions = new ArrayList<>();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_DISCOUNT_TYPES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_DISCOUNT_TYPES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");
                                listOfferPromotions = new ArrayList<>();


                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject transobj = dataobj.getJSONObject(i);

                                    com.nextclick.crm.ShopNowModule.Models.BannerContentType
                                            bannerContentType = new BannerContentType();
                                    bannerContentType.setID(transobj.getString("id"));
                                    bannerContentType.setName(transobj.getString("name"));

                                    listOfferPromotions.add(bannerContentType);
                                }
                                setSelection();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void setSelection() {


        if (promotionObject != null && promotionObject.getpromotion_banner_discount_type_id() != null &&
                !promotionObject.getpromotion_banner_discount_type_id().isEmpty() &&
                !promotionObject.getpromotion_banner_discount_type_id().equals("null")) {
            if (listOfferPromotions != null &&
                    listOfferPromotions.size() > 0) {

                for (int i = 0; i < listOfferPromotions.size(); i++) {
                    if (listOfferPromotions.get(i).getID().equals(promotionObject.getpromotion_banner_discount_type_id())) {
                        String extra="";
                        if(promotionObject.getpromotion_banner_discount_type_id().equals("1") || promotionObject.getpromotion_banner_discount_type_id().equals("2")) {
                            extra = " - " + "<font color=#800000>"+(promotionObject.getpromotion_banner_discount_type_id().equals("1")?"":getString(R.string.Rs))+promotionObject.getDiscount()+(promotionObject.getpromotion_banner_discount_type_id().equals("1")?"%":"")+"</font>";
                        }
                        tv_offer_type.setText(Html.fromHtml(listOfferPromotions.get(i).getName() +extra));
                        break;
                    }
                }
            }
        }
    }
}