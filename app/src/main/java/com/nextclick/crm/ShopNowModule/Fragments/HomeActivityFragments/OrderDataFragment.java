package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.OrdersAdapter;
import com.nextclick.crm.ShopNowModule.Models.OrderItems;
import com.nextclick.crm.ShopNowModule.Models.OrderModel;
import com.nextclick.crm.ShopNowModule.Models.SubOrderItems;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class OrderDataFragment extends Fragment {


    private RecyclerView orders_recycler;
    private Context mContext;
    PreferenceManager preferenceManager;
    private View root;
    private String token;
    String orders_url, start_date_str, end_date_str;
    private ArrayList<OrderModel> orderModelArrayList;
    private OrdersAdapter ordersAdapter = null;
    TextView emptyView;


    public OrderDataFragment(String orders_url, String start_date_str, String end_date_str) {
        // Required empty public constructor
        this.orders_url = orders_url;
        this.start_date_str = start_date_str;
        this.end_date_str = end_date_str;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_order_data, container, false);
        initView();
        if (getActivity() != null) {
            getOrders();
        }
        return root;
    }

    private void getOrders() {
        String url = "";
        if (start_date_str != null && end_date_str != null && !start_date_str.equalsIgnoreCase("") && !end_date_str.equalsIgnoreCase("")) {
            url = orders_url + "?start_date=" + start_date_str + "&end_date=" + end_date_str;
        } else {
            url = orders_url;
        }
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("orders_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    //JSONArray resultArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    Object aObj = dataObject.get("result");
                                    if (!(aObj instanceof JSONArray) || aObj instanceof JSONObject) {
                                        orders_recycler.setVisibility(View.GONE);
                                        emptyView.setVisibility(View.VISIBLE);
                                        return;
                                    }
                                    JSONArray resultArray =(JSONArray) aObj;
                                    if (resultArray.length() > 0) {
                                        orderModelArrayList = new ArrayList<>();
                                        for (int i = 0; i < resultArray.length(); i++) {
                                            OrderModel orderModel = new OrderModel();
                                            JSONObject orderObject = resultArray.getJSONObject(i);
                                            orderModel.setId(orderObject.getString("id"));
                                            orderModel.setDiscount(orderObject.getInt("discount"));
                                            orderModel.setDelivery_fee(orderObject.getInt("delivery_fee"));
                                            orderModel.setPayment_method_id(orderObject.getInt("payment_method_id"));
                                            orderModel.setPayment_name(orderObject.getJSONObject("payment_method").getString("name"));
                                            orderModel.setTax(orderObject.getInt("tax"));
                                            orderModel.setTotal(orderObject.getInt("total"));
                                            orderModel.setOrder_track(orderObject.getString("order_track"));
                                            orderModel.setOrder_status(orderObject.getInt("order_status"));
                                            orderModel.setDelivery(orderObject.getInt("delivery"));
                                            orderModel.setRejected_reason(orderObject.getString("rejected_reason"));
                                            orderModel.setOtp(orderObject.getString("otp"));
                                            orderModel.setFirst_name(orderObject.getJSONObject("user").getString("first_name"));
                                            String orderItemname = " ";
                                            try {
                                                ArrayList<OrderItems> orderItemsArrayList = new ArrayList<>();
                                                JSONArray orderItemsArray = orderObject.getJSONArray("order_items");
                                                for (int j = 0; j < orderItemsArray.length(); j++) {
                                                    JSONObject orderItemObject = orderItemsArray.getJSONObject(j);
                                                    OrderItems orderItems = new OrderItems();
                                                    orderItems.setItemId(orderItemObject.getString("item_id"));
                                                    orderItems.setOrderId(orderItemObject.getString("order_id"));
                                                    orderItems.setPrice(orderItemObject.getInt("price"));
                                                    orderItems.setQuantity(orderItemObject.getInt("quantity"));
                                                    try {
                                                        orderItems.setItemName(orderItemObject.getString("name"));
                                                        orderItemname = orderItemname + orderItemObject.getString("name") + ", ";
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    orderItemsArrayList.add(orderItems);
                                                }

                                                orderModel.setOrder_items(orderItemname);

                                                orderModel.setOrderItemsList(orderItemsArrayList);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            String suborderItemname = " ";
                                            try {
                                                ArrayList<SubOrderItems> suborderItemsArrayList = new ArrayList<>();
                                                JSONArray orderItemsArray = orderObject.getJSONArray("sub_order_items");
                                                for (int j = 0; j < orderItemsArray.length(); j++) {
                                                    JSONObject sub_item_object = orderItemsArray.getJSONObject(j);
                                                    SubOrderItems orderItems = new SubOrderItems();
                                                    orderItems.setItem_id(sub_item_object.getString("item_id"));
                                                    orderItems.setSec_item_id(sub_item_object.getString("sec_item_id"));
                                                    orderItems.setOrder_id(sub_item_object.getString("order_id"));
                                                    orderItems.setPrice(sub_item_object.getInt("price"));
                                                    orderItems.setQuantity(sub_item_object.getInt("quantity"));
                                                    try {
                                                        orderItems.setName(sub_item_object.getString("name"));
                                                        suborderItemname = suborderItemname + sub_item_object.getString("name") + ", ";
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    orderModel.setSub_order_items(suborderItemname);
                                                    suborderItemsArrayList.add(orderItems);

                                                }
                                                orderModel.setSubOrderItemsArrayList(suborderItemsArrayList);


                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            orderModelArrayList.add(orderModel);
                                        }

                                        ordersAdapter = new OrdersAdapter(mContext, orderModelArrayList);

                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }

                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }

                                        };
                                        orders_recycler.setLayoutManager(layoutManager);
                                        orders_recycler.setAdapter(ordersAdapter);
                                        if (orderModelArrayList.isEmpty()) {
                                            orders_recycler.setVisibility(View.GONE);
                                            emptyView.setVisibility(View.VISIBLE);
                                        }
                                        else {
                                            orders_recycler.setVisibility(View.VISIBLE);
                                            emptyView.setVisibility(View.GONE);
                                        }


                                    }
                                }


                            } catch (Exception e) {
                                //LoadingDialog.dialog.dismiss();
                                e.printStackTrace();
                                UIMsgs.showToast(mContext, "Orders Not Available");
                            }
                        } else {
                            //LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                        LoadingDialog.dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //LoadingDialog.dialog.dismiss();
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void initView() {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        orders_recycler = root.findViewById(R.id.orders_recycler);
        emptyView= root.findViewById(R.id.empty_view);
    }
}