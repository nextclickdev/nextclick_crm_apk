package com.nextclick.crm.ShopNowModule.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.SubscriptionPlan;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;

import java.util.ArrayList;

public class SettingPackageAdapter extends RecyclerView.Adapter<SettingPackageAdapter.ViewHolder> {

    private int parentPos=-1;
    Context context;
    ArrayList<SubscriptionPlan> listOFNotificationDetails;
    public MediaPlayer mp;
    SharedPreferences preferenceManager;



    public SettingPackageAdapter(Context context, ArrayList<SubscriptionPlan> subscriptionPlans, int position) {
        this.context =context;
        this.listOFNotificationDetails =subscriptionPlans;
        preferenceManager= PreferenceManager.getDefaultSharedPreferences(context);
        this.parentPos=position;
    }

    @NonNull
    @Override
    public SettingPackageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_privilege_setting, parent, false);
        return new SettingPackageAdapter.ViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull SettingPackageAdapter.ViewHolder holder, int position) {

        holder.chk_state.setVisibility(View.GONE);
        holder.unchk_state.setVisibility(View.GONE);

        holder.chk_state1.setVisibility(View.GONE);
        holder.unchk_state1.setVisibility(View.GONE);

        holder.tv_name.setVisibility(View.GONE);
        if(parentPos==0) {
            holder.tv_name.setVisibility(View.VISIBLE);
            holder.tv_name.setText(listOFNotificationDetails.get(position).getTitle());
        }
        else {

            if (listOFNotificationDetails.get(position).getIsActive().equals("1")) {
                holder.layout_panel.setBackgroundColor(context.getColor(R.color.palaegreen));
                try {
                    if (listOFNotificationDetails.get(position).getSubscriptionSetting().get(parentPos).getStatus()) {
                        holder.chk_state.setVisibility(View.VISIBLE);
                    } else {
                        holder.unchk_state.setVisibility(View.VISIBLE);
                    }
                }catch (Exception ex)
                {
                    holder.unchk_state.setVisibility(View.VISIBLE);
                }
            } else {
                holder.layout_panel.setBackgroundColor(context.getColor(R.color.gray));
                try {
                if (listOFNotificationDetails.get(position).getSubscriptionSetting().get(parentPos).getStatus()) {
                    holder.chk_state1.setVisibility(View.VISIBLE);
                } else {
                    holder.unchk_state1.setVisibility(View.VISIBLE);
                }
                }catch (Exception ex)
                {
                    holder.unchk_state.setVisibility(View.VISIBLE);
                }
            }


        }
    }

    @Override
    public int getItemCount() {
        return listOFNotificationDetails!=null? listOFNotificationDetails.size(): 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final ImageView chk_state;
        private final ImageView unchk_state;
        private final ImageView chk_state1;
        private final ImageView unchk_state1;
        TextView tv_name;
        LinearLayout layout_panel;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            chk_state = itemView.findViewById(R.id.chk_state);
            unchk_state= itemView.findViewById(R.id.unchk_state);
            chk_state1 = itemView.findViewById(R.id.chk_state1);
            unchk_state1= itemView.findViewById(R.id.unchk_state1);
            layout_panel = itemView.findViewById(R.id.layout_panel);
        }
    }

}
