package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Constants.ValidationMessages;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ViewSubscriptionPrivileges;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.NotificationSoundsActivity;
import com.nextclick.crm.activities.TermsAndConditionsActivity;
import com.nextclick.crm.dialogs.Language_Dialog;
import com.nextclick.crm.dialogs.Logout_Dialog;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.VendorSettingsResponse.VendorSettingsResponse;
import com.nextclick.crm.models.responseModels.VendorSettingsResponse.VendorSettingsUpdateResponse;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SETTINGSREAD;
import static com.nextclick.crm.Config.Config.SETTINGSUPDATE;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SettingsFragment extends Fragment implements HttpReqResCallBack {

    private final Boolean isProductManagementAvailable;
    private Button btnUpdateDetails,action_change_language,action_change_notification,action_terms_conditions,action_subscription_settings,logout;
    private PreferenceManager preferenceManager;
    private TextInputEditText tvMinOrderPrice, tvDeliveryFreeRange, tvMinDeliveryFee, tvExtraDeliveryFee, tvTax;

    private String id = "";
    private String vendorID = "";


    private CustomDialog mCustomDialog;

    public SettingsFragment(Boolean isProductManagementAvailable) {
        this.isProductManagementAvailable = isProductManagementAvailable;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        getSettingsDetails();
        return view;
    }


    private void initializeUi(View view) {
        tvTax = view.findViewById(R.id.tvTax);
        btnUpdateDetails = view.findViewById(R.id.btnUpdateDetails);
        tvMinOrderPrice = view.findViewById(R.id.tvMinOrderPrice);
        tvMinDeliveryFee = view.findViewById(R.id.tvMinDeliveryFee);
        tvExtraDeliveryFee = view.findViewById(R.id.tvExtraDeliveryFee);
        tvDeliveryFreeRange = view.findViewById(R.id.tvDeliveryFreeRange);
        action_change_language = view.findViewById(R.id.action_change_language);
        action_change_notification = view.findViewById(R.id.action_change_notification);
        action_subscription_settings = view.findViewById(R.id.action_subscription_settings);
        action_terms_conditions = view.findViewById(R.id.action_terms_conditions);
        logout = view.findViewById(R.id.logout);
        action_subscription_settings.setVisibility(View.GONE);
        mCustomDialog = new CustomDialog(getContext());
        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
        }

        if(!isProductManagementAvailable)
        {
            btnUpdateDetails.setEnabled(false);
            btnUpdateDetails.setClickable(false);
            btnUpdateDetails.setAlpha(0.5f);
        }

        action_change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Language_Dialog language_dialog = new Language_Dialog(getContext(), (AppCompatActivity) getActivity());
                language_dialog.showDialog();
            }
        });
        action_change_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), NotificationSoundsActivity.class);
                startActivity(i);
            }

        });
        action_subscription_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ViewSubscriptionPrivileges.class);
                startActivity(i);
            }
        }); action_terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), TermsAndConditionsActivity.class);
                startActivity(i);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout_Dialog logout_dialog = new Logout_Dialog(getContext(), 1);
                logout_dialog.showDialog();
            }
        });
        getsettings();
    }

    private void initializeListeners() {
        btnUpdateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvMinOrderPrice.getText().toString()==null ||tvMinOrderPrice.getText().toString().trim().isEmpty())
                {
                    tvMinOrderPrice.setError(ValidationMessages.EMPTY);
                    tvMinOrderPrice.requestFocus();
                    return;
                }
                LoadingDialog.loadDialog(getActivity());
               // token = "Bearer " + preferenceManager.getString(USER_TOKEN);
                updatesettings(tvMinOrderPrice.getText().toString().trim());
              //  UpdateVendorSettingsApiCall.serviceCalToUpdateVendorSettings(getActivity(), SettingsFragment.this, null, token, vendorID, Objects.requireNonNull(tvMinOrderPrice.getText()).toString(), tvDeliveryFreeRange.getText().toString(), tvMinDeliveryFee.getText().toString(), tvExtraDeliveryFee.getText().toString(), tvTax.getText().toString());
            }
        });
    }

    private void getSettingsDetails() {
        LoadingDialog.loadDialog(getActivity());
        //token = "Bearer " + preferenceManager.getString(USER_TOKEN);
       // GetVendorSettingsApiCall.serviceCalToGetVendorSettings(getActivity(), this, null, token);
    }

    public void getsettings(){
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SETTINGSREAD,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject settings "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");

                                JSONArray jsonArray=jsonObject.getJSONArray("data");
                                JSONObject jsonObject1=jsonArray.getJSONObject(0);
                                String min_stock=jsonObject1.getString("min_stock");
                                 id=jsonObject1.getString("id");
                                tvMinOrderPrice.setText(min_stock);
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(getContext(), "API is failed with "+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void updatesettings(String minimumprice){

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("min_stock", minimumprice);
        uploadMap.put("id", id);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request settings "+ json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SETTINGSUPDATE,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject ettings "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                Toast.makeText(getContext(), ""+message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                            Toast.makeText(getContext(), "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(getContext(), "Update Stock API failed with "+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_VENDOR_SETTINGS:
                if (jsonResponse != null) {
                    try {
                        VendorSettingsResponse vendorSettingsResponse = new Gson().fromJson(jsonResponse, VendorSettingsResponse.class);
                        if (vendorSettingsResponse != null) {
                            boolean status = vendorSettingsResponse.status;
                            if (status) {
                                vendorID = String.valueOf(vendorSettingsResponse.data.shopSettings.vendorId);
                                String tax = String.valueOf(vendorSettingsResponse.data.shopSettings.tax);
                                String minOrderValue = String.valueOf(vendorSettingsResponse.data.shopSettings.minOrderPrice);
                                String minDeliveryFee = String.valueOf(vendorSettingsResponse.data.shopSettings.minDeliveryFee);
                                String extraDeliveryFee = String.valueOf(vendorSettingsResponse.data.shopSettings.extraDeliveryFee);
                                String deliveryFreeRange = String.valueOf(vendorSettingsResponse.data.shopSettings.deliveryFreeRange);
                                tvTax.setText(tax);
                                tvMinOrderPrice.setText(minOrderValue);
                                tvMinDeliveryFee.setText(minDeliveryFee);
                                tvExtraDeliveryFee.setText(extraDeliveryFee);
                                tvDeliveryFreeRange.setText(deliveryFreeRange);
                            }
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_UPDATE_VENDOR_SETTINGS:
                if (jsonResponse != null) {
                    try {
                        VendorSettingsUpdateResponse vendorSettingsUpdateResponse = new Gson().fromJson(jsonResponse, VendorSettingsUpdateResponse.class);
                        if (vendorSettingsUpdateResponse != null) {
                            boolean status = vendorSettingsUpdateResponse.status;
                            String message = vendorSettingsUpdateResponse.message;
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            default:
                break;
        }
    }

}
