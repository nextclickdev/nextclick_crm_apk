package com.nextclick.crm.ShopNowModule.Models;

import java.util.ArrayList;

public class OrderModel {


    String id;
    int discount;
    int delivery_fee;
    int payment_method_id;
    String payment_name;
    int tax;
    int total;
    String order_track;
    int order_status;
    int delivery;
    String otp;
    String rejected_reason;
    String first_name;
    String order_items;
    String sub_order_items;
    ArrayList<OrderItems> orderItemsList;
    ArrayList<SubOrderItems> subOrderItemsArrayList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(int delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public int getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(int payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    public int getTax() {
        return tax;
    }

    public void setTax(int tax) {
        this.tax = tax;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getOrder_track() {
        return order_track;
    }

    public void setOrder_track(String order_track) {
        this.order_track = order_track;
    }

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getRejected_reason() {
        return rejected_reason;
    }

    public void setRejected_reason(String rejected_reason) {
        this.rejected_reason = rejected_reason;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getOrder_items() {
        return order_items;
    }

    public void setOrder_items(String order_items) {
        this.order_items = order_items;
    }

    public String getSub_order_items() {
        return sub_order_items;
    }

    public void setSub_order_items(String sub_order_items) {
        this.sub_order_items = sub_order_items;
    }

    public ArrayList<OrderItems> getOrderItemsList() {
        return orderItemsList;
    }

    public void setOrderItemsList(ArrayList<OrderItems> orderItemsList) {
        this.orderItemsList = orderItemsList;
    }

    public ArrayList<SubOrderItems> getSubOrderItemsArrayList() {
        return subOrderItemsArrayList;
    }

    public void setSubOrderItemsArrayList(ArrayList<SubOrderItems> subOrderItemsArrayList) {
        this.subOrderItemsArrayList = subOrderItemsArrayList;
    }
}

