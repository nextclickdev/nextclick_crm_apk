package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.PromotionsAdapter;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class MyPromotionsActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etSearch;
    private ImageView ivSearch;
    TextView tv_no_data;
    private String searchString;
    private RecyclerView recyclerview_my_promotions,recyclerview_promotions;
    private PromotionsAdapter myPromotionsAdapter,myPromotionsAdapter1;
    private FloatingActionButton add_promotions;
    private Context mContext;
    LinearLayout layout_promotions,layout_my_promotions;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    Boolean isMyPromotions=false;
    List<PromotionObject> listMyPromotions;
    List<PromotionObject> listMyPromotions1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_my_promotions);
       // getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        if(getIntent().hasExtra("isMyPromotions"))
        {
            isMyPromotions=getIntent().getBooleanExtra("isMyPromotions",false);
        }
        mContext=this;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(MyPromotionsActivity.this);
        listMyPromotions = new ArrayList<>();
        listMyPromotions1 = new ArrayList<>();
        init();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor Clicked on Promotions history");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPromotions();
    }

    private void init() {
        recyclerview_my_promotions = findViewById(R.id.recyclerview_my_promotions);
        myPromotionsAdapter= new PromotionsAdapter(this,listMyPromotions,false,
                true);
        recyclerview_promotions = findViewById(R.id.recyclerview_promotions);
        myPromotionsAdapter1= new PromotionsAdapter(this,listMyPromotions1,false,
                true);
       tv_no_data = findViewById(R.id.tv_no_data);
       // recyclerview_my_promotions.setLayoutManager(new LinearLayoutManager(this));


        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recyclerview_promotions.setLayoutManager(staggeredGridLayoutManager);

        recyclerview_my_promotions.setAdapter(myPromotionsAdapter);
        recyclerview_promotions.setAdapter(myPromotionsAdapter1);


        add_promotions = findViewById(R.id.add_promotions);
        add_promotions.setOnClickListener(this);
        ImageView img_back=findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.add_promotions:
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }

    private void getPromotions() {
        listMyPromotions.clear();
        listMyPromotions1.clear();
        mCustomDialog.show();
        JSONObject requestBody =null;
        if (searchString != null && !searchString.isEmpty()) {
            Map<String, String> uploadMap = new HashMap<>();
            uploadMap.put("q", searchString);
            requestBody  = new JSONObject(uploadMap);
            System.out.println("aaaaaaaaa request" + requestBody);
        }

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        JSONObject finalRequestBody = requestBody;
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.GET_PROMOTION_HISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_PROMOTION_BANNERS response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                JSONArray bannersArray = dataObj.getJSONArray("banners");
                                for (int i = 0; i < bannersArray.length(); i++) {
                                    try
                                    {
                                        PromotionObject promotionObject = new PromotionObject();
                                        JSONObject banObj = bannersArray.getJSONObject(i);

                                        if (banObj.has("joined_user_id"))
                                            promotionObject.setJoined_user_id(banObj.getString("joined_user_id"));

                                        JSONObject transobj = banObj.getJSONObject("details");
                                        promotionObject.setID(transobj.getString("id"));
                                        if (transobj.has("title"))
                                            promotionObject.setHeader(transobj.getString("title"));
                                        if (transobj.has("desc"))
                                            promotionObject.setDescription(transobj.getString("desc"));

                                        if (transobj.has("offer_title"))
                                            promotionObject.setOfferTile(transobj.getString("offer_title"));
                                        if (transobj.has("offer_details"))
                                            promotionObject.setOfferDetails(transobj.getString("offer_details"));

                                        if (banObj.has("banner_image"))
                                            promotionObject.setIcon(banObj.getString("banner_image"));

                                        promotionObject.setcat_id(transobj.getString("cat_id"));
                                        promotionObject.setsub_cat_id(transobj.getString("sub_cat_id"));
                                        promotionObject.setbrand_id(transobj.getString("brand_id"));
                                        promotionObject.setconstituency_id(transobj.getString("constituency_id"));
                                        promotionObject.setpromotion_banner_position_id(transobj.getString("promotion_banner_position_id"));
                                        promotionObject.setcontent_type(transobj.getString("content_type"));
                                        // promotionObject.setDescription(transobj.getString("created_user_id"));
                                        //  promotionObject.setHeader(transobj.getString("updated_user_id"));
                                        promotionObject.setpublished_on(transobj.getString("published_on"));
                                        promotionObject.setexpired_on(transobj.getString("expired_on"));
                                        promotionObject.setowner(transobj.getString("owner"));
                                        promotionObject.setstatus(transobj.getString("status"));
                                        promotionObject.setAccessibility(transobj.getString("accessibility"));

                                        if (promotionObject.getcontent_type().equals("3"))
                                            listMyPromotions.add(promotionObject);
                                        else
                                            listMyPromotions1.add(promotionObject);
                                    }catch (Exception ex)
                                    {

                                    }
                                }
                               // listMyPromotions.addAll(listMyPromotions1);
                                if (listMyPromotions.size() > 0 || listMyPromotions1.size() > 0) {

                                    Collections.sort(listMyPromotions);

                                    myPromotionsAdapter.setRefresh(listMyPromotions);

                                    myPromotionsAdapter1.setRefresh(listMyPromotions1);
                                }
                                else {
                                    recyclerview_my_promotions.setVisibility(View.GONE);
                                    recyclerview_promotions.setVisibility(View.GONE);
                                    tv_no_data.setVisibility(View.VISIBLE);
                                }
                            }

                        } catch (JSONException e) {
                            System.out.println("aaaaaaaaaa   catch " + e);
                            if (listMyPromotions.size() > 0)
                                myPromotionsAdapter.setRefresh(listMyPromotions);
                            else {
                                recyclerview_my_promotions.setVisibility(View.GONE);
                                tv_no_data.setVisibility(View.VISIBLE);
                            }
                        }
                        finally {
                            mCustomDialog.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return finalRequestBody == null ? null : finalRequestBody.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };


        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void deletePrmotion(String promotionID) {
        Map<String, String> uploadMap = new HashMap<>();
        if (promotionID != null && !promotionID.isEmpty()) {

            uploadMap.put("id", promotionID);
        }

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.DELETE_PROMOTION_BANNERS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                Toast.makeText(mContext, "Promotion Deleted successfully.", Toast.LENGTH_SHORT).show();
                                getPromotions();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void joinPromotin(String promotionID) {
        Map<String, String> uploadMap = new HashMap<>();
        if (promotionID != null && !promotionID.isEmpty()) {

            uploadMap.put("promotion_banner_id", promotionID);
        }

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();


        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.JOIN_PROMOTION,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");

                            if (status) {
                                Toast.makeText(mContext, "Promotion Deleted successfully.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}