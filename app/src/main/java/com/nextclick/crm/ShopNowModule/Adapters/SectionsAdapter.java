package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.CreateAndUpdateSectionActivity;
import com.nextclick.crm.ShopNowModule.Models.SectionModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SECTION_D;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class SectionsAdapter extends RecyclerView.Adapter<SectionsAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private final Transformation transformation;

    private final List<SectionModel> data;
    private final List<SectionModel> data_full;

    private String token = "";

    public SectionsAdapter(Context context, List<SectionModel> itemPojos) {
        this.data = itemPojos;
        this.context = context;
        data_full = new ArrayList<>(itemPojos);
        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();

    }

    @NonNull
    @Override
    public SectionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_section, parent, false);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new SectionsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SectionsAdapter.ViewHolder holder, final int position) {
        final SectionModel sectionModel = data.get(position);

        String sectionName = sectionModel.getName();
        String menuName = sectionModel.getMenu_name();
        String productName = sectionModel.getProductName();

        holder.tvMenu.setText(menuName);
        holder.tvProduct.setText(productName);
        holder.tvSectionName.setText(sectionName);
    }

    private void deleteSection(final List<SectionModel> data, final int position, String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SECTION_D + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("sec_del_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(context, "Deleted");
                                    data.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    UIMsgs.showToast(context, "Unable to delete. Please try later");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                UIMsgs.showToast(context, OOPS);
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(context, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SectionModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (SectionModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                data.clear();
                data.addAll((List) results.values);
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivDelete;
        private final TextView tvSectionName;
        private final TextView tvCategory;
        private final TextView tvMenu;
        private final TextView tvProduct;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMenu = itemView.findViewById(R.id.tvMenu);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvProduct = itemView.findViewById(R.id.tvProduct);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvSectionName = itemView.findViewById(R.id.tvSectionName);
            ivDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.ivDelete) {
                showDeleteAlertDialog(getLayoutPosition());
            } else {
                final SectionModel sectionModel = data.get(getLayoutPosition());
                Intent intent = new Intent(context, CreateAndUpdateSectionActivity.class);
                intent.putExtra("section_id", sectionModel.getId());
                intent.putExtra("type", "u");
                context.startActivity(intent);
            }
        }
    }

    private void showDeleteAlertDialog(final int layoutPosition) {
        final SectionModel sectionModel = data.get(layoutPosition);
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Are you sure to remove..?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteSection(data, layoutPosition, sectionModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}

