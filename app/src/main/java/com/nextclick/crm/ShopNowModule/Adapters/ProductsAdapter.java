package com.nextclick.crm.ShopNowModule.Adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.productDetails.activities.ProductUpdateActivity;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.PRODUCT_C;
import static com.nextclick.crm.Config.Config.PRODUCT_D;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private final Transformation transformation;

    private final List<ProductModel> data;
    private final List<ProductModel> data_full;

    private String token;


    public ProductsAdapter(Context context, List<ProductModel> itemPojos) {
        this.context = context;
        this.data = itemPojos;
        data_full = new ArrayList<>(itemPojos);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }


    @NonNull
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_product_item, parent, false);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new ProductsAdapter.ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ViewHolder holder, final int position) {
        final ProductModel productModel = data.get(position);

        String productName = productModel.getName();
        String productDescription = productModel.getDesc();
        int productPrice = productModel.getPrice();
        int discountPrice = productModel.getDiscount();
        String menuName = productModel.getMenu_name();
        String productImageUrl = productModel.getProduct_image();
        String categoryName = productModel.getCategoryName();
        String status = productModel.getAvailabilityStatus();

        holder.tvMenu.setText(menuName);
        holder.tvCategory.setText(categoryName);
        holder.tvProductName.setText(productName);
        holder.tvProductDescription.setText(Html.fromHtml(productDescription));
        holder.tvPrice.setText(context.getString(R.string.price_symbol) + " " + productPrice);
        holder.tvDiscount.setText(context.getString(R.string.price_symbol) + " " + discountPrice);

        if (status.equalsIgnoreCase("1")) {
            holder.tvProductAvailability.setText(context.getString(R.string.in_stock));
            holder.tvProductAvailability.setTextColor(Color.parseColor("#1B5E20"));
        } else {
            holder.tvProductAvailability.setText(context.getString(R.string.out_of_stock));
            holder.tvProductAvailability.setTextColor(Color.parseColor("#B71C1C"));
        }

        if (productImageUrl != null) {
            if (!productImageUrl.isEmpty()) {
                Picasso.get()
                        .load(productImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.ivProductImage);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.ivProductImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(transformation)
                    .into(holder.ivProductImage);
        }
    }

    private void deleteProduct(final List<ProductModel> data, final int position, String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String deleteProductUrl = PRODUCT_D + id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, deleteProductUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(context, "Deleted");
                                    data.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    UIMsgs.showToast(context, "Unable to delete. Please try later");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ProductModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ProductModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            try {
                data.clear();
                data.addAll((List) results.values);
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivProductImage;
        private final ImageView ivDelete;
        private final TextView tvProductName;
        private final TextView tvProductDescription;
        private final TextView tvCategory;
        private final TextView tvMenu;
        private final TextView tvPrice;
        private final TextView tvDiscount;
        private final TextView tvProductAvailability;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMenu = itemView.findViewById(R.id.tvMenu);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);
            tvProductAvailability = itemView.findViewById(R.id.tvProductAvailability);

            ivDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.ivDelete) {
                showDeleteAlertDialog(getLayoutPosition());
            } else {
                final ProductModel productModel = data.get(getLayoutPosition());
                /*Intent intent = new Intent(context, ProductAddOrUpdate.class);
                intent.putExtra("product_id", productModel.getId());
                intent.putExtra("type", "u");*/
                Intent intent = new Intent(context, ProductUpdateActivity.class);
                intent.putExtra("product_id", productModel.getId());
                intent.putExtra("previous_activity", "OrdersFragment");
               // intent.putExtra("currentposition",currentposition);
                context.startActivity(intent);
            }
        }

    /*    @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ProductModel productModel = data.get(getLayoutPosition());
            int status = productModel.getStatus();
            if (isChecked) {
                if (status == 2) {
                    productModel.setStatus(1);
                    serviceCallToUpdateStatus(productModel.getId(), 1);
                }
            } else {
                if (status == 1) {
                    productModel.setStatus(2);
                    serviceCallToUpdateStatus(productModel.getId(), 2);
                }
            }
        }
    }*/

        private void serviceCallToUpdateStatus(String productId, int status) {
            LoadingDialog.loadDialog(context);
            Map<String, String> uploadMap = new HashMap<>();
            uploadMap.put("id", productId);
            uploadMap.put("status", String.valueOf(status));
            String url = PRODUCT_C;
            final String jsonObject = new JSONObject(uploadMap).toString();
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response != null) {
                                Log.d("asd", response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(context, OOPS);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN, token);
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonObject.getBytes(StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }

        private void showDeleteAlertDialog(final int position) {
            final ProductModel productModel = data.get(position);
            new AlertDialog.Builder(context)
                    .setTitle("Alert")
                    .setMessage("Are you sure to remove..?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteProduct(data, position, productModel.getId());
                        }
                    })
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }
}

