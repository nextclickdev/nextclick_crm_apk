package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.CreateAndUpdateMenuActivity;
import com.nextclick.crm.ShopNowModule.Models.MenuModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MENU_D;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;

public class MenusAdapter extends RecyclerView.Adapter<MenusAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private final Transformation transformation;

    private final List<MenuModel> data;
    private final List<MenuModel> data_full;

    private String token = "";


    public MenusAdapter(Context activity, List<MenuModel> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
        data_full = new ArrayList<>(itemPojos);
        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }


    @NonNull
    @Override
    public MenusAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_menu_item, parent, false);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new MenusAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MenusAdapter.ViewHolder holder, final int position) {
        final MenuModel menuModel = data.get(position);

        String menuName = menuModel.getName();
        String menuImageUrl = menuModel.getImage();
        String menuDescription = menuModel.getDesc();

        holder.tvMenuName.setText(menuName);
        holder.tvMenuDescription.setText(menuDescription);

        if (menuImageUrl != null) {
            if (!menuImageUrl.isEmpty()) {
                Picasso.get()
                        .load(menuImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.ivMenuImage);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.ivMenuImage);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(transformation)
                    .into(holder.ivMenuImage);
        }
    }

    private void deleteCategory(final List<MenuModel> data, final int position, String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String url = MENU_D + id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    String message = jsonObject.getString("message");
                                    UIMsgs.showToast(context, message);
                                    data.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    UIMsgs.showToast(context, "Unable to delete. Please try later");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Asd", "asd");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<MenuModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (MenuModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivMenuImage;
        private final ImageView ivDelete;
        private final TextView tvMenuName;
        private final TextView tvMenuDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivDelete = itemView.findViewById(R.id.ivDelete);
            tvMenuName = itemView.findViewById(R.id.tvMenuName);
            ivMenuImage = itemView.findViewById(R.id.ivMenuImage);
            tvMenuDescription = itemView.findViewById(R.id.tvMenuDescription);
            ivDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.ivDelete) {
                showDeleteAlertDialog(getLayoutPosition());
            } else {
                final MenuModel menuModel = data.get(getLayoutPosition());
                Intent intent = new Intent(context, CreateAndUpdateMenuActivity.class);
                intent.putExtra("menu_id", menuModel.getId());
                intent.putExtra("type", "u");
                context.startActivity(intent);
            }
        }
    }

    private void showDeleteAlertDialog(final int layoutPosition) {
        final MenuModel menuModel = data.get(layoutPosition);
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Are you sure to remove..?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteCategory(data, layoutPosition, menuModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
