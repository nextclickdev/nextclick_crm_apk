package com.nextclick.crm.ShopNowModule.Activities;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.PromotionsAdapter;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;

import java.util.List;

public class PromotionViewFragment extends Fragment {

    private final PromotionsListActivity promotionsListActivity;
    private final boolean isNextClickPromotions;
    View view;

    TextView tv_no_data;
    private RecyclerView recyclerview_promotions;
    private PromotionsAdapter promotionsAdapter;
    private Context mContext;
    List<PromotionObject> listPromotions;
    Boolean is_admin;

    public PromotionViewFragment(PromotionsListActivity promotionsListActivity,boolean isNextClickPromotions,List<PromotionObject> listPromotions)
    {
        this.listPromotions=listPromotions;
        this.promotionsListActivity=promotionsListActivity;
        this.isNextClickPromotions=isNextClickPromotions;
    }

    public PromotionViewFragment(PromotionsListActivity promotionsListActivity,boolean isNextClickPromotions,List<PromotionObject> listPromotions,Boolean is_admin)
    {
        this.listPromotions=listPromotions;
        this.promotionsListActivity=promotionsListActivity;
        this.isNextClickPromotions=isNextClickPromotions;
        this.is_admin=is_admin;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_view_promotions, container, false);

        recyclerview_promotions = view.findViewById(R.id.recyclerview_promotions);
        tv_no_data = view.findViewById(R.id.tv_no_data);
        promotionsAdapter = new PromotionsAdapter(promotionsListActivity,listPromotions,true,isNextClickPromotions,is_admin);
        if(isNextClickPromotions)
        recyclerview_promotions.setLayoutManager(new LinearLayoutManager(promotionsListActivity));
        else
        {
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
            recyclerview_promotions.setLayoutManager(staggeredGridLayoutManager);
        }

        recyclerview_promotions.setAdapter(promotionsAdapter);
        if (listPromotions.size() > 0) {
            recyclerview_promotions.setVisibility(View.VISIBLE);
            tv_no_data.setVisibility(View.GONE);
        }
        return view;
    }

    public void refreshData(List<PromotionObject> listPromotions)
    {
        this.listPromotions=listPromotions;
        if (listPromotions.size() > 0) {
            promotionsAdapter.setRefresh(listPromotions);
            recyclerview_promotions.setVisibility(View.VISIBLE);
            tv_no_data.setVisibility(View.GONE);
        }
        else
        {
            recyclerview_promotions.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.VISIBLE);
        }
    }
}
