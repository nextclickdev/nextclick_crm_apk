package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Adapters.ProductsAdapter;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MYPRODUCTLIST;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class MyProductsFragment extends Fragment implements View.OnClickListener {

    private Button apply_;
    private Context mContext;
    private TextView tvError;
    private EditText etSearch;
    private ImageView ivSearch;
    private ProductsAdapter productsAdapter;
    private LinearLayoutManager layoutManager;
    private PreferenceManager preferenceManager;
    private FloatingActionButton add_product_fab;
    private RecyclerView seller_products_recycler;
    private Spinner filetr_menu_spinner, filter_category_spinner;

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<ProductModel> productModelsList = new ArrayList<>();

    private boolean isScrolled;
    private boolean isScrolling = false;

    private int count = 0;
    private int totalItems;
    private int page_no = 1;
    private int currentItems;
    private int scrollOutItems;
    public static int status_count = 0;


    private String token = "";
    private String menu_id = "";
    private String sub_cat_id = "";
    private String enteredText = "";


    public MyProductsFragment() {
    }

    public MyProductsFragment(Context mContext) {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myproducts, container, false);
        initializeUi(view);
        initializeListeners();
        productModelsList.clear();
        getProducts();
        getCategories();
        adapterSetter();
        prepareSpinnerItemsClicks();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);

        apply_ = view.findViewById(R.id.apply_p);
        tvError = view.findViewById(R.id.tvError);
        etSearch = view.findViewById(R.id.etSearch);
        ivSearch = view.findViewById(R.id.ivSearch);
        add_product_fab = view.findViewById(R.id.add_product_fab);
        filetr_menu_spinner = view.findViewById(R.id.filetr_menu_spinner);
        filter_category_spinner = view.findViewById(R.id.filter_category_spinner);
        seller_products_recycler = view.findViewById(R.id.seller_products_recycler);
    }

    private void initializeListeners() {
        apply_.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        add_product_fab.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                enteredText = charSequence.toString();
               /* if (productsAdapter != null) {
                    productsAdapter.getFilter().filter(enteredText);
                }*/

               /* search_string = enteredText;
                page_no = 1;
                count = 0;
                seller_products_recycler.setAdapter(null);
                productModelsList = new ArrayList<>();
                adapterSetter();
                getProducts();*/

                /*if (enteredText.isEmpty()) {
                    page_no = 1;
                    count = 0;
                    seller_products_recycler.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    adapterSetter();
                    getProducts();
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void prepareSpinnerItemsClicks() {
        filter_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filter_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    menusList.clear();
                    menuIDList.clear();
                    filetr_menu_spinner.setAdapter(null);
                    getMenus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void prepareDetails() {
        seller_products_recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolled = true;
                    isScrolling = false;
                    count++;
                    ++page_no;
                    getProducts();
                }
            }
        });
    }

    private void getProducts() {
        System.out.println("aaaaaaaaaaaa  dailog ");
        LoadingDialog.loadDialog(getContext());
        Map<String, String> dataMap = new HashMap<>();

        int limit =10;
        int offset =((page_no-1)* limit);
        //dataMap.put("page_no", page_no + "");
        dataMap.put("limit",  limit+ "");
        dataMap.put("offset", offset  + "");
        dataMap.put("q", enteredText);
        dataMap.put("menu_id", menu_id);
        dataMap.put("shop_by_cat_id", sub_cat_id);
        final String data = new JSONObject(dataMap).toString();
        System.out.println("aaaaaaaa  data "+data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,MYPRODUCTLIST,/* ALLPRODUCTLIST,*/
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("aaaaaaaaaaaa  dailog 11 "+response);
                      //  LoadingDialog.dialog.dismiss();
                        LoadingDialog.dialog.cancel();
                        if (response != null) {
                            Log.d("Product_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        if (dataArray.length() > 0) {
                                            listIsFull();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject productObject = dataArray.getJSONObject(i);
                                                ProductModel productModel = new ProductModel();
                                                productModel.setId(productObject.getString("id"));
                                                productModel.setSub_cat_id(productObject.getString("sub_cat_id"));
                                                productModel.setMenu_id(productObject.getString("menu_id"));
                                                productModel.setProduct_code(productObject.getString("product_code"));
                                                productModel.setName(productObject.getString("name"));
                                                productModel.setDesc(Html.fromHtml(productObject.getString("desc")).toString());
                                              //  productModel.setQuantity(productObject.getInt("quantity"));
                                              //  productModel.setPrice(productObject.getInt("price"));
                                              //  productModel.setDiscount(productObject.getInt("discount"));
                                             //   productModel.setItem_type(productObject.getInt("item_type"));
                                                productModel.setStatus(productObject.getInt("status"));
                                               // productModel.setProduct_image(productObject.getString("product_image"));
                                                productModel.setAvailabilityStatus(productObject.getString("status"));
                                                try {
                                                    productModel.setCategoryName(productObject.getJSONObject("sub_category").getString("name"));
                                                    productModel.setMenu_name(productObject.getJSONObject("menu").getString("name"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                ArrayList<ImagesModel> imageslist=new ArrayList<>();
                                                try {
                                                   /* JSONArray jsonArray=productObject.getJSONArray("item_images");
                                                    for (int k=0;k<jsonArray.length();k++){
                                                        JSONObject jsonObject1=jsonArray.getJSONObject(k);
                                                        ImagesModel imagesModel=new ImagesModel();
                                                        imagesModel.setId(jsonObject1.getString("id"));
                                                        imagesModel.setImage(jsonObject1.getString("image"));
                                                        imageslist.add(imagesModel);
                                                    }
                                                    */
                                                    String productImage = productObject.getString("image");
                                                    if(productImage!=null)
                                                    {
                                                        ImagesModel imagesModel=new ImagesModel();
                                                        imagesModel.setImage(productImage);
                                                        imageslist.add(imagesModel);
                                                    }
                                                    productModel.setImagelist(imageslist);
                                                }catch (Exception e){

                                                }

                                                productModelsList.add(productModel);
                                                productsAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    } catch (Exception e) {
                                        LoadingDialog.dialog.cancel();
                                        System.out.println("aaaaaaa catch "+e.getMessage());
                                        e.printStackTrace();
                                        if (!isScrolled) {
                                            listIsEmpty();
                                        }
                                        if (count == 0) {
                                            UIMsgs.showToast(mContext, "Products Not Available");
                                        } else {
                                            UIMsgs.showToast(mContext, "You've reached the end");
                                        }
                                    }
                                } else {
                                    LoadingDialog.dialog.cancel();
                                    if (!isScrolled) {
                                        listIsEmpty();
                                    }
                                }
                            } catch (Exception e) {
                                LoadingDialog.dialog.cancel();
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                                if (!isScrolled) {
                                    listIsEmpty();
                                }
                            }
                        } else {
                            LoadingDialog.dialog.cancel();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            if (!isScrolled) {
                                listIsEmpty();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.cancel();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                        if (!isScrolled) {
                            listIsEmpty();
                        }
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        seller_products_recycler.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        seller_products_recycler.setVisibility(View.GONE);
    }

    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("menu_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filetr_menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("aaaaaa catch manu "+e.getMessage());
                                UIMsgs.showToast(mContext, "No menus available for the selected menu");
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error menu "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filter_category_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaa catch cat "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error cat "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void adapterSetter() {
        productsAdapter = new ProductsAdapter(mContext, productModelsList);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        seller_products_recycler.setLayoutManager(layoutManager);
        seller_products_recycler.setAdapter(productsAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_product_fab:
               // mContext.startActivity(new Intent(mContext, CreateAndUpdateProductActivity.class));
                mContext.startActivity(new Intent(mContext, ProductAddOrUpdate.class));
                break;
            case R.id.apply_p:
                page_no = 1;
                count = 0;
                seller_products_recycler.setAdapter(null);
                productModelsList = new ArrayList<>();
                productModelsList.clear();
                getProducts();
                adapterSetter();
                break;
            case R.id.ivSearch:
                page_no = 1;
                count = 0;
                seller_products_recycler.setAdapter(null);
                productModelsList = new ArrayList<>();
                adapterSetter();
                productModelsList.clear();
                productModelsList.clear();
                getProducts();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (status_count == 1) {
            productModelsList.clear();
            getProducts();
            status_count = 0;
        }
    }
}