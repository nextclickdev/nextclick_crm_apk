package com.nextclick.crm.ShopNowModule.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Common.Activities.RegisterVendorActivity;
import com.nextclick.crm.Common.Adapters.CategoryAdapter;
import com.nextclick.crm.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class BottomCategoryFragment  extends BottomSheetDialogFragment {

    private final RegisterVendorActivity registerVendorActivity;
    private RecyclerView recyclerView_subcategories;
    private final ArrayList<String> categories;
    private final ArrayList<String> categoryId;
    private Context context;
    ArrayList<String> selectedCategoryIDs;

    public BottomCategoryFragment(Context mContext, ArrayList<String> categoryId, ArrayList<String> categories,
                                  ArrayList<String> selectedCategoryIDs,
                                  RegisterVendorActivity registerVendorActivity) {
        this.context = mContext;
        this.categoryId = categoryId;
        this.categories = categories;
        this.selectedCategoryIDs=selectedCategoryIDs;
        this.registerVendorActivity = registerVendorActivity;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull @NotNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);



        ArrayList<String> selectedCategories=new ArrayList<>();
        View contentView = View.inflate(getContext(), R.layout.select_categories, null);
        context = contentView.getContext();
        dialog.setContentView(contentView);

        Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);

        RecyclerView recyclerView_categories=dialog.findViewById(R.id.recyclerView_categories);
        RecyclerView recyclerView_subcategories=dialog.findViewById(R.id.recyclerView_subcategories);

        TextView tv_no_categories=dialog.findViewById(R.id.tv_no_categories);
        TextView tv_no_sub_categories=dialog.findViewById(R.id.tv_no_sub_categories);
        TextView tv_error=dialog.findViewById(R.id.tv_error);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_categories.setLayoutManager(linearLayoutManager);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context);
        linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView_subcategories.setLayoutManager(linearLayoutManager1);

        CategoryAdapter categoryAdapter = new CategoryAdapter(context, null,recyclerView_subcategories,tv_no_categories,tv_no_sub_categories,selectedCategoryIDs,selectedCategories,registerVendorActivity);
        recyclerView_categories.setAdapter(categoryAdapter);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedCategoryIDs.size() > 0) {
                    dialog.dismiss();
                    registerVendorActivity.setSelectedCategoryIDs(selectedCategoryIDs,selectedCategories);
                }
                else {
                    tv_error.setVisibility(View.VISIBLE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tv_error.setVisibility(View.GONE);
                        }
                    },2000);
                }
            }
        });

      //  dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int height = displayMetrics.heightPixels;
        int maxHeight = (int) (height*0.44); //custom height of bottom sheet

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((BottomSheetBehavior) behavior).setPeekHeight(maxHeight);

       // dialog.show();
    }
}
