package com.nextclick.crm.ShopNowModule.Activities;

import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.SubscriptionSettingAdapter2;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.promocodes.adatpetrs.SubscriptionSettingAdapter;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewSubscriptionPrivileges extends AppCompatActivity {

    RecyclerView recycle_settings,recycle_status;
    TextView tv_error;
    PreferenceManager preferenceManager;
    private Context mContext;
    private CustomDialog customDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_subscription_privileges);

        this.mContext =getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        customDialog=new CustomDialog(this);

        ImageView img_back=findViewById(R.id.img_back);
        recycle_settings=findViewById(R.id.recycle_settings);
        recycle_status=findViewById(R.id.recycle_status);
        tv_error=findViewById(R.id.tv_error);

       /*  ArrayList<SubscriptionSetting> settings= Utility.getSettingInstance(getApplicationContext());

       if(settings!=null && settings.size()>0) {
            recycle_settings.setLayoutManager(new GridLayoutManager(ViewSubscriptionPrivileges.this, 1));
            SubscriptionSettingAdapter adapter = new SubscriptionSettingAdapter(ViewSubscriptionPrivileges.this, settings);
            recycle_settings.setAdapter(adapter);
        }
        else
        {
            recycle_settings.setVisibility(View.GONE);
            tv_error.setVisibility(View.VISIBLE);
        }*/

        //dynamic
        getSubscriptionServicesList();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private ArrayList<SubscriptionSetting> settingModalArrayList;
    private ArrayList<SubscriptionPlan> totalPackages;
    private void getSubscriptionServicesList() {
        settingModalArrayList = new ArrayList<>();
        totalPackages = new ArrayList<>();
        customDialog.show();

        String url= Config.URL_GET_SUBSCRIPTION_PACKAGES+"/?service_id=2";
        System.out.println("aaaaaaaaaa   URL_GET_SUBSCRIPTION_PACKAGES url " + url);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("aaaaaaaaaa   URL_GET_SUBSCRIPTION_PACKAGES " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");
                            if (status && http_code == 200) {
                                JSONArray responsearray = jsonObject.getJSONArray("data");

                                for (int k = 0; k < responsearray.length(); k++) {

                                    ArrayList<SubscriptionSetting> packageSetting = new ArrayList<>();
                                    JSONArray featuresArray = responsearray.getJSONObject(k).getJSONArray("features");


                                    for (int i = 0; i < featuresArray.length(); i++) {
                                        JSONObject jsonObject1 = featuresArray.getJSONObject(i);

                                        String key = jsonObject1.getString("description");
                                        String setting_description = jsonObject1.getString("description");
                                        String setting_status = jsonObject1.getString("status");

                                        SubscriptionSetting subscriptionSetting = new SubscriptionSetting(key, setting_description, setting_status.equals("1"));
//                                        if (k == 0) {
//                                            settingModalArrayList.add(subscriptionSetting);
//                                            if(i == 0)//for custom title
//                                            settingModalArrayList.add(subscriptionSetting);
//                                        }
//                                        if(i == 0)//for custom title
//                                        packageSetting.add(subscriptionSetting);

                                        packageSetting.add(subscriptionSetting);
                                    }
                                    SubscriptionPlan plan = new SubscriptionPlan();
                                    plan.setSubscriptionSetting(packageSetting);

                                    plan.setTitle(responsearray.getJSONObject(k).getString("title"));
                                    plan.setIsActive(responsearray.getJSONObject(k).getString("is_active"));
                                    totalPackages.add(plan);
                                }
                            }

                        } catch (JSONException e) {
                            System.out.println("SUBSCRIPTION_SETTINGS   catch " + e);
                        }
                        finally {
                            customDialog.dismiss();
                            ArrayList<SubscriptionSetting> settings= settingModalArrayList;
                            if(settings!=null && settings.size()>0) {
//                                recycle_settings.setLayoutManager(new GridLayoutManager(ViewSubscriptionPrivileges.this, 1));
//                                SubscriptionSettingAdapter adapter = new SubscriptionSettingAdapter(ViewSubscriptionPrivileges.this, settings,true);
//                                recycle_settings.setAdapter(adapter);

                                recycle_status.setLayoutManager(new GridLayoutManager(ViewSubscriptionPrivileges.this, 1));
                                SubscriptionSettingAdapter2 adapter1 = new SubscriptionSettingAdapter2(ViewSubscriptionPrivileges.this,settings,
                                        totalPackages,false);
                                recycle_status.setAdapter(adapter1);
                            }
                            else
                            {
                                recycle_settings.setVisibility(View.GONE);
                                recycle_status.setVisibility(View.GONE);
                                tv_error.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("SUBSCRIPTION_SETTINGS  settings error  " + error.getMessage());
                customDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaaaa token " + preferenceManager.getString(USER_TOKEN));
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void getSubscriptionServicesList_old() {
        settingModalArrayList = new ArrayList<>();
        customDialog.show();

        String url= Config.URL_GET_SUBSCRIPTION_SETTINGS+"/?service_id=2";
        System.out.println("aaaaaaaaaa   SUBSCRIPTION_SETTINGS url " + url);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            System.out.println("aaaaaaaaaa   SUBSCRIPTION_SETTINGS " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");
                            if (status && http_code == 200) {
                                JSONObject dataObj = jsonObject.getJSONObject("data");

                                JSONArray responsearray = dataObj.getJSONArray("settings");
                                for (int i = 0; i < responsearray.length(); i++) {
                                    JSONObject jsonObject1 = responsearray.getJSONObject(i);

                                    String key = jsonObject1.getString("setting_key");
                                    String setting_description = "";
                                    if (jsonObject1.has("master_setting"))
                                        setting_description = jsonObject1.getJSONObject("master_setting").getString("description");
                                    String setting_status = jsonObject1.getString("status");

                                    SubscriptionSetting subscriptionSetting = new SubscriptionSetting(key, setting_description, setting_status.equals("1"));
                                    settingModalArrayList.add(subscriptionSetting);
                                }
                            }

                        } catch (JSONException e) {
                            System.out.println("SUBSCRIPTION_SETTINGS   catch " + e);
                        }
                        finally {
                            customDialog.dismiss();
                            ArrayList<SubscriptionSetting> settings= settingModalArrayList;
//                            if(settings!=null && settings.size()>0) {
//                                recycle_settings.setLayoutManager(new GridLayoutManager(ViewSubscriptionPrivileges.this, 1));
//                                SubscriptionSettingAdapter adapter = new SubscriptionSettingAdapter(ViewSubscriptionPrivileges.this, settings,true);
//                                recycle_settings.setAdapter(adapter);
//
//                                recycle_status.setLayoutManager(new GridLayoutManager(ViewSubscriptionPrivileges.this, 1));
//                                SubscriptionSettingAdapter adapter1 = new SubscriptionSettingAdapter(ViewSubscriptionPrivileges.this, settings,false);
//                                recycle_status.setAdapter(adapter1);
//                            }
//                            else
//                            {
//                                recycle_settings.setVisibility(View.GONE);
//                                tv_error.setVisibility(View.VISIBLE);
//                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("SUBSCRIPTION_SETTINGS  settings error  " + error.getMessage());
                customDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaaaa token " + preferenceManager.getString(USER_TOKEN));
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}