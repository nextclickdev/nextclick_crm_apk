package com.nextclick.crm.ShopNowModule.Fragments.MyProductFragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.ServicesAdapter;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.adapters.ViewPagerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.VENDOR_PRODUCTS_COUNT;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;

public class ProductsHomeFragment extends Fragment implements View.OnClickListener {

    Context mContext;
    private TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    private FloatingActionButton add_product_fab;
    TextView tv_add_product;
    private int currentposition;
    public static int viewpagercurrentposion=0;
    private ServicesAdapter servicesAdapter;
    private CustomDialog mCustomDialog;
    private PreferenceManager preferenceManager;

    public ProductsHomeFragment() {


    }


    public ProductsHomeFragment(Context mContext) {
        this.mContext=mContext;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        System.out.println("isVisibleToUser "+isVisibleToUser);
        if (isVisibleToUser) {
        }
        else {
        }
    }

    public static boolean isFragmentVisible(Fragment fragment) {
        Activity activity = fragment.getActivity();
        View focusedView = fragment.getView().findFocus();
        return activity != null
                && focusedView != null
                && focusedView == activity.getWindow().getDecorView().findFocus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products_home, container, false);
        initializeUi(view);

        this.getActivity().setTitle("My Products");

        //System.out.println("isFragmentVisible "+isFragmentVisible(this));
       // Log.e("isFragmentVisible","isFragmentVisible "+isFragmentVisible(this));

        return  view;
    }

    private void initializeUi(View view) {

        if (mContext == null)
            mContext = getActivity();
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        add_product_fab = view.findViewById(R.id.add_product_fab);
        tv_add_product= view.findViewById(R.id.tv_add_product);
        viewPager = view.findViewById(R.id.viewPager);


        mCustomDialog=new CustomDialog(getContext());
        preferenceManager=new PreferenceManager(getContext());
        getProductscount();
        add_product_fab.setOnClickListener(this);
        tv_add_product.setOnClickListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (viewPager != null) {
                    if(servicesAdapter!=null)
                        servicesAdapter.selectedIndex(position);
                    if(Utility.RefreshMyProducts) {
                        Log.e("Refresh", "Refresh");
                        currentposition = position;
                        refreshViewPager();
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(viewPager!=null &&Utility.ShowPendingProuctsItem)
        {
            Utility.ShowPendingProuctsItem=false;
            Utility.RefreshProducts=true;
            currentposition =3;
            refreshViewPager();
        }


    }

    private void refreshViewPager() {
        viewPager.postDelayed(new Runnable() {

            @Override
            public void run() {
                if (viewPager.getCurrentItem() == currentposition) {
                    if (viewPager.getAdapter() != null) {
                        viewPager.setAdapter(null);
                    }
                    viewPager.setAdapter(viewPagerAdapter);
                }
                viewPager.setCurrentItem(currentposition);
            }
        }, 100);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.add_product_fab:
            case R.id.tv_add_product:
                Intent i=new Intent(mContext, ProductAddOrUpdate.class);
                i.putExtra("currentposition",viewPager.getCurrentItem());
                startActivity(i);
                //finish();
                break;
            default:
                break;
        }
    }

    public void invokePendingTab() {
        currentposition =3;
        viewPager.setCurrentItem(currentposition);
    }


    public void setViewHolder(ServicesAdapter servicesAdapter) {
        this.servicesAdapter=servicesAdapter;
    }

    public void doAction(String id) {
        if(viewPager==null)
            return;
        switch (id) {
            case "2101":
                currentposition =0;
                viewPager.setCurrentItem(0);
                break;
            case "2102":
                currentposition =1;
                viewPager.setCurrentItem(1);
                break;
            case "2103":
                currentposition =2;
                viewPager.setCurrentItem(2);
                break;
            case "2104":
                currentposition =3;
                viewPager.setCurrentItem(3);
                break;
        }
    }

    public void onDestroyActionMode() {

    }

    public void getProductscount() {

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_PRODUCTS_COUNT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        mCustomDialog.dismiss();
                        if (response != null) {
                            Log.d("Productscount_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  myproducts "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");

                                if (status) {
                                    try {
                                        //JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        String catalogue_count=dataObject.getString("catalogue_count");
                                        String inventory_instock_count=dataObject.getString("inventory_instock_count");
                                        String inventory_outofstock_count=dataObject.getString("inventory_outofstock_count");
                                        String pendig_count=dataObject.getString("pendig_count");
                                        String approved_count=dataObject.getString("approved_count");

                                        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager(), null,catalogue_count,inventory_instock_count,
                                                inventory_outofstock_count,pendig_count,approved_count);
                                        viewPager.setAdapter(viewPagerAdapter);
                                        currentposition=0;//getIntent().getIntExtra("currentposition",0);
                                        tabLayout.setupWithViewPager(viewPager);
                                        viewPager.setCurrentItem(currentposition);

                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch myproduct  "+e.getMessage());
                                        e.printStackTrace();
                                    }
                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(getContext(), MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomDialog.dismiss();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaaaaa token  "+preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}