package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.CategoryAdapter;
import com.nextclick.crm.Common.Models.SubCategorySelection;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.UIValidations;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.Helpers.UIHelpers.Validations;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.ShopNowModule.Adapters.MultiImagesAdapter;
import com.nextclick.crm.ShopNowModule.Models.CategoryObject;
import com.nextclick.crm.ShopNowModule.Models.MultiImageModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.FindAddressInMap;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.nextclick.crm.Config.Config.CATEGORY_LIST;
import static com.nextclick.crm.Config.Config.GET_BANKS;
import static com.nextclick.crm.Config.Config.MainAppVendorRegistration;
import static com.nextclick.crm.Config.Config.States;
import static com.nextclick.crm.Config.Config.UPDATE_BANK_DETAILS;
import static com.nextclick.crm.Config.Config.UPDATE_IMAGES;
import static com.nextclick.crm.Config.Config.UPDATE_PROFILE;
import static com.nextclick.crm.Config.Config.UPDATE_PROFILE_DETAILS;
import static com.nextclick.crm.Config.Config.UPDATE_SOCIAL_MEDIA_LINKS;
import static com.nextclick.crm.Config.Config.USER_PROFILE;
import static com.nextclick.crm.Config.Config.USER_PROFILE_UPDATE;
import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.isNull;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;
import static com.nextclick.crm.Helpers.UIHelpers.Validations.isValidURL;


public class Profilefragment extends Fragment implements View.OnClickListener, SubCategorySelection {

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    // Double lattitude, longitude;
    LocationManager locationManager;
    EditText tv_gst_no, tv_labour_certificate_no, tv_fssai_no, tv_owner_name, pincode;
    RelativeLayout layout_category;
    TextView tv_sub_categories;
    private Context mcontext;
    private View rootView;
    ImageView img_profile;
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<CategoryObject> ListCategoryObject = new ArrayList<>();
    ArrayList<String> banks = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    public String catId,mDescription;
    public String catName;
    public ArrayList<String> selectedCategoryIDs = new ArrayList<>();
    PreferenceManager preferenceManager;
    private String token;
    TextInputEditText tv_user_first_name, tv_user_last_name;
    private EditText shop_location, latitude_et, longitude_et,
            unique_id, category, listing_name, email, description,
            address, landmark, ac_holder_name, bank_name, bank_branch,
            ac_number, ifsc_code, mobile_code, mobile_et, landline_code,
            landline_et, whatsapp_et, helpline_et,
            website_link, instagram_link, twitter_link, fb_link;
    private String shop_location_str, latitude_et_str, longitude_et_str, unique_id_str,
            category_str, listing_name_str, email_str, description_str, address_str,
            landmark_str, ac_holder_name_str, bank_name_str, bank_branch_str, ac_number_str,
            ifsc_code_str, mobile_code_str, mobile_et_str, landline_code_str, landline_et_str,
            whatsapp_et_str, helpline_et_str, fb_link_str, insta_link_str, twitter_link_str, website_str;
    private LinearLayout user_layout, profile_layout, bank_details_layout, social_layout, banners_layout;
    private CardView profile_card, bank_details_card, social_card, banners_card;
    private TextView bank_text, profile_text, social_text, banners_text;
    private RadioButton open, close;
    private Button get_location, update_profile, update_bank_details, update_social_media, update_images, update_user;
    private RecyclerView multi_image_recycler;
    private ImageView cover_image, add_image, tv_addcoverimage;
    String coverImageBase64, profile64;
    public static ArrayList<String> base64ImageArray = new ArrayList<>();
    public static ArrayList<MultiImageModel> multiImageModels = new ArrayList<>();

    private char selection_character = 'c';
    private static final int CAMERA_REQUEST = 8;
    private String userChoosenTask;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;
    public int flag = 0;
    AutoCompleteTextView bank_spinner;
    Spinner state_spinner, district_spinner, constituency_spinner;

    LinearLayoutManager multiImagelinearLayoutManager;
    MultiImagesAdapter multiImagesAdapter;


    private int AVAILABILITY = -1;
    private final int OPEN = 1;
    private final int CLOSE = 0;
    private final int mobile = 1;
    private final int landline = 2;
    private final int whatsapp = 3;
    private final int helpline = 4;
    //private String constId="1";

    private String stateid = "", districtid = "", constitueid = "";//test
    private CustomDialog mCustomDialog;
    private String selectedBank;
    private final boolean canEditShopLoation = true;

    public Profilefragment(Context context) {
        this.mcontext = context;
    }

    public Profilefragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profilefragment_new_2, container, false);
        init();
        return rootView;
    }


    private void init() {
        mcontext = getActivity();
        mCustomDialog = new CustomDialog(mcontext);
        preferenceManager = new PreferenceManager(mcontext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        img_profile = rootView.findViewById(R.id.img_profile);
        update_user = rootView.findViewById(R.id.update_user);
        tv_user_first_name = rootView.findViewById(R.id.tv_user_first_name);
        tv_user_last_name = rootView.findViewById(R.id.tv_user_last_name);
        shop_location = rootView.findViewById(R.id.shop_location);
        latitude_et = rootView.findViewById(R.id.latitude_et);
        longitude_et = rootView.findViewById(R.id.longitude_et);
        unique_id = rootView.findViewById(R.id.unique_id);
        category = rootView.findViewById(R.id.category);
        listing_name = rootView.findViewById(R.id.listing_name);
        email = rootView.findViewById(R.id.email);
        description = rootView.findViewById(R.id.description);
        address = rootView.findViewById(R.id.address);
        landmark = rootView.findViewById(R.id.landmark);
        ac_holder_name = rootView.findViewById(R.id.ac_holder_name);
        bank_name = rootView.findViewById(R.id.bank_name);
        bank_branch = rootView.findViewById(R.id.bank_branch);
        ac_number = rootView.findViewById(R.id.ac_number);
        ifsc_code = rootView.findViewById(R.id.ifsc_code);
        open = rootView.findViewById(R.id.open);
        close = rootView.findViewById(R.id.close);
        mobile_code = rootView.findViewById(R.id.mobile_code);
        mobile_et = rootView.findViewById(R.id.mobile_et);
        landline_code = rootView.findViewById(R.id.landline_code);
        landline_et = rootView.findViewById(R.id.landline_et);
        whatsapp_et = rootView.findViewById(R.id.whatsapp_et);
        helpline_et = rootView.findViewById(R.id.helpline_et);
        website_link = rootView.findViewById(R.id.website_link);
        instagram_link = rootView.findViewById(R.id.instagram_link);
        twitter_link = rootView.findViewById(R.id.twitter_link);
        fb_link = rootView.findViewById(R.id.fb_link);
        user_layout = rootView.findViewById(R.id.user_layout);
        profile_layout = rootView.findViewById(R.id.profile_layout);
        bank_details_layout = rootView.findViewById(R.id.bank_details_layout);
        social_layout = rootView.findViewById(R.id.social_layout);
        banners_layout = rootView.findViewById(R.id.banners_layout);

        profile_card = rootView.findViewById(R.id.profile_card);
        bank_details_card = rootView.findViewById(R.id.bank_details_card);
        social_card = rootView.findViewById(R.id.social_card);
        banners_card = rootView.findViewById(R.id.banners_card);
        profile_text = rootView.findViewById(R.id.profile_text);
        bank_text = rootView.findViewById(R.id.bank_text);
        social_text = rootView.findViewById(R.id.social_text);
        banners_text = rootView.findViewById(R.id.banners_text);
        add_image = rootView.findViewById(R.id.tv_addimage);
        tv_addcoverimage = rootView.findViewById(R.id.tv_addcoverimage);

        get_location = rootView.findViewById(R.id.get_location);
        update_profile = rootView.findViewById(R.id.update_profile);
        update_bank_details = rootView.findViewById(R.id.update_bank_details);
        update_social_media = rootView.findViewById(R.id.update_social_media);
        update_images = rootView.findViewById(R.id.update_images);

        multi_image_recycler = rootView.findViewById(R.id.multi_image_recycler);
        cover_image = rootView.findViewById(R.id.cover_image);

        tv_gst_no = rootView.findViewById(R.id.tv_gst_no);
        tv_labour_certificate_no = rootView.findViewById(R.id.tv_labour_certificate_no);
        tv_fssai_no = rootView.findViewById(R.id.tv_fssai_no);
        tv_owner_name = rootView.findViewById(R.id.tv_owner_name);
        pincode = rootView.findViewById(R.id.pincode);
        layout_category = rootView.findViewById(R.id.layout_category);
        tv_sub_categories = rootView.findViewById(R.id.tv_sub_categories);

        address.setEnabled(false);
        address.setClickable(false);
        pincode.setEnabled(false);
        pincode.setClickable(false);


        layout_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //showCategoryPopup();
            }
        });
        listing_name.setEnabled(false);
        listing_name.setClickable(false);
        tv_owner_name.setEnabled(false);
        tv_owner_name.setClickable(false);

        latitude_et.setEnabled(false);
        longitude_et.setEnabled(false);
        latitude_et.setClickable(false);
        longitude_et.setClickable(false);

        if (!canEditShopLoation) {
            latitude_et.setVisibility(View.GONE);
            longitude_et.setVisibility(View.GONE);
            get_location.setVisibility(View.GONE);
        }

        shop_location.setClickable(false);
        shop_location.setEnabled(false);

        mobile_et.setClickable(false);
        mobile_et.setEnabled(false);
        email.setClickable(false);
        email.setEnabled(false);
        bank_spinner = (AutoCompleteTextView) rootView.findViewById(R.id.bank_spinner);
        state_spinner = (Spinner) rootView.findViewById(R.id.state_spinner);
        district_spinner = (Spinner) rootView.findViewById(R.id.district_spinner);
        constituency_spinner = (Spinner) rootView.findViewById(R.id.constituency_spinner);

      /*  state_spinner.setEnabled(false);
        state_spinner.setClickable(false);
        district_spinner.setEnabled(false);
        district_spinner.setClickable(false);
        constituency_spinner.setEnabled(false);
        constituency_spinner.setClickable(false);
*/

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    stateid = statesId.get(position - 1);
                    districtsId.clear();
                    districts.clear();
                    districtsDataFetcher(stateid);
                } else {
                    stateid = "";
                    districtid = "";
                    constitueid = "";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    ArrayAdapter ad = new ArrayAdapter(mcontext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(ad);
                    constituency_spinner.setAdapter(ad);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    districtid = districtsId.get(position - 1);
                    constituencies.clear();
                    constituenciesId.clear();
                    constituenciesDataFetcher(stateid, districtid);
                } else {
                    districtid = "";
                    constitueid = "";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    ArrayAdapter ad = new ArrayAdapter(mcontext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    constituency_spinner.setAdapter(ad);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        constituency_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (districtid.isEmpty()) {
                    constitueid = "";
                } else {
                    try {
                        if (position != 0)
                            constitueid = constituenciesId.get(position - 1);
                        else
                            constitueid = "";
                    } catch (IndexOutOfBoundsException e) {
                    }

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

/*        bank_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    selectedBank = banks.get(position);
                } else {
                    selectedBank = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    bank_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String selectedItem=bank_spinner.getAdapter().getItem(position).toString();
            if (position != 0) {
                selectedBank = selectedItem;
            } else {
                selectedBank = "";
            }
        }
    });
        states.add("Select");
        banks.add("Select");
        getBankNames();

        getProfile();
        getUserProfile();

        categories.add("Select");
        //   categoryDataFetcher(); -- no category update in profile

        get_location.setOnClickListener(this);
        update_profile.setOnClickListener(this);
        img_profile.setOnClickListener(this);
        update_user.setOnClickListener(this);
        update_bank_details.setOnClickListener(this);
        update_social_media.setOnClickListener(this);
        update_images.setOnClickListener(this);
        profile_card.setOnClickListener(this);
        bank_details_card.setOnClickListener(this);
        banners_card.setOnClickListener(this);
        social_card.setOnClickListener(this);
        add_image.setOnClickListener(this);
        cover_image.setOnClickListener(this);
        tv_addcoverimage.setOnClickListener(this);

        multiImagelinearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mcontext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };


    }

    private void getProfile() {
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mCustomDialog.dismiss();
                if (response != null) {
                    Log.d("p_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            listing_name.setText(dataObject.getString("business_name"));
                            //email.setText(dataObject.getString("email"));
                            // unique_id.setText(dataObject.getString("unique_id"));


                            try {
                                catName = dataObject.getJSONObject("category").getString("name");
                                catId = dataObject.getJSONObject("category").getString("id");
                                preferenceManager.putString("VendorCategory", catId);
                                category.setText(catName);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                           /* if(dataObject.has("desc")) {
                                String desc = dataObject.getString("desc");
                                if (desc != null && !desc.isEmpty() && !desc.equals("null"))
                                    description.setText(dataObject.getString("desc"));
                            }*/
                            if (dataObject.has("address")) {
                                JSONObject addressObj = dataObject.getJSONObject("address");
                                address.setText(addressObj.getString("line1"));
                                latitude_et.setText(addressObj.getString("lat"));
                                longitude_et.setText(addressObj.getString("lng"));

                                shop_location.setText(addressObj.getString("location"));

                                pincode.setText(addressObj.getString("zip_code"));

                                constitueid = addressObj.getString("constituency");
                                stateid = addressObj.getString("state");
                                districtid = addressObj.getString("district");
                                preferenceManager.putString("VendorConstituency", constitueid);
                            }
                            // address.setText(dataObject.getString("address"));
                            // landmark.setText(dataObject.getString("landmark"));

                            try {
                                setText(tv_fssai_no, dataObject, "fssai_number");
                                setText(tv_labour_certificate_no, dataObject, "labour_certificate_number");
                                setText(tv_gst_no, dataObject, "gst_number");
                                setText(tv_owner_name, dataObject, "owner_name");
                                setText(pincode, dataObject, "pincode");
                                setText(description,dataObject,"business_description");


                       /*         try {
                                    if (dataObject.has("constituency")) {
                                        constitueid = dataObject.getJSONObject("constituency").getString("id");
                                        stateid = dataObject.getJSONObject("constituency").getString("state_id");
                                        districtid = dataObject.getJSONObject("constituency").getString("district_id");

                                        preferenceManager.putString("VendorConstituency", constitueid);
                                    }
                                } catch (Exception exception) {
                                    Log.d("TAG", "onResponse: ");
                                }
*/
                                if (dataObject.has("vendor_sub_categories")) {
                                    JSONArray vendorData = dataObject.getJSONArray("vendor_sub_categories");


                                    if (vendorData.length() > 0) {
                                        ArrayList<String> selectedCategoryIDs = new ArrayList<>();
                                        ArrayList<String> selectedCategoryNames = new ArrayList<>();
                                        for (int i = 0; i < vendorData.length(); i++) {
                                            JSONObject item = vendorData.getJSONObject(i);

                                            JSONObject catobje = item.getJSONObject("sub_categories");

                                            selectedCategoryIDs.add(catobje.getString("id"));
                                            selectedCategoryNames.add(catobje.getString("name"));
                                        }
                                        setSelectedCategoryIDs(selectedCategoryIDs, selectedCategoryNames);
                                    }
                                }
                            } catch (Exception ex) {
                            }

                            statesDataFetcher();

                            try {
                                int availability = dataObject.getInt("availability");
                                if (availability == OPEN) {
                                    open.setChecked(true);
                                }
                                if (availability == CLOSE) {
                                    close.setChecked(true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        /*    try {
                                shop_location.setText(dataObject.getJSONObject("location").getString("address"));
                                if(dataObject.has("location_address"))
                                {
                                    shop_location.setText(dataObject.getString("location_address"));
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }*/
                            try {
                                bank_name.setText(dataObject.getJSONObject("bank_details").getString("bank_name"));
                                bank_branch.setText(dataObject.getJSONObject("bank_details").getString("bank_branch"));
                                ac_holder_name.setText(dataObject.getJSONObject("bank_details").getString("ac_holder_name"));
                                ac_number.setText(dataObject.getJSONObject("bank_details").getString("ac_number"));
                                ifsc_code.setText(dataObject.getJSONObject("bank_details").getString("ifsc"));

                                selectedBank = dataObject.getJSONObject("bank_details").getString("bank_name");
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mcontext,
                                        android.R.layout.simple_spinner_item, banks);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                bank_spinner.setAdapter(adapter);
                                if (banks.size() > 1) {
                                    Integer selIndex = banks.indexOf(selectedBank);
                                    if (selIndex != -1) {
                                        bank_spinner.setText(bank_spinner.getAdapter().getItem(selIndex).toString(),false);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                mDescription = dataObject.getString("business_description");

                                whatsapp_et.setText(dataObject.getString("whats_app_no"));
                                helpline_et.setText(dataObject.getString("secondary_contact"));
                                /*JSONArray contactsArray = dataObject.getJSONArray("contacts");
                                if (contactsArray.length() > 0) {
                                    for (int i = 0; i < contactsArray.length(); i++) {
                                        JSONObject contactObject = contactsArray.getJSONObject(i);
                                        int type = contactObject.getInt("type");


                                        if (i==2 ||type == whatsapp) {

                                            whatsapp_et.setText(contactObject.getString("number"));
                                        }
                                        if (i==3 || type == helpline) {
                                            //mobile_code.setText("std_code");
                                            helpline_et.setText(contactObject.getString("number"));
                                        }
                                        if (i==0 ||type == mobile) {
                                            mobile_et.setText(contactObject.getString("number"));
                                        }
                                    }
                                }*/

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                JSONArray linksArray = dataObject.getJSONArray("links");
                                if (linksArray.length() > 0) {
                                    for (int i = 0; i < linksArray.length(); i++) {
                                        JSONObject linkObject = linksArray.getJSONObject(i);
                                        int type = linkObject.getInt("type");
                                        if (type == 1) {
                                            fb_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 2) {
                                            twitter_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 3) {
                                            instagram_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 4) {
                                            website_link.setText(linkObject.getString("url"));
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                /*Picasso.get()
                                        .load(dataObject.getString("cover"))
                                        .into(cover_image);*/

                                Glide.with(mcontext)
                                        .load(dataObject.getString("cover"))
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.image_placeholder)
                                        .into(cover_image);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                multiImageModels.clear();
                                JSONArray bannersArray = dataObject.getJSONArray("banners");
                                if (bannersArray.length() > 0) {
                                    for (int i = 0; i < bannersArray.length(); i++) {
                                        MultiImageModel multiImageModel = new MultiImageModel();
                                        JSONObject bannerObject = bannersArray.getJSONObject(i);
                                        multiImageModel.setId(bannerObject.getString("id"));
                                        multiImageModel.setImage(bannerObject.getString("image"));
                                        multiImageModels.add(multiImageModel);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                            multiImagesAdapter.notifyDataSetChanged();
                            multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                            multi_image_recycler.setAdapter(multiImagesAdapter);

                        } else {
                            UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void setText(EditText et, JSONObject dataObject, String key) throws JSONException {
        if (dataObject.has(key)) {
            String value = dataObject.getString(key);
            if (value != null && !value.isEmpty() && !value.equals("null"))
                et.setText(dataObject.getString(key));
        }
    }

    public static final int UPDATE_LOCATION = 999;

    public void updateLocation(Intent intent) {
        ArrayList<String> locArray = new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if (locArray != null && locArray.size() >= 3) {
                shop_location.setText(locArray.get(0));
                latitude_et.setText(locArray.get(1));
                longitude_et.setText(locArray.get(2));
            }
        } catch (Exception ex) {
            getLocation();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.get_location:
                //LoadingDialog.loadDialog(mcontext);
                //getLocation();

                new AlertDialog.Builder(mcontext)
                        .setTitle(getString(R.string.location_header))
                        .setMessage(getString(R.string.update_location_info))
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Gson gson = new Gson();
                                ArrayList<String> locArray = new ArrayList<>();
                                locArray.add(shop_location.getText().toString());
                                locArray.add(latitude_et.getText().toString());
                                locArray.add(longitude_et.getText().toString());
                                String locationString = gson.toJson(locArray);
                                Intent intent = new Intent(mcontext, FindAddressInMap.class);
                                intent.putExtra("location", locationString);
                                ((ShopNowHomeActivity) mcontext).startActivityForResult(intent, UPDATE_LOCATION);
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


                break;

            case R.id.update_profile:
                if (isValidProfile()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_PROFILE;//UPDATE_PROFILE_DETAILS;

                   /* Map<Object, Object> uploadMap = new HashMap<>();
                    uploadMap.put("name", listing_name_str);
                    uploadMap.put("address", address_str);
                    uploadMap.put("email", email_str);
                    uploadMap.put("landmark", landmark_str);
                    uploadMap.put("desc", description_str);
                    uploadMap.put("availability", AVAILABILITY + "".trim());
                    uploadMap.put("location_name", shop_location_str);
                    uploadMap.put("latitude", latitude_et_str);
                    uploadMap.put("longitude", longitude_et_str);

                    Map<String, String> mobilemap = new HashMap<>();
                    mobilemap.put("code", mobile_code_str);
                    mobilemap.put("number", mobile_et_str);
                    uploadMap.put("mobile", mobilemap);
                    Map<String, String> landlineMap = new HashMap<>();
                    landlineMap.put("code", landline_code_str);
                    landlineMap.put("number", landline_et_str);
                    uploadMap.put("landline", landlineMap);
                    Map<String, String> whatsappMap = new HashMap<>();
                    whatsappMap.put("code", "00");
                    whatsappMap.put("number", whatsapp_et_str);
                    uploadMap.put("whatsapp", whatsappMap);
                    Map<String, String> helplineMap = new HashMap<>();
                    helplineMap.put("code", "00");
                    helplineMap.put("number", helpline_et_str);
                    uploadMap.put("helpline", helplineMap);*/

                    data = getJsonData();//new JSONObject(uploadMap).toString();
                    Log.e("Update Profile", data);
                    updateBusinessDetails(url, data);
                }

                break;

            case R.id.update_bank_details:
                if (isValidBankDetails()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_BANK_DETAILS;

                    Map<String, String> uploadMap = new HashMap<>();
                    uploadMap.put("bank_name", bank_name_str);
                    uploadMap.put("bank_branch", bank_branch_str);
                    uploadMap.put("ifsc", ifsc_code_str);
                    uploadMap.put("ac_holder_name", ac_holder_name_str);
                    uploadMap.put("ac_number", ac_number_str);
                    data = new JSONObject(uploadMap).toString();
                    updateDetails(url, data);
                }
                break;
            case R.id.update_social_media:
                if (isValidLinks()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_SOCIAL_MEDIA_LINKS;

                    Map<String, String> uploadMap = new HashMap<>();
                    uploadMap.put("facebook", fb_link_str);
                    uploadMap.put("twitter", twitter_link_str);
                    uploadMap.put("instagram", insta_link_str);
                    uploadMap.put("website", website_str);

                    data = new JSONObject(uploadMap).toString();
                    updateDetails(url, data);

                }
                break;
           /* case R.id.cover_image:
                selection_character = 'c';
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;  */
            case R.id.tv_addcoverimage:
                selection_character = 'c';
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;

            case R.id.update_images:

                String url = "";
                String data = "";
                url = UPDATE_IMAGES;

                if (base64ImageArray.size() > 0 || (coverImageBase64 != null && coverImageBase64.length() > 0)) {
                    JSONObject json = new JSONObject();
                    if (coverImageBase64 != null && coverImageBase64.length() > 0) {
                        Map<String, Object> uploadMap = new HashMap<>();
                        uploadMap.put("cover_image", coverImageBase64);
                        json = new JSONObject(uploadMap);
                    }
                    if (base64ImageArray.size() > 0) {
                        try {
                            JSONArray imageMap = new JSONArray();
                            for (int i = 0; i < base64ImageArray.size(); i++) {
                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put("image", base64ImageArray.get(i));
                                imageMap.put(jsonObject1);
                            }
                            json.put("banners", imageMap);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    /*Map<String, Object> uploadMap = new HashMap<>();
                    uploadMap.put("cover_image", basse64Converter(cover_image));
                    if (base64ImageArray.size() > 0) {
                        uploadMap.put("image", base64ImageArray);
                    }*/
                    data = json.toString();
                    updateDetails(url, data);
                } else
                    UIMsgs.showToast(mcontext, "No Updated images are found.");

                break;

            case R.id.bank_details_card:
                onBankDetailsClick();
                break;
            case R.id.profile_card:
                onProfileClick();
                break;

            case R.id.social_card:
                onSocialLayoutClick();
                break;

            case R.id.banners_card:
                onBannersClick();
                break;

            case R.id.tv_addimage:

                selection_character = 'b';
                boolean selecting1 = selectImage();
                if (selecting1) {

                }

                break;
            case R.id.img_profile:
                selection_character = 'p';
                selectImage();
                break;

            case R.id.update_user:
                String first_name_str = tv_user_first_name.getText().toString();
                String last_name_str = tv_user_last_name.getText().toString();
                if (first_name_str.length() == 0) {
                    tv_user_first_name.setError("Please Enter First Name");
                    tv_user_first_name.requestFocus();
                } else if (last_name_str.length() == 0) {
                    tv_user_last_name.setError("Please Enter Last Name");
                    tv_user_last_name.requestFocus();
                } else {
                    String jsonString = "";
                    Map<String, Object> mainData = new HashMap<>();
                    mainData.put("first_name", first_name_str);
                    mainData.put("last_name", last_name_str);
                    if (profile64 != null && !profile64.isEmpty())
                        mainData.put("profile_image", "" + profile64);
                    //mainData.put("passcode", listing_name_str);
                    ObjectMapper mapperObj = new ObjectMapper();
                    try {
                        jsonString = mapperObj.writeValueAsString(mainData);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(mcontext, "profile update failed" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                    updateDetails(USER_PROFILE_UPDATE, jsonString);
                }

                break;
        }
    }

    private String getJsonData() {
        /*Map<Object, Object> uploadMap = new HashMap<>();
                    uploadMap.put("name", listing_name_str);
                    uploadMap.put("address", address_str);
                    uploadMap.put("email", email_str);
                    uploadMap.put("landmark", landmark_str);
                    uploadMap.put("desc", description_str);
                    uploadMap.put("availability", AVAILABILITY + "".trim());
                    uploadMap.put("location_name", shop_location_str);
                    uploadMap.put("latitude", latitude_et_str);
                    uploadMap.put("longitude", longitude_et_str);

                    Map<String, String> mobilemap = new HashMap<>();
                    mobilemap.put("code", mobile_code_str);
                    mobilemap.put("number", mobile_et_str);
                    uploadMap.put("mobile", mobilemap);
                    Map<String, String> landlineMap = new HashMap<>();
                    landlineMap.put("code", landline_code_str);
                    landlineMap.put("number", landline_et_str);
                    uploadMap.put("landline", landlineMap);
                    Map<String, String> whatsappMap = new HashMap<>();
                    whatsappMap.put("code", "00");
                    whatsappMap.put("number", whatsapp_et_str);
                    uploadMap.put("whatsapp", whatsappMap);
                    Map<String, String> helplineMap = new HashMap<>();
                    helplineMap.put("code", "00");
                    helplineMap.put("number", helpline_et_str);
                    uploadMap.put("helpline", helplineMap);

                    data = getJsonData();//new JSONObject(uploadMap).toString();*/

        String jsonString = "";

        Map<String, Object> mainData = new HashMap<>();
        mainData.put("intent", Utility.VendorIntent);
        mainData.put("name", listing_name_str);//businessname

        Map<String, String> businessAddress = new HashMap<>();//business_address
        businessAddress.put("location", shop_location_str);
        businessAddress.put("lat", latitude_et.getText().toString());
        businessAddress.put("lng", longitude_et.getText().toString());
        businessAddress.put("line1", address_str);
        businessAddress.put("zip_code", pincode.getText().toString());
        businessAddress.put("constituency", constitueid);
        businessAddress.put("state", stateid);
        businessAddress.put("district", districtid);
        mainData.put("business_address", businessAddress);

        mainData.put("secondary_contact", helpline_et.getText().toString());
        mainData.put("whats_app_no", whatsapp_et.getText().toString());
        mainData.put("business_category", catId);//business cat
        mainData.put("business_description",description.getText().toString());
        //mainData.put("sub_categories",productsArray);//sub catid
        try {

            if (selectedCategoryIDs != null && selectedCategoryIDs.size() > 0) {
                mainData.put("sub_categories", selectedCategoryIDs);
            }

        } catch (Exception ex) {
            Toast.makeText(mcontext, "exception " + ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }

        mainData.put("owner", tv_owner_name.getText().toString());//optional
        mainData.put("gst", tv_gst_no.getText().toString());
        mainData.put("labour_certificate_number", tv_labour_certificate_no.getText().toString());
        mainData.put("fssai", tv_fssai_no.getText().toString());

        //additonal fields
        // mainData.put("availability", AVAILABILITY + "".trim());

     /*   Map<String, Object> mainData = new HashMap<>();

        //  mainData.put("ref_id", referal_id.getText().toString().trim());
        mainData.put("name", listing_name_str);//businessname
        mainData.put("location_address", shop_location_str);
        mainData.put("latitude", latitude_et_str);
        mainData.put("longitude", longitude_et_str);
        mainData.put("address", address_str);
        mainData.put("constituency_id", constitueid);
        mainData.put("pincode",pincode.getText().toString());
        mainData.put("email", email.getText().toString());

        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> two = new HashMap<>();
        // two.put("number", landline_str);
        //two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_et.getText().toString());
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", helpline_et.getText().toString());
        four.put("code", "+91");

        Map<String, String> one = new HashMap<>();
        //one.put("number", mobile_et.getText().toString()); --no update for primary contact
        //one.put("code", "+91");

        //contact.put("1", one);
       // contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        mainData.put("contacts", contact);


        mainData.put("sub_category_id",sub_cats);//
        mainData.put("owner_name",tv_owner_name.getText().toString());//optional
        mainData.put("gst_number",tv_gst_no.getText().toString());
        mainData.put("labour_certificate_number",tv_labour_certificate_no.getText().toString());
        mainData.put("fssai_number",tv_fssai_no.getText().toString());*/
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            jsonString = mapperObj.writeValueAsString(mainData);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mcontext, "profile update failed" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
        return jsonString;
    }

    private void onBankDetailsClick() {
        user_layout.setVisibility(View.GONE);
        profile_layout.setVisibility(View.GONE);
        banners_layout.setVisibility(View.GONE);
        social_layout.setVisibility(View.GONE);
        bank_details_layout.setVisibility(View.VISIBLE);
        profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        profile_text.setTextColor(getResources().getColor(R.color.black));
        banners_text.setTextColor(getResources().getColor(R.color.black));
        social_text.setTextColor(getResources().getColor(R.color.black));
        bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
        bank_text.setTextColor(getResources().getColor(R.color.white));
    }

    private void onBannersClick() {
        banners_layout.setVisibility(View.VISIBLE);
        bank_details_layout.setVisibility(View.GONE);
        social_layout.setVisibility(View.GONE);
        profile_layout.setVisibility(View.GONE);
        user_layout.setVisibility(View.GONE);
        banners_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
        banners_text.setTextColor(getResources().getColor(R.color.white));
        bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        bank_text.setTextColor(getResources().getColor(R.color.black));
        profile_text.setTextColor(getResources().getColor(R.color.black));
        social_text.setTextColor(getResources().getColor(R.color.black));
    }

    private void onSocialLayoutClick() {
        social_layout.setVisibility(View.VISIBLE);
        bank_details_layout.setVisibility(View.GONE);
        profile_layout.setVisibility(View.GONE);
        banners_layout.setVisibility(View.GONE);
        user_layout.setVisibility(View.GONE);
        social_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
        social_text.setTextColor(getResources().getColor(R.color.white));
        bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        bank_text.setTextColor(getResources().getColor(R.color.black));
        banners_text.setTextColor(getResources().getColor(R.color.black));
        profile_text.setTextColor(getResources().getColor(R.color.black));
    }

    private void onProfileClick() {
        profile_layout.setVisibility(View.VISIBLE);
        user_layout.setVisibility(View.GONE);
        bank_details_layout.setVisibility(View.GONE);
        social_layout.setVisibility(View.GONE);
        banners_layout.setVisibility(View.GONE);
        profile_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
        profile_text.setTextColor(getResources().getColor(R.color.white));
        bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        bank_text.setTextColor(getResources().getColor(R.color.black));
        banners_text.setTextColor(getResources().getColor(R.color.black));
        social_text.setTextColor(getResources().getColor(R.color.black));
    }


    private void onUserProfileClick() {
        profile_layout.setVisibility(View.GONE);
        user_layout.setVisibility(View.VISIBLE);
        bank_details_layout.setVisibility(View.GONE);
        social_layout.setVisibility(View.GONE);
        banners_layout.setVisibility(View.GONE);
        profile_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
        profile_text.setTextColor(getResources().getColor(R.color.white));
        bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
        bank_text.setTextColor(getResources().getColor(R.color.black));
        banners_text.setTextColor(getResources().getColor(R.color.black));
        social_text.setTextColor(getResources().getColor(R.color.black));
    }


    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }


    //Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items =
                {
                        "Choose from Library",
                        "Open Camera",
                        "Cancel"
                };

        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(mcontext);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(mcontext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data, SELECT_FILE);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            if (selection_character == 'c') {

                cover_image.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) cover_image.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                //coverImageBase64 =basse64Converter(cover_image);

                byte[] byteArray = baos.toByteArray();
                coverImageBase64 = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));


            } else if (selection_character == 'p') {

                img_profile.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) img_profile.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                //coverImageBase64 =basse64Converter(cover_image);

                byte[] byteArray = baos.toByteArray();
                profile64 = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
            } else {
                MultiImageModel multiImageModel = new MultiImageModel();

                multiImageModel.setBitmap(photo);
                multiImageModels.add(multiImageModel);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();


                base64ImageArray.add(resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT)));

                multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                multiImagesAdapter.notifyDataSetChanged();
                multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                multi_image_recycler.setAdapter(multiImagesAdapter);
            }
            flag = 1;
        }


    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data, int SELECT_FILE) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(mcontext.getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mcontext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);


        if (this.SELECT_FILE == SELECT_FILE) {

            if (selection_character == 'c') {
                cover_image.setImageBitmap(bm);
                Bitmap bitmap = ((BitmapDrawable) cover_image.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteArray = baos.toByteArray();
                coverImageBase64 = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
            } else if (selection_character == 'p') {
                img_profile.setImageBitmap(bm);
                Bitmap bitmap = ((BitmapDrawable) img_profile.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteArray = baos.toByteArray();
                profile64 = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
            } else {
                MultiImageModel multiImageModel = new MultiImageModel();

                multiImageModel.setBitmap(bm);
                multiImageModels.add(multiImageModel);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteArray = baos.toByteArray();
                base64ImageArray.add(resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT)));
                multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                multiImagesAdapter.notifyDataSetChanged();
                multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                multi_image_recycler.setAdapter(multiImagesAdapter);
            }
        } else {

        }
        flag = 1;


    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    private void updateBusinessDetails(String url, final String data) {

        Log.d("updateDetails", data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("updateDetail", response);
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mcontext, "Updated Successfully");


                                    preferenceManager.putString("VendorConstituency", constitueid);

                                } else {
                                    UIMsgs.showToast(mcontext, Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mcontext, MAINTENANCE);
                        }

                        mCustomDialog.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mcontext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void updateDetails(String url, final String data) {

        Log.d("updateDetails", data);
        System.out.println("aaaaaaaaaaa updateDetails  " + data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mcontext, "Updated Successfully");

                                    if (url.equals(UPDATE_IMAGES)) {
                                        base64ImageArray.clear();
                                        coverImageBase64 = null;
                                    } else if (url.equals(UPDATE_PROFILE_DETAILS)) {
                                        preferenceManager.putString("VendorConstituency", constitueid);
                                    }

                                } else {
                                    UIMsgs.showToast(mcontext, Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mcontext, MAINTENANCE);
                        }

                        mCustomDialog.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mcontext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private boolean isValidBankDetails() {

        ac_holder_name_str = ac_holder_name.getText().toString().trim();
        ac_number_str = ac_number.getText().toString().trim();
        bank_branch_str = bank_branch.getText().toString().trim();
        bank_name_str = selectedBank;//bank_name.getText().toString();
        ifsc_code_str = ifsc_code.getText().toString().trim();

        int ac_holder_l = ac_holder_name_str.length();
        int ac_number_l = ac_number_str.length();
        int bank_branch_l = bank_branch_str.length();
        int bank_name_l = selectedBank.length();//bank_name_str.length();
        int ifsc_code_l = ifsc_code_str.length();

        if (ac_holder_l == 0) {
            setEditTextErrorMethod(ac_holder_name, "Please enter holder name");
            return false;
        } else if (ac_holder_l < 3) {
            setEditTextErrorMethod(ac_holder_name, "Please enter valid holder name");
            return false;
        } else if (bank_name_l == 0) {
            //  setEditTextErrorMethod(bank_name, "Please enter bank name");
            showToast(mcontext, "Please enter bank name");
            return false;
        } else if (bank_name_l < 3) {
            //  setEditTextErrorMethod(bank_name, "Please enter valid bank name");
            showToast(mcontext, "Please enter valid bank name");
            return false;
        } else if (bank_branch_l == 0) {
            setEditTextErrorMethod(bank_branch, "Please enter Branch name");
            return false;
        }/* else if (bank_branch_l < 4) {
            setEditTextErrorMethod(bank_branch, "Please enter valid Branch name");
            return false;
        }*/ else if (ac_number_l == 0) {
            setEditTextErrorMethod(ac_number, "Please enter A/C number");
            return false;
        } else if (ac_number_l < 9) {
            setEditTextErrorMethod(ac_number, "Please enter valid A/C number");
            return false;
        } else if (ifsc_code_l == 0) {
            setEditTextErrorMethod(ifsc_code, "Please enter ifsc code");
            return false;
        } else if (ifsc_code_l < 6) {
            setEditTextErrorMethod(ifsc_code, "Please enter valid ifsc code");
            return false;
        } else if (!validateISFCCode(ifsc_code.getText().toString())) {
            setEditTextErrorMethod(ifsc_code, "Please enter valid ifsc code");
            return false;
        }
        return true;
    }

    private static final String MOBILE_PATTERN = "^[6-9]\\d{9}$";
    private final Pattern patternMobile = Pattern.compile(MOBILE_PATTERN);

    private static final String ISFC_CODE_PATTERN = "^[A-Z]{4}0[0-9]{6}$";
    private final Pattern patternISFCCode = Pattern.compile(ISFC_CODE_PATTERN);
    private Matcher matcher;

    public boolean validateMOBILENUMBER(String mobile) {
        matcher = patternMobile.matcher(mobile);
        return matcher.matches();
    }

    public boolean validateISFCCode(String code) {
        matcher = patternISFCCode.matcher(code);
        return matcher.matches();
    }

    private void showToast(Context mcontext, String message) {
        Toasty.error(mcontext, message, Toast.LENGTH_SHORT).show();
        // UIMsgs.showToast(mcontext, message);
    }

    private boolean isValidProfile() {
        int shop_loc_length = (shop_location_str = shop_location.getText().toString().trim()).length();
        int lat_length = (latitude_et_str = latitude_et.getText().toString().trim()).length();
        int long_length = (longitude_et_str = longitude_et.getText().toString().trim()).length();
        if (open.isChecked()) {
            AVAILABILITY = OPEN;
        } else if (close.isChecked()) {
            AVAILABILITY = CLOSE;
        }
        unique_id_str = unique_id.getText().toString().trim();
        category_str = category.getText().toString().trim();
        int list_name_length = (listing_name_str = listing_name.getText().toString().trim()).length();
        description_str = description.getText().toString().trim();
        int address_length = (address_str = address.getText().toString().trim()).length();

        Boolean valid = true;
        if (shop_loc_length == 0) {
            setEditTextErrorMethod(shop_location, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (shop_loc_length < 3) {
            setEditTextErrorMethod(shop_location, INVALID);
            valid = false;
        }
        if (canEditShopLoation && lat_length < 2) {
            setEditTextErrorMethod(latitude_et, INVALID);
            valid = false;
        }
        if (canEditShopLoation && long_length < 2) {
            setEditTextErrorMethod(longitude_et, INVALID);
            valid = false;
        }
       /*if (AVAILABILITY == -1) {
            showToast(mcontext, "Please provide availability");
            valid =false;
        }*/
        else if (list_name_length == 0) {
            listing_name.setError(EMPTY_NOT_ALLOWED);
            listing_name.requestFocus();
            valid = false;
        } else if (catId == null) {
            showToast(mcontext, "Please Select Business Categories");
            valid = false;
        } else if (selectedCategoryIDs == null || selectedCategoryIDs.size() == 0) {
            showToast(mcontext, "Please Select Business Categories");
            valid = false;
        } /*else if (tv_owner_name.getText().toString() == null || tv_owner_name.getText().toString().isEmpty()) {
            tv_owner_name.setError(EMPTY_NOT_ALLOWED);
            tv_owner_name.requestFocus();
            valid = false;
        }*/
        /*else if(tv_gst_no.getText().toString()==null || tv_gst_no.getText().toString().isEmpty())
        {
            tv_gst_no.setError(EMPTY_NOT_ALLOWED);
            tv_gst_no.requestFocus();
            validity = false;
        }*/
        else if (!Validations.IsEmpty(tv_gst_no.getText()) && !Validations.isValidGST(tv_gst_no.getText().toString())) {
            tv_gst_no.setError(INVALID);
            tv_gst_no.requestFocus();
            valid = false;
        }
        /*
        else if(tv_labour_certificate_no.getText().toString()==null || tv_labour_certificate_no.getText().toString().isEmpty())
        {
            tv_labour_certificate_no.setError(EMPTY_NOT_ALLOWED);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }*/
        else if (!Validations.IsEmpty(tv_labour_certificate_no.getText()) && !Validations.isValidLabourCertificationNumber(tv_labour_certificate_no.getText().toString())) {
            tv_labour_certificate_no.setError(INVALID);
            tv_labour_certificate_no.requestFocus();
            valid = false;
        }
        /*else if(tv_fssai_no.getText().toString()==null || tv_fssai_no.getText().toString().isEmpty())
        {
            tv_fssai_no.setError(EMPTY_NOT_ALLOWED);
            tv_fssai_no.requestFocus();
            validity = false;
        }*/
        else if (!Validations.IsEmpty(tv_fssai_no.getText()) && !Validations.isValidFSSAI(tv_fssai_no.getText().toString())) {
            tv_fssai_no.setError(INVALID);
            tv_fssai_no.requestFocus();
            valid = false;
        } else if (address_length < 0) {
            address.setError(EMPTY_NOT_ALLOWED);
            address.requestFocus();
            valid = false;
        } else if (stateid == null || stateid.length() == 0) {
            showToast(mcontext, "Please Select State");
            valid = false;
        } else if (districtid == null || districtid.length() == 0) {
            showToast(mcontext, "Please Select District");
            valid = false;
        } else if (constitueid == null || constitueid.length() == 0) {
            showToast(mcontext, "Please Select Constituency");
            //constituency_spinner.requestFocus();
            valid = false;
        } else if (pincode.getText() == null || pincode.getText().toString().length() == 0) {
            pincode.setError(UIValidations.EMPTY);
            pincode.requestFocus();
            valid = false;
        } else if (pincode.getText().toString().startsWith("0") || pincode.getText().toString().length() < 6) {
            pincode.setError("PIN CODE is  Invalid");
            pincode.requestFocus();
            valid = false;
        }
     /*    else if (!isValidEmail(email.getText().toString())) {
            setEditTextErrorMethod(email, INVALID_MAIL);
            valid  = false;
        }
       else if (mobile_et.getText().toString().length() == 0) {
            mobile_et.setError(UIValidations.EMPTY);
            mobile_et.requestFocus();
            valid  = false;
        }
        else if (mobile_et.getText().toString().length() < 10) {
            mobile_et.setError(INVALID_MOBILE);
            mobile_et.requestFocus();
            valid  = false;
        }else if (!validateMOBILENUMBER(mobile_et.getText().toString())) {
            setEditTextErrorMethod(mobile_et, INVALID_MOBILE);
            valid = false;
        }*/
       /* if (whatsapp_et.getText().toString().trim().isEmpty()) {
            setEditTextErrorMethod(whatsapp_et, EMPTY);
            valid = false;
        } else if (!validateMOBILENUMBER(whatsapp_et.getText().toString())) {
            setEditTextErrorMethod(whatsapp_et, INVALID_WHATSAPP);
            valid = false;
        }*/
        return valid;
    }

    private boolean isValidLinks() {
        fb_link_str = fb_link.getText().toString().trim();
        insta_link_str = instagram_link.getText().toString().trim();
        twitter_link_str = twitter_link.getText().toString().trim();
        website_str = website_link.getText().toString().trim();

        if (fb_link_str.length() > 1) {
            if (!isValidURL(fb_link_str)) {
                setEditTextErrorMethod(fb_link, "INVALID LINK");
                return false;
            }
        } else if (insta_link_str.length() > 1) {
            if (!isValidURL(insta_link_str)) {
                setEditTextErrorMethod(instagram_link, "INVALID LINK");
                return false;
            }
        } else if (twitter_link_str.length() > 1) {
            if (!isValidURL(twitter_link_str)) {
                setEditTextErrorMethod(twitter_link, "INVALID LINK");
                return false;
            }
        } else if (website_str.length() > 1) {
            if (!isValidURL(website_str)) {
                setEditTextErrorMethod(website_link, "INVALID LINK");
                return false;
            }
        }

        return true;
    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            // locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, (android.location.LocationListener) this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    public void doAction(String id) {
        switch (id) {
            case "1701":
                onUserProfileClick();
                break;
            case "1702":
                onProfileClick();
                break;
            case "1703":
                onBankDetailsClick();
                break;
            case "1704":
                onSocialLayoutClick();
                break;
            case "1705":
                onBannersClick();
                break;
        }
    }

//Location End

    public void categoryDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("aaaa CATEGORY_LIST", "" + response);
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");


                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            CategoryObject c = new CategoryObject();

                            c.setCatID(dataObject.getString("id"));
                            c.setCatName(dataObject.getString("name"));
                            c.setCatImage(dataObject.getString("image"));
                            categoryId.add(c.getCatID());
                            categories.add(c.getCatName());
                            ListCategoryObject.add(c);
                        }
                    } else {
                        UIMsgs.showToast(mcontext, messagae);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mcontext,
                            android.R.layout.simple_spinner_item, categories);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mcontext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext, error + "", Toast.LENGTH_SHORT).show();
                categoryDataFetcher();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs, ArrayList<String> selectedCategoryNames) {
        this.selectedCategoryIDs = selectedCategoryIDs;
        StringBuilder str = new StringBuilder();
        for (String eachstring : selectedCategoryNames) {
            str.append(eachstring).append(",");
        }


        String finalString = str.toString();
        if (finalString.endsWith(","))
            finalString = finalString.substring(0, finalString.length() - 1);


        String text = "<font color=#F26B35> Selected Category : </font> <font color=#333333>" + catName + "</font><br>" +
                "\n <font color=#F26B35> Sub Categories : </font> <font color=#333333>" + finalString + "</font>";

        tv_sub_categories.setText(Html.fromHtml(text));
    }

    private void showCategoryPopup() {
        //categories

        if (categoryId != null && categoryId.size() > 1) {
            ArrayList<String> selectedCategories = new ArrayList<>();

            Dialog dialog = new Dialog(mcontext);

            View contentView = View.inflate(mcontext, R.layout.select_categories, null);
            //context = contentView.getContext();
            dialog.setContentView(contentView);

            LinearLayout layout_root = dialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mcontext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels;
            layout_root.setLayoutParams(layoutParams);


            Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = dialog.findViewById(R.id.closeButton);

            RecyclerView recyclerView_categories = dialog.findViewById(R.id.recyclerView_categories);
            RecyclerView recyclerView_subcategories = dialog.findViewById(R.id.recyclerView_subcategories);

            TextView tv_no_categories = dialog.findViewById(R.id.tv_no_categories);
            TextView tv_no_sub_categories = dialog.findViewById(R.id.tv_no_sub_categories);
            TextView tv_error = dialog.findViewById(R.id.tv_error);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mcontext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_categories.setLayoutManager(linearLayoutManager);

            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(mcontext);
            linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_subcategories.setLayoutManager(linearLayoutManager1);

            CategoryAdapter categoryAdapter = new CategoryAdapter(mcontext, ListCategoryObject,
                    recyclerView_subcategories, tv_no_categories, tv_no_sub_categories, selectedCategoryIDs,
                    selectedCategories,
                    this);
            recyclerView_categories.setAdapter(categoryAdapter);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedCategoryIDs.size() > 0) {
                        dialog.dismiss();
                        setSelectedCategoryIDs(selectedCategoryIDs, selectedCategories);
                    } else {
                        tv_error.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tv_error.setVisibility(View.GONE);
                            }
                        }, 2000);
                    }
                }
            });

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            window.setGravity(Gravity.BOTTOM);

            dialog.show();

        }

    }

    @Override
    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs) {
        this.selectedCategoryIDs = selectedCategoryIDs;
    }

    @Override
    public void setCategory(CategoryObject category) {
        this.catId = category.getCatID();
        this.catName = category.getCatName();
    }


    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();

    public void statesDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));


                        }
                        // progressBar.setVisibility(View.GONE);

                    } else {
                        UIMsgs.showToast(mcontext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mcontext,
                            android.R.layout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);

                    Integer selIndex = statesId.indexOf(stateid);
                    if (selIndex != -1) {
                        state_spinner.setSelection(selIndex + 1);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mcontext, "No data found...!");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                // map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        districts.add("Select");

                        try {
                            JSONArray dataArray = dataObject.getJSONArray("districts");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject districtObject = dataArray.getJSONObject(i);
                                districtsId.add(districtObject.getString("id"));
                                districts.add(districtObject.getString("name"));
                            }
                        } catch (Exception ex) {
                        }

                    } else {
                        UIMsgs.showToast(mcontext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mcontext,
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);


                    Integer selIndex = districtsId.indexOf(districtid);
                    if (selIndex != -1) {
                        district_spinner.setSelection(selIndex + 1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("constituencies_resp", response);

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        try {
                            JSONObject dataObject = jsonData.getJSONObject("data");
                            JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject districtObject = dataArray.getJSONObject(i);
                                constituenciesId.add(districtObject.getString("id"));
                                constituencies.add(districtObject.getString("name"));
                            }
                        } catch (Exception ex) {
                        }

                    } else {
                        UIMsgs.showToast(mcontext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mcontext,
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);


                    Integer selIndex = constituenciesId.indexOf(constitueid);
                    if (selIndex != -1) {
                        constituency_spinner.setSelection(selIndex + 1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }


    public void getBankNames() {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_BANKS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Integer status = jsonData.getInt("status_code");//status_code : 200
                    if (status == 200) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            // statesId.add(dataObject.getString("code"));
                            banks.add(dataObject.getString("name"));
                        }
                       adapter = new ArrayAdapter<String>(mcontext,
                                android.R.layout.simple_spinner_item, banks);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        bank_spinner.setAdapter(adapter);
                        if (selectedBank!=null&&!selectedBank.isEmpty()){
                            Integer selIndex = banks.indexOf(selectedBank);
                            if (selIndex != -1) {
                                String val = String.valueOf(banks.size());
                                bank_spinner.setSelection(selIndex);
                            }
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                // map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    private void getUserProfile() {
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, USER_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mCustomDialog.dismiss();
                if (response != null) {
                    Log.d("getUserProfile", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        Integer status_code = jsonObject.getInt("http_code");
                        if (status || status_code == 200) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            email.setText(dataObject.getString("email"));
                            mobile_et.setText(dataObject.getString("phone"));
                            tv_user_first_name.setText(dataObject.getString("first_name"));
                            tv_user_last_name.setText(isNull(dataObject.getString("last_name")));


                            try {


                                Glide.with(mcontext)
                                        .load(dataObject.getString("profile_image"))
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.ic_baseline_person_24)
                                        .into(img_profile);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}