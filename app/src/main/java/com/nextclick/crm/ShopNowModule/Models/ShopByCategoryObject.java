package com.nextclick.crm.ShopNowModule.Models;

import java.util.ArrayList;

public class ShopByCategoryObject {
    String id,image,title,shopByCategoryID;
    private ArrayList<ProductModel> selectedProductsList;
    private ArrayList<ProductModel> selectedXProducts,selectedYProducts;

    public void setID(String id) {
        this.id=id;
    }
    public String getID() {
        return this.id;
    }
    public void setImage(String image) {
        this.image=image;
    }
    public String getImage() {
        return this.image;
    }
    public void setTitle(String title) {
        this.title=title;
    }
    public String getTitle() {
        return this.title;
    }

    public void setShopByCategoryID(String shopByCategoryID) {
        this.shopByCategoryID=shopByCategoryID;
    }
    public String getShopByCategoryID() {
        return this.shopByCategoryID;
    }


    public void setSelectedProducts(ArrayList<ProductModel> selectedProductsList) {
        this.selectedProductsList=selectedProductsList;
    }
    public ArrayList<ProductModel> getSelectedProducts() {
        return this.selectedProductsList;
    }


    public void setselectedYProducts(ArrayList<ProductModel> selectedYProducts) {
        this.selectedYProducts=selectedYProducts;
    }
    public ArrayList<ProductModel> getselectedYProducts() {
        return this.selectedYProducts;
    }
    public void setselectedXProducts(ArrayList<ProductModel> selectedXProducts) {
        this.selectedXProducts=selectedXProducts;
    }
    public ArrayList<ProductModel> getselectedXProducts() {
        return this.selectedXProducts;
    }

    String maxOrderQty,discount;
    public void setDiscount(String discount) {
        this.discount=discount;
    }
    public String getDiscount() {
        return this.discount;
    }

    public void setMaxOrderQty(String maxOrderQty) {
        this.maxOrderQty=maxOrderQty;
    }
    public String getMaxOrderQty() {
        return this.maxOrderQty;
    }
}
