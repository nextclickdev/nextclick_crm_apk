package com.nextclick.crm.ShopNowModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.AddPromotionActivity;
import com.nextclick.crm.ShopNowModule.Activities.JoinOfferPromotion;
import com.nextclick.crm.ShopNowModule.Activities.MyPromotionsActivity;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsListActivity;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class PromotionsAdapter extends RecyclerView.Adapter<PromotionsAdapter.ViewHolder> {

    private Boolean is_admin =false;
    private boolean isMyPromotions;
    LayoutInflater inflter;
    Context context;
    private String token;
    List<PromotionObject> data;
    boolean isPromotionListLayout =false;
    PromotionsListActivity activity;

    public PromotionsAdapter(FragmentActivity activity, List<PromotionObject> promotions) {
        this.context = activity;
        this.data = promotions;
    }

    public PromotionsAdapter(PromotionsListActivity activity, List<PromotionObject> promotions,boolean isPromotionListLayout,boolean isMyPromotions) {
        this.context = activity;
        this.activity=activity;
        this.data = promotions;
        this.isPromotionListLayout=isPromotionListLayout;
        this.isMyPromotions=isMyPromotions;


    }
    public PromotionsAdapter(PromotionsListActivity activity, List<PromotionObject> promotions,boolean isPromotionListLayout,boolean isMyPromotions,Boolean is_admin) {
        this.context = activity;
        this.activity = activity;
        this.data = promotions;
        this.isPromotionListLayout = isPromotionListLayout;
        this.isMyPromotions = isMyPromotions;
        this.is_admin = is_admin;
    }

    public PromotionsAdapter(MyPromotionsActivity activity, List<PromotionObject> promotions,
                             boolean isPromotionListLayout, boolean isMyPromotions) {
        this.context = activity;
        this.data = promotions;
        this.isPromotionListLayout=isPromotionListLayout;
        this.isMyPromotions=isMyPromotions;
    }

    @NonNull
    @Override
    public PromotionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(context).inflate(R.layout.promotion_list_view, parent, false);


        token ="Bearer " + new PreferenceManager(context).getString(USER_TOKEN);
        return new PromotionsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PromotionsAdapter.ViewHolder holder, final int position) {
        final PromotionObject promotionObject = data.get(position);

        if(isMyPromotions && promotionObject.getcontent_type().equals("3")) {
            ViewGroup.LayoutParams params = holder.top_layout.getLayoutParams();
            float factor = holder.itemView.getContext().getResources().getDisplayMetrics().density;
            params.height = (int) (130 * factor);
            holder.top_layout.setLayoutParams(params);
        }

        holder.tv_header.setText(promotionObject.getHeader());
        holder.tv_subHeader.setText(promotionObject.getSubHeader());
        holder.tv_description.setText(promotionObject.getDescription());

        holder.layout_vendor_promotion.setVisibility(View.GONE);
        if(promotionObject.getcontent_type().equals("3")) {
            holder.layout_vendor_promotion.setVisibility(View.VISIBLE);

            if(promotionObject.getOfferTile()!=null && !promotionObject.getOfferTile().equals("null"))
            holder.tv_offer_tile.setText(promotionObject.getOfferTile());
            if(promotionObject.getOfferDetails()!=null && !promotionObject.getOfferDetails().equals("null"))
            holder.tv_offer_description.setText(promotionObject.getOfferDetails());
        }

        /*if(!promotionObject.getcontent_type().equals("1"))
        {
            holder.tv_header.setVisibility(View.GONE);
        }*/

        if (promotionObject.getJoined_user_id() != null && !promotionObject.getJoined_user_id().isEmpty()) {
            if(!isMyPromotions) {
                holder.btn_activate.setEnabled(false);
                holder.btn_activate.setClickable(false);
                holder.btn_activate.setAlpha(0.5f);
            }
            else
            holder.btn_activate.setVisibility(View.GONE);
        } else {
            if(!isMyPromotions) {
                holder.btn_activate.setEnabled(true);
                holder.btn_activate.setClickable(true);
                holder.btn_activate.setAlpha(1f);
            }
            else
            holder.btn_activate.setVisibility(View.VISIBLE);

            holder.btn_activate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (activity != null) {

                        new AlertDialog.Builder(context)
                                .setTitle(context.getString(R.string.join_promotion))
                                .setMessage(context.getString(R.string.join_promotion_header))
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        if (promotionObject.getcontent_type().equals("4")) {
                                            Intent intent = new Intent(context, JoinOfferPromotion.class);
                                            intent.putExtra("promotionID", promotionObject.getID());
                                            context.startActivity(intent);
                                        } else {
                                            activity.joinPromotin(promotionObject.getID());
                                        }

                                    }})
                                .setNegativeButton(android.R.string.no, null).show();


                    }
                }
            });
        }
        if (!isMyPromotions && !isPromotionListLayout) {
            holder.tv_expiry.setVisibility(View.VISIBLE);

            Long daysLeft = getTimeLeft(promotionObject.getpublished_on(), promotionObject.getexpired_on());
            if (daysLeft <= 0)
                daysLeft = 0L;
            holder.tv_expiry.setText(daysLeft + " days left");


        }
        holder.img_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddPromotionActivity.class);
                intent.putExtra("promotionID", promotionObject.getID());
                intent.putExtra("IsViewPromotion", true);
                context.startActivity(intent);
            }
        });


        holder.img_edit.setVisibility(View.GONE);//always for current flow
        if (!isMyPromotions && !is_admin) {
            holder.img_delete.setVisibility(View.GONE);
            holder.img_edit.setVisibility(View.GONE);
        } else {
            holder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AddPromotionActivity.class);
                    intent.putExtra("promotionID", promotionObject.getID());
                    context.startActivity(intent);
                }
            });

            holder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (activity != null) {
                        activity.deletePrmotion(promotionObject.getID());
                    }
                }
            });
        }

        Picasso.get().load(promotionObject.getIcon())
                .placeholder(R.drawable.no_image)//ic_baseline_image_not_supported_24
                .into(holder.img_view);

       // if(!isMyPromotions)
         //   holder.img_view.

    //no_image
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setRefresh(List<PromotionObject> listPromotions) {
        this.data=listPromotions;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_header,tv_subHeader,tv_description,tv_icon,tv_expiry,tv_offer_tile,tv_offer_description;
        CardView card_view;
        ImageView img_view,img_edit,img_delete;
        //ImageView btn_activate;
        Button btn_activate;
        RelativeLayout layout_vendor_promotion;
        LinearLayout top_layout;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            top_layout= itemView.findViewById(R.id.top_layout);
            card_view= itemView.findViewById(R.id.card_view);
            img_view= itemView.findViewById(R.id.img_view);
            tv_icon = itemView.findViewById(R.id.tv_icon);
            tv_header = itemView.findViewById(R.id.tv_header1);
            tv_subHeader = itemView.findViewById(R.id.tv_subHeader);
            tv_description = itemView.findViewById(R.id.tv_description);
            btn_activate = itemView.findViewById(R.id.btn_join_pro);
            //btn_activate = itemView.findViewById(R.id.btn_activate);
            tv_expiry= itemView.findViewById(R.id.tv_expiry);
            img_edit= itemView.findViewById(R.id.img_edit);
            img_delete= itemView.findViewById(R.id.img_delete);

            layout_vendor_promotion= itemView.findViewById(R.id.layout_vendor_promotion);
            tv_offer_tile= itemView.findViewById(R.id.tv_offer_tile);
            tv_offer_description= itemView.findViewById(R.id.tv_offer_description);
        }
    }

    public long getTimeLeft(String startdate,String enddate) {
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate=null,endDate=null;
        try {
            startDate = mdformat.parse(startdate);
            endDate = mdformat.parse(enddate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;

        return elapsedDays;
    }
}

