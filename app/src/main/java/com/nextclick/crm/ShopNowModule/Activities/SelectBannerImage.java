package com.nextclick.crm.ShopNowModule.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.SliderAdapter;
import com.nextclick.crm.ShopNowModule.Models.SliderItems;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SelectBannerImage extends AppCompatActivity implements View.OnClickListener {

    private CustomDialog mCustomDialog;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private ViewPager2 viewPager2;
    List<SliderItems> sliderItems;
    private final Handler sliderHandler = new Handler();
    private Runnable sliderRunnable;
    LinearLayout layout_position;
    TextView tv_current_index,tv_sliders_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_banner_image);

       // getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }

        mContext = SelectBannerImage.this;
        mCustomDialog = new CustomDialog(SelectBannerImage.this);
        preferenceManager = new PreferenceManager(mContext);

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to select promotion banner image");
        }

        ImageView img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        //TextView tv_select = findViewById(R.id.tv_select);
        //tv_select.setOnClickListener(this);
        String promotionType = "";
        if (getIntent().hasExtra("promotionType")) {
            promotionType = getIntent().getStringExtra("promotionType");
        }
        layout_position = findViewById(R.id.layout_position);
        tv_sliders_count= findViewById(R.id.tv_sliders_count);
        tv_current_index= findViewById(R.id.tv_current_index);
        viewPager2 = findViewById(R.id.viewPagerImageSlider);
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(3);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float r = 1 - Math.abs(position);
                page.setScaleY(0.85f + r * 0.15f);
            }
        });

        viewPager2.setPageTransformer(compositePageTransformer);

       viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                tv_current_index.setText(""+(position+1));

               // sliderHandler.removeCallbacks(sliderRunnable);
              //  sliderHandler.postDelayed(sliderRunnable, 2000); // slide duration 2 seconds
            }
        });


        /* sliderRunnable = new Runnable() {
            @Override
            public void run() {
                viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
            }
        };*/


        getBanners(promotionType);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //sliderHandler.removeCallbacks(sliderRunnable);
    }

    @Override
    protected void onResume() {
        super.onResume();
       // sliderHandler.postDelayed(sliderRunnable, 2000);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.img_back) {
            finish();
        }
    }
    private void getBanners(String promotionType) {
        sliderItems = new ArrayList<>();

        /*String[] staticTypes=new String[]{"https://cognitiveseo.com/blog/wp-content/uploads/2019/11/URL-structure_cognitiveSEO.jpg",
        "https://www.ryrob.com/wp-content/uploads/2020/04/What-is-a-URL-Website-URLs-Explained-and-Best-Practices-for-Creating-URLs.jpg",
        "https://www.wpbeginner.com/wp-content/uploads/2016/12/customshorturls.jpg",
                "https://cognitiveseo.com/blog/wp-content/uploads/2019/11/URL-structure_cognitiveSEO.jpg",
                "https://www.ryrob.com/wp-content/uploads/2020/04/What-is-a-URL-Website-URLs-Explained-and-Best-Practices-for-Creating-URLs.jpg",
                "https://www.wpbeginner.com/wp-content/uploads/2016/12/customshorturls.jpg",
                "https://cognitiveseo.com/blog/wp-content/uploads/2019/11/URL-structure_cognitiveSEO.jpg",
                "https://www.ryrob.com/wp-content/uploads/2020/04/What-is-a-URL-Website-URLs-Explained-and-Best-Practices-for-Creating-URLs.jpg",
                "https://www.wpbeginner.com/wp-content/uploads/2016/12/customshorturls.jpg",
                "https://cognitiveseo.com/blog/wp-content/uploads/2019/11/URL-structure_cognitiveSEO.jpg",
                "https://www.ryrob.com/wp-content/uploads/2020/04/What-is-a-URL-Website-URLs-Explained-and-Best-Practices-for-Creating-URLs.jpg",
                "https://www.wpbeginner.com/wp-content/uploads/2016/12/customshorturls.jpg",
                "https://cognitiveseo.com/blog/wp-content/uploads/2019/11/URL-structure_cognitiveSEO.jpg",
                "https://www.ryrob.com/wp-content/uploads/2020/04/What-is-a-URL-Website-URLs-Explained-and-Best-Practices-for-Creating-URLs.jpg",
                "https://www.wpbeginner.com/wp-content/uploads/2016/12/customshorturls.jpg"};

        for (int i = 0; i < staticTypes.length; i++) {

            SliderItems sliderItem = new SliderItems(staticTypes[i]);
           // sliderItem.setID("" + (i + 1));
            sliderItems.add(sliderItem);
        }

        viewPager2.setAdapter(new SliderAdapter(this,sliderItems));
        layout_position.setVisibility(View.VISIBLE);
        tv_sliders_count.setText(""+sliderItems.size());*/

        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_BANNERS_TEMPLATES,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("GET_BANNERS_TEMPLATES response  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONArray dataobj = jsonObject.getJSONArray("data");
                                for (int i = 0; i < dataobj.length(); i++) {
                                    JSONObject transobj = dataobj.getJSONObject(i);
                                    SliderItems sliderItem = new SliderItems(transobj.getString("name"));
                                     sliderItem.setID("" + (i + 1));
                                    sliderItems.add(sliderItem);
                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        } finally {
                            mCustomDialog.dismiss();
                            showImages();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "onErrorResponse " + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void showImages() {

        viewPager2.setAdapter(new SliderAdapter(this, sliderItems));
        layout_position.setVisibility(View.VISIBLE);
        tv_sliders_count.setText("" + sliderItems.size());
    }

    public void onBannerSelect(SliderItems sliderItems) {
        Intent intent = new Intent();
        intent.putExtra("SelectedImage", sliderItems.getImage());
        setResult(AddPromotionActivity.FLAG_GET_BANNER, intent);
        finish();
    }
}