package com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments;

import static androidx.fragment.app.DialogFragment.STYLE_NORMAL;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.ShopNowModule.Adapters.DashboardCartAdapter;
import com.nextclick.crm.ShopNowModule.Fragments.MyProductFragments.ProductsHomeFragment;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.activities.customer_support.CustomerSupportActivity;
import com.nextclick.crm.orders.fragments.OrdersHistoryFragment;
import com.nextclick.crm.orders.fragments.WalletFragment;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.promocodes.PromocodelistFragment;
import com.nextclick.crm.subcriptions.FeaturesModel;
import com.nextclick.crm.subcriptions.PackageFeatureModel;
import com.nextclick.crm.subcriptions.SubcriptionsActivity;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.nextclick.crm.subcriptions.SubscriptionModel;
import com.zoho.desk.asap.ZDPHomeConfiguration;
import com.zoho.desk.asap.api.ZohoDeskPortalSDK;
import com.zoho.desk.asap.asap_community.ZDPortalCommunity;
import com.zoho.desk.asap.asap_tickets.ZDPortalTickets;
import com.zoho.desk.asap.withchat.ZDPortalHome;
import com.zoho.desk.chat.ZDPortalChat;
import com.zoho.desk.chat.ZDPortalChatUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.GET_STATISTICS;
import static com.nextclick.crm.Config.Config.OLD_SUBCRIPTIONS;
import static com.nextclick.crm.Config.Config.SHOP_AVAILABLE;
import static com.nextclick.crm.Config.Config.VENDOR_PRODUCTS_COUNT;
import static com.nextclick.crm.Config.Config.WALLETHISTORY;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class HomeFragment extends Fragment implements View.OnClickListener {


    private View root;
    private CardView product_card, orders_card, settings_card, profile_card, wallet_card, promocard_card, report_card;
    // private CardView product_card, orders_card, settings_card, profile_card,wallet_card,promocard_card;
    private Context mContext;
    PreferenceManager preferenceManager;
    String token;
    private BottomSheetDialog bottomSheetDialog;

    private ProgressBar active_products_progress_bar, all_orders_progress_bar, completed_orders_progress_bar;
    private TextView active_products_current, active_products_target, all_orders_current,
            all_orders_target, completed_orders_current, completed_orders_target, tv_amount;
    int all_orders_current_count = 0, all_orders_increasing_count = 150;
    int completed_orders_current_count = 0, completed_orders_increasing_count = 100;
    private SubscriptionModel subscriptionModel;
    int active_products_current_count = 0, active_products_increasing_count = 50;
    String year, month, day, start_date_str, end_date_str;
    private CustomDialog mCustomDialog;
    private TextView tv_title, tv_daysleft;
    RelativeLayout layout_subscriptions_panel;
    RecyclerView recycler_dashobard;
    boolean checkservice;
    DashboardCartAdapter dashboardCartAdapter;
    public ZohoDeskPortalSDK apiProvider;
    LabeledSwitch labeledSwitch;
    int availability;
    private boolean isPlanExists;
    private boolean isOpened = false;
    public HomeFragment(Context mContext) {

    }

    public HomeFragment(boolean checkservice) {
        this.checkservice = checkservice;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (UserData.isNewDesign)
            root = inflater.inflate(R.layout.fragment_home_new, container, false);
        else
            root = inflater.inflate(R.layout.fragment_home, container, false);
        initView();

        product_card.setOnClickListener(this);
        orders_card.setOnClickListener(this);
        settings_card.setOnClickListener(this);
        profile_card.setOnClickListener(this);
        wallet_card.setOnClickListener(this);
        report_card.setOnClickListener(this);
        promocard_card.setOnClickListener(this);
        return root;
    }

    private void initView() {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        product_card = root.findViewById(R.id.product_card);
        orders_card = root.findViewById(R.id.orders_card);
        settings_card = root.findViewById(R.id.settings_card);
        profile_card = root.findViewById(R.id.profile_card);
        wallet_card = root.findViewById(R.id.wallet_card);
        report_card = root.findViewById(R.id.report_card);
        promocard_card = root.findViewById(R.id.promocard_card);
        active_products_progress_bar = root.findViewById(R.id.active_products_progress_bar);
        all_orders_progress_bar = root.findViewById(R.id.all_orders_progress_bar);
        completed_orders_progress_bar = root.findViewById(R.id.completed_orders_progress_bar);
        active_products_current = root.findViewById(R.id.active_products_current);
        active_products_target = root.findViewById(R.id.active_products_target);
        all_orders_current = root.findViewById(R.id.all_orders_current);
        all_orders_target = root.findViewById(R.id.all_orders_target);
        completed_orders_current = root.findViewById(R.id.completed_orders_current);
        completed_orders_target = root.findViewById(R.id.completed_orders_target);
        tv_amount = root.findViewById(R.id.tv_amount);
        tv_title = root.findViewById(R.id.tv_title);
        tv_daysleft = root.findViewById(R.id.tv_daysleft);
        layout_subscriptions_panel = root.findViewById(R.id.layout_subscriptions_panel);
        mCustomDialog = new CustomDialog(mContext);
        subscriptionModel = new SubscriptionModel();
        labeledSwitch = root.findViewById(R.id.service_availability_status);
        labeledSwitch.setLabelOff(getString(R.string.close));
        labeledSwitch.setLabelOn(getString(R.string.open));
        labeledSwitch.setColorOn(getResources().getColor(R.color.orange));
        labeledSwitch.setColorOff(getResources().getColor(R.color.white));

        try {
            labeledSwitch.setOn(preferenceManager.getString("availability").equalsIgnoreCase("1"));
        } catch (NullPointerException e) {
            labeledSwitch.setVisibility(View.GONE);
        }


        labeledSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                String text = "";
                // labeledSwitch.setOn(!isOn);
                if (availability == 1) {
                    text = getString(R.string.close);
                } else {
                    text = getString(R.string.open);
                }
                new AlertDialog.Builder(mContext)
                        .setTitle(getString(R.string.shop_status))
                        .setPositiveButton("" + text, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (isOn) {
                                    Map<String, Object> mainData = new HashMap<>();
                                    mainData.put("availability", 1);
                                    String data = new JSONObject(mainData).toString();
                                    updateDetails(data, 1);
                                } else {
                                    Map<String, Object> mainData = new HashMap<>();
                                    mainData.put("availability", 0);
                                    String data = new JSONObject(mainData).toString();
                                    updateDetails(data, 0);
                                }

                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                labeledSwitch.setOn(!labeledSwitch.isOn());
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                //UIMsgs.showToast(mContext,"onSwitched "+isOn);
            }
        });

        System.out.println("aaaaaaa checkservice  " + checkservice);

        if (UserData.isNewDesign) {
            recycler_dashobard = root.findViewById(R.id.recycler_dashobard);
            dashboardCartAdapter = new DashboardCartAdapter(mContext, this,isPlanExists,subscriptionModel);
            recycler_dashobard.setAdapter(dashboardCartAdapter);
            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
            recycler_dashobard.setLayoutManager(staggeredGridLayoutManager);
        }
        getoldSubscriptions();

        getDatewallet();
        getWallet(start_date_str, end_date_str, "", "");

        //  getStatistics();

        layout_subscriptions_panel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SubcriptionsActivity.class);
                intent.putExtra("isPlanExists", "" + isPlanExists);
                startActivity(intent);
            }
        });


        if (preferenceManager != null && preferenceManager.getString("VendorName") == null) {
            CardView layout_subscriptions = root.findViewById(R.id.layout_subscriptions);
            layout_subscriptions.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getoldSubscriptions();
    }

    private void updateDetails(final String data, int whichposition) {

        Log.d("updateDetails", data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_AVAILABLE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(getContext(), "Updated Successfully");

                                    if (whichposition == 0) {
                                        labeledSwitch.setOn(false);
                                        availability = 0;
                                        preferenceManager.putString("availability", "" + 0);
                                    } else if (whichposition == 2) {

                                    } else {
                                        labeledSwitch.setOn(true);
                                        availability = 1;
                                        preferenceManager.putString("availability", "" + 1);
                                    }
                                } else {
                                    UIMsgs.showToast(getContext(), Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaaa error " + e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(getContext(), MAINTENANCE);
                        }

                        mCustomDialog.dismiss();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa error " + error.getMessage());
                mCustomDialog.dismiss();
                UIMsgs.showToast(getContext(), OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaaaa token " + preferenceManager.getString(USER_TOKEN));
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void getDatewallet() {
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year = "" + calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }
        if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str = year + "-" + month + "-" + day;
        end_date_str = year + "-" + month + "-" + day;

    }

    public void getWallet(String start_date_str, String end_date_str, String s, String s1) {

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                try {
                                    JSONObject paymentobj = dataobj.getJSONObject("user");
                                    tv_amount.setText("₹ " + paymentobj.getString("wallet"));
                                } catch (JSONException e2) {

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getStatistics() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_STATISTICS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa statisticks  " + jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            active_products_current_count = dataObject.getInt("active_products_count");
                            all_orders_current_count = dataObject.getInt("pending_orders_count")
                                    + dataObject.getInt("completed_orders_count")
                                    + dataObject.getInt("cancelled_orders_count")
                                    + dataObject.getInt("rejected_orders_count");
                            completed_orders_current_count = dataObject.getInt("completed_orders_count");
                            active_products_current.setText(active_products_current_count + "");
                            all_orders_current.setText(all_orders_current_count + "");
                            completed_orders_current.setText(completed_orders_current_count + "");
                            progressSetter();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("aaaaaaaaa catch  " + e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaa error  " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void progressSetter() {

        if (all_orders_current_count <= all_orders_increasing_count) {
            all_orders_progress_bar.setMax(all_orders_increasing_count);
            all_orders_progress_bar.setProgress(all_orders_current_count);
            all_orders_target.setText(all_orders_increasing_count + "");
        } else {
            while (all_orders_current_count >= all_orders_increasing_count) {
                all_orders_increasing_count = all_orders_increasing_count + 150;//150 is default count
            }
            all_orders_progress_bar.setMax(all_orders_increasing_count);
            all_orders_progress_bar.setProgress(all_orders_current_count);
            all_orders_target.setText(all_orders_increasing_count + "");
        }

        if (completed_orders_current_count <= completed_orders_increasing_count) {
            completed_orders_progress_bar.setMax(completed_orders_increasing_count);
            completed_orders_progress_bar.setProgress(completed_orders_current_count);
            completed_orders_target.setText(completed_orders_increasing_count + "");
        } else {
            while (completed_orders_current_count >= completed_orders_increasing_count) {
                completed_orders_increasing_count = completed_orders_increasing_count + 100;//100 is default count
            }
            completed_orders_progress_bar.setMax(completed_orders_increasing_count);
            completed_orders_target.setText(completed_orders_increasing_count + "");
            completed_orders_progress_bar.setProgress(completed_orders_current_count);

        }

        if (active_products_current_count <= active_products_increasing_count) {
            active_products_progress_bar.setMax(active_products_increasing_count);
            active_products_progress_bar.setProgress(active_products_current_count);
            active_products_target.setText(active_products_increasing_count + "");
        } else {
            while (active_products_current_count >= active_products_increasing_count) {
                active_products_increasing_count = active_products_increasing_count + 50;//50 is default count
            }
            active_products_progress_bar.setMax(active_products_increasing_count);
            active_products_progress_bar.setProgress(active_products_current_count);
            active_products_target.setText(active_products_increasing_count + "");
        }


    }

    public void doAction(Integer id) {

        if (preferenceManager != null && preferenceManager.getString("VendorName") == null) {
            askLogin();
            return;
        }
        if (!checkservice) {
            Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
            return;
        }

        FragmentTransaction ft;
        switch (id) {

            case R.id.product_card:
                if (checkservice) {

                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.catalogue_products)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (Constants.Use_ActivityDesign) {
                        Intent i = new Intent(getContext(), MainProductpage.class);
                        startActivity(i);
                    } else {
                        ft = getFragmentManager().beginTransaction();
                        clearFragments();
                        getProductscount(ft);

                    }
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                /*ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.shop_now_nav_host_fragment, new MyProductsFragment(mContext));
                getActivity().setTitle(getString(R.string.my_promotions));
                ft.commit();*/
                break;

            case R.id.orders_card:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.orders_listing)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ft = getFragmentManager().beginTransaction();
                    clearFragments();
               /* final Handler handler = new Handler(Looper.getMainLooper());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        OrdersHomeFragment productsHomeFragment = new OrdersHomeFragment(mContext);
                        handleBackEvent(productsHomeFragment);
                        ft.replace(R.id.shop_now_nav_host_fragment, productsHomeFragment, "ProductsHomeFragment");
                        getActivity().setTitle(getString(R.string.Orders));
                        setToolbarTile(getString(R.string.Orders));
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                }, 100);*/
                    // Fragment fragment =new OrdersFragment(mContext);
                    Fragment fragment = new OrdersHistoryFragment(mContext);
                    //Fragment fragment =new OrdersHomeFragment(mContext);
                    handleBackEvent(fragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, fragment);
                    getActivity().setTitle(getString(R.string.Orders));
                    setToolbarTile(getString(R.string.Orders));
                    ft.addToBackStack(null);
                    ft.commit();
                    ((ShopNowHomeActivity) getActivity()).setOrderFragmentHeaders(fragment);
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.settings_card:
                if (checkservice) {
//                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.products_listing)) {
//                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                    ft = getFragmentManager().beginTransaction();
                    Fragment settingsFragment = new SettingsFragment(Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.catalogue_products));
                    handleBackEvent(settingsFragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, settingsFragment);
                    getActivity().setTitle(getString(R.string.action_settings));
                    setToolbarTile(getString(R.string.action_settings));
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.profile_card:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.vendor_shop_profile)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    ft = getFragmentManager().beginTransaction();
                    Fragment profilefragment = new Profilefragment(mContext);
                    handleBackEvent(profilefragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, profilefragment);
                    getActivity().setTitle("Profile");
                    setToolbarTile("Profile");
                    ft.addToBackStack(null);
                    ft.commit();
                    ((ShopNowHomeActivity) getActivity()).setProfileFragmentHeaders(profilefragment);
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.wallet_card:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.wallet_listing)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ft = getFragmentManager().beginTransaction();
                    Fragment walletFragment = new WalletFragment(mContext);
                    handleBackEvent(walletFragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, walletFragment);
                    getActivity().setTitle(getString(R.string.wallet));
                    setToolbarTile(getString(R.string.wallet));
                    ft.addToBackStack(null);
                    ft.commit();
                    ((ShopNowHomeActivity) getActivity()).setWalletFragmentHeaders(walletFragment);
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.promocard_card:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.promocodes)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ft = getFragmentManager().beginTransaction();
                    Fragment promocodelistFragment = new PromocodelistFragment(mContext);
                    handleBackEvent(promocodelistFragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, promocodelistFragment);
                    getActivity().setTitle("Promocodes");
                    setToolbarTile("Promocodes");
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.report_card:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.reports)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ft = getFragmentManager().beginTransaction();
                    Fragment reportFragment = new ReportsFragment(mContext);
                    handleBackEvent(reportFragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, reportFragment);
                    getActivity().setTitle(getString(R.string.reports));
                    setToolbarTile(getString(R.string.reports));
                    ft.addToBackStack(null);
                    ft.commit();
                    ((ShopNowHomeActivity) getActivity()).setReportFragmentHeaders(reportFragment);
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.support_card:
                if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.customer_feedback)) {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(new Intent(requireContext(), CustomerSupportActivity.class));
            /*    if (checkservice) {
                    //support related action
                    ZohoDeskPortalSDK.Logger.enableLogs();
                    ZDPortalChatUser chatUser = new ZDPortalChatUser();
                    chatUser.setName(preferenceManager.getString("VendorName"));
                    chatUser.setEmail(preferenceManager.getString("email"));
                    // chatUser.setPhone(phone);
                    ZDPortalChat.setGuestUserDetails(chatUser);

                    apiProvider = ZohoDeskPortalSDK.getInstance(getActivity());
                    apiProvider.initDesk(Long.parseLong(getResources().getString(R.string.orgid)), "edbsndc215d3e00c2eb6a3ef71cd0050c1a052c0a4eb43c04324cefc8273de7a28cc9",
                            ZohoDeskPortalSDK.DataCenter.IN);
                    ZDPHomeConfiguration homeConfiguration = new ZDPHomeConfiguration.Builder()
                            .showKB(true)
                            .showMyTickets(true)
                            .showCommunity(true)
                            .showLiveChat(true)
                            .showNavDrawer(true).build();
                    ZDPortalHome.show(getActivity(), homeConfiguration);
                    ZDPortalTickets.show(getActivity());
                    ZDPortalCommunity.show(getActivity());
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }*/

                break;
            case 101:
                if (checkservice) {
                    if (!Utility.isServiceOptionEnabled(Utility.getSettingInstance(mContext), Utility.promotions)) {
                        Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_granted), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    ft = getFragmentManager().beginTransaction();
                    Fragment promotionsFragment = new PromotionsFragment(mContext);
                    handleBackEvent(promotionsFragment);
                    ft.replace(R.id.shop_now_nav_host_fragment, promotionsFragment);
                    getActivity().setTitle(getString(R.string.Promotions));
                    setToolbarTile(getString(R.string.Promotions));
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Toast.makeText(mContext, "" + mContext.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                }

                break;

        }

    }

    public void getProductscount(FragmentTransaction ft) {

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, VENDOR_PRODUCTS_COUNT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        mCustomDialog.dismiss();
                        if (response != null) {
                            Log.d("Productscount_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  myproducts " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");

                                if (status) {
                                    try {
                                        //JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        String catalogue_count = dataObject.getString("catalogue_count");
                                        int inventory_instock_count = dataObject.getInt("inventory_instock_count");
                                        int inventory_outofstock_count = dataObject.getInt("inventory_outofstock_count");
                                        String pendig_count = dataObject.getString("pendig_count");
                                        String approved_count = dataObject.getString("approved_count");

                                        final Handler handler = new Handler(Looper.getMainLooper());
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                ProductsHomeFragment productsHomeFragment = new ProductsHomeFragment(mContext);
                                                handleBackEvent(productsHomeFragment);
                                                ft.replace(R.id.shop_now_nav_host_fragment, productsHomeFragment, "ProductsHomeFragment");
                                                getActivity().setTitle(getString(R.string.my_products));
                                                setToolbarTile(getString(R.string.my_products));
                                                ft.addToBackStack(null);
                                                ft.commit();
                                                ((ShopNowHomeActivity) getActivity()).setProductFragmentHeaders(productsHomeFragment, (inventory_instock_count + inventory_outofstock_count), catalogue_count, pendig_count, approved_count);
                                            }
                                        }, 100);

                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch myproduct  " + e.getMessage());
                                        e.printStackTrace();
                                    }
                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  " + e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomDialog.dismiss();
                        System.out.println("aaaaaaa error 111  " + error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaaaaa token  " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void askLogin() {
        ((ShopNowHomeActivity) getActivity()).askLogin();
    }

    @Override
    public void onClick(View v) {
        doAction(v.getId());
    }

    private void handleBackEvent(Fragment fragment) {
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                setToolbarTile(getString(R.string.menu_home));
                getFragmentManager().popBackStack();
                /*if(getFragmentManager().getBackStackEntryCount()>0) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.shop_now_nav_host_fragment, new HomeFragment());
                    ft.commit();
                }*/
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(fragment, callback);
    }

    private void setToolbarTile(String title) {
        ((ShopNowHomeActivity) getActivity()).setToolbarTile(title);
        // getSupportActionBar().setTitle(title);
    }

    private void clearFragments() {
        for (Fragment fragment : getFragmentManager().getFragments()) {
            if (fragment instanceof HomeFragment) {
                continue;
            } else if (fragment != null) {
                getFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }

    private void getoldSubscriptions() {
        mCustomDialog.show();
        String url = OLD_SUBCRIPTIONS + "/?service_id=2";
        System.out.println("aaaaaaaa oldsubscriptions  " + url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response != null) {
                                mCustomDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa response OLD_SUBCRIPTIONS  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject dataObject1 = jsonArray.getJSONObject(i);
                                        if (dataObject1.getString("status").equalsIgnoreCase("active")) {
                                            isPlanExists = true;
                                            String days = dataObject1.getJSONObject("packages").getString("days");
                                            String dateStart = getDate();
                                            String dateStop = dataObject1.getString("created_at");
                                            JSONArray packageFeaturesArray = dataObject1.getJSONArray("package_features");
//                                        JsonParser parser = new JsonParser();
//                                        JsonElement mJson = parser.parse(response);
//                                        Gson gson = new Gson();
//                                        CouponModel couponModel = gson.fromJson(mJson, CouponModel.class);
                                            ArrayList<PackageFeatureModel> packageFeatureModelArrayList = new ArrayList<>();
                                            for (int j = 0; j < packageFeaturesArray.length(); j++) {
                                                JSONObject packageFeatureObject = packageFeaturesArray.getJSONObject(j);
                                                PackageFeatureModel packageFeatureModel = new PackageFeatureModel();
                                                packageFeatureModel.setTitle(packageFeatureObject.getString("title"));
                                                packageFeatureModel.setIs_active(packageFeatureObject.getInt("is_active"));
                                                JSONArray featuresArray = packageFeatureObject.getJSONArray("features");
                                                ArrayList<FeaturesModel> featuresModelArrayList = new ArrayList<>();
                                                for (int k = 0; k < featuresArray.length(); k++) {
                                                    JSONObject featuresObject = featuresArray.getJSONObject(k);
                                                    FeaturesModel featuresModel = new FeaturesModel();
                                                    featuresModel.setDescription(featuresObject.getString("description"));
                                                    featuresModel.setStatus(featuresObject.getInt("status"));
                                                    featuresModelArrayList.add(featuresModel);
                                                    packageFeatureModel.setFeaturesModelArrayList(featuresModelArrayList);
                                                }
                                                packageFeatureModelArrayList.add(packageFeatureModel);
                                                subscriptionModel.setPackageFeatureModelArrayList(packageFeatureModelArrayList);
                                            }
                                           dashboardCartAdapter.notifyDataSetChanged();
                                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                            Date d1 = null;
                                            Date d2 = null;

                                            d1 = format.parse(dateStart);
                                            d2 = format.parse(dateStop);

                                            long diff = d2.getTime() - d1.getTime();

                                            long diffSeconds = diff / 1000 % 60;
                                            long diffMinutes = diff / (60 * 1000) % 60;
                                            long diffHours = diff / (60 * 60 * 1000) % 24;
                                            long diffDays = diff / (24 * 60 * 60 * 1000);

                                            System.out.print(diffDays + " days, ");
                                            System.out.print(diffHours + " hours, ");
                                            System.out.print(diffMinutes + " minutes, ");
                                            System.out.print(diffSeconds + " seconds.");

                                            Long remainingdays = Long.parseLong(days) + diffDays;
                                            System.out.println("aaaaaaa days  " + diffDays + " " + remainingdays);
                                            tv_daysleft.setText("" + remainingdays + " " + mContext.getString(R.string.days_left));
                                            tv_title.setText(dataObject1.getJSONObject("packages").getString("title"));
                                            break;
                                        }
                                    }
                                } else {
//                                    showBottomSheet();
                                }
                            } else {
                                mCustomDialog.dismiss();
                            }
                        } catch (Exception e) {
                            mCustomDialog.dismiss();
                            e.printStackTrace();
                        } finally {
                            if (!isPlanExists) {
                                tv_title.setVisibility(View.GONE);
                                if (!isOpened){
                                    showBottomSheet();
                                    isOpened = true;
                                }

                                tv_daysleft.setText(getContext().getString(R.string.no_subscription_found));
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        tv_title.setVisibility(View.GONE);
                        tv_daysleft.setText("you dont have plan please subscribe ");
                        System.out.println("aaaaaaa error  " + error.getMessage());
                        mCustomDialog.dismiss();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                //  map.put(AUTH_TOKEN, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjExIiwidGltZSI6MTYyNTExNjY3OX0.hGHKRItuzSimfyH_awycj58enHX_xncMhJWEfQpIjrg");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void showBottomSheet() {
        View bottomSheetView = getLayoutInflater().inflate(R.layout.subscribe_bottomsheet, null);

        bottomSheetDialog = new BottomSheetDialog(requireContext(),R.style.BottomSheetDialogStyle);
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetDialog.show();
        bottomSheetDialog.setCancelable(false);
        TextView titleTextView = bottomSheetView.findViewById(R.id.titleTextView);
        Button subscribeButton = bottomSheetView.findViewById(R.id.subscribeButton);
        TextView close = bottomSheetView.findViewById(R.id.tv_close);

        // Set click listener for the "Subscribe Now" button
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requireActivity().finish();
            }
        });
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle button click event
                // Add your logic here
                // For example, navigate to the subscription activity
                bottomSheetDialog.cancel();
                isOpened = false;
                startActivity(new Intent(requireContext(), SubcriptionsActivity.class).putExtra("isPlanExists", "" + isPlanExists));

            }
        });
    }
    public String getDate() {
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year = "" + calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }
        if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        //   start_date.setText(day+"-"+month+"-"+year);
        //  end_date.setText(day+"-"+month+"-"+year);

        //  enddate=year+"-"+month+"-"+day;

        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
        int cHour = calander.get(Calendar.HOUR);
        int cMinute = calander.get(Calendar.MINUTE);
        int cSecond = calander.get(Calendar.SECOND);

        String startdate = year + "-" + month + "-" + day + " " + cHour + ":" + cMinute + ":" + cSecond;
        return startdate;
    }

}