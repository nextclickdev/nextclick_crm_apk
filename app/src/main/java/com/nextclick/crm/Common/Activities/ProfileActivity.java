package com.nextclick.crm.Common.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.MultiImagesAdapter;
import com.nextclick.crm.ShopNowModule.Models.MultiImageModel;
import com.nextclick.crm.Utilities.AppPermissions;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nextclick.crm.Config.Config.UPDATE_BANK_DETAILS;
import static com.nextclick.crm.Config.Config.UPDATE_IMAGES;
import static com.nextclick.crm.Config.Config.UPDATE_PROFILE_DETAILS;
import static com.nextclick.crm.Config.Config.UPDATE_SOCIAL_MEDIA_LINKS;
import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_MAIL;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_MOBILE;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;
import static com.nextclick.crm.Helpers.UIHelpers.Validations.isValidEmail;
import static com.nextclick.crm.Helpers.UIHelpers.Validations.isValidURL;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Double lattitude, longitude;
    LocationManager locationManager;

    private ImageView ivBackArrow;
    private Context mcontext;
    PreferenceManager preferenceManager;
    private String token;
    private EditText shop_location, latitude_et, longitude_et,
            unique_id, category, listing_name, email, description,
            address, landmark, ac_holder_name, bank_name, bank_branch,
            ac_number, ifsc_code, mobile_code, mobile_et, landline_code,
            landline_et, whatsapp_et, helpline_et,
            website_link, instagram_link, twitter_link, fb_link;
    private String shop_location_str, latitude_et_str, longitude_et_str, unique_id_str,
            category_str, listing_name_str, email_str, description_str, address_str,
            landmark_str, ac_holder_name_str, bank_name_str, bank_branch_str, ac_number_str,
            ifsc_code_str, mobile_code_str, mobile_et_str, landline_code_str, landline_et_str,
            whatsapp_et_str, helpline_et_str, fb_link_str, insta_link_str, twitter_link_str, website_str;
    private LinearLayout profile_layout, bank_details_layout, social_layout, banners_layout;
    private CardView profile_card, bank_details_card, social_card, banners_card;
    private TextView bank_text, profile_text, social_text, banners_text, add_image;
    private RadioButton open, close;
    private Button get_location, update_profile, update_bank_details, update_social_media, update_images;
    private RecyclerView multi_image_recycler;
    private ImageView cover_image;
    public static ArrayList<String> base64ImageArray = new ArrayList<>();
    public static ArrayList<MultiImageModel> multiImageModels = new ArrayList<>();

    private char selection_character = 'c';
    private static final int CAMERA_REQUEST = 8;
    private String userChoosenTask;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;
    public int flag = 0;


    LinearLayoutManager multiImagelinearLayoutManager;
    MultiImagesAdapter multiImagesAdapter;


    private int AVAILABILITY = -1;
    private final int OPEN = 1;
    private final int CLOSE = 0;
    private final int mobile = 1;
    private final int landline = 2;
    private final int whatsapp = 3;
    private final int helpline = 4;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);
        checkAppPermissions();
    }

    private void checkAppPermissions() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                init();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }

    private void init() {
        mcontext = this;
        ivBackArrow = findViewById(R.id.ivBackArrow);
        preferenceManager = new PreferenceManager(mcontext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        shop_location = findViewById(R.id.shop_location);
        latitude_et = findViewById(R.id.latitude_et);
        longitude_et = findViewById(R.id.longitude_et);
        unique_id = findViewById(R.id.unique_id);
        category = findViewById(R.id.category);
        listing_name = findViewById(R.id.listing_name);
        email = findViewById(R.id.email);
        description = findViewById(R.id.description);
        address = findViewById(R.id.address);
        landmark = findViewById(R.id.landmark);
        ac_holder_name = findViewById(R.id.ac_holder_name);
        bank_name = findViewById(R.id.bank_name);
        bank_branch = findViewById(R.id.bank_branch);
        ac_number = findViewById(R.id.ac_number);
        ifsc_code = findViewById(R.id.ifsc_code);
        open = findViewById(R.id.open);
        close = findViewById(R.id.close);
        mobile_code = findViewById(R.id.mobile_code);
        mobile_et = findViewById(R.id.mobile_et);
        landline_code = findViewById(R.id.landline_code);
        landline_et = findViewById(R.id.landline_et);
        whatsapp_et = findViewById(R.id.whatsapp_et);
        helpline_et = findViewById(R.id.helpline_et);
        website_link = findViewById(R.id.website_link);
        instagram_link = findViewById(R.id.instagram_link);
        twitter_link = findViewById(R.id.twitter_link);
        fb_link = findViewById(R.id.fb_link);

        profile_layout = findViewById(R.id.profile_layout);
        bank_details_layout = findViewById(R.id.bank_details_layout);
        social_layout = findViewById(R.id.social_layout);
        banners_layout = findViewById(R.id.banners_layout);

        profile_card = findViewById(R.id.profile_card);
        bank_details_card = findViewById(R.id.bank_details_card);
        social_card = findViewById(R.id.social_card);
        banners_card = findViewById(R.id.banners_card);
        profile_text = findViewById(R.id.profile_text);
        bank_text = findViewById(R.id.bank_text);
        social_text = findViewById(R.id.social_text);
        banners_text = findViewById(R.id.banners_text);
        add_image = findViewById(R.id.add_image);


        get_location = findViewById(R.id.get_location);
        update_profile = findViewById(R.id.update_profile);
        update_bank_details = findViewById(R.id.update_bank_details);
        update_social_media = findViewById(R.id.update_social_media);
        update_images = findViewById(R.id.update_images);

        multi_image_recycler = findViewById(R.id.multi_image_recycler);
        cover_image = findViewById(R.id.cover_image);

        getProfile();
        get_location.setOnClickListener(this);
        update_profile.setOnClickListener(this);
        update_bank_details.setOnClickListener(this);
        update_social_media.setOnClickListener(this);
        update_images.setOnClickListener(this);
        profile_card.setOnClickListener(this);
        bank_details_card.setOnClickListener(this);
        banners_card.setOnClickListener(this);
        social_card.setOnClickListener(this);
        add_image.setOnClickListener(this);
        cover_image.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);

        multiImagelinearLayoutManager = new LinearLayoutManager(mcontext, LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mcontext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };


    }

    private void getProfile() {

        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("p_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            listing_name.setText(dataObject.getString("name"));
                            email.setText(dataObject.getString("email"));
                            unique_id.setText(dataObject.getString("unique_id"));
                            description.setText(dataObject.getString("desc"));
                            address.setText(dataObject.getString("address"));
                            landmark.setText(dataObject.getString("landmark"));
                            try {
                                int availability = dataObject.getInt("availability");
                                if (availability == OPEN) {
                                    open.setChecked(true);
                                }
                                if (availability == CLOSE) {
                                    close.setChecked(true);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                shop_location.setText(dataObject.getJSONObject("location").getString("address"));
                                latitude_et.setText(dataObject.getJSONObject("location").getString("latitude"));
                                longitude_et.setText(dataObject.getJSONObject("location").getString("longitude"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                category.setText(dataObject.getJSONObject("category").getString("name"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                bank_name.setText(dataObject.getJSONObject("bank_details").getString("bank_name"));
                                bank_branch.setText(dataObject.getJSONObject("bank_details").getString("bank_branch"));
                                ac_holder_name.setText(dataObject.getJSONObject("bank_details").getString("ac_holder_name"));
                                ac_number.setText(dataObject.getJSONObject("bank_details").getString("ac_number"));
                                ifsc_code.setText(dataObject.getJSONObject("bank_details").getString("ifsc"));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONArray contactsArray = dataObject.getJSONArray("contacts");
                                if (contactsArray.length() > 0) {
                                    for (int i = 0; i < contactsArray.length(); i++) {
                                        JSONObject contactObject = contactsArray.getJSONObject(i);
                                        int type = contactObject.getInt("type");
                                        if (type == mobile) {
                                            mobile_code.setText(contactObject.getString("std_code"));
                                            mobile_et.setText(contactObject.getString("number"));
                                        }
                                        if (type == landline) {
                                            landline_code.setText(contactObject.getString("std_code"));
                                            landline_et.setText(contactObject.getString("number"));
                                        }
                                        if (type == whatsapp) {

                                            whatsapp_et.setText(contactObject.getString("number"));
                                        }
                                        if (type == helpline) {
                                            //mobile_code.setText("std_code");
                                            helpline_et.setText(contactObject.getString("number"));
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                JSONArray linksArray = dataObject.getJSONArray("links");
                                if (linksArray.length() > 0) {
                                    for (int i = 0; i < linksArray.length(); i++) {
                                        JSONObject linkObject = linksArray.getJSONObject(i);
                                        int type = linkObject.getInt("type");
                                        if (type == 1) {
                                            fb_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 2) {
                                            twitter_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 3) {
                                            instagram_link.setText(linkObject.getString("url"));
                                        }
                                        if (type == 4) {
                                            website_link.setText(linkObject.getString("url"));
                                        }
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                Picasso.get()
                                        .load(dataObject.getString("cover"))
                                        /*.networkPolicy(NetworkPolicy.NO_CACHE)
                                        .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                                        .into(cover_image);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONArray bannersArray = dataObject.getJSONArray("banners");
                                if (bannersArray.length() > 0) {
                                    for (int i = 0; i < bannersArray.length(); i++) {
                                        MultiImageModel multiImageModel = new MultiImageModel();
                                        JSONObject bannerObject = bannersArray.getJSONObject(i);
                                        multiImageModel.setId(bannerObject.getString("id"));
                                        multiImageModel.setImage(bannerObject.getString("image"));
                                        multiImageModels.add(multiImageModel);
                                    }
                                    multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                                    multiImagesAdapter.notifyDataSetChanged();
                                    multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                                    multi_image_recycler.setAdapter(multiImagesAdapter);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mcontext, "Sorry for inconvenience! Unable to fetch your data");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.get_location:
                LoadingDialog.loadDialog(mcontext);
                getLocation();
                break;

            case R.id.update_profile:
                if (isValidProfile()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_PROFILE_DETAILS;

                    Map<Object, Object> uploadMap = new HashMap<>();
                    uploadMap.put("name", listing_name_str);
                    uploadMap.put("address", address_str);
                    uploadMap.put("email", email_str);
                    uploadMap.put("landmark", landmark_str);
                    uploadMap.put("desc", description_str);
                    uploadMap.put("availability", AVAILABILITY + "".trim());
                    uploadMap.put("location_name", shop_location_str);
                    uploadMap.put("latitude", latitude_et_str);
                    uploadMap.put("longitude", longitude_et_str);

                    Map<String, String> mobilemap = new HashMap<>();
                    mobilemap.put("code", mobile_code_str);
                    mobilemap.put("number", mobile_et_str);
                    uploadMap.put("mobile", mobilemap);
                    Map<String, String> landlineMap = new HashMap<>();
                    landlineMap.put("code", landline_code_str);
                    landlineMap.put("number", landline_et_str);
                    uploadMap.put("landline", landlineMap);
                    Map<String, String> whatsappMap = new HashMap<>();
                    whatsappMap.put("code", "00");
                    whatsappMap.put("number", whatsapp_et_str);
                    uploadMap.put("whatsapp", whatsappMap);
                    Map<String, String> helplineMap = new HashMap<>();
                    helplineMap.put("code", "00");
                    helplineMap.put("number", helpline_et_str);
                    uploadMap.put("helpline", helplineMap);

                    data = new JSONObject(uploadMap).toString();
                    updateDetails(url, data);
                }

                break;

            case R.id.update_bank_details:
                if (isValidBankDetails()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_BANK_DETAILS;

                    Map<String, String> uploadMap = new HashMap<>();
                    uploadMap.put("bank_name", bank_name_str);
                    uploadMap.put("bank_branch", bank_branch_str);
                    uploadMap.put("ifsc", ifsc_code_str);
                    uploadMap.put("ac_holder_name", ac_holder_name_str);
                    uploadMap.put("ac_number", ac_number_str);
                    data = new JSONObject(uploadMap).toString();
                    updateDetails(url, data);
                }
                break;
            case R.id.update_social_media:
                if (isValidLinks()) {
                    String url = "";
                    String data = "";
                    url = UPDATE_SOCIAL_MEDIA_LINKS;

                    Map<String, String> uploadMap = new HashMap<>();
                    uploadMap.put("facebook", fb_link_str);
                    uploadMap.put("twitter", twitter_link_str);
                    uploadMap.put("instagram", insta_link_str);
                    uploadMap.put("website", website_str);

                    data = new JSONObject(uploadMap).toString();
                    updateDetails(url, data);

                }
                break;
            case R.id.cover_image:
                selection_character = 'c';
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;
            case R.id.update_images:

                String url = "";
                String data = "";
                url = UPDATE_IMAGES;

                Map<String, Object> uploadMap = new HashMap<>();
                uploadMap.put("cover_image", basse64Converter(cover_image));
                if (base64ImageArray.size() > 0) {
                    for (int i = 0; i < base64ImageArray.size(); i++) {
                        uploadMap.put("image", base64ImageArray.get(i));
                    }
                }

                data = new JSONObject(uploadMap).toString();
                updateDetails(url, data);


                break;

            case R.id.bank_details_card:
                profile_layout.setVisibility(View.GONE);
                banners_layout.setVisibility(View.GONE);
                social_layout.setVisibility(View.GONE);
                bank_details_layout.setVisibility(View.VISIBLE);
                profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                profile_text.setTextColor(getResources().getColor(R.color.black));
                banners_text.setTextColor(getResources().getColor(R.color.black));
                social_text.setTextColor(getResources().getColor(R.color.black));
                bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
                bank_text.setTextColor(getResources().getColor(R.color.white));
                break;
            case R.id.profile_card:
                profile_layout.setVisibility(View.VISIBLE);
                bank_details_layout.setVisibility(View.GONE);
                social_layout.setVisibility(View.GONE);
                banners_layout.setVisibility(View.GONE);
                profile_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
                profile_text.setTextColor(getResources().getColor(R.color.white));
                bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                bank_text.setTextColor(getResources().getColor(R.color.black));
                banners_text.setTextColor(getResources().getColor(R.color.black));
                social_text.setTextColor(getResources().getColor(R.color.black));
                break;

            case R.id.social_card:
                social_layout.setVisibility(View.VISIBLE);
                bank_details_layout.setVisibility(View.GONE);
                profile_layout.setVisibility(View.GONE);
                banners_layout.setVisibility(View.GONE);
                social_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
                social_text.setTextColor(getResources().getColor(R.color.white));
                bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                banners_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                bank_text.setTextColor(getResources().getColor(R.color.black));
                banners_text.setTextColor(getResources().getColor(R.color.black));
                profile_text.setTextColor(getResources().getColor(R.color.black));
                break;

            case R.id.banners_card:
                banners_layout.setVisibility(View.VISIBLE);
                bank_details_layout.setVisibility(View.GONE);
                social_layout.setVisibility(View.GONE);
                profile_layout.setVisibility(View.GONE);
                banners_card.setCardBackgroundColor(getResources().getColor(R.color.Iconblue));
                banners_text.setTextColor(getResources().getColor(R.color.white));
                bank_details_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                profile_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                social_card.setCardBackgroundColor(getResources().getColor(R.color.gray));
                bank_text.setTextColor(getResources().getColor(R.color.black));
                profile_text.setTextColor(getResources().getColor(R.color.black));
                social_text.setTextColor(getResources().getColor(R.color.black));
                break;

            case R.id.add_image:

                selection_character = 'b';
                boolean selecting1 = selectImage();
                if (selecting1) {

                }

                break;
        }
    }


    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }


    //Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items =
                {
                        "Choose from Library",
                        "Open Camera",
                        "Cancel"
                };

        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(mcontext);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data, SELECT_FILE);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            if (selection_character == 'c') {

                cover_image.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) cover_image.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            } else {
                MultiImageModel multiImageModel = new MultiImageModel();

                multiImageModel.setBitmap(photo);
                multiImageModels.add(multiImageModel);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();


                base64ImageArray.add(resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT)));

                multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                multiImagesAdapter.notifyDataSetChanged();
                multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                multi_image_recycler.setAdapter(multiImagesAdapter);
            }
            flag = 1;
        }


    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data, int SELECT_FILE) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(mcontext.getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);


        if (this.SELECT_FILE == SELECT_FILE) {

            if (selection_character == 'c') {
                cover_image.setImageBitmap(bm);
                Bitmap bitmap = ((BitmapDrawable) cover_image.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            } else {
                MultiImageModel multiImageModel = new MultiImageModel();

                multiImageModel.setBitmap(bm);
                multiImageModels.add(multiImageModel);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] byteArray = baos.toByteArray();
                base64ImageArray.add(resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT)));
                multiImagesAdapter = new MultiImagesAdapter(mcontext, multiImageModels);
                multiImagesAdapter.notifyDataSetChanged();
                multi_image_recycler.setLayoutManager(multiImagelinearLayoutManager);
                multi_image_recycler.setAdapter(multiImagesAdapter);
            }
        } else {

        }
        flag = 1;


    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void updateDetails(String url, final String data) {
        LoadingDialog.loadDialog(mcontext);
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mcontext, "Updated Successfully");

                                } else {
                                    UIMsgs.showToast(mcontext, Html.fromHtml(jsonObject.getString("data")) + "");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mcontext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mcontext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private boolean isValidBankDetails() {

        ac_holder_name_str = ac_holder_name.getText().toString().trim();
        ac_number_str = ac_number.getText().toString().trim();
        bank_branch_str = bank_name.getText().toString().trim();
        bank_name_str = bank_name.getText().toString();
        ifsc_code_str = ifsc_code.getText().toString().trim();

        int ac_holder_l = ac_holder_name_str.length();
        int ac_number_l = ac_number_str.length();
        int bank_branch_l = bank_branch_str.length();
        int bank_name_l = bank_name_str.length();
        int ifsc_code_l = ifsc_code_str.length();

        if (ac_holder_l == 0) {
            setEditTextErrorMethod(ac_holder_name, EMPTY_NOT_ALLOWED);
            return false;
        } else if (ac_holder_l < 3) {
            setEditTextErrorMethod(ac_holder_name, INVALID);
            return false;
        } else if (ac_number_l == 0) {
            setEditTextErrorMethod(ac_number, EMPTY_NOT_ALLOWED);
            return false;
        } else if (ac_number_l < 9) {
            setEditTextErrorMethod(ac_number, INVALID);
            return false;
        } else if (bank_branch_l == 0) {
            setEditTextErrorMethod(bank_branch, EMPTY_NOT_ALLOWED);
            return false;
        } else if (bank_branch_l < 4) {
            setEditTextErrorMethod(bank_branch, INVALID);
            return false;
        } else if (ifsc_code_l == 0) {
            setEditTextErrorMethod(ifsc_code, EMPTY_NOT_ALLOWED);
            return false;
        } else if (ifsc_code_l < 6) {
            setEditTextErrorMethod(ifsc_code, INVALID);
            return false;
        } else if (bank_name_l == 0) {
            setEditTextErrorMethod(bank_name, EMPTY_NOT_ALLOWED);
            return false;
        } else if (bank_name_l < 3) {
            setEditTextErrorMethod(bank_name, INVALID);
            return false;
        }

        return true;
    }


    private boolean isValidProfile() {
        int shop_loc_length = (shop_location_str = shop_location.getText().toString().trim()).length();
        int lat_length = (latitude_et_str = latitude_et.getText().toString().trim()).length();
        int long_length = (longitude_et_str = longitude_et.getText().toString().trim()).length();
        if (open.isChecked()) {
            AVAILABILITY = OPEN;
        } else if (close.isChecked()) {
            AVAILABILITY = CLOSE;
        }
        unique_id_str = unique_id.getText().toString().trim();
        category_str = category.getText().toString().trim();
        int list_name_length = (listing_name_str = listing_name.getText().toString().trim()).length();
        int email_lenth = (email_str = email.getText().toString().trim()).length();
        int mobile_code_length = (mobile_code_str = mobile_code.getText().toString().trim()).length();
        int mobile_length = (mobile_et_str = mobile_et.getText().toString().trim()).length();
        int ll_code_length = (landline_code_str = landline_code.getText().toString().trim()).length();
        int ll_et_str = (landline_et_str = landline_et.getText().toString().trim()).length();
        int whatsapp_length = (whatsapp_et_str = whatsapp_et.getText().toString().trim()).length();
        int help_length = (helpline_et_str = helpline_et.getText().toString().trim()).length();
        description_str = description.getText().toString().trim();
        int address_length = (address_str = address.getText().toString().trim()).length();
        int landmark_length = (landmark_str = landmark.getText().toString().trim()).length();

        if (shop_loc_length == 0) {
            setEditTextErrorMethod(shop_location, EMPTY_NOT_ALLOWED);
            return false;
        } else if (shop_loc_length < 3) {
            setEditTextErrorMethod(shop_location, INVALID);
            return false;
        } else if (lat_length < 2) {
            setEditTextErrorMethod(latitude_et, INVALID);
            return false;
        } else if (long_length < 2) {
            setEditTextErrorMethod(longitude_et, INVALID);
            return false;
        } else if (AVAILABILITY == -1) {
            UIMsgs.showToast(mcontext, "Please provide availability");
            return false;
        } else if (list_name_length == 0) {
            UIMsgs.showToast(mcontext, EMPTY_NOT_ALLOWED);
            return false;
        } else if (!isValidEmail(email_str)) {
            setEditTextErrorMethod(email, INVALID_MAIL);
            return false;
        } else if (mobile_code_length < 2) {
            setEditTextErrorMethod(mobile_code, INVALID);
            return false;
        } else if (mobile_length < 10) {
            setEditTextErrorMethod(mobile_et, INVALID_MOBILE);
            return false;
        } else if (whatsapp_length < 10) {
            setEditTextErrorMethod(whatsapp_et, INVALID);
            return false;
        } else if (address_length < 5) {
            setEditTextErrorMethod(address, "Please provide valid data");
            return false;
        } else if (landmark_length < 5) {
            setEditTextErrorMethod(landmark, "Please provide valid data");
            return false;
        }


        return true;
    }

    private boolean isValidLinks() {
        fb_link_str = fb_link.getText().toString().trim();
        insta_link_str = instagram_link.getText().toString().trim();
        twitter_link_str = twitter_link.getText().toString().trim();
        website_str = website_link.getText().toString().trim();

        if (fb_link_str.length() > 1) {
            if (!isValidURL(fb_link_str)) {
                setEditTextErrorMethod(fb_link, "INVALID LINK");
                return false;
            }
        } else if (insta_link_str.length() > 1) {
            if (!isValidURL(insta_link_str)) {
                setEditTextErrorMethod(instagram_link, "INVALID LINK");
                return false;
            }
        } else if (twitter_link_str.length() > 1) {
            if (!isValidURL(twitter_link_str)) {
                setEditTextErrorMethod(twitter_link, "INVALID LINK");
                return false;
            }
        } else if (website_str.length() > 1) {
            if (!isValidURL(website_str)) {
                setEditTextErrorMethod(website_link, "INVALID LINK");
                return false;
            }
        }

        return true;
    }

    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            String address = addresses.get(0).getAddressLine(0) + "";
            String[] arr = address.split(",");
            /*Toast.makeText(this, arr.length+"", Toast.LENGTH_SHORT).show();*/

            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            latitude_et.setText(lattitude + "");
            longitude_et.setText(longitude + "");
            shop_location.setText(address);
            LoadingDialog.dialog.dismiss();


        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        checkAppPermissions();
                    } else {
                        //checkAppPermissions();
                        init();
                        //UIMsgs.showToast(this, "Permission is denied for "+Manifest.permission.CAMERA);
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        checkAppPermissions();
                    } else {
                        //checkAppPermissions();
                        UIMsgs.showToast(this, "Permission is denied for "+Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}


