package com.nextclick.crm.Common;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.ShopByCatAdapter;
import com.nextclick.crm.Common.Models.ShopByCategory;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_U;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class ShopBycategeroesFragment extends Fragment {

    View view;
    private ArrayList<ShopByCategory> shopbycategorylist;
    private CustomDialog customDialog;
    private final Context mContext;
    private ShopByCatAdapter shopByCatAdapter;
    private RecyclerView recyclerView_shopBycategory;
    private PreferenceManager preferenceManager;

    public ShopBycategeroesFragment(Context mContext) {
        // Required empty public constructor
       this.mContext=mContext;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view =inflater.inflate(R.layout.fragment_shop_bycategeroes, container, false);
        recyclerView_shopBycategory=view.findViewById(R.id.recyclerView_shopBycategory);

        shopbycategorylist=new ArrayList<>();
        customDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);

        getshopbycategory();

        shopByCatAdapter=new ShopByCatAdapter(mContext,shopbycategorylist,ShopBycategeroesFragment.this);
        recyclerView_shopBycategory.setAdapter(shopByCatAdapter);

        return view;
    }
    public void getshopbycategory() {
        shopbycategorylist.clear();
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        if (response != null) {
                            Log.d("shopbycat_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  shopbycat "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataarray=jsonObject.getJSONArray("data");
                                    for (int i=0;i<dataarray.length();i++){
                                        JSONObject dataonject=dataarray.getJSONObject(i);
                                       ShopByCategory shopByCategory=new ShopByCategory();
                                       shopByCategory.setId(dataonject.getString("id"));
                                       shopByCategory.setCat_id(dataonject.getString("cat_id"));
                                       shopByCategory.setType(dataonject.getString("type"));
                                       shopByCategory.setName(dataonject.getString("name"));
                                       shopByCategory.setDesc(dataonject.getString("desc"));
                                       shopByCategory.setVendor_id(dataonject.getString("vendor_id"));
                                       shopByCategory.setStatus(dataonject.getString("status"));
                                       shopByCategory.setImage(dataonject.getString("image"));
                                        shopbycategorylist.add(shopByCategory);
                                    }
                                    shopByCatAdapter.setchnage(shopbycategorylist);
                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();

                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setcategory(ShopByCategory shopByCategory, String s) {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("id", ""+shopByCategory.getId());
        searchMap.put("status", ""+s);
        final String data = new JSONObject(searchMap).toString();

        shopbycategorylist.clear();
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_U,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        customDialog.dismiss();
                        if (response != null) {
                            Log.d("shopbycat_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  shopbycat "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                } else {

                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}