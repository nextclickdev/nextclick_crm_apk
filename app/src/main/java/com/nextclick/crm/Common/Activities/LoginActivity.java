package com.nextclick.crm.Common.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Validations;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.FCM;
import static com.nextclick.crm.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.crm.Config.Config.LOGIN;
import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.FCM_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_PASSWORD;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_USERID;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.Validations.isValidEmail;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText etUserId, etPassword;
    private TextView tvForgotPassword, tvRegister;
    private Button btnLogin;
    private String user_id_str, password_str;
    private Context mContext;
    PreferenceManager preferenceManager;
    Validations validations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new_new);
     //   getSupportActionBar().hide();
        init();
        initializeListeners();
    }

    private void init() {
        mContext = LoginActivity.this;
        mCustomDialog=new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);
        etUserId = findViewById(R.id.etUserId);
        etPassword = findViewById(R.id.etPassword);
        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);

     //   etUserId.setFilters(new InputFilter[] { filter });

       // Utility.setSpecialCharFilter(etUserId);
    }



    private void initializeListeners() {
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (isValid()) {
                    Map<String, String> dataMap = new HashMap<>();
                    dataMap.put("identity", user_id_str);
                    dataMap.put("password", password_str);
                    JSONObject json = new JSONObject(dataMap);
                    login(json);
                }
                break;
            case R.id.tvForgotPassword:
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_forgotpassword, null);

                final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
                final TextView ok = alertLayout.findViewById(R.id.ok);

                AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle(getString(R.string.forgot_password));
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String forgotmail = mailtext.getText().toString().trim();
                        if (forgotmail.length() == 0) {
                            mailtext.setError("Should Not Be Empty");
                            mailtext.requestFocus();
                        } else if (!isValidEmail(forgotmail)) {
                            mailtext.setError("Invalid");
                            mailtext.requestFocus();
                        } else {
                            dialog.dismiss();
                            forgotPassword(forgotmail);
                        }

                    }
                });
                break;
            case R.id.tvRegister:
                goToRegister();
                break;
            default:
                break;
        }
    }

    private void goToRegister() {
        Intent registerIntent = new Intent(this, RegisterVendorActivity.class);
        startActivity(registerIntent);
    }

    public void forgotPassword(final String forgotMail) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        Log.e("forgotPassword", "request: "+data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mCustomDialog.dismiss();
                Log.e("forgotPassword", "response: "+response);
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle(getString(R.string.forgot_password));
                            alert.setMessage(getString(R.string.reset_mail_sent));
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();

                        } else {
                            mCustomDialog.dismiss();
                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage(message);
                            alert.setCancelable(true);
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }

                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Unable to send");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mContext, OOPS);
                mCustomDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private boolean isValid() {
        boolean valid = true;

        user_id_str = etUserId.getText().toString().trim();
        password_str = etPassword.getText().toString().trim();
        int user_id_length = user_id_str.length(), password_length = password_str.length();
        if (user_id_length == 0) {
            setTextFieldErrorMethod(etUserId, EMPTY);
            valid = false;
        } else if (user_id_length < 5) {
            setTextFieldErrorMethod(etUserId, INVALID_USERID);
            valid = false;
        } else if (password_length == 0) {
            setTextFieldErrorMethod(etPassword, EMPTY);
            valid = false;
        } else if (password_length < 4) {
            setTextFieldErrorMethod(etPassword, INVALID_PASSWORD);
            valid = false;
        }


        return valid;

    }

    private void setTextFieldErrorMethod(TextInputEditText field, String err_msg) {
        field.setError(err_msg);
        field.requestFocus();
    }

    private CustomDialog mCustomDialog;
    private void login(JSONObject json) {
        mCustomDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("login_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String token = dataObject.getString("token");
                            preferenceManager.putString(USER_TOKEN, token);
                            try {

                                FirebaseInstanceId.getInstance().getInstanceId()
                                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                if (!task.isSuccessful()) {

                                                    return;
                                                }

                                                String firebasetoken = task.getResult().getToken();
                                                String msg = getString(R.string.fcm_token, firebasetoken);
                                                System.out.println("aaaaaaa token  "+firebasetoken);
                                                System.out.println("aaaaaaa msg  "+msg);
                                                grantFCMPermission(firebasetoken);
                                            }
                                        });
                               /* FirebaseInstanceId.getInstance().getInstanceId()
                                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                if (!task.isSuccessful()) {
                                                    return;
                                                }
                                                String token = task.getResult().getToken();
                                                String msg = getString(R.string.fcm_token, token);
                                                System.out.println("aaaaaaaa firebse token "+token);
                                                Log.d("TAG", token);
                                                grantFCMPermission(token);
                                            }
                                        });*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        mCustomDialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    mCustomDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    boolean isMixPanelSupport =true;
    private void grantFCMPermission(final String msg) {
        mCustomDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        System.out.println("aaaaaaaaa  daa  "+ data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.initializeMixPanel(getApplicationContext());
                                MyMixPanel.createUserID(etUserId.getText().toString().trim());
                            }
                            shareUserDetailsToMixPanel();

                            preferenceManager.putString(FCM_TOKEN, msg);
                            if(!UserData.isNewDesign || preferenceManager.getString("VendorName")!= null) {
                                startActivity(new Intent(mContext, ShopNowHomeActivity.class));//ServicesActivity
                                finish();
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaaaaa catch  "+e.getMessage());
                        mCustomDialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    mCustomDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                System.out.println("aaaaaaaaa  error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);

                System.out.println("aaaaaaa map "+ map);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void shareUserDetailsToMixPanel() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("p_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            MyMixPanel.sendUserLogin(dataObject.getString("name"),"",dataObject.getString("email"));
                            MyMixPanel.logEvent("Vendor Logged into the CRM application");

                            if (UserData.isNewDesign && preferenceManager.getString("VendorName") == null) {

                                preferenceManager.putString("VendorName", dataObject.getString("name"));
                                if(dataObject.has("constituency")){
                                   String constitueid=dataObject.getJSONObject("constituency").getString("id");
                                    preferenceManager.putString("VendorConstituency", constitueid);
                                }

                                if(dataObject.has("category"))
                                    preferenceManager.putString("VendorCategory", dataObject.getJSONObject("category").getString("id"));

                                Intent intent = new Intent(mContext, ShopNowHomeActivity.class);//ServicesActivity
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}