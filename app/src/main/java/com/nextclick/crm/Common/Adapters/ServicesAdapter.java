package com.nextclick.crm.Common.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Common.Activities.ServiceInfoActivity;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.LeadMangementModule.Activities.LeadsHomeActivity;
import com.nextclick.crm.OnDemandServicesModule.activities.OnDemandServicesHomeActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.MyPromotionsActivity;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsActivity;
import com.nextclick.crm.ShopNowModule.Activities.PromotionsListActivity;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.Profilefragment;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.ReportsFragment;
import com.nextclick.crm.ShopNowModule.Fragments.MyProductFragments.ProductsHomeFragment;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.apiCalls.VerifySubscriptionApiCall;
import com.nextclick.crm.doctorsModule.activities.DoctorsHomeActivity;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.orders.fragments.OrdersHistoryFragment;
import com.nextclick.crm.orders.fragments.WalletFragment;
import com.nextclick.crm.reports.ReportsActivity;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import static com.nextclick.crm.Constants.Constants.SERVICE;
import static com.nextclick.crm.Constants.Constants.SERVICE_SELECTED;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> implements Filterable, HttpReqResCallBack {

    private final Context context;
    private ServicesModel serviceModel;
    private final Transformation transformation;
    private final PreferenceManager preferenceManager;
    CustomDialog mCustomDialog;

    private final List<ServicesModel> data;
    private final List<ServicesModel> data_full;

    private String token = "";
    private ShopNowHomeActivity shopNowHomeActivity;
    private Fragment parentFragment;

    public ServicesAdapter(Context activity, List<ServicesModel> completData,int shopInIndex) {
        this.context = activity;
        this.selectedPosition=shopInIndex;
        this.data = completData;


        data_full = new ArrayList<>(completData);
        preferenceManager = new PreferenceManager(context);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    @NonNull
    @Override
    public ServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        if(UserData.isNewDesign)
            itemView =  LayoutInflater.from(context).inflate(R.layout.supporter_service_item_new, parent, false);
        else
            itemView =  LayoutInflater.from(context).inflate(R.layout.supporter_service_item, parent, false);
        return new ServicesAdapter.ViewHolder(itemView);
    }
    Integer selectedPosition =-1;
    Integer prevPosition =-1;
    @Override
    public void onBindViewHolder(@NonNull ServicesAdapter.ViewHolder holder, int position) {
        final ServicesModel serviceModel = data.get(position);


        if(selectedPosition == position) {
            holder.layout_bg.setBackgroundColor(Color.parseColor("#F26B35"));
            //scrollToPosition
        }
        else
            holder.layout_bg.setBackground(null);

        if(position==data.size()-1)
        {
            holder.separator.setVisibility(View.GONE);
        }
        else
            holder.separator.setVisibility(View.VISIBLE);

        String serviceName = serviceModel.getName();
        String serviceImageUrl = serviceModel.getImage();
        String serviceDescription = serviceModel.getDesc();

        holder.tvServiceName.setText(serviceName);
        holder.tvServiceDescription.setText(serviceDescription);
        if(serviceDescription!=null && !serviceDescription.isEmpty()){
            holder.tvServiceDescription.setVisibility(View.VISIBLE);
        }


       /* if (serviceImageUrl != null) {
            if (!serviceImageUrl.isEmpty()) {
                Picasso.get()
                        .load(serviceImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivServicePic);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .fit().centerCrop()
                        .into(holder.ivServicePic);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(transformation)
                    .fit().centerCrop()
                    .into(holder.ivServicePic);
        }
*/
        if (serviceName.equalsIgnoreCase("Ecommerce") || serviceName.equalsIgnoreCase("On Demand Services")) {

            holder.tvServiceName.setText( context.getString(R.string.od_services));
            holder.tvServiceDescription.setText( context.getString(R.string.services_booking));

            holder.layout_background.setBackgroundColor(Color.parseColor("#60B9CB"));
           // holder.cardView.setBackgroundResource(R.drawable.shopin_bg);
            holder.ivServicePic.setImageResource(R.drawable.shopin);
            /*holder.cardView.setCardBackgroundColor(Color.parseColor("#DD373F"));*/
            holder.tvServiceName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90FFFFFF"));
        } else if (serviceName.equalsIgnoreCase("Leads")) {

            holder.tvServiceName.setText( context.getString(R.string.Leads));
            holder.tvServiceDescription.setText( context.getString(R.string.Leads));

            holder.layout_background.setBackgroundColor(Color.parseColor("#168AAF"));
          //  holder.cardView.setBackgroundResource(R.drawable.leads_bg);
            holder.ivServicePic.setImageResource(R.drawable.leads);
            /*holder.cardView.setCardBackgroundColor(Color.parseColor("#EA5B79"));*/
            holder.tvServiceName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90FFFFFF"));
        } else if (serviceName.equalsIgnoreCase("My hospital")) {

            holder.tvServiceName.setText( context.getString(R.string.my_hospitals));
            holder.tvServiceDescription.setText( context.getString(R.string.my_hospitals));

            holder.layout_background.setBackgroundColor(Color.parseColor("#D98096"));
         //   holder.cardView.setBackgroundResource(R.drawable.myhospital_bg);
            holder.ivServicePic.setImageResource(R.drawable.myhospital);
           /* holder.cardView.setCardBackgroundColor(Color.parseColor("#3F48A7"));*/
            holder.tvServiceName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90FFFFFF"));
        } else if (serviceName.equalsIgnoreCase("Services Booking")|| serviceName.startsWith("Shop")) {

            holder.tvServiceName.setText( context.getString(R.string.shop_in));
            holder.tvServiceDescription.setText( context.getString(R.string.shop_in_description));

            holder.layout_background.setBackgroundColor(Color.parseColor("#E5ADAC"));
          //  holder.cardView.setBackgroundResource(R.drawable.ondemandservices_bg);
            holder.ivServicePic.setImageResource(R.drawable.ondemandservices);
            holder.tvServiceName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90FFFFFF"));
           /*holder.cardView.setCardBackgroundColor(Color.parseColor("#EFD610"));
            holder.tvServiceName.setTextColor(Color.parseColor("#000000"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90000000"));*/
        }else if (serviceName.startsWith("Promotions")) {

            holder.tvServiceName.setText( context.getString(R.string.PromotionsAds));
            holder.layout_background.setBackgroundColor(Color.parseColor("#C14DE6"));
            //  holder.cardView.setBackgroundResource(R.drawable.ondemandservices_bg);
            holder.ivServicePic.setImageResource(R.drawable.ic_promotion_tile);
            holder.tvServiceName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90FFFFFF"));
           /*holder.cardView.setCardBackgroundColor(Color.parseColor("#EFD610"));
            holder.tvServiceName.setTextColor(Color.parseColor("#000000"));
            holder.tvServiceDescription.setTextColor(Color.parseColor("#90000000"));*/
        }
        else {
            holder.cardView.setBackgroundResource(R.drawable.white_bg);
            holder.ivServicePic.setImageResource(R.drawable.no_img_circle);
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ServicesModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ServicesModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public void setParentActivity(ShopNowHomeActivity shopNowHomeActivity) {
        this.shopNowHomeActivity=shopNowHomeActivity;
    }
    public void setParentFragment(Fragment fragment) {

        this.parentFragment = fragment;

        if (parentFragment instanceof OrdersHistoryFragment)
            ((OrdersHistoryFragment) parentFragment).setViewHolder(this);
        if (parentFragment instanceof ProductsHomeFragment)
            ((ProductsHomeFragment) parentFragment).setViewHolder(this);
    }


    public void selectedIndex(int position) {
        prevPosition=selectedPosition;
        selectedPosition=position;
        notifyDataSetChanged();
    }

    public boolean isEqual(List<ServicesModel> source) {
        return data.equals(source);
    }

    public Integer getSelectedIndex() {
        return selectedPosition;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final CardView cardView;
        private final LinearLayout layout_bg;
        RelativeLayout layout_background;
        private final ImageView ivServicePic;
        private final ImageView ivInfo;
        private final TextView tvServiceName;
        private final TextView tvServiceDescription;
        View separator;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout_bg= itemView.findViewById(R.id.layout_bg);
            separator= itemView.findViewById(R.id.separator);
            ivInfo = itemView.findViewById(R.id.ivInfo);
            cardView = itemView.findViewById(R.id.cardView);
            ivServicePic = itemView.findViewById(R.id.ivServicePic);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceDescription = itemView.findViewById(R.id.tvServiceDescription);
            layout_background = itemView.findViewById(R.id.layout_background);
            ivInfo.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //if(selectedPosition == getLayoutPosition())
              //  return;
            int viewId = view.getId();
            serviceModel = data.get(getLayoutPosition());
            prevPosition=selectedPosition;
            selectedPosition=getLayoutPosition();
            if(serviceModel.getId().startsWith("170")) {
                if (parentFragment instanceof Profilefragment)
                    ((Profilefragment) parentFragment).doAction(serviceModel.getId());
            }
            else if(serviceModel.getId().startsWith("180")) {
                if (parentFragment instanceof ReportsFragment)
                    ((ReportsFragment) parentFragment).doAction(serviceModel.getId());
            }
            else if(serviceModel.getId().startsWith("190")) {
                if (parentFragment instanceof WalletFragment)
                    ((WalletFragment) parentFragment).doAction(serviceModel.getId());
            }
            else if(serviceModel.getId().startsWith("200")) {
                if (parentFragment instanceof OrdersHistoryFragment)
                    ((OrdersHistoryFragment) parentFragment).doAction(serviceModel.getId());
            }
            else if(serviceModel.getId().startsWith("210")) {
                if (parentFragment instanceof ProductsHomeFragment)
                    ((ProductsHomeFragment) parentFragment).doAction(serviceModel.getId());
            }
            else if (viewId == R.id.ivInfo) {
                String serviceName = serviceModel.getName();
                Intent servicesInfoIntent = new Intent(context, ServiceInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.service_name), serviceName);
                bundle.putString(context.getString(R.string.service_description), serviceModel.getDesc());
                bundle.putString(context.getString(R.string.service_languages), serviceModel.getLanguages());
                servicesInfoIntent.putExtras(bundle);
                context.startActivity(servicesInfoIntent);
            } else {
                //LoadingDialog.loadDialog(context);
                mCustomDialog=new CustomDialog(context);
                mCustomDialog.show();
                token ="Bearer " + new PreferenceManager(context).getString(USER_TOKEN);
                VerifySubscriptionApiCall.serviceCallToVerifySubscription(context, null, ServicesAdapter.this, serviceModel.getId(), token);
            }
            notifyDataSetChanged();
        }

    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if(mCustomDialog!=null)
            mCustomDialog.dismiss();
        if (requestType == Constants.SERVICE_CALL_TO_VERIFY_SUBSCRIPTION) {
            if (jsonResponse != null) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(jsonResponse);
                    boolean status = jsonObject.getBoolean("status");
                    // int http_code = jsonObject.getInt("http_code");
                    if (!status) {
                        Toast.makeText(context, "" + context.getString(R.string.no_services_available), Toast.LENGTH_SHORT).show();
                        selectedPosition = prevPosition;
                        notifyDataSetChanged();
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //   boolean status = verifySubscriptionResponse.getStatus();
                //    String message = verifySubscriptionResponse.getMessage();
                //  if (status) {
                String id = serviceModel.getId();
                Intent intent = null;
                if (id.equalsIgnoreCase("2")) {
                    intent = new Intent(context, ShopNowHomeActivity.class);
                    //  intent = new Intent(context, MainProductpage.class);
                } else if (id.equalsIgnoreCase("4")) {
                    intent = new Intent(context, LeadsHomeActivity.class);
                } else if (id.equalsIgnoreCase("8")) {
                    intent = new Intent(context, OnDemandServicesHomeActivity.class);
                } else if (id.equalsIgnoreCase("11")) {
                    intent = new Intent(context, DoctorsHomeActivity.class);
                    //intent = new Intent(context, DoctorBookingHomeActivity.class);
                } else if (id.equalsIgnoreCase("12")) {
                    //intent = new Intent(context, PromotionsActivity.class);
                    if (shopNowHomeActivity != null) {
                        shopNowHomeActivity.showPromotionFragment();
                        return;
                    } else
                        intent = new Intent(context, PromotionsActivity.class);
                } else if (id.equalsIgnoreCase("5678")) {
                    intent = new Intent(context, ReportsActivity.class);
                } else if (id.equalsIgnoreCase("1601")) {
                    intent = new Intent(context, PromotionsListActivity.class);
                    intent.putExtra("isMyPromotions", true);
                } else if (id.equalsIgnoreCase("1602")) {
                    intent = new Intent(context, MyPromotionsActivity.class);
                } else if (id.startsWith("170")) {
                    //profile
                    if (parentFragment instanceof Profilefragment)
                        ((Profilefragment) parentFragment).doAction(id);
                } else {
                    UIMsgs.showToast(context, "This service will be available soon");
                }
                if (intent != null) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.putExtra("Service_id", serviceModel.getId());
                    preferenceManager.putString(SERVICE, SERVICE_SELECTED);
                    context.startActivity(intent);
                }
                       /* } else {
                            Intent intent = new Intent(context, SubscriptionActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(context.getString(R.string.service_id), serviceModel.getId());
                            intent.putExtras(bundle);
                            context.startActivity(intent);
                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        }*/
                //  }
            }
            // LoadingDialog.dialog.dismiss();
        }
    }
}

