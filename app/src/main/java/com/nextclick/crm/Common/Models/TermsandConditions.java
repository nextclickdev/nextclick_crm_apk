package com.nextclick.crm.Common.Models;

public class TermsandConditions {

    String id;
    String app_details_id;
    String desc;
    String title;
    String page_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getApp_details_id() {
        return app_details_id;
    }

    public void setApp_details_id(String app_details_id) {
        this.app_details_id = app_details_id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPage_id() {
        return page_id;
    }

    public void setPage_id(String page_id) {
        this.page_id = page_id;
    }
}
