package com.nextclick.crm.Common.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;

public class ServiceInfoActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView ivClose;
    private TextView tvServiceName, tvServiceDescription, tvServiceLanguage, tvOK;

    private String serviceName = "";
    private String serviceLanguages = "";
    private String serviceDescription = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_service_info);
        getDataFromIntent();
        initializeUi();
        initializeListeners();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to service info activity");
        }
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.service_name)))
                serviceName = bundle.getString(getString(R.string.service_name));
            if (bundle.containsKey(getString(R.string.service_description)))
                serviceDescription = bundle.getString(getString(R.string.service_description));
            if (bundle.containsKey(getString(R.string.service_languages)))
                serviceLanguages = bundle.getString(getString(R.string.service_languages));
        }
    }

    private void initializeUi() {
        tvOK = findViewById(R.id.tvOK);
        ivClose = findViewById(R.id.ivClose);
        tvServiceName = findViewById(R.id.tvServiceName);
        tvServiceLanguage = findViewById(R.id.tvServiceLanguage);
        tvServiceDescription = findViewById(R.id.tvServiceDescription);

        tvServiceName.setText(serviceName);
        tvServiceLanguage.setText(serviceLanguages);
        tvServiceDescription.setText(serviceDescription);
    }

    private void initializeListeners() {
        tvOK.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.tvOK:
                finish();
                break;
            default:
                break;
        }
    }
}
