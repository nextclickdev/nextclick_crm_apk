package com.nextclick.crm.Common.Activities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.ServicesAdapter;
import com.nextclick.crm.Common.Models.ServicesModel;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.activities.NotificationActivity;
import com.nextclick.crm.activities.SupportListActivity;
import com.nextclick.crm.dialogs.Language_Dialog;
import com.nextclick.crm.dialogs.Logout_Dialog;
import com.nextclick.crm.subcriptions.SubcriptionsActivity;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

import static com.nextclick.crm.Config.Config.ACCEPT_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.REMOVEtoken;
import static com.nextclick.crm.Config.Config.VALIDATE_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Config.Config.VERIFY_TOKEN;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.FCM_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.Constants.VENDOR_ID;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class ServicesActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    private TextView tvError;
    private RecyclerView services_recycler;
    private ServicesAdapter servicesAdapter;
    private ImageView ivNotification, ivLogout,ivProfile;
    private PreferenceManager preferenceManager;

    private ArrayList<ServicesModel> serviceModelArrayList;

    private String token = "";
    private String vendor_id = "";
    private CustomDialog customDialog;
    private boolean checkterms=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
      //  getSupportActionBar().setTitle(getString(R.string.Nextclick));

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to services activity");
        }
       /* ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater inflator = (LayoutInflater) this .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.customimageview, null);

        actionBar.setCustomView(v);*/
       // setStatusBar();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }
    private void getValidateUser(String tcid, String title, String desc) {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("page_id", "2");
        termsmpa.put("tc_id", ""+tcid);
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VALIDATE_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {
                                customDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa validate user "+jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (!status) {

                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.activity_web, null);
                                    TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                    AlertDialog.Builder alert = new AlertDialog.Builder(ServicesActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle(title);
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    tv_terms_id.setText(desc);
                                    alert.setCancelable(false);
                                    alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                                accepttermsandconditions(tcid);
                                        }
                                    })
                                           /* .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                }
                                            })*/;
                                    AlertDialog dialog = alert.create();
                                    dialog.show();
                                }else {
                                    checkterms=true;
                                    System.out.println("aaaaaaaaa already accept");
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void gettermsandconditions() {
        customDialog.show();
        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id", "2");
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaa getterms  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject dataobject = jsonObject.getJSONObject("data");

                                        getValidateUser(dataobject.getString("id"),
                                                dataobject.getString("title"),dataobject.getString("desc"));
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void accepttermsandconditions(String id) {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("page_id", "2");
        termsmpa.put("tc_id", ""+id);
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("terms_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    checkterms=true;
                                    preferenceManager.putString("acceptterms", "1");
                                    System.out.println("aaaaaaaa terms conditions sucess ");
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void setStatusBar() {
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
    }

    private void initializeUi() {
        mContext = ServicesActivity.this;
        customDialog=new CustomDialog(mContext);
        tvError = findViewById(R.id.tvError);
        ivLogout = findViewById(R.id.ivLogout);
        ivProfile = findViewById(R.id.ivProfile);
        ivNotification = findViewById(R.id.ivNotification);
        preferenceManager = new PreferenceManager(mContext);
        services_recycler = findViewById(R.id.services_recycler);
        vendor_id = preferenceManager.getString(VENDOR_ID);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        System.out.println("aaaaaaa  token  "+token);
        try{
            if (!preferenceManager.getString("acceptterms").equalsIgnoreCase("1")){
                gettermsandconditions();
            }
        }catch (NullPointerException e){

        }

    }

    private void initializeListeners() {
        ivLogout.setOnClickListener(this);
        ivProfile.setOnClickListener(this);
        ivNotification.setOnClickListener(this);
    }

    private void prepareDetails() {
        if (vendor_id != null) {
            getServices();

        } else {
            verifyToken();
        }
    }

    private void verifyToken() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VERIFY_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                        JSONObject userDetailObject = jsonObject.getJSONObject("data").getJSONObject("token").getJSONObject("userdetail");
                                        vendor_id = userDetailObject.getString("id");
                                        preferenceManager.putString(VENDOR_ID, vendor_id);
                                        getServices();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    UIMsgs.showToast(mContext, "Unable to get services,please re-install");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void getServices() {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    listIsFull();
                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    JSONObject servicesObject = dataObject.getJSONObject("services");
                                    System.out.println("aaaaaaaaa  data  "+ dataObject);
                                    String is_admin=dataObject.getString("is_admin");
                                    String email=dataObject.getString("email");
                                    preferenceManager.putString("is_admin",is_admin);
                                    preferenceManager.putString("email",email);
                                    Iterator x = servicesObject.keys();
                                    JSONArray servicesjsonArray = new JSONArray();

                                    /*try {

                                        JSONArray jsonArray=dataObject.getJSONArray("contacts");
                                        for (int i=0;i<jsonArray.length();i++){
                                            JSONObject jsonObject1=jsonArray.getJSONObject(i);

                                            if (i==0){
                                                String number = jsonObject1.getString("number");
                                                preferenceManager.putString("number",number);
                                            }
                                        }
                                    }catch (NullPointerException e){

                                    }*/
                                    while (x.hasNext()) {
                                        String key = (String) x.next();
                                        servicesjsonArray.put(servicesObject.get(key));
                                    }

                                    if (servicesjsonArray.length() > 0) {
                                        serviceModelArrayList = new ArrayList<>();
                                        for (int i = 0; i < servicesjsonArray.length(); i++) {
                                            ServicesModel serviceModel = new ServicesModel();
                                            JSONObject serviceObject = servicesjsonArray.getJSONObject(i);
                                            Log.d("service_object", serviceObject + "");
                                            String id = serviceObject.getString("id");
                                            String name = serviceObject.getString("name");
                                            String description = serviceObject.getString("desc");
                                            String languages = serviceObject.getString("languages");

                                            String image = null;
                                            try {
                                                image = serviceObject.getString("image");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            serviceModel.setId(id);
                                            serviceModel.setName(name);
                                            serviceModel.setImage(image);
                                            serviceModel.setDesc(description);
                                            serviceModel.setLanguages(languages);
                                            serviceModelArrayList.add(serviceModel);
                                        }
                                        servicesAdapter = new ServicesAdapter(mContext, serviceModelArrayList,0);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {
                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }
                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }
                                        };
                                        //RecyclerView.LayoutManager layoutManager = new GridLayoutManager(ServicesActivity.this, 2);
                                        services_recycler.setLayoutManager(layoutManager);
                                        services_recycler.setAdapter(servicesAdapter);
                                    }
                                } else {
                                    listIsEmpty();
                                    UIMsgs.showToast(mContext, "Service not available");
                                }
                            } catch (Exception e) {
                                listIsEmpty();
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                UIMsgs.showToast(mContext, "Service not available");
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            listIsEmpty();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, OOPS);
                        listIsEmpty();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(mContext)
                .setTitle("Alert")
                .setMessage("Are you sure to exit..?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void runtimeEnableAutoInit() {
        // [START fcm_runtime_enable_auto_init]
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        // [END fcm_runtime_enable_auto_init]
    }

    public void deviceGroupUpstream() {
        // [START fcm_device_group_upstream]
        String to = "a_unique_key"; // the notification key
        AtomicInteger msgId = new AtomicInteger();
        FirebaseMessaging.getInstance().send(new RemoteMessage.Builder(to)
                .setMessageId(String.valueOf(msgId.get()))
                .addData("hello", "world")
                .build());
        // [END fcm_device_group_upstream]
    }

    // [START fcm_get_account]
    public String getAccount() {
        // This call requires the Android GET_ACCOUNTS permission
        Account[] accounts = AccountManager.get(this /* activity */).
                getAccountsByType("com.google");
        if (accounts.length == 0) {
            return null;
        }
        return accounts[0].name;
    }
    // [END fcm_get_account]

    public void getAuthToken() {
        // [START fcm_get_token]
        String accountName = getAccount();

        // Initialize the scope using the client ID you got from the Console.
        final String scope = "audience:server:client_id:"
                + "1262xxx48712-9qs6n32447mcj9dirtnkyrejt82saa52.apps.googleusercontent.com";

        String idToken = null;
        try {
            idToken = GoogleAuthUtil.getToken(this, accountName, scope);
        } catch (Exception e) {
        }
        // [END fcm_get_token]
    }

    // [START fcm_add_to_group]
    public String addToGroup(
            String senderId, String userEmail, String registrationId, String idToken)
            throws IOException, JSONException {
        URL url = new URL("https://fcm.googleapis.com/fcm/googlenotification");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);

        // HTTP request header
        con.setRequestProperty("project_id", senderId);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("POST");
        con.connect();

        // HTTP request
        JSONObject data = new JSONObject();
        data.put("operation", "add");
        data.put("notification_key_name", userEmail);
        data.put("registration_ids", new JSONArray(Collections.singletonList(registrationId)));
        data.put("id_token", idToken);

        OutputStream os = con.getOutputStream();
        os.write(data.toString().getBytes(StandardCharsets.UTF_8));
        os.close();

        // Read the response into a string
        InputStream is = con.getInputStream();
        String responseString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
        is.close();

        // Parse the JSON string and return the notification key
        JSONObject response = new JSONObject(responseString);
        return response.getString("notification_key");
    }
    // [END fcm_add_to_group]

    public void removeFromGroup(String userEmail, String registrationId, String idToken) throws JSONException {
        // [START fcm_remove_from_group]
        // HTTP request
        JSONObject data = new JSONObject();
        data.put("operation", "remove");
        data.put("notification_key_name", userEmail);
        data.put("registration_ids", new JSONArray(Collections.singletonList(registrationId)));
        data.put("id_token", idToken);
        // [END fcm_remove_from_group]
    }

    public void sendUpstream() {
        final String SENDER_ID = "YOUR_SENDER_ID";
        final int messageId = 0; // Increment for each
        // [START fcm_send_upstream]
        FirebaseMessaging fm = FirebaseMessaging.getInstance();
        fm.send(new RemoteMessage.Builder(SENDER_ID + "@fcm.googleapis.com")
                .setMessageId(Integer.toString(messageId))
                .addData("my_message", "Hello World")
                .addData("my_action", "SAY_HELLO")
                .build());
        // [END fcm_send_upstream]
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivNotification:
                goToNotifications();
                break;
            case R.id.ivLogout:
                Logout_Dialog logout_dialog=new Logout_Dialog(ServicesActivity.this,0);
                logout_dialog.showDialog();
               // showDeleteAlertDialog();
                break;
            case R.id.ivProfile:
                goToProfile();
                break;
            default:
                break;
        }
    }

    private void goToProfile() {
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        startActivity(profileIntent);
    }

    private void showDeleteAlertDialog() {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure do you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        preferenceManager.clear();
                        goToLogin();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void goToLogin() {

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor Logged out from the CRM application");
            MyMixPanel.logOutCurrentUser();
        }

        Intent loginIntent = new Intent(this, LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(loginIntent);
    }

    private void goToNotifications() {
        Intent notificationsIntent = new Intent(this, NotificationActivity.class);
        startActivity(notificationsIntent);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        services_recycler.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        services_recycler.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_profile:
                goToProfile();
                return true;
            case R.id.action_notification:
                goToNotifications();
                return true;
                case R.id.action_support:
                    startActivity(new Intent(ServicesActivity.this, SupportListActivity.class));
                return true;

                case R.id.action_logout:
                    Logout_Dialog logout_dialog=new Logout_Dialog(ServicesActivity.this,0);
                    logout_dialog.showDialog();
                return true;
            case R.id.action_subcription:
                Intent intent=new Intent(ServicesActivity.this, SubcriptionsActivity.class);
              //  intent.putExtra("mylist", serviceModelArrayList);
                startActivity(intent);
                return true;
            case R.id.action_change_language:
                Language_Dialog language_dialog=new Language_Dialog(ServicesActivity.this,ServicesActivity.this);
                language_dialog.showDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setLogout() {
        grantFCMPermission(preferenceManager.getString(FCM_TOKEN));

    }

    private void grantFCMPermission(final String msg) {
        customDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                REMOVEtoken, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        customDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa response  removetoken "+ jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.logEvent("Vendor Logged out from the CRM application");
                                MyMixPanel.logOutCurrentUser();
                            }
                            preferenceManager.clear();

                            Intent loginIntent = new Intent(mContext, LoginActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(loginIntent);
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               customDialog.dismiss();
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}