package com.nextclick.crm.Common.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Activities.RegisterVendorActivity;
import com.nextclick.crm.Common.Models.SubCategorySelection;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.CategoryObject;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.CATEGORY_LIST;

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.ViewHolder> {

    private final String categoryID;
    private final SubCategorySelection registerVendorActivity;
    private final ArrayList<CategoryObject> categories;

    public ArrayList<String> selectedCategoryIDs,selectedCategories;
    private final Context context;
    Integer selectedIndex=0;

    public SubCategoryAdapter(Context mContext, String categoryID, ArrayList<CategoryObject> subcategoriesList,
                              ArrayList<String> selectedCategoryIDs,ArrayList<String> selectedCategories,
                              SubCategorySelection registerVendorActivity) {
        this.context = mContext;
        this.categoryID=categoryID;
        this.categories = subcategoriesList;
        this.registerVendorActivity = registerVendorActivity;
        this.selectedCategoryIDs=selectedCategoryIDs;
        this.selectedCategories=selectedCategories;
    }

    @Override
    public SubCategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.sub_category_layout, parent, false);
        return new SubCategoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SubCategoryAdapter.ViewHolder holder,final int position) {

        holder.tv_category_name.setText(categories.get(position).getCatName());

        if(position==categories.size()-1)
        {
            holder.view_sep.setVisibility(View.GONE);
        }
        else
            holder.view_sep.setVisibility(View.VISIBLE);

        if (!categories.get(position).getCatImage().isEmpty()) {

            holder.img_category.setVisibility(View.GONE);
            Glide.with(context)
                    .load(categories.get(position).getCatImage())
                    .placeholder(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(holder.img_category);
        }

        if (selectedCategoryIDs.contains(categories.get(position).getCatID()))
        {
            holder.check_box.setChecked(true);
        }

        holder.check_box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!selectedCategoryIDs.contains(categories.get(position).getCatID())) {
                        selectedCategoryIDs.add(categories.get(position).getCatID());
                        selectedCategories.add(categories.get(position).getCatName());
                    }
                } else {
                    selectedCategoryIDs.remove(categories.get(position).getCatID());
                    selectedCategories.remove(categories.get(position).getCatName());
                }
                registerVendorActivity.setSelectedCategoryIDs(selectedCategoryIDs);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_category_name,tv_no_sub_categories;
        RelativeLayout layout_category;
        ImageView img_category;
        CheckBox check_box;
        View view_sep;

        public ViewHolder(View itemView) {
            super(itemView);
            layout_category= itemView.findViewById(R.id.layout_category);
            tv_category_name= itemView.findViewById(R.id.tv_category_name);
            img_category= itemView.findViewById(R.id.img_category);
            check_box= itemView.findViewById(R.id.check_box);
            view_sep=itemView.findViewById(R.id.view_sep);
        }
    }
}
