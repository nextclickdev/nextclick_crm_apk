package com.nextclick.crm.Common.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ShopCategoryObject;

import java.util.ArrayList;
import java.util.List;

public class CategoryModelAdapter extends RecyclerView.Adapter<CategoryModelAdapter.ViewHolder> {

    public List<ShopCategoryObject> item_list;

    public CategoryModelAdapter(List<ShopCategoryObject> listShops) {
        this.item_list=listShops;
    }


    @Override
    public CategoryModelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_item, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CategoryModelAdapter.ViewHolder holder, int position) {

        final int pos = position;

        holder.item_name.setText(item_list.get(position).getName());

        holder.chkSelected.setChecked(item_list.get(position).getSelected());

        holder.chkSelected.setTag(item_list.get(position));

        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ShopCategoryObject model = (ShopCategoryObject) cb.getTag();

                model.setSelected(cb.isChecked());
                item_list.get(pos).setSelected(cb.isChecked());
            }
        });


    }

    @Override
    public int getItemCount() {
        return item_list.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView item_name;
        public CheckBox chkSelected;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            item_name = itemLayoutView.findViewById(R.id.txt_Name);
            chkSelected = itemLayoutView.findViewById(R.id.chk_selected);
        }
    }

}
