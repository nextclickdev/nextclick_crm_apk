package com.nextclick.crm.Common.Models;

public class NotificationSoundsModel {

    boolean ischecked;
    int sounds;

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }

    public int getSounds() {
        return sounds;
    }

    public void setSounds(int sounds) {
        this.sounds = sounds;
    }
}
