package com.nextclick.crm.Common.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.MediaStore;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.CategoryAdapter;
import com.nextclick.crm.Common.Models.SubCategorySelection;
import com.nextclick.crm.Common.Models.TermsandConditions;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.IncomingSms;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.Helpers.UIHelpers.Validations;
import com.nextclick.crm.OnDemandsModule.Adapters.TimingListAdapter;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.Profilefragment;
import com.nextclick.crm.ShopNowModule.Models.CategoryObject;
import com.nextclick.crm.Utilities.ConnectionDetector;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.FindAddressInMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static com.nextclick.crm.Config.Config.ACCEPT_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.CATEGORY_LIST;
import static com.nextclick.crm.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.MainAppVendorRegistration;
import static com.nextclick.crm.Config.Config.States;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_MOBILE;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIValidations.EMPTY;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

public class RegisterVendorActivity extends AppCompatActivity implements LocationListener, SubCategorySelection {

    public String catName;
    Validations validations;

    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;
    Double lattitude, longitude;
    String currentAddress;
    String otp_str;
    Bundle bundle;
    Context mContext;
    SharedPreferences sharedPreferences;
    PreferenceManager preferenceManager;
    String sID,dID;
    int PLACE_PICKER_REQUEST = 1;
    int count = 0;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String coverPhotStringImageUrl1 = "";
    public static String bannerPhotStringImageUrl1 = "";
    public int flag = 0;
    char imageSelection;
    LocationManager locationManager;
    ConnectionDetector connectionDetector;
    EditText tv_gst_no,tv_labour_certificate_no,tv_fssai_no,tv_owner_name,pincode;

    ArrayList<String> openingTimeList = new ArrayList<String>();
    ArrayList<String> closingTimeList = new ArrayList<String>();
    TermsandConditions termsandConditions;

    ArrayList<String> selectedSubCategoryList = new ArrayList<String>();
    ArrayList<String> selectedAmenitiesList = new ArrayList<String>();
    ArrayList<String> selectedServicesList = new ArrayList<String>();
    ArrayList<String> holidayList = new ArrayList<>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<CategoryObject> ListCategoryObject=new ArrayList<>();
    ArrayList<String> timings = new ArrayList<>();
    Map<String, String> bannerBase64 = new HashMap<>();
    Map<String, String> servicesMap = new HashMap<>();
    Map<String, String> amenitiesMap = new HashMap<>();
    Map<String, Object> subCategorymap = new HashMap<>();
    Map<String, String> subCategorydatamap = new HashMap<>();
    Map<String, String> servicesdatamap = new HashMap<>();
    Map<String, String> amenitiesdatamap = new HashMap<>();
    ArrayList<String> amenitiesList = new ArrayList<>();
    ArrayList<String> servicesLits = new ArrayList<>();
    ArrayList<String> subcategoriesList = new ArrayList<>();

    ArrayList<String> servicesids = new ArrayList<>();
    ArrayList<String> selectedservicesid = new ArrayList<>();

    ArrayList<String> amenitiesids = new ArrayList<>();
    ArrayList<String> selectedamenitiesids = new ArrayList<>();

    ArrayList<String> subcategoryids = new ArrayList<>();
    ArrayList<String> selectedsubcategoryids = new ArrayList<>();


    EditText newListName,  complete_address,
            landmark, std_code, landline, mobile, altMobile, helpline, email,
            whatsapp, facebook_link, instagram_link, website_url,
            everyday_opening_time, everyday_closing_time, twitter_link,
            holiday_opening_time, holiday_closing_time, verify_mobile,
            otp_E_text, referal_id,country_code,country_code1;
    TextInputEditText business_pwd,re_business_pwd;
    TextView taptoopenmap, add_it,tv_sub_categories,location_et;
    LinearLayout fieldsLayout, servicelayout, timings_list_layout;
    ListView timing_listView;
    ScrollView scroll;
    Button /*next,*/ submit;
    CheckBox terms, sun, mon, tue, wed, thu, fri, sat;
    ImageView back, cover_photo, add_cover_photo, add_banner_photo;
    ImageView banner_photo;
    Spinner state_spinner, district_spinner, constituency_spinner, categories_spinner;
    private CheckBox check_sameasabove;

    ListView servicesListview, aminitiesListView, subCategoryListView, selectedSubcategoryListView;
    RelativeLayout layout_category;

    WebView webView;

    String newListName_str, location_et_str, pincode_str,
            complete_address_str, landmark_str, std_code_str,
            landline_str, mobile_str, helpline_str, email_str,
            whatsapp_str, facebook_link_str, instagram_link_str,
            website_url_str, twitter_link_str, constId,
            everyday_open_str, everyday_close_str, holiday_open_str,
            holiday_close_str, password_str,re_password_str;
    public String catId;
    public ArrayList<String> selectedCategoryIDs =new ArrayList<>();

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;


    ProgressDialog progressDialog;
    AlertDialog.Builder builder;

    String termshtml;


    //private String imagePath;
    private List<String> pathList;
    String imageEncoded;


    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 4; // by default length is 4


    //firebase related
    private static final String TAG = "PhoneAuthActivity";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private ViewGroup mPhoneNumberViews;
    private ViewGroup mSignedInViews;

    private TextView mStatusText;
    private TextView mDetailText;

    private EditText mPhoneNumberField;
    private EditText mVerificationField;

    private Button mStartButton;
    private Button mVerifyButton;
    private Button mResendButton;
    private Button mSignOutButton;
    private boolean isOtpVerified=false;
    private String verifiedmobile;


    private ArrayList permissionsToRequest;
    private final ArrayList permissionsRejected = new ArrayList();
    private final ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UIMsgs.showToast(mContext, "Check Your connection");
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private final IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(editTextone!=null) {
                editTextone.setText("" + otp.charAt(0));
                editTexttwo.setText("" + otp.charAt(1));
                editTextthree.setText("" + otp.charAt(2));
                editTextfour.setText("" + otp.charAt(3));
                editTextFive.setText("" + otp.charAt(4));
                editTextSix.setText("" + otp.charAt(5));
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UIMsgs.showToast(mContext, "Check Your connection");
        }
    }

    private final String blockCharacterSet = "@~#^|$%&*!";

    private final InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register_vendor_refactor);//layout_register_vendor
      //  getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        mContext = getApplicationContext();
        init();
        connectionDetector = new ConnectionDetector(mContext);

        validations = new Validations();
        preferenceManager = new PreferenceManager(mContext);

        servicelayout.setVisibility(View.GONE);

        //progressBar.setVisibility(View.VISIBLE);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching fields data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();


        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0)
            requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                    ALL_PERMISSIONS_RESULT);



        //Chechking Versions
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk <= android.os.Build.VERSION_CODES.M) {
            //submit.setBackgroundResource( R.drawable.border_button);
            //submit.setBackground(mContext.getColor(#F26B35));
        } else {
            //submit.setBackground(ContextCompat.getDrawable(context, R.drawable.ready));
            //add.setBackgroundResource( R.drawable.border_button);
            add_it.setBackgroundResource(R.drawable.border_button);
            //next.setBackgroundResource( R.drawable.border_button);
            //submit.setBackgroundResource(R.drawable.border_button);
        }


        getLocation();
        statesDataFetcher();
        categoryDataFetcher();
        builder = new AlertDialog.Builder(this);
        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettermsandconditions();


            }
        });

        bundle = getIntent().getExtras();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }


        add_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timings_list_layout.setVisibility(View.VISIBLE);

                String open = everyday_opening_time.getText().toString().trim();
                String close = everyday_closing_time.getText().toString().trim();
                if (open.length() > 0 && close.length() > 0) {

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    Date opentime = null;
                    Date closetime = null;
                    try {
                        opentime = sdf.parse(open);
                        closetime = sdf.parse(close);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long reqOpen = opentime.getTime();
                    long reqClose = closetime.getTime();

                    if (openingTimeList.size() > 0 && closingTimeList.size() > 0) {
                        Date checkertime = null;
                        try {
                            checkertime = sdf.parse(closingTimeList.get(closingTimeList.size() - 1));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long timechecker = checkertime.getTime();

                        if (reqOpen > timechecker) {

                            openingTimeList.add(open);
                            closingTimeList.add(close);

                        } else {
                            everyday_opening_time.requestFocus();
                            Toast.makeText(mContext, "please choose valid timings", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        openingTimeList.add(open);
                        closingTimeList.add(close);
                        everyday_opening_time.setText("".trim());
                        everyday_opening_time.setHint("00:00");
                        everyday_closing_time.setText("".trim());
                        everyday_closing_time.setHint("00:00");
                    }
                    ViewGroup.LayoutParams params;
                    params = timings_list_layout.getLayoutParams();
                    params.height = openingTimeList.size() * 135;
                    timings_list_layout.setLayoutParams(params);
                    TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                    timing_listView.setAdapter(timingListAdapter);
                    timingListAdapter.notifyDataSetChanged();

                }


            }
        });

        /*verify_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 10) {
                    //Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();

                    sendSMS(verify_mobile.getText().toString().trim(),generateRandomNumber());

                    //Toast.makeText(MainActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        timing_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openingTimeList.remove(position);
                closingTimeList.remove(position);
                TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                timing_listView.setAdapter(timingListAdapter);
                timingListAdapter.notifyDataSetChanged();
                //Toast.makeText(mContext, timings.size() + "", Toast.LENGTH_SHORT).show();
            }
        });


        everyday_opening_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RegisterVendorActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_opening_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        everyday_closing_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RegisterVendorActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_closing_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        holiday_opening_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RegisterVendorActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holiday_opening_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        holiday_closing_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RegisterVendorActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        holiday_closing_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                finish();

            }
        });

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0) {
                    sID = statesId.get(position - 1);
                    districtsId.clear();
                    districts.clear();
                    districtsDataFetcher(sID);
                }
                else
                {
                    sID="";
                    dID="";
                    constId="";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    ArrayAdapter ad = new ArrayAdapter(mContext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(ad);
                    constituency_spinner.setAdapter(ad);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        district_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0 && sID != null) {
                    dID = districtsId.get(position - 1);
                    constituencies.clear();
                    constituenciesId.clear();
                    constituenciesDataFetcher(sID, dID);
                }
                else {
                    dID="";
                    constId="";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    ArrayAdapter ad = new ArrayAdapter(mContext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    constituency_spinner.setAdapter(ad);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        add_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        add_banner_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'b';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
               /* Intent mIntent = new Intent(getApplicationContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 60);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 3);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
*/

            }
        });

       /* next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dataFetcherFromFields();
                if (validator()) {

                    servicesDataFetcher(catId);
                     //fieldsLayout.setVisibility(View.GONE);
                    servicelayout.setVisibility(View.VISIBLE);
                    servicelayout.requestFocus();
                }
            }
        });*/

        categories_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    servicesDataFetcher(categoryId.get(categories_spinner.getSelectedItemPosition() - 1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dataFetcherFromFields();
                boolean isValid=validator();
                if (isValid)
                {
                    if (terms.isChecked()) {
                        if(isOtpVerified &&  mobile_str.equals(verifiedmobile))
                        {
                            dataSender();
                        }
                        else
                            mailValidator(progressDialog);
                    } else {
                        UIMsgs.showToast(mContext, "Please accept terms and conditions");
                    }
                }
            }
        });

       /* timing_listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        aminitiesListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        servicesListview.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        subCategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        selectedSubcategoryListView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/
        /*Log.i("CHECK", subCategoryListView.getChildCount()+"");*/
        final ArrayAdapter<String> subcategoryadapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_dropdown_item_1line, selectedSubCategoryList);
        subCategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                String item = subcategoriesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedSubCategoryList.add(item);
                    selectedsubcategoryids.add(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                } else {
                    selectedSubCategoryList.remove(item);
                    selectedsubcategoryids.remove(subcategoryids.get(position));
                    subcategoryadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });
        layout_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCategoryPopup();
            }
        });

        aminitiesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = aminitiesListView.getCount();
                SparseBooleanArray checked = aminitiesListView.getCheckedItemPositions();

                String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedamenitiesids.add(amenitiesids.get(position));
                } else {
                    selectedamenitiesids.remove(amenitiesids.get(position));
                }

            }
        });

        servicesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int len = servicesListview.getCount();
                SparseBooleanArray checked = servicesListview.getCheckedItemPositions();

                //String item = amenitiesList.get(position);
                if (checked.get(position)) {

                    /* do whatever you want with the checked item */
                    selectedservicesid.add(servicesids.get(position));
                } else {
                    selectedservicesid.remove(servicesids.get(position));
                }


            }
        });
        /*selectedSubcategoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int len = subCategoryListView.getCount();
                SparseBooleanArray checked = subCategoryListView.getCheckedItemPositions();

                if (checked.get(position)) {
                    String item = timings.get(position);
                    *//* do whatever you want with the checked item *//*
                    selectedSubCategoryList.add(item);

                    subcategoryadapter.setDropDownViewResource(android.R.fieldsLayout.simple_dropdown_item_1line);
                    selectedSubcategoryListView.setAdapter(subcategoryadapter);
                }
            }
        });*/
    }

    InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {
            for (int i=start; i<end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i)) &&
                        !Character.isLowerCase(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };

    private void gettermsandconditions() {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id=", "1");
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS+"?page_id=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try{
                                        JSONArray dataarray = jsonObject.getJSONArray("data");
                                        JSONObject dataobject = dataarray.getJSONObject(0);
                                        termsandConditions=new TermsandConditions();
                                        termsandConditions.setId(dataobject.getString("id"));
                                        termsandConditions.setApp_details_id(dataobject.getString("app_details_id"));
                                        termsandConditions.setTitle(dataobject.getString("title"));
                                        termsandConditions.setPage_id(dataobject.getString("page_id"));
                                        termsandConditions.setDesc(dataobject.getString("desc"));
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                        webView = alertLayout.findViewById(R.id.ss_web);
                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                        AlertDialog.Builder alert = new AlertDialog.Builder(RegisterVendorActivity.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        WebSettings webSettings = webView.getSettings();
                                        Resources res = getResources();
                                        int fontSize = 10;
                                        webSettings.setDefaultFontSize(fontSize);
                                        webSettings.setJavaScriptEnabled(true);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        webView.loadDataWithBaseURL(null, dataobject.getString("desc"), "text/html; charset=UTF-8", "utf-8", null);
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                terms.setChecked(true);
                                                //   accepttermsandconditions(termsandConditions.getId());
                                            }
                                        })
                                           /* .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                    terms.setChecked(false);
                                                }
                                            })*/;
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }catch (Exception e){
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        termsandConditions=new TermsandConditions();
                                        termsandConditions.setId(dataobject.getString("id"));
                                        termsandConditions.setApp_details_id(dataobject.getString("app_details_id"));
                                        termsandConditions.setTitle(dataobject.getString("title"));
                                        termsandConditions.setPage_id(dataobject.getString("page_id"));
                                        termsandConditions.setDesc(dataobject.getString("desc"));
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);

                                        webView = alertLayout.findViewById(R.id.ss_web);
                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                        AlertDialog.Builder alert = new AlertDialog.Builder(RegisterVendorActivity.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        WebSettings webSettings = webView.getSettings();
                                        Resources res = getResources();
                                        int fontSize = 10;
                                        webSettings.setDefaultFontSize(fontSize);
                                        webSettings.setJavaScriptEnabled(true);
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        webView.loadDataWithBaseURL(null, dataobject.getString("desc"), "text/html; charset=UTF-8", "utf-8", null);
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                terms.setChecked(true);
                                                //   accepttermsandconditions(termsandConditions.getId());
                                            }
                                        })
                                           /* .setNegativeButton("Decline", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //  Action for 'NO' Button
                                                    terms.setChecked(false);
                                                }
                                            })*/;
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                showToast(mContext,"Terms and Conditions API failed :"+response);
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

           /* @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void accepttermsandconditions(String id) {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("page_id", "1");
        termsmpa.put("tc_id", ""+id);
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("terms_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    System.out.println("aaaaaaaa terms conditions sucess ");
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs, ArrayList<String> selectedCategoryNames) {
        this.selectedCategoryIDs=selectedCategoryIDs;
        StringBuilder str = new StringBuilder();
        for (String eachstring : selectedCategoryNames) {
            str.append(eachstring).append(",");
        }


        String finalString=str.toString();
        if(finalString.endsWith(","))
            finalString=finalString.substring(0,finalString.length()-1);


        String text = "<font color=#F26B35> Selected Category : </font> <font color=#333333>" + catName+ "</font><br>"+
                "\n <font color=#F26B35> Sub Categories : </font> <font color=#333333>" + finalString+ "</font>";

        tv_sub_categories.setText(Html.fromHtml(text));
    }

    @Override
    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs) {
        this.selectedCategoryIDs=selectedCategoryIDs;
    }

    @Override
    public void setCategory(CategoryObject category) {
        this.catId = category.getCatID();
        this.catName = category.getCatName();
    }

    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("No Connection..."); // Calls onProgressUpdate()
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
               /* progressDialog.dismiss();
                finalResult.setText(result);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                /*Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }


        @Override
        protected void onPreExecute() {
               /* progressDialog = ProgressDialog.show(MainActivity.this,
                        "ProgressDialog",
                        "Wait for "+time.getText().toString()+ " seconds");*/
           /* if(!connectionDetector.isConnectingToInternet()){
                resp="No Internet";
                Intent intent = new Intent(mContext,ConnectionAlert.class);
                startActivity(intent);
                finish();
            }
            else {
                resp = "Connected";
            }
*/
        }


        @Override
        protected void onProgressUpdate(String... text) {
            /*finalResult.setText(text[0]);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
               /* Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }


        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            currentAddress = addresses.get(0).getAddressLine(0) + "";
           // location_et.setText(currentAddress);//by default we are not showing current location here

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterVendorActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(RegisterVendorActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== Profilefragment.UPDATE_LOCATION) {
            updateLocation(data);
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (imageSelection == 'c') {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                cover_photo.setImageBitmap(photo);

                Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
            if (imageSelection == 'b') {
                bannerBase64.clear();
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                banner_photo.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);
            }
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }



       /* if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {

            this.pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");

                ArrayList<Bitmap> bitmapList = new ArrayList<>();
                for (int i = 0; i < pathList.size(); i++) {
                   *//* sb.append("Photo"+(i+1)+":"+pathList.get(i));
                    sb.append("\n");*//*
                    Bitmap bitmap1 = BitmapFactory.decodeFile(pathList.get(i));//assign your bitmap;
                   *//* Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;*//*


                    bitmapList.add(bitmap1);

                    Bitmap mergedImg = mergeMultiple(bitmapList);

                    banner_photo.setImageBitmap(mergedImg);

                }
                //tvResult.setText(sb.toString()); // here this is textview for sample use...

                onSelectFromGalleryResult(bitmapList);
                // Toast.makeText(mContext, sb.toString() + "", Toast.LENGTH_SHORT).show();
            }
        }*/

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            cover_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }
        if (imageSelection == 'b') {

            banner_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);
            bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);

            flag = 1;
        }

    }

    private void onSelectFromGalleryResult(ArrayList data) {


        bannerBase64.clear();

        for (int i = 0; i < data.size(); i++) {

            Bitmap bitmap = (Bitmap) data.get(i);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(i + "".trim(), bannerBitString);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);


            flag = 1;
        }


    }


    //Image Selection End


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == ALL_PERMISSIONS_RESULT) {
            for (Object perms : permissionsToRequest) {
                if (!hasPermission((String) perms)) {
                    permissionsRejected.add(perms);
                }
            }

            if (permissionsRejected.size() > 0) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                          /*  showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new
                                                        String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });*/
                        return;
                    }
                }

            }
        } else {
            int permissionLocation = ContextCompat.checkSelfPermission(RegisterVendorActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            }
        }
    }


    public void statesDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));
                        }
                        // progressBar.setVisibility(View.GONE);

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
               // map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("districts");
                        districts.add("Select");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            districtsId.add(districtObject.getString("id"));
                            districts.add(districtObject.getString("name"));
                        }

                    } else {
                        UIMsgs.showToast(getApplicationContext(), messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        progressDialog.show();
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("constituencies_resp", response);

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            constituenciesId.add(districtObject.getString("id"));
                            constituencies.add(districtObject.getString("name"));
                        }

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void categoryDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            CategoryObject c=new CategoryObject();
                            c.setCatID(dataObject.getString("id"));
                            c.setCatName(dataObject.getString("name"));
                            c.setCatImage(dataObject.getString("image"));
                            categoryId.add(c.getCatID());
                            categories.add(c.getCatName());
                            ListCategoryObject.add(c);
                        }

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, categories);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    categories_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();
                categoryDataFetcher();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    public void servicesDataFetcher(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = CATEGORY_LIST + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    type = !datatype.equalsIgnoreCase("java.lang.Boolean");
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            amenitiesdatamap.clear();
                            amenitiesList.clear();
                            amenitiesids.clear();
                            amenitiesMap.clear();
                            subcategoryids.clear();
                            subcategoriesList.clear();
                            subCategorydatamap.clear();
                            subCategorymap.clear();
                            servicesids.clear();
                            servicesLits.clear();
                            servicesdatamap.clear();
                            servicesMap.clear();

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            termshtml = jsonObject.getString("terms");
                            LinearLayout aminities_layout,
                                    services_layout, sub_categories_layout;
                            aminities_layout = findViewById(R.id.aminities_layout);
                            services_layout = findViewById(R.id.services_layout);
                            sub_categories_layout = findViewById(R.id.sub_categories_layout);

                            try {
                                JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                                if (amenitiesJsonArray.length() > 0) {
                                    for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                        JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                        String amenityname = ammenitiesArray.getString("name");
                                        String amenityid = ammenitiesArray.getString("id");
                                        String amenitycatId = ammenitiesArray.getString("cat_id");
                                        amenitiesdatamap.put(amenityid, amenityname);
                                        amenitiesList.add(amenityname);
                                        amenitiesids.add(amenityid);

                                    }

                                    ViewGroup.LayoutParams params;
                                    params = aminities_layout.getLayoutParams();
                                    params.height = amenitiesList.size() * 250;
                                    aminities_layout.setLayoutParams(params);

                                    aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                    ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                    amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                    aminitiesListView.setAdapter(amenitiesAdapter);
                                } else {
                                    UIMsgs.showToast(mContext, "Sorry...! No Amenities Found");
                                }
                            } catch (Exception ex) {
                            }

                            try {
                                JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                                if (subCategoryJsonArray.length() > 0) {
                                    for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                        JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                        String subcategoryname = subCategoryObject.getString("name");
                                        String subcategoryid = subCategoryObject.getString("id");
                                        String subcategorycatId = subCategoryObject.getString("cat_id");
                                        subCategorydatamap.put(subcategoryid, subcategoryname);
                                        subcategoriesList.add(subcategoryname);
                                        subcategoryids.add(subcategoryid);

                                    }

                                    ViewGroup.LayoutParams params;
                                    params = sub_categories_layout.getLayoutParams();
                                    /* if(subcategoriesList.size()<=3){*/
                                    params.height = subcategoriesList.size() * 300;
                               /* }else{
                                    params.height = subcategoriesList.size()*155;
                                }*/

                                    sub_categories_layout.setLayoutParams(params);
                                } else {
                                    UIMsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                                }
                            } catch (Exception ex) {
                            }


                            ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                    android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                            subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                            subCategoryListView.setAdapter(subcategoryAdapter);

                            try {
                                JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                                Iterator x = serviceJsonobject.keys();
                                JSONArray servicesjsonArray = new JSONArray();


                                while (x.hasNext()) {
                                    String key = (String) x.next();
                                    servicesjsonArray.put(serviceJsonobject.get(key));
                                }
                                //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                                if (servicesjsonArray.length() > 0) {
                                    for (int i = 0; i < servicesjsonArray.length(); i++) {
                                        JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                        String servicename = servicesData.getString("name");
                                        String serviceid = servicesData.getString("id");
                                        String servicecatId = servicesData.getString("cat_id");
                                        servicesdatamap.put(serviceid, servicename);
                                        servicesLits.add(servicename);
                                        servicesids.add(serviceid);

                                    }

                                    ViewGroup.LayoutParams serviceparams = services_layout.getLayoutParams();
                                    if (servicesLits.size() <= 2) {
                                        serviceparams.height = servicesLits.size() * 500;
                                    } else {
                                        serviceparams.height = servicesLits.size() * 250;
                                    }

                                    //Toast.makeText(mContext, serviceparams.height+"", Toast.LENGTH_SHORT).show();
                                    services_layout.setLayoutParams(serviceparams);
                                    ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                            android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                    servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                    serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                    servicesListview.setAdapter(serviceAdapter);
                                } else {
                                    UIMsgs.showToast(mContext, "Sorry...! No Services Found");
                                }
                            } catch (Exception ex) {
                            }

                        } else {
                            UIMsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UIMsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    //UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);
    }
    public void servicesDataFetcher_backup(final String categoryId) {
        //String tempCategories = Categories + "1";
        String tempCategories = CATEGORY_LIST + categoryId;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, tempCategories, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    Boolean type;
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    String datatype = jsonData.get("data").getClass().getName();
                    type = !datatype.equalsIgnoreCase("java.lang.Boolean");
                    if (type) {
                        if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                            amenitiesdatamap.clear();
                            amenitiesList.clear();
                            amenitiesids.clear();
                            amenitiesMap.clear();
                            subcategoryids.clear();
                            subcategoriesList.clear();
                            subCategorydatamap.clear();
                            subCategorymap.clear();
                            servicesids.clear();
                            servicesLits.clear();
                            servicesdatamap.clear();
                            servicesMap.clear();

                            String data = jsonData.getString("data");

                            JSONObject jsonObject = jsonData.getJSONObject("data");

                            termshtml = jsonObject.getString("terms");
                            LinearLayout aminities_layout,
                                    services_layout, sub_categories_layout;
                            aminities_layout = findViewById(R.id.aminities_layout);
                            services_layout = findViewById(R.id.services_layout);
                            sub_categories_layout = findViewById(R.id.sub_categories_layout);


                            JSONArray amenitiesJsonArray = jsonObject.getJSONArray("amenities");
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (amenitiesJsonArray.length() > 0) {

                                for (int i = 0; i < amenitiesJsonArray.length(); i++) {
                                    JSONObject ammenitiesArray = (JSONObject) amenitiesJsonArray.get(i);
                                    String amenityname = ammenitiesArray.getString("name");
                                    String amenityid = ammenitiesArray.getString("id");
                                    String amenitycatId = ammenitiesArray.getString("cat_id");
                                    amenitiesdatamap.put(amenityid, amenityname);
                                    amenitiesList.add(amenityname);
                                    amenitiesids.add(amenityid);

                                }

                                ViewGroup.LayoutParams params;
                                params = aminities_layout.getLayoutParams();
                                params.height = amenitiesList.size() * 250;
                                aminities_layout.setLayoutParams(params);

                                aminitiesListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                ArrayAdapter<String> amenitiesAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, amenitiesList);
                                amenitiesAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                aminitiesListView.setAdapter(amenitiesAdapter);
                            } else {
                                UIMsgs.showToast(mContext, "Sorry...! No Amenities Found");
                            }


                            JSONArray subCategoryJsonArray = jsonObject.getJSONArray("sub_categories");
                            if (subCategoryJsonArray.length() > 0) {
                                for (int i = 0; i < subCategoryJsonArray.length(); i++) {
                                    JSONObject subCategoryObject = (JSONObject) subCategoryJsonArray.get(i);
                                    String subcategoryname = subCategoryObject.getString("name");
                                    String subcategoryid = subCategoryObject.getString("id");
                                    String subcategorycatId = subCategoryObject.getString("cat_id");
                                    subCategorydatamap.put(subcategoryid, subcategoryname);
                                    subcategoriesList.add(subcategoryname);
                                    subcategoryids.add(subcategoryid);


                                }

                                ViewGroup.LayoutParams params;
                                params = sub_categories_layout.getLayoutParams();
                                /* if(subcategoriesList.size()<=3){*/
                                params.height = subcategoriesList.size() * 300;
                               /* }else{
                                    params.height = subcategoriesList.size()*155;
                                }*/

                                sub_categories_layout.setLayoutParams(params);

                                ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, subcategoriesList);
                                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                subCategoryListView.setAdapter(subcategoryAdapter);
                            } else {
                                UIMsgs.showToast(mContext, "Sorry...! No SubCategories Found");
                            }


                            JSONObject serviceJsonobject = jsonObject.getJSONObject("services");

                            Iterator x = serviceJsonobject.keys();
                            JSONArray servicesjsonArray = new JSONArray();


                            while (x.hasNext()) {
                                String key = (String) x.next();
                                servicesjsonArray.put(serviceJsonobject.get(key));
                            }
                            //Toast.makeText(mContext, String.valueOf(amenitiesJsonArray), Toast.LENGTH_SHORT).show();
                            if (servicesjsonArray.length() > 0) {
                                for (int i = 0; i < servicesjsonArray.length(); i++) {
                                    JSONObject servicesData = (JSONObject) servicesjsonArray.get(i);
                                    String servicename = servicesData.getString("name");
                                    String serviceid = servicesData.getString("id");
                                    String servicecatId = servicesData.getString("cat_id");
                                    servicesdatamap.put(serviceid, servicename);
                                    servicesLits.add(servicename);
                                    servicesids.add(serviceid);

                                }

                                ViewGroup.LayoutParams serviceparams = services_layout.getLayoutParams();
                                if (servicesLits.size() <= 2) {
                                    serviceparams.height = servicesLits.size() * 500;
                                } else {
                                    serviceparams.height = servicesLits.size() * 250;
                                }

                                //Toast.makeText(mContext, serviceparams.height+"", Toast.LENGTH_SHORT).show();
                                services_layout.setLayoutParams(serviceparams);
                                ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(getApplicationContext(),
                                        android.R.layout.simple_list_item_multiple_choice, servicesLits);
                                servicesListview.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
                                serviceAdapter.setDropDownViewResource(android.R.layout.simple_list_item_multiple_choice);
                                servicesListview.setAdapter(serviceAdapter);
                            } else {
                                UIMsgs.showToast(mContext, "Sorry...! No Services Found");
                            }


                        } else {
                            UIMsgs.showToast(mContext, messagae);
                            //Toast.makeText(ServicesAminitiesActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        UIMsgs.showToast(mContext, "Sorry..! No data found for the selected category");
                        fieldsLayout.setVisibility(View.VISIBLE);
                        servicelayout.setVisibility(View.GONE);
                    }


                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.fieldsLayout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.fieldsLayout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);*/


                } catch (JSONException e) {
                    e.printStackTrace();
                    //UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
              /*  ServicesAminitiesAdapter servicesAminitiesAdapter = new ServicesAminitiesAdapter(mContext,serviceAminitesList);
                amenitiesRecycle.setAdapter(servicesAminitiesAdapter);*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(RegisterVendorActivity.this, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);

                return map;
            }
        };

        /*stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));*/
        requestQueue.add(stringRequest);


    }


    public void dataFetcherFromFields() {

        newListName_str = newListName.getText().toString().trim();
        location_et_str = location_et.getText().toString().trim();
        pincode_str = pincode.getText().toString().trim();
        complete_address_str = complete_address.getText().toString();//.trim();
        landmark_str = landmark.getText().toString().trim();
        std_code_str = std_code.getText().toString().trim();
        landline_str = landline.getText().toString().trim();
        mobile_str = mobile.getText().toString().trim();
        helpline_str = helpline.getText().toString().trim();
        email_str = email.getText().toString().trim();
        password_str = business_pwd.getText().toString().trim();
        re_password_str = re_business_pwd.getText().toString().trim();
        whatsapp_str = whatsapp.getText().toString().trim();
        facebook_link_str = facebook_link.getText().toString().trim();
        instagram_link_str = instagram_link.getText().toString().trim();
        website_url_str = website_url.getText().toString().trim();
        twitter_link_str = twitter_link.getText().toString().trim();
        /*constId= constituenciesId.get(constituency_spinner.getSelectedItemPosition()-1);
        catId= categoryId.get(categories_spinner.getSelectedItemPosition()-1);*/
        newListName.setFilters(new InputFilter[]{filter});
        newListName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                //Char at newly inserted pos.
                char currentChar = s.charAt(start + before);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
       // newListName.setFilters(new InputFilter[] { filter });
        if (constituencies.size() > 0) {
            if (!constituency_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                constId = constituenciesId.get(constituency_spinner.getSelectedItemPosition() - 1);
            else
                constId = null;
        } else
            constId = null;
       /* if (categories.size() > 0) {
            if (!categories_spinner.getSelectedItem().toString().equalsIgnoreCase("select"))
                catId = categoryId.get(categories_spinner.getSelectedItemPosition() - 1);
            else
                catId = null;
        } else
            catId = null;*/


        everyday_open_str = everyday_opening_time.getText().toString().trim();
        everyday_close_str = everyday_closing_time.getText().toString().trim();
        holiday_open_str = holiday_opening_time.getText().toString().trim();
        holiday_close_str = holiday_closing_time.getText().toString().trim();


        if (sun.isChecked()) {
            holidayList.add("1");
        }
        if (mon.isChecked()) {
            holidayList.add("2");
        }
        if (tue.isChecked()) {
            holidayList.add("3");
        }
        if (wed.isChecked()) {
            holidayList.add("4");
        }
        if (thu.isChecked()) {
            holidayList.add("5");
        }
        if (fri.isChecked()) {
            holidayList.add("6");
        }
        if (sat.isChecked()) {
            holidayList.add("7");
        }

    }

    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private final View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }
                        editTexttwo.requestFocus();
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }
                        editTextthree.requestFocus();

                    } else if (text.length() == 0)
                        editTextone.requestFocus();
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }
                        editTextfour.requestFocus();
                    } else if (text.length() == 0)
                        editTexttwo.requestFocus();
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }
                        editTextFive.requestFocus();
                    } else if (text.length() == 0)
                        editTextthree.requestFocus();
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }
                        editTextSix.requestFocus();
                    } else if (text.length() == 0)
                        editTextfour.requestFocus();
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {

                        editTextFive.requestFocus();
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            Log.e(TAG, "afterTextChanged: hide keyboard");
                        } catch (Exception e) {
                            Log.e(TAG, "afterTextChanged: " + e);
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            Log.d(TAG, "beforeTextChanged: " + arg0);
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            Log.d(TAG, "onKey: keyCode = " + keyCode + ", event = " + event.toString());
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        else if (editTextSix.getText().toString().trim().length() == 1)
                            try {

                                //  ((BaseActivity) getActivity()).hideSoftKeyboard();
                            } catch (Exception e) {
                                Log.e(TAG, "afterTextChanged: " + e.toString());
                            }
                        break;
                }

            }
            return false;
        }
    }
    GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp,String mobilenumber) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);


        LinearLayout layout_root= dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels-20;
        layout_root.setLayoutParams(layoutParams);

        Button applyButton = dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);
         editTextone = dialog.findViewById(R.id.editTextone);
        editTexttwo = dialog.findViewById(R.id.editTexttwo);
         editTextthree = dialog.findViewById(R.id.editTextthree);
        editTextfour = dialog.findViewById(R.id.editTextfour);
        editTextFive = dialog.findViewById(R.id.editTextFive);
        editTextSix = dialog.findViewById(R.id.editTextSix);
        TextView tv_error = dialog.findViewById(R.id.tv_error);
        TextView tv_timer_resend = dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp(mobilenumber);
            }
        });
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }


         watcher1 = new GenericTextWatcher(editTextone);
         watcher2 = new GenericTextWatcher(editTexttwo);
         watcher3 = new GenericTextWatcher(editTextthree);
         watcher4 = new GenericTextWatcher(editTextfour);
         watcher5 = new GenericTextWatcher(editTextFive);
         watcher6 = new GenericTextWatcher(editTextSix);
        editTextone.addTextChangedListener(watcher1);
        editTextone.setOnKeyListener(watcher1);
        editTexttwo.addTextChangedListener(watcher2);
        editTexttwo.setOnKeyListener(watcher2);
        editTextthree.addTextChangedListener(watcher3);
        editTextthree.setOnKeyListener(watcher3);
        editTextfour.addTextChangedListener(watcher4);
        editTextfour.setOnKeyListener(watcher4);
        editTextFive.addTextChangedListener(watcher5);
        editTextFive.setOnKeyListener(watcher5);
        editTextSix.addTextChangedListener(watcher6);
        editTextSix.setOnKeyListener(watcher6);


        /*editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextone.length() == 1) {
                    editTextone.clearFocus();
                    editTexttwo.requestFocus();
                    editTexttwo.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTexttwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTexttwo.length() == 1) {
                    editTexttwo.clearFocus();
                    editTextthree.requestFocus();
                    editTextthree.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextthree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextthree.length() == 1) {
                    editTextthree.clearFocus();
                    editTextfour.requestFocus();
                    editTextfour.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextfour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextfour.length() == 1) {
                    editTextfour.clearFocus();
                    editTextFive.requestFocus();
                    editTextFive.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextFive.length() == 1) {
                    editTextFive.clearFocus();
                    editTextSix.requestFocus();
                    editTextSix.setCursorVisible(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });*/

        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    callVerifyOTPApi(otp,mobilenumber);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    private void sendOtp(final String mobile) {
        LoadingDialog.loadDialog(mContext);
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        fcmMap.put("passowrd", password_str);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+ json);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+ jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            showVerifyOTPDialog(null,mobile);
                        } else {
                            UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        LoadingDialog.dialog.dismiss();
                    }
                } else {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void callVerifyOTPApi(String otp,String mobilenumer) {

        LoadingDialog.loadDialog(mContext);

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobilenumer);
        fcmMap.put("otp", otp);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.VERIFY_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("VolleyResponse", "VERIFY_OTP: respnse " + response);

                    JSONObject jsonObject = new JSONObject(response);
                    boolean status = jsonObject.getBoolean("status");
                    if (!status) {
                        //  Toast.makeText(getApplicationContext(), "Your order has been failed with " + response, Toast.LENGTH_SHORT).show();
                        //  showErrorDialog("Incorrect otp");
                        //or
                        //show alert on same dialog

                        UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());

                        //show alert on same dialog
                        showVerifyOTPDialog(otp,mobilenumer);
                    } else {
                        isOtpVerified=true;
                        verifiedmobile=mobilenumer;
                        dataSender();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    LoadingDialog.dialog.dismiss();
                } finally {
                    LoadingDialog.dialog.dismiss();
                }
                //Toast.makeText(mContext, "responce "+response, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Log.d("fetch user error", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void dataSender() {

        progressDialog = new ProgressDialog(RegisterVendorActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while uploading the data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
       /* Map<String, String> socialdata = new HashMap<>();
        socialdata.put("1", facebook_link_str);
        socialdata.put("2", twitter_link_str);
        socialdata.put("3", instagram_link_str);
        socialdata.put("4", website_url_str);

        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> two = new HashMap<>();
        two.put("number", landline_str);
        two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", helpline_str);
        four.put("code", "");
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        Map<String, String> holiday = new HashMap<>();
        if (holidayList.size() > 0) {
            for (int i = 0; i < holidayList.size(); i++) {
                holiday.put(i + "".trim(), holidayList.get(i));
            }
        }

        Map<String, Object> timingsMap = new HashMap<>();

        for (int i = 0; i < openingTimeList.size(); i++) {

            Map<String, Object> insideTimings = new HashMap<>();
            insideTimings.put("start_time", openingTimeList.get(i));
            insideTimings.put("end_time", closingTimeList.get(i));

            timingsMap.put(i + "".trim(), insideTimings);

        }*/


        Map<String, Object> mainData = new HashMap<>();

      //  mainData.put("ref_id", referal_id.getText().toString().trim());
        mainData.put("name", newListName_str);//businessname
        mainData.put("category_id", catId);//business cat
        mainData.put("location_address", location_et_str);
        mainData.put("latitude", String.valueOf(lattitude));
        mainData.put("longitude", String.valueOf(longitude));
        mainData.put("address", complete_address_str);
        mainData.put("constituency_id", constId);
        mainData.put("pincode",pincode.getText().toString());
        mainData.put("email", email_str);
        mainData.put("primary_number", mobile_str);
        mainData.put("password", password_str);
        mainData.put("cover", coverPhotStringImageUrl1);

            try {
                if (bannerBase64 != null && bannerBase64.size() > 0) {
                    mainData.put("banner", bannerBase64);//bannerBase64.get("0")
                    /*
                    Map<String, Object> imageMap = new HashMap<>();

                    for (int i = 0; i < bannerBase64.size(); i++) {
                      //  mainData.put("banner", bannerBase64.get(i));
                        imageMap.put(i + "".trim(), bannerBase64.get(i));
                    }
                    mainData.put("banner", imageMap);*/
                }
            }
            catch (Exception ex){}
      //  mainData.put("landmark", landmark_str);
       // mainData.put("timings", timingsMap);
       /* if (holiday_open_str.equalsIgnoreCase("select") | holiday_close_str.equalsIgnoreCase("select")) {
            mainData.put("holiday_open_time", "");
            mainData.put("holiday_close_time", "");
        } else {
            mainData.put("holiday_open_time", holiday_open_str);
            mainData.put("holiday_close_time", holiday_close_str);
        }*/

        /*Map<String, Object> banner = new HashMap<>();
        banner.put("0", bannerBase64);
     //   mainData.put("banner", banner);*/

       // mainData.put("holidays", holiday);
        //
        //mainData.put("social", socialdata);

        //new fields
        Map<String, Map<String, String>> contact = new HashMap<>();
        Map<String, String> two = new HashMap<>();
       // two.put("number", landline_str);
        //two.put("code", std_code_str);
        Map<String, String> three = new HashMap<>();
        three.put("number", whatsapp_str);
        three.put("code", "+91");
        Map<String, String> four = new HashMap<>();
        four.put("number", altMobile.getText().toString());
        four.put("code",country_code1.getText().toString());

        Map<String, String> one = new HashMap<>();
        one.put("number", mobile_str);
        one.put("code", country_code.getText().toString());

        contact.put("1", one);
        contact.put("2", two);
        contact.put("3", three);
        contact.put("4", four);

        mainData.put("contacts", contact);
        StringBuilder str = new StringBuilder();
        Map<String, String> sub_cats = new HashMap<>();
        int index=0;
        for (String eachstring : selectedCategoryIDs) {
            //str.append(eachstring).append(",");
            sub_cats.put(""+index,eachstring);
            index++;
        }
        //String finalString=str.toString();
       // if(finalString.endsWith(","))
         //   finalString=finalString.substring(0,finalString.length()-1);

        mainData.put("sub_category_id",sub_cats);//sub catid
        mainData.put("owner_name",tv_owner_name.getText().toString());//optional
        mainData.put("gst_number",tv_gst_no.getText().toString());
        mainData.put("labour_certificate_number",tv_labour_certificate_no.getText().toString());
        mainData.put("fssai_number",tv_fssai_no.getText().toString());
      //  mainData.put("additional_mobile_number", altMobile.getText().toString());//optional- sending through contacts obj
       // mainData.put("whats_app_number", whatsapp_str);//whats app - sending through contacts obj
        //


        //JSONObject json = new JSONObject(mainData);
        //submission(json, progressDialog);
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            String jsonString = mapperObj.writeValueAsString(mainData);
            JSONObject json = new JSONObject(mainData);
          //  System.out.println("aaaaaaaa  json  "+json.toString());
            submission(jsonString, progressDialog);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Please select less size images", Toast.LENGTH_SHORT).show();
        }



    }

    public void dataClearance() {


        selectedSubCategoryList.clear();
        selectedAmenitiesList.clear();
        selectedServicesList.clear();
        holidayList.clear();

        coverPhotStringImageUrl1 = "".trim();
        banner_photo.setImageBitmap(null);
        cover_photo.setImageBitmap(null);
        bannerBase64.clear();
        servicesMap.clear();
        amenitiesMap.clear();
        subCategorymap.clear();
        subCategorydatamap.clear();
        servicesdatamap.clear();
        amenitiesdatamap.clear();
        amenitiesList.clear();
        servicesLits.clear();
        subcategoriesList.clear();

        servicesids.clear();
        selectedservicesid.clear();

        amenitiesids.clear();
        selectedamenitiesids.clear();

        subcategoryids.clear();
        selectedsubcategoryids.clear();

    }


    public void mailValidator(final ProgressDialog progressDialog) {



        /*RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EMAIL_VERIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {

                            try {

                                JSONObject object = new JSONObject(response);
                                String status = object.getString("status");
                                if (status.equalsIgnoreCase("true")) {

                                    Toast.makeText(CommonFiledsActivity.this, "OTP has been sent to your mail", Toast.LENGTH_SHORT).show();
                                    //clearCache();

                                    LayoutInflater inflater = getLayoutInflater();
                                    View alertLayout = inflater.inflate(R.layout.otp_layout, null);

                                    otp_E_text =alertLayout.findViewById(R.id.otp_next);

                                    AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
                                    alert.setIcon(R.mipmap.ic_launcher);
                                    alert.setTitle("OTP");
                                    // this is set the view from XML inside AlertDialog
                                    alert.setView(alertLayout);
                                    alert.setCancelable(false);


                                    alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            String otp_verify = otp_E_text.getText().toString().trim();
                                            if(otp_verify.length()==4){
                                                if(otp_str.equalsIgnoreCase(otp_verify)){
                                                    dataSender();

                                                }
                                                else{
                                                    Toast.makeText(mContext, "Invalid otp Please press submit button again", Toast.LENGTH_LONG).show();
                                                }
                                            }


                                        }
                                    });

                                    AlertDialog dialog = alert.create();
                                    dialog.show();




                                } else {
                                    progressDialog.dismiss();
                                    String message = object.getString("message");
                                    Toast.makeText(CommonFiledsActivity.this, Html.fromHtml(message), Toast.LENGTH_SHORT).show();

                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(CommonFiledsActivity.this, e + "", Toast.LENGTH_SHORT).show();

                            }
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(CommonFiledsActivity.this, *//*error +*//* "Please check the internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email_str);
                params.put("sub", SUBJECT);
                params.put("mes", "HI SIR/MA'AM your asset : " +newListName_str.toUpperCase()+"is going to be connected with NEXTCLICK.\n Your OTP is : "+otp_str+" \n Please provide this otp to our executive to complete the REGISTRATION");

                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);*/
        progressDialog.dismiss();
       // dataSender();
        sendOtp(mobile_str);
/*

        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.activity_phone_auth, null);


        mPhoneNumberViews = alertLayout.findViewById(R.id.phoneAuthFields);
        mSignedInViews = alertLayout.findViewById(R.id.signedInButtons);

        mStatusText = alertLayout.findViewById(R.id.status);
        mDetailText = alertLayout.findViewById(R.id.detail);

        mPhoneNumberField = alertLayout.findViewById(R.id.fieldPhoneNumber);
        mVerificationField = alertLayout.findViewById(R.id.fieldVerificationCode);

        mStartButton = alertLayout.findViewById(R.id.buttonStartVerification);
        mVerifyButton = alertLayout.findViewById(R.id.buttonVerifyPhone);
        mResendButton = alertLayout.findViewById(R.id.buttonResend);
        mSignOutButton = alertLayout.findViewById(R.id.signOutButton);

        // Assign click listeners
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validatePhoneNumber()) {
                    return;
                }
                //signOut();

                startPhoneNumberVerification(mPhoneNumberField.getText().toString());
            }
        });
       */
/* mVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
            }
        });*//*

        mResendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
            }
        });
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        */
/*@Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonStartVerification:
                    if (!validatePhoneNumber()) {
                        return;
                    }

                    startPhoneNumberVerification(mPhoneNumberField.getText().toString());
                    break;
                case R.id.buttonVerifyPhone:
                    String code = mVerificationField.getText().toString();
                    if (TextUtils.isEmpty(code)) {
                        mVerificationField.setError("Cannot be empty.");
                        return;
                    }

                    verifyPhoneNumberWithCode(mVerificationId, code);
                    break;
                case R.id.buttonResend:
                    resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
                    break;
                case R.id.signOutButton:
                    signOut();
                    break;
            }
        }*//*

        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };

        AlertDialog.Builder alert = new AlertDialog.Builder(CommonFiledsActivity.this);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("OTP");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        alert.setCancelable(false);


        alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                */
/*String otp_verify = otp_E_text.getText().toString().trim();
                if (otp_verify.length() == 4) {
                    if (otp_str.equalsIgnoreCase(otp_verify)) {
                        dataSender();

                    } else {
                        Toast.makeText(mContext, "Invalid otp Please press submit button again", Toast.LENGTH_LONG).show();
                    }
                }
*//*

                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                //dataSender();

            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
*/


    }

    public void submission(final String json, final ProgressDialog progressDialog) {

        final String data = json;
        Log.v("requesr register ",data);
        System.out.println("aaaaaaaaaaa  request regeter  "+ json);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MainAppVendorRegistration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+ jsonObject);
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                           // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status ) {

                                //signOut();//need to be check
                                /*Intent intent = new Intent(CommonFiledsActivity.this, FinishActivity.class);
                                startActivity(intent);
                                finish();*/
                               // UIMsgs.showAlert(mContext,);
                               // showAlert(mContext,""+message);
                            //    savetermsandconditions(progressDialog,message);
                                 UIMsgs.showToast(mContext, "Successfully Submitted");
                              //  onBackPressed();
                            //    dataClearance();
                                finish();
                            } else {
                                UIMsgs.showToast(mContext, message
                                );
                                Log.d("er msg", Html.fromHtml(message).toString());
                                progressDialog.dismiss();
                                servicelayout.setVisibility(View.GONE);
                                fieldsLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UIMsgs.showToast(mContext,e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  catchh "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UIMsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
                servicelayout.setVisibility(View.GONE);
                fieldsLayout.setVisibility(View.VISIBLE);
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void savetermsandconditions(ProgressDialog progressDialog,String mesg) {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+ jsonObject);
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status ) {

                                showAlert(mContext,""+mesg);
                            } else {
                                UIMsgs.showToast(mContext, message);
                                Log.d("er msg", Html.fromHtml(message).toString());
                                progressDialog.dismiss();
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UIMsgs.showToast(mContext,e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  error "+error.getMessage());
            //    UIMsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public boolean validator()
    {
        boolean validity = true;

        String[] emailafterdot=email_str.split(".");


        if (newListName_str.length() == 0) {
            newListName.setError("Please Enter Business Name");
            newListName.requestFocus();
            validity = false;
        }
        else if (catId == null) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if (selectedCategoryIDs == null || selectedCategoryIDs.size() == 0) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if(tv_owner_name.getText().toString()==null || tv_owner_name.getText().toString().isEmpty())
        {
            tv_owner_name.setError("Please enter Owner name");
            tv_owner_name.requestFocus();
            validity = false;
        }
        else if(tv_gst_no.getText().toString()==null || tv_gst_no.getText().toString().isEmpty())
        {
            tv_gst_no.setError(EMPTY_NOT_ALLOWED);
            tv_gst_no.requestFocus();
            validity = false;
        }
      /*  else if(!Validations.IsEmpty(tv_gst_no.getText()) && !Validations.isValidGST(tv_gst_no.getText().toString()))
        {
            tv_gst_no.setError(INVALID);
            tv_gst_no.requestFocus();
            validity = false;
        }*/
        else if(tv_labour_certificate_no.getText().toString()==null || tv_labour_certificate_no.getText().toString().isEmpty())
        {
            tv_labour_certificate_no.setError(EMPTY_NOT_ALLOWED);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }
        /*else if(!Validations.IsEmpty(tv_labour_certificate_no.getText()) && !Validations.isValidLabourCertificationNumber(tv_labour_certificate_no.getText().toString()))
        {
            tv_labour_certificate_no.setError(INVALID);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }*/
        else if(tv_fssai_no.getText().toString()==null || tv_fssai_no.getText().toString().isEmpty())
        {
            tv_fssai_no.setError(EMPTY_NOT_ALLOWED);
            tv_fssai_no.requestFocus();
            validity = false;
        }
    /*    else if(!Validations.IsEmpty(tv_fssai_no.getText()) && !Validations.isValidFSSAI(tv_fssai_no.getText().toString()))
        {
            tv_fssai_no.setError(INVALID);
            tv_fssai_no.requestFocus();
            validity = false;
        }*/
        else if (location_et_str.length() == 0) {
            location_et.setError("Wait untill location fetched");
            location_et.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() == 0) {
            complete_address.setError(EMPTY_NOT_ALLOWED);
            complete_address.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() < 6) {
            complete_address.setError("Please provide complete details");
            complete_address.requestFocus();
            validity = false;
        }
        else if (sID == null || sID.length() == 0) {
            showToast(mContext, "Please Select State");
            validity = false;
        }
        else if (dID == null || dID.length() == 0) {
            showToast(mContext, "Please Select District");
            validity = false;
        }
        else if (constId == null || constId.length() == 0) {
            showToast(mContext, "Please Select Constituency");
            constituency_spinner.requestFocus();
            validity = false;
        }
        else if (pincode_str.length() == 0) {
            pincode.setError(EMPTY);
            pincode.requestFocus();
            validity = false;
        }
        else if (pincode_str.startsWith("0") ||  pincode_str.length() < 6) {
            pincode.setError("PIN CODE is  Invalid");
            pincode.requestFocus();
            validity = false;
        }
        else if (validations.isBlank(email_str)) {
            email.setError(EMPTY);
            email.requestFocus();
            validity = false;
        }
        else if (validations.isValidEmail(email_str)) {
            email.setError(INVALID);
            email.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() == 0) {
            mobile.setError(EMPTY);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() < 10) {
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }
       /* else if (whatsapp_str.length() == 0) {
            whatsapp.setError(EMPTY);
            whatsapp.requestFocus();
            validity = false;
        }
        else if (whatsapp_str.length() < 10) {
            whatsapp.setError(INVALID);
            whatsapp.requestFocus();
            validity = false;
        }
        else if (whatsapp_str.startsWith("0") || (whatsapp_str.startsWith("1")) || whatsapp_str.startsWith("2")||
                whatsapp_str.startsWith("3")|| whatsapp_str.startsWith("4")||whatsapp_str.startsWith("5")){
            whatsapp.setError("Invalid Whatsapp Number");
            whatsapp.requestFocus();
            validity = false;
        }*/
        /*else if (landmark_str.length() == 0) {
            landmark.setError(EMPTY_NOT_ALLOWED);
            landmark.requestFocus();
            validity = false;
        }
        else if (landmark_str.length() <= 6) {
            landmark.setError("Please provide clear details");
            landmark.requestFocus();
            validity = false;
        }
        else if (openingTimeList.size() < 1) {
            Toast.makeText(mContext, "Please provide timings", Toast.LENGTH_SHORT).show();
            everyday_opening_time.requestFocus();
            validity = false;
        }
        else if (std_code_str.length() > 0 && std_code_str.length() < 3) {
            std_code.setError(INVALID);
            std_code.requestFocus();
            validity = false;
        }

        else if (landline_str.length() > 0 && landline_str.length() < 6) {
            landline.setError(INVALID);
            landline.requestFocus();
            validity = false;
        }*/
        else  if (password_str.length() == 0) {
            business_pwd.setError(EMPTY);
            business_pwd.requestFocus();
            validity = false;
        }
        else if (password_str.length() < 4) {
            business_pwd.setError("Minimum 4 Characters");
            business_pwd.requestFocus();
            validity = false;
        } else  if (re_password_str.length() == 0) {
            re_business_pwd.setError(EMPTY);
            re_business_pwd.requestFocus();
            validity = false;
        }
        else if (re_password_str.length() < 4) {
            re_business_pwd.setError("Minimum 4 Characters");
            re_business_pwd.requestFocus();
            validity = false;
        }else if (!password_str.equalsIgnoreCase(re_password_str)){
            re_business_pwd.setError("Password Not Match");
            re_business_pwd.requestFocus();
            validity = false;
        }

        /*else if (helpline_str.length() > 0 && helpline_str.length() < 10) {
            helpline.setError(INVALID);
            helpline.requestFocus();
            validity = false;
        }*/
        /*if (facebook_link_str.length() != 0) {

            if (facebook_link_str.length() < 15) {
                facebook_link.setError(INVALID);
                facebook_link.requestFocus();
                validity = false;
            }
        }

        if (instagram_link_str.length() != 0) {

            if (instagram_link_str.length() < 10) {
                instagram_link.setError(INVALID);
                instagram_link.requestFocus();
                validity = false;
            }
        }

        if (twitter_link_str.length() != 0) {

            if (twitter_link_str.length() < 10) {
                twitter_link.setError(INVALID);
                twitter_link.requestFocus();
                validity = false;
            }
        }

        if (website_url_str.length() != 0) {

            if (website_url_str.length() < 5) {
                website_url.setError(INVALID);
                website_url.requestFocus();
                validity = false;
            }
        }

        if (everyday_open_str.equalsIgnoreCase("select")) {
            everyday_opening_time.requestFocus();
            UIMsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }

        if (everyday_close_str.equalsIgnoreCase("select")) {
            everyday_closing_time.requestFocus();
            UIMsgs.showToast(mContext, TIMEINAVLID);
            validity = false;
        }*/


        else if (coverPhotStringImageUrl1 == null || coverPhotStringImageUrl1.length() <= 0) {
            showToast(mContext, "Please Select Cover Image");
            cover_photo.requestFocus();
            validity = false;
        }
        else if (bannerBase64 == null || bannerBase64.size() <= 0) {//2
            showToast(mContext, "Please Select Banner Image");
            banner_photo.requestFocus();
            validity = false;
        }


        /* constId, catId,  */


        return validity;
    }

    private void showToast(Context mContext, String message) {
        Toasty.error(mContext, message, Toast.LENGTH_SHORT).show();
       // UIMsgs.showToast(mContext, message);
    }

    private Bitmap mergeMultiple(ArrayList<Bitmap> parts) {
        final int IMAGE_MAX_SIZE = 1200000;
        int value = parts.size();
        int height = parts.get(0).getHeight();
        int width = parts.get(0).getWidth();

        double y = Math.sqrt(IMAGE_MAX_SIZE
                / (((double) width) / height));
        double x = (y / height) * width;
        Bitmap result = Bitmap.createScaledBitmap(parts.get(0), (int) x, (int) y, true);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        for (int i = 0; i < parts.size(); i++) {
            canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 4), parts.get(i).getHeight() * (i / 4), paint);
        }
        return result;
    }

    public void init() {

        otp_str = generateRandomNumber();
        newListName = findViewById(R.id.new_list_name);
        location_et =findViewById(R.id.location);
        RelativeLayout location_layout= findViewById(R.id.location_layout);
        location_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                ArrayList<String> locArray=new ArrayList<>();
                locArray.add(currentAddress);
                locArray.add(""+lattitude);
                locArray.add(""+longitude);
                String locationString = gson.toJson(locArray);
                Intent intent=new Intent(getApplicationContext(), FindAddressInMap.class);
                intent.putExtra("location",locationString);
                startActivityForResult(intent,Profilefragment.UPDATE_LOCATION);
            }
        });
        pincode = findViewById(R.id.pincode);
        complete_address = findViewById(R.id.complet_address);
        landmark = findViewById(R.id.landmark);
        std_code = findViewById(R.id.std_code);
        country_code = findViewById(R.id.country_code);
        country_code1 = findViewById(R.id.country_code1);
        landline = findViewById(R.id.landline);
        mobile = findViewById(R.id.mobile);
        altMobile = findViewById(R.id.mobile1);
        helpline = findViewById(R.id.helpLine);
        email = findViewById(R.id.email);
        whatsapp = findViewById(R.id.whatsapp);
        facebook_link = findViewById(R.id.fb_link);
        instagram_link = findViewById(R.id.instagram_link);
        website_url = findViewById(R.id.website_url);
        twitter_link = findViewById(R.id.twitter_link);
        verify_mobile = findViewById(R.id.verifymobile);
        //otp = (EditText) findViewById(R.id.otp);
        check_sameasabove=findViewById(R.id.check_sameasabove);

        taptoopenmap = findViewById(R.id.map_opener);
        add_it = findViewById(R.id.add_one_more);
        tv_sub_categories=findViewById(R.id.tv_sub_categories);
        fieldsLayout = findViewById(R.id.fieldsLayout);
        servicelayout = findViewById(R.id.service_n_aminities_layout);
        scroll = findViewById(R.id.common_fields_scroll);
        timings_list_layout = findViewById(R.id.timings_list_layout);
        timing_listView = findViewById(R.id.timings_list);

        //next = (Button) findViewById(R.id.next);
        submit = findViewById(R.id.submit);

        terms = findViewById(R.id.terms);
        sun = findViewById(R.id.check_sunday);
        mon = findViewById(R.id.check_monday);
        tue = findViewById(R.id.check_tuesday);
        wed = findViewById(R.id.check_wednesday);
        thu = findViewById(R.id.check_thursday);
        fri = findViewById(R.id.check_friday);
        sat = findViewById(R.id.check_saturday);

        back = findViewById(R.id.back);
        cover_photo = findViewById(R.id.cover_photo);
        add_cover_photo = findViewById(R.id.add_cover_photo);
        banner_photo = findViewById(R.id.banner_photo);

        add_banner_photo = findViewById(R.id.add_banner_photo);


        state_spinner = findViewById(R.id.state_spinner);
        district_spinner = findViewById(R.id.district_spinner);
        constituency_spinner = findViewById(R.id.constituency_spinner);
        categories_spinner = findViewById(R.id.category_spinner);
        everyday_opening_time = findViewById(R.id.everyday_opening_spinner);
        everyday_closing_time = findViewById(R.id.everyday_closing_spinner);
        holiday_opening_time = findViewById(R.id.holiday_opening_spinner);
        holiday_closing_time = findViewById(R.id.holiday_closing_spinner);
        referal_id = findViewById(R.id.referal_id);
        business_pwd = findViewById(R.id.business_pwd);
        re_business_pwd = findViewById(R.id.re_business_pwd);

        complete_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                complete_address.setSelection(complete_address.getText().length());
                return false;
            }
        });

        states.add("Select");
        categories.add("Select");

        layout_category=findViewById(R.id.layout_category);

        servicesListview = findViewById(R.id.services_recycle);
        aminitiesListView = findViewById(R.id.amenities_recycle);
        subCategoryListView = findViewById(R.id.subCategory);
        subCategoryListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        selectedSubcategoryListView = findViewById(R.id.selected_subcategory);

        newListName.setFilters(new InputFilter[] { filter });


        tv_gst_no=findViewById(R.id.tv_gst_no);
        tv_labour_certificate_no=findViewById(R.id.tv_labour_certificate_no);
        tv_fssai_no=findViewById(R.id.tv_fssai_no);
        tv_owner_name=findViewById(R.id.tv_owner_name);
        pincode=findViewById(R.id.pincode);

        check_sameasabove.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    whatsapp.setText(mobile.getText().toString().trim());
                }else {
                    whatsapp.setText("");
                }
            }
        });
    }

    public String generateRandomNumber() {
        int randomNumber;

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        //randomNumber = Integer.parseInt(s);

        return s;
    }


    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                           /* FirebaseUser user = task.getResult().getUser();
                            // [START_EXCLUDE]
                            updateUI(STATE_SIGNIN_SUCCESS, user);*/
                            // [END_EXCLUDE]
                            dataSender();


                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationField.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            //updateUI(STATE_SIGNIN_FAILED);
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
   /* private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }*/
/*
    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }*/

    /* private void updateUI(int uiState, FirebaseUser user) {
         updateUI(uiState, user, null);
     }

     private void updateUI(int uiState, PhoneAuthCredential cred) {
         updateUI(uiState, null, cred);
     }

     *//*private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                // Initialized state, show only the phone number field and start button
                enableViews(mStartButton, mPhoneNumberField);
                disableViews(mVerifyButton, mResendButton, mVerificationField);
                mDetailText.setText(null);
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, the
                enableViews(mVerifyButton, mResendButton, mPhoneNumberField, mVerificationField);
                disableViews(mStartButton);
                mDetailText.setText(R.string.status_code_sent);
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                enableViews(mStartButton, mVerifyButton, mResendButton, mPhoneNumberField,
                        mVerificationField);
                mDetailText.setText(R.string.status_verification_failed);
                break;
            case STATE_VERIFY_SUCCESS:
                // Verification has succeeded, proceed to firebase sign in
                disableViews(mStartButton, mVerifyButton, mResendButton, mPhoneNumberField,
                        mVerificationField);
                mDetailText.setText(R.string.status_verification_succeeded);

                // Set the verification text based on the credential
                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        mVerificationField.setText(cred.getSmsCode());
                    } else {
                        mVerificationField.setText(R.string.instant_validation);
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                mDetailText.setText(R.string.status_sign_in_failed);
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }

        if (user == null) {
            // Signed out
            mPhoneNumberViews.setVisibility(View.VISIBLE);
            mSignedInViews.setVisibility(View.GONE);

            mStatusText.setText(R.string.signed_out);
        } else {
            // Signed in
            mPhoneNumberViews.setVisibility(View.GONE);
            mSignedInViews.setVisibility(View.VISIBLE);

            enableViews(mPhoneNumberField, mVerificationField);
            mPhoneNumberField.setText(null);
            mVerificationField.setText(null);

            mStatusText.setText(R.string.signed_in);
            mDetailText.setText(getString(R.string.firebase_status_fmt, user.getUid()));
        }
    }*/
    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }
    public  void showAlert(Context context,String message) {
        android.app.AlertDialog.Builder alertDialogBuilder = new
                android.app.AlertDialog.Builder(RegisterVendorActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showCategoryPopup() {
        //categories

        if(categoryId!=null && categoryId.size()>1)
        {
         //   BottomCategoryFragment fragment = new BottomCategoryFragment(mContext, categoryId,categories,selectedCategoryIDs,this);
          //  fragment.show(getSupportFragmentManager(), fragment.getTag());



            ArrayList<String> selectedCategories=new ArrayList<>();

            Dialog dialog = new Dialog(this);

            View contentView = View.inflate(mContext, R.layout.select_categories, null);
            //context = contentView.getContext();
            dialog.setContentView(contentView);

            LinearLayout layout_root= dialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels;
            layout_root.setLayoutParams(layoutParams);


            Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = dialog.findViewById(R.id.closeButton);

            RecyclerView recyclerView_categories=dialog.findViewById(R.id.recyclerView_categories);
            RecyclerView recyclerView_subcategories=dialog.findViewById(R.id.recyclerView_subcategories);

            TextView tv_no_categories=dialog.findViewById(R.id.tv_no_categories);
            TextView tv_no_sub_categories=dialog.findViewById(R.id.tv_no_sub_categories);
            TextView tv_error=dialog.findViewById(R.id.tv_error);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_categories.setLayoutManager(linearLayoutManager);

            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(mContext);
            linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_subcategories.setLayoutManager(linearLayoutManager1);


            CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, ListCategoryObject,
                    recyclerView_subcategories,
                    tv_no_categories,tv_no_sub_categories,selectedCategoryIDs,selectedCategories,
                    this);
            recyclerView_categories.setAdapter(categoryAdapter);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedCategories.size() > 0) {
                        dialog.dismiss();
                        setSelectedCategoryIDs(selectedCategoryIDs,selectedCategories);
                    }
                    else {
                        tv_error.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tv_error.setVisibility(View.GONE);
                            }
                        },2000);
                    }
                }
            });

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            window.setGravity(Gravity.BOTTOM);

             dialog.show();

        }

    }


    public void updateLocation(Intent intent)
    {
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3)
            {
                location_et.setText(locArray.get(0));
                location_et.setError(null);
                lattitude = Double.parseDouble(locArray.get(1));
                longitude = Double.parseDouble(locArray.get(2));
            }
        }catch (Exception ex) {
            getLocation();
        }
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
}
