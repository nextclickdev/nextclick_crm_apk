package com.nextclick.crm.Common.Models;

import com.nextclick.crm.ShopNowModule.Models.CategoryObject;

import java.util.ArrayList;

public interface SubCategorySelection {
    void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs);
    void setCategory(CategoryObject category);
}
