package com.nextclick.crm.Common.Adapters;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Common.Models.ShopByCategory;
import com.nextclick.crm.Common.ShopBycategeroesFragment;
import com.nextclick.crm.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class ShopByCatAdapter extends RecyclerView.Adapter<ShopByCatAdapter.ViewHolder> {

    private final Context context;
    ArrayList<ShopByCategory> shopbycategorylist;
    ShopBycategeroesFragment shopBycategeroesFragment;

    public ShopByCatAdapter(Context mContext, ArrayList<ShopByCategory> shopbycategorylist, ShopBycategeroesFragment shopBycategeroesFragment) {
        this.context=mContext;
        this.shopbycategorylist=shopbycategorylist;
        this.shopBycategeroesFragment=shopBycategeroesFragment;
    }

    @NonNull
    @Override
    public ShopByCatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView =  LayoutInflater.from(context).inflate(R.layout.shopbycatlist, parent, false);
         return new ShopByCatAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopByCatAdapter.ViewHolder holder, int position) {
        final ShopByCategory shopByCategory = shopbycategorylist.get(position);
        holder.tv_name.setText(""+shopByCategory.getName());

        Glide.with(context)
                .load(shopByCategory.getImage())
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(holder.img_shop);

        holder.cSwitch.setTextOn("Active");
        holder.cSwitch.setTextOff("In-Active");

        if (shopByCategory.getStatus().equalsIgnoreCase("1")){
            holder.cSwitch.setChecked(true);
            holder.cSwitch.setText("Active");
        }else {
            holder.cSwitch.setChecked(false);
            holder.cSwitch.setText("In-Active");
        }
        holder.cSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    shopBycategeroesFragment.setcategory(shopByCategory,"2");
                }else {
                    shopBycategeroesFragment.setcategory(shopByCategory,"1");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return shopbycategorylist.size();
    }

    public void setchnage(ArrayList<ShopByCategory> shopbycategorylist) {
        this.shopbycategorylist=shopbycategorylist;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img_shop;
        private final TextView tv_name;
        @SuppressLint("UseSwitchCompatOrMaterialCode")
        private final Switch cSwitch;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_shop= itemView.findViewById(R.id.img_shop);
            tv_name=itemView.findViewById(R.id.tv_name);
            cSwitch=itemView.findViewById(R.id.cSwitch);
        }

    }

}

