package com.nextclick.crm.Common.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.dialogs.Language_Dialog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.VENDOR_PROFILE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;


public class SplashScreen extends AppCompatActivity {
    Context mContext;
    PreferenceManager preferenceManager;
    String TAG = "Splash Screen";
    ImageView splashimage, lion_image;
    private final boolean isLoginMandatory = false;

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash_screen);

        lion_image = findViewById(R.id.lion_image);


        Language_Dialog language_dialog = new Language_Dialog(SplashScreen.this, SplashScreen.this);
        language_dialog.LoadLanguage();

        /*try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }*/
        //  getSupportActionBar().hide();
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);

        Glide.with(mContext)
                .load(getResources().getDrawable(R.drawable.lion))
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(lion_image);

        try {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.POST_NOTIFICATIONS}, 101);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 101);

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (preferenceManager.getString(USER_TOKEN) != null && preferenceManager.getString("approval_status") != null) {
                        Log.d("tok", preferenceManager.getString(USER_TOKEN));

                        if (MyMixPanel.isMixPanelSupport) {
                            MyMixPanel.initializeMixPanel(getApplicationContext());
                            shareUserDetailsToMixPanel();
                        }

                        if (!UserData.isNewDesign || preferenceManager.getString("VendorName") != null) {
                            Intent intent = new Intent(mContext, ShopNowHomeActivity.class);//ServicesActivity
                            // Intent intent = new Intent(mContext, ImageActivity.class);//ServicesActivity
                            startActivity(intent);
                            finish();
                        }
                    } else {
                        preferenceManager.putString(USER_TOKEN, null);
                        Intent intent;
                        if (isLoginMandatory)
                            intent = new Intent(mContext, LoginActivity.class);
                        else
                            intent = new Intent(mContext, ShopNowHomeActivity.class);//ServicesActivity

                        startActivity(intent);
                        finish();
                    }
                }
            }, 2000);
        }
    }


    private void shareUserDetailsToMixPanel() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("p_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            MyMixPanel.sendUserLogin(dataObject.getString("name"), "", dataObject.getString("email"));

                            MyMixPanel.logEvent("Vendor opened the application");
                            String is_admin = dataObject.getString("is_admin");
                            String email = dataObject.getString("email");
                            preferenceManager.putString("is_admin", is_admin);
                            preferenceManager.putString("email", email);

                            if (UserData.isNewDesign && preferenceManager.getString("VendorName") == null) {

                                preferenceManager.putString("VendorName", dataObject.getString("name"));
                                Intent intent = new Intent(mContext, ShopNowHomeActivity.class);//ServicesActivity
                                startActivity(intent);
                                finish();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
