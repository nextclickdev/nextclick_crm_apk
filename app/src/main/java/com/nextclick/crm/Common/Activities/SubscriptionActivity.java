package com.nextclick.crm.Common.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.apiCalls.SubscriptionDetailsApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.getSubscriptionsResponse.GetSubscriptionsResponse;
import com.google.gson.Gson;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class SubscriptionActivity extends AppCompatActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;

    private String token = "";
    private String serviceId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_subscription);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();

        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to subscription activity");
        }
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.service_id))) {
                serviceId = bundle.getString(getString(R.string.service_id));
            }
        }
    }

    private void initializeUi() {
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(this);
        token = "Bearer " +new PreferenceManager(this).getString(USER_TOKEN);
        SubscriptionDetailsApiCall.serviceCallForSubscriptionDetails(this, null, null, serviceId, token);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_SUBSCRIPTIONS) {
            if (jsonResponse != null) {
                GetSubscriptionsResponse getSubscriptionsResponse = new Gson().fromJson(jsonResponse, GetSubscriptionsResponse.class);
                if (getSubscriptionsResponse != null) {
                    getSubscriptionsResponse.getStatus();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }
}
