package com.nextclick.crm.authentication;

import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.view.KeyEvent.KEYCODE_DEL;
import static android.view.View.GONE;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.IncomingSms;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nextclick.crm.Config.Config.FCM;
import static com.nextclick.crm.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.crm.Config.Config.LOGIN;
import static com.nextclick.crm.Config.Config.USER_PROFILE;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.FCM_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID_MOBILE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

import es.dmoral.toasty.Toasty;

public class SigninActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText etUserId, etPassword;
    private TextView tvForgotPassword, tvRegister,tv_change_loginType;
    private Button btnLogin;
    private String user_id_str,password_str;
    private Context mContext;
    PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;
    TextInputLayout layout_password;


    private ArrayList permissionsToRequest;
    private final ArrayList permissionsRejected = new ArrayList();
    private final ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        init();
        initializeListeners();

        //showVerifyOTPDialog(null,null,false);

        permissions.add(READ_SMS);
        permissions.add(RECEIVE_SMS);
        permissions.add(SEND_SMS);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (permissionsToRequest.size() > 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]),
                        ALL_PERMISSIONS_RESULT);
            }
        }
     }

    private void init() {
        mContext = SigninActivity.this;
        mCustomDialog=new CustomDialog(mContext);
        preferenceManager = new PreferenceManager(mContext);
        etUserId = findViewById(R.id.etUserId);
        etPassword = findViewById(R.id.etPassword);
        tvRegister = findViewById(R.id.tvRegister);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);
        layout_password= findViewById(R.id.layout_password);
        tv_change_loginType= findViewById(R.id.tv_change_loginType);
     //   etUserId.setFilters(new InputFilter[] { filter });

       // Utility.setSpecialCharFilter(etUserId);
    }



    private void initializeListeners() {
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
    }
    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private final IncomingSms receiver = new IncomingSms() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                System.out.println("aaaaaaaaa  message  "+message);
                getOtpFromMessage(message);
            }
        }
    };
    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{6}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            System.out.println("aaaaaaa  otp "+matcher.group(0));
            String otp=matcher.group(0);
            if(otp!=null) {
                if(editTextone!=null) {
                    editTextone.setText("" + otp.charAt(0));
                    editTexttwo.setText("" + otp.charAt(1));
                    editTextthree.setText("" + otp.charAt(2));
                    editTextfour.setText("" + otp.charAt(3));
                    editTextFive.setText("" + otp.charAt(4));
                    editTextSix.setText("" + otp.charAt(5));
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (isValid()) {
                    if (!isOtpLogin)
                        login(password_str);
                    else
                        sendOTP(user_id_str);
                }
                break;
            case R.id.tvForgotPassword:
                Intent registerIntent = new Intent(this, ResetPasswordActivity.class);
                startActivity(registerIntent);
                        /*

                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_forgotpassword, null);

                final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
                final TextView ok = alertLayout.findViewById(R.id.ok);

                AlertDialog.Builder alert = new AlertDialog.Builder(SigninActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle(getString(R.string.forgot_password));
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String forgotmail = mailtext.getText().toString().trim();
                        if (forgotmail.length() == 0) {
                            mailtext.setError("Should Not Be Empty");
                            mailtext.requestFocus();
                        } else if (!isValidEmail(forgotmail)) {
                            mailtext.setError("Invalid");
                            mailtext.requestFocus();
                        } else {
                            dialog.dismiss();
                            forgotPassword(forgotmail);
                        }

                    }
                });*/
                break;
            case R.id.tvRegister:
                goToRegister();
                break;
            default:
                break;
        }
    }

    private void goToRegister() {
        Intent registerIntent = new Intent(this, VerifyAndCreateAccount.class);//SigninActivity
        startActivity(registerIntent);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SigninActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("Do you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void forgotPassword(final String forgotMail) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        Log.e("forgotPassword", "request: "+data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mCustomDialog.dismiss();
                Log.e("forgotPassword", "response: "+response);
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(SigninActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle(getString(R.string.forgot_password));
                            alert.setMessage(getString(R.string.reset_mail_sent));
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();

                        } else {
                            mCustomDialog.dismiss();
                            AlertDialog.Builder alert = new AlertDialog.Builder(SigninActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage(message);
                            alert.setCancelable(true);
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }

                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Unable to send");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mContext, OOPS);
                mCustomDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private boolean isValid() {
        boolean valid = true;

        user_id_str = etUserId.getText().toString();
        password_str = etPassword.getText().toString();
        if (user_id_str ==null || user_id_str.length()  == 0) {
            setTextFieldErrorMethod(etUserId, EMPTY);
            valid = false;
        }  else if (user_id_str.length() < 10) {
            setTextFieldErrorMethod(etUserId, INVALID_MOBILE);
            valid = false;
        }
        else if (user_id_str.startsWith("0") || (user_id_str.startsWith("1")) || user_id_str.startsWith("2")||
                user_id_str.startsWith("3")|| user_id_str.startsWith("4")||user_id_str.startsWith("5")){
            setTextFieldErrorMethod(etUserId, INVALID_MOBILE);
            valid = false;
        }
        else if(!isOtpLogin) {
            if (password_str == null || password_str.length() == 0) {
                setTextFieldErrorMethod(etPassword, EMPTY);
                valid = false;
            }
        }
        return valid;
    }

    private void setTextFieldErrorMethod(TextInputEditText field, String err_msg) {
        field.setError(err_msg);
        field.requestFocus();
    }

    private void login(String input) {

        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("identity", user_id_str);
        if (isOtpLogin)
            dataMap.put("otp", input);
        else
            dataMap.put("password", input);
        dataMap.put(APP_ID, APP_ID_VALUE);
        dataMap.put("intent", Utility.VendorIntent);//Possible Intents: ["user", "delivery_partner", "vendor", "executive"]
        JSONObject json = new JSONObject(dataMap);

        mCustomDialog.show();
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("login_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String token = dataObject.getString("token");
                            try {
                                preferenceManager.putString("approval_status", null);

                                if(dataObject.has("approval_status")) {
                                    String approval_status = dataObject.getString("approval_status");
                                    preferenceManager.putString(USER_TOKEN, token);

                                    if (approval_status.equals("1"))//1-active,2-in active, 3- pending
                                    {
                                        //preferenceManager.putString(USER_TOKEN, token);
                                        preferenceManager.putString("approval_status", approval_status);

                                        FirebaseInstanceId.getInstance().getInstanceId()
                                                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                        if (!task.isSuccessful()) {

                                                            return;
                                                        }

                                                        String firebasetoken = task.getResult().getToken();
                                                        String msg = getString(R.string.fcm_token, firebasetoken);
                                                        System.out.println("aaaaaaa token  " + firebasetoken);
                                                        System.out.println("aaaaaaa msg  " + msg);
                                                        grantFCMPermission(firebasetoken);
                                                    }
                                                });
                                    }
                                    else if (approval_status.equals("3"))
                                    {
                                        Intent registerIntent = new Intent(mContext, WelcomeActivity.class);
                                        startActivity(registerIntent);
                                        //Toast.makeText(mContext, "Your account is waiting for admin approval, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        Intent intent = new Intent(mContext, WaitingActivity.class);
                                        intent.putExtra("approval_status",approval_status);
                                        startActivity(intent);
                                        //Toast.makeText(mContext, "Your status is currently in-active, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                                    }
                                    /*else if (approval_status.equals("4"))
                                    {
                                        Toast.makeText(mContext, "Your account is in waiting for admin approval, please contact the nextclick admin team.", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                        Toast.makeText(mContext, "approval_status is invalid"+approval_status, Toast.LENGTH_SHORT).show();*/
                                }
                                else
                                    Toast.makeText(mContext, "approval_status is not coming from the backend api", Toast.LENGTH_SHORT).show();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                              //  if (isOtpLogin)
                                  //  showVerifyOTPDialog(input, user_id_str, false);
                                //else

                                String messagae = jsonObject.getString("message");

                                if(messagae.equals("INCORRECT_PASSWORD"))
                                {
                                    messagae="Incorrect password";
                                }
                               else if(messagae.equals("USER_NOT_EXISTS"))
                                {
                                    messagae="You don't have the account please sign up";
                                }
                                else if(messagae.equals("INCORRECT_OTP"))
                                {
                                    //messagae="Incorrect otp";
                                    showVerifyOTPDialog(input, user_id_str, false);
                                    return;
                                }
                                Toasty.error(mContext, messagae, Toast.LENGTH_SHORT).show();
                                /*if(jsonObject.getString("data").contains("Incorrect Login"))
                                {
                                    Toasty.error(mContext, "No details found with given mobile no", Toast.LENGTH_SHORT).show();
                                }
                                else
                                    Toasty.error(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_SHORT).show();*/
                            }
                            catch (Exception ex) {
                                Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("message")), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (Exception e) {
                        mCustomDialog.dismiss();
                        System.out.println("aaaaaaaaaaa catch  "+e.getMessage());
                      //  UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    mCustomDialog.dismiss();
                  //  UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    boolean isMixPanelSupport =true;
    private void grantFCMPermission(final String msg) {
        mCustomDialog.show();
        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("token", msg);
        JSONObject json = new JSONObject(fcmMap);
        final String data = json.toString();
        System.out.println("aaaaaaaaa  daa  "+ data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                FCM, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("fcm_res", response);
                    try {
                        mCustomDialog.dismiss();
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {

                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.initializeMixPanel(getApplicationContext());
                                MyMixPanel.createUserID(etUserId.getText().toString().trim());
                            }
                            shareUserDetailsToMixPanel();

                            preferenceManager.putString(FCM_TOKEN, msg);
                            System.out.println("aaaaaaaa ccheck "+UserData.isNewDesign +"  "+preferenceManager.getString("VendorName"));
                            if(!UserData.isNewDesign || preferenceManager.getString("VendorName")!= null) {
                                startActivity(new Intent(mContext, ShopNowHomeActivity.class));//ServicesActivity
                                finish();
                            }
                        } else {
                            Toast.makeText(mContext, Html.fromHtml(jsonObject.getString("data")), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        System.out.println("aaaaaaaaaa catch  "+e.getMessage());
                        mCustomDialog.dismiss();
                        UIMsgs.showToast(mContext, "Something went wrong");
                        e.printStackTrace();
                    }
                } else {
                    mCustomDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                System.out.println("aaaaaaaaa  error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                map.put(APP_ID, APP_ID_VALUE);

                System.out.println("aaaaaaa map "+ map);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void shareUserDetailsToMixPanel() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, USER_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("p_res", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean status = jsonObject.getBoolean("status");
                        Integer status_code = jsonObject.getInt("http_code");
                        if (status || status_code == 200) {
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            String userName=dataObject.getString("display_name");

                            try {
                                if (dataObject.has("business_info")) {
                                    userName = dataObject.getJSONObject("business_info").getString("business_name");
                                }
                            }catch (Exception ex){}

                            MyMixPanel.sendUserLogin(userName,"",dataObject.getString("email"));
                            MyMixPanel.logEvent("Vendor Logged into the CRM application");

                            if (UserData.isNewDesign && preferenceManager.getString("VendorName") == null) {

                                //userName = userName.replaceAll("[?@,';^]*", "");

                                preferenceManager.putString("VendorName",userName);//dataObject.getString("name")
                                preferenceManager.putString("number",dataObject.getString("phone"));
                                try {
                                    if (dataObject.has("business_address")) {
                                        JSONObject business_address = dataObject.getJSONObject("business_address");
                                        if (business_address.has("constituency")) {
                                            String constitueid = business_address.getString("constituency");
                                            preferenceManager.putString("VendorConstituency", constitueid);
                                        }
                                    }
                                }catch (Exception ex){}

                                Intent intent = new Intent(mContext, ShopNowHomeActivity.class);//ServicesActivity
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    Boolean isOtpLogin =true;
    public void changeLoginType(View view)
    {
        if(tv_change_loginType.getText().toString().equals(getString(R.string.login_with_password)))
        {
            isOtpLogin =false;
            tv_change_loginType.setText(getString(R.string.login_with_otp));
            layout_password.setVisibility(View.VISIBLE);
        }
        else
        {
            isOtpLogin =true;
            tv_change_loginType.setText(getString(R.string.login_with_password));
            layout_password.setVisibility(GONE);
        }
    }



    private void sendOTP(String mobile) {

        final ProgressDialog progressDialog = new ProgressDialog(SigninActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while sending the otp.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Map<String, String> fcmMap = new HashMap<>();
        fcmMap.put("mobile", mobile);
        JSONObject json = new JSONObject(fcmMap);
        System.out.println("aaaaaaa request "+ json);
        final String data = json.toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Config.SEND_OTP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaa response "+ jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            //showVerifyOTPDialog(jsonObject.getJSONObject("data").getString("otp"),mobile,true);
                            showVerifyOTPDialog(null,mobile,false);
                        } else {
                            UIMsgs.showToast(mContext, Html.fromHtml(jsonObject.getString("message")).toString());
                        }
                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Something went wrong", Toast.LENGTH_SHORT);
                        e.printStackTrace();
                    } finally {
                        progressDialog.dismiss();
                    }
                } else {
                    progressDialog.dismiss();
                    UIMsgs.showToast(mContext, "Server under maintenance", Toast.LENGTH_SHORT);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                System.out.println("aaaaaaa error  "+error.getMessage());
                UIMsgs.showToast(mContext, "Something went wrong");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                //  map.put(AUTH_TOKEN, preferenceManager.getString(TOKEN_KEY));
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    SigninActivity.GenericTextWatcher watcher1,watcher2,watcher3,watcher4,watcher5,watcher6;
    EditText editTextone,editTexttwo,editTextthree, editTextfour,editTextFive,editTextSix;
    private void showVerifyOTPDialog(String otp,String mobilenumber,boolean isTest) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.verify_otp_dialog);


        LinearLayout layout_root= dialog.findViewById(R.id.layout_root);
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
        layoutParams.width = displayMetrics.widthPixels-50;
        layout_root.setLayoutParams(layoutParams);

        Button applyButton = dialog.findViewById(R.id.dialogButtonOK);
        ImageView closeButton = dialog.findViewById(R.id.closeButton);
         editTextone = dialog.findViewById(R.id.editTextone);
        editTexttwo = dialog.findViewById(R.id.editTexttwo);
         editTextthree = dialog.findViewById(R.id.editTextthree);
        editTextfour = dialog.findViewById(R.id.editTextfour);
        editTextFive = dialog.findViewById(R.id.editTextFive);
        editTextSix = dialog.findViewById(R.id.editTextSix);
        TextView tv_error = dialog.findViewById(R.id.tv_error);
        TextView tv_timer_resend = dialog.findViewById(R.id.tv_timer_resend);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_timer_resend.setText("Resends remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tv_timer_resend.setText("Resend Otp");
            }

        }.start();
        tv_timer_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv_timer_resend.getText().toString().equals("Resend Otp"))
                    sendOTP(mobilenumber);
            }
        });
        if(otp!=null&& !otp.isEmpty() && otp.length() >= 6)
        {
            if(!isTest)
                tv_error.setVisibility(View.VISIBLE);
            editTextone.setText(otp.substring(0, 1));
            editTexttwo.setText(otp.substring(1, 2));
            editTextthree.setText(otp.substring(2, 3));
            editTextfour.setText(otp.substring(3, 4));
            editTextFive.setText(otp.substring(4, 5));
            editTextSix.setText(otp.substring(5, 6));
        }


         /*watcher1 = new SigninActivity.GenericTextWatcher(editTextone);
         watcher2 = new SigninActivity.GenericTextWatcher(editTexttwo);
         watcher3 = new SigninActivity.GenericTextWatcher(editTextthree);
         watcher4 = new SigninActivity.GenericTextWatcher(editTextfour);
         watcher5 = new SigninActivity.GenericTextWatcher(editTextFive);
         watcher6 = new SigninActivity.GenericTextWatcher(editTextSix);
        editTextone.addTextChangedListener(watcher1);
        editTextone.setOnKeyListener(watcher1);
        editTexttwo.addTextChangedListener(watcher2);
        editTexttwo.setOnKeyListener(watcher2);
        editTextthree.addTextChangedListener(watcher3);
        editTextthree.setOnKeyListener(watcher3);
        editTextfour.addTextChangedListener(watcher4);
        editTextfour.setOnKeyListener(watcher4);
        editTextFive.addTextChangedListener(watcher5);
        editTextFive.setOnKeyListener(watcher5);
        editTextSix.addTextChangedListener(watcher6);
        editTextSix.setOnKeyListener(watcher6);*/
        Utility.setupOtpInputs(mContext,editTextone,editTexttwo,editTextthree,editTextfour,editTextFive,editTextSix);


        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = editTextone.getText().toString() + editTexttwo.getText().toString() +
                        editTextthree.getText().toString() + editTextfour.getText().toString() +
                        editTextFive.getText().toString() + editTextSix.getText().toString();

                if (otp != null && !otp.isEmpty() && otp.length() >= 6) {
                    dialog.dismiss();
                    login(otp);
                }
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public class GenericTextWatcher implements TextWatcher, View.OnKeyListener {
        private final View view;
        String previousText = "";

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.editTextone:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextone.removeTextChangedListener(watcher1);
                            editTextone.setText(previousText);
                            editTextone.addTextChangedListener(watcher1);

                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(text);
                            editTexttwo.addTextChangedListener(watcher2);
                        }
                        requestFocusAndSelection(editTexttwo,1);
                    }
                    break;
                case R.id.editTexttwo:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTexttwo.removeTextChangedListener(watcher2);
                            editTexttwo.setText(previousText);
                            editTexttwo.addTextChangedListener(watcher2);

                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(text);
                            editTextthree.addTextChangedListener(watcher3);

                        }
                      //  editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree,1);

                    } else if (text.length() == 0) {
                      //  editTextone.requestFocus();
                        requestFocusAndSelection(editTextone, 0);
                    }
                    break;
                case R.id.editTextthree:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextthree.removeTextChangedListener(watcher3);
                            editTextthree.setText(previousText);
                            editTextthree.addTextChangedListener(watcher3);

                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(text);
                            editTextfour.addTextChangedListener(watcher4);
                        }
                      //  editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour,1);
                    } else if (text.length() == 0) {
                       // editTexttwo.requestFocus();
                        requestFocusAndSelection(editTexttwo, 0);
                    }
                    break;
                case R.id.editTextfour:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextfour.removeTextChangedListener(watcher4);
                            editTextfour.setText(previousText);
                            editTextfour.addTextChangedListener(watcher4);

                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(text);
                            editTextFive.addTextChangedListener(watcher5);
                        }
                      //  editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,1);
                    } else if (text.length() == 0) {
                       // editTextthree.requestFocus();
                        requestFocusAndSelection(editTextthree, 0);
                    }
                    break;
                case R.id.editTextFive:
                    if (text.length() == 1) {
                        if (previousText.length() > 0) {
                            editTextFive.removeTextChangedListener(watcher5);
                            editTextFive.setText(previousText);
                            editTextFive.addTextChangedListener(watcher5);

                            editTextSix.removeTextChangedListener(watcher6);
                            editTextSix.setText(text);
                            editTextSix.addTextChangedListener(watcher6);
                        }
                       // editTextSix.requestFocus();
                        requestFocusAndSelection(editTextSix,1);
                    } else if (text.length() == 0) {
                        //editTextfour.requestFocus();
                        requestFocusAndSelection(editTextfour, 0);
                    }
                    break;
                case R.id.editTextSix:
                    if (text.length() == 0) {
                       // editTextFive.requestFocus();
                        requestFocusAndSelection(editTextFive,0);
                    } else {
                        try {
                            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        } catch (Exception e) {
                        }
                    }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            if (arg0.length() > 0) {
                previousText = arg0.toString();
            }
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            previousText = "";
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KEYCODE_DEL) {
                switch (view.getId()) {
                    case R.id.editTexttwo:
                        if (editTexttwo.getText().toString().trim().length() == 0)
                            editTextone.requestFocus();
                        break;
                    case R.id.editTextthree:
                        if (editTextthree.getText().toString().trim().length() == 0)
                            editTexttwo.requestFocus();
                        break;
                    case R.id.editTextfour:
                        if (editTextfour.getText().toString().trim().length() == 0)
                            editTextthree.requestFocus();
                        break;
                    case R.id.editTextFive:
                        if (editTextFive.getText().toString().trim().length() == 0)
                            editTextfour.requestFocus();
                        break;
                    case R.id.editTextSix:
                        if (editTextSix.getText().toString().trim().length() == 0)
                            editTextFive.requestFocus();
                        break;
                }

            }
            return false;
        }
    }

    private void requestFocusAndSelection(EditText et, int i) {
        et.requestFocus();
        if (et.getText().length() > 0)
            et.setSelection(1);
    }

}