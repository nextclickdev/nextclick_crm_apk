package com.nextclick.crm.authentication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.nextclick.crm.R;

public class WaitingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        String approval_status = getIntent().getStringExtra("approval_status");
        TextView tv_text = findViewById(R.id.tv_text);
        if(approval_status.equals("2"))
        tv_text.setText("Your status is currently in-active, please contact the nextclick admin team.");
        else if(approval_status.equals("4")) {
            //tv_text.setText(getString(R.string.account_approval_waiting));

            tv_text.setClickable(true);
            tv_text.setMovementMethod(LinkMovementMethod.getInstance());
            String text = getString(R.string.account_approval_waiting)+"<a href='http://www.nextclick.info/contact-us.html'>Click Here</a>";
            /*https://www.nextclick.in/contact.html*/
            tv_text.setText(Html.fromHtml(text));
        }
        else
            tv_text.setText("Your account is waiting for admin approval with status "+approval_status);
    }


    public void goToBack(View view) {
        finish();
    }
}