package com.nextclick.crm.authentication;

import static com.nextclick.crm.Config.Config.FORGOT_PASSWORD;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIValidations.EMPTY;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Validations;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ResetPasswordActivity extends AppCompatActivity {

    TextInputEditText tv_user_mail;
    String email_str;
    Validations validations;
    private CustomDialog mCustomDialog;
    private Context mContext;
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }
        mContext = ResetPasswordActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(mContext);
        tv_user_mail= findViewById(R.id.tv_user_mail);
        validations = new Validations();
    }

    public void goToBack(View view) {
        finish();
    }
    public void submit(View view) {
        boolean isValid=validator();
        if (isValid) {
            forgotPassword(email_str);
        }
        /*  callNextScreen();*/
    }

    boolean skipMobile = true;
    private boolean validator() {
        boolean validity = true;
        email_str = tv_user_mail.getText().toString();
        if (validations.isBlank(email_str)) {
            tv_user_mail.setError(EMPTY);
            tv_user_mail.requestFocus();
            validity = false;
        } else {
            validity=  Pattern.compile("^(?:\\d{10})$").matcher(email_str).matches();
            if(skipMobile)
                validity=false;
            if(!validity)
            {
                if (validations.isValidEmail(email_str)) {
                    tv_user_mail.setError(skipMobile?"Invalid Email ID":"Invalid Email ID or Mobile");//Invalid Email ID or Mobile
                    tv_user_mail.requestFocus();
                    validity = false;
                }
                else
                    validity = true;
            }

            /*
            if (email_str.contains("@")) {
                //email
                if (validations.isValidEmail(email_str)) {
                    tv_user_mail.setError("Invalid Email ID");
                    tv_user_mail.requestFocus();
                    validity = false;
                }
            } else {
                if (email_str.length() == 0) {
                    tv_user_mail.setError(EMPTY);
                    tv_user_mail.requestFocus();
                    validity = false;
                } else if (email_str.length() != 10) {
                    tv_user_mail.setError(INVALID_MOBILE);
                    tv_user_mail.requestFocus();
                    validity = false;
                } else if (email_str.startsWith("0") || (email_str.startsWith("1")) || email_str.startsWith("2") ||
                        email_str.startsWith("3") || email_str.startsWith("4") || email_str.startsWith("5")) {
                    tv_user_mail.setError(INVALID_MOBILE);
                    tv_user_mail.requestFocus();
                    validity = false;
                }
            }*/
        }

        return validity;
    }

    public void forgotPassword(final String forgotMail) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("email", forgotMail);//identity
        //datamap.put("phone", phone);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();

        Log.e("forgotPassword", "request: "+data);
        mCustomDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, FORGOT_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mCustomDialog.dismiss();
                Log.e("forgotPassword", "response: "+response);
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ResetPasswordActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle(getString(R.string.forgot_password));
                            alert.setMessage(getString(R.string.reset_mail_sent));
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                    finish();
                                }
                            });
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        } else {
                            mCustomDialog.dismiss();
                            AlertDialog.Builder alert = new AlertDialog.Builder(ResetPasswordActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage(message);
                            alert.setCancelable(true);
                            alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }

                    } catch (Exception e) {
                        UIMsgs.showToast(mContext, "Unable to send");
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                UIMsgs.showToast(mContext, OOPS);
                mCustomDialog.dismiss();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

}