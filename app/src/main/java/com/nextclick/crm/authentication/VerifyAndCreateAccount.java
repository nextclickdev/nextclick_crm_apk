package com.nextclick.crm.authentication;

import static com.nextclick.crm.Constants.ValidationMessages.INVALID_MOBILE;
import static com.nextclick.crm.Helpers.UIHelpers.UIValidations.EMPTY;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextclick.crm.R;
import com.google.android.material.textfield.TextInputEditText;

public class VerifyAndCreateAccount extends AppCompatActivity {


    ImageView signup_back_button;
    Button continue_btn;
    private TextView signup_title_text;
    TextInputEditText tv_user_mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_and_create_account);
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }


        signup_back_button = findViewById(R.id.signup_back_button);
        continue_btn = findViewById(R.id.continue_btn);
        signup_title_text = findViewById(R.id.signup_title_text);
        tv_user_mobile = findViewById(R.id.tv_user_mobile);
        if (getIntent().hasExtra("mobile"))
            tv_user_mobile.setText(getIntent().getStringExtra("mobile"));
    }

    public void goToBack(View view) {
        finish();
    }

    public void ContinueToNextScreen(View view) {
        String mobile_str=tv_user_mobile.getText().toString();
        if (mobile_str.length() == 0) {
            tv_user_mobile.setError(EMPTY);
            tv_user_mobile.requestFocus();
        }
        else if (mobile_str.length() < 10) {
            tv_user_mobile.setError(INVALID_MOBILE);
            tv_user_mobile.requestFocus();
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            tv_user_mobile.setError(INVALID_MOBILE);
            tv_user_mobile.requestFocus();
        }else {

            Intent intent = new Intent(VerifyAndCreateAccount.this, VerifyOtpActivity.class);//SignUpKYCActivity, WelcomeActivity
            intent.putExtra("mobile",mobile_str);
            startActivityForResult(intent,1111);
            finish();

            /*//Add transition
            Pair[] pairs = new Pair[3];
            pairs[0] = new Pair<View, String>(signup_back_button, "transition_back_arrow_btn");
            pairs[1] = new Pair<View, String>(signup_title_text, "transition_title_text");
            pairs[2] = new Pair<View, String>(continue_btn, "transition_next_btn");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(VerifyAndCreateAccount.this, pairs);
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1111)
        {
            // finish();
        }
    }
}