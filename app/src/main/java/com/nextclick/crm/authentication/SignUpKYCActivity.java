package com.nextclick.crm.authentication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nextclick.crm.R;
import com.google.android.material.textfield.TextInputEditText;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.telephony.gsm.SmsManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Adapters.CategoryAdapter;
import com.nextclick.crm.Common.Models.SubCategorySelection;
import com.nextclick.crm.Common.Models.TermsandConditions;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.Helpers.UIHelpers.Validations;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.Profilefragment;
import com.nextclick.crm.ShopNowModule.Models.CategoryObject;
import com.nextclick.crm.Utilities.ConnectionDetector;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.FindAddressInMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ACCEPT_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.CATEGORY_LIST;
import static com.nextclick.crm.Config.Config.GET_TERMSCONDITIONS;
import static com.nextclick.crm.Config.Config.MainAppVendorRegistration;
import static com.nextclick.crm.Config.Config.States;
import static com.nextclick.crm.Constants.Constants.APP_ID;
import static com.nextclick.crm.Constants.Constants.APP_ID_VALUE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.INVALID;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIValidations.EMPTY;

import es.dmoral.toasty.Toasty;

public class SignUpKYCActivity extends AppCompatActivity implements View.OnClickListener,
        LocationListener, SubCategorySelection {


    public String catName;
    Validations validations;
    Double lattitude, longitude;
    String currentAddress;
    String otp_str;
    Bundle bundle;
    Context mContext;
    PreferenceManager preferenceManager;
    String sID,dID;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int SELECT_MULTIPLE_FILE = 1;
    private String userChoosenTask;
    public static String coverPhotStringImageUrl1 = "";
    public static String bannerPhotStringImageUrl1 = "";
    public int flag = 0;
    char imageSelection;
    LocationManager locationManager;
    ConnectionDetector connectionDetector;
    TermsandConditions termsandConditions;
    ArrayList<String> selectedSubCategoryList = new ArrayList<String>();
    ArrayList<String> selectedAmenitiesList = new ArrayList<String>();
    ArrayList<String> selectedServicesList = new ArrayList<String>();
    ArrayList<String> holidayList = new ArrayList<>();
    ArrayList<String> statesId = new ArrayList<>();
    ArrayList<String> states = new ArrayList<>();
    ArrayList<String> districtsId = new ArrayList<>();
    ArrayList<String> districts = new ArrayList<>();
    ArrayList<String> constituenciesId = new ArrayList<>();
    ArrayList<String> constituencies = new ArrayList<>();
    ArrayList<String> categoryId = new ArrayList<>();
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<CategoryObject> ListCategoryObject=new ArrayList<>();
    ArrayList<String> timings = new ArrayList<>();
    Map<String, String> bannerBase64 = new HashMap<>();
    Map<String, String> servicesMap = new HashMap<>();
    Map<String, String> amenitiesMap = new HashMap<>();
    Map<String, Object> subCategorymap = new HashMap<>();
    Map<String, String> subCategorydatamap = new HashMap<>();
    Map<String, String> servicesdatamap = new HashMap<>();
    Map<String, String> amenitiesdatamap = new HashMap<>();
    ArrayList<String> amenitiesList = new ArrayList<>();
    ArrayList<String> servicesLits = new ArrayList<>();
    ArrayList<String> subcategoriesList = new ArrayList<>();
    ArrayList<String> servicesids = new ArrayList<>();
    ArrayList<String> selectedservicesid = new ArrayList<>();
    ArrayList<String> amenitiesids = new ArrayList<>();
    ArrayList<String> selectedamenitiesids = new ArrayList<>();
    ArrayList<String> subcategoryids = new ArrayList<>();
    ArrayList<String> selectedsubcategoryids = new ArrayList<>();

    TextInputEditText newListName,tv_owner_name,tv_gst_no,tv_labour_certificate_no,tv_fssai_no,pincode,complet_address;
    TextInputEditText altMobile, whatsapp;
    TextInputEditText select_business_categories,location_et;
    AutoCompleteTextView state_spinner, district_spinner, constituency_spinner;
    Button /*next,*/ submit;
    CheckBox terms;
    ProgressDialog progressDialog;
    ImageView cover_photo, add_cover_photo, banner_photo, add_banner_photo;
    String newListName_str, location_et_str, pincode_str,
            complete_address_str, whatsapp_str,constId,altMobile_str;
    public String catId;
    public ArrayList<String> selectedCategoryIDs =new ArrayList<>();

    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    AlertDialog.Builder builder;
    int range = 9;  // to generate a single number with this range, by default its 0..9
    int length = 4; // by default length is 4
    //firebase related
    private static final String TAG = "KYCActivity";


    private ArrayList permissionsToRequest;
    private final ArrayList permissionsRejected = new ArrayList();
    private final ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 101;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_kycactivity);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
        }

        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while fetching fields data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        //progressDialog.show();

       // createTestRecord();
        ImageView signup_back_button = findViewById(R.id.signup_back_button);
        signup_back_button.setOnClickListener(this);
        select_business_categories = findViewById(R.id.select_business_categories);
        select_business_categories.setKeyListener(null);
        select_business_categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCategoryPopup();
            }
        });
        location_et =findViewById(R.id.location);
        location_et.setKeyListener(null);
        location_et.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson=new Gson();
                ArrayList<String> locArray=new ArrayList<>();
                locArray.add(currentAddress);
                locArray.add(""+lattitude);
                locArray.add(""+longitude);
                String locationString = gson.toJson(locArray);
                Intent intent=new Intent(getApplicationContext(), FindAddressInMap.class);
                intent.putExtra("location",locationString);
                startActivityForResult(intent,Profilefragment.UPDATE_LOCATION);
            }
        });


        init();
        categoryDataFetcher();

        connectionDetector = new ConnectionDetector(mContext);

        validations = new Validations();

        AsyncTaskRunner asyncTaskRunner = new AsyncTaskRunner();
        asyncTaskRunner.execute();



        getLocation();
        statesDataFetcher();
        builder = new AlertDialog.Builder(this);


        bundle = getIntent().getExtras();

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }

        state_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0) {
                    sID = statesId.get(position - 1);
                    districtsId.clear();
                    districts.clear();
                    district_spinner.setText("");
                    constituency_spinner.setText("");
                    districtsDataFetcher(sID);
                }
                else
                {
                    sID="";
                    dID="";
                    constId="";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    district_spinner.setText("Select");
                    constituency_spinner.setText("Select");
                    ArrayAdapter ad = new ArrayAdapter(mContext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(ad);
                    constituency_spinner.setAdapter(ad);
                }
            }
        });

        district_spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ((position - 1) >= 0 && sID != null) {
                    dID = districtsId.get(position - 1);
                    constituencies.clear();
                    constituenciesId.clear();
                    constituency_spinner.setText("");
                    constituenciesDataFetcher(sID, dID);
                }
                else {
                    dID="";
                    constId="";
                    ArrayList<String> selectlist = new ArrayList<>();
                    selectlist.add("Select");
                    constituency_spinner.setText("Select");
                    ArrayAdapter ad = new ArrayAdapter(mContext,
                            android.R.layout.simple_spinner_item, selectlist);
                    ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(ad);
                }
            }
        });

        add_cover_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'c';
                boolean selecting = selectImage();
                if (selecting) {
                    //visibilty_gone.setVisibility(View.GONE);
                }
            }
        });

        add_banner_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSelection = 'b';
                boolean selecting = selectImage();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataFetcherFromFields();
                boolean isValid=validator();
                if (isValid)
                {
                    if (terms.isChecked()) {
                        dataSender();
                    } else {
                        UIMsgs.showToast(mContext, "Please accept terms and conditions");
                    }
                }
            }
        });
    }

    private void createTestRecord()  {
        progressDialog = new ProgressDialog(SignUpKYCActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while uploading the data.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        InputStream is = getResources().openRawResource(R.raw.input);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }

            is.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jsonString = writer.toString();
        RegisterVendor(jsonString, progressDialog);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup_back_button) {
            finish();
        }
    }

    public void callLoginActivity(View view) {
       // Intent intent = new Intent(SignUpKYCActivity.this, SigninActivity.class);
       // startActivity(intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UIMsgs.showToast(mContext, "Check Your connection");
        }
    }
    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (!connectionDetector.isConnectingToInternet()) {
            //Toast.makeText(mContext, "P", Toast.LENGTH_SHORT).show();
            UIMsgs.showToast(mContext, "Check Your connection");
        }
    }

    private final String blockCharacterSet = "@~#^|$%&*!";

    private final InputFilter filter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            if (source != null && blockCharacterSet.contains(("" + source))) {
                return "";
            }
            return null;
        }
    };
 InputFilter filter1 = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
                                   int dstart, int dend) {
            for (int i=start; i<end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i)) &&
                        !Character.isLowerCase(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };

    private void gettermsandconditions() {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("?page_id=", "1");
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GET_TERMSCONDITIONS+"?page_id=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    try{
                                        JSONArray dataarray = jsonObject.getJSONArray("data");
                                        JSONObject dataobject = dataarray.getJSONObject(0);
                                        termsandConditions=new TermsandConditions();
                                        termsandConditions.setId(dataobject.getString("id"));
                                        termsandConditions.setApp_details_id(dataobject.getString("app_details_id"));
                                        termsandConditions.setTitle(dataobject.getString("title"));
                                        termsandConditions.setPage_id(dataobject.getString("page_id"));
                                        termsandConditions.setDesc(dataobject.getString("desc"));
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);
                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);


                                        AlertDialog.Builder alert = new AlertDialog.Builder(SignUpKYCActivity.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        Resources res = getResources();
                                        int fontSize = 10;
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                terms.setChecked(true);
                                            }
                                        });
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }catch (Exception e){
                                        JSONObject dataobject = jsonObject.getJSONObject("data");
                                        termsandConditions=new TermsandConditions();
                                        termsandConditions.setId(dataobject.getString("id"));
                                        termsandConditions.setApp_details_id(dataobject.getString("app_details_id"));
                                        termsandConditions.setTitle(dataobject.getString("title"));
                                        termsandConditions.setPage_id(dataobject.getString("page_id"));
                                        termsandConditions.setDesc(dataobject.getString("desc"));
                                        LayoutInflater inflater = getLayoutInflater();
                                        View alertLayout = inflater.inflate(R.layout.activity_web, null);
                                        TextView tv_terms_id = alertLayout.findViewById(R.id.tv_terms_id);
                                        AlertDialog.Builder alert = new AlertDialog.Builder(SignUpKYCActivity.this);
                                        alert.setIcon(R.mipmap.ic_launcher);
                                        alert.setTitle(dataobject.getString("title"));
                                        // this is set the view from XML inside AlertDialog
                                        alert.setView(alertLayout);
                                        alert.setCancelable(false);
                                        Resources res = getResources();
                                        int fontSize = 10;
                                        tv_terms_id.setText(Html.fromHtml(dataobject.getString("desc")));
                                        alert.setCancelable(true);
                                        alert.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                terms.setChecked(true);
                                            }
                                        });
                                        AlertDialog dialog = alert.create();
                                        dialog.show();
                                    }

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                showToast(mContext,"Terms and Conditions API failed :"+response);
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void accepttermsandconditions(String id) {

        Map<String, String> termsmpa = new HashMap<>();
        termsmpa.put("page_id", "1");
        termsmpa.put("tc_id", ""+id);
        final String data = new JSONObject(termsmpa).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("terms_res", response);

                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    System.out.println("aaaaaaaa terms conditions sucess ");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs, ArrayList<String> selectedCategoryNames) {
        this.selectedCategoryIDs=selectedCategoryIDs;
        StringBuilder str = new StringBuilder();
        for (String eachstring : selectedCategoryNames) {
            str.append(eachstring).append(",");
        }


        String finalString=str.toString();
        if(finalString.endsWith(","))
            finalString=finalString.substring(0,finalString.length()-1);


        String text = "<font color=#F26B35> Selected Category : </font> <font color=#333333>" + catName+ "</font><br>"+
                "\n <font color=#F26B35> Sub Categories : </font> <font color=#333333>" + finalString+ "</font>";

        select_business_categories.setText(Html.fromHtml(text));
    }

    @Override
    public void setSelectedCategoryIDs(ArrayList<String> selectedCategoryIDs) {
        this.selectedCategoryIDs=selectedCategoryIDs;
    }

    @Override
    public void setCategory(CategoryObject category) {
        this.catId = category.getCatID();
        this.catName = category.getCatName();
    }

    class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            publishProgress("No Connection..."); // Calls onProgressUpdate()
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }

            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }

        }


        @Override
        protected void onPreExecute() {

        }
        @Override
        protected void onProgressUpdate(String... text) {
            /*finalResult.setText(text[0]);*/
            if (!connectionDetector.isConnectingToInternet()) {
                resp = "No Internet";
               /* Intent intent = new Intent(mContext, ConnectionAlert.class);
                startActivity(intent);
                finish();*/
                Toast.makeText(mContext, resp, Toast.LENGTH_SHORT).show();
            } else {
                resp = "Connected";
            }
        }
    }


    /*Locataion Start*/
    @SuppressLint("MissingPermission")
    void getLocation() {

        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //locationText.setText("Latitude: " + location_et.getLatitude() + "\n Longitude: " + location_et.getLongitude());


        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            /*locationText.setText(locationText.getText() + "\n"+addresses.get(0).getAddressLine(0)+", "+
                    addresses.get(0).getAddressLine(1)+", "+addresses.get(0).getAddressLine(2));
            */
            Double lat1 = location.getLatitude();
            Double lang1 = location.getLongitude();
            lattitude = lat1;
            longitude = lang1;
            currentAddress = addresses.get(0).getAddressLine(0) + "";
           // location_et.setText(currentAddress);//by default we are not showing current location here

        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

//Location End


    //Image Selection Start

    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpKYCActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)

            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(SignUpKYCActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode== Profilefragment.UPDATE_LOCATION) {
            updateLocation(data);
        }
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            if (imageSelection == 'c') {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                cover_photo.setImageBitmap(photo);

                Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
            if (imageSelection == 'b') {
                bannerBase64.clear();
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                banner_photo.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                byte[] byteArray = baos.toByteArray();
                bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);
            }
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }



       /* if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {

            this.pathList = data.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb = new StringBuilder("");

                ArrayList<Bitmap> bitmapList = new ArrayList<>();
                for (int i = 0; i < pathList.size(); i++) {
                   *//* sb.append("Photo"+(i+1)+":"+pathList.get(i));
                    sb.append("\n");*//*
                    Bitmap bitmap1 = BitmapFactory.decodeFile(pathList.get(i));//assign your bitmap;
                   *//* Bitmap bitmap2 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap3 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;
                    Bitmap bitmap4 = BitmapFactory.decodeResource(getResources(), drawable.user);//assign your bitmap;*//*


                    bitmapList.add(bitmap1);

                    Bitmap mergedImg = mergeMultiple(bitmapList);

                    banner_photo.setImageBitmap(mergedImg);

                }
                //tvResult.setText(sb.toString()); // here this is textview for sample use...

                onSelectFromGalleryResult(bitmapList);
                // Toast.makeText(mContext, sb.toString() + "", Toast.LENGTH_SHORT).show();
            }
        }*/

    }


    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

        if (imageSelection == 'c') {

            cover_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) cover_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            coverPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);

            flag = 1;
        }
        if (imageSelection == 'b') {

            banner_photo.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) banner_photo.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            bannerPhotStringImageUrl1 = Base64.encodeToString(byteArray, Base64.DEFAULT);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);
            bannerBase64.put(0 + "".trim(), bannerPhotStringImageUrl1);

            flag = 1;
        }

    }

    private void onSelectFromGalleryResult(ArrayList data) {


        bannerBase64.clear();

        for (int i = 0; i < data.size(); i++) {

            Bitmap bitmap = (Bitmap) data.get(i);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            String bannerBitString = Base64.encodeToString(byteArray, Base64.DEFAULT);
            bannerBase64.put(i + "".trim(), bannerBitString);
            //  System.out.println("ByteArra"+coverPhotStringImageUrl1);


            flag = 1;
        }


    }


    //Image Selection End


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == ALL_PERMISSIONS_RESULT) {
            for (Object perms : permissionsToRequest) {
                if (!hasPermission((String) perms)) {
                    permissionsRejected.add(perms);
                }
            }

            if (permissionsRejected.size() > 0) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                        return;
                    }
                }

            }
        } else {
            int permissionLocation = ContextCompat.checkSelfPermission(SignUpKYCActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                getLocation();
            }
        }
    }


    public void statesDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            statesId.add(dataObject.getString("id"));
                            states.add(dataObject.getString("name"));
                        }
                        // progressBar.setVisibility(View.GONE);

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, states);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    state_spinner.setAdapter(adapter);
                   // state_spinner.setPadding(0,5,0,5);

                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
               // map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void districtsDataFetcher(String stateid) {
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("districts");
                        districts.add("Select");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            districtsId.add(districtObject.getString("id"));
                            districts.add(districtObject.getString("name"));
                        }

                    } else {
                        UIMsgs.showToast(getApplicationContext(), messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, districts);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    district_spinner.setAdapter(adapter);
                    if (districts.size() > 0)
                        district_spinner.setSelection(0);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void constituenciesDataFetcher(String stateid, String districtid) {
        progressDialog.show();
        final int err = 0;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, States + "/" + stateid + "/" + districtid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("constituencies_resp", response);

                try {
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        constituencies.add("Select");
                        JSONObject dataObject = jsonData.getJSONObject("data");
                        JSONArray dataArray = dataObject.getJSONArray("constituenceis");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject districtObject = dataArray.getJSONObject(i);
                            constituenciesId.add(districtObject.getString("id"));
                            constituencies.add(districtObject.getString("name"));
                        }

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            android.R.layout.simple_spinner_item, constituencies);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    constituency_spinner.setAdapter(adapter);
                    progressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    // UIMsgs.showToast(mContext, "No data found...!");
                    progressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();


            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

    public void dataFetcherFromFields() {
        newListName_str = newListName.getText().toString().trim();
        location_et_str = location_et.getText().toString().trim();
        pincode_str = pincode.getText().toString().trim();
        complete_address_str = complet_address.getText().toString();//.trim();
        whatsapp_str = whatsapp.getText().toString().trim();
        altMobile_str = altMobile.getText().toString();
        if (constituencies.size() > 0) {
            int index = constituencies.indexOf(constituency_spinner.getText().toString());
            if (index != -1)
                constId = "" + constituenciesId.get(index-1);//constituenciesId.get(constituency_spinner.getListSelection() - 1);
            else
                constId = null;
        } else
            constId = null;
    }

    public void dataSender() {



        Map<String, Object> mainData = new HashMap<>();
        mainData.put("intent", Utility.VendorIntent);
        mainData.put("name", newListName_str);//businessname

        Map<String, String> businessAddress = new HashMap<>();//business_address
        businessAddress.put("location", location_et_str);
        businessAddress.put("lat", String.valueOf(lattitude));
        businessAddress.put("lng", String.valueOf(longitude));
        businessAddress.put("line1", complete_address_str);
        businessAddress.put("constituency", constId);
        businessAddress.put("state", sID);
        businessAddress.put("district", dID);
        businessAddress.put("zip_code",pincode.getText().toString());
        mainData.put("business_address", businessAddress);
        mainData.put("logo", coverPhotStringImageUrl1);


            try {
                if (bannerBase64 != null && bannerBase64.size() > 0) {
                    mainData.put("banner", bannerBase64.get("0"));//first one only
                }

                if (selectedCategoryIDs != null && selectedCategoryIDs.size() > 0) {
                    mainData.put("sub_categories", selectedCategoryIDs);
                }

            }
            catch (Exception ex){
                Toast.makeText(mContext, "exception "+ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }


        mainData.put("secondary_contact", altMobile.getText().toString());
        mainData.put("whats_app_no", whatsapp_str);


        mainData.put("business_category", catId);//business cat
        //mainData.put("sub_categories",productsArray);//sub catid
        mainData.put("owner",tv_owner_name.getText().toString());//optional
        mainData.put("gst",tv_gst_no.getText().toString());
        mainData.put("labour_certificate_number",tv_labour_certificate_no.getText().toString());
        mainData.put("fssai",tv_fssai_no.getText().toString());
        ObjectMapper mapperObj = new ObjectMapper();
        try {
            String jsonString = mapperObj.writeValueAsString(mainData);

            progressDialog = new ProgressDialog(SignUpKYCActivity.this);
            progressDialog.setIcon(R.drawable.nextclick_logo_black);
            progressDialog.setMessage("Please wait while uploading the data.....");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setProgress(0);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            RegisterVendor(jsonString, progressDialog);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "exception "+e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void dataClearance() {


        selectedSubCategoryList.clear();
        selectedAmenitiesList.clear();
        selectedServicesList.clear();
        holidayList.clear();

        coverPhotStringImageUrl1 = "".trim();
        banner_photo.setImageBitmap(null);
        cover_photo.setImageBitmap(null);
        bannerBase64.clear();
        servicesMap.clear();
        amenitiesMap.clear();
        subCategorymap.clear();
        subCategorydatamap.clear();
        servicesdatamap.clear();
        amenitiesdatamap.clear();
        amenitiesList.clear();
        servicesLits.clear();
        subcategoriesList.clear();

        servicesids.clear();
        selectedservicesid.clear();

        amenitiesids.clear();
        selectedamenitiesids.clear();

        subcategoryids.clear();
        selectedsubcategoryids.clear();

    }

    public void RegisterVendor(final String json, final ProgressDialog progressDialog) {

        final String data = json;
        Log.v("requesr register ",data);
        System.out.println("aaaaaaaaaaa  request regeter  "+ json);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MainAppVendorRegistration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            Boolean status = jsonObject.getBoolean("status");
                            String message = jsonObject.getString("message");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status ) {
                                 UIMsgs.showToast(mContext, "Successfully Submitted, please wait for the admin approval.");
                                 finish();
                               // callLoginActivity(null);
                            } else {
                                UIMsgs.showToast(mContext, message);
                                Log.d("er msg", Html.fromHtml(message).toString());
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UIMsgs.showToast(mContext,e.getMessage());
                        }
                        finally {
                            progressDialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  catchh "+error.getMessage());
                //Toast.makeText(LoginActivity.this, error+"", Toast.LENGTH_SHORT).show();
                UIMsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, "Bearer " +preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void savetermsandconditions(ProgressDialog progressDialog,String mesg) {

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ACCEPT_TERMSCONDITIONS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaaaa  response register "+ jsonObject);
                            Boolean status = jsonObject.getBoolean("status");
                            String http = jsonObject.getString("http_code");
                            String message = jsonObject.getString("message");
                            // String data = jsonObject.getString("data");
                            System.out.println("aaaaaaa responce "+jsonObject);
                            if (status ) {

                                showAlert(mContext,""+mesg);
                            } else {
                                UIMsgs.showToast(mContext, message);
                                Log.d("er msg", Html.fromHtml(message).toString());
                                progressDialog.dismiss();
                            }

                        } catch (Exception e) {
                            System.out.println("aaaaaaaa  catchh "+e.getMessage());
                            UIMsgs.showToast(mContext,e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaa  error "+error.getMessage());
            //    UIMsgs.showToast(mContext, "Some Error Occured.. Please provide data again...");
                progressDialog.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(APP_ID, APP_ID_VALUE);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public boolean validator()
    {
        boolean validity = true;
        if (newListName_str.length() == 0) {
            newListName.setError("Please Enter Business Name");
            newListName.requestFocus();
            validity = false;
        }
        else if (catId == null) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if (selectedCategoryIDs == null || selectedCategoryIDs.size() == 0) {
            showToast(mContext, "Please Select Business Categories");
            validity = false;
        }
        else if(tv_owner_name.getText().toString()==null || tv_owner_name.getText().toString().isEmpty())
        {
            tv_owner_name.setError("Please enter Owner name");
            tv_owner_name.requestFocus();
            validity = false;
        }
       /* else if(tv_gst_no.getText().toString()==null || tv_gst_no.getText().toString().isEmpty())
        {
            tv_gst_no.setError(EMPTY_NOT_ALLOWED);
            tv_gst_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_gst_no.getText()) && !Validations.isValidGST(tv_gst_no.getText().toString()))
        {
            tv_gst_no.setError(INVALID);
            tv_gst_no.requestFocus();
            validity = false;
        }
    /*    else if(tv_labour_certificate_no.getText().toString()==null || tv_labour_certificate_no.getText().toString().isEmpty())
        {
            tv_labour_certificate_no.setError(EMPTY_NOT_ALLOWED);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_labour_certificate_no.getText()) &&
                !Validations.isValidLabourCertificationNumber(tv_labour_certificate_no.getText().toString()))
        {
            tv_labour_certificate_no.setError(INVALID);
            tv_labour_certificate_no.requestFocus();
            validity = false;
        }
      /*  else if(tv_fssai_no.getText().toString()==null || tv_fssai_no.getText().toString().isEmpty())
        {
            tv_fssai_no.setError(EMPTY_NOT_ALLOWED);
            tv_fssai_no.requestFocus();
            validity = false;
        }*/
        else if(!Validations.IsEmpty(tv_fssai_no.getText()) && !Validations.isValidFSSAI(tv_fssai_no.getText().toString()))
        {
            tv_fssai_no.setError(INVALID);
            tv_fssai_no.requestFocus();
            validity = false;
        }
        else if (location_et_str.length() == 0) {
            location_et.setError("Wait untill location fetched");
            location_et.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() == 0) {
            complet_address.setError(EMPTY_NOT_ALLOWED);
            complet_address.requestFocus();
            validity = false;
        }
        else if (complete_address_str.length() < 6) {
            complet_address.setError("Please provide complete details");
            complet_address.requestFocus();
            validity = false;
        }
        else if (sID == null || sID.length() == 0) {
            showToast(mContext, "Please Select State");
            validity = false;
        }
        else if (dID == null || dID.length() == 0) {
            showToast(mContext, "Please Select District");
            validity = false;
        }
        else if (constId == null || constId.length() == 0) {
            showToast(mContext, "Please Select Constituency");
            constituency_spinner.requestFocus();
            validity = false;
        }
        else if (pincode_str.length() == 0) {
            pincode.setError(EMPTY);
            pincode.requestFocus();
            validity = false;
        }
        else if (pincode_str.startsWith("0") ||  pincode_str.length() < 6) {
            pincode.setError("PIN CODE is  Invalid");
            pincode.requestFocus();
            validity = false;
        }
       /* else if (validations.isBlank(email_str)) {
            email.setError(EMPTY);
            email.requestFocus();
            validity = false;
        }
        else if (validations.isValidEmail(email_str)) {
            email.setError(INVALID);
            email.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() == 0) {
            mobile.setError(EMPTY);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.length() < 10) {
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }
        else if (mobile_str.startsWith("0") || (mobile_str.startsWith("1")) || mobile_str.startsWith("2")||
                mobile_str.startsWith("3")|| mobile_str.startsWith("4")||mobile_str.startsWith("5")){
            mobile.setError(INVALID_MOBILE);
            mobile.requestFocus();
            validity = false;
        }*/
        if (validity && altMobile_str.length() > 0) {
           if (altMobile_str.length() < 10) {
               altMobile.setError(INVALID);
               altMobile.requestFocus();
                validity = false;
            } else if (altMobile_str.startsWith("0") || (altMobile_str.startsWith("1")) || altMobile_str.startsWith("2") ||
                   altMobile_str.startsWith("3") || altMobile_str.startsWith("4") || altMobile_str.startsWith("5")) {
               altMobile.setError("Invalid Alternative Mobile Number");
               altMobile.requestFocus();
                validity = false;
            }
        }
        if (validity && whatsapp_str.length() > 0) {
            if (whatsapp_str.length() < 10) {
                whatsapp.setError(INVALID);
                whatsapp.requestFocus();
                validity = false;
            } else if (whatsapp_str.startsWith("0") || (whatsapp_str.startsWith("1")) || whatsapp_str.startsWith("2") ||
                    whatsapp_str.startsWith("3") || whatsapp_str.startsWith("4") || whatsapp_str.startsWith("5")) {
                whatsapp.setError("Invalid Whatsapp Number");
                whatsapp.requestFocus();
                validity = false;
            }

        }
        if ((whatsapp_str.isEmpty()))
        {
            validity = false;
            whatsapp.setError("");
        }
        if(!validity)
        {
            return validity;
        }
        /*else if (landmark_str.length() == 0) {
            landmark.setError(EMPTY_NOT_ALLOWED);
            landmark.requestFocus();
            validity = false;
        }
        else if (landmark_str.length() <= 6) {
            landmark.setError("Please provide clear details");
            landmark.requestFocus();
            validity = false;
        }
        else if (openingTimeList.size() < 1) {
            Toast.makeText(mContext, "Please provide timings", Toast.LENGTH_SHORT).show();
            everyday_opening_time.requestFocus();
            validity = false;
        }
        else if (std_code_str.length() > 0 && std_code_str.length() < 3) {
            std_code.setError(INVALID);
            std_code.requestFocus();
            validity = false;
        }

        else if (landline_str.length() > 0 && landline_str.length() < 6) {
            landline.setError(INVALID);
            landline.requestFocus();
            validity = false;
        }*/
       /* else  if (password_str.length() == 0) {
            business_pwd.setError(EMPTY);
            business_pwd.requestFocus();
            validity = false;
        }
        else if (password_str.length() < 4) {
            business_pwd.setError("Minimum 4 Characters");
            business_pwd.requestFocus();
            validity = false;
        } else  if (re_password_str.length() == 0) {
            re_business_pwd.setError(EMPTY);
            re_business_pwd.requestFocus();
            validity = false;
        }
        else if (re_password_str.length() < 4) {
            re_business_pwd.setError("Minimum 4 Characters");
            re_business_pwd.requestFocus();
            validity = false;
        }else if (!password_str.equalsIgnoreCase(re_password_str)){
            re_business_pwd.setError("Password Not Match");
            re_business_pwd.requestFocus();
            validity = false;
        }*/

        /*else if (helpline_str.length() > 0 && helpline_str.length() < 10) {
            helpline.setError(INVALID);
            helpline.requestFocus();
            validity = false;
        }*/
        else if (coverPhotStringImageUrl1 == null || coverPhotStringImageUrl1.length() <= 0) {
            showToast(mContext, "Please Select Cover Image");
            cover_photo.requestFocus();
            validity = false;
        }
        else if (bannerBase64 == null || bannerBase64.size() <= 0) {//2
            showToast(mContext, "Please Select Banner Image");
            banner_photo.requestFocus();
            validity = false;
        }
        return validity;
    }

    private void showToast(Context mContext, String message) {
        Toasty.error(mContext, message, Toast.LENGTH_SHORT).show();
       // UIMsgs.showToast(mContext, message);
    }

    private Bitmap mergeMultiple(ArrayList<Bitmap> parts) {
        final int IMAGE_MAX_SIZE = 1200000;
        int value = parts.size();
        int height = parts.get(0).getHeight();
        int width = parts.get(0).getWidth();

        double y = Math.sqrt(IMAGE_MAX_SIZE
                / (((double) width) / height));
        double x = (y / height) * width;
        Bitmap result = Bitmap.createScaledBitmap(parts.get(0), (int) x, (int) y, true);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        for (int i = 0; i < parts.size(); i++) {
            canvas.drawBitmap(parts.get(i), parts.get(i).getWidth() * (i % 4), parts.get(i).getHeight() * (i / 4), paint);
        }
        return result;
    }

    public void init() {

        otp_str = generateRandomNumber();
        newListName = findViewById(R.id.new_list_name);

        pincode = findViewById(R.id.pincode);
        complet_address = findViewById(R.id.complet_address);
        altMobile =  findViewById(R.id.mobile1);
        whatsapp = findViewById(R.id.whatsapp);
        submit = findViewById(R.id.submit);
        terms = findViewById(R.id.terms);
        cover_photo =findViewById(R.id.cover_photo);
        add_cover_photo =findViewById(R.id.add_cover_photo);
        banner_photo = findViewById(R.id.banner_photo);
        add_banner_photo = findViewById(R.id.add_banner_photo);
        state_spinner =  findViewById(R.id.state_spinner);
        district_spinner = findViewById(R.id.district_spinner);
        constituency_spinner = findViewById(R.id.constituency_spinner);


        state_spinner.setKeyListener(null);
        state_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state_spinner.showDropDown();
            }
        });
        district_spinner.setKeyListener(null);
        district_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                district_spinner.showDropDown();
            }
        });
        constituency_spinner.setKeyListener(null);
        constituency_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constituency_spinner.showDropDown();
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettermsandconditions();
            }
        });
        /*complet_address.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                complet_address.setSelection(complet_address.getText().length());
                return false;
            }
        });*/
        states.add("Select");
        categories.add("Select");
        newListName.setFilters(new InputFilter[] { filter });


        tv_gst_no=findViewById(R.id.gst_no);
        tv_labour_certificate_no=findViewById(R.id.tv_labour_certificate_no);
        tv_fssai_no=findViewById(R.id.tv_fssai_no);
        tv_owner_name=findViewById(R.id.tv_owner_name);
        pincode=findViewById(R.id.pincode);
    }

    public String generateRandomNumber() {
        int randomNumber;
        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++) {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0) { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }
        //randomNumber = Integer.parseInt(s);
        return s;
    }


    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex.getMessage(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public  void showAlert(Context context,String message) {
        android.app.AlertDialog.Builder alertDialogBuilder = new
                android.app.AlertDialog.Builder(SignUpKYCActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showCategoryPopup() {
        //categories

        System.out.println("aaaaaaaaa "+categoryId);
        if(categoryId!=null && categoryId.size()>1)
        {
         //   BottomCategoryFragment fragment = new BottomCategoryFragment(mContext, categoryId,categories,selectedCategoryIDs,this);
          //  fragment.show(getSupportFragmentManager(), fragment.getTag());



            ArrayList<String> selectedCategories=new ArrayList<>();

            Dialog dialog = new Dialog(this);

            View contentView = View.inflate(mContext, R.layout.select_categories_new, null);//select_categories
            //context = contentView.getContext();
            dialog.setContentView(contentView);

            LinearLayout layout_root= dialog.findViewById(R.id.layout_root);
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            ViewGroup.LayoutParams layoutParams = layout_root.getLayoutParams();
            layoutParams.width = displayMetrics.widthPixels;
            layout_root.setLayoutParams(layoutParams);


            Button dialogButton = dialog.findViewById(R.id.dialogButtonOK);
            ImageView closeButton = dialog.findViewById(R.id.closeButton);

            RecyclerView recyclerView_categories=dialog.findViewById(R.id.recyclerView_categories);
            RecyclerView recyclerView_subcategories=dialog.findViewById(R.id.recyclerView_subcategories);

            TextView tv_no_categories=dialog.findViewById(R.id.tv_no_categories);
            TextView tv_no_sub_categories=dialog.findViewById(R.id.tv_no_sub_categories);
            TextView tv_error=dialog.findViewById(R.id.tv_error);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_categories.setLayoutManager(linearLayoutManager);

            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(mContext);
            linearLayoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView_subcategories.setLayoutManager(linearLayoutManager1);


            CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, ListCategoryObject,
                    recyclerView_subcategories,
                    tv_no_categories,tv_no_sub_categories,selectedCategoryIDs,selectedCategories,
                    this);
            recyclerView_categories.setAdapter(categoryAdapter);

            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedCategories.size() > 0) {
                        dialog.dismiss();
                        setSelectedCategoryIDs(selectedCategoryIDs,selectedCategories);
                    }
                    else {
                        tv_error.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tv_error.setVisibility(View.GONE);
                            }
                        },2000);
                    }
                }
            });

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(android.R.color.transparent);
            window.setGravity(Gravity.BOTTOM);

             dialog.show();

        }

    }


    public void updateLocation(Intent intent)
    {
        ArrayList<String> locArray=new ArrayList<>();
        try {
            Gson gson = new Gson();
            String carListAsString = intent.getStringExtra("location");
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            locArray = gson.fromJson(carListAsString, type);
            if(locArray!=null && locArray.size()>=3) {
                lattitude = Double.parseDouble(locArray.get(1));
                longitude = Double.parseDouble(locArray.get(2));
                location_et.setText(locArray.get(0));
                location_et.setError(null);
                if (locArray.size() >= 4 && (pincode.getText().toString() == null || pincode.getText().toString().length() == 0))
                    pincode.setText(locArray.get(3));
            }
        }catch (Exception ex) {
            getLocation();
        }
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();
        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    } private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    public void categoryDataFetcher() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, CATEGORY_LIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    progressDialog.dismiss();
                    JSONObject jsonData = new JSONObject(response);
                    String status = jsonData.getString("status");
                    String messagae = jsonData.getString("message");
                    if (status.equalsIgnoreCase("true") && messagae.equalsIgnoreCase("Success..!")) {

                        JSONArray dataArray = jsonData.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObject = dataArray.getJSONObject(i);
                            CategoryObject c=new CategoryObject();
                            c.setCatID(dataObject.getString("id"));
                            c.setCatName(dataObject.getString("name"));
                            c.setCatImage(dataObject.getString("image"));
                            categoryId.add(c.getCatID());
                            categories.add(c.getCatName());
                            ListCategoryObject.add(c);
                        }

                    } else {
                        UIMsgs.showToast(mContext, messagae);
                        //Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    UIMsgs.showToast(mContext, String.valueOf(e));
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, error + "", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                //map.put("Authorization", token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
