package com.nextclick.crm.OnDemandServicesModule.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.OnDemandServicesModule.adapters.ServicesTabAdapter;
import com.nextclick.crm.OnDemandServicesModule.fragments.ApprovedServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.MyServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.PendingServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.ServicesAdminFragment;
import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.adapter.DoctorsTabAdapter;
import com.nextclick.crm.doctorsModule.fragment.MyDoctorsFragment;
import com.google.android.material.tabs.TabLayout;

public class OnDemandServicesActivity extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabLayout;
    private ImageView ivBackArrow;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_on_demand_services);
        initializeUi();
        initializeListeners();
        initializeTabs();
    }

    private void initializeUi() {
        tabLayout = findViewById(R.id.tabLayout);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void initializeTabs() {
        ServicesTabAdapter doctorsTabAdapter = new ServicesTabAdapter(this, getSupportFragmentManager(), tabLayout, R.id.fragmentContainer);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.admin)).setTag(getResources().getString(R.string.admin)), ServicesAdminFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.my_services)).setTag(getResources().getString(R.string.my_services)), MyServicesFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.approved)).setTag(getResources().getString(R.string.approved)), ApprovedServicesFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.pending)).setTag(getResources().getString(R.string.pending)), PendingServicesFragment.class, null);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }
}
