package com.nextclick.crm.OnDemandServicesModule.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.activities.AddAsYourServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.activities.EditServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.approvedServiceFragmentResponse.ApprovedServiceDetails;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.DeleteServiceApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ApprovedServicesFragmentAdapter  extends RecyclerView.Adapter<ApprovedServicesFragmentAdapter.ViewHolder> implements HttpReqResCallBack {

    private final Context context;
    private final PreferenceManager preferenceManager;
    private ApprovedServiceDetails approvedServiceDetails;

    private final LinkedList<ApprovedServiceDetails> listOfApprovedServiceDetails;

    private int selectedPosition = -1;

    private String token = "";

    public ApprovedServicesFragmentAdapter(Context context, LinkedList<ApprovedServiceDetails> listOfApprovedServiceDetails) {
        this.context = context;
        preferenceManager = new PreferenceManager(context);
        this.listOfApprovedServiceDetails = listOfApprovedServiceDetails;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_approved_services_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ApprovedServiceDetails approvedServiceDetails = listOfApprovedServiceDetails.get(position);
        if (approvedServiceDetails != null) {
            String serviceName = approvedServiceDetails.getName();
            String serviceImage = approvedServiceDetails.getImage();
            String serviceDescription = approvedServiceDetails.getDesc();

            holder.tvServiceName.setText(serviceName);
            holder.tvDescription.setText(Html.fromHtml(serviceDescription));

            if (serviceImage != null) {
                if (!serviceImage.isEmpty()) {
                    Picasso.get()
                            .load(serviceImage)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivServicePic);
                } else {
                    Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivServicePic);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivServicePic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfApprovedServiceDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivServicePic;
        private final TextView tvServiceName;
        private final TextView tvQualification;
        private final TextView tvDescription;
        private final TextView tvAddAsYourService;
        private final TextView tvDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDelete = itemView.findViewById(R.id.tvDelete);
            ivServicePic = itemView.findViewById(R.id.ivServicePic);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvQualification = itemView.findViewById(R.id.tvQualification);
            tvAddAsYourService = itemView.findViewById(R.id.tvAddAsYourService);

            tvDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
            tvAddAsYourService.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            selectedPosition = getLayoutPosition();
            approvedServiceDetails = listOfApprovedServiceDetails.get(getLayoutPosition());
            if (id == R.id.tvDelete) {
                showDeleteServiceAlertDialog();
            } else if (id == R.id.tvAddAsYourService) {
                showAddAsYourServiceAlertDialog();
            } else {
                int serviceId = approvedServiceDetails.getId();
                int ODServiceID = approvedServiceDetails.getOnDemandServiceID();
                Intent editService = new Intent(context, EditServiceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
                bundle.putString(context.getString(R.string.od_service_id), String.valueOf(ODServiceID));
                editService.putExtras(bundle);
                context.startActivity(editService);
            }
        }
    }

    private void showAddAsYourServiceAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.do_you_want_to_add_this_as_your_service));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int serviceId = approvedServiceDetails.getId();
                int ODServiceID = approvedServiceDetails.getOnDemandServiceID();
                Intent addAsYourService = new Intent(context, AddAsYourServiceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
                bundle.putString(context.getString(R.string.od_service_id), String.valueOf(ODServiceID));
                addAsYourService.putExtras(bundle);
                context.startActivity(addAsYourService);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showDeleteServiceAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.are_you_sure_do_you_want_to_delete));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoadingDialog.loadDialog(context);
                token = "Bearer " + preferenceManager.getString(USER_TOKEN);
                DeleteServiceApiCall.serviceCallForDeleteService(context, null, ApprovedServicesFragmentAdapter.this, approvedServiceDetails.getId(), approvedServiceDetails.getOnDemandServiceID(), token);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_DELETE_SERVICE) {
            if (jsonResponse != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    boolean status = jsonObject.getBoolean("status");
                    String message = jsonObject.getString("message");
                    if (status) {
                        if (selectedPosition != -1) {
                            listOfApprovedServiceDetails.remove(selectedPosition);
                            notifyDataSetChanged();
                        }
                    }
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }
}
