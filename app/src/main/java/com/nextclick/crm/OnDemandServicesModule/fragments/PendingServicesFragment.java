package com.nextclick.crm.OnDemandServicesModule.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.activities.AddServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.adapters.PendingServicesFragmentAdapter;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.servicePendingFragmentResponse.PendingServiceDetails;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.servicePendingFragmentResponse.PendingServicesFragmentResponse;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.PendingServiceDetailsApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class PendingServicesFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private RecyclerView rvServicesPendingDetails;
    private FloatingActionButton fabAddService;
    private PreferenceManager preferenceManager;

    private LinkedList<PendingServiceDetails> listOfPendingServiceDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_pending_service_details, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddService = view.findViewById(R.id.fabAddService);
        rvServicesPendingDetails = view.findViewById(R.id.rvServicesPendingDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
        }
    }

    private void initializeListeners() {
        fabAddService.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        PendingServiceDetailsApiCall.serviceCallForPendingServiceDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshServices = preferenceManager.getBoolean(getString(R.string.refresh_services));
        if (refreshServices) {
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_PENDING_SERVICE_DETAILS) {
            if (jsonResponse != null) {
                PendingServicesFragmentResponse pendingServicesFragmentResponse = new Gson().fromJson(jsonResponse, PendingServicesFragmentResponse.class);
                if (pendingServicesFragmentResponse != null) {
                    boolean status = pendingServicesFragmentResponse.getStatus();
                    String message = pendingServicesFragmentResponse.getMessage();
                    if (status) {
                        listOfPendingServiceDetails = pendingServicesFragmentResponse.getListOfPendingServiceDetails();
                        if (listOfPendingServiceDetails != null) {
                            if (listOfPendingServiceDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        listIsEmpty();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        PendingServicesFragmentAdapter pendingServicesFragmentAdapter = new PendingServicesFragmentAdapter(getActivity(), listOfPendingServiceDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvServicesPendingDetails.setLayoutManager(layoutManager);
        rvServicesPendingDetails.setItemAnimator(new DefaultItemAnimator());
        rvServicesPendingDetails.setAdapter(pendingServicesFragmentAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvServicesPendingDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvServicesPendingDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddService) {
            goToAddService();
        }
    }

    private void goToAddService() {
        if (getActivity() != null) {
            Intent addServiceIntent = new Intent(getActivity(), AddServiceActivity.class);
            startActivity(addServiceIntent);
        }
    }
}
