package com.nextclick.crm.OnDemandServicesModule.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.OnDemandServicesModule.activities.EditServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.myServicesResponse.MyServiceDetails;
import com.nextclick.crm.R;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class MyServicesFragmentAdapter  extends RecyclerView.Adapter<MyServicesFragmentAdapter.ViewHolder> {

    private final Context context;
    private final LinkedList<MyServiceDetails> listOfMyServiceDetails;

    public MyServicesFragmentAdapter(Context context, LinkedList<MyServiceDetails> listOfMyServiceDetails) {
        this.context = context;
        this.listOfMyServiceDetails = listOfMyServiceDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_services_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyServiceDetails myServiceDetails = listOfMyServiceDetails.get(position);
        String serviceName = myServiceDetails.getName();
        String serviceImageUrl = myServiceDetails.getImage();
        String serviceDescription = myServiceDetails.getDesc();


        holder.tvServiceName.setText(serviceName);
        holder.tvDescription.setText(Html.fromHtml(serviceDescription));

        if (serviceImageUrl != null) {
            if (!serviceImageUrl.isEmpty()) {
                Picasso.get()
                        .load(serviceImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivServicePic);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivServicePic);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.ivServicePic);
        }

    }

    @Override
    public int getItemCount() {
        return listOfMyServiceDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivServicePic;
        private final TextView tvServiceName;
        private final TextView tvQualification;
        private final TextView tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivServicePic = itemView.findViewById(R.id.ivServicePic);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvQualification = itemView.findViewById(R.id.tvQualification);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            MyServiceDetails myServiceDetails = listOfMyServiceDetails.get(getLayoutPosition());
            int serviceId = myServiceDetails.getId();
            int ODServiceId = myServiceDetails.getOnDemandServiceID();
            Intent editService = new Intent(context, EditServiceActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
            bundle.putString(context.getString(R.string.od_service_id), String.valueOf(ODServiceId));
            editService.putExtras(bundle);
            context.startActivity(editService);
        }
    }
}
