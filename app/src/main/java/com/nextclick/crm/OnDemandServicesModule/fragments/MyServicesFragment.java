package com.nextclick.crm.OnDemandServicesModule.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.activities.AddServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.adapters.MyServicesFragmentAdapter;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.myServicesResponse.MyServiceDetails;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.myServicesResponse.MyServicesResponse;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.MyServicesDetailsApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class MyServicesFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private RecyclerView rvMyServicesDetails;
    private FloatingActionButton fabAddService;
    private PreferenceManager preferenceManager;

    private LinkedList<MyServiceDetails> listOfMyServiceDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_my_services_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddService = view.findViewById(R.id.fabAddService);
        rvMyServicesDetails = view.findViewById(R.id.rvMyServicesDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
        }
    }

    private void initializeListeners() {
        fabAddService.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        MyServicesDetailsApiCall.serviceCallForMyServiceDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshServices = preferenceManager.getBoolean(getString(R.string.refresh_services));
        if (refreshServices) {
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_MY_SERVICE_DETAILS) {
            if (jsonResponse != null) {
                MyServicesResponse myServicesResponse = new Gson().fromJson(jsonResponse, MyServicesResponse.class);
                if (myServicesResponse != null) {
                    boolean status = myServicesResponse.getStatus();
                    if (status) {
                        listOfMyServiceDetails = myServicesResponse.getListOfMyServiceDetails();
                        if (listOfMyServiceDetails != null) {
                            if (listOfMyServiceDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        MyServicesFragmentAdapter myServicesFragmentAdapter = new MyServicesFragmentAdapter(getActivity(), listOfMyServiceDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyServicesDetails.setLayoutManager(layoutManager);
        rvMyServicesDetails.setItemAnimator(new DefaultItemAnimator());
        rvMyServicesDetails.setAdapter(myServicesFragmentAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMyServicesDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMyServicesDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddService) {
            goToAddService();
        }
    }

    private void goToAddService() {
        if (getActivity() != null) {
            Intent addServiceIntent = new Intent(getActivity(), AddServiceActivity.class);
            startActivity(addServiceIntent);
        }
    }
}