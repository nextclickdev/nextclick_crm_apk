package com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.approvedServiceFragmentResponse;

import com.nextclick.crm.models.responseModels.approvedDoctorsResponse.ApprovedDoctorDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ApprovedServicesResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LinkedList<ApprovedServiceDetails> listOfApprovedServiceDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<ApprovedServiceDetails> getListOfApprovedServiceDetails() {
        return listOfApprovedServiceDetails;
    }

    public void setListOfApprovedServiceDetails(LinkedList<ApprovedServiceDetails> listOfApprovedServiceDetails) {
        this.listOfApprovedServiceDetails = listOfApprovedServiceDetails;
    }
}
