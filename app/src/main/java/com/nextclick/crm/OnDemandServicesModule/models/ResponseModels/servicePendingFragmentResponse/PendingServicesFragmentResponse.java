package com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.servicePendingFragmentResponse;

import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse.ServicesAdminDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class PendingServicesFragmentResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LinkedList<PendingServiceDetails> listOfPendingServiceDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<PendingServiceDetails> getListOfPendingServiceDetails() {
        return listOfPendingServiceDetails;
    }

    public void setListOfPendingServiceDetails(LinkedList<PendingServiceDetails> listOfPendingServiceDetails) {
        this.listOfPendingServiceDetails = listOfPendingServiceDetails;
    }
}
