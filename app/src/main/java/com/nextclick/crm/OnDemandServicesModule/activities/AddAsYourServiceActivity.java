package com.nextclick.crm.OnDemandServicesModule.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.ServiceTypesResponse.GetServiceTypesResponse;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.ServiceTypesResponse.ServiceTypeDetails;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceDetailsResponse.GetServiceDetailsResponse;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceDetailsResponse.ServiceDetails;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.AppPermissions;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.apiCalls.AddAsYourServiceApiCall;
import com.nextclick.crm.apiCalls.GetServiceDetailsApiCall;
import com.nextclick.crm.apiCalls.GetServiceTypesApiCall;
import com.nextclick.crm.doctorsModule.activities.DaysNamesActivity;
import com.nextclick.crm.interfaces.DaysNameCallBack;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.ServiceTiming;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class AddAsYourServiceActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, HttpReqResCallBack, DaysNameCallBack {


    private LinearLayout llTimings;
    private Spinner spinnerServiceType;
    private ServiceDetails serviceDetails;
    private RelativeLayout rlImageLayout;
    private TextView tvSubmit, tvHolidays;
    private PreferenceManager preferenceManager;
    private BottomSheetDialog pickerBottomSheetDialog;
    private ImageView ivBackArrow, ivProfilePic, ivAddTimings;
    private EditText etName, etDescription, etServiceDuration, etFee, etDiscount;

    private ArrayList<String> listOfPhotoPaths;
    private LinkedList<Integer> listOfSelectedDayIds;
    private LinkedList<String> listOfSelectedDayNames;
    private LinkedList<Integer> listOfServiceTypeIds;
    private LinkedList<String> listOfServiceTypesNames;
    private LinkedHashMap<Integer, JSONObject> mapOfTimings;
    private LinkedList<ServiceTypeDetails> listOfServiceDetails;

    private String token = "";
    private String serviceId = "";
    private String serviceTypeName = "";
    private String selectedImageUrl = "";
    private String onDemandServiceID = "";

    private int count = 0;
    private int serviceTypeId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_as_your_service);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        prepareServiceDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.service_id)))
                serviceId = bundle.getString(getString(R.string.service_id));
            if (bundle.containsKey(getString(R.string.od_service_id)))
                onDemandServiceID = bundle.getString(getString(R.string.od_service_id));
        }
    }

    private void initializeUi() {
        etFee = findViewById(R.id.etFee);
        etName = findViewById(R.id.etName);
        tvSubmit = findViewById(R.id.tvSubmit);
        llTimings = findViewById(R.id.llTimings);
        tvHolidays = findViewById(R.id.tvHolidays);
        etDiscount = findViewById(R.id.etDiscount);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etServiceDuration = findViewById(R.id.etServiceDuration);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        ivAddTimings = findViewById(R.id.ivAddTimings);
        rlImageLayout = findViewById(R.id.rlImageLayout);
        etDescription = findViewById(R.id.etDescription);
        spinnerServiceType = findViewById(R.id.spinnerServiceType);
        preferenceManager = new PreferenceManager(this);
        listOfSelectedDayIds = new LinkedList<>();
        listOfServiceTypesNames = new LinkedList<>();

        UserData.getInstance().setContext(this);
        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
        mapOfTimings = new LinkedHashMap<>();
        count = 0;
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        tvHolidays.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        ivAddTimings.setOnClickListener(this);
        rlImageLayout.setOnClickListener(this);
        spinnerServiceType.setOnItemSelectedListener(this);
    }

    private void prepareDetails() {
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetServiceTypesApiCall.serviceCallToGetServiceTypes(this, null, null, token);
    }

    private void prepareServiceDetails() {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetServiceDetailsApiCall.serviceCallToGetServiceDetails(this, null, null, token, serviceId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.rlImageLayout:
                prepareImageDetails();
                break;
            case R.id.tvSubmit:
                prepareSubmitDetails();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            case R.id.tvHolidays:
                goToDayNames();
                break;
            case R.id.ivAddTimings:
                prepareAddTimingsDetails();
                break;
            default:
                break;
        }
    }

    private void prepareAddTimingsDetails() {
        count = count + 1;
        mapOfTimings.put(count, new JSONObject());
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_add_doctor_timings, null);

        TextView tvStartTime = view.findViewById(R.id.tvStartTime);
        TextView tvEndTime = view.findViewById(R.id.tvEndTime);
        tvStartTime.setTag(count);
        tvEndTime.setTag(count);
        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int startTimeTag = (int) v.getTag();
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddAsYourServiceActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                        tvStartTime.setText(time);
                        if (mapOfTimings.containsKey(startTimeTag)) {
                            JSONObject jsonObject = mapOfTimings.get(count);
                            if (jsonObject != null) {
                                try {
                                    jsonObject.put("start_time", time);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int endTimeTag = (int) v.getTag();
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(AddAsYourServiceActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                        tvEndTime.setText(time);
                        if (mapOfTimings.containsKey(endTimeTag)) {
                            JSONObject jsonObject = mapOfTimings.get(count);
                            if (jsonObject != null) {
                                try {
                                    jsonObject.put("end_time", time);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        llTimings.addView(view);
    }

    private void prepareImageDetails() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                showBottomSheetView();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }

    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(listOfPhotoPaths)
                .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle(getString(R.string.select_image))
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(true)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.ic_custom_camera)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickPhoto(this, Constants.PICK_GALLERY);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void showTimePicker(TextView clickedTextView) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                datetime.set(Calendar.MINUTE, minute);
                String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                clickedTextView.setText(time);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void goToDayNames() {
        Intent dayNamesIntent = new Intent(this, DaysNamesActivity.class);
        startActivity(dayNamesIntent);
    }

    private void prepareSubmitDetails() {

        String fee = etFee.getText().toString();
        String name = etName.getText().toString();
        String holidays = tvHolidays.getText().toString();
        String discount = etDiscount.getText().toString();
        String serviceDuration = etServiceDuration.getText().toString();
        String description = etDescription.getText().toString();

        if (!name.isEmpty()) {
            if (!description.isEmpty()) {
                if (!serviceDuration.isEmpty()) {
                    if (!fee.isEmpty()) {
                        if (!discount.isEmpty()) {
                            if (mapOfTimings.size() != 0) {
                                if (!holidays.isEmpty()) {
                                    if (serviceTypeId != -1) {
                                        if (!selectedImageUrl.isEmpty()) {
                                            String base64 = base64Converter();
                                            if (!base64.isEmpty()) {
                                                prepareAddAsYourServiceApiCall(name, description, serviceDuration, fee, discount, mapOfTimings, listOfSelectedDayIds, serviceTypeId, base64);
                                            }
                                        } else {
                                            Toast.makeText(this, getString(R.string.please_select_image), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(this, getString(R.string.please_select_speciality), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(this, getString(R.string.please_enter_holidays), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(this, getString(R.string.please_select_timings), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.please_enter_discount), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.please_enter_price), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.please_enter_service_duration), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_enter_service_description), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_service_name), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareAddAsYourServiceApiCall(String name, String description, String serviceDuration, String fee, String discount, LinkedHashMap<Integer, JSONObject> mapOfTimings, LinkedList<Integer> listOfSelectedDayIds, int serviceTypeID, String base64) {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        AddAsYourServiceApiCall.serviceCallForAddAsYourService(this, null, null, name, description, serviceDuration, fee, discount, mapOfTimings, listOfSelectedDayIds, serviceTypeID, base64, token, serviceId,onDemandServiceID);
    }


    private String base64Converter() {
        ivProfilePic.buildDrawingCache();
        Bitmap bitmap = ivProfilePic.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int viewId = parent.getId();
        if (viewId == R.id.spinnerServiceType) {
            serviceTypeName = spinnerServiceType.getSelectedItem().toString();
            int index = listOfServiceTypesNames.indexOf(serviceTypeName);
            if (index != -1) {
                serviceTypeId = listOfServiceTypeIds.get(index);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedDaysNames(LinkedList<Integer> listOfSelectedDayIds, LinkedList<String> listOfSelectedDayNames) {
        this.listOfSelectedDayIds = listOfSelectedDayIds;
        this.listOfSelectedDayNames = listOfSelectedDayNames;

        tvHolidays.setText(TextUtils.join(",", listOfSelectedDayNames));
        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SERVICE_TYPES:
                if (jsonResponse != null) {
                    GetServiceTypesResponse getServiceTypesResponse = new Gson().fromJson(jsonResponse, GetServiceTypesResponse.class);
                    if (getServiceTypesResponse != null) {
                        boolean status = getServiceTypesResponse.getStatus();
                        String message = getServiceTypesResponse.getMessage();
                        if (status) {
                            listOfServiceDetails = getServiceTypesResponse.getListOfServiceTypesDetails();
                            if (listOfServiceDetails != null) {
                                if (listOfServiceDetails.size() != 0) {
                                    listOfServiceTypeIds = new LinkedList<>();
                                    listOfServiceTypesNames = new LinkedList<>();
                                    for (int index = 0; index < listOfServiceDetails.size(); index++) {
                                        ServiceTypeDetails serviceTypeDetails = listOfServiceDetails.get(index);
                                        String name = serviceTypeDetails.getName();
                                        int specialityId = serviceTypeDetails.getId();
                                        listOfServiceTypesNames.add(name);
                                        listOfServiceTypeIds.add(specialityId);
                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddAsYourServiceActivity.this, android.R.layout.simple_spinner_item, listOfServiceTypesNames);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnerServiceType.setAdapter(adapter);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                //LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_GET_SERVICE_DETAILS:
                if (jsonResponse != null) {
                    GetServiceDetailsResponse getServiceDetailsResponse = new Gson().fromJson(jsonResponse, GetServiceDetailsResponse.class);
                    if (getServiceDetailsResponse != null) {
                        boolean status = getServiceDetailsResponse.getStatus();
                        String message = getServiceDetailsResponse.getMessage();
                        if (status) {
                            serviceDetails = getServiceDetailsResponse.getServiceDetails();
                            if (serviceDetails != null) {
                                String name = serviceDetails.getName();
                                selectedImageUrl = serviceDetails.getImage();
                                String description = serviceDetails.getDesc();
                                serviceTypeId = serviceDetails.getServiceCategoryID();
                                int serviceDuration = serviceDetails.getServiceDuration();
                                double fee = serviceDetails.getPrice();
                                double discount = serviceDetails.getDiscount();
                                LinkedList<ServiceTiming> listOfServiceTimings = serviceDetails.getListOfServiceTimings();
                                LinkedList<Integer> listOfHolidays = serviceDetails.getListOfHolidays();

                                etName.setText(name);
                                etDescription.setText(Html.fromHtml(description));

                                if (listOfServiceTypeIds != null) {
                                    if (listOfServiceTypeIds.size() != 0) {
                                        int index = listOfServiceTypeIds.indexOf(serviceTypeId);
                                        if (index != -1) {
                                            spinnerServiceType.setSelection(index);
                                        }
                                    }
                                }


                                etServiceDuration.setText(String.valueOf(serviceDuration));

                                etFee.setText(String.valueOf(fee));
                                etDiscount.setText(String.valueOf(discount));

                                listOfSelectedDayIds = new LinkedList<>();
                                listOfSelectedDayNames = new LinkedList<>();
                                if (listOfHolidays != null) {
                                    if (listOfHolidays.size() != 0) {
                                        for (int holidaysIndex = 0; holidaysIndex < listOfHolidays.size(); holidaysIndex++) {
                                            int holidayId = listOfHolidays.get(holidaysIndex);
                                            String holidayName = "";
                                            if (holidayId == 1) {
                                                holidayName = "Sunday";
                                            } else if (holidayId == 2) {
                                                holidayName = "Monday";
                                            } else if (holidayId == 3) {
                                                holidayName = "Tuesday";
                                            } else if (holidayId == 4) {
                                                holidayName = "Wednesday";
                                            } else if (holidayId == 5) {
                                                holidayName = "Thursday";
                                            } else if (holidayId == 6) {
                                                holidayName = "Friday";
                                            } else if (holidayId == 7) {
                                                holidayName = "Saturday";
                                            }
                                            listOfSelectedDayIds.add(holidayId);
                                            listOfSelectedDayNames.add(holidayName);
                                        }
                                        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
                                        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
                                        tvHolidays.setText(TextUtils.join(",", listOfSelectedDayNames));
                                    }
                                }

                                if (listOfServiceTimings != null) {
                                    if (listOfServiceTimings.size() != 0) {
                                        for (int index = 0; index < listOfServiceTimings.size(); index++) {
                                            ServiceTiming serviceTiming = listOfServiceTimings.get(index);
                                            prepareSetServiceTimings(serviceTiming);
                                        }
                                    }
                                }

                                if (selectedImageUrl != null) {
                                    if (!selectedImageUrl.isEmpty()) {
                                        Glide.with(this)
                                                .load(selectedImageUrl)
                                                .placeholder(R.drawable.ic_default_circle)
                                                .error(R.drawable.ic_default_circle)
                                                .into(ivProfilePic);
                                    } else {
                                        Glide.with(this)
                                                .load(R.drawable.ic_default_circle)
                                                .placeholder(R.drawable.ic_default_circle)
                                                .error(R.drawable.ic_default_circle)
                                                .into(ivProfilePic);
                                    }
                                } else {
                                    Glide.with(this)
                                            .load(R.drawable.ic_default_circle)
                                            .placeholder(R.drawable.ic_default_circle)
                                            .error(R.drawable.ic_default_circle)
                                            .into(ivProfilePic);
                                }

                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_ADD_AS_YOUR_SERVICE:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            preferenceManager.putBoolean(getString(R.string.refresh_services), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void prepareSetServiceTimings(ServiceTiming serviceTiming) {
        try {
            count = count + 1;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("start_time", serviceTiming.getStartTime());
            jsonObject.put("end_time", serviceTiming.getEndTime());
            mapOfTimings.put(count, jsonObject);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_add_doctor_timings, null);

            TextView tvStartTime = view.findViewById(R.id.tvStartTime);
            TextView tvEndTime = view.findViewById(R.id.tvEndTime);
            tvStartTime.setTag(count);
            tvEndTime.setTag(count);
            tvStartTime.setText(serviceTiming.getStartTime());
            tvEndTime.setText(serviceTiming.getEndTime());
            tvStartTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int startTimeTag = (int) v.getTag();
                    Calendar calendar = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(AddAsYourServiceActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                            Calendar datetime = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);
                            String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                            tvStartTime.setText(time);
                            if (mapOfTimings.containsKey(startTimeTag)) {
                                JSONObject jsonObject = mapOfTimings.get(count);
                                if (jsonObject != null) {
                                    try {
                                        jsonObject.put("start_time", time);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                }
            });

            tvEndTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int endTimeTag = (int) v.getTag();
                    Calendar calendar = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(AddAsYourServiceActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                            Calendar datetime = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);
                            String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                            tvEndTime.setText(time);
                            if (mapOfTimings.containsKey(endTimeTag)) {
                                JSONObject jsonObject = mapOfTimings.get(count);
                                if (jsonObject != null) {
                                    try {
                                        jsonObject.put("end_time", time);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                }
            });

            llTimings.addView(view);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        if (listOfPhotoPaths != null) {
                            if (listOfPhotoPaths.size() != 0) {
                                selectedImageUrl = listOfPhotoPaths.get(0);
                                Glide.with(this)
                                        .load(new File(selectedImageUrl))
                                        .placeholder(R.drawable.ic_default_circle)
                                        .error(R.drawable.ic_default_circle)
                                        .into(ivProfilePic);
                            }
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);
                        Glide.with(this)
                                .load(new File(selectedImageUrl))
                                .placeholder(R.drawable.ic_default_circle)
                                .error(R.drawable.ic_default_circle)
                                .into(ivProfilePic);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }
}
