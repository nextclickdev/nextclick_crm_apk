package com.nextclick.crm.OnDemandServicesModule.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.adapters.OnDemandServicesHomeAdapter;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.DashboardApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.dashboardCountsResponse.DashboardCountResponse;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class OnDemandServicesHomeActivity  extends AppCompatActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private RecyclerView rvOnDemandServicesDetails;

    private LinkedList<String> listOfCollectionNames;
    private LinkedList<Integer> listOfCollectionImage;

    private String serviceID = "";
    private String activeCount = "0";
    private String inActiveCount = "0";
    private String pendingBookingCount = "0";
    private String acceptedBookingCount = "0";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_on_demand_services_home);

        initializeUi();
        initializeListeners();
        getDashboardCounts();

    }

    private void initializeUi() {
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvOnDemandServicesDetails = findViewById(R.id.rvOnDemandServicesDetails);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void getDashboardCounts() {
        Intent intent = getIntent();
        PreferenceManager preferenceManager = new PreferenceManager(this);

        LoadingDialog.loadDialog(this);
        String token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        serviceID = intent.getStringExtra("Service_id");
        DashboardApiCall.serviceCallForDashboardCounts(this, null, null, token,serviceID);
    }

    private void prepareDetails() {
        listOfCollectionNames = new LinkedList<>();
        listOfCollectionImage = new LinkedList<>();

        listOfCollectionNames.add(getString(R.string.services));
        listOfCollectionNames.add(getString(R.string.bookings));

        listOfCollectionImage.add(R.drawable.ic_on_demand_services);
        listOfCollectionImage.add(R.drawable.ic_booking);

        initializeAdapter();
    }

    private void initializeAdapter() {
        OnDemandServicesHomeAdapter onDemandServicesHomeAdapter = new OnDemandServicesHomeAdapter(this, listOfCollectionNames, listOfCollectionImage,activeCount,inActiveCount,pendingBookingCount,acceptedBookingCount,serviceID);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvOnDemandServicesDetails.setLayoutManager(layoutManager);
        rvOnDemandServicesDetails.setItemAnimator(new DefaultItemAnimator());
        rvOnDemandServicesDetails.setAdapter(onDemandServicesHomeAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }
    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_DASHBOARD_COUNTS) {
            if (jsonResponse != null) {
                try {
                    DashboardCountResponse dashboardCountResponse = new Gson().fromJson(jsonResponse, DashboardCountResponse.class);
                    if (dashboardCountResponse != null) {
                        boolean status = dashboardCountResponse.getStatus();

                        if (status) {
                            activeCount = String.valueOf(dashboardCountResponse.getCountData().getActiveCount());
                            inActiveCount = String.valueOf(dashboardCountResponse.getCountData().getInActiveCount());
                            pendingBookingCount = String.valueOf(dashboardCountResponse.getCountData().getPendingBookingsCount());
                            acceptedBookingCount = String.valueOf(dashboardCountResponse.getCountData().getAcceptedBookingsCount());
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
            LoadingDialog.dialog.dismiss();
            prepareDetails();
        }
    }
}