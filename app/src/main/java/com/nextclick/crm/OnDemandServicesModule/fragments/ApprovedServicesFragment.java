package com.nextclick.crm.OnDemandServicesModule.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.activities.AddServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.adapters.ApprovedServicesFragmentAdapter;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.approvedServiceFragmentResponse.ApprovedServiceDetails;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.approvedServiceFragmentResponse.ApprovedServicesResponse;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.ApprovedServiceDetailsApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ApprovedServicesFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private RecyclerView rvApprovedDetails;
    private FloatingActionButton fabAddService;
    private PreferenceManager preferenceManager;

    private LinkedList<ApprovedServiceDetails> listOfApprovedServiceDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_approved_services, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeListeners() {
        fabAddService.setOnClickListener(this);
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddService = view.findViewById(R.id.fabAddService);
        rvApprovedDetails = view.findViewById(R.id.rvApprovedDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
        }
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        ApprovedServiceDetailsApiCall.serviceCallForApprovedServiceDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshServices = preferenceManager.getBoolean(getString(R.string.refresh_services));
        if (refreshServices) {
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_APPROVED_SERVICE_DETAILS) {
            if (jsonResponse != null) {
                ApprovedServicesResponse approvedServicesResponse = new Gson().fromJson(jsonResponse, ApprovedServicesResponse.class);
                if (approvedServicesResponse != null) {
                    boolean status = approvedServicesResponse.getStatus();
                    String message = approvedServicesResponse.getMessage();
                    if (status) {
                        listOfApprovedServiceDetails = approvedServicesResponse.getListOfApprovedServiceDetails();
                        if (listOfApprovedServiceDetails != null) {
                            if (listOfApprovedServiceDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        listIsEmpty();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        ApprovedServicesFragmentAdapter approvedServicesFragmentAdapter = new ApprovedServicesFragmentAdapter(getActivity(), listOfApprovedServiceDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvApprovedDetails.setLayoutManager(layoutManager);
        rvApprovedDetails.setItemAnimator(new DefaultItemAnimator());
        rvApprovedDetails.setAdapter(approvedServicesFragmentAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvApprovedDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvApprovedDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddService) {
            goToAddService();
        }
    }

    private void goToAddService() {
        if (getActivity() != null) {
            Intent addServiceIntent = new Intent(getActivity(), AddServiceActivity.class);
            startActivity(addServiceIntent);
        }
    }
}
