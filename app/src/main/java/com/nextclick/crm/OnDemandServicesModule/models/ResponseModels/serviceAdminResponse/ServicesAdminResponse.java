package com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse;

import com.nextclick.crm.models.responseModels.doctorsAdminResponse.DoctorAdminDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse.ServicesAdminDetails;

import java.util.LinkedList;

public class ServicesAdminResponse {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("http_code")
    @Expose
    private Integer httpCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LinkedList<ServicesAdminDetails> listOfServiceAdminDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(Integer httpCode) {
        this.httpCode = httpCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedList<ServicesAdminDetails> getListOfServiceAdminDetails() {
        return listOfServiceAdminDetails;
    }

    public void setListOfServiceAdminDetails(LinkedList<ServicesAdminDetails> listOfServiceAdminDetails) {
        this.listOfServiceAdminDetails = listOfServiceAdminDetails;
    }
}
