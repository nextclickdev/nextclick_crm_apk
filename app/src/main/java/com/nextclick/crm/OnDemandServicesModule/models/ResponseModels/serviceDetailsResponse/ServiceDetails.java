package com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceDetailsResponse;

import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.ServiceTiming;
import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.Speciality;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;

public class ServiceDetails {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("od_cat_id")
    @Expose
    private Integer serviceCategoryID;
    @SerializedName("service_duration")
    @Expose
    private Integer serviceDuration;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("holidays")
    @Expose
    private LinkedList<Integer> listOfHolidays;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("od_category")
    @Expose
    private ODCategory ODCategory;
    @SerializedName("service_timings")
    @Expose
    private LinkedList<ServiceTiming> listOfServiceTimings;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getServiceCategoryID() {
        return serviceCategoryID;
    }

    public void setServiceCategoryID(Integer serviceCategoryID) {
        this.serviceCategoryID = serviceCategoryID;
    }

    public Integer getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(Integer serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public LinkedList<Integer> getListOfHolidays() {
        return listOfHolidays;
    }

    public void setListOfHolidays(LinkedList<Integer> listOfHolidays) {
        this.listOfHolidays = listOfHolidays;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceDetailsResponse.ODCategory getODCategory() {
        return ODCategory;
    }

    public void setODCategory(com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceDetailsResponse.ODCategory ODCategory) {
        this.ODCategory = ODCategory;
    }

    public LinkedList<ServiceTiming> getListOfServiceTimings() {
        return listOfServiceTimings;
    }

    public void setListOfServiceTimings(LinkedList<ServiceTiming> listOfServiceTimings) {
        this.listOfServiceTimings = listOfServiceTimings;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
