package com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.servicePendingFragmentResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PendingServiceDetails {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("od_service_id")
    @Expose
    private Integer OnDemandServiceID;
    @SerializedName("od_cat_id")
    @Expose
    private Integer onDemandCategoryID;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("image")
    @Expose
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOnDemandServiceID() {
        return OnDemandServiceID;
    }

    public void setOnDemandServiceID(Integer onDemandServiceID) {
        OnDemandServiceID = onDemandServiceID;
    }

    public Integer getOnDemandCategoryID() {
        return onDemandCategoryID;
    }

    public void setOnDemandCategoryID(Integer onDemandCategoryID) {
        this.onDemandCategoryID = onDemandCategoryID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
