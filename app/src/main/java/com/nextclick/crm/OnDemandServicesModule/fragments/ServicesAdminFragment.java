package com.nextclick.crm.OnDemandServicesModule.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.OnDemandServicesModule.activities.AddServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.adapters.ServicesAdminFragmentAdapter;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse.ServicesAdminDetails;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse.ServicesAdminResponse;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.ServicesAdminDetailsApiCall;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ServicesAdminFragment  extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private FloatingActionButton fabAddService;
    private RecyclerView rvServicesAdminDetails;
    private PreferenceManager preferenceManager;

    private LinkedList<ServicesAdminDetails> listOfServiceAdminDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_services_admin_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddService = view.findViewById(R.id.fabAddService);
        rvServicesAdminDetails = view.findViewById(R.id.rvServicesAdminDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
        }
    }

    private void initializeListeners() {
        fabAddService.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        ServicesAdminDetailsApiCall.serviceCallForServicesAdminDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshServices = preferenceManager.getBoolean(getString(R.string.refresh_services));
        if (refreshServices) {
            preferenceManager.putBoolean(getString(R.string.refresh_services), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_SERVICE_ADMIN_DETAILS) {
            if (jsonResponse != null) {
                try {
                    ServicesAdminResponse servicesAdminResponse = new Gson().fromJson(jsonResponse, ServicesAdminResponse.class);
                    if (servicesAdminResponse != null) {
                        boolean status = servicesAdminResponse.getStatus();
                        String message = servicesAdminResponse.getMessage();
                        if (status) {
                            listOfServiceAdminDetails = servicesAdminResponse.getListOfServiceAdminDetails();
                            if (listOfServiceAdminDetails != null) {
                                if (listOfServiceAdminDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsFull();
                            }
                        } else {
                            listIsEmpty();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                    listIsEmpty();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        ServicesAdminFragmentAdapter servicesAdminFragmentAdapter = new ServicesAdminFragmentAdapter(getActivity(), listOfServiceAdminDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvServicesAdminDetails.setLayoutManager(layoutManager);
        rvServicesAdminDetails.setItemAnimator(new DefaultItemAnimator());
        rvServicesAdminDetails.setAdapter(servicesAdminFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvServicesAdminDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvServicesAdminDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddService) {
            goToAddService();
        }
    }

    private void goToAddService() {
        if (getActivity() != null) {
            Intent addServiceIntent = new Intent(getActivity(), AddServiceActivity.class);
            startActivity(addServiceIntent);
        }
    }
}