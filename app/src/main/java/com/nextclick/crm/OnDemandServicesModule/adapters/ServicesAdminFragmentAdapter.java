package com.nextclick.crm.OnDemandServicesModule.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.OnDemandServicesModule.activities.AddAsYourServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.activities.EditServiceActivity;
import com.nextclick.crm.OnDemandServicesModule.models.ResponseModels.serviceAdminResponse.ServicesAdminDetails;
import com.nextclick.crm.R;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class ServicesAdminFragmentAdapter extends RecyclerView.Adapter<ServicesAdminFragmentAdapter.ViewHolder> {

    private final Context context;
    private ServicesAdminDetails servicesAdminDetails;
    private final LinkedList<ServicesAdminDetails> listOfServiceAdminDetails;

    public ServicesAdminFragmentAdapter(Context context, LinkedList<ServicesAdminDetails> listOfServiceAdminDetails) {
        this.context = context;
        this.listOfServiceAdminDetails = listOfServiceAdminDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_services_admin_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ServicesAdminDetails servicesAdminDetails = listOfServiceAdminDetails.get(position);
        if (servicesAdminDetails != null) {
            String serviceName = servicesAdminDetails.getName();
            String serviceImage = servicesAdminDetails.getImage();
            String serviceDescription = servicesAdminDetails.getDesc();

            holder.tvServiceName.setText(serviceName);
            holder.tvDescription.setText(Html.fromHtml(serviceDescription));

            if (serviceImage != null) {
                if (!serviceImage.isEmpty()) {
                    Picasso.get()
                            .load(serviceImage)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivServicePic);
                } else {
                    Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivServicePic);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivServicePic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfServiceAdminDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivServicePic;
        private final TextView tvServiceName; /*tvQualification,*/ private final TextView tvDescription;
        private final TextView tvAddItAsYourService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivServicePic = itemView.findViewById(R.id.ivServicePic);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
           // tvQualification = itemView.findViewById(R.id.tvQualification);
            tvAddItAsYourService = itemView.findViewById(R.id.tvAddItAsYourService);

            itemView.setOnClickListener(this);
            tvAddItAsYourService.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            servicesAdminDetails = listOfServiceAdminDetails.get(getLayoutPosition());
            if (id == R.id.tvAddItAsYourService) {
                showAddItAsYourServiceAlertDialog();
            } else {
                int serviceId = servicesAdminDetails.getId();
                int ODServiceId = servicesAdminDetails.getOnDemandServiceID();
                Intent editService = new Intent(context, EditServiceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
                bundle.putString(context.getString(R.string.od_service_id), String.valueOf(ODServiceId));
                editService.putExtras(bundle);
                context.startActivity(editService);
            }
        }
    }


    private void showAddItAsYourServiceAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.do_you_want_to_add_this_as_your_service));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int serviceId = servicesAdminDetails.getId();
                int ODServiceID = servicesAdminDetails.getOnDemandServiceID();
                Intent addItAsYourService = new Intent(context, AddAsYourServiceActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.service_id), String.valueOf(serviceId));
                bundle.putString(context.getString(R.string.od_service_id), String.valueOf(ODServiceID));
                addItAsYourService.putExtras(bundle);
                context.startActivity(addItAsYourService);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
