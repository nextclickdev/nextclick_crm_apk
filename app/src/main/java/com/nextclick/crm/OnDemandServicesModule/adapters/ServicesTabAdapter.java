package com.nextclick.crm.OnDemandServicesModule.adapters;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.nextclick.crm.OnDemandServicesModule.fragments.ApprovedServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.MyServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.PendingServicesFragment;
import com.nextclick.crm.OnDemandServicesModule.fragments.ServicesAdminFragment;
import com.nextclick.crm.Utilities.TabInfo;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class ServicesTabAdapter implements TabLayout.OnTabSelectedListener {

    private final Context context;
    private final TabLayout tabLayout;
    private final int fragmentContainer;
    private final FragmentManager fragmentManager;
    private TextView tvSearch, tvClear, tvSavedFilter;
    private final ArrayList<TabInfo> mTabs = new ArrayList<>();
    private Fragment fragment;

    public ServicesTabAdapter(Context context, FragmentManager fragmentManager, TabLayout tabLayout, int fragmentContainer) {
        this.context = context;
        this.tabLayout = tabLayout;
        this.fragmentManager = fragmentManager;
        this.fragmentContainer = fragmentContainer;
        tabLayout.addOnTabSelectedListener(this);
    }

    public void addTab(TabLayout.Tab tabSpec, Class<?> clss, Bundle args) {
        String tag = (String) tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        tabLayout.addTab(tabSpec);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        String tabName = (String) tab.getTag();
        TabInfo info = mTabs.get(position);
        switch (position) {
            case 0:
                fragment = new ServicesAdminFragment();
                break;
            case 1:
               fragment = new MyServicesFragment();
                break;
            case 2:
                fragment = new ApprovedServicesFragment();
                break;
            case 3:
                fragment = new PendingServicesFragment();
                break;
            default:
                break;
        }

        fragmentManager.beginTransaction().replace(fragmentContainer, fragment).commit();

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}