package com.nextclick.crm.Config;

public interface Config {

    //public String BASE_URL = "http://apticks.com/nextclick_test/";

    //public String BASE_URL = "http://vendor.nextclick.in/";

    //Main live
//  public String BASE_URL ="https://app.nextclick.in/";//"https://nextclick.in/app/";

    //Main live Debug-Testing purpose
    //  public String BASE_URL ="http://app.nextclick.in/";//"https://nextclick.in/app/";
    //Main live Debug-Testing purpose

    //  public String BASE_URL ="http://debug.nextclick.in/";

    // public String BASE_URL ="http://debug.nextclick.in/";
    // Test
    String BASE_URL = "https://test.nextclick.in/";//"http://test.nextclick.in/";
//  String BASE_URL = "https://dbtest.nextclick.in/";

    // String BASE_URL="http://13.233.215.21/";//aws

    String LOGIN = BASE_URL + "/auth/api/auth/login";
    String REGISTER_USER = BASE_URL + "auth/api/auth/register";
    String USER_PROFILE = BASE_URL + "user/profile/me?intent=vendor";
    String USER_PROFILE_UPDATE = BASE_URL + "user/profile/update";
    String VERIFY_TOKEN = BASE_URL + "/auth/api/auth/verify";
    String FCM = BASE_URL + "general/api/fcm_notify/grant_fcm_permission";
    String PROFILE = BASE_URL + "user/master/profile/r";
    String FORGOT_PASSWORD = BASE_URL + "/auth/api/auth/forgot_password";
    String VENDOR_PROFILE = BASE_URL + "vendor/api/vendor/profile/r/";
    String SHOP_AVAILABLE = BASE_URL + "vendor/api/vendor/profile/availability";

    String INDIVIDUAL_VENDOR = BASE_URL + "/user/master/vendor/";//vendor_id needs to be passed

    /*Shop by category CRUD*/
    String SHOP_BY_CATEGORY_C = BASE_URL + "vendor/api/ecom/shop_by_category/c";
    String SHOP_BY_CATEGORY_R = BASE_URL + "vendor/api/ecom/shop_by_category/r/";//if single needed then it requires id
    String SHOP_BY_CATEGORY_U = BASE_URL + "vendor/api/ecom/shop_by_category/u";
    String SHOP_BY_CATEGORY_D = BASE_URL + "vendor/api/ecom/shop_by_category/d/";//id needed

    /*Menu CRUD*/
    String MENU_C = BASE_URL + "vendor/api/ecom/menus/c/";
    String MENU_R = BASE_URL + "vendor/api/ecom/menus/r/";//if single item needed the it reuires id
    String MENU_U = BASE_URL + "vendor/api/ecom/menus/u/";
    String MENU_D = BASE_URL + "vendor/api/ecom/menus/d/";///id needed

    String BRANDS_CREATE = BASE_URL + "vendor/api/ecom/brands/c/";

    /*Product CRUD*/
    String PRODUCT_C = BASE_URL + "vendor/api/ecom/products/c/";
    String PRODUCT_R = BASE_URL + "vendor/api/ecom/products/r/";//if single item needed the it reuires id
    String PRODUCT_U = BASE_URL + "vendor/api/ecom/products/u/";
    String PRODUCT_D = BASE_URL + "vendor/api/ecom/products/d/";///id needed

    /*Sections CRUD*/
    String SECTION_C = BASE_URL + "vendor/api/ecom/sections/c/";
    String SECTION_R = BASE_URL + "vendor/api/ecom/sections/r/";//if single item needed the it reuires id
    String SECTION_U = BASE_URL + "vendor/api/ecom/sections/u/";
    String SECTION_D = BASE_URL + "vendor/api/ecom/sections/d/";///id needed

    /*Section Item CRUD*/
    String SECTION_ITEM_C = BASE_URL + "vendor/api/ecom/section_items/c/";
    String SECTION_ITEM_R = BASE_URL + "vendor/api/ecom/section_items/r/";//if single item needed the it reuires id
    String SECTION_ITEM_U = BASE_URL + "vendor/api/ecom/section_items/u/";
    String SECTION_ITEM_D = BASE_URL + "vendor/api/ecom/section_items/d/";///id needed
    String CONTACT_FORM = BASE_URL + "general/api/support/customer_support";
    String CUSTOMER_SUPPORT_REQUESTS = BASE_URL + "general/api/support/customer_support_list?app_details_id=2";
    String CUSTMER_SUPPORR = "https://test.nextclick.in/general/api/support/customer_support_detail?support_id=29";
    String UPDATE_BANK_DETAILS = BASE_URL + "vendor/api/vendor/profile/u/bank_details";
    String UPDATE_PROFILE_DETAILS = BASE_URL + "vendor/api/vendor/profile/profile";
    String UPDATE_SOCIAL_MEDIA_LINKS = BASE_URL + "vendor/api/vendor/profile/social";
    String UPDATE_IMAGES = BASE_URL + "vendor/api/vendor/profile/cover_and_banner_images";
    String REMOVE_BANNER = BASE_URL + "vendor/api/vendor/profile/delete_banner";

    //Order
    String ORDERHISTORY = BASE_URL + "vendor/api/ecom/ecom_orders/vendor_orders";
    String ORDERDETAILS = BASE_URL + "vendor/api/ecom/ecom_orders/order_details";
    String ORDERACCEPT = BASE_URL + "vendor/api/ecom/ecom_orders/accept";
    String ORDERREJECT = BASE_URL + "vendor/api/ecom/ecom_order_reject";
    String OUTOFDELEVERY = BASE_URL + "vendor/api/ecom/ecom_orders/verify_out_for_delivery";
    // public String OUTOFDELEVERY = "http://192.168.29.65/nextclick/vendor/api/ecom/ecom_orders/verify_out_for_delivery";
    String EXTENDINGPREPARETIME = BASE_URL + "vendor/api/ecom/ecom_orders/extend_preparation_time";


    String ALL_ORDERS = BASE_URL + "vendor/api/ecom/orders/r/all";
    String UPCOMING_ORDERS = BASE_URL + "vendor/api/ecom/orders/r/upcoming";
    String PAST_ORDERS = BASE_URL + "vendor/api/ecom/orders/r/past";
    String CANCELLED_ORDERS = BASE_URL + "vendor/api/ecom/orders/r/cancelled";
    String REJECTED_ORDERS = BASE_URL + "vendor/api/ecom/orders/r/rejected";
    String SINGLE_ORDER = BASE_URL + "vendor/api/ecom/orders/r/all/";//id needed
    String REJECT_ORDER = BASE_URL + "vendor/api/ecom/orders/reject_order?order_id=";//id needed
    String ACCEPT_ORDER = BASE_URL + "vendor/api/ecom/orders/accept_order?order_id=";//id needed
    String OUT_FOR_DELVERY = BASE_URL + "vendor/api/ecom/orders/our_for_delivery?order_id=";//id needed

    /*Specialities CRUD*/
    String SPECIALITY_C = BASE_URL + "vendor/api/bookings/specialities/c/";
    String SPECIALITY_R = BASE_URL + "vendor/api/bookings/specialities/r/";//if single item needed the it reuires id
    String SPECIALITY_U = BASE_URL + "vendor/api/bookings/specialities/u/";
    String SPECIALITY_D = BASE_URL + "vendor/api/bookings/specialities/d/";///id needed

    /*Doctor CRUD*/
    String DOCTOR_C = BASE_URL + "vendor/api/bookings/doctors/c/";
    String DOCTOR_R = BASE_URL + "vendor/api/bookings/doctors/r/";//if single item needed the it reuires id
    String DOCTOR_U = BASE_URL + "vendor/api/bookings/doctors/u/";
    String DOCTOR_D = BASE_URL + "vendor/api/bookings/doctors/d/";///id needed

    /*On Demand Categories CRUD*/


    String ODC_C = BASE_URL + "vendor/api/bookings/on_demand_categories/c/";
    String ODC_R = BASE_URL + "vendor/api/bookings/on_demand_categories/r/";//if single item needed the it reuires id
    String ODC_U = BASE_URL + "vendor/api/bookings/on_demand_categories/u/";
    String ODC_D = BASE_URL + "vendor/api/bookings/on_demand_categories/d/";///id needed

    /*On Demand Services CRUD*/

    String ODS_C = BASE_URL + "vendor/api/bookings/on_demand_services/c/";
    String ODS_R = BASE_URL + "vendor/api/bookings/on_demand_services/r/";//if single item needed the it reuires id
    String ODS_U = BASE_URL + "vendor/api/bookings/on_demand_services/u/";
    String ODS_D = BASE_URL + "vendor/api/bookings/on_demand_services/d/";///id needed

    /*Leads*/
    String LEADS_R = BASE_URL + "vendor/api/vendor/leads/";


    String GET_STATISTICS = BASE_URL + "vendor/api/ecom/statistics/";


    String NOTIFICATIONS = BASE_URL + "general/api/fcm_notify/notifications/r";
    String NOTIFICATIONS_READ = BASE_URL + "general/api/fcm_notify/notifications/in_active/";
    String NOTIFICATIONS_COUNT = BASE_URL + "vendor/api/utility/notification_count/";

    String FAQ_LIST = BASE_URL + "general/api/master/faqs?app_id=2";
    String REMOVEtoken = BASE_URL + "general/api/fcm_notify/remove_fcm_permission";


    String VERIFY_SUBSCRIPTIONS = BASE_URL + "vendor/api/vendor/subscriptions/verify";
    String SUBSCRIPTIONS_DETAILS = BASE_URL + "vendor/api/vendor/subscriptions/all_plans";
    String DOCTOR_ADMIN_DETAILS = BASE_URL + "vendor/api/bookings/doctors/list/admin_doctors";
    String MY_DOCTOR_DETAILS = BASE_URL + "vendor/api/bookings/doctors/list/my_doctors";
    String APPROVED_DOCTOR_DETAILS = BASE_URL + "vendor/api/bookings/doctors/list/approved_doctors";
    String PENDING_DOCTOR_DETAILS = BASE_URL + "vendor/api/bookings/doctors/list/pending_doctors";

    String DOCTOR_DETAILS = BASE_URL + "vendor/api/bookings/doctors/doctor_details";
    String ADD_TO_MY_DOCTOR = BASE_URL + "vendor/api/bookings/doctors/add_to_my_doctors";
    String EDIT_DOCTOR_DETAILS = BASE_URL + "vendor/api/bookings/doctors/u";
    String DELETE_DOCTOR = BASE_URL + "vendor/api/bookings/doctors/d";


    String SERVICE_ADMIN_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/list/admin_service";
    String MY_SERVICE_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/list/my_service";
    String APPROVED_SERVICE_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/list/approved_service";
    String PENDING_SERVICE_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/list/pending_service";

    String SERVICE_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/service_details";
    String ADD_TO_MY_SERVICE = BASE_URL + "vendor/api/bookings/on_demand_services/add_to_my_services";
    String EDIT_SERVICE_DETAILS = BASE_URL + "vendor/api/bookings/on_demand_services/u";
    String DELETE_SERVICE = BASE_URL + "vendor/api/bookings/on_demand_services/d";
    String CREATE_SERVICE = BASE_URL + "vendor/api/bookings/on_demand_services/c";
    String SERVICE_CATEGORIES = BASE_URL + "vendor/api/bookings/get_od_categories";

    String DASHBOARD_COUNTS = BASE_URL + "vendor/api/bookings/bookings_dashboard/";
    String SERVICE_GET_VENDOR_SETTINGS = BASE_URL + "vendor/api/vendor/settings/r";
    String UPDATE_VENDOR_SETTINGS = BASE_URL + "vendor/api/vendor/settings/u";
    String SERVICE_BOOKINGS = BASE_URL + "vendor/api/bookings/booking/vendor_bookings";
    String SERVICE_BOOKING_DETAILS = BASE_URL + "vendor/api/bookings/booking/vendor_bookings/";
    String REJECT_BOOKING = BASE_URL + "vendor/api/bookings/booking/reject";
    String ACCEPT_BOOKING = BASE_URL + "vendor/api/bookings/booking/accept";
    String COMPLETE_BOOKING = BASE_URL + "vendor/api/bookings/booking/completed";


    String States = BASE_URL + "general/api/master/states/";
    String CATEGORY_LIST = BASE_URL + "general/api/master/categories/";
    String MainAppVendorRegistration = BASE_URL + "user/profile/manage";//executive/vendors/c
    String UPDATE_PROFILE = BASE_URL+"vendor/api/vendor/profile/profile";
    String ALLPRODUCTLIST = BASE_URL + "vendor/api/ecom/products/list";
    String PRODUCTS_HISTORY = BASE_URL+"vendor/api/ecom/vendor_product_search";
    String MYPRODUCTLIST = BASE_URL + "vendor/api/ecom/vendor_products/my_products";
    String ADDPRODUCTTOMYLIST = BASE_URL + "vendor/api/ecom/vendor_products/add_to_my_list";
    String PRODUCTDETAILS = BASE_URL + "vendor/api/ecom/vendor_products/vendor_product_details/";
    String UPDATEVARIENT = BASE_URL + "vendor/api/ecom/vendor_products/update_variant";
    String BULKUPDATEVARIENT = BASE_URL + "vendor/api/ecom/vendor_variants_bulk/update";
    String BRANDS = BASE_URL + "/general/api/master/categories/";
    String DELETEIMAGE = BASE_URL + "vendor/api/ecom/products/delete_image";
    String VENDOR_VARIANTS_BULK = BASE_URL + "vendor/api/ecom/vendor_variants_bulk/r";
    String VENDOR_PRODUCTS_COUNT = BASE_URL + "vendor/api/utility/products_count";


    String GET_TAXES = BASE_URL + "general/api/master/tax_types";
    String GET_TAX_BY_ID = BASE_URL + "general/api/master/tax_types/";

    String WALLETHISTORY = BASE_URL + "payment/api/payment/wallet";

    //Settings

    String SETTINGSREAD = BASE_URL + "user/ecom/stock_settings/r";
    String SETTINGSCREATE = BASE_URL + "user/ecom/stock_settings/c";
    String SETTINGSDELETE = BASE_URL + "user/ecom/stock_settings/d/";
    String SETTINGSUPDATE = BASE_URL + "user/ecom/stock_settings/u";

    //Support

    String SUPPORTCREATE = BASE_URL + "general/api/support/support_queries/c";
    String REQUESTTYPE = BASE_URL + "user/ecom/request_type/r";
    String SUPPORTLIST = BASE_URL + "general/api/support/support_queries/r";
    String SUPPORTUODATE = BASE_URL + "general/api/support/support_queries/u";

    String SEND_OTP = BASE_URL + "auth/api/auth/otp";//AUthor-sunil//"auth/api/auth/otp_gen";
    String VERIFY_OTP = BASE_URL + "auth/api/auth/validate_otp";//AUthor-sunil// "auth/api/auth/verify_otp";

    //terms and conditions
    String GET_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/termsconditions";
    String ACCEPT_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/accept_tc";
    String VALIDATE_TERMSCONDITIONS = BASE_URL + "/general/api/terms_conditions/validate_user";

    //ads/promotions
    String CREATE_PROMOTION_BANNERS = BASE_URL + "/promos/api/promotion_banners/manage_promotion_banners/c";
    String UPDATE_PROMOTION_BANNERS = BASE_URL + "/promos/api/promotion_banners/manage_promotion_banners/u";
    String DELETE_PROMOTION_BANNERS = BASE_URL + "/promos/api/promotion_banners/manage_promotion_banners/d";
    String GET_PROMOTION_BANNERS = BASE_URL + "/promos/api/promotion_banners/manage_promotion_banners/r";
    String GET_SHOP_BY_CATEGORIES = BASE_URL + "/vendor/api/ecom/shop_by_category/r";
    String JOIN_PROMOTION = BASE_URL + "/promos/api/promotion_banners/join_the_promotion";
    String GET_PROMOTION_HISTORY = BASE_URL + "/promos/api/promotion_banners/manage_promotion_banners/history";
    String GET_BANNER_POSITIONS = BASE_URL + "/promos/api/promotion_banners/banner_positions";
    String GET_CONTENT_TYPES = BASE_URL + "/promos/api/promotion_banners/content_types_list";
    String GET_DISCOUNT_TYPES = BASE_URL + "/promos/api/promotion_banners/discount_types";
    String GET_BANNER_AVAILABILITY = BASE_URL + "/promos/api/promotion_banners/banner_availability_check";
    String GET_BANNERS_TEMPLATES = BASE_URL + "promos/api/promotion_banner_images/promotion_banner_images/r";


    String URL_States = BASE_URL + "general/api/master/states/";
    String REVIEW_CREATION = BASE_URL + "general/api/master/ratings/c";
    String REVIEW_RETRIEVAL = BASE_URL + "general/api/master/ratings/r";

    int manageaccount_service_id = 2;
    int lead_management_service_id = 4;

    //Rewards
    String GET_REWARDS_LIST = BASE_URL + "/promos/api/promotion_codes/promocodes_list";
    String PROMOCODE_CREATE = BASE_URL + "/promos/api/promotion_codes/promotion_codes/c";
    String PROMOCODE_UPDATE = BASE_URL + "/promos/api/promotion_codes/promotion_codes/u";
    String PROMOCODE_DELETE = BASE_URL + "/promos/api/promotion_codes/promotion_codes/d";

    String SHOPBYCATEGORY = BASE_URL + "/promos/api/promotion_codes/shop_by_category";
    String SCARCHCARDS = BASE_URL + "/promos/api/promotion_codes/scratchcards";


    //Subcriptions
    String LIST_SUBCRIPTIONS = BASE_URL + "vendor/api/subscriptions/list_packages";
    String OLD_SUBCRIPTIONS = BASE_URL + "vendor/api/subscriptions/vendor_package";
    String CHECK_SUBCRIPTIONS = BASE_URL + "vendor/api/subscriptions/validity_vendor_package";
    String Buy_SUBCRIPTIONS = BASE_URL + "vendor/api/subscriptions/payment_status/";
    String UPGRADABLE_VENDOR_SUBCRIPTIONS = BASE_URL + "vendor/api/subscriptions/upgradable_vendor_packages";


    //Reports
    String GET_REPORTS = BASE_URL + "/vendor/api/vendor/sales_reports";

    String GET_DELIVERY_BOY_LOCATION = BASE_URL + "delivery/api/delivery/current_location/get";


    String GET_BANKS = BASE_URL + "payment/api/payment/banks";//get
    String PAYMENT_LINK = BASE_URL + "payment/api/payment/create_payment_link";//post


    //Returns

    String GET_REURNS_DATA = BASE_URL + "/general/api/return_policies/return_policies/"; //get

    String URL_GET_SUBSCRIPTION_SETTINGS = BASE_URL + "vendor/api/subscriptions/my_package";
    String URL_GET_SUBSCRIPTION_PACKAGES = BASE_URL + "vendor/api/subscriptions/package_features";


    String GET_BANK_DETAILS = BASE_URL + "general/api/master/bankdetails";

    String POST_MANUAL_PAYMENT = BASE_URL + "general/api/master/manualpayment";


    //  public String PLACES_API_KEY = "AIzaSyCALiS-pCxXQuxz2kuYuiAoUoL49dmiPjo";
    String PLACES_API_KEY = "AIzaSyD_GskdsJWOwql0Q0QNxWiqVSKzqgqOvYk";

}