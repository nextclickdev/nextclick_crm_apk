package com.nextclick.crm.reports;

public class OrderReportObject {

    String received,accepted,preparing,outForDelivery,rejected,cancelled;

    public void setReceived(String received) {
        this.received=received;
    }
    public String getReceived(){return  received;}

    public void setAccepted(String accepted) {
        this.accepted=accepted;
    }
    public String getAccepted(){return  accepted;}

    public void setPreparing(String preparing) {
        this.preparing=preparing;
    }
    public String getPreparing(){return  preparing;}

    public void setOutForDelivery(String outForDelivery) {
        this.outForDelivery=outForDelivery;
    }
    public String getOutForDelivery(){return  outForDelivery;}

    public void setRejected(String rejected) {
        this.rejected=rejected;
    }
    public String getRejected(){return  rejected;}

    public void setCancelled(String cancelled) {
        this.cancelled=cancelled;
    }
    public String getCancelled(){return  cancelled;}
}
