package com.nextclick.crm.reports;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.MyPromotionsActivity;
import com.nextclick.crm.ShopNowModule.Models.PromotionObject;
import com.nextclick.crm.Utilities.PreferenceManager;

import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.orders.OrderAdapter;
import com.nextclick.crm.orders.model.OrderHistoryModel;
import com.nextclick.crm.orders.model.OrderStatus;
import com.nextclick.crm.orders.model.PaymentMethod;
import com.nextclick.crm.orders.model.PaymentModel;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ReportsActivity extends AppCompatActivity implements View.OnClickListener{

    String[] info = {"Jan", "Feb", "March", "April", "May", "June"};
    String SelectedPaymentType= CATERORY2;
    public static final String CATERORY1 = "CATERORY1";
    public static final String CATERORY2 = "CATERORY2";
    private TextView start_date,end_date,tv_get;
    ImageView order_back_image_;
    private Context mContext;
    private PreferenceManager preferenceManager;
    private CustomDialog mCustomDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);
        getSupportActionBar().hide();
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to reportss activity");
        }
        orderslist = new ArrayList<>();
        mContext=this;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog=new CustomDialog(ReportsActivity.this);
        initPieChart();
        intBarchart();

        start_date = findViewById(R.id.start_date);
        end_date = findViewById(R.id.end_date);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        tv_get= findViewById(R.id.tv_get);
        tv_get.setOnClickListener(this);
        order_back_image_= findViewById(R.id.order_back_image_);
        order_back_image_.setOnClickListener(this);

        LinearLayout layout_report1= findViewById(R.id.layout_report1);
        LinearLayout  layout_report2= findViewById(R.id.layout_report2);

        TextView tv_header1 = findViewById(R.id.tv_header1);
        TextView tv_header2 = findViewById(R.id.tv_header2);
        LinearLayout layout_header1= findViewById(R.id.layout_header1);
        LinearLayout  layout_header2= findViewById(R.id.layout_header2);
        layout_header1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= CATERORY2)
                {
                    SelectedPaymentType= CATERORY2;
                    layout_header1.setBackgroundColor(0XFF660033);
                    tv_header1.setTextColor(0XFFFFFFFF);


                    layout_header2.setBackgroundColor(0XFFCDCDCD);
                    tv_header2.setTextColor(0XFF000000);

                    layout_report2.setVisibility(View.GONE);
                    layout_report1.setVisibility(View.VISIBLE);

                }
            }
        });
        layout_header2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(SelectedPaymentType!= CATERORY1)
                {
                    SelectedPaymentType= CATERORY1;
                    layout_header2.setBackgroundColor(0XFF660033);
                    tv_header2.setTextColor(0XFFFFFFFF);

                    layout_header1.setBackgroundColor(0XFFCDCDCD);
                    tv_header1.setTextColor(0XFF000000);

                    layout_report2.setVisibility(View.VISIBLE);
                    layout_report1.setVisibility(View.GONE);
                }
            }
        });

        // getReports("","");

    }

    private void initPieChart() {
        PieChart pieChart = findViewById(R.id.piechart);
        pieChart.setUsePercentValues(true);
        pieChart.setExtraOffsets(25, 5, 25, 0);

        pieChart.setCenterText("Vendor Sales Report");

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(30f);

        pieChart.setEntryLabelColor(Color.BLACK);

        List<PieEntry> yvalues = new ArrayList<>();
        yvalues.add(new PieEntry(10f, info[0]));
        yvalues.add(new PieEntry(10f, info[1]));
        yvalues.add(new PieEntry(10f, info[2]));
        yvalues.add(new PieEntry(10f, info[3]));
        yvalues.add(new PieEntry(30f, info[4]));
        yvalues.add(new PieEntry(30f, info[5]));

        PieDataSet dataSet = new PieDataSet(yvalues, "");
        dataSet.setSliceSpace(3f);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new PercentFormatter());

        pieChart.setData(data);

        pieChart.setEntryLabelTextSize(13);

        int[] colors = {Color.RED, Color.rgb(255, 128, 0), Color.YELLOW, Color.GREEN, Color.CYAN, Color.BLUE, Color.MAGENTA};
        dataSet.setColors(ColorTemplate.createColors(colors));


        Description d = new Description();
        d.setTextSize(16);
        d.setPosition(65, 50);

        d.setTextAlign(Paint.Align.LEFT);
        d.setText("");//Orders
        pieChart.setDescription(d);


        pieChart.setTransparentCircleRadius(30f);
        pieChart.setHoleRadius(30f);

        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        pieChart.animateXY(1500, 1500);

        dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(1.2f);
        dataSet.setValueLinePart2Length(0.4f);
    }

    // variable for our bar chart
    BarChart barChart;

    // variable for our bar data.
    BarData barData;

    // variable for our bar data set.
    BarDataSet barDataSet;

    // array list for storing entries.
    ArrayList barEntriesArrayList;
    private void intBarchart() {
        barChart = findViewById(R.id.chart);

        getBarEntries();

        // creating a new bar data set.
        barDataSet = new BarDataSet(barEntriesArrayList, "Monthly Sales");

        // creating a new bar data and
        // passing our bar data set.
        barData = new BarData(barDataSet);

        // below line is to set data
        // to our bar chart.
        barChart.setData(barData);

        // adding color to our bar data set.
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);

        // setting text color.
        barDataSet.setValueTextColor(Color.BLACK);

        // setting text size
        barDataSet.setValueTextSize(16f);
        barChart.getDescription().setEnabled(false);
    }

    private void getBarEntries() {
        // creating a new array list
        barEntriesArrayList = new ArrayList<>();

        // adding new entry to our array list with bar
        // entry and passing x and y axis value to it.
        barEntriesArrayList.add(new BarEntry(1f, 4));
        barEntriesArrayList.add(new BarEntry(2f, 6));
        barEntriesArrayList.add(new BarEntry(3f, 8));
        barEntriesArrayList.add(new BarEntry(4f, 2));
        barEntriesArrayList.add(new BarEntry(5f, 4));
        barEntriesArrayList.add(new BarEntry(6f, 1));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_get:
                // getReports(startdate,enddate);
                break;
            case R.id.order_back_image_:
                finish();
                break;
            case R.id.start_date:
                datepicker(0);
                break;
            case R.id.end_date:
                if (start_date.getText().toString() == null || start_date.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please enter the start date.", Toast.LENGTH_SHORT).show();
                } else {
                    datepicker(1);
                }
        }
    }

    List<PromotionObject> orderslist;
    private void getReports(String startdate, String enddate) {
        orderslist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", startdate);
        uploadMap.put("end_date", enddate);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+ json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_REPORTS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONArray jsonArray1=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray1.length();i++){
                                    JSONObject orderobject=jsonArray1.getJSONObject(i);
                                    OrderHistoryModel orderHistoryModel=new OrderHistoryModel();

                                    // orderslist.add(orderHistoryModel);
                                }
                            }

                        } catch (JSONException e) {
                            //  Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="",startdate="",enddate="";
    String year,month,day;
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        start_date.setText(day+"-"+month+"-"+year);
        end_date.setText(day+"-"+month+"-"+year);
        startdate=year+"-"+month+"-"+day;
        enddate=year+"-"+month+"-"+day;

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //  System.out.println("aaaaaa currentdate  "+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+"  "+c.get(Calendar.YEAR));
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    startdate=fromyear+"-"+frommonth+"-"+fromday;
                    //   System.out.println("aaaaaaaa  startdate   "+startdate);
                    //start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    start_date.setText(""+startdate);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    enddate=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+enddate);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }
}