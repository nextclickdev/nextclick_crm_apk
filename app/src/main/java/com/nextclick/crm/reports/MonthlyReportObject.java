package com.nextclick.crm.reports;

import com.nextclick.crm.orders.model.OrderStatus;
import com.nextclick.crm.orders.model.PaymentModel;

public class MonthlyReportObject {

    String month,year,amount,week;

    public void setMonth(String month) {
        this.month=month;
    }
    public String getMonth(){return  month;}

    public void setYear(String year) {
        this.year=year;
    }
    public String getYear(){return  year;}

    public void setAmount(String amount) {
        this.amount=amount;
    }
    public String getAmount(){return  amount;}

    public void setWeek(String week) {
        this.week=week;
    }
    public String getWeek(){return  week;}
}
