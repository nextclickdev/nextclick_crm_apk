package com.nextclick.crm.doctorsModule.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.AddAsYourDoctorActivity;
import com.nextclick.crm.doctorsModule.activities.EditDoctorActivity;
import com.nextclick.crm.models.responseModels.doctorsAdminResponse.DoctorAdminDetails;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class DoctorsAdminFragmentAdapter extends RecyclerView.Adapter<DoctorsAdminFragmentAdapter.ViewHolder> {

    private final Context context;
    private DoctorAdminDetails doctorAdminDetails;
    private final LinkedList<DoctorAdminDetails> listOfDoctorAdminDetails;

    public DoctorsAdminFragmentAdapter(Context context, LinkedList<DoctorAdminDetails> listOfDoctorAdminDetails) {
        this.context = context;
        this.listOfDoctorAdminDetails = listOfDoctorAdminDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_doctors_admin_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DoctorAdminDetails doctorAdminDetails = listOfDoctorAdminDetails.get(position);
        if (doctorAdminDetails != null) {
            String doctorName = doctorAdminDetails.getName();
            String doctorImage = doctorAdminDetails.getImage();
            String doctorDescription = doctorAdminDetails.getDesc();
            String doctorQualification = doctorAdminDetails.getQualification();

            holder.tvDoctorName.setText(doctorName);
            holder.tvDescription.setText(doctorDescription);
            holder.tvQualification.setText(doctorQualification);

            if (doctorImage != null) {
                if (!doctorImage.isEmpty()) {
                    Picasso.get()
                            .load(doctorImage)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivDoctorPic);
                } else {
                    Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivDoctorPic);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivDoctorPic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfDoctorAdminDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivDoctorPic;
        private final TextView tvDoctorName;
        private final TextView tvQualification;
        private final TextView tvDescription;
        private final TextView tvAddAsYourDoctor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivDoctorPic = itemView.findViewById(R.id.ivDoctorPic);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvQualification = itemView.findViewById(R.id.tvQualification);
            tvAddAsYourDoctor = itemView.findViewById(R.id.tvAddAsYourDoctor);

            itemView.setOnClickListener(this);
            tvAddAsYourDoctor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            doctorAdminDetails = listOfDoctorAdminDetails.get(getLayoutPosition());
            if (id == R.id.tvAddAsYourDoctor) {
                showAddAsYourDoctorAlertDialog();
            } else {
                int doctorId = doctorAdminDetails.getId();
                int hospitalDoctorId = doctorAdminDetails.getHospDoctorId();
                Intent addAsYourDoctor = new Intent(context, EditDoctorActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.doctor_id), String.valueOf(doctorId));
                bundle.putString(context.getString(R.string.hospital_doctor_id), String.valueOf(hospitalDoctorId));
                addAsYourDoctor.putExtras(bundle);
                context.startActivity(addAsYourDoctor);
            }
        }
    }


    private void showAddAsYourDoctorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.do_you_want_to_add_him_as_your_doctor));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int doctorId = doctorAdminDetails.getId();
                int hospitalDoctorId = doctorAdminDetails.getHospDoctorId();
                Intent addAsYourDoctor = new Intent(context, AddAsYourDoctorActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.doctor_id), String.valueOf(doctorId));
                bundle.putString(context.getString(R.string.hospital_doctor_id), String.valueOf(hospitalDoctorId));
                addAsYourDoctor.putExtras(bundle);
                context.startActivity(addAsYourDoctor);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
