package com.nextclick.crm.doctorsModule.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.BookingAcceptApiCall;
import com.nextclick.crm.apiCalls.BookingCompleteApiCall;
import com.nextclick.crm.apiCalls.BookingRejectApiCall;
import com.nextclick.crm.apiCalls.GetBookingDetailsApiCall;
import com.nextclick.crm.doctorsModule.adapter.BookingDetailsAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingDetailsResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class BookingDetailsActivity extends AppCompatActivity implements View.OnClickListener, HttpReqResCallBack {

    private ImageView ivBackArrow;
    private CardView cvAccept, cvReject, cvStatus;
    private RecyclerView rvBookingsDetails;
    private TextView tvError, tvTrackID, tvBookingsCount, tvStatus, tvCreatedAt, tvPrice, tvDiscount, tvTotalPrice, tvBookingStatus;

    private String token = "";
    private String trackID = "";
    private String bookingId = "";
    private String createdUserId = "";

    private PreferenceManager preferenceManager;
    private BookingDetailsResponse.ServiceItem serviceItem;
    private BookingDetailsResponse.BookingsData bookingsData;
    private LinkedList<BookingDetailsResponse.BookingItemsDetails> listOfBookingItemDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_booking_details);
        getIntentDetails();
        initializeUi();
        initializeListeners();
        getBookingDetails();
    }

    private void getIntentDetails() {
        Intent intent = getIntent();
        bookingId = intent.getStringExtra(getString(R.string.booking_id));
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        tvStatus = findViewById(R.id.tvStatus);
        tvPrice = findViewById(R.id.tvPrice);
        cvAccept = findViewById(R.id.cvAccept);
        cvReject = findViewById(R.id.cvReject);
        cvStatus = findViewById(R.id.cvStatus);
        tvTrackID = findViewById(R.id.tvTrackID);
        tvDiscount = findViewById(R.id.tvDiscount);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tvCreatedAt = findViewById(R.id.tvCreatedAt);
        tvTotalPrice = findViewById(R.id.tvTotalPrice);
        tvBookingsCount = findViewById(R.id.tvBookingsCount);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        rvBookingsDetails = findViewById(R.id.rvBookingsDetails);

        preferenceManager = new PreferenceManager(this);
    }

    private void initializeListeners() {
        cvAccept.setOnClickListener(this);
        cvReject.setOnClickListener(this);
        cvStatus.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);

    }
    @Override
    public void onResume() {
        super.onResume();
        boolean refreshBookings = preferenceManager.getBoolean(getString(R.string.refresh_bookings));
        if (refreshBookings) {
            preferenceManager.putBoolean(getString(R.string.refresh_bookings), false);
            getBookingDetails();
        }
    }

    private void getBookingDetails() {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetBookingDetailsApiCall.serviceCallForBookingsDetails(this, null, null, token, bookingId);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_BOOKING_DETAILS:
                if (jsonResponse != null) {
                    try {
                        BookingDetailsResponse bookingsListResponse = new Gson().fromJson(jsonResponse, BookingDetailsResponse.class);
                        if (bookingsListResponse != null) {
                            boolean status = bookingsListResponse.getStatus();
                            String message = bookingsListResponse.getMessage();
                            if (status) {
                                bookingsData = bookingsListResponse.getBookingsData();
                                listOfBookingItemDetails = bookingsData.getListOfBookingItemDetails();
                                setBookingDetails();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            listIsFull();
                        }
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        listIsEmpty();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_REJECT_BOOKING:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            preferenceManager.putBoolean(getString(R.string.refresh_bookings), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
            case Constants.SERVICE_CALL_TO_ACCEPT_BOOKING:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            preferenceManager.putBoolean(getString(R.string.refresh_bookings), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_COMPLETE_BOOKING:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                            preferenceManager.putBoolean(getString(R.string.refresh_bookings), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
            default:
                break;
        }
    }

    private void setBookingDetails() {
        trackID = bookingsData.getTrackId();
        String bookingCount = String.valueOf(listOfBookingItemDetails.size());
        String status = bookingsData.getBookingStatus();
        String createdAt = bookingsData.getCreatedAt();
        String price = bookingsData.getSubTotal();
        String discount = bookingsData.getDiscount();
        String total = bookingsData.getTotal();
        createdUserId = bookingsData.getCreatedUserId();

        tvTrackID.setText(trackID);
        tvBookingsCount.setText(bookingCount);
        tvStatus.setText(status);
        tvCreatedAt.setText(createdAt);
        tvPrice.setText(String.format("₹%s", price));
        tvDiscount.setText(String.format("-₹%s", discount));
        tvTotalPrice.setText(String.format("₹%s", total));
        cvAccept.setVisibility(View.GONE);
        cvReject.setVisibility(View.GONE);
        cvStatus.setVisibility(View.GONE);

        if(status.equalsIgnoreCase("0")){
            tvStatus.setText(getString(R.string.cancelled));
            tvStatus.setTextColor(Color.parseColor("#B22222"));
            //tvBookingStatus.setText(getString(R.string.cancelled));
        }else if(status.equalsIgnoreCase("1")){
            tvStatus.setText(getString(R.string.received));
            tvStatus.setTextColor(Color.parseColor("#4682B4"));
            cvAccept.setVisibility(View.VISIBLE);
            cvReject.setVisibility(View.VISIBLE);
            cvStatus.setVisibility(View.GONE);
        }else if(status.equalsIgnoreCase("2")){
            tvStatus.setText(getString(R.string.accepted));
            tvStatus.setTextColor(Color.parseColor("#228B22"));
            cvStatus.setVisibility(View.VISIBLE);
        }else if(status.equalsIgnoreCase("3")){
            tvStatus.setText(getString(R.string.servicing));
            tvStatus.setTextColor(Color.parseColor("#803F48A7"));
            tvBookingStatus.setText(getString(R.string.servicing));
        }else if(status.equalsIgnoreCase("4")){
            tvStatus.setText(getString(R.string.completed));
            tvStatus.setTextColor(Color.parseColor("#228B22"));
            tvBookingStatus.setText(getString(R.string.completed));
        }else if(status.equalsIgnoreCase("5")){
            tvStatus.setText(getString(R.string.rejected));
            tvStatus.setTextColor(Color.parseColor("#B22222"));
            tvBookingStatus.setText(getString(R.string.rejected));
        }else{
            tvStatus.setText(getString(R.string.na));
            tvStatus.setTextColor(Color.parseColor("#B22222"));
            tvBookingStatus.setText(getString(R.string.na));
        }

    }


    private void initializeAdapter() {
        BookingDetailsAdapter bookingDetailsAdapter = new BookingDetailsAdapter(this, listOfBookingItemDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvBookingsDetails.setLayoutManager(layoutManager);
        rvBookingsDetails.setItemAnimator(new DefaultItemAnimator());
        rvBookingsDetails.setAdapter(bookingDetailsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvBookingsDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvBookingsDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.cvAccept:
                LoadingDialog.loadDialog(this);
                BookingAcceptApiCall.serviceCallForAcceptBooking(this, null, null, token, bookingId,trackID,createdUserId);
                break;
            case R.id.cvReject:
                LoadingDialog.loadDialog(this);
                BookingRejectApiCall.serviceCallForRejectBooking(this, null, null, token, bookingId,trackID,createdUserId);
                break;
            case R.id.cvStatus:
                LoadingDialog.loadDialog(this);
                BookingCompleteApiCall.serviceCallForCompleteBooking(this, null, null, token, bookingId,trackID,createdUserId);
                break;
            default:
                break;
        }
    }
}
