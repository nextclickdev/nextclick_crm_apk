package com.nextclick.crm.doctorsModule.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.doctorsModule.adapter.DaysNamesAdapter;
import com.nextclick.crm.interfaces.CloseCallBack;

import java.util.LinkedList;

public class DaysNamesActivity extends AppCompatActivity implements View.OnClickListener, CloseCallBack {

    private TextView tvSave;
    private ImageView ivBackArrow;
    private RecyclerView rvDayNames;

    private LinkedList<String> listOfDayNames;
    private LinkedList<Integer> listOfDayNamesIds;
    private LinkedList<Integer> listOfSelectedDayIds;
    private LinkedList<String> listOfSelectedDayNames;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_day_names);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
    }

    private void getDataFromIntent() {
        listOfSelectedDayIds = UserData.getInstance().getListOfSelectedDayIds();
        listOfSelectedDayNames = UserData.getInstance().getListOfSelectedDayNames();
    }

    private void initializeUi() {
        tvSave = findViewById(R.id.tvSave);
        rvDayNames = findViewById(R.id.rvDayNames);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void prepareDetails() {
        listOfDayNames = new LinkedList<>();
        listOfDayNamesIds = new LinkedList<>();

        listOfDayNames.add("Sunday");
        listOfDayNames.add("Monday");
        listOfDayNames.add("Tuesday");
        listOfDayNames.add("Wednesday");
        listOfDayNames.add("Thursday");
        listOfDayNames.add("Friday");
        listOfDayNames.add("Saturday");

        listOfDayNamesIds.add(1);
        listOfDayNamesIds.add(2);
        listOfDayNamesIds.add(3);
        listOfDayNamesIds.add(4);
        listOfDayNamesIds.add(5);
        listOfDayNamesIds.add(6);
        listOfDayNamesIds.add(7);

        initializeAdapter();
    }

    private void initializeAdapter() {
        DaysNamesAdapter daysNamesAdapter = new DaysNamesAdapter(this, listOfDayNames, listOfDayNamesIds, listOfSelectedDayIds, listOfSelectedDayNames, tvSave);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvDayNames.setItemAnimator(new DefaultItemAnimator());
        rvDayNames.setLayoutManager(layoutManager);
        rvDayNames.setAdapter(daysNamesAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }

    @Override
    public void close() {
        finish();
    }
}
