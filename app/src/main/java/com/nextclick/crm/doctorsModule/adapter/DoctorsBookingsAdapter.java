package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.BookingDetailsActivity;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingDetails;

import java.util.LinkedList;

public class DoctorsBookingsAdapter  extends RecyclerView.Adapter<DoctorsBookingsAdapter.ViewHolder> {

    private final Context context;
    private BookingDetails bookingDetails;
    private final LinkedList<BookingDetails> listOfBookings;

    public DoctorsBookingsAdapter(Context context, LinkedList<BookingDetails> listOfBookings) {
        this.context = context;
        this.listOfBookings = listOfBookings;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_doctors_booking_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BookingDetails bookingDetails = listOfBookings.get(position);
        if (bookingDetails != null) {

            String trackID = bookingDetails.getTrackID();
            String totalAmount = bookingDetails.getTotal();
            String status = bookingDetails.getBookingStatus();
            String createdAt = bookingDetails.getCreatedAt();

            holder.tvTrackID.setText(String.format("#%s", trackID));
            holder.tvAmount.setText(String.format("%s %s", context.getString(R.string.rupee), totalAmount));
            holder.tvCreatedAt.setText(createdAt);

            GradientDrawable drawable = (GradientDrawable)holder.tvStatus.getBackground();

            if(status.equalsIgnoreCase("0")){
                holder.tvStatus.setText(context.getString(R.string.cancelled));
                drawable.setColor(Color.parseColor("#B22222"));
            }else if(status.equalsIgnoreCase("1")){
                holder.tvStatus.setText(context.getString(R.string.received));
                drawable.setColor(Color.parseColor("#4682B4"));
            }else if(status.equalsIgnoreCase("2")){
                holder.tvStatus.setText(context.getString(R.string.accepted));
                drawable.setColor(Color.parseColor("#228B22"));
            }else if(status.equalsIgnoreCase("3")){
                holder.tvStatus.setText(context.getString(R.string.servicing));
                drawable.setColor(Color.parseColor("#803F48A7"));
            }else if(status.equalsIgnoreCase("4")){
                holder.tvStatus.setText(context.getString(R.string.completed));
                drawable.setColor(Color.parseColor("#228B22"));
            }else if(status.equalsIgnoreCase("5")){
                holder.tvStatus.setText(context.getString(R.string.rejected));
                drawable.setColor(Color.parseColor("#B22222"));
            }else{
                holder.tvStatus.setText(context.getString(R.string.na));
                drawable.setColor(Color.parseColor("#B22222"));
            }

        }
    }

    @Override
    public int getItemCount() {
        return listOfBookings.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView tvTrackID;
        private final TextView tvAmount;
        private final TextView tvStatus;
        private final TextView tvCreatedAt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTrackID = itemView.findViewById(R.id.tvTrackID);
            tvStatus = itemView.findViewById(R.id.tvStatus);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvCreatedAt = itemView.findViewById(R.id.tvCreatedAt);

            itemView.setOnClickListener(this);
            tvCreatedAt.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            bookingDetails = listOfBookings.get(getLayoutPosition());
                String bookingId = bookingDetails.getId();
                Intent doctorBookingDetails = new Intent(context, BookingDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.booking_id), String.valueOf(bookingId));
                doctorBookingDetails.putExtras(bundle);
                context.startActivity(doctorBookingDetails);

        }
    }
}
