package com.nextclick.crm.doctorsModule.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.adapter.DoctorsTabAdapter;
import com.nextclick.crm.doctorsModule.fragment.ApprovedDoctorsFragment;
import com.nextclick.crm.doctorsModule.fragment.DoctorsAdminFragment;
import com.nextclick.crm.doctorsModule.fragment.MyDoctorsFragment;
import com.nextclick.crm.doctorsModule.fragment.PendingDoctorsFragment;
import com.google.android.material.tabs.TabLayout;

public class DoctorsActivity extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabLayout;
    private ImageView ivBackArrow;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_doctors);
        initializeUi();
        initializeListeners();
        initializeTabs();
    }

    private void initializeUi() {
        tabLayout = findViewById(R.id.tabLayout);
        ivBackArrow = findViewById(R.id.ivBackArrow);
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }

    private void initializeTabs() {
        DoctorsTabAdapter doctorsTabAdapter = new DoctorsTabAdapter(this, getSupportFragmentManager(), tabLayout, R.id.fragmentContainer);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.admin)).setTag(getResources().getString(R.string.admin)), DoctorsAdminFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.my_doctors)).setTag(getResources().getString(R.string.my_doctors)), MyDoctorsFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.approved)).setTag(getResources().getString(R.string.approved)), ApprovedDoctorsFragment.class, null);
        doctorsTabAdapter.addTab(tabLayout.newTab().setText(getResources().getString(R.string.pending)).setTag(getResources().getString(R.string.pending)), PendingDoctorsFragment.class, null);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }
}
