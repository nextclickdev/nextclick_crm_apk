package com.nextclick.crm.doctorsModule.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.GetBookingsApiCall;
import com.nextclick.crm.doctorsModule.adapter.DoctorsBookingsAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingDetails;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingsListResponse;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class DoctorsBookingsActivity extends AppCompatActivity implements View.OnClickListener, HttpReqResCallBack {
    private TextView tvError;
    private ImageView ivBackArrow;
    private RecyclerView rvDoctorsBookingsDetails;
    private PreferenceManager preferenceManager;

    private LinkedList<BookingDetails> listOfBookings;

    private String token = "";
    private String serviceID = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_doctors_bookings);
        initializeUi();
        initializeListeners();
        getBookingDetails();
    }

    private void initializeUi() {
        tvError = findViewById(R.id.tvError);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        rvDoctorsBookingsDetails = findViewById(R.id.rvDoctorsBookingsDetails);
        preferenceManager = new PreferenceManager(this);

        Intent intent = getIntent();
        serviceID = intent.getStringExtra(getString(R.string.service_id));
    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
    }
    @Override
    public void onResume() {
        super.onResume();
        boolean refreshBookings = preferenceManager.getBoolean(getString(R.string.refresh_bookings));
        if (refreshBookings) {
            preferenceManager.putBoolean(getString(R.string.refresh_bookings), false);
            getBookingDetails();
        }
    }
    private void getBookingDetails() {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetBookingsApiCall.serviceCallForBookingsList(this, null, null, token,serviceID);
    }



    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_GET_BOOKINGS) {
            if (jsonResponse != null) {
                try {
                    BookingsListResponse bookingsListResponse = new Gson().fromJson(jsonResponse, BookingsListResponse.class);
                    if (bookingsListResponse != null) {
                        boolean status = bookingsListResponse.getStatus();
                        String message = bookingsListResponse.getMessage();
                        if (status) {
                            listOfBookings = bookingsListResponse.getListOfBookings();
                            initializeAdapter();
                        } else {
                            listIsEmpty();
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        listIsFull();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                    listIsEmpty();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }


    private void initializeAdapter() {
        DoctorsBookingsAdapter doctorsBookingsAdapter = new DoctorsBookingsAdapter(this, listOfBookings);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvDoctorsBookingsDetails.setLayoutManager(layoutManager);
        rvDoctorsBookingsDetails.setItemAnimator(new DefaultItemAnimator());
        rvDoctorsBookingsDetails.setAdapter(doctorsBookingsAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvDoctorsBookingsDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvDoctorsBookingsDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.ivBackArrow) {
            onBackPressed();
        }
    }
}
