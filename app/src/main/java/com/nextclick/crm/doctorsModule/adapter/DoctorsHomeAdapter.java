package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.DoctorsActivity;
import com.nextclick.crm.doctorsModule.activities.DoctorsBookingsActivity;

import java.util.LinkedList;

public class DoctorsHomeAdapter extends RecyclerView.Adapter<DoctorsHomeAdapter.ViewHolder> {

    private final Context context;
    private final LinkedList<String> listOfCollectionNames;
    private final LinkedList<Integer> listOfCollectionImage;

    private String serviceID = "";
    private String activeCount = "";
    private String inActiveCount = "";
    private String pendingBookingCount = "";
    private String acceptedBookingCount = "";

    public DoctorsHomeAdapter(Context context, LinkedList<String> listOfCollectionNames, LinkedList<Integer> listOfCollectionImage, String activeCount, String inActiveCount, String pendingBookingCount, String acceptedBookingCount, String serviceID) {
        this.context = context;
        this.serviceID = serviceID;
        this.listOfCollectionImage = listOfCollectionImage;
        this.listOfCollectionNames = listOfCollectionNames;
        this.activeCount = activeCount;
        this.inActiveCount = inActiveCount;
        this.pendingBookingCount = pendingBookingCount;
        this.acceptedBookingCount = acceptedBookingCount;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_doctors_home_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int collectionImage = listOfCollectionImage.get(position);
        String collectionName = listOfCollectionNames.get(position);

        holder.ivCollectionImage.setImageResource(collectionImage);
        holder.tvCollectionName.setText(collectionName);

        if(collectionName.equalsIgnoreCase(context.getString(R.string.doctors))){
            holder.tvActive.setText(activeCount);
            holder.tvInActive.setText(inActiveCount);
        }else if(collectionName.equalsIgnoreCase(context.getString(R.string.bookings))){
            holder.tvLeftTitle.setText(context.getString(R.string.pending));
            holder.tvRightTitle.setText(context.getString(R.string.accepted));
            holder.tvActive.setText(pendingBookingCount);
            holder.tvInActive.setText(acceptedBookingCount);
        }
    }

    @Override
    public int getItemCount() {
        return listOfCollectionNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivCollectionImage;
        private final TextView tvCollectionName;
        private final TextView tvActive;
        private final TextView tvInActive;
        private final TextView tvLeftTitle;
        private final TextView tvRightTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvActive = itemView.findViewById(R.id.tvActive);
            tvInActive = itemView.findViewById(R.id.tvInActive);
            tvLeftTitle = itemView.findViewById(R.id.tvLeftTitle);
            tvRightTitle = itemView.findViewById(R.id.tvRightTitle);
            tvCollectionName = itemView.findViewById(R.id.tvCollectionName);
            ivCollectionImage = itemView.findViewById(R.id.ivCollectionImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String collectionName = listOfCollectionNames.get(getLayoutPosition());
            if (collectionName.equalsIgnoreCase(context.getString(R.string.doctors))) {
                goToDoctors();
            } else if (collectionName.equalsIgnoreCase(context.getString(R.string.bookings))) {
                goToBookings();
                //Toast.makeText(context, context.getString(R.string.under_development), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void goToBookings() {
        Intent doctorsBookingsIntent = new Intent(context, DoctorsBookingsActivity.class);
        doctorsBookingsIntent.putExtra(context.getString(R.string.service_id),serviceID);
        context.startActivity(doctorsBookingsIntent);
    }

    private void goToDoctors() {
        Intent doctorsIntent = new Intent(context, DoctorsActivity.class);
        context.startActivity(doctorsIntent);
    }
}
