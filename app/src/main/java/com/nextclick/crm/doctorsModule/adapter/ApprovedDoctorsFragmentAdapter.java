package com.nextclick.crm.doctorsModule.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.DeleteDoctorApiCall;
import com.nextclick.crm.doctorsModule.activities.AddAsYourDoctorActivity;
import com.nextclick.crm.doctorsModule.activities.EditDoctorActivity;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.approvedDoctorsResponse.ApprovedDoctorDetails;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ApprovedDoctorsFragmentAdapter extends RecyclerView.Adapter<ApprovedDoctorsFragmentAdapter.ViewHolder> implements HttpReqResCallBack {

    private final Context context;
    private final PreferenceManager preferenceManager;
    private ApprovedDoctorDetails approvedDoctorDetails;

    private final LinkedList<ApprovedDoctorDetails> listOfApprovedDoctorDetails;

    private int selectedPosition = -1;

    private String token = "";

    public ApprovedDoctorsFragmentAdapter(Context context, LinkedList<ApprovedDoctorDetails> listOfApprovedDoctorDetails) {
        this.context = context;
        preferenceManager = new PreferenceManager(context);
        this.listOfApprovedDoctorDetails = listOfApprovedDoctorDetails;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_approved_doctors_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ApprovedDoctorDetails approvedDoctorDetails = listOfApprovedDoctorDetails.get(position);
        if (approvedDoctorDetails != null) {
            String doctorName = approvedDoctorDetails.getName();
            String doctorImage = approvedDoctorDetails.getImage();
            String doctorDescription = approvedDoctorDetails.getDesc();
            String doctorQualification = approvedDoctorDetails.getQualification();

            holder.tvDoctorName.setText(doctorName);
            holder.tvDescription.setText(doctorDescription);
            holder.tvQualification.setText(doctorQualification);

            if (doctorImage != null) {
                if (!doctorImage.isEmpty()) {
                    Picasso.get()
                            .load(doctorImage)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivDoctorPic);
                } else {
                    Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivDoctorPic);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivDoctorPic);
            }
        }
    }

    @Override
    public int getItemCount() {
        return listOfApprovedDoctorDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivDoctorPic;
        private final TextView tvDoctorName;
        private final TextView tvQualification;
        private final TextView tvDescription;
        private final TextView tvAddAsYourDoctor;
        private final TextView tvDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDelete = itemView.findViewById(R.id.tvDelete);
            ivDoctorPic = itemView.findViewById(R.id.ivDoctorPic);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvQualification = itemView.findViewById(R.id.tvQualification);
            tvAddAsYourDoctor = itemView.findViewById(R.id.tvAddAsYourDoctor);

            tvDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
            tvAddAsYourDoctor.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();
            selectedPosition = getLayoutPosition();
            approvedDoctorDetails = listOfApprovedDoctorDetails.get(getLayoutPosition());
            if (id == R.id.tvDelete) {
                showDeleteDoctorAlertDialog();
            } else if (id == R.id.tvAddAsYourDoctor) {
                showAddAsYourDoctorAlertDialog();
            } else {
                int doctorId = approvedDoctorDetails.getId();
                int hospitalDoctorId = approvedDoctorDetails.getHospDoctorId();
                Intent addAsYourDoctor = new Intent(context, EditDoctorActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.doctor_id), String.valueOf(doctorId));
                bundle.putString(context.getString(R.string.hospital_doctor_id), String.valueOf(hospitalDoctorId));
                addAsYourDoctor.putExtras(bundle);
                context.startActivity(addAsYourDoctor);
            }
        }
    }

    private void showAddAsYourDoctorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.do_you_want_to_add_him_as_your_doctor));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int doctorId = approvedDoctorDetails.getId();
                int hospitalDoctorId = approvedDoctorDetails.getHospDoctorId();
                Intent addAsYourDoctor = new Intent(context, AddAsYourDoctorActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(context.getString(R.string.doctor_id), String.valueOf(doctorId));
                bundle.putString(context.getString(R.string.hospital_doctor_id), String.valueOf(hospitalDoctorId));
                addAsYourDoctor.putExtras(bundle);
                context.startActivity(addAsYourDoctor);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void showDeleteDoctorAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(context.getString(R.string.are_you_sure_do_you_want_to_delete));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoadingDialog.loadDialog(context);
                token = "Bearer " + preferenceManager.getString(USER_TOKEN);
                DeleteDoctorApiCall.serviceCallForDeleteDoctor(context, null, ApprovedDoctorsFragmentAdapter.this, approvedDoctorDetails.getId(), approvedDoctorDetails.getHospDoctorId(), token);
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_DELETE_DOCTOR) {
            if (jsonResponse != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    boolean status = jsonObject.getBoolean("status");
                    String message = jsonObject.getString("message");
                    if (status) {
                        if (selectedPosition != -1) {
                            listOfApprovedDoctorDetails.remove(selectedPosition);
                            notifyDataSetChanged();
                        }
                    }
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }
}
