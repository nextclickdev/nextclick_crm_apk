package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.BookingDetailsActivity;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingDetails;
import com.nextclick.crm.models.responseModels.bookingsListResponse.BookingDetailsResponse;
import com.facebook.stetho.common.StringUtil;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class BookingDetailsAdapter extends RecyclerView.Adapter<BookingDetailsAdapter.ViewHolder> {

    private final Context context;
    private BookingDetails bookingDetails;
    private final LinkedList<BookingDetailsResponse.BookingItemsDetails> listOfBookingItemDetails;

    public BookingDetailsAdapter(Context context, LinkedList<BookingDetailsResponse.BookingItemsDetails> listOfBookingItemDetails) {
        this.context = context;
        this.listOfBookingItemDetails = listOfBookingItemDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_booking_details_item_1, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BookingDetailsResponse.BookingItemsDetails bookingItemDetails = listOfBookingItemDetails.get(position);
        if (bookingItemDetails != null) {

            String image = bookingItemDetails.getServiceItem().getImage();
            String name = bookingItemDetails.getServiceItem().getName();
            String totalAmount = bookingItemDetails.getTotal();
            String price = bookingItemDetails.getPrice();
            if (bookingItemDetails.getServiceTimings() != null) {
                String fromTime = bookingItemDetails.getServiceTimings().getStartTime();
                String toTime = bookingItemDetails.getServiceTimings().getEndTime();
                holder.tvTimeSlot.setText(String.format("%s - %s", fromTime, toTime));
            }
            String date = bookingItemDetails.getBookingDate();

            holder.tvName.setText(String.format("%s", name));
            holder.tvDate.setText(date);

            holder.tvPrice.setText("₹" + totalAmount);
            holder.tvPriceWithOutDiscount.setText(price);

            holder.tvPriceWithOutDiscount.setPaintFlags(holder.tvPriceWithOutDiscount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            if (image != null) {
                if (!image.isEmpty()) {
                    Picasso.get()
                            .load(image)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivImage);
                } else {
                    Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivImage);
                }
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivImage);
            }

        }
    }

    @Override
    public int getItemCount() {
        return listOfBookingItemDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivImage;
        private final TextView tvName;
        private final TextView tvDate;
        private final TextView tvTimeSlot;
        private final TextView tvPrice;
        private final TextView tvPriceWithOutDiscount;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.ivImage);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvName = itemView.findViewById(R.id.tvName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTimeSlot = itemView.findViewById(R.id.tvTimeSlot);
            tvPriceWithOutDiscount = itemView.findViewById(R.id.tvPriceWithOutDiscount);

        }

    }
}

