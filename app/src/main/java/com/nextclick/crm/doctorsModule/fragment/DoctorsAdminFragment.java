package com.nextclick.crm.doctorsModule.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.DoctorsAdminDetailsApiCall;
import com.nextclick.crm.doctorsModule.activities.AddDoctorActivity;
import com.nextclick.crm.doctorsModule.adapter.DoctorsAdminFragmentAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.doctorsAdminResponse.DoctorAdminDetails;
import com.nextclick.crm.models.responseModels.doctorsAdminResponse.DoctorsAdminResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class DoctorsAdminFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private FloatingActionButton fabAddDoctor;
    private RecyclerView rvDoctorsAdminDetails;
    private PreferenceManager preferenceManager;

    private LinkedList<DoctorAdminDetails> listOfDoctorAdminDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_doctors_admin_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddDoctor = view.findViewById(R.id.fabAddDoctor);
        rvDoctorsAdminDetails = view.findViewById(R.id.rvDoctorsAdminDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
        }
    }

    private void initializeListeners() {
        fabAddDoctor.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        DoctorsAdminDetailsApiCall.serviceCallForDoctorAdminDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshDoctors = preferenceManager.getBoolean(getString(R.string.refresh_doctors));
        if (refreshDoctors) {
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_DOCTOR_ADMIN_DETAILS) {
            if (jsonResponse != null) {
                try {
                    DoctorsAdminResponse doctorsAdminResponse = new Gson().fromJson(jsonResponse, DoctorsAdminResponse.class);
                    if (doctorsAdminResponse != null) {
                        boolean status = doctorsAdminResponse.getStatus();
                        String message = doctorsAdminResponse.getMessage();
                        if (status) {
                            listOfDoctorAdminDetails = doctorsAdminResponse.getListOfDoctorAdminDetails();
                            if (listOfDoctorAdminDetails != null) {
                                if (listOfDoctorAdminDetails.size() != 0) {
                                    listIsFull();
                                    initializeAdapter();
                                } else {
                                    listIsEmpty();
                                }
                            } else {
                                listIsFull();
                            }
                        } else {
                            listIsEmpty();
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                    listIsEmpty();
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        DoctorsAdminFragmentAdapter doctorsAdminFragmentAdapter = new DoctorsAdminFragmentAdapter(getActivity(), listOfDoctorAdminDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvDoctorsAdminDetails.setLayoutManager(layoutManager);
        rvDoctorsAdminDetails.setItemAnimator(new DefaultItemAnimator());
        rvDoctorsAdminDetails.setAdapter(doctorsAdminFragmentAdapter);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvDoctorsAdminDetails.setVisibility(View.VISIBLE);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvDoctorsAdminDetails.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddDoctor) {
            goToAddDoctor();
        }
    }

    private void goToAddDoctor() {
        if (getActivity() != null) {
            Intent addDoctorIntent = new Intent(getActivity(), AddDoctorActivity.class);
            startActivity(addDoctorIntent);
        }
    }
}
