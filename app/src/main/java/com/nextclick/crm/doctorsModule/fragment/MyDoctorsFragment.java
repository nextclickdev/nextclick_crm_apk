package com.nextclick.crm.doctorsModule.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.MyDoctorsDetailsApiCall;
import com.nextclick.crm.doctorsModule.activities.AddDoctorActivity;
import com.nextclick.crm.doctorsModule.adapter.MyDoctorsFragmentAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.myDoctorsResponse.MyDoctorDetails;
import com.nextclick.crm.models.responseModels.myDoctorsResponse.MyDoctorsResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class MyDoctorsFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private RecyclerView rvMyDoctorsDetails;
    private FloatingActionButton fabAddDoctor;
    private PreferenceManager preferenceManager;

    private LinkedList<MyDoctorDetails> listOfMyDoctorDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_my_doctors_fragment, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddDoctor = view.findViewById(R.id.fabAddDoctor);
        rvMyDoctorsDetails = view.findViewById(R.id.rvMyDoctorsDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
        }
    }

    private void initializeListeners() {
        fabAddDoctor.setOnClickListener(this);
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        MyDoctorsDetailsApiCall.serviceCallForMyDoctorDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshDoctors = preferenceManager.getBoolean(getString(R.string.refresh_doctors));
        if (refreshDoctors) {
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_MY_DOCTOR_DETAILS) {
            if (jsonResponse != null) {
                MyDoctorsResponse myDoctorsResponse = new Gson().fromJson(jsonResponse, MyDoctorsResponse.class);
                if (myDoctorsResponse != null) {
                    boolean status = myDoctorsResponse.getStatus();
                    if (status) {
                        listOfMyDoctorDetails = myDoctorsResponse.getListOfMyDoctorDetails();
                        if (listOfMyDoctorDetails != null) {
                            if (listOfMyDoctorDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        MyDoctorsFragmentAdapter myDoctorsFragmentAdapter = new MyDoctorsFragmentAdapter(getActivity(), listOfMyDoctorDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvMyDoctorsDetails.setLayoutManager(layoutManager);
        rvMyDoctorsDetails.setItemAnimator(new DefaultItemAnimator());
        rvMyDoctorsDetails.setAdapter(myDoctorsFragmentAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvMyDoctorsDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvMyDoctorsDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddDoctor) {
            goToAddDoctor();
        }
    }

    private void goToAddDoctor() {
        if (getActivity() != null) {
            Intent addDoctorIntent = new Intent(getActivity(), AddDoctorActivity.class);
            startActivity(addDoctorIntent);
        }
    }
}
