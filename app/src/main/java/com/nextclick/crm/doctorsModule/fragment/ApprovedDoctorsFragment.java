package com.nextclick.crm.doctorsModule.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.apiCalls.ApprovedDoctorDetailsApiCall;
import com.nextclick.crm.doctorsModule.activities.AddDoctorActivity;
import com.nextclick.crm.doctorsModule.adapter.ApprovedDoctorsFragmentAdapter;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.approvedDoctorsResponse.ApprovedDoctorDetails;
import com.nextclick.crm.models.responseModels.approvedDoctorsResponse.ApprovedDoctorsResponse;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.LinkedList;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class ApprovedDoctorsFragment extends Fragment implements HttpReqResCallBack, View.OnClickListener {

    private TextView tvError;
    private RecyclerView rvApprovedDetails;
    private FloatingActionButton fabAddDoctor;
    private PreferenceManager preferenceManager;

    private LinkedList<ApprovedDoctorDetails> listOfApprovedDoctorDetails;

    private String token = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_approved_doctors, container, false);
        initializeUi(view);
        initializeListeners();
        prepareDetails();
        return view;
    }

    private void initializeListeners() {
        fabAddDoctor.setOnClickListener(this);
    }

    private void initializeUi(View view) {
        tvError = view.findViewById(R.id.tvError);
        fabAddDoctor = view.findViewById(R.id.fabAddDoctor);
        rvApprovedDetails = view.findViewById(R.id.rvApprovedDetails);

        if (getActivity() != null) {
            preferenceManager = new PreferenceManager(getActivity());
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
        }
    }

    private void prepareDetails() {
        LoadingDialog.loadDialog(getActivity());
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        ApprovedDoctorDetailsApiCall.serviceCallForApprovedDoctorDetails(getActivity(), this, null, token);
    }

    @Override
    public void onResume() {
        super.onResume();
        boolean refreshDoctors = preferenceManager.getBoolean(getString(R.string.refresh_doctors));
        if (refreshDoctors) {
            preferenceManager.putBoolean(getString(R.string.refresh_doctors), false);
            prepareDetails();
        }
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        if (requestType == Constants.SERVICE_CALL_TO_GET_APPROVED_DOCTOR_DETAILS) {
            if (jsonResponse != null) {
                ApprovedDoctorsResponse approvedDoctorsResponse = new Gson().fromJson(jsonResponse, ApprovedDoctorsResponse.class);
                if (approvedDoctorsResponse != null) {
                    boolean status = approvedDoctorsResponse.getStatus();
                    String message = approvedDoctorsResponse.getMessage();
                    if (status) {
                        listOfApprovedDoctorDetails = approvedDoctorsResponse.getListOfApprovedDoctorDetails();
                        if (listOfApprovedDoctorDetails != null) {
                            if (listOfApprovedDoctorDetails.size() != 0) {
                                listIsFull();
                                initializeAdapter();
                            } else {
                                listIsEmpty();
                            }
                        } else {
                            listIsEmpty();
                        }
                    } else {
                        listIsEmpty();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }
                }
            }
            LoadingDialog.dialog.dismiss();
        }
    }

    private void initializeAdapter() {
        ApprovedDoctorsFragmentAdapter approvedDoctorsFragmentAdapter = new ApprovedDoctorsFragmentAdapter(getActivity(), listOfApprovedDoctorDetails);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvApprovedDetails.setLayoutManager(layoutManager);
        rvApprovedDetails.setItemAnimator(new DefaultItemAnimator());
        rvApprovedDetails.setAdapter(approvedDoctorsFragmentAdapter);
    }

    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        rvApprovedDetails.setVisibility(View.GONE);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        rvApprovedDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.fabAddDoctor) {
            goToAddDoctor();
        }
    }

    private void goToAddDoctor() {
        if (getActivity() != null) {
            Intent addDoctorIntent = new Intent(getActivity(), AddDoctorActivity.class);
            startActivity(addDoctorIntent);
        }
    }
}
