package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.nextclick.crm.Utilities.TabInfo;
import com.nextclick.crm.doctorsModule.fragment.ApprovedDoctorsFragment;
import com.nextclick.crm.doctorsModule.fragment.DoctorsAdminFragment;
import com.nextclick.crm.doctorsModule.fragment.MyDoctorsFragment;
import com.nextclick.crm.doctorsModule.fragment.PendingDoctorsFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class DoctorsTabAdapter implements TabLayout.OnTabSelectedListener {

    private final Context context;
    private final TabLayout tabLayout;
    private final int fragmentContainer;
    private final FragmentManager fragmentManager;
    private TextView tvSearch, tvClear, tvSavedFilter;
    private final ArrayList<TabInfo> mTabs = new ArrayList<>();
    private Fragment fragment;

    public DoctorsTabAdapter(Context context, FragmentManager fragmentManager, TabLayout tabLayout, int fragmentContainer) {
        this.context = context;
        this.tabLayout = tabLayout;
        this.fragmentManager = fragmentManager;
        this.fragmentContainer = fragmentContainer;
        tabLayout.addOnTabSelectedListener(this);
    }

    public void addTab(TabLayout.Tab tabSpec, Class<?> clss, Bundle args) {
        String tag = (String) tabSpec.getTag();
        TabInfo info = new TabInfo(tag, clss, args);
        mTabs.add(info);
        tabLayout.addTab(tabSpec);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        String tabName = (String) tab.getTag();
        TabInfo info = mTabs.get(position);
        switch (position) {
            case 0:
                fragment = new DoctorsAdminFragment();
                break;
            case 1:
                fragment = new MyDoctorsFragment();
                break;
            case 2:
                fragment = new ApprovedDoctorsFragment();
                break;
            case 3:
                fragment = new PendingDoctorsFragment();
                break;
            default:
                break;
        }

        fragmentManager.beginTransaction().replace(fragmentContainer, fragment).commit();

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
