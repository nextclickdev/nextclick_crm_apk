package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.EditDoctorActivity;
import com.nextclick.crm.doctorsModule.fragment.MyDoctorsFragment;
import com.nextclick.crm.models.responseModels.myDoctorsResponse.MyDoctorDetails;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;

public class MyDoctorsFragmentAdapter extends RecyclerView.Adapter<MyDoctorsFragmentAdapter.ViewHolder> {

    private final Context context;
    private final LinkedList<MyDoctorDetails> listOfMyDoctorDetails;

    public MyDoctorsFragmentAdapter(Context context, LinkedList<MyDoctorDetails> listOfMyDoctorDetails) {
        this.context = context;
        this.listOfMyDoctorDetails = listOfMyDoctorDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_my_doctors_fragment_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyDoctorDetails myDoctorDetails = listOfMyDoctorDetails.get(position);
        String doctorName = myDoctorDetails.getName();
        String doctorImageUrl = myDoctorDetails.getImage();
        String doctorDescription = myDoctorDetails.getDesc();
        String doctorQualification = myDoctorDetails.getQualification();


        holder.tvDoctorName.setText(doctorName);
        holder.tvDescription.setText(doctorDescription);
        holder.tvQualification.setText(doctorQualification);

        if (doctorImageUrl != null) {
            if (!doctorImageUrl.isEmpty()) {
                Picasso.get()
                        .load(doctorImageUrl)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivDoctorPic);
            } else {
                Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivDoctorPic);
            }
        } else {
            Picasso.get()
                    .load(R.drawable.ic_default_place_holder)
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.ivDoctorPic);
        }

    }

    @Override
    public int getItemCount() {
        return listOfMyDoctorDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView ivDoctorPic;
        private final TextView tvDoctorName;
        private final TextView tvQualification;
        private final TextView tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivDoctorPic = itemView.findViewById(R.id.ivDoctorPic);
            tvDoctorName = itemView.findViewById(R.id.tvDoctorName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvQualification = itemView.findViewById(R.id.tvQualification);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            MyDoctorDetails myDoctorDetails = listOfMyDoctorDetails.get(getLayoutPosition());
            int doctorId = myDoctorDetails.getId();
            int hospitalDoctorId = myDoctorDetails.getHospDoctorId();
            Intent addAsYourDoctor = new Intent(context, EditDoctorActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(context.getString(R.string.doctor_id), String.valueOf(doctorId));
            bundle.putString(context.getString(R.string.hospital_doctor_id), String.valueOf(hospitalDoctorId));
            addAsYourDoctor.putExtras(bundle);
            context.startActivity(addAsYourDoctor);
        }
    }
}
