package com.nextclick.crm.doctorsModule.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.interfaces.CloseCallBack;
import com.nextclick.crm.interfaces.DaysNameCallBack;

import java.util.LinkedList;

public class DaysNamesAdapter extends RecyclerView.Adapter<DaysNamesAdapter.ViewHolder> implements View.OnClickListener {

    private final Context context;
    private final LinkedList<String> listOfDayNames;
    private final LinkedList<Integer> listOfDayNamesIds;
    private LinkedList<Integer> listOfSelectedDayIds;
    private LinkedList<String> listOfSelectedDayNames;

    public DaysNamesAdapter(Context context, LinkedList<String> listOfDayNames, LinkedList<Integer> listOfDayNamesIds, LinkedList<Integer> listOfSelectedDayIds, LinkedList<String> listOfSelectedDayNames, TextView tvSave) {
        this.context = context;
        tvSave.setOnClickListener(this);
        this.listOfDayNames = listOfDayNames;
        this.listOfDayNamesIds = listOfDayNamesIds;
        this.listOfSelectedDayIds = listOfSelectedDayIds;
        this.listOfSelectedDayNames = listOfSelectedDayNames;
        if (this.listOfSelectedDayIds == null) {
            this.listOfSelectedDayIds = new LinkedList<>();
        }
        if (this.listOfSelectedDayNames == null) {
            this.listOfSelectedDayNames = new LinkedList<>();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_day_names_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String dayName = listOfDayNames.get(position);
        int dayNameId = listOfDayNamesIds.get(position);
        holder.tvDayName.setText(dayName);
        holder.checkBox.setChecked(listOfSelectedDayIds.contains(dayNameId));
    }

    @Override
    public int getItemCount() {
        return listOfDayNames.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.tvSave) {
            Context context = UserData.getInstance().getContext();
            if (context != null) {
                DaysNameCallBack daysNameCallBack = (DaysNameCallBack) context;
                daysNameCallBack.selectedDaysNames(listOfSelectedDayIds, listOfSelectedDayNames);
            }

            CloseCallBack closeCallBack = (CloseCallBack) this.context;
            closeCallBack.close();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {

        private final CheckBox checkBox;
        private final TextView tvDayName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.checkBox);
            tvDayName = itemView.findViewById(R.id.tvDayName);
            checkBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String dayName = listOfDayNames.get(getLayoutPosition());
            int dayNameId = listOfDayNamesIds.get(getLayoutPosition());
            if (isChecked) {
                if (!listOfSelectedDayIds.contains(dayNameId)) {
                    listOfSelectedDayIds.add(dayNameId);
                    listOfSelectedDayNames.add(dayName);
                }
            } else {
                if (listOfSelectedDayIds.contains(dayNameId)) {
                    int index = listOfSelectedDayIds.indexOf(dayNameId);
                    if (index != -1) {
                        listOfSelectedDayIds.remove(index);
                        listOfSelectedDayNames.remove(index);
                    }
                }
            }
        }
    }
}
