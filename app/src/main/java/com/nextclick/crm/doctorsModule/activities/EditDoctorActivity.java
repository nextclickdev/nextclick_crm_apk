package com.nextclick.crm.doctorsModule.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.AppPermissions;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.UserData;
import com.nextclick.crm.apiCalls.EditDoctorApiCall;
import com.nextclick.crm.apiCalls.GetDoctorDetailsApiCall;
import com.nextclick.crm.apiCalls.GetSpecialitiesApiCall;
import com.nextclick.crm.interfaces.DaysNameCallBack;
import com.nextclick.crm.interfaces.HttpReqResCallBack;
import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.DoctorDetails;
import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.GetDoctorDetailsResponse;
import com.nextclick.crm.models.responseModels.getDoctorDetailsResponse.ServiceTiming;
import com.nextclick.crm.models.responseModels.getSpecialitiesResponse.GetSpecialitiesResponse;
import com.nextclick.crm.models.responseModels.getSpecialitiesResponse.SpecialitiesDetails;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class EditDoctorActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, HttpReqResCallBack, DaysNameCallBack, CompoundButton.OnCheckedChangeListener {


    private LinearLayout llTimings;
    private Spinner spinnerSpeciality;
    private DoctorDetails doctorDetails;
    private RelativeLayout rlImageLayout;
    private TextView tvSubmit, tvHolidays;
    private SwitchCompat statusSwitchCompact;
    private PreferenceManager preferenceManager;
    private BottomSheetDialog pickerBottomSheetDialog;
    private ImageView ivBackArrow, ivProfilePic, ivAddTimings;
    private EditText etName, etDescription, etQualification, etExperience, etLanguages, etFee, etDiscount;

    private ArrayList<String> listOfPhotoPaths;
    private LinkedList<Integer> listOfSelectedDayIds;
    private LinkedList<String> listOfSelectedDayNames;
    private LinkedList<Integer> listOfSpecialitiesIds;
    private LinkedList<String> listOfSpecialitiesNames;
    private LinkedHashMap<Integer, JSONObject> mapOfTimings;
    private LinkedList<SpecialitiesDetails> listOfSpecialitiesDetails;

    private String token = "";
    private String doctorId = "";
    private String specialityName = "";
    private String selectedImageUrl = "";
    private String hospitalDoctorId = "";

    private int count = 0;
    private int specialityId = -1;
    private int statusDoctorDetails = -1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_as_your_doctor);
        getDataFromIntent();
        initializeUi();
        initializeListeners();
        prepareDetails();
        prepareDoctorDetails();
    }

    private void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(getString(R.string.doctor_id)))
                doctorId = bundle.getString(getString(R.string.doctor_id));
            if (bundle.containsKey(getString(R.string.hospital_doctor_id)))
                hospitalDoctorId = bundle.getString(getString(R.string.hospital_doctor_id));
        }
    }

    private void initializeUi() {
        etFee = findViewById(R.id.etFee);
        etName = findViewById(R.id.etName);
        tvSubmit = findViewById(R.id.tvSubmit);
        llTimings = findViewById(R.id.llTimings);
        tvHolidays = findViewById(R.id.tvHolidays);
        etDiscount = findViewById(R.id.etDiscount);
        etLanguages = findViewById(R.id.etLanguages);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        etExperience = findViewById(R.id.etExperience);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        ivAddTimings = findViewById(R.id.ivAddTimings);
        rlImageLayout = findViewById(R.id.rlImageLayout);
        etDescription = findViewById(R.id.etDescription);
        etQualification = findViewById(R.id.etQualification);
        spinnerSpeciality = findViewById(R.id.spinnerSpeciality);
        spinnerSpeciality = findViewById(R.id.spinnerSpeciality);
        statusSwitchCompact = findViewById(R.id.statusSwitchCompact);

        preferenceManager = new PreferenceManager(this);
        listOfSelectedDayIds = new LinkedList<>();
        listOfSpecialitiesNames = new LinkedList<>();

        UserData.getInstance().setContext(this);
        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
        mapOfTimings = new LinkedHashMap<>();
        count = 0;
    }

    private void initializeListeners() {
        tvSubmit.setOnClickListener(this);
        tvHolidays.setOnClickListener(this);
        ivBackArrow.setOnClickListener(this);
        ivAddTimings.setOnClickListener(this);
        rlImageLayout.setOnClickListener(this);
        spinnerSpeciality.setOnItemSelectedListener(this);
        statusSwitchCompact.setOnCheckedChangeListener(this);
    }

    private void prepareDetails() {
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetSpecialitiesApiCall.serviceCallToGetSpecialities(this, null, null, token);
    }

    private void prepareDoctorDetails() {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        GetDoctorDetailsApiCall.serviceCallToGetDoctorDetails(this, null, null, token, doctorId);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.rlImageLayout:
                prepareImageDetails();
                break;
            case R.id.tvSubmit:
                prepareSubmitDetails();
                break;
            case R.id.tvImagePicker:
                listOfPhotoPaths = new ArrayList<>();
                onPickPhoto();
                closeBottomSheetView();
                break;
            case R.id.tvCamera:
                listOfPhotoPaths = new ArrayList<>();
                captureImage();
                closeBottomSheetView();
                break;
            case R.id.tvCancel:
                closeBottomSheetView();
                break;
            case R.id.tvHolidays:
                goToDayNames();
                break;
            case R.id.ivAddTimings:
                prepareAddTimingsDetails();
                break;
            default:
                break;
        }
    }

    private void prepareAddTimingsDetails() {
        count = count + 1;
        mapOfTimings.put(count, new JSONObject());
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_add_doctor_timings, null);

        TextView tvStartTime = view.findViewById(R.id.tvStartTime);
        TextView tvEndTime = view.findViewById(R.id.tvEndTime);
        tvStartTime.setTag(count);
        tvEndTime.setTag(count);
        tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int startTimeTag = (int) v.getTag();
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(EditDoctorActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                        tvStartTime.setText(time);
                        if (mapOfTimings.containsKey(startTimeTag)) {
                            JSONObject jsonObject = mapOfTimings.get(count);
                            if (jsonObject != null) {
                                try {
                                    jsonObject.put("start_time", time);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        tvEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int endTimeTag = (int) v.getTag();
                Calendar calendar = Calendar.getInstance();
                TimePickerDialog timePickerDialog = new TimePickerDialog(EditDoctorActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                        tvEndTime.setText(time);
                        if (mapOfTimings.containsKey(endTimeTag)) {
                            JSONObject jsonObject = mapOfTimings.get(count);
                            if (jsonObject != null) {
                                try {
                                    jsonObject.put("end_time", time);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();
            }
        });

        llTimings.addView(view);
    }

    private void prepareImageDetails() {
        if (AppPermissions.checkPermissionForAccessExternalStorage(this)) {
            if (AppPermissions.checkPermissionForCamera(this)) {
                showBottomSheetView();
            } else {
                AppPermissions.requestPermissionForCamera(this);
            }
        } else {
            AppPermissions.requestPermissionForAccessExternalStorage(this);
        }
    }

    @SuppressLint("InflateParams")
    private void showBottomSheetView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.layout_image_picker_sheet, null);

        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvCamera = view.findViewById(R.id.tvCamera);
        TextView tvImagePicker = view.findViewById(R.id.tvImagePicker);

        tvCancel.setOnClickListener(this);
        tvCamera.setOnClickListener(this);
        tvImagePicker.setOnClickListener(this);

        pickerBottomSheetDialog = new BottomSheetDialog(this, R.style.BottomSheetDialog);
        pickerBottomSheetDialog.setContentView(view);
        pickerBottomSheetDialog.setCanceledOnTouchOutside(true);
        pickerBottomSheetDialog.show();
    }

    private void closeBottomSheetView() {
        if (pickerBottomSheetDialog != null) {
            pickerBottomSheetDialog.cancel();
        }
    }

    private void onPickPhoto() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(listOfPhotoPaths)
                .setActivityTheme(R.style.FilePickerTheme)
                .setActivityTitle(getString(R.string.select_image))
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(true)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.ic_custom_camera)
                .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .pickPhoto(this, Constants.PICK_GALLERY);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void showTimePicker(TextView clickedTextView) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                Calendar datetime = Calendar.getInstance();
                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                datetime.set(Calendar.MINUTE, minute);
                String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                clickedTextView.setText(time);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    private void goToDayNames() {
        Intent dayNamesIntent = new Intent(this, DaysNamesActivity.class);
        startActivity(dayNamesIntent);
    }

    private void prepareSubmitDetails() {

        String fee = etFee.getText().toString();
        String name = etName.getText().toString();
        String holidays = tvHolidays.getText().toString();
        String discount = etDiscount.getText().toString();
        String languages = etLanguages.getText().toString();
        String experience = etExperience.getText().toString();
        String description = etDescription.getText().toString();
        String qualification = etQualification.getText().toString();

        if (!name.isEmpty()) {
            if (!description.isEmpty()) {
                if (!qualification.isEmpty()) {
                    if (!experience.isEmpty()) {
                        if (!languages.isEmpty()) {
                            if (!fee.isEmpty()) {
                                if (!discount.isEmpty()) {
                                    if (mapOfTimings.size() != 0) {
                                        if (!holidays.isEmpty()) {
                                            if (specialityId != -1) {
                                                if (!selectedImageUrl.isEmpty()) {
                                                    String base64 = base64Converter();
                                                    if (!base64.isEmpty()) {
                                                        prepareEditDoctorServiceCall(name, description, qualification, experience, languages, fee, discount, mapOfTimings, listOfSelectedDayIds, specialityId, base64);
                                                    }
                                                } else {
                                                    Toast.makeText(this, getString(R.string.please_select_image), Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(this, getString(R.string.please_select_speciality), Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(this, getString(R.string.please_enter_holidays), Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(this, getString(R.string.please_select_timings), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(this, getString(R.string.please_enter_discount), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(this, getString(R.string.please_enter_fee), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(this, getString(R.string.please_enter_language), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.please_enter_doctor_experience), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.please_enter_doctor_qualification), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.please_enter_doctor_description), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.please_enter_doctor_name), Toast.LENGTH_SHORT).show();
        }
    }

    private void prepareEditDoctorServiceCall(String name, String description, String qualification, String experience, String languages, String fee, String discount, LinkedHashMap<Integer, JSONObject> mapOfTimings, LinkedList<Integer> listOfSelectedDayIds, int specialityId, String base64) {
        LoadingDialog.loadDialog(this);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        EditDoctorApiCall.serviceCallForEditDoctor(this, null, null, name, description, qualification, experience, languages, fee, discount, mapOfTimings, listOfSelectedDayIds, specialityId, base64, token, doctorId, hospitalDoctorId,statusDoctorDetails);
    }


    private String base64Converter() {
        ivProfilePic.buildDrawingCache();
        Bitmap bitmap = ivProfilePic.getDrawingCache();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return Base64.encodeToString(image, 0);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int viewId = parent.getId();
        if (viewId == R.id.spinnerSpeciality) {
            specialityName = spinnerSpeciality.getSelectedItem().toString();
            int index = listOfSpecialitiesNames.indexOf(specialityName);
            if (index != -1) {
                specialityId = listOfSpecialitiesIds.get(index);
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void selectedDaysNames(LinkedList<Integer> listOfSelectedDayIds, LinkedList<String> listOfSelectedDayNames) {
        this.listOfSelectedDayIds = listOfSelectedDayIds;
        this.listOfSelectedDayNames = listOfSelectedDayNames;

        tvHolidays.setText(TextUtils.join(",", listOfSelectedDayNames));
        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
    }

    @Override
    public void jsonResponseReceived(String jsonResponse, int statusCode, int requestType) {
        switch (requestType) {
            case Constants.SERVICE_CALL_TO_GET_SPECIALITIES_DETAILS:
                if (jsonResponse != null) {
                    GetSpecialitiesResponse getSpecialitiesResponse = new Gson().fromJson(jsonResponse, GetSpecialitiesResponse.class);
                    if (getSpecialitiesResponse != null) {
                        boolean status = getSpecialitiesResponse.getStatus();
                        String message = getSpecialitiesResponse.getMessage();
                        if (status) {
                            listOfSpecialitiesDetails = getSpecialitiesResponse.getListOfSpecialitiesDetails();
                            if (listOfSpecialitiesDetails != null) {
                                if (listOfSpecialitiesDetails.size() != 0) {
                                    listOfSpecialitiesIds = new LinkedList<>();
                                    listOfSpecialitiesNames = new LinkedList<>();
                                    for (int index = 0; index < listOfSpecialitiesDetails.size(); index++) {
                                        SpecialitiesDetails specialitiesDetails = listOfSpecialitiesDetails.get(index);
                                        String name = specialitiesDetails.getName();
                                        int specialityId = specialitiesDetails.getId();
                                        listOfSpecialitiesNames.add(name);
                                        listOfSpecialitiesIds.add(specialityId);
                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditDoctorActivity.this, android.R.layout.simple_spinner_item, listOfSpecialitiesNames);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spinnerSpeciality.setAdapter(adapter);
                                }
                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                //LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_GET_DOCTOR_DETAILS:
                if (jsonResponse != null) {
                    GetDoctorDetailsResponse getDoctorDetailsResponse = new Gson().fromJson(jsonResponse, GetDoctorDetailsResponse.class);
                    if (getDoctorDetailsResponse != null) {
                        boolean status = getDoctorDetailsResponse.getStatus();
                        String message = getDoctorDetailsResponse.getMessage();
                        if (status) {
                            doctorDetails = getDoctorDetailsResponse.getDoctorDetails();
                            if (doctorDetails != null) {
                                String name = doctorDetails.getName();
                                selectedImageUrl = doctorDetails.getImage();
                                String description = doctorDetails.getDesc();
                                specialityId = doctorDetails.getHospSpecialtyId();
                                String qualification = doctorDetails.getQualification();
                                double experience = doctorDetails.getExperience();
                                LinkedList<String> listOfLanguages = doctorDetails.getListOfLanguages();
                                int fee = doctorDetails.getFee();
                                int discount = doctorDetails.getDiscount();
                                LinkedList<ServiceTiming> listOfServiceTimings = doctorDetails.getListOfServiceTimings();
                                LinkedList<Integer> listOfHolidays = doctorDetails.getListOfHolidays();
                                statusDoctorDetails = doctorDetails.getStatus();
                                etName.setText(name);
                                etDescription.setText(description);

                                if (listOfSpecialitiesIds != null) {
                                    if (listOfSpecialitiesIds.size() != 0) {
                                        int index = listOfSpecialitiesIds.indexOf(specialityId);
                                        if (index != -1) {
                                            spinnerSpeciality.setSelection(index);
                                        }
                                    }
                                }

                                if (statusDoctorDetails == 1) {
                                    statusSwitchCompact.setChecked(true);
                                } else if (statusDoctorDetails == 2) {
                                    statusSwitchCompact.setChecked(false);
                                }
                                etQualification.setText(qualification);
                                etExperience.setText(String.valueOf(experience));

                                if (listOfLanguages != null) {
                                    if (listOfLanguages.size() != 0) {
                                        String languages = TextUtils.join(",", listOfLanguages);
                                        etLanguages.setText(languages);
                                    }
                                }

                                etFee.setText(String.valueOf(fee));
                                etDiscount.setText(String.valueOf(discount));

                                listOfSelectedDayIds = new LinkedList<>();
                                listOfSelectedDayNames = new LinkedList<>();
                                if (listOfHolidays != null) {
                                    if (listOfHolidays.size() != 0) {
                                        for (int holidaysIndex = 0; holidaysIndex < listOfHolidays.size(); holidaysIndex++) {
                                            int holidayId = listOfHolidays.get(holidaysIndex);
                                            String holidayName = "";
                                            if (holidayId == 1) {
                                                holidayName = "Sunday";
                                            } else if (holidayId == 2) {
                                                holidayName = "Monday";
                                            } else if (holidayId == 3) {
                                                holidayName = "Tuesday";
                                            } else if (holidayId == 4) {
                                                holidayName = "Wednesday";
                                            } else if (holidayId == 5) {
                                                holidayName = "Thursday";
                                            } else if (holidayId == 6) {
                                                holidayName = "Friday";
                                            } else if (holidayId == 7) {
                                                holidayName = "Saturday";
                                            }
                                            listOfSelectedDayIds.add(holidayId);
                                            listOfSelectedDayNames.add(holidayName);
                                        }
                                        UserData.getInstance().setListOfSelectedDayIds(listOfSelectedDayIds);
                                        UserData.getInstance().setListOfSelectedDayNames(listOfSelectedDayNames);
                                        tvHolidays.setText(TextUtils.join(",", listOfSelectedDayNames));
                                    }
                                }

                                if (listOfServiceTimings != null) {
                                    if (listOfServiceTimings.size() != 0) {
                                        for (int index = 0; index < listOfServiceTimings.size(); index++) {
                                            ServiceTiming serviceTiming = listOfServiceTimings.get(index);
                                            prepareSetServiceTimings(serviceTiming);
                                        }
                                    }
                                }

                                if (selectedImageUrl != null) {
                                    if (!selectedImageUrl.isEmpty()) {
                                        Glide.with(this)
                                                .load(selectedImageUrl)
                                                .placeholder(R.drawable.ic_default_circle)
                                                .error(R.drawable.ic_default_circle)
                                                .into(ivProfilePic);
                                    } else {
                                        Glide.with(this)
                                                .load(R.drawable.ic_default_circle)
                                                .placeholder(R.drawable.ic_default_circle)
                                                .error(R.drawable.ic_default_circle)
                                                .into(ivProfilePic);
                                    }
                                } else {
                                    Glide.with(this)
                                            .load(R.drawable.ic_default_circle)
                                            .placeholder(R.drawable.ic_default_circle)
                                            .error(R.drawable.ic_default_circle)
                                            .into(ivProfilePic);
                                }

                            }
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            case Constants.SERVICE_CALL_TO_UPDATE_DOCTOR_DEAILS:
                if (jsonResponse != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(jsonResponse);
                        boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            preferenceManager.putBoolean(getString(R.string.refresh_doctors), true);
                            finish();
                        } else {
                            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                LoadingDialog.dialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void prepareSetServiceTimings(ServiceTiming serviceTiming) {
        try {
            count = count + 1;
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("start_time", serviceTiming.getStartTime());
            jsonObject.put("end_time", serviceTiming.getEndTime());
            mapOfTimings.put(count, jsonObject);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.layout_add_doctor_timings, null);

            TextView tvStartTime = view.findViewById(R.id.tvStartTime);
            TextView tvEndTime = view.findViewById(R.id.tvEndTime);
            tvStartTime.setTag(count);
            tvEndTime.setTag(count);
            tvStartTime.setText(serviceTiming.getStartTime());
            tvEndTime.setText(serviceTiming.getEndTime());
            tvStartTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int startTimeTag = (int) v.getTag();
                    Calendar calendar = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(EditDoctorActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                            Calendar datetime = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);
                            String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                            tvStartTime.setText(time);
                            if (mapOfTimings.containsKey(startTimeTag)) {
                                JSONObject jsonObject = mapOfTimings.get(count);
                                if (jsonObject != null) {
                                    try {
                                        jsonObject.put("start_time", time);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                }
            });

            tvEndTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int endTimeTag = (int) v.getTag();
                    Calendar calendar = Calendar.getInstance();
                    TimePickerDialog timePickerDialog = new TimePickerDialog(EditDoctorActivity.this, R.style.DateAndTimePicker, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                            Calendar datetime = Calendar.getInstance();
                            datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            datetime.set(Calendar.MINUTE, minute);
                            String time = ((hourOfDay < 10) ? ("0" + hourOfDay) : hourOfDay) + ":" + ((minute < 10) ? ("0" + minute) : minute);
                            tvEndTime.setText(time);
                            if (mapOfTimings.containsKey(endTimeTag)) {
                                JSONObject jsonObject = mapOfTimings.get(count);
                                if (jsonObject != null) {
                                    try {
                                        jsonObject.put("end_time", time);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                    timePickerDialog.show();
                }
            });

            llTimings.addView(view);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        switch (requestCode) {
            case Constants.PICK_GALLERY:
                listOfPhotoPaths = new ArrayList<>();
                if (resultIntent != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        listOfPhotoPaths.addAll(resultIntent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                        if (listOfPhotoPaths != null) {
                            if (listOfPhotoPaths.size() != 0) {
                                selectedImageUrl = listOfPhotoPaths.get(0);
                                Glide.with(this)
                                        .load(new File(selectedImageUrl))
                                        .placeholder(R.drawable.ic_default_circle)
                                        .error(R.drawable.ic_default_circle)
                                        .into(ivProfilePic);
                            }
                        }
                    }
                }
                break;
            case Constants.REQUEST_CODE_CAPTURE_IMAGE:
                listOfPhotoPaths = new ArrayList<>();
                List<Image> listOfImages = ImagePicker.getImages(resultIntent);
                if (listOfImages != null) {
                    if (listOfImages.size() != 0) {
                        selectedImageUrl = listOfImages.get(0).getPath();
                        listOfPhotoPaths.add(selectedImageUrl);
                        Glide.with(this)
                                .load(new File(selectedImageUrl))
                                .placeholder(R.drawable.ic_default_circle)
                                .error(R.drawable.ic_default_circle)
                                .into(ivProfilePic);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions.length != 0 && grantResults.length != 0) {
            switch (requestCode) {
                case Constants.REQUEST_CODE_FOR_CAMERA:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                case Constants.REQUEST_CODE_FOR_EXTERNAL_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        prepareImageDetails();
                    } else {
                        prepareImageDetails();
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            statusDoctorDetails = 1;
        } else {
            statusDoctorDetails = 2;
        }
    }
}
