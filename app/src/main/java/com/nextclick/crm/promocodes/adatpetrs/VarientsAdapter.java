package com.nextclick.crm.promocodes.adatpetrs;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.promocodes.activity.AddPromocard;

import java.util.ArrayList;

public class VarientsAdapter extends RecyclerView.Adapter<VarientsAdapter.ViewHolder>  {

    private boolean isPromotioProducts;
    private boolean isSelected;
    private Integer selectionMode,promotionDiscountType;
    ArrayList<ProductAdd> productlist;
    Context context;
    ProductModel productModel;
    int whichactivity,checkupdate;
    Double promotionDiscount;

    public VarientsAdapter(Context context, ProductModel productModel, ArrayList<ProductAdd>
            productdetailslist, int whichactivity, int checkupdate) {
        this.context=context;
        this.productlist=productdetailslist;
        this.productModel=productModel;
        this.whichactivity=whichactivity;
        this.checkupdate=checkupdate;
    }

    public VarientsAdapter(Context context, ProductModel productModel, ArrayList<ProductAdd>
            productdetailslist, int whichactivity, int checkupdate, Integer promotionOfferType, boolean isSelected, Integer promotionDiscountType, Double promotionDiscount) {
        this.context=context;
        this.productlist=productdetailslist;
        this.productModel=productModel;
        this.whichactivity=whichactivity;
        this.checkupdate=checkupdate;
        this.isSelected = false;//isSelected;
        this.selectionMode =promotionOfferType;
        this.promotionDiscountType=promotionDiscountType;
        this.promotionDiscount=promotionDiscount;
        isPromotioProducts=true;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.varientlayout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ProductAdd varientmodel = productlist.get(position);

        holder.item_price.setVisibility(View.GONE);
        holder.tv_pprice.setVisibility(View.GONE);
        if (whichactivity == 1) {
            holder.cb_button.setVisibility(View.GONE);

        }

        if (checkupdate == 1) {
            if (varientmodel.isIsselect()) {
                holder.cb_button.setEnabled(false);
                holder.cb_button.setClickable(false);
                holder.cb_button.setVisibility(View.VISIBLE);
                holder.cb_button.setChecked(true);
                holder.tv_pprice.setVisibility(View.GONE);
            }
        } else {

        }
           /* if (productsModel.isChecked()){
              //  holder.layout_change.setBackgroundColor(context.getResources().getColor(R.color.transparent_orange));
              //  holder.cardview_item.setBackground(context.getResources().getDrawable(R.drawable.orange_boarder_line));
                holder.layout_change.setBackground(context.getResources().getDrawable(R.drawable.orange_boarder_line));
                holder.rb_button.setChecked(true);
            }else {
              //  holder.cardview_item.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.layout_change.setBackgroundColor(context.getResources().getColor(R.color.white));
                holder.rb_button.setChecked(false);
            }*/

        holder.item_price.setText(varientmodel.getPrice());
        holder.item_discount.setText("Discount "+varientmodel.getDiscount()+"%");
        holder.tv_pprice.setText("" + varientmodel.getPrice());
        if(isPromotioProducts) {
            holder.layout_pricing.setVisibility(View.VISIBLE);
            holder.tv_pprice.setVisibility(View.VISIBLE);
        }
        try {
            String name = varientmodel.getSizecolor().substring(0, 1).toUpperCase() + varientmodel.getSizecolor().substring(1);
            if(whichactivity == 1)
                name ="Menu : "+name;
            holder.item_name.setText(name);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("aaaaaaaa   " + e.getMessage());
        }


        holder.cb_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    if (isPromotioProducts && selectionMode == 0 && isSelected) {
                        //single selection
                        holder.cb_button.setChecked(false);
                        return;
                    }
                    if (isPromotioProducts) {
                        double discount = productlist.get(position).getDiscountInDouble();
                        double discountAmount = (productlist.get(position).getPriceInDouble() * discount) / 100;

                        Log.e("discount", "In item discount " + discount);
                        Log.e("discount", "In item price " + productlist.get(position).getPriceInDouble());

                        if (promotionDiscountType == 1 && discount + promotionDiscount > 100) {
                            Log.e("discount", "discount percentage is high " + discount + promotionDiscount);

                            //discount is more
                            holder.cb_button.setChecked(false);
                            UIMsgs.showToast(context, "This item will not available for this promotion due to product discount is more than 100 percentage after setting this promotion");
                            return;
                        }
                        if (promotionDiscountType == 2 && discountAmount + promotionDiscount > productlist.get(position).getPriceInDouble()) {
                            Log.e("discount", "discount amount is high " + discountAmount + promotionDiscount);
                            //discount amount is more
                            holder.cb_button.setChecked(false);
                            UIMsgs.showToast(context, "This item will not available for this promotion due to product discount amount is more than actual product amount after setting this promotion");
                            return;
                        }
                    }

                    productModel.getSelectedvarientlist().get(position).setIsselect(true);
                    if (context instanceof AddPromocard)
                        ((AddPromocard) context).setProduct(productModel.getSelectedvarientlist().get(position));
                    isSelected = true;
                } else {
                    //  productModel.getSelectedvarientlist().get(position).setIsselect(false);
                    if (isPromotioProducts) {
                        productModel.getSelectedvarientlist().get(position).setIsselect(false);
                        isSelected = selectionMode != 0 && isSelected;
                    }
                }
                //   ((AddPromocard)context).setonProductvarient(productid,productlist.get(position));
            }
        });
    }

    public  void setrefresh(ArrayList<ProductAdd> productlist){
        this.productlist=productlist;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return productlist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView item_name;
        private final TextView item_price;
        private final TextView item_stock;
        private final TextView item_discount;
        private final TextView item_weight;
        private final TextView tv_pprice;
        private CardView cardview_item;
        private final CheckBox cb_button;
        private final LinearLayout layout_change;
        private final LinearLayout layout_discount;
        RelativeLayout layout_pricing;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_price=itemView.findViewById(R.id.item_price);
            item_name=itemView.findViewById(R.id.item_name);
            item_stock=itemView.findViewById(R.id.item_stock);
            item_discount=itemView.findViewById(R.id.item_discount);
           // cardview_item=itemView.findViewById(R.id.cardview_item);
            cb_button=itemView.findViewById(R.id.cb_button);
            layout_change=itemView.findViewById(R.id.layout_change);
            item_weight=itemView.findViewById(R.id.item_weight);
            tv_pprice=itemView.findViewById(R.id.tv_pprice);
            layout_discount=itemView.findViewById(R.id.layout_discount);
            layout_pricing=itemView.findViewById(R.id.layout_pricing);

        }
    }
}
