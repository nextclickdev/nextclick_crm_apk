package com.nextclick.crm.promocodes.adatpetrs;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.nextclick.crm.R;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;
import com.nextclick.crm.subcriptions.FeaturesModel;
import com.nextclick.crm.subcriptions.PackageFeatureModel;

import java.util.ArrayList;


    public class SubscriptionSettingAdapter extends RecyclerView.Adapter<SubscriptionSettingAdapter.ViewHolder> {
        private final Boolean isKeyName;
        android.content.Context context;
        int posi;
        ArrayList<FeaturesModel> listOFNotificationDetails;
        public MediaPlayer mp;
        SharedPreferences preferenceManager;

        public SubscriptionSettingAdapter(android.content.Context context, ArrayList<FeaturesModel> listOFNotificationDetails, Boolean isKeyName) {
            this.context =context;
            this.listOFNotificationDetails =listOFNotificationDetails;
            preferenceManager = PreferenceManager.getDefaultSharedPreferences(context);
            this.isKeyName=isKeyName;
            this.posi = posi;
        }

        @NonNull
        @Override
        public SubscriptionSettingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView;
            if (isKeyName)
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_privilege, parent, false);
            else
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.show_privilege_value, parent, false);
            return new SubscriptionSettingAdapter.ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull SubscriptionSettingAdapter.ViewHolder holder, int position) {

            if(position == 0)
                holder.tv_name.setVisibility(View.GONE);
            else
                holder.tv_name.setVisibility(View.VISIBLE);
            holder.tv_name.setText(listOFNotificationDetails.get(position).getDescription());
           /* if (listOFNotificationDetails.get(position).getStatus()){
                holder.chk_state.setChecked(true);
            }else {
                holder.chk_state.setChecked(false);
            }*/
            holder.chk_state.setVisibility(View.GONE);
            holder.unchk_state.setVisibility(View.GONE);

        }

        @Override
        public int getItemCount() {
            return listOFNotificationDetails!=null? listOFNotificationDetails.size(): 0;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            private final ImageView chk_state;
            private final ImageView unchk_state;
            TextView tv_name;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                tv_name = itemView.findViewById(R.id.tv_name);
                chk_state = itemView.findViewById(R.id.chk_state);
                unchk_state= itemView.findViewById(R.id.unchk_state);
            }
        }
    }

