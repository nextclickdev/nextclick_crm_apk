package com.nextclick.crm.promocodes.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PromocodesModel implements Serializable {
    String id,promo_title,promo_code,promo_type,valid_from,valid_to,category,shop_by_category,menu,
            brand,promo_image,discount_type,discount,uses,created_user_id,created_at,status,promocode_status;

    ArrayList<PromoProducts> productslist;

    public String getPromocode_status() {
        return promocode_status;
    }

    public void setPromocode_status(String promocode_status) {
        this.promocode_status = promocode_status;
    }

    public ArrayList<PromoProducts> getProductslist() {
        return productslist;
    }

    public void setProductslist(ArrayList<PromoProducts> productslist) {
        this.productslist = productslist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPromo_title() {
        return promo_title;
    }

    public void setPromo_title(String promo_title) {
        this.promo_title = promo_title;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public void setPromo_code(String promo_code) {
        this.promo_code = promo_code;
    }

    public String getPromo_type() {
        return promo_type;
    }

    public void setPromo_type(String promo_type) {
        this.promo_type = promo_type;
    }

    public String getValid_from() {
        return valid_from;
    }

    public void setValid_from(String valid_from) {
        this.valid_from = valid_from;
    }

    public String getValid_to() {
        return valid_to;
    }

    public void setValid_to(String valid_to) {
        this.valid_to = valid_to;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getShop_by_category() {
        return shop_by_category;
    }

    public void setShop_by_category(String shop_by_category) {
        this.shop_by_category = shop_by_category;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getPromo_image() {
        return promo_image;
    }

    public void setPromo_image(String promo_image) {
        this.promo_image = promo_image;
    }

    public String getDiscount_type() {
        return discount_type;
    }

    public void setDiscount_type(String discount_type) {
        this.discount_type = discount_type;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getUses() {
        return uses;
    }

    public void setUses(String uses) {
        this.uses = uses;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String vendor_user_id,vendor_name,cover_image,is_scratched,vendor_id;
    public String getIs_scratched() {
        return is_scratched;
    }

    public void setIs_scratched(String is_scratched) {
        this.is_scratched = is_scratched;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public String getVendor_user_id() {
        return vendor_user_id;
    }

    public void setVendor_user_id(String vendor_user_id) {
        this.vendor_user_id = vendor_user_id;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }
}
