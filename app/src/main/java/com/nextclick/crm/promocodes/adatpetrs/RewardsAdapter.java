package com.nextclick.crm.promocodes.adatpetrs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.promocodes.PromocodelistFragment;
import com.nextclick.crm.promocodes.model.GlideBlurTransformation;
import com.nextclick.crm.promocodes.model.PromocodesModel;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RewardsAdapter extends RecyclerView.Adapter<RewardsAdapter.ViewHolder> {

    Context context;
    ArrayList<PromocodesModel> rewardslist;
    int whichActivity;
    private final CustomDialog customDialog;

    PromocodelistFragment promocodelistFragment;


    public RewardsAdapter(Context context, ArrayList<PromocodesModel> rewardslist,int whichActivity,PromocodelistFragment promocodelistFragment) {
        this.context=context;
        this.rewardslist=rewardslist;
        this.whichActivity=whichActivity;
        this.promocodelistFragment = promocodelistFragment;
        customDialog=new CustomDialog(context);
    }

    @NonNull
    @Override
    public RewardsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.rewards_adapter_layout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new RewardsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RewardsAdapter.ViewHolder holder, int position) {

          if (rewardslist.get(position).getPromocode_status().equalsIgnoreCase("Active")){
            holder.layout_expire.setVisibility(View.GONE);
        }else {
              holder.layout_expire.setVisibility(View.VISIBLE);
          }

        if (position==0){
            holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_four));
        }else {
            int value=position%4;
            if (value==1){
                holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_one));
            }else if (value==2){
                holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_two));
            }else if (value==3){
                holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_three));
            }else if (value==0){
                holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_four));
            }else {
                holder.mScratchCard.setBackgroundColor(context.getResources().getColor(R.color.scarch_one));
            }
            // System.out.println("aaaaaaaaaaaaaa  value  "+value);
        }
        //  System.out.println("aaaaaaa productimage "+rewardslist.get(position).getProductslist().get(0).getImage());
        // try{
        Glide.with(context)
                .load(rewardslist.get(position).getProductslist().get(0).getImage())
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(holder.img_blur_product);
           /* try{
                holder.img_blur_product.invalidate();
                BitmapDrawable drawable = (BitmapDrawable) holder.img_blur_product.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                Bitmap blurredBitmap = BlurBuilder.blur( context, bitmap );
                System.out.println("aaaaaaa bitmap "+bitmap);
                holder.img_blur_product1.setVisibility(View.GONE);
                holder.img_blur_product.setImageBitmap(blurredBitmap);
            }catch (NullPointerException e){

            }*/


      /*  BitmapDrawable drawable = (BitmapDrawable) holder.img_blur_product1.getDrawable();
        Bitmap bitmap = drawable.getBitmap();*/



        //   Bitmap blurredBitmap = BlurBuilder.blur( context, bitmap );

        //  holder.img_blur_product.setBackgroundDrawable(new BitmapDrawable( context.getResources(), blurredBitmap ));
       /* }catch (IndexOutOfBoundsException e){

        }*/
       // holder.tv_vendor_name.setText(rewardslist.get(position).getVendor_name());
        boolean isScrached=true;//rewardslist.get(position).getIs_scratched().equalsIgnoreCase("1")-always
        if (isScrached){
           // holder.mScratchCard.setVisibility(View.GONE);
            holder.img_blur_product1.setVisibility(View.GONE);
            if (rewardslist.get(position).getDiscount_type().equalsIgnoreCase("2")){
                holder.tv_amount.setText(rewardslist.get(position).getDiscount()+" %Off");
            }else {
                holder.tv_amount.setText("₹ "+rewardslist.get(position).getDiscount());
            }
            /*Glide.with(context)
                    .load(rewardslist.get(position).getProductslist().get(0).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.img_blur_product);*/
            holder.img_blur_product1.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(rewardslist.get(position).getProductslist().get(0).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    //.transform(new GlideBlurTransformation(context))
                    .into(holder.img_blur_product1);
        }else {
            holder.img_blur_product1.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(rewardslist.get(position).getProductslist().get(0).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .transform(new GlideBlurTransformation(context))
                    .into(holder.img_blur_product1);
        }

        holder.tv_expire.setText("Expiry on "+rewardslist.get(position).getValid_to());
        holder.mScratchCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // setRewardclick(rewardslist.get(position),position);
            }
        });
        holder.layout_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent=new Intent(context,RewardDetailsActivity.class);
                intent.putExtra("rewardssmodel",rewardslist.get(position));
                context.startActivity(intent);*/
            }
        });
        //  holder.mScratchCard.setScratchDrawable(context.getResources().getDrawable(R.drawable.reward));
       /* holder.mScratchCard.setOnScratchListener(new ScratchCard.OnScratchListener() {
            @Override
            public void onScratch(ScratchCard scratchCard, float visiblePercent) {
                if (visiblePercent > 0.3) {
                    holder.mScratchCard.setVisibility(View.GONE);
                }
            }
        });*/

        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder alertDialogBuilder = new
                        android.app.AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure want to delete?c");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        promocodelistFragment.setdeletePromocode(rewardslist.get(position));
                    }
                });
                alertDialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });

        holder.tv_vendor_name.setText(rewardslist.get(position).getProductslist().get(0).getName());
        holder.tv_name.setText(rewardslist.get(position).getProductslist().get(0).getName());
    }



    @Override
    public int getItemCount() {
        return rewardslist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}

    public void setchanged(ArrayList<PromocodesModel> rewardslist) {
        this.rewardslist=rewardslist;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        // private ScratchCard mScratchCard;
        private final LinearLayout layout_details;
        private final ImageView mScratchCard;
        private final ImageView img_blur_product;
        private final ImageView img_blur_product1;
        private final ImageView img_delete;
        private final TextView tv_name;
        private final TextView tv_amount;
        private final TextView tv_expire;
        private final TextView tv_vendorname;
        private final TextView tv_vendor_name;
        private final RelativeLayout layout_expire;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mScratchCard=itemView.findViewById(R.id.scratchCard);
            layout_details=itemView.findViewById(R.id.layout_details);
            tv_name=itemView.findViewById(R.id.tv_name);
            tv_amount=itemView.findViewById(R.id.tv_amount);
            tv_expire=itemView.findViewById(R.id.tv_expire);
            tv_vendorname=itemView.findViewById(R.id.tv_vendorname);
            tv_vendor_name=itemView.findViewById(R.id.tv_vendor_name);
            img_blur_product=itemView.findViewById(R.id.img_blur_product);
            img_blur_product1=itemView.findViewById(R.id.img_blur_product1);
            layout_expire = itemView.findViewById(R.id.layout_expire);
            img_delete = itemView.findViewById(R.id.img_delete);
        }
    }

    Bitmap drawable_from_url(String url) throws java.io.IOException {
        HttpURLConnection connection = (HttpURLConnection)new URL(url) .openConnection();
        connection.setRequestProperty("User-agent","Mozilla/4.0");
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }
    public static Drawable LoadImageFromWebURL(String url) {
        try {
            InputStream iStream = (InputStream) new URL(url).getContent();
            Drawable drawable = Drawable.createFromStream(iStream, "scarchname");
            return drawable;
        } catch (Exception e) {
            System.out.println("aaaaaaaaa catch  "+e.getMessage());
            return null;
        }}


}

