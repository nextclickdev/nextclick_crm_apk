package com.nextclick.crm.promocodes.adatpetrs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.promocodes.activity.AddPromocard;
import com.bumptech.glide.Glide;
import com.nextclick.crm.promocodes.model.ScarchCardModel;

import java.util.ArrayList;
import java.util.List;

public class ScarchCardAdapter extends RecyclerView.Adapter<ScarchCardAdapter.ViewHolder> {

    private final Context context;
    ArrayList<ScarchCardModel> scarchcardlist;
    int[] colors ={R.color.green_100,R.color.blue,R.color.red,R.color.orange,R.color.Brown};


    public ScarchCardAdapter(Context mContext, ArrayList<ScarchCardModel> scarchcardlist) {
        this.context=mContext;
        this.scarchcardlist=scarchcardlist;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.scarchcard_adapter, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (scarchcardlist.get(position).isSelect()){
            holder.layout_select.setVisibility(View.VISIBLE);
        }else {
            holder.layout_select.setVisibility(View.GONE);
        }

        try{
            Glide.with(context)
                    .load(scarchcardlist.get(position).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.img_scarch);
        }catch (NullPointerException e){

        }

      //  holder.layout_inner.setBackgroundColor(colors[position]);

        holder.layout_inner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AddPromocard)context).setScarchcard(scarchcardlist.get(position));

            }
        });

    }

    @Override
    public int getItemCount() {
        return scarchcardlist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void setchanged(ArrayList<ScarchCardModel> scarchcardlist) {
        this.scarchcardlist=scarchcardlist;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private final LinearLayout layout_select;
        private final LinearLayout layout_inner;
        private final ImageView img_scarch;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout_select=itemView.findViewById(R.id.layout_select);
            layout_inner=itemView.findViewById(R.id.layout_inner);
            img_scarch=itemView.findViewById(R.id.img_scarch);


        }

    }
}

