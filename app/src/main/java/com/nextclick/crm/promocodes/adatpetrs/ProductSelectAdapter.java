package com.nextclick.crm.promocodes.adatpetrs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.promocodes.activity.AddPromocard;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.PRODUCTDETAILS;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class ProductSelectAdapter extends RecyclerView.Adapter<ProductSelectAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private List<ProductModel> data,promocodesList1;
    private final List<ProductModel> data_full;
    private final int currentposition;
    private int checkupdate;
    private final CustomDialog customDialog;
    private ArrayList<ProductAdd> varientlist;
    private final PreferenceManager preferenceManager;

    public ProductSelectAdapter(Context context, List<ProductModel> itemPojos, int currentposition) {
        this.context = context;
        this.data = itemPojos;
        this.currentposition = currentposition;
        data_full = new ArrayList<>(itemPojos);
        customDialog=new CustomDialog(context);
        preferenceManager=new PreferenceManager(context);
    }

    public ProductSelectAdapter(Context mContext, ArrayList<ProductModel> promocodesList, int i,
                                int checkupdate, ArrayList<ProductModel> promocodesList1) {
        this.context = mContext;
        this.data = promocodesList;
        this.currentposition = i;
        this.checkupdate = checkupdate;
        this.promocodesList1 = promocodesList1;
        data_full = new ArrayList<>(promocodesList);
        customDialog=new CustomDialog(context);
        preferenceManager=new PreferenceManager(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (currentposition==0){
            View itemView = LayoutInflater.from(context).inflate(R.layout.product_select_adpter, parent, false);
            return new ViewHolder(itemView);
        }else {
            View itemView = LayoutInflater.from(context).inflate(R.layout.product_selected_promocode, parent, false);
            return new ViewHolder(itemView);
        }

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final ProductModel productModel = data.get(position);
        holder.recycle_varients.setLayoutManager(new GridLayoutManager(context,1));


        if(holder.img_delete!=null) {
            holder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof AddPromocard)
                    {
                        ((AddPromocard)context).deleteProduct(position);
                    }
                }
            });
        }


        if (checkupdate==1){

            if (productModel.isSetpromocode()){
                holder.cb_checkproduct.setChecked(true);
                holder.cb_checkproduct.setEnabled(false);
                holder.cb_checkproduct.setClickable(false);
            }else {
                holder.cb_checkproduct.setChecked(false);
            }

        }


        if (currentposition==1){

                holder.cb_checkproduct.setVisibility(View.GONE);
                holder.recycle_varients.setVisibility(View.VISIBLE);
                holder.recycle_varients.setLayoutManager(new GridLayoutManager(context,1));
                varientlist=new ArrayList<>();
                try {
                    for (int k=0;k<productModel.getSelectedvarientlist().size();k++){
                        if (productModel.getSelectedvarientlist().get(k).isIsselect()){
                            System.out.println("aaaaaa k  "+k);
                            varientlist.add(productModel.getSelectedvarientlist().get(k));
                        }
                    }
                }catch (NullPointerException e){
                }
                VarientsAdapter varientsAdapter=new VarientsAdapter(context,productModel,varientlist,1,checkupdate);
                holder.recycle_varients.setAdapter(varientsAdapter);
                holder.ivProductImage.setVisibility(View.GONE);


        }else {

        }
        if (checkupdate==1){

            holder.recycle_varients.setVisibility(View.VISIBLE);
            holder.recycle_varients.setLayoutManager(new GridLayoutManager(context,1));
            varientlist=new ArrayList<>();
            System.out.println("aaaaaaaa  "+promocodesList1.size());
            try {
                if(promocodesList1!=null) {
                    for (int i = 0; i < promocodesList1.size(); i++) {
                        if (promocodesList1.get(i).getId().equalsIgnoreCase(data.get(position).getId())) {
                            holder.recycle_varients.setVisibility(View.VISIBLE);
                            System.out.println("aaaaaaaa  " + promocodesList1.get(i).getSelectedvarientlist().size());
                            for (int k = 0; k < promocodesList1.get(i).getSelectedvarientlist().size(); k++) {
                                if (promocodesList1.get(i).getSelectedvarientlist().get(k).isIsselect()) {
                                    getProductToUpdate(data.get(position), holder);
                                    //  System.out.println("aaaaaa k  "+promocodesList1.get(i).getSelectedvarientlist().get(k).getSizecolor());
                                    //  varientlist.add(promocodesList1.get(i).getSelectedvarientlist().get(k));
                                }
                            }
                            // VarientsAdapter varientsAdapter=new VarientsAdapter(context,productModel,varientlist,1,checkupdate);
                            //  holder.recycle_varients.setAdapter(varientsAdapter);
                        } else {
                            //  holder.recycle_varients.setVisibility(View.GONE);
                        }

                    }
                }

            }catch (NullPointerException e){
            }

            holder.ivProductImage.setVisibility(View.GONE);

        }
        String productName = productModel.getName().substring(0, 1).toUpperCase() + productModel.getName().substring(1);
        String productDescription = productModel.getDesc().substring(0, 1).toUpperCase()
                + productModel.getDesc().substring(1);

        String menuName = productModel.getMenu_name();
        try{
            String productImageUrl = productModel.getImagelist().get(0).getImage();
            if (productImageUrl != null) {
                if (!productImageUrl.isEmpty()) {
                    Glide.with(context)
                            .load(productImageUrl)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivProductImage);
                } else {

                    Glide.with(context)
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .into(holder.ivProductImage);
                }
            } else {
                Glide.with(context)
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .into(holder.ivProductImage);
            }

        }catch (NullPointerException e){
            System.out.println("aaaaaa  catch adapter "+e.getMessage());
        }

        String categoryName = productModel.getCategoryName();

        holder.tvMenu.setText(menuName);
        holder.tvCategory.setText(categoryName);
        holder.tvProductName.setText(productName);
        holder.tvProductDescription.setText("Category : "+categoryName);
      //  holder.tvProductDescription.setText(Html.fromHtml(productDescription));
      //  holder.tvPrice.setText(context.getString(R.string.price_symbol) + " " + productPrice);
     //   holder.tvDiscount.setText(context.getString(R.string.price_symbol) + " " + discountPrice);



        if (currentposition==0){
           /* holder.cb_checkproduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("aaaaaaaaaa check "+data.get(position).isCheckproductselect());
                    if (data.get(position).isCheckproductselect()){
                        for (int j=0;j<data.get(position).getSelectedvarientlist().size();j++){
                            data.get(position).getSelectedvarientlist().get(j).setIsselect(false);
                        }
                        holder.recycle_varients.setVisibility(View.GONE);
                        data.get(position).setSetpromocode(false);
                    }else{

                        System.out.println("aaaaaaaaaaaaaa check  "+ holder.cb_checkproduct.isChecked());
                        for (int j=0;j<data.size();j++){
                            data.get(j).setCheckproductselect(false);
                        }
                        data.get(position).setCheckproductselect(true);
                        for (int j=0;j<data.size();j++){
                            if (data.get(j).isCheckproductselect()){
                                holder.cb_checkproduct.setChecked(true);
                            }else{
                                holder.cb_checkproduct.setChecked(false);
                            }
                        }
                        holder.recycle_varients.setVisibility(View.VISIBLE);
                        getProductToUpdate(data.get(position),holder);
                        data.get(position).setSetpromocode(true);
                    }


                }
            });*/
            holder.cb_checkproduct.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    System.out.println("aaaaaaaaaaa check "+isChecked);
                    if (isChecked){
                        for (int j=0;j<data.size();j++){
                            data.get(position).setCheckproductselect(false);
                        }
                        data.get(position).setCheckproductselect(true);
                       /* for (int j=0;j<data.size();j++){
                            if (data.get(j).isCheckproductselect()){
                                holder.cb_checkproduct.setChecked(true);
                            }else{
                                holder.cb_checkproduct.setChecked(false);
                            }
                        }*/
                        holder.recycle_varients.setVisibility(View.VISIBLE);
                        getProductToUpdate(data.get(position),holder);
                        data.get(position).setSetpromocode(true);



                    }else {
                        for (int j=0;j<data.get(position).getSelectedvarientlist().size();j++){
                            data.get(position).getSelectedvarientlist().get(j).setIsselect(false);
                        }
                        holder.recycle_varients.setVisibility(View.GONE);
                        data.get(position).setSetpromocode(false);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ProductModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ProductModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            try {
                data.clear();
                data.addAll((List) results.values);
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void setchanged(ArrayList<ProductModel> productModelsList) {
        this.data=productModelsList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private final ImageView ivProductImage;
        private final TextView tvProductName;
        private final TextView tvProductDescription;
        private final TextView tvCategory;
        private final TextView tvMenu;
        private final TextView tvPrice;
        private final TextView tvDiscount;
        private final TextView tvProductAvailability;
        private final TextView tv_varient_header;
        CheckBox cb_checkproduct;
        private final RecyclerView recycle_varients;
        ImageView img_delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvMenu = itemView.findViewById(R.id.tvMenu);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);
            tvProductAvailability = itemView.findViewById(R.id.tvProductAvailability);
            cb_checkproduct = itemView.findViewById(R.id.cb_checkproduct);
            recycle_varients = itemView.findViewById(R.id.recycle_varients);
            tv_varient_header= itemView.findViewById(R.id.tv_varient_header);
            img_delete =itemView.findViewById(R.id.img_delete);
        }

    }

    private void getProductToUpdate(ProductModel productModel, ViewHolder holder) {
        System.out.println("aaaaa productid  "+productModel.getId());
        varientlist=new ArrayList<>();
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PRODUCTDETAILS + productModel.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        customDialog.cancel();
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa jsonobject  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject dataObject = jsonObject.getJSONObject("data");

                                    JSONArray sectionsarray=dataObject.getJSONArray("sections");
                                    JSONObject sectionobject=sectionsarray.getJSONObject(0);


                                    JSONArray sectionarray=dataObject.getJSONArray("vendor_product_varinats");
                                    varientlist.clear();
                                    for (int i=0;i<sectionarray.length();i++){
                                        JSONObject jsonObject1=sectionarray.getJSONObject(i);

                                        ProductAdd productAdd=new ProductAdd();
                                        productAdd.setPrice(jsonObject1.getString("price"));
                                        productAdd.setSizecolor(jsonObject1.getString("section_item_name"));
                                        productAdd.setId(jsonObject1.getString("id"));
                                        productAdd.setItem_id(jsonObject1.getString("item_id"));
                                        productAdd.setStatus(jsonObject1.getInt("status"));

                                        productAdd.setSku(jsonObject1.getString("sku"));
                                        productAdd.setDiscount(jsonObject1.getString("discount"));
                                        productAdd.setStock(jsonObject1.getString("stock"));
                                        productAdd.setWeight(jsonObject1.getString("weight"));
                                        productAdd.setIsselect(false);
                                        if (checkupdate==1){
                                            if(promocodesList1!=null) {
                                                System.out.println("aaaaaaaaa size  " + promocodesList1.size());
                                                for (int j = 0; j < promocodesList1.size(); j++) {
                                                    System.out.println("aaaaaaaaa list size  " + promocodesList1.get(j).getSelectedvarientlist().size());
                                                    for (int k = 0; k < promocodesList1.get(j).getSelectedvarientlist().size(); k++) {
                                                        System.out.println("aaaaaaaa check  " + promocodesList1.get(j).getSelectedvarientlist().get(k).getId() + "  " + jsonObject1.getString("id"));
                                                        if (promocodesList1.get(j).getSelectedvarientlist().get(k).getId().equalsIgnoreCase(jsonObject1.getString("id"))) {
                                                            productAdd.setIsselect(true);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                       // productAdd.setIsselect(false);
                                        if (jsonObject1.getInt("status")==1){
                                            varientlist.add(productAdd);
                                        }

                                        //   }
                                    }
                                    productModel.setSelectedvarientlist(varientlist);
                                    VarientsAdapter varientsAdapter=new VarientsAdapter(context,productModel,varientlist,0,checkupdate);
                                    holder.recycle_varients.setAdapter(varientsAdapter);
                                    holder.tv_varient_header.setVisibility(View.VISIBLE);


                                } else {
                                    UIMsgs.showToast(context, OOPS);
                                    holder.tv_varient_header.setVisibility(View.GONE);
                                }


                            } catch (Exception e) {
                                System.out.println("aaaaaa catch  "+e.getMessage());
                                e.printStackTrace();
                                UIMsgs.showToast(context, OOPS);
                                holder.tv_varient_header.setVisibility(View.GONE);
                            }

                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                        if(LoadingDialog.dialog!=null)
                            LoadingDialog.dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(LoadingDialog.dialog!=null)
                    LoadingDialog.dialog.dismiss();
                customDialog.cancel();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}

