package com.nextclick.crm.promocodes.model;

import java.io.Serializable;

public class PromoProducts implements Serializable {
    String promotion_code_id,id,product_id,vendor_product_variant_id,name,desc,varient_name,image;

    public String getVarient_name() {
        return varient_name;
    }

    public void setVarient_name(String varient_name) {
        this.varient_name = varient_name;
    }

    public String getPromotion_code_id() {
        return promotion_code_id;
    }

    public void setPromotion_code_id(String promotion_code_id) {
        this.promotion_code_id = promotion_code_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getVendor_product_variant_id() {
        return vendor_product_variant_id;
    }

    public void setVendor_product_variant_id(String vendor_product_variant_id) {
        this.vendor_product_variant_id = vendor_product_variant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}

