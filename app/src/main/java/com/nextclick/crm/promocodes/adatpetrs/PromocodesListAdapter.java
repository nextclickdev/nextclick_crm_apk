package com.nextclick.crm.promocodes.adatpetrs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Common.Activities.RegisterVendorActivity;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.promocodes.PromocodelistFragment;
import com.nextclick.crm.promocodes.activity.AddPromocard;
import com.nextclick.crm.promocodes.model.PromocodesModel;
import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PromocodesListAdapter extends RecyclerView.Adapter<PromocodesListAdapter.ViewHolder> {

    private final Context context;
    private List<PromocodesModel> data;
    PromocodelistFragment promocodelistFragment;


    public PromocodesListAdapter(Context context, List<PromocodesModel> itemPojos, PromocodelistFragment promocodelistFragment) {
        this.context = context;
        this.data = itemPojos;
        this.promocodelistFragment = promocodelistFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(context).inflate(R.layout.promocodelist_adapter, parent, false);
            return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.tv_title.setText(context.getString(R.string.title_cap)+" : "+data.get(position).getPromo_title());
        holder.tv_promocode.setText(""+data.get(position).getPromo_code());
        holder.tv_expdate.setText(context.getString(R.string.exp_cap)+" :"+data.get(position).getValid_to());
        holder.tv_status.setText(context.getString(R.string.status_cap)+" : "+data.get(position).getPromocode_status());

        if (data.get(position).getDiscount_type().equalsIgnoreCase("1")){
            holder.tv_discount.setText("₹ "+data.get(position).getDiscount());
        }else {
            holder.tv_discount.setText(data.get(position).getDiscount()+"%Off");
        }
        if (data.get(position).getPromocode_status().equalsIgnoreCase("Active")){
            holder.layout_expire.setVisibility(View.GONE);
        }else {
            holder.layout_expire.setVisibility(View.VISIBLE);
        }
        /*if (data.get(position).getStatus().equalsIgnoreCase("1")){
            holder.tv_status.setText("status : Active");
        }else {
            holder.tv_status.setText("status : In-Active");
            holder.tv_status.setTextColor(context.getResources().getColor(R.color.red));
        }*/


        holder.img_edit.setVisibility(View.GONE);
        holder.img_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder alertDialogBuilder = new
                        android.app.AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("Are you sure want to delete?");
                alertDialogBuilder.setCancelable(true);
                alertDialogBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        promocodelistFragment.setdeletePromocode(data.get(position));
                    }
                });
                alertDialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });
        holder.img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent intent=new Intent(context, AddPromocard.class);
              intent.putExtra("whichactivity",1);
              intent.putExtra("data", data.get(position));

              context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    public void setchanged(ArrayList<PromocodesModel> productModelsList) {
        this.data=productModelsList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private final ImageView img_delete;
        private final ImageView img_edit;
        private final TextView tv_title;
        private final TextView tv_promocode;
        private final TextView tv_expdate;
        private final TextView tv_status;
        private final TextView tv_discount;
        private final RelativeLayout layout_promo;
        private final RelativeLayout layout_expire;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            img_delete = itemView.findViewById(R.id.img_delete);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_promocode = itemView.findViewById(R.id.tv_promocode);
            tv_expdate = itemView.findViewById(R.id.tv_expdate);
            tv_status = itemView.findViewById(R.id.tv_status);
            layout_promo = itemView.findViewById(R.id.layout_promo);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            img_edit = itemView.findViewById(R.id.img_edit);
            layout_expire = itemView.findViewById(R.id.layout_expire);


        }

    }
}

