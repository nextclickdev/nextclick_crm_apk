package com.nextclick.crm.promocodes.model;


public class SubscriptionSetting {

    private String serviceKey;
    private String serviceName;
    private Boolean status;

    public SubscriptionSetting(String serviceKey, String serviceName,Boolean status) {
        this.serviceKey = serviceKey;
        this.serviceName = serviceName;
        this.status=status;
    }

    public String getServiceKey() {
        return serviceKey;
    }

    public void setServiceKey(String serviceKey) {
        this.serviceKey = serviceKey;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}