package com.nextclick.crm.Utilities.netoworkutil;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NetworkRequest {

    //getApplicationContext()
    public void makeRequest(Context context, final String url, HashMap<String, String> headers , final VolleyCallback callback) {

        NetworkManager rq = new NetworkManager(Request.Method.GET,
                url, null,null,
                new Response.Listener() {
                    @Override
                    public void onResponse(Object response) {
                        Log.v("Response", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            callback.onSuccess(jsonObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v("Response", error.toString());
                        String err = null;
                        if (error instanceof com.android.volley.NoConnectionError){
                            err = "No internet Access!";
                        }
                        try {
                            if(err != "null") {
                                callback.onError(err);
                            }
                            else {
                                callback.onError(error.toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };
        rq.setPriority(Request.Priority.HIGH);
        VolleyController.getInstance(context).addToRequestQueue(rq);
    }
}
