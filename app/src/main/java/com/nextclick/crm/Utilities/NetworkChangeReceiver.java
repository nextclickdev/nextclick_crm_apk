package com.nextclick.crm.Utilities;


import static com.nextclick.crm.Utilities.NetworkUtil.TYPE_NOT_CONNECTED;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.nextclick.crm.R;

public class NetworkChangeReceiver extends BroadcastReceiver {

    AlertDialog alertDialog;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        Integer status = NetworkUtil.getConnectivityStatus(context);
        Log.e("NetworkChangeReceiver","NetworkChangeReceiver "+ status);
        try {
            if(status.equals(TYPE_NOT_CONNECTED)) {
                alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle(context.getString(R.string.no_internet_header));
                alertDialog.setMessage(context.getString(R.string.no_internet_info));
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setButton(context.getString(R.string.exit), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });

                alertDialog.setCancelable(false);
                alertDialog.show();
            }
            else if(alertDialog!=null)
            {
                alertDialog.dismiss();
            }
        } catch (Exception e) {
        }
    }
}