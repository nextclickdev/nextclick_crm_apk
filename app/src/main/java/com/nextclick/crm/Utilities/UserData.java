package com.nextclick.crm.Utilities;

import android.content.Context;


import com.nextclick.crm.ShopNowModule.Models.ServicesPojo;

import java.util.ArrayList;
import java.util.LinkedList;

public class UserData {

    private static UserData userData = null;

    public static  boolean isNewDesign = true;

    private Context context;
    private LinkedList<Integer> listOfSelectedDayIds;
    private LinkedList<String> listOfSelectedDayNames;

    private ArrayList<ServicesPojo> servicesList;
    private UserData() {
    }

    public static UserData getInstance() {
        if (userData == null) {
            userData = new UserData();
        }
        return userData;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public LinkedList<Integer> getListOfSelectedDayIds() {
        return listOfSelectedDayIds;
    }

    public void setListOfSelectedDayIds(LinkedList<Integer> listOfSelectedDayIds) {
        this.listOfSelectedDayIds = listOfSelectedDayIds;
    }

    public LinkedList<String> getListOfSelectedDayNames() {
        return listOfSelectedDayNames;
    }

    public void setListOfSelectedDayNames(LinkedList<String> listOfSelectedDayNames) {
        this.listOfSelectedDayNames = listOfSelectedDayNames;
    }

    public ArrayList<ServicesPojo> getServicesList() {
        return servicesList;
    }

    public void setServicesList(ArrayList<ServicesPojo> servicesList) {
        this.servicesList = servicesList;
    }
}
