package com.nextclick.crm.interfaces;

import java.util.LinkedList;

public interface DaysNameCallBack {
    void selectedDaysNames(LinkedList<Integer> listOfSelectedDayIds, LinkedList<String> listOfSelectedDayNames);
}
