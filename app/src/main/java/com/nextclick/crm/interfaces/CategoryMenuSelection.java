package com.nextclick.crm.interfaces;

public interface CategoryMenuSelection {
    void onCategoryMenuSelection(String sub_cat_id, String menu_id);
}
