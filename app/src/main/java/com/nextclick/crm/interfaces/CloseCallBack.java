package com.nextclick.crm.interfaces;

public interface CloseCallBack {
    void close();
}
