package com.nextclick.crm.Helpers.UIHelpers;

import android.content.Intent;

public interface OnIntentReceived {
    void onIntent(Intent i, int resultCode);
}
