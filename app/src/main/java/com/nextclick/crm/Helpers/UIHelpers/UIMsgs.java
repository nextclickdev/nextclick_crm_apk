package com.nextclick.crm.Helpers.UIHelpers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.nextclick.crm.R;
import com.nextclick.crm.doctorsModule.activities.AddAsYourDoctorActivity;
import com.google.android.material.snackbar.Snackbar;


//import android.support.design.widget.Snackbar;
//import android.support.v7.app.AlertDialog;

/**
 * Created by Sarath on 27/05/2020.
 */

public class UIMsgs {
    public static ProgressDialog progressDialog;
    private final Context context;

    public UIMsgs(Context mContext) {
        this.context = mContext;
    }


    public static void showToastErrorMessage(Context context, String message) {
        showToast(context, message);
    }
    public static String isNull(String str) {
        if(TextUtils.isEmpty(str)||str.equalsIgnoreCase("null"))
            return "";
        else
            return str;
    }

    public static void showToastErrorMessage(Context context, int respCode) {}/*{
       if(respCode == 1){
           showToast(context, R.string.lbl_forgot_password_failure_message);
       } else if(respCode == 2){
            showToast(context, R.string.lbl_forgot_password_success_message);
        }
       else if (respCode == 400) {
            showToast(context, R.string.bad_request);
        } else if (respCode == 401 || respCode == 403) {
            showToast(context, R.string.invalid_sign_in);
        } else if (respCode == 404) {
            showToast(context, R.string.no_resource);
        } else if (respCode == 500) {
            showToast(context, R.string.internal_server_error);
        } else if (respCode == 501) {
            showToast(context, R.string.unimplemented);
        } else if (respCode == 502) {
            showToast(context, R.string.bad_gateway);
        } else if (respCode == 503) {
            showToast(context, R.string.service_unavailable);
        } else if (respCode == 504) {
            showToast(context, R.string.server_not_reachable);
        } else if (respCode == -1) {
            showToast(context, R.string.certificate_unavailable);
        }
    }*/

    public static void showToast(Context context, int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
    public static void showToast(Context context, String msg,int LENGTH_LONG) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showProgressDialog(String message, Context mContext) {
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("loading");
        if (progressDialog != null && !progressDialog.isShowing())
            progressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public Typeface setRobotoThin() {
        Typeface font = Typeface.createFromAsset(
                context.getAssets(),
                "fonts/androidnation.ttf");
        return font;
    }

    /*@Author : Sai
     for displaying snackbars
    param1 : View view ->
    param2 : String msg ->*/
    public static void showSnackBar(View view, String msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        view = snackbar.getView();
        //TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void showSnackBar(View view, int msg) {
        Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        view = snackbar.getView();
        //TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void showDialog(Context mContext, int title, String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);

        LinearLayout parent = (LinearLayout) positiveButton.getParent();
        parent.setGravity(Gravity.CENTER_HORIZONTAL);
        View leftSpacer = parent.getChildAt(1);
        leftSpacer.setVisibility(View.GONE);
    }


    /*public static void showCustomToast(Context context, String msg , String type){

        if(type.equalsIgnoreCase(SUCCESS)) {
            Toasty.success(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(ERROR)) {
            Toasty.error(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(WARNING)) {
            Toasty.warning(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(INFO)) {
            Toasty.info(context, msg,Toasty.LENGTH_LONG).show();
        }
        if(type.equalsIgnoreCase(NORMAL)) {
            Toasty.normal(context, msg,Toasty.LENGTH_LONG).show();
        }



    }
*/
    public static void setEditTextErrorMethod(EditText editText, String err_msg){
        editText.setError(err_msg);
        editText.requestFocus();
    }

    public static void showAlert(Context context,String message) {
        android.app.AlertDialog.Builder alertDialogBuilder = new
                android.app.AlertDialog.Builder(context, R.style.MyDialogTheme);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.orange));
            }
        });

        alertDialog.show();
    }

}
