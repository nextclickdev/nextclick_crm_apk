package com.nextclick.crm.Helpers.UIHelpers;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.model.OrderStatus;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nextclick.crm.promocodes.model.SubscriptionSetting;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Utility {
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_READ_MEDIA = 124;
    private static final int REQUEST_PERMISSION_READ_MEDIA = 1;

    public static boolean ShowPendingProuctsItem;
    public static boolean RefreshMyProducts;//make it true when you want to refresh
    public static boolean RefreshProducts;//make it true when you want to refresh
    public static boolean RefreshCatelogue;//make it true when you want to refresh
    public static boolean RefreshApprovedProducts;//make it true when you want to refresh
    public static boolean isNewAuthentication = true;
    static String defaultFormat = "yyyy-MM-dd HH:mm:ss";

    public static boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;

        // Adjust the API levels according to your requirements
        int minApiLevel = Build.VERSION_CODES.LOLLIPOP; // API level 21
        int targetApiLevel = Build.VERSION_CODES.M; // API level 23

        if (currentAPIVersion >= minApiLevel && currentAPIVersion < targetApiLevel) {
            // For Android versions 21 to 22 (Lollipop to Marshmallow), use READ_EXTERNAL_STORAGE
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestStoragePermission(context);
            } else {
                // Permission already granted...
                return true;
            }
        } else if (currentAPIVersion >= targetApiLevel) {
            // For Android versions 23 and above, use media-specific permissions
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_IMAGES)
                    != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_VIDEO)
                            != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(context, Manifest.permission.READ_MEDIA_AUDIO)
                            != PackageManager.PERMISSION_GRANTED) {
              requestMediaPermissions(context);
            } else {
                // Permissions already granted...
                return true;
            }
        } else {
            // For Android versions below 21, no need to handle permissions, just proceed
            return true;
        }

        return false;
    }

    private static void requestStoragePermission(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("External storage permission is necessary");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
    }

    private static void requestMediaPermissions(final Context context) {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_MEDIA_IMAGES) ||
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_MEDIA_VIDEO) ||
                ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_MEDIA_AUDIO)) {

            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("Media storage permission is necessary");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{
                                    Manifest.permission.READ_MEDIA_IMAGES,
                                    Manifest.permission.READ_MEDIA_VIDEO,
                                    Manifest.permission.READ_MEDIA_AUDIO
                            }, MY_PERMISSIONS_REQUEST_READ_MEDIA);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{
                            Manifest.permission.READ_MEDIA_IMAGES,
                            Manifest.permission.READ_MEDIA_VIDEO,
                            Manifest.permission.READ_MEDIA_AUDIO
                    }, MY_PERMISSIONS_REQUEST_READ_MEDIA);
        }
    }
    /**
     * Round to certain number of decimals
     *
     * @param d
     * @param decimalPlace
     * @return
     */
    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    /**
     * Round to certain number of decimals
     *
     * @param d
     * @param decimalPlace
     * @return
     */
    public static Double round(Double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void setFilters(TextInputEditText inputField, InputFilter filter) {
        inputField.setFilters(new InputFilter[]{filter});
    }

    public static Date getDateFromString(String dateString) {
        return getDateFromString(dateString, defaultFormat);
    }

    public static Date getDateFromString(String dateString, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private static final String blockCharacterSet = "@";

    public static void setSpecialCharFilter(TextInputEditText inputField) {
        inputField.setFilters(new InputFilter[]{SpecialCharFilter});
    }

    private static final InputFilter SpecialCharFilter = new InputFilter() {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

            // if (source != null && blockCharacterSet.contains(("" + source.toString().substring(source.length()-1)))) {
            if (source != null && source.toString().contains(blockCharacterSet)) {
                return "";
            }
            return null;
        }
    };

    public static String getBigDecimalValue(String str) {
        return new BigDecimal(str).toString();
    }

    public static Double getBigDecimalDoubleValue(String str) {
        return new BigDecimal(str).doubleValue();
    }

    public static String roundFloat(String str, int places) {

        BigDecimal bigDecimal = new BigDecimal(str);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.toString();
    }

    public static int indexOfAny(String str, char[] searchChars) {
        if (searchChars.toString().isEmpty()) {
            return -1;
        }
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            for (int j = 0; j < searchChars.length; j++) {
                if (searchChars[j] == ch) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String UserIntent = "user";
    public static String DPIntent = "delivery_partner";
    public static String VendorIntent = "vendor";
    public static String ExecutiveIntent = "executive";

    public static void setupOtpInputs(Context mContext, EditText editTextone, EditText editTexttwo, EditText editTextthree,
                                      EditText editTextfour, EditText editTextFive, EditText editTextSix) {
        addTextChangedListener(editTextone, editTexttwo);
        addTextChangedListener(editTexttwo, editTextthree);
        addTextChangedListener(editTextthree, editTextfour);
        addTextChangedListener(editTextfour, editTextFive);
        addTextChangedListener(editTextFive, editTextSix);
        //    addTextChangedListener(editTextone,editTexttwo);

        editTextone.setOnKeyListener(new GenericKeyEvent(editTextone, null));
        editTexttwo.setOnKeyListener(new GenericKeyEvent(editTexttwo, editTextone));
        editTextthree.setOnKeyListener(new GenericKeyEvent(editTextthree, editTexttwo));
        editTextfour.setOnKeyListener(new GenericKeyEvent(editTextfour, editTextthree));
        editTextFive.setOnKeyListener(new GenericKeyEvent(editTextFive, editTextfour));
        editTextSix.setOnKeyListener(new GenericKeyEvent(editTextSix, editTextFive));
    }

    private static void addTextChangedListener(EditText editTextone, EditText editTexttwo) {
        editTextone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().trim().isEmpty())
                    editTexttwo.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static String getCurrentOrderStatus(OrderStatus orderStatus) {

        String statusId = orderStatus.getId();
        String deliveryPatnerStatus = orderStatus.getDeliveryPatnerStatus();
        boolean delBoyStatus = (deliveryPatnerStatus == null || deliveryPatnerStatus.isEmpty() || deliveryPatnerStatus.equals("null"));

        switch (statusId) {
            //user priorty checks
            case "5"://picked up
            case "16": //rejected or cancelled
            case "17": //rejected or cancelled
            case "18": //auto cancelled
                if (!delBoyStatus && deliveryPatnerStatus.equals("500")) {
                    //this will come delivery patner rejected the order when user is unavailable
                    return deliveryPatnerStatus;
                }
                break;
            case "1":
            case "9"://received
            case "11":
            case "3"://Order has been Preparing
            case "4"://ready to pickup
                return statusId;
            case "12"://out for delivery
                if (!delBoyStatus) {
                    return deliveryPatnerStatus;
                }
                break;
            default:
                break;
        }
        return statusId;
    }

    public static double getDiscount(String discount) {
        if (discount == null || discount.equals("null"))
            return 0;
        try {
            return Double.parseDouble(discount);
        } catch (Exception ex) {
            return 0;
        }
    }


    public static class GenericKeyEvent implements View.OnKeyListener {

        private final EditText currentView, previousView;

        public GenericKeyEvent(EditText currentView, EditText previousView) {
            this.currentView = currentView;
            this.previousView = previousView;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL &&
                    currentView.getId() != R.id.editTextone && currentView.getText().toString().isEmpty()) {
                //If current is empty then previous EditText's number will also be deleted
                if (previousView != null) {
                    //previousView.setText(null);
                    previousView.requestFocus();
                }
                return true;
            }
            return false;
        }
    }


    public static String listing = "listing";
    public static String vendor_shop_profile = "vendor_shop_profile";
    public static String products_listing = "products_listing";
    public static String adding_new_products = "adding_new_products";
    public static String catalogue_products = "catalogue_products";
    public static String orders_listing = "orders";//orders_listing
    public static String wallet_listing = "orders";//ideally wallet includes vendor profile
    public static String cod_accpetance = "cod_accpetance";
    public static String track_order = "track_order";
    public static String reviews_and_ratings = "reviews_and_ratings";
    public static String promotions = "promotions";
    public static String scratch_cards = "scratch_cards";
    public static String people_management = "people_management";
    public static String analytics = "analytics";
    public static String login_devices = "login_devices";
    public static String multiple_login_sessions = "multiple_login_sessions";
    public static String reports = "sales_report";
    public static String promocodes = "promocodes";
    public static String customer_feedback = "customer_feedback";

    public static ArrayList<SubscriptionSetting> single_instance = null;

    public static ArrayList<SubscriptionSetting> getSettingInstance(Context mContext) {
        if (single_instance == null) {
            loadSettings(mContext);
        }
        return single_instance;
    }

    private static void loadSettings(Context mContext) {
        PreferenceManager preferenceManager = new PreferenceManager(mContext);
        if (preferenceManager.getString("SubscriptionSettings") != null) {
            Gson gson = new Gson();
            String json = preferenceManager.getString("SubscriptionSettings");
            Type type = new TypeToken<ArrayList<SubscriptionSetting>>() {
            }.getType();
            single_instance = gson.fromJson(json, type);
        }
    }

    public static boolean isServiceOptionEnabled(ArrayList<SubscriptionSetting> SubscriptionSettingList, String key) {
        if (Skip_Subscription_Settings)
            return Skip_Subscription_Settings;
        if (SubscriptionSettingList != null && SubscriptionSettingList.size() > 0) {
            for (int i = 0; i < SubscriptionSettingList.size(); i++) {
                if (SubscriptionSettingList.get(i).getServiceKey().equals(key))
                    return SubscriptionSettingList.get(i).getStatus();
            }
        }
        return false;
    }

    public static boolean check_product_page = false;
    static boolean Skip_Subscription_Settings = false;//false-default
    public static boolean Skip_Final_PAYMENT = true;//false-default
    public static boolean isMixPanelSupport = false; //true- default
    public static boolean enable_Pay_Later = true; //false- default
}
