package com.nextclick.crm.orders.model;

public class ProductDetailsModel {
    String ecom_order_id,id,item_id,vendor_product_variant_id,qty,price,rate_of_discount,sub_total,discount,tax,total,cancellation_message,
            status,promocode_id,promocode_discount,
            promotion_banner_id,offer_product_id,offer_product_variant_id,offer_product_qty,
            promotion_banner_discount,promotion_banner_discount_type_id;

    ItemModel itemModel;
    ItemModel setOfferitemmodel;
    boolean checkstatus;

    public boolean isCheckstatus() {
        return checkstatus;
    }

    public void setCheckstatus(boolean checkstatus) {
        this.checkstatus = checkstatus;
    }

    public String getPromotion_banner_discount() {
        return promotion_banner_discount;
    }

    public void setPromotion_banner_discount(String promotion_banner_discount) {
        this.promotion_banner_discount = promotion_banner_discount;
    }

    public ItemModel getSetOfferitemmodel() {
        return setOfferitemmodel;
    }

    public void setSetOfferitemmodel(ItemModel setOfferitemmodel) {
        this.setOfferitemmodel = setOfferitemmodel;
    }

    public String getPromotion_banner_id() {
        return promotion_banner_id;
    }

    public void setPromotion_banner_id(String promotion_banner_id) {
        this.promotion_banner_id = promotion_banner_id;
    }

    public String getOffer_product_id() {
        return offer_product_id;
    }

    public void setOffer_product_id(String offer_product_id) {
        this.offer_product_id = offer_product_id;
    }

    public String getOffer_product_variant_id() {
        return offer_product_variant_id;
    }

    public void setOffer_product_variant_id(String offer_product_variant_id) {
        this.offer_product_variant_id = offer_product_variant_id;
    }

    public String getPromotion_banner_discount_type_id() {
        return promotion_banner_discount_type_id;
    }

    public void setPromotion_banner_discount_type_id(String promotion_banner_discount_type_id) {
        this.promotion_banner_discount_type_id = promotion_banner_discount_type_id;
    }

    public String getOffer_product_qty() {
        return offer_product_qty;
    }

    public void setOffer_product_qty(String offer_product_qty) {
        this.offer_product_qty = offer_product_qty;
    }

    public String getPromocode_id() {
        return promocode_id;
    }

    public void setPromocode_id(String promocode_id) {
        this.promocode_id = promocode_id;
    }

    public String getPromocode_discount() {
        return promocode_discount;
    }

    public void setPromocode_discount(String promocode_discount) {
        this.promocode_discount = promocode_discount;
    }

    public String getEcom_order_id() {
        return ecom_order_id;
    }

    public void setEcom_order_id(String ecom_order_id) {
        this.ecom_order_id = ecom_order_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getVendor_product_variant_id() {
        return vendor_product_variant_id;
    }

    public void setVendor_product_variant_id(String vendor_product_variant_id) {
        this.vendor_product_variant_id = vendor_product_variant_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRate_of_discount() {
        return rate_of_discount;
    }

    public void setRate_of_discount(String rate_of_discount) {
        this.rate_of_discount = rate_of_discount;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCancellation_message() {
        return cancellation_message;
    }

    public void setCancellation_message(String cancellation_message) {
        this.cancellation_message = cancellation_message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ItemModel getItemModel() {
        return itemModel;
    }

    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }
}
