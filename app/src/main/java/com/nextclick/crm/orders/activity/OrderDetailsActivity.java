package com.nextclick.crm.orders.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.orders.adapters.OrderItemsAdapter;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.model.CustomerModel;
import com.nextclick.crm.orders.model.DeleveryModesmodel;
import com.nextclick.crm.orders.model.DeliveryBoyObject;
import com.nextclick.crm.orders.model.DeliveryJobObject;
import com.nextclick.crm.orders.model.ImagesModel;
import com.nextclick.crm.orders.model.ItemModel;
import com.nextclick.crm.orders.model.OrderDetailsmodel;
import com.nextclick.crm.orders.model.OrderStatus;
import com.nextclick.crm.orders.model.PaymentMethod;
import com.nextclick.crm.orders.model.PaymentModel;
import com.nextclick.crm.orders.model.ProductDetailsModel;
import com.nextclick.crm.orders.model.SectionItemModel;
import com.nextclick.crm.orders.model.ShippingAddress;
import com.nextclick.crm.orders.model.VarientModel;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.nextclick.crm.Config.Config.EXTENDINGPREPARETIME;
import static com.nextclick.crm.Config.Config.ORDERACCEPT;
import static com.nextclick.crm.Config.Config.ORDERDETAILS;
import static com.nextclick.crm.Config.Config.ORDERREJECT;
import static com.nextclick.crm.Config.Config.OUTOFDELEVERY;
import static com.nextclick.crm.Config.Config.PAYMENT_LINK;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Services.InAppMessagingService.mp;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private RelativeLayout layout_discount, layout_promodiscount, layout_promotion_discount, layout_servicecharge;
    private LinearLayout status_layout;
    private String token, deliverydstatus;
    private RecyclerView itemsRecycler;
    private CardView cardview_returnotp;
    private TextView return_otp, tv_servicecharge, tv_promotiondiscount, tv_promodiscount, tv_trackid, tv_status, tv_time, tv_customername, tv_customerphone, tv_deleverymode,
            tv_deleverypartnarname, tv_deleverypartnermobile, tv_outfordelevery,
            payment_type, tv_sub_total, tv_item_grand_total_total, tv_discount, tv_tax, tv_shippingaddress, tv_preparationtime,
            tv_deliveryfee;
    private ImageView refresh_order_, order_back_image_, vehicle_type;
    private String orderId, orderbaseid = "", updatetime = "", deliverymodeid = "";
    private LinearLayout layout_delevery, layout_delevery_one, layout_customer_phonenumber, layout_customer_name;
    private CardView accept_order_card, reject_order_card, out_card, status_card, preparation_card;
    Button btn_generate_payment_link;

    private CustomDialog mCustomDialog;
    private int position;
    private FragmentInterface fragmentInterfaceListener;

    ArrayList<ProductDetailsModel> productlist;
    private int preparationtime, prearationtimest;
    private ScrollView view_scrollview;
    CountDownTimer countDownTimer, reject_countdowntimer;
    private boolean paymentStatus = true;
    String amountTobeCollected = "", orderConfirmationtime = "", rejectordercreatedat = "", reject_status = "",
            serial_number, order_status = "", order_status_details = "";
    private String trackID;
    private boolean isViewCreated;
    CheckBox tv_selectall;

    AlertDialog dialog4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_details_activity_new);

        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.Iconblue));
        }
        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to order details activity");
        }
        init();
        // getOrders();
        accept_order_card.setOnClickListener(this);
        reject_order_card.setOnClickListener(this);
        out_card.setOnClickListener(this);
        preparation_card.setOnClickListener(this);
        order_back_image_.setOnClickListener(this);
        refresh_order_.setOnClickListener(this);
        tv_preparationtime.setOnClickListener(this);

       /* chronometer.start();
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(final Chronometer chronometer) {
                timeWhenStopped = (int) (SystemClock.elapsedRealtime() - chronometer.getBase());

                timecheck=timeWhenStopped/1000;
                System.out.println("aaaaaaaaaaaa  timecheck111  "+timecheck);

                if (timecheck==20.0){

                    System.out.println("aaaaaaaaaaaa  music on ");

                }else {

                }
            }
        });*/
        isViewCreated = true;
    }

    private void init() {
        position = getIntent().getIntExtra("position", 0);
        mContext = OrderDetailsActivity.this;
        orderId = getIntent().getStringExtra("order_id");
        order_status = getIntent().getStringExtra("status");
        System.out.println("aaaaaaaaaaa order_id  " + orderId);

        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        itemsRecycler = findViewById(R.id.itemsRecycler);
        refresh_order_ = findViewById(R.id.refresh_order_);
        order_back_image_ = findViewById(R.id.order_back_image_);
        vehicle_type = findViewById(R.id.vehicle_type);
        tv_trackid = findViewById(R.id.tv_trackid);
        tv_customername = findViewById(R.id.tv_customername);
        tv_customerphone = findViewById(R.id.tv_customerphone);
        tv_deleverymode = findViewById(R.id.tv_deleverymode);
        tv_deleverypartnarname = findViewById(R.id.tv_deleverypartnarname);
        tv_deleverypartnermobile = findViewById(R.id.tv_deleverypartnermobile);
        tv_outfordelevery = findViewById(R.id.tv_outfordelevery);
        tv_time = findViewById(R.id.tv_time);
        layout_delevery = findViewById(R.id.layout_delevery);
        layout_delevery_one = findViewById(R.id.layout_delevery_one);
        payment_type = findViewById(R.id.payment_type);
        accept_order_card = findViewById(R.id.accept_order_card);
        reject_order_card = findViewById(R.id.reject_order_card);
        out_card = findViewById(R.id.out_card);
        tv_preparationtime = findViewById(R.id.tv_preparationtime);
        preparation_card = findViewById(R.id.preparation_card);
        status_card = findViewById(R.id.status_card);
        tv_status = findViewById(R.id.tv_status);
        tv_sub_total = findViewById(R.id.tv_sub_total);
        tv_shippingaddress = findViewById(R.id.tv_shippingaddress);
        tv_item_grand_total_total = findViewById(R.id.tv_item_grand_total_total);
        tv_servicecharge = findViewById(R.id.tv_servicecharge);
        tv_deliveryfee = findViewById(R.id.tv_deliveryfee);
        tv_discount = findViewById(R.id.tv_discount);
        tv_tax = findViewById(R.id.tv_tax);
        layout_discount = findViewById(R.id.layout_discount);
        status_layout = findViewById(R.id.status_layout);
        view_scrollview = findViewById(R.id.view_scrollview);
        tv_promodiscount = findViewById(R.id.tv_promodiscount);
        layout_promodiscount = findViewById(R.id.layout_promodiscount);
        layout_promotion_discount = findViewById(R.id.layout_promotion_discount);
        tv_promotiondiscount = findViewById(R.id.tv_promotiondiscount);
        layout_customer_phonenumber = findViewById(R.id.layout_customer_phonenumber);
        layout_customer_name = findViewById(R.id.layout_customer_name);
        layout_servicecharge = findViewById(R.id.layout_servicecharge);
        cardview_returnotp = findViewById(R.id.cardview_returnotp);
        return_otp = findViewById(R.id.return_otp);
        //  chronometer = (Chronometer)findViewById(R.id.chronometerExample);

        btn_generate_payment_link = findViewById(R.id.btn_generate_payment_link);
        btn_generate_payment_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePaymentOnlineLink();
            }
        });


        mCustomDialog = new CustomDialog(mContext);

        productlist = new ArrayList<>();
        accept_order_card.setVisibility(View.GONE);
        reject_order_card.setVisibility(View.GONE);
        out_card.setVisibility(View.GONE);
        preparation_card.setVisibility(View.GONE);
        if (position == 1) {
            System.out.println("aaaaaaaaa position  ");
            if (mp != null) {
                mp.stop();
                mp = null;
                //  mp.reset();
                // mp.release();
                System.out.println("aaaaaaaaa position  mpp ");
            }
        }
        //   startTimer();
        // updateCountDownText();
        // reverseTimer(1200);
    }

    public void timerset() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer.onFinish();
        }
        String[] splited = updatetime.split(" ");
        String time = splited[1];
        Date currentTime = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(currentTime.getTime());

        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");

        try {
            Date date1 = format.parse(splited[1]);

            Date date2 = format.parse("" + formattedDate);

            System.out.println("aaaaaaaaaa date1  " + date1 + "  date2  " + date2);

            long mills = date2.getTime() - date1.getTime();
            int hours = (int) (mills / (1000 * 60 * 60));
            //  int mins = (int) (mills % (1000*60*60));
            int mins = (int) ((mills / (1000 * 60)) % 60);
            long Secs = (int) (mills / 1000) % 60;
            long total = preparationtime - mins * 60 - Secs;

            System.out.println("aaaaaaa  diff  " + mins + " sec  " + Secs + " total  " + total);

            System.out.println("aaaaaaaaaa " + preparationtime);

            tv_preparationtime.setText("");

            countDownTimer = new CountDownTimer((total * 1000) + 1000, 1000) {

                public void onTick(long millisUntilFinished) {

                    int seconds = (int) (millisUntilFinished / 1000);
                    int minutes = seconds / 60;
                    seconds = seconds % 60;
                    tv_preparationtime.setText(getString(R.string.Preparation) + " : " + String.format("%02d", minutes)
                            + ":" + String.format("%02d", seconds) + " \n" + getString(R.string.update));
                }

                public void onFinish() {
                    tv_preparationtime.setText("Preparation time Completed");
                }
            }.start();

            System.out.println("aaaaaaaa splited[1]  " + splited[1]);

        } catch (ParseException e) {
            e.printStackTrace();
        }

       /* Timer updateTimer = new Timer();
        updateTimer.schedule(new TimerTask()
        {
            public void run()
            {
                try
                {
                    SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
                    Date date1 = format.parse(splited[1]);
                    Date date2 = format.parse(""+formattedDate);
                    long mills = date2.getTime() - date1.getTime();
                    Log.v("Data1", ""+date1.getTime());
                    Log.v("Data2", ""+date2.getTime());
                  //  System.out.println("aaaaaaaa date1 "+date1+"  date2  "+date2);
                    int hours = (int) (mills/(1000 * 60 * 60));
                  //  int mins = (int) (mills % (1000*60*60));
                    int mins = (int) ((mills/(1000*60)) % 60);
                    long Secs = (int) (mills / 1000) % 60;

                    String diff = hours + ":" + mins; // updated value every1 second
                    System.out.println("aaaaaa diff "+diff+" hr "+hours+" min "+mins+" sec  "+Secs);
                    tv_preparationtime.setText(diff);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

        }, 0, 1000);*/
    }

    @Override
    public void onBackPressed() {
        if (position == 1) {
            Intent i = new Intent(mContext, ShopNowHomeActivity.class);
            i.putExtra("position", 1);
            startActivity(i);
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tv_preparationtime:

                LayoutInflater inflater4 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout4 = inflater4.inflate(R.layout.supporter_reject_order, null);

                final TextInputLayout layout_reason = alertLayout4.findViewById(R.id.layout_reason);
                final TextInputLayout layout_time = alertLayout4.findViewById(R.id.layout_time);
                layout_reason.setVisibility(View.GONE);
                layout_time.setVisibility(View.VISIBLE);

                final EditText reason4 = alertLayout4.findViewById(R.id.time);
                final Button submit4 = alertLayout4.findViewById(R.id.submit);
                final TextView tv_text4 = alertLayout4.findViewById(R.id.tv_text);

                tv_text4.setText(getString(R.string.tellus_preparation_time));
                AlertDialog.Builder alert = new AlertDialog.Builder(OrderDetailsActivity.this);
                alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle(getString(R.string.update_prep_time));
                reason4.setHint(getString(R.string.In_minutes));
                reason4.setMaxLines(1);
                reason4.setMaxEms(2);

                // reason4.setText("20");
                // this is set the view from XML inside AlertDialog
                alert.setView(alertLayout4);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert.setCancelable(true);
                final AlertDialog dialog = alert.create();
                dialog.show();
                reason4.setInputType(InputType.TYPE_CLASS_NUMBER);

                int maxLength = 2;
                InputFilter[] filters = new InputFilter[1];
                filters[0] = new InputFilter.LengthFilter(maxLength);
                reason4.setFilters(filters);

                submit4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String res = reason4.getText().toString().trim();
                        int res1 = Integer.parseInt(res);

                        if (res.length() == 0) {
                            reason4.setError("Should Not Be Empty");
                            reason4.requestFocus();
                        } else if (res1 < 10) {
                            reason4.setError("Minimum 10 minutes");
                            reason4.requestFocus();
                        } else {
                            dialog.dismiss();
                            int totalpreptation = prearationtimest + Integer.parseInt(res);
                            Map<String, String> uploadMap = new HashMap<>();
                            uploadMap.put("preparation_time", "" + totalpreptation);
                            uploadMap.put("order_id", orderId);
                            JSONObject json = new JSONObject(uploadMap);
                            updatepreparationtime(json);
                        }

                    }
                });
                break;

            case R.id.accept_order_card:
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout = inflater.inflate(R.layout.supporter_reject_order, null);

                final TextInputLayout layout_reason1 = alertLayout.findViewById(R.id.layout_reason);
                final TextInputLayout layout_time1 = alertLayout.findViewById(R.id.layout_time);
                layout_reason1.setVisibility(View.GONE);
                layout_time1.setVisibility(View.VISIBLE);

                final EditText reason = alertLayout.findViewById(R.id.time);
                final Button submit = alertLayout.findViewById(R.id.submit);
                final TextView tv_text = alertLayout.findViewById(R.id.tv_text);

                tv_text.setText(getString(R.string.tellus_preparation_time));
                AlertDialog.Builder alert1 = new AlertDialog.Builder(OrderDetailsActivity.this);
                alert1.setIcon(R.mipmap.ic_launcher);
                alert1.setTitle(getString(R.string.Preparation));
                reason.setHint(getString(R.string.In_minutes));
                // reason.setText("20");
                // this is set the view from XML inside AlertDialog
                alert1.setView(alertLayout);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert1.setCancelable(true);
                final AlertDialog dialog1 = alert1.create();
                dialog1.show();
                reason.setInputType(InputType.TYPE_CLASS_NUMBER);
                int maxLength1 = 2;
                InputFilter[] filters1 = new InputFilter[1];
                filters1[0] = new InputFilter.LengthFilter(maxLength1);
                reason.setFilters(filters1);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String res = reason.getText().toString().trim();
                        if (!res.isEmpty()){
                            int res1 = Integer.parseInt(res);
                            if (res.length() == 0) {
                                reason.setError("Should Not Be Empty");
                                reason.requestFocus();
                            } else if (res1 < 10) {
                                reason.setError("Minimum 10 minutes");
                                reason.requestFocus();
                            }
                            else {
                                Map<String, String> uploadMap = new HashMap<>();
                                uploadMap.put("preparation_time", res);
                                uploadMap.put("order_id", orderId);
                                JSONObject json = new JSONObject(uploadMap);
                                acceptOrder(json, dialog1);
                            }

                        } else {
                            reason.setError("Should Not Be Empty");
                            reason.requestFocus();
                        }


                    }
                });


                break;

            case R.id.reject_order_card:

                LayoutInflater inflater1 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View alertLayout1 = inflater1.inflate(R.layout.supporter_reject_order, null);

                final EditText reason1 = alertLayout1.findViewById(R.id.reason);
                final Button submit1 = alertLayout1.findViewById(R.id.submit);
                tv_selectall = alertLayout1.findViewById(R.id.tv_selectall);
                RecyclerView recycle_products = alertLayout1.findViewById(R.id.recycle_products);
                recycle_products.setVisibility(View.VISIBLE);
                tv_selectall.setVisibility(View.VISIBLE);
                recycle_products.setLayoutManager(new GridLayoutManager(mContext, 1));
                OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(mContext, productlist, 1, deliverydstatus);
                recycle_products.setAdapter(orderItemsAdapter);
                AlertDialog.Builder alert4 = new AlertDialog.Builder(OrderDetailsActivity.this);
                alert4.setIcon(R.mipmap.ic_launcher);
                alert4.setTitle(getString(R.string.reject));
                reason1.setHint(getString(R.string.reason));
                // this is set the view from XML inside AlertDialog
                alert4.setView(alertLayout1);
                // disallow cancel of AlertDialog on click of back button and outside touch
                alert4.setCancelable(true);
                dialog4 = alert4.create();
                dialog4.show();


                tv_selectall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean checksselect = false;
                        for (int i = 0; i < productlist.size(); i++) {
                            if (!productlist.get(i).isCheckstatus()) {
                                checksselect = true;
                                break;
                            }
                        }
                        if (checksselect) {
                            for (int i = 0; i < productlist.size(); i++) {
                                productlist.get(i).setCheckstatus(true);
                            }
                            orderItemsAdapter.setchange(productlist);
                            tv_selectall.setChecked(true);
                        } else {
                            for (int i = 0; i < productlist.size(); i++) {
                                productlist.get(i).setCheckstatus(false);
                            }
                            orderItemsAdapter.setchange(productlist);
                            tv_selectall.setChecked(false);

                        }

                    }
                });

               /* tv_selectall.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                      //  if (isChecked){
                        for (int i=0;i<productlist.size();i++){
                            productlist.get(i).setCheckstatus(isChecked);
                        }
                        orderItemsAdapter.setchange(productlist);
                      *//*  }else {

                        }*//*
                    }
                });*/

                submit1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String res = reason1.getText().toString().trim();
                        if (res.length() == 0) {
                            reason1.setError("Should Not Be Empty");
                            reason1.requestFocus();
                        } else if (res.length() < 6) {
                            reason1.setError("Minimum 6 characters");
                            reason1.requestFocus();
                        } else {
                            ArrayList<ProductDetailsModel> rejectlist = new ArrayList();
                            for (int i = 0; i < productlist.size(); i++) {
                                if (productlist.get(i).isCheckstatus()) {
                                    rejectlist.add(productlist.get(i));
                                } else {
                                    // check=true;
                                }
                            }
                            if (productlist.size() == rejectlist.size()) {
                                Map<String, Object> uploadMap = new HashMap<>();
                                uploadMap.put("reason", res);
                                uploadMap.put("order_id", orderId);
                                uploadMap.put("is_total_order_rejected", 1);
                                JSONObject json = new JSONObject(uploadMap);

                                System.out.println("aaaaaaaa   " + json);
                                rejectOrder(json.toString(), dialog4);

                            } else if (rejectlist.size() != 0) {
                                JSONArray jsonArray = new JSONArray();
                                for (int k = 0; k < rejectlist.size(); k++) {
                                    JSONObject jsonObject1 = new JSONObject();
                                    try {
                                        jsonObject1.put("product_id", rejectlist.get(k).getItem_id());
                                        jsonObject1.put("product_varient_id", rejectlist.get(k).getVendor_product_variant_id());
                                        jsonArray.put(jsonObject1);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                Map<String, Object> uploadMap = new HashMap<>();
                                uploadMap.put("reason", res);
                                uploadMap.put("order_id", orderId);
                                uploadMap.put("is_total_order_rejected", 0);
                                JSONObject json = new JSONObject(uploadMap);
                                try {
                                    json.put("rejected_products", jsonArray);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("aaaaaaaa   " + json);
                                rejectOrder(json.toString(), dialog4);
                            } else {
                                Toast.makeText(mContext, "Please select the product", Toast.LENGTH_SHORT).show();
                            }

                        }

                    }
                });


                break;

            case R.id.out_card:
                showOTPDialog();
                break;

            case R.id.refresh_order_:
                isRefreshStarted = false;
                try {
                    if (countDownTimer != null) {
                        countDownTimer.onFinish();
                        countDownTimer.cancel();
                    }
                } catch (NullPointerException e) {

                }
                getOrders();
                break;

            case R.id.order_back_image_:
                onBackPressed();
                break;
        }
    }

    public void checkboxselect(int check) {
        boolean chekcstatus = true;
        for (int i = 0; i < productlist.size(); i++) {
            if (!productlist.get(i).isCheckstatus()) {
                chekcstatus = false;
                break;
            }
        }
        System.out.println("aaaaaaaaaaa chekcstatus  " + chekcstatus);
        tv_selectall.setChecked(chekcstatus);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (dialog4 != null) {
            dialog4.dismiss();
        }
        //  if (isViewCreated){
        System.out.println("aaaaaaaaaa onressumen orderdetail");
        isRefreshStarted = false;
        try {
            if (countDownTimer != null) {
                countDownTimer.onFinish();
                countDownTimer.cancel();
            }
        } catch (NullPointerException e) {

        }


        getOrders();
        //  }

    }

    private void showOTPDialog() {

        LayoutInflater inflater2 = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout2 = inflater2.inflate(R.layout.supporter_otp_input, null);

        RelativeLayout layout_payment_status = alertLayout2.findViewById(R.id.layout_payment_status);
        CheckBox tb_payment_status = alertLayout2.findViewById(R.id.chk_payment_status);//(ToggleButton) alertLayout2.findViewById(R.id.tb_payment_status);
        final EditText et_collected_amount = alertLayout2.findViewById(R.id.et_collected_amount);
        LinearLayout layout_payment_input = alertLayout2.findViewById(R.id.layout_payment_input);
        TextView tv_error = alertLayout2.findViewById(R.id.tv_error);
        TextView tv_otptext = alertLayout2.findViewById(R.id.tv_otptext);

        et_collected_amount.setText("" + amountTobeCollected);
        et_collected_amount.setEnabled(false);
        et_collected_amount.setClickable(false);

        if (deliverymodeid.equalsIgnoreCase("1")) {
            tv_otptext.setText("" + getResources().getString(R.string.customerOTP));
        } else {
            tv_otptext.setText("" + getResources().getString(R.string.TellusOTP));
        }

        if (!paymentStatus) {
            //not received yet
            layout_payment_status.setVisibility(View.VISIBLE);
        } else {
            layout_payment_input.setVisibility(View.GONE);
        }


        tb_payment_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    tv_error.setVisibility(View.GONE);
                    layout_payment_input.setVisibility(View.VISIBLE);
                } else
                    layout_payment_input.setVisibility(View.GONE);
            }
        });

        final EditText otp = alertLayout2.findViewById(R.id.order_otp);
        final Button submit2 = alertLayout2.findViewById(R.id.submit);
        final TextView tv_invalidotp = alertLayout2.findViewById(R.id.tv_invalidotp);

        AlertDialog.Builder alert2 = new AlertDialog.Builder(OrderDetailsActivity.this);
        alert2.setIcon(R.mipmap.ic_launcher);
        alert2.setTitle(getString(R.string.enter_otp));
        // this is set the view from XML inside AlertDialog
        alert2.setView(alertLayout2);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert2.setCancelable(true);
        final AlertDialog dialog2 = alert2.create();
        dialog2.show();
        submit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!paymentStatus) {
                    if (!tb_payment_status.isChecked()) {
                        String message = getString(R.string.enable_payment_check);
                        tv_error.setText(message);
                        tv_error.setVisibility(View.VISIBLE);
                        return;
                    }
                }
                String otp_str = otp.getText().toString().trim();
                if (otp_str.length() == 0) {
                    otp.setError("Should Not Be Empty");
                    otp.requestFocus();
                } else if (otp_str.length() < 6) {
                    otp.setError("Minimum 6 characters");
                    otp.requestFocus();
                } else {
                    Map<String, String> uploadMap = new HashMap<>();
                    uploadMap.put("otp", "" + otp.getText().toString().trim());
                    uploadMap.put("order_id", orderbaseid);
                    JSONObject json = new JSONObject(uploadMap);
                    System.out.println("aaaaaaa outof delevery  " + json);
                    outDelivery(json.toString(), dialog2, tv_invalidotp);
                }

            }
        });
    }

    private void outDelivery(String otp_str, final AlertDialog dialog1, TextView tv_invalidotp) {
       /* fragmentInterfaceListener=(FragmentInterface)mContext;
        fragmentInterfaceListener.sendDataMethod(true);*/
        dialog1.dismiss();
        mCustomDialog.show();
        // LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, OUTOFDELEVERY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("out_resp", response);
                    //  LoadingDialog.dialog.dismiss();
                    mCustomDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa response outofdelevery " + jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");
                            // recreate();
                            UIMsgs.showAlert(mContext, "" + message);
                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.logEvent("Vendor out for delivery order");
                            }
                            //  UIMsgs.showToast(mContext,"Status changed");

                            //   fragmentInterfaceListener.sendDataMethod(true);
                            order_status = "";
                            getOrders();
                        } else {
                            // UIMsgs.showToast(mContext,jsonObject.getString("message"));
                            dialog1.show();
                            tv_invalidotp.setText("" + jsonObject.getString("message"));
                            tv_invalidotp.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("aaaaaa catch  " + e.getMessage());
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, "Unable to change status. Please try later");
                    }

                } else {
                    mCustomDialog.dismiss();
                    //  LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                // LoadingDialog.dialog.dismiss();
                System.out.println("aaaaaaaa error   " + error.getMessage());
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return otp_str == null ? null : otp_str.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void rejectOrder(String res, AlertDialog dialog) {
        dialog.dismiss();
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERREJECT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.d("rej_resp", response);
                    LoadingDialog.dialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        System.out.println("aaaaaaaa rej_resp " + jsonObject);
                        boolean status = jsonObject.getBoolean("status");
                        if (status) {
                            String message = jsonObject.getString("message");

                            accept_order_card.setVisibility(View.GONE);
                            if (MyMixPanel.isMixPanelSupport) {
                                MyMixPanel.logEvent("Vendor rejected order");
                            }
                            UIMsgs.showAlert(mContext, "" + message);
                            //  UIMsgs.showToast(mContext,"Order Rejected");
                            //  recreate();
                            //  fragmentInterfaceListener.sendDataMethod(true);
                            order_status = "";
                            getOrders();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext, "Unable to reject. Please try later");
                    }

                } else {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(mContext, MAINTENANCE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return res == null ? null : res.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void acceptOrder(JSONObject json, AlertDialog dialog1) {
        System.out.println("aaaaaaaaaaa acceptorder " + json.toString());
        dialog1.dismiss();
        mCustomDialog.show();
        //  LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERACCEPT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("aaaaaaaaaaa acceptorder " + response);
                        if (response != null) {
                            Log.d("ac_resp", response);
                            mCustomDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa order accept " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    String message = jsonObject.getString("message");

                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Vendor accepted order");
                                    }
                                    UIMsgs.showAlert(mContext, "" + message);
                                    // UIMsgs.showToast(mContext,"Order Accepted");
                                    //  recreate();
                                    //  fragmentInterfaceListener.sendDataMethod(true);
                                    accept_order_card.setVisibility(View.GONE);
                                    order_status = "";
                                    getOrders();
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaaaaaa acceptorder " + e.getMessage());
                                e.printStackTrace();
                                LoadingDialog.dialog.dismiss();
                                UIMsgs.showToast(mContext, "Unable to accept. Please try later");
                            }

                        } else {
                            mCustomDialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaaaa acceptorder " + error.getMessage());
                mCustomDialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void updatepreparationtime(JSONObject json) {
        mCustomDialog.show();
        //  LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EXTENDINGPREPARETIME,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("ac_resp", response);
                            mCustomDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa order accept " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    String message = jsonObject.getString("message");
                                    UIMsgs.showAlert(mContext, "" + message);
                                    if (MyMixPanel.isMixPanelSupport) {
                                        MyMixPanel.logEvent("Vendor Adding preparation time");
                                    }
                                    //  UIMsgs.showToast(mContext,"Order Accepted");
                                    //  recreate();
                                    //  fragmentInterfaceListener.sendDataMethod(true);
                                    countDownTimer.onFinish();
                                    countDownTimer.cancel();
                                    getOrders();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                LoadingDialog.dialog.dismiss();
                                UIMsgs.showToast(mContext, "Unable to update. Please try later");
                            }

                        } else {
                            mCustomDialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    boolean isRefreshStarted = false;

    public void getOrders() {
        if (isRefreshStarted)
            return;

        isRefreshStarted = true;
        productlist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("order_id", orderId);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request" + json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERDETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {

                        try {
                            if (mCustomDialog != null) {
                                if (mCustomDialog.isShowing()) {
                                    try {
                                        mCustomDialog.dismiss();
                                    } catch (IllegalArgumentException e) {

                                    }

                                }
                            }
                            isRefreshStarted = false;
                            //  mCustomDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            int http_code = jsonObject.getInt("http_code");

                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");
                                OrderDetailsmodel orderDetailsmodel = new OrderDetailsmodel();

                                orderDetailsmodel.setTrack_id(dataobj.getString("track_id"));
                                orderDetailsmodel.setDelivery_fee(dataobj.getString("delivery_fee"));
                                orderDetailsmodel.setTotal(dataobj.getString("total"));
                                orderDetailsmodel.setUsed_wallet_amount(dataobj.getString("used_wallet_amount"));
                                orderDetailsmodel.setMessage(dataobj.getString("message"));
                                orderDetailsmodel.setPreparation_time(dataobj.getString("preparation_time"));
                                orderDetailsmodel.setShipping_address_id(dataobj.getString("shipping_address_id"));
                                orderDetailsmodel.setCreated_at(dataobj.getString("created_at"));
                                orderDetailsmodel.setDelivery_mode_id(dataobj.getString("delivery_mode_id"));
                                orderDetailsmodel.setCreated_user_id(dataobj.getString("created_user_id"));
                                orderDetailsmodel.setOrder_status_id(dataobj.getString("order_status_id"));
                                orderDetailsmodel.setPayment_id(dataobj.getString("payment_id"));
                                orderDetailsmodel.setId(dataobj.getString("id"));
                                deliverymodeid = dataobj.getString("delivery_mode_id");
                                orderConfirmationtime = dataobj.getString("order_confirmation_time");
                                try {
                                    return_otp.setText("Return Otp : " + dataobj.getString("order_return_otp"));
                                } catch (Exception e) {

                                }

                                try {
                                    JSONArray jsonArray = dataobj.getJSONArray("reject_request");
                                    JSONObject rejetcrequest = jsonArray.getJSONObject(0);
                                    rejectordercreatedat = rejetcrequest.getString("created_at");
                                    reject_status = rejetcrequest.getString("status");
                                } catch (Exception e) {

                                }

                                try {
                                    if (!dataobj.getString("preparation_time").equalsIgnoreCase("null")) {
                                        try {
                                            preparationtime = (Integer.parseInt(dataobj.getString("preparation_time")) * 60);
                                            prearationtimest = (Integer.parseInt(dataobj.getString("preparation_time")));
                                        } catch (NumberFormatException ex) {
                                        }

                                        //  updatetime=dataobj.getString("created_at");
                                        //  timerset();

                                    }


                                } catch (NullPointerException e) {

                                }

                                orderbaseid = dataobj.getString("id");
                                timezone(dataobj.getString("created_at"));
                                // tv_time.setText(dataobj.getString("created_at"));
                                tv_trackid.setText("Order id : #" + dataobj.getString("track_id"));
                                trackID = dataobj.getString("track_id");
                                tv_sub_total.setText(mContext.getString(R.string.Rs) + " " + dataobj.getString("track_id"));
                                Double total = Double.parseDouble(dataobj.getString("total"));
                                System.out.println("aaaaaaaaaa total " + total);
                                tv_item_grand_total_total.setText(String.format("%.2f", total));
                                try {
                                    Double deleveryfee = Double.parseDouble(dataobj.getString("delivery_fee"));
                                    Double grandtotal = Double.parseDouble(dataobj.getString("total"));
                                    Double total1 = grandtotal - deleveryfee;
                                    System.out.println("aaaaaaaaaa total1 " + total1);
                                    tv_item_grand_total_total.setText(String.format("%.2f", total1));
                                    // tv_item_grand_total_total.setText(""+total);
                                } catch (NumberFormatException e) {

                                } catch (NullPointerException e1) {

                                }

                                //   tv_deliveryfee.setText(dataobj.getString("delivery_fee"));

                                if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1")) {
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                    tv_outfordelevery.setText("READY TO PICKUP");
                                } else {
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                    tv_outfordelevery.setText(getString(R.string.out_for_delivery));
                                }
                                try {
                                    //  layout_delevery.setVisibility(View.VISIBLE);
                                    //    layout_delevery_one.setVisibility(View.VISIBLE);


                                    JSONObject deleveryjob = dataobj.getJSONObject("delivery_job");
                                    JSONObject deleveryboyobj = deleveryjob.getJSONObject("delivery_boy");

                                    try {
                                        deliverydstatus = deleveryjob.getString("status");
                                        if (deliverydstatus.equalsIgnoreCase("601") || deliverydstatus.equalsIgnoreCase("602") || deliverydstatus.equalsIgnoreCase("604")) {
                                            cardview_returnotp.setVisibility(View.VISIBLE);
                                        } else {
                                            cardview_returnotp.setVisibility(View.GONE);
                                        }
                                    } catch (Exception e) {
                                        cardview_returnotp.setVisibility(View.GONE);
                                    }

                                    tv_deleverypartnarname.setText(deleveryboyobj.getString("first_name") + " " +
                                            deleveryboyobj.getString("last_name"));
                                    tv_deleverypartnermobile.setText(deleveryboyobj.getString("phone"));
                                } catch (JSONException e) {
                                    layout_delevery.setVisibility(View.GONE);
                                    layout_delevery_one.setVisibility(View.GONE);
                                }

                                try {
                                    JSONObject shippingobj = dataobj.getJSONObject("shipping_address");
                                    ShippingAddress shippingAddress = new ShippingAddress();
                                    shippingAddress.setId(shippingobj.getString("id"));
                                    shippingAddress.setPhone(shippingobj.getString("phone"));
                                    shippingAddress.setEmail(shippingobj.getString("email"));
                                    shippingAddress.setName(shippingobj.getString("name"));
                                    shippingAddress.setLandmark(shippingobj.getString("landmark"));
                                    shippingAddress.setAddress(shippingobj.getString("address"));
                                    //  shippingAddress.setLocation_id(shippingobj.getString("location_id"));
                                    orderDetailsmodel.setShippingAddress(shippingAddress);
                                    tv_customername.setText(shippingobj.getString("name"));
                                    tv_customerphone.setText(shippingobj.getString("phone"));
                                    tv_shippingaddress.setText("" + shippingobj.getString("address"));
                                } catch (JSONException e1) {
                                    System.out.println("aaaaaa error " + e1.getMessage());
                                }

                                try {
                                    JSONObject deleveryong = dataobj.getJSONObject("delivery_mode");
                                    DeleveryModesmodel deleveryModesmodel = new DeleveryModesmodel();
                                    deleveryModesmodel.setId(deleveryong.getString("id"));
                                    deleveryModesmodel.setName(deleveryong.getString("name"));
                                    deleveryModesmodel.setDesc(deleveryong.getString("desc"));
                                    orderDetailsmodel.setDeleveryModesmodel(deleveryModesmodel);
                                    tv_deleverymode.setText("" + deleveryong.getString("name"));
                                } catch (JSONException e2) {

                                }

                                try {
                                    JSONObject customerobj = dataobj.getJSONObject("customer");
                                    CustomerModel customerModel = new CustomerModel();
                                    customerModel.setId(customerobj.getString("id"));
                                    customerModel.setUnique_id(customerobj.getString("unique_id"));
                                    customerModel.setFirst_name(customerobj.getString("first_name"));
                                    customerModel.setPhone(customerobj.getString("phone"));
                                    orderDetailsmodel.setCustomerModel(customerModel);
                                    tv_customername.setText(customerobj.getString("first_name"));
                                    tv_customerphone.setText(customerobj.getString("phone"));
                                } catch (JSONException e2) {

                                }
                                try {
                                    JSONObject orderostatusobj = dataobj.getJSONObject("order_status");
                                    OrderStatus orderStatus = new OrderStatus();
                                    orderStatus.setId(orderostatusobj.getString("id"));
                                    orderStatus.setDelivery_mode_id(orderostatusobj.getString("delivery_mode_id"));
                                    orderStatus.setSerial_number(orderostatusobj.getString("serial_number"));
                                    serial_number = orderostatusobj.getString("serial_number");
                                    orderStatus.setStatus(orderostatusobj.getString("status"));
                                    orderDetailsmodel.setOrderStatus(orderStatus);

                                    try {
                                        JSONArray timearray = orderostatusobj.getJSONArray("accepted_time");
                                        JSONObject timeobject = timearray.getJSONObject(0);
                                        updatetime = timeobject.getString("created_at");
                                        timerset();

                                    } catch (Exception e) {

                                    }


                                    // updatetime=orderostatusobj.getJSONObject("time").getString("created_at");
                                    //   timerset();

                                    if (order_status == null || order_status.equalsIgnoreCase("") || order_status.isEmpty()) {
                                        System.out.println("aaaaaaaaaa status if " + order_status + "  " + orderostatusobj.getString("status"));
                                        tv_status.setText(orderostatusobj.getString("status"));
                                        order_status_details = orderostatusobj.getString("status");
                                    } else {
                                        System.out.println("aaaaaaaaaa status else " + order_status);
                                        tv_status.setText(order_status);
                                    }

                                    // tv_status.setText(orderostatusobj.getString("status"));
                                    //  order_status=orderostatusobj.getString("status");

                                    if (orderostatusobj.getString("serial_number").equalsIgnoreCase("100")) {
                                        accept_order_card.setVisibility(View.VISIBLE);
                                        reject_order_card.setVisibility(View.VISIBLE);
                                        status_layout.setVisibility(View.VISIBLE);
                                        view_scrollview.setBottom(55);
                                        out_card.setVisibility(View.GONE);
                                        preparation_card.setVisibility(View.GONE);
                                    } else if (orderostatusobj.getString("serial_number").equalsIgnoreCase("102")) {
                                        accept_order_card.setVisibility(View.GONE);
                                        reject_order_card.setVisibility(View.GONE);
                                        out_card.setVisibility(View.VISIBLE);
                                        preparation_card.setVisibility(View.VISIBLE);
                                        status_layout.setVisibility(View.VISIBLE);
                                        view_scrollview.setBottom(55);
                                    } else if (orderostatusobj.getString("serial_number").equalsIgnoreCase("103")) {
                                        accept_order_card.setVisibility(View.GONE);
                                        reject_order_card.setVisibility(View.GONE);
                                        out_card.setVisibility(View.GONE);
                                        preparation_card.setVisibility(View.GONE);
                                        status_layout.setVisibility(View.GONE);
                                        view_scrollview.setBottom(0);
                                    } else if (orderostatusobj.getString("serial_number").equalsIgnoreCase("300") ||
                                            orderostatusobj.getString("serial_number").equalsIgnoreCase("301") ||
                                            orderostatusobj.getString("serial_number").equalsIgnoreCase("104")) {
                                        accept_order_card.setVisibility(View.GONE);
                                        reject_order_card.setVisibility(View.GONE);
                                        out_card.setVisibility(View.GONE);
                                        preparation_card.setVisibility(View.GONE);
                                        status_layout.setVisibility(View.GONE);
                                        view_scrollview.setBottom(0);
                                    }
                                } catch (JSONException e2) {

                                }


                                try {
                                    JSONObject paymentobj = dataobj.getJSONObject("payment");
                                    PaymentModel paymentModel = new PaymentModel();
                                    paymentModel.setId(paymentobj.getString("id"));
                                    paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));
                                    paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                    paymentModel.setAmount(paymentobj.getString("amount"));
                                    paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                    paymentModel.setMessage(paymentobj.getString("message"));
                                    paymentModel.setStatus(paymentobj.getString("status"));
                                    JSONObject payobj = paymentobj.getJSONObject("payment_method");
                                    PaymentMethod paymentMethod = new PaymentMethod();
                                    paymentMethod.setId(payobj.getString("id"));
                                    paymentMethod.setName(payobj.getString("name"));
                                    paymentMethod.setDescription(payobj.getString("description"));
                                    paymentModel.setPaymentMethod(paymentMethod);
                                    orderDetailsmodel.setPaymentModel(paymentModel);

                                    try {
                                        String payment_Method = "";
                                        switch (paymentMethod.getId()) {
                                            case "1":
                                                payment_Method = "Cash on Delivery";
                                                break;
                                            case "3":
                                                payment_Method = "Wallet Money";
                                                break;
                                            case "4":
                                                payment_Method = "Online Payment Link";
                                                break;
                                            default:
                                                payment_Method = "Online";
                                        }
                                        payment_type.setText(payment_Method);
                                    } catch (Exception ex) {
                                        payment_type.setText(payobj.getString("name"));
                                    }

                                    if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1")) {
                                        layout_customer_phonenumber.setVisibility(View.VISIBLE);
                                        layout_customer_name.setVisibility(View.VISIBLE);
                                    } else {
                                        layout_customer_phonenumber.setVisibility(View.GONE);
                                        layout_customer_name.setVisibility(View.GONE);
                                    }
                                    paymentStatus = true;
                                    btn_generate_payment_link.setVisibility(View.GONE);
                                    if (Integer.parseInt(orderDetailsmodel.getOrder_status_id()) > 1 &&
                                            Integer.parseInt(orderDetailsmodel.getOrder_status_id()) != 5) {
                                        //5- completed
                                        //after order accpeted only this block will execute
                                        if (dataobj.getString("delivery_mode_id").equalsIgnoreCase("1") &&
                                                paymentModel.getPayment_method_id() != null &&
                                                paymentModel.getPayment_method_id().equals("1")) {
                                            //for self pick case only
                                            if (!paymentModel.getStatus().equals("2")) {
                                                paymentStatus = false;
                                                amountTobeCollected = orderDetailsmodel.getTotal();
                                                if (orderDetailsmodel.getOrder_status_id().equalsIgnoreCase("17") ||
                                                        orderDetailsmodel.getOrder_status_id().equalsIgnoreCase("16")) {
                                                    btn_generate_payment_link.setVisibility(View.GONE);
                                                } else {
                                                    btn_generate_payment_link.setVisibility(View.VISIBLE);
                                                }

                                            }
                                            // else
                                            //  payment_type.setText("Online payment link");
                                        } else {
                                            btn_generate_payment_link.setVisibility(View.GONE);
                                        }
                                    } else {
                                        btn_generate_payment_link.setVisibility(View.GONE);
                                    }

                                } catch (JSONException e2) {

                                }

                                try {
                                    Object aObj = null;
                                    if (dataobj.has("delivery_job"))
                                        aObj = dataobj.get("delivery_job");
                                    if (aObj instanceof JSONObject) {

                                        JSONObject delivery_job = (JSONObject) aObj;
                                        DeliveryJobObject deliveryJobObject = new DeliveryJobObject();
                                        deliveryJobObject.setID(delivery_job.getString("id"));
                                        deliveryJobObject.setJobID(delivery_job.getString("job_id"));
                                        deliveryJobObject.setRating(delivery_job.getString("rating"));
                                        deliveryJobObject.setFeedback(delivery_job.getString("feedback"));
                                        deliveryJobObject.setJobType(delivery_job.getString("job_type"));
                                        deliveryJobObject.setDeliveryBoyUserID(delivery_job.getString("delivery_boy_user_id"));
                                        deliveryJobObject.setStatus(delivery_job.getString("status"));

                                        if (delivery_job.getString("status").equalsIgnoreCase("500")) {
                                            try {
                                                JSONObject deliveryreject = delivery_job.getJSONObject("rejected_reason");
                                                deliveryJobObject.setRejectreson_id(deliveryreject.getString("id"));
                                                deliveryJobObject.setRejectreason(deliveryreject.getString("reason"));
                                                status_layout.setVisibility(View.GONE);
                                                tv_status.setText("Rejected By Delivery Patner : " + deliveryreject.getString("reason"));
                                                order_status = "Rejected By Delivery Patner : " + deliveryreject.getString("reason");
                                            } catch (Exception e) {

                                            }

                                        }

                                        if (delivery_job.has("delivery_boy")) {
                                            JSONObject delivery_boy = delivery_job.getJSONObject("delivery_boy");
                                            DeliveryBoyObject deliveryBoyObject = new DeliveryBoyObject();
                                            deliveryBoyObject.setId(delivery_boy.getString("id"));
                                            deliveryBoyObject.setFirst_name(delivery_boy.getString("first_name"));
                                            deliveryBoyObject.setLast_name(delivery_boy.getString("last_name"));
                                            deliveryBoyObject.setPhone(delivery_boy.getString("phone"));
                                            deliveryBoyObject.setUnique_id(delivery_boy.getString("unique_id"));
                                            deliveryBoyObject.setProfileImage(delivery_boy.getString("profile_image"));

                                            if (delivery_boy.has("delivery_boy_pickup_image"))
                                                deliveryBoyObject.setDelivery_boy_pickup_image(delivery_boy.getString("delivery_boy_pickup_image"));
                                            if (delivery_boy.has("delivery_boy_delivery_image"))
                                                deliveryBoyObject.setDelivery_boy_delivery_image(delivery_boy.getString("delivery_boy_delivery_image"));

                                            deliveryJobObject.setDeliveryBoy(deliveryBoyObject);
                                        }

                                    }
                                } catch (Exception ex) {
                                }

                                JSONArray productarray = dataobj.getJSONArray("ecom_order_details");

                                for (int k = 0; k < productarray.length(); k++) {
                                    JSONObject productonj = productarray.getJSONObject(k);
                                    ProductDetailsModel productDetailsModel = new ProductDetailsModel();

                                    productDetailsModel.setId(productonj.getString("id"));
                                    productDetailsModel.setEcom_order_id(productonj.getString("ecom_order_id"));
                                    productDetailsModel.setItem_id(productonj.getString("item_id"));
                                    productDetailsModel.setVendor_product_variant_id(productonj.getString("vendor_product_variant_id"));
                                    productDetailsModel.setQty(productonj.getString("qty"));
                                    productDetailsModel.setPrice(productonj.getString("price"));
                                    productDetailsModel.setRate_of_discount(productonj.getString("rate_of_discount"));
                                    productDetailsModel.setSub_total(productonj.getString("sub_total"));
                                    productDetailsModel.setDiscount(productonj.getString("discount"));
                                    productDetailsModel.setTax(productonj.getString("tax"));
                                    productDetailsModel.setTotal(productonj.getString("total"));
                                    productDetailsModel.setCancellation_message(productonj.getString("cancellation_message"));
                                    productDetailsModel.setStatus(productonj.getString("status"));
                                    productDetailsModel.setPromotion_banner_id(productonj.getString("promotion_banner_id"));
                                    productDetailsModel.setOffer_product_id(productonj.getString("offer_product_id"));
                                    productDetailsModel.setOffer_product_variant_id(productonj.getString("offer_product_variant_id"));
                                    productDetailsModel.setOffer_product_qty(productonj.getString("offer_product_qty"));
                                    productDetailsModel.setPromotion_banner_discount(productonj.getString("promotion_banner_discount"));


                                    try {
                                        productDetailsModel.setPromocode_id(productonj.getString("promocode_id"));
                                        productDetailsModel.setPromocode_discount(productonj.getString("promocode_discount"));
                                    } catch (Exception e) {

                                    }
                                    try {
                                        JSONObject itemobj = productonj.getJSONObject("item");
                                        ItemModel itemModel = new ItemModel();
                                        itemModel.setId(itemobj.getString("id"));
                                        itemModel.setName(itemobj.getString("name"));
                                        itemModel.setDesc(itemobj.getString("desc"));

                                        try {
                                            itemModel.setService_id(itemobj.getJSONObject("service_charge").getString("id"));
                                            itemModel.setService_charge(itemobj.getJSONObject("service_charge").getString("service_tax"));
                                        } catch (JSONException | NullPointerException e) {

                                        }


                                        try {
                                            ArrayList<ImagesModel> imagelist = new ArrayList<>();
                                            JSONArray jsonArray = itemobj.getJSONArray("item_images");
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject imgobj = jsonArray.getJSONObject(i);
                                                ImagesModel imagesModel = new ImagesModel();
                                                imagesModel.setId(imgobj.getString("id"));
                                                imagesModel.setItem_id(imgobj.getString("item_id"));
                                                imagesModel.setExt(imgobj.getString("ext"));
                                                imagesModel.setImage(imgobj.getString("image"));
                                                imagelist.add(imagesModel);

                                            }
                                            itemModel.setImagelist(imagelist);
                                        } catch (JSONException e) {

                                        }

                                        JSONObject varientobj = itemobj.getJSONObject("varinat");
                                        VarientModel varientModel = new VarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setTax_id(varientobj.getString("tax_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));

                                        try {
                                            varientModel.setReturn_id(varientobj.getString("return_id"));
                                            varientModel.setReturn_available(varientobj.getString("return_available"));
                                            varientModel.setReturn_id(varientobj.getJSONObject("return").getString("return_days"));
                                        } catch (Exception e) {

                                        }

                                        JSONObject sectionobj = varientobj.getJSONObject("section_item");
                                        SectionItemModel sectionItemModel = new SectionItemModel();
                                        sectionItemModel.setId(sectionobj.getString("id"));
                                        sectionItemModel.setName(sectionobj.getString("name"));
                                        sectionItemModel.setWeight(sectionobj.getString("weight"));

                                        varientModel.setSectionItemModel(sectionItemModel);

                                        itemModel.setVarientModel(varientModel);
                                        productDetailsModel.setItemModel(itemModel);


                                    } catch (JSONException e2) {

                                    }
                                    try {
                                        JSONObject itemobj = productonj.getJSONObject("offer_item");
                                        ItemModel itemModel = new ItemModel();
                                        itemModel.setId(itemobj.getString("id"));
                                        itemModel.setName(itemobj.getString("name"));
                                        itemModel.setDesc(itemobj.getString("desc"));


                                        ArrayList<ImagesModel> imageslist = new ArrayList<>();
                                        try {
                                            JSONArray jsonArray = itemobj.getJSONArray("item_images");
                                            for (int M = 0; M < jsonArray.length(); M++) {
                                                JSONObject jsonObject1 = jsonArray.getJSONObject(M);
                                                ImagesModel imagesModel = new ImagesModel();
                                                imagesModel.setId(jsonObject1.getString("id"));
                                                imagesModel.setImage(jsonObject1.getString("image"));
                                                imageslist.add(imagesModel);
                                            }
                                            itemModel.setImagelist(imageslist);
                                        } catch (Exception e) {

                                        }


                                        JSONObject varientobj = itemobj.getJSONObject("varinat");
                                        VarientModel varientModel = new VarientModel();
                                        varientModel.setId(varientobj.getString("id"));
                                        varientModel.setSku(varientobj.getString("sku"));
                                        varientModel.setPrice(varientobj.getString("price"));
                                        varientModel.setStock(varientobj.getString("stock"));
                                        varientModel.setDiscount(varientobj.getString("discount"));
                                        varientModel.setTax_id(varientobj.getString("tax_id"));
                                        varientModel.setStatus(varientobj.getString("status"));
                                        varientModel.setSection_item_id(varientobj.getString("section_item_id"));


                                        JSONObject sectionobj = varientobj.getJSONObject("section_item");
                                        SectionItemModel sectionItemModel = new SectionItemModel();
                                        sectionItemModel.setId(sectionobj.getString("id"));
                                        sectionItemModel.setName(sectionobj.getString("name"));
                                        sectionItemModel.setWeight(sectionobj.getString("weight"));

                                        varientModel.setSectionItemModel(sectionItemModel);

                                        itemModel.setVarientModel(varientModel);
                                        productDetailsModel.setSetOfferitemmodel(itemModel);


                                    } catch (Exception e) {

                                    }
                                    try {
                                        JSONObject promotion_banner = productonj.getJSONObject("promotion_banner");
                                        productDetailsModel.setPromotion_banner_discount_type_id(promotion_banner.getString("promotion_banner_discount_type_id"));

                                    } catch (Exception e) {

                                    }

                                    productlist.add(productDetailsModel);
                                }

                                if (productlist.size() > 0) {
                                    boolean checkreject = false;
                                    for (int i = 0; i < productlist.size(); i++) {
                                        if (productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                            checkreject = true;
                                        }
                                    }
                                    if (checkreject) {
                                        try {
                                            Date createdTime = Utility.getDateFromString(rejectordercreatedat);
                                            System.out.println("aaaaaaaaa date and time  " + Calendar.getInstance().getTime() + " created " + createdTime.getTime());
                                            long remSeconds = (Calendar.getInstance().getTime().getTime() - createdTime.getTime());
                                            int time = Integer.parseInt(orderConfirmationtime) * 60000;
                                            long totaltime = time - (remSeconds);
                                            System.out.println("aaaaaaaaa timer  " + reject_status);
                                            if (reject_status.equalsIgnoreCase("1") ||
                                                    !serial_number.equalsIgnoreCase("301")) {
                                                System.out.println("aaaaaaaaaaa user cancel ");
                                                reject_countdowntimer = new CountDownTimer(totaltime, 1000) { // adjust the milli seconds here
                                                    public void onTick(long millisUntilFinished) {
                                                        tv_status.setText("Waiting for user approve " + String.format("%d min, %d sec",
                                                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                                                                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                                        accept_order_card.setVisibility(View.GONE);
                                                        reject_order_card.setVisibility(View.GONE);
                                                        if (reject_status.equalsIgnoreCase("2") && !serial_number.equalsIgnoreCase("102")) {
                                                            accept_order_card.setVisibility(View.VISIBLE);
                                                            reject_order_card.setVisibility(View.GONE);
                                                            try {
                                                                reject_countdowntimer.onFinish();
                                                                reject_countdowntimer.cancel();
                                                            } catch (NullPointerException e) {

                                                            }
                                                        } else {
                                                            if (serial_number.equalsIgnoreCase("102")) {
                                                                try {
                                                                    reject_countdowntimer.onFinish();
                                                                    reject_countdowntimer.cancel();
                                                                } catch (NullPointerException e) {

                                                                }
                                                            }
                                                        }
                                                    }

                                                    public void onFinish() {
                                                        if (order_status == null || order_status.equalsIgnoreCase("") || order_status.isEmpty()) {
                                                            System.out.println("aaaaaaaaaa status if " + order_status + "  " + order_status_details);
                                                            tv_status.setText(order_status_details);
                                                        } else {
                                                            System.out.println("aaaaaaaaaa status else " + order_status);
                                                            tv_status.setText(order_status);
                                                        }
                                                        // tv_status.setText(""+order_status);
                                                        if (serial_number.equalsIgnoreCase("102")) {
                                                            accept_order_card.setVisibility(View.GONE);
                                                            reject_order_card.setVisibility(View.GONE);
                                                        } else if (serial_number.equalsIgnoreCase("101")) {
                                                            accept_order_card.setVisibility(View.VISIBLE);
                                                            reject_order_card.setVisibility(View.GONE);
                                                        } else if (serial_number.equalsIgnoreCase("100")) {
                                                            accept_order_card.setVisibility(View.VISIBLE);
                                                            reject_order_card.setVisibility(View.GONE);
                                                        } else {
                                                            accept_order_card.setVisibility(View.GONE);
                                                            reject_order_card.setVisibility(View.GONE);
                                                        }
                                                    }
                                                }.start();
                                            } else if (reject_status.equalsIgnoreCase("2")) {
                                                if (order_status == null || order_status.equalsIgnoreCase("") || order_status.isEmpty()) {
                                                    System.out.println("aaaaaaaaaa status if " + order_status + "  " + order_status_details);
                                                    tv_status.setText(order_status_details);
                                                } else {
                                                    System.out.println("aaaaaaaaaa status else " + order_status);
                                                    tv_status.setText(order_status);
                                                }
                                                try {
                                                    reject_countdowntimer.onFinish();
                                                    reject_countdowntimer.cancel();
                                                } catch (NullPointerException e) {

                                                }
                                                if (serial_number.equalsIgnoreCase("102")) {
                                                    accept_order_card.setVisibility(View.GONE);
                                                    reject_order_card.setVisibility(View.GONE);
                                                    if (order_status == null || order_status.equalsIgnoreCase("") || order_status.isEmpty()) {
                                                        System.out.println("aaaaaaaaaa status if " + order_status + "  " + order_status_details);
                                                        tv_status.setText(order_status_details);
                                                    } else {
                                                        System.out.println("aaaaaaaaaa status else " + order_status);
                                                        tv_status.setText(order_status);
                                                    }
                                                } else if (serial_number.equalsIgnoreCase("101")) {
                                                    accept_order_card.setVisibility(View.VISIBLE);
                                                    reject_order_card.setVisibility(View.GONE);
                                                } else if (serial_number.equalsIgnoreCase("100")) {

                                                    accept_order_card.setVisibility(View.VISIBLE);
                                                    reject_order_card.setVisibility(View.GONE);
                                                } else {
                                                    accept_order_card.setVisibility(View.GONE);
                                                    reject_order_card.setVisibility(View.GONE);
                                                }
                                            } else {
                                                try {
                                                    accept_order_card.setVisibility(View.VISIBLE);
                                                    reject_order_card.setVisibility(View.GONE);
                                                    reject_countdowntimer.onFinish();
                                                    reject_countdowntimer.cancel();
                                                } catch (NullPointerException e) {

                                                }

                                                if (order_status == null || order_status.equalsIgnoreCase("") || order_status.isEmpty()) {
                                                    System.out.println("aaaaaaaaaa status if " + order_status + "  " + order_status_details);
                                                    tv_status.setText(order_status_details);
                                                } else {
                                                    System.out.println("aaaaaaaaaa status else " + order_status);
                                                    tv_status.setText(order_status);
                                                }
                                            }
                                        } catch (NumberFormatException e) {
                                            System.out.println("aaaaaaaaaaaa catch " + e.getMessage());
                                        }
                                    }
                                    OrderItemsAdapter orderItemsAdapter = new OrderItemsAdapter(mContext, productlist, 0, deliverydstatus);

                                    LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                        @Override
                                        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                            LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                private static final float SPEED = 300f;// Change this value (default=25f)

                                                @Override
                                                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                    return SPEED / displayMetrics.densityDpi;
                                                }

                                            };
                                            smoothScroller.setTargetPosition(position);
                                            startSmoothScroll(smoothScroller);
                                        }

                                    };
                                    itemsRecycler.setLayoutManager(layoutManager);
                                    itemsRecycler.setAdapter(orderItemsAdapter);

                                    double servicecharge = 0.0, subtotal = 0.0, discount = 0.0, rateofdiscount = 0.0, tax = 0.0, promodiscount = 0.0, promotion_discount = 0.0;

                                    for (int i = 0; i < productlist.size(); i++) {
                                        try {
                                            if (deliverydstatus.equalsIgnoreCase("601") || deliverydstatus.equalsIgnoreCase("602") || deliverydstatus.equalsIgnoreCase("603")) {
                                                if (!productlist.get(i).getItemModel().getVarientModel().getReturn_available().equalsIgnoreCase("1")) {
                                                    if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                        subtotal = subtotal + Double.parseDouble(productlist.get(i).getSub_total());
                                                        discount = discount + Double.parseDouble(productlist.get(i).getDiscount());
                                                        rateofdiscount = rateofdiscount + Double.parseDouble(productlist.get(i).getRate_of_discount());
                                                        tax = tax + Double.parseDouble(productlist.get(i).getTax());
                                                    }
                                                    try {
                                                        if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                            promodiscount = promodiscount + Double.parseDouble(productlist.get(i).getPromocode_discount());
                                                        }
                                                    } catch (NumberFormatException | NullPointerException e2) {

                                                    }
                                                    try {
                                                        promotion_discount = promotion_discount + Double.parseDouble(productlist.get(i).getPromotion_banner_discount());
                                                    } catch (NumberFormatException | NullPointerException e1) {
                                                    }
                                                    try {
                                                        servicecharge = servicecharge + ((((Double.parseDouble(productlist.get(i).getTotal()) / 100.0f) * Double.parseDouble(productlist.get(i).getItemModel().getService_charge()))) * Integer.parseInt(productlist.get(i).getQty()));
                                                        //  servicecharge=servicecharge+Integer.parseInt(productlist.get(i).getItemModel().getService_charge());
                                                    } catch (NumberFormatException | NullPointerException e8) {

                                                    }
                                                }
                                            } else {
                                                if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                    subtotal = subtotal + Double.parseDouble(productlist.get(i).getSub_total());
                                                    discount = discount + Double.parseDouble(productlist.get(i).getDiscount());
                                                    rateofdiscount = rateofdiscount + Double.parseDouble(productlist.get(i).getRate_of_discount());
                                                    tax = tax + Double.parseDouble(productlist.get(i).getTax());
                                                }

                                                try {
                                                    if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                        promodiscount = promodiscount + Double.parseDouble(productlist.get(i).getPromocode_discount());
                                                    }
                                                } catch (NumberFormatException | NullPointerException e3) {

                                                }
                                                try {
                                                    promotion_discount = promotion_discount + Double.parseDouble(productlist.get(i).getPromotion_banner_discount());
                                                } catch (NumberFormatException | NullPointerException e1) {
                                                }
                                                try {
                                                    servicecharge = servicecharge + ((((Double.parseDouble(productlist.get(i).getTotal()) / 100.0f) * Double.parseDouble(productlist.get(i).getItemModel().getService_charge()))) * Integer.parseInt(productlist.get(i).getQty()));
                                                } catch (NumberFormatException | NullPointerException eq) {

                                                }
                                                System.out.println("aaaaaaaaa  value " + subtotal + " " + discount + " " + tax + " " + servicecharge);

                                            }

                                        } catch (Exception e) {
                                            if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                subtotal = subtotal + Double.parseDouble(productlist.get(i).getSub_total());
                                                discount = discount + Double.parseDouble(productlist.get(i).getDiscount());
                                                rateofdiscount = rateofdiscount + Double.parseDouble(productlist.get(i).getRate_of_discount());
                                                tax = tax + Double.parseDouble(productlist.get(i).getTax());
                                            }

                                            try {
                                                if (!productlist.get(i).getStatus().equalsIgnoreCase("4")) {
                                                    promodiscount = promodiscount + Double.parseDouble(productlist.get(i).getPromocode_discount());
                                                }
                                            } catch (NumberFormatException | NullPointerException e3) {

                                            }
                                            try {
                                                promotion_discount = promotion_discount + Double.parseDouble(productlist.get(i).getPromotion_banner_discount());
                                            } catch (NumberFormatException | NullPointerException e1) {
                                            }
                                            try {
                                                try {
                                                    servicecharge = servicecharge + ((((Double.parseDouble(productlist.get(i).getTotal()) / 100.0f) * Double.parseDouble(productlist.get(i).getItemModel().getService_charge()))) * Integer.parseInt(productlist.get(i).getQty()));

                                                    //  servicecharge=servicecharge+Integer.parseInt(productlist.get(i).getItemModel().getService_charge());
                                                } catch (NumberFormatException | NullPointerException e5) {
                                                    servicecharge = servicecharge + ((((Double.parseDouble(productlist.get(i).getTotal()) / 100.0f) * Double.parseDouble(productlist.get(i).getItemModel().getService_charge()))) * Integer.parseInt(productlist.get(i).getQty()));
                                                }
                                                //  servicecharge=servicecharge+Integer.parseInt(productlist.get(i).getItemModel().getService_charge());
                                            } catch (NumberFormatException | NullPointerException eq) {

                                            }
                                        }


                                    }
                                    // System.out.println("aaaaaaaaa dscount   "+discount);
                                    if (discount == 0.0 | discount == 0) {
                                        tv_discount.setText("0.0");
                                        layout_delevery.setVisibility(View.GONE);
                                    } else {
                                        tv_discount.setText("-" + Utility.roundFloat("" + discount, 2));
                                    }
                                    tv_sub_total.setText(mContext.getString(R.string.Rs) + " " + subtotal);

                                    tv_tax.setText("+" + tax);

                                    // Double promoapplied=0.0;
                                    if (promodiscount != 0.0) {
                                        layout_promodiscount.setVisibility(View.VISIBLE);
                                        tv_promodiscount.setText("- " + promodiscount);
                                        // promoapplied=Double.parseDouble(tv_item_grand_total_total.getText().toString().trim())-promodiscount;
                                    } else {
                                        layout_promodiscount.setVisibility(View.GONE);
                                    }

                                    if (promotion_discount != 0.0) {
                                        layout_promotion_discount.setVisibility(View.VISIBLE);
                                        tv_promotiondiscount.setText("- " + promotion_discount);
                                    } else {
                                        layout_promotion_discount.setVisibility(View.GONE);
                                    }

                                    // Double totalvalue=Double.parseDouble(tv_item_grand_total_total.getText().toString().trim())-promodiscount-promotion_discount;
                                    Double totalvalue = 0.0;
                                           /* Double.parseDouble(tv_item_grand_total_total.getText().toString().trim())-servicecharge;
                                    tv_item_grand_total_total.setText(String.format("%.2f", totalvalue));*/

                                    System.out.println("aaaaaaaaaaa serice charse  " + servicecharge);
                                    try {
                                        if (deliverydstatus.equalsIgnoreCase("601") || deliverydstatus.equalsIgnoreCase("602") || deliverydstatus.equalsIgnoreCase("603")) {
                                            totalvalue = subtotal - discount + tax - servicecharge - promodiscount - promotion_discount;
                                            System.out.println("aaaaaaaaa subtotal " + totalvalue + " value " + subtotal + " " + discount + " " + tax + " " + servicecharge);
                                            tv_item_grand_total_total.setText(getResources().getString(R.string.Rs) + String.format("%.2f", totalvalue));
                                        } else {
                                            totalvalue = subtotal - discount + tax - servicecharge - promodiscount - promotion_discount;
                                            // totalvalue=Double.parseDouble(tv_item_grand_total_total.getText().toString().trim())-servicecharge;
                                            tv_item_grand_total_total.setText(getResources().getString(R.string.Rs) + String.format("%.2f", totalvalue));
                                            System.out.println("aaaaaaaaa subtotal else  " + totalvalue + " value " + subtotal + " " + discount + " " + tax + " " + servicecharge);

                                        }
                                    } catch (NullPointerException e) {
                                        totalvalue = subtotal - discount + tax - servicecharge - promodiscount - promotion_discount;
                                        //  totalvalue=Double.parseDouble(tv_item_grand_total_total.getText().toString().trim())-servicecharge;
                                        tv_item_grand_total_total.setText(getResources().getString(R.string.Rs) + String.format("%.2f", totalvalue));
                                        System.out.println("aaaaaaaaa subtotal catch " + totalvalue + " value " + subtotal + " " + discount + " " + tax + " " + servicecharge);

                                    }

                                    if (servicecharge == 0.0) {
                                        layout_servicecharge.setVisibility(View.VISIBLE);  // future gone
                                    } else {
                                        layout_servicecharge.setVisibility(View.VISIBLE);
                                    }
                                    tv_servicecharge.setText("- " + getResources().getString(R.string.Rs) + String.format("%.2f", servicecharge));

                                }

                            }

                        } catch (JSONException e) {
                            if (mCustomDialog != null) {
                                if (mCustomDialog.isShowing()) {
                                    try {
                                        mCustomDialog.dismiss();
                                    } catch (IllegalArgumentException e1) {

                                    }
                                }
                            }
                            //   mCustomDialog.dismiss();
                            Toast.makeText(mContext, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                isRefreshStarted = false;
                if (mCustomDialog != null) {
                    if (mCustomDialog.isShowing()) {
                        try {
                            mCustomDialog.dismiss();
                        } catch (IllegalArgumentException e) {

                        }
                    }
                }
                //  mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaa token  " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void timezone(String timezone) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Date past = format.parse("2016.02.05 AD at 23:59:30");
            Date past = format.parse(timezone);

            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                tv_time.setText(seconds + " seconds ago");
                // System.out.println(seconds+" seconds ago");
            } else if (minutes < 60) {
                tv_time.setText(minutes + " minutes ago");
                System.out.println(minutes + " minutes ago");
            } else if (hours < 24) {
                tv_time.setText(hours + " hours ago");
                System.out.println(hours + " hours ago");
            } else {
                tv_time.setText(days + " days ago");
                System.out.println(days + " days ago");
            }
        } catch (Exception j) {
            System.out.println("aaaaa catch  " + j.getMessage());
            j.printStackTrace();
        }
    }

    public interface FragmentInterface {
        void sendDataMethod(boolean check);
    }

    private void generatePaymentOnlineLink() {
        LoadingDialog.loadDialog(this);
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("order", trackID);//

        String data = new JSONObject(dataMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(OrderDetailsActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PAYMENT_LINK,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        LoadingDialog.dialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Integer status = jsonObject.getInt("status_code");//status_code : 200
                            if (status == 200) {
                                Toast.makeText(OrderDetailsActivity.this, "Payment link has successfully sent to customer email id.", Toast.LENGTH_SHORT).show();
                                System.out.println("aaaaaaaaaa   payment link generated " + jsonObject);
                            } else
                                Toast.makeText(OrderDetailsActivity.this, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            Toast.makeText(OrderDetailsActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                Toast.makeText(OrderDetailsActivity.this, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}