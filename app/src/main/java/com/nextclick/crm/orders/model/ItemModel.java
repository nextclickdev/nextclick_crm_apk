package com.nextclick.crm.orders.model;

import java.util.ArrayList;

public class ItemModel {

    String id,name,desc,service_id,service_charge;
    ArrayList<ImagesModel> imagelist;
    VarientModel varientModel;

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getService_charge() {
        return service_charge;
    }

    public void setService_charge(String service_charge) {
        this.service_charge = service_charge;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<ImagesModel> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ImagesModel> imagelist) {
        this.imagelist = imagelist;
    }

    public VarientModel getVarientModel() {
        return varientModel;
    }

    public void setVarientModel(VarientModel varientModel) {
        this.varientModel = varientModel;
    }
}
