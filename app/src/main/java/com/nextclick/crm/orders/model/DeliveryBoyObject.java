package com.nextclick.crm.orders.model;

public class DeliveryBoyObject {

    String id,unique_id,first_name,phone,last_name;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }


    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    String ProfileImage;
    public void setProfileImage(String title) { this.ProfileImage = title; }
    public String getProfileImage() { return ProfileImage; }

    String delivery_boy_pickup_image,delivery_boy_delivery_image;
    public void setDelivery_boy_pickup_image(String delivery_boy_pickup_image) {
        this.delivery_boy_pickup_image=delivery_boy_pickup_image;
    }
    public String getDelivery_boy_pickup_image() { return delivery_boy_pickup_image; }

    public void setDelivery_boy_delivery_image(String delivery_boy_delivery_image) {
        this.delivery_boy_delivery_image=delivery_boy_delivery_image;
    }
    public String getDelivery_boy_delivery_image() { return delivery_boy_delivery_image; }
}
