package com.nextclick.crm.orders.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.dialogs.DatePickerResponse;
import com.nextclick.crm.dialogs.MyDatePickerDialog;
import com.nextclick.crm.orders.adapters.TransactionsAdapter;
import com.nextclick.crm.orders.model.Transactions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.WALLETHISTORY;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class WalletFragment extends Fragment implements DatePickerResponse {

    private RecyclerView txnListRecyclerView;
    private TextView tv_walletBalance,start_date,tv_get,end_date,tv_tran_header,tv_no_trans;
    private CustomDialog mCustomDialog;
    private Context mContext;
    private ArrayList<Transactions> transactionlist;
    private PreferenceManager preferenceManager;
    String year,month,day,start_date_str,end_date_str;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    TransactionsAdapter transactionsAdapter;
    private View view;
    LinearLayout layout_calendar;

    public WalletFragment(Context mContext) {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view =inflater.inflate(R.layout.fragment_wallet, container, false);
        init();
        return view;
    }
    public void init(){
        mContext= getContext();
        txnListRecyclerView=view.findViewById(R.id.txnListRecyclerView);
        tv_walletBalance=view.findViewById(R.id.tv_walletBalance);
        start_date=view.findViewById(R.id.start_date);
        tv_get=view.findViewById(R.id.tv_get);
        end_date=view.findViewById(R.id.end_date);
        layout_calendar=view.findViewById(R.id.layout_calendar);
        tv_tran_header=view.findViewById(R.id.tv_tran_header);
        tv_no_trans=view.findViewById(R.id.tv_no_trans);
        mCustomDialog=new CustomDialog(mContext);
        txnListRecyclerView.setLayoutManager(new GridLayoutManager(mContext,1));
        transactionlist=new ArrayList<>();
        preferenceManager=new PreferenceManager(mContext);
        transactionsAdapter = new TransactionsAdapter(mContext,transactionlist);

        txnListRecyclerView.setHasFixedSize(true);
        txnListRecyclerView.setAdapter(transactionsAdapter);

        getDate();

        getWallet(start_date_str,end_date_str,"","");

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datepicker(0);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((fromyear.equals("")) || (frommonth.equals("")) || (fromday.equals(""))){
                    Toast.makeText(mContext, "Please select startdate", Toast.LENGTH_SHORT).show();
                }else {
                    datepicker(1);
                }
            }
        });

        tv_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  int currentitem=orders_view_pager.getCurrentItem();
                //  AddTabsToFragments(currentitem);
                getWallet(start_date_str,end_date_str,"","");
            }
        });
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    start_date_str=fromyear+"-"+frommonth+"-"+fromday;
                    start_date.setText(""+fromday+"/"+frommonth+"/"+fromyear);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    end_date_str=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    //  end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+today+"/"+tomonth+"/"+toyear);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }
    public void getWallet(String start_date_str, String end_date_str, String s, String s1){
        transactionlist.clear();

        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", start_date_str);
        uploadMap.put("end_date", end_date_str);
        uploadMap.put("type", s);
        uploadMap.put("status", s1);

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+ json);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, WALLETHISTORY,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                String message=jsonObject.getString("message");
                                JSONObject dataobj=jsonObject.getJSONObject("data");


                                try{
                                    JSONArray transarray=dataobj.getJSONArray("wallet_transactions");
                                    for (int i=0;i<transarray.length();i++){
                                        JSONObject transobj=transarray.getJSONObject(i);
                                        Transactions transactionsPojo=new Transactions();
                                        transactionsPojo.setId(transobj.getString("id"));
                                        transactionsPojo.setTxn_id(transobj.getString("txn_id"));
                                        transactionsPojo.setAccount_user_id(transobj.getString("account_user_id"));
                                        transactionsPojo.setAmount(transobj.getString("amount"));
                                        transactionsPojo.setType(transobj.getString("type"));
                                        transactionsPojo.setBalance(transobj.getString("balance"));
                                        transactionsPojo.setEcom_order_id(transobj.getString("ecom_order_id"));
                                        transactionsPojo.setMessage(transobj.getString("message"));
                                        transactionsPojo.setCreated_at(transobj.getString("created_at"));
                                        transactionsPojo.setUpdated_at(transobj.getString("updated_at"));
                                        transactionsPojo.setStatus(transobj.getString("status"));
                                        try {
                                            if(transobj.has("track_id"))
                                                transactionsPojo.setTrack_id(transobj.getString("track_id"));
                                            else if (transobj.has("order") && transobj.getJSONObject("order").has("track_id"))
                                                transactionsPojo.setTrack_id(transobj.getJSONObject("order").getString("track_id"));
                                        }
                                        catch (Exception ex){}
                                        transactionlist.add(transactionsPojo);

                                    }

                                    transactionsAdapter.setrefresh(transactionlist);
                                    if(transactionlist!=null && transactionlist.size()>0) {
                                        tv_no_trans.setVisibility(View.GONE);
                                        tv_tran_header.setVisibility(View.VISIBLE);
                                        txnListRecyclerView.setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {
                                        tv_no_trans.setVisibility(View.VISIBLE);
                                        tv_tran_header.setVisibility(View.GONE);
                                        txnListRecyclerView.setVisibility(View.GONE);
                                    }

                                }catch (JSONException e1){
                                    System.out.println("aaaaaaa catch  "+e1.getMessage());
                                }

                                try{

                                    JSONObject paymentobj=dataobj.getJSONObject("user");
                                    tv_walletBalance.setText("₹ "+ Utility.roundFloat(paymentobj.getString("wallet"),2));
                                    //tv_walletBalance.setText("₹ "+paymentobj.getString("wallet"));
                                }catch (JSONException e2){

                                }
                            }

                        } catch (JSONException e) {
                            Toast.makeText(mContext, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }

        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

        start_date.setText(""+day+"/"+month+"/"+year);
        end_date.setText(""+day+"/"+month+"/"+year);

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
      /* int cHour = calander.get(Calendar.Ho);
        int cMinute = calander.get(Calendar.MINUTE);
        int cSecond = calander.get(Calendar.SECOND);
*/
    }

    public void doAction(String id) {
        layout_calendar.setVisibility(View.GONE);
        tv_tran_header.setVisibility(View.GONE);
        tv_no_trans.setVisibility(View.GONE);
        txnListRecyclerView.setVisibility(View.GONE);
        getDate();
        switch (id) {
            case "1901":
                getWallet(start_date_str, end_date_str, "", "");//today
                break;
            case "1902": {
                Calendar calander = Calendar.getInstance();
                // Add 8 months to current date
                calander.add(Calendar.DATE, -7);
                int mday = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                year = "" + calander.get(Calendar.YEAR);
                if ((cMonth) <= 9) {
                    month = 0 + "" + (cMonth);
                } else {
                    month = "" + (cMonth);
                }
                if (mday <= 9) {
                    day = 0 + "" + mday;
                } else {
                    day = "" + mday;
                }
                start_date_str = year + "-" + month + "-" + day;
                start_date.setText("" + day + "/" + month + "/" + year);
                getWallet(start_date_str, end_date_str, "", "");//weekly
            }
            break;
            case "1903": {
                Calendar calander = Calendar.getInstance();
                // Add 8 months to current date
                calander.add(Calendar.MONTH, -1);
                int mday = calander.get(Calendar.DAY_OF_MONTH);
                int cMonth = calander.get(Calendar.MONTH) + 1;
                year = "" + calander.get(Calendar.YEAR);
                if ((cMonth) <= 9) {
                    month = 0 + "" + (cMonth);
                } else {
                    month = "" + (cMonth);
                }
                if (mday <= 9) {
                    day = 0 + "" + mday;
                } else {
                    day = "" + mday;
                }

                start_date_str = year + "-" + month + "-" + day;
                start_date.setText("" + day + "/" + month + "/" + year);
                getWallet(start_date_str, end_date_str, "", "");//monthly
            }
            break;
            case "1904":
                //layout_calendar.setVisibility(View.VISIBLE);
                txnListRecyclerView.setVisibility(View.GONE);
                break;
        }
    }

    public void onCalendarAction(FragmentManager supportFragmentManager) {
        MyDatePickerDialog dialog=new MyDatePickerDialog();
        //dialog.showDialog(supportFragmentManager,this);
        dialog.showStartAndEndDatePickers(mContext,this);
    }


    @Override
    public Context getActivityContext() {
        return mContext;
    }

    @Override
    public void onDateSelected(String startDate, String endDate) {
        start_date_str =startDate;
        start_date.setText(startDate);
        end_date_str =endDate;
        end_date.setText(endDate);
        getWallet(start_date_str, end_date_str, "", "");//monthly
    }
}