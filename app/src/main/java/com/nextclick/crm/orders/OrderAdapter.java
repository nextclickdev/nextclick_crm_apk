package com.nextclick.crm.orders;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.activity.OrderDetailsActivity;
import com.nextclick.crm.orders.activity.TrackOrderActivity;
import com.nextclick.crm.orders.model.OrderHistoryModel;

import java.util.ArrayList;
import java.util.Calendar;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {


    LayoutInflater inflter;
    Context context;
    private String token;
    ArrayList<OrderHistoryModel> data;


    public OrderAdapter(Context activity, ArrayList<OrderHistoryModel> orderslist) {
        this.context = activity;
        this.data = orderslist;
    }


    @NonNull
    @Override
    public OrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.order_history_new, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new OrderAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderAdapter.ViewHolder holder, final int position) {
        final OrderHistoryModel orderModel = data.get(position);

        System.out.println("aaaaaaaaaa orderid  "+data.get(position).getTrack_id());
        holder.tv_orderid.setText("Order #"+data.get(position).getTrack_id());
        Double totalamount=0.0;
        try{
             totalamount=Double.parseDouble(data.get(position).getTotal())-Double.parseDouble(data.get(position).getDelivery_fee());
        }catch (NumberFormatException e){
             totalamount=Double.parseDouble(data.get(position).getTotal());
        }
       // holder.total_amount.setText(context.getString(R.string.Rs) +String.format("%.2f",totalamount));
        holder.total_amount.setText("₹ "+ Utility.roundFloat(""+totalamount,2));

        try
        {
            String paymentMethod="";
            switch (data.get(position).getPaymentModel().getPaymentMethod().getId())
            {
                case "1":
                    paymentMethod="Cash on Delivery";
                    break;
                case "3":
                    paymentMethod="Wallet Money";
                    break;
                case "4":
                    paymentMethod="Online Payment Link";
                    break;
                default:
                    paymentMethod="Online";
            }
            holder.tv_paymentmethod.setText("Payment type : "+paymentMethod);
        }
        catch (Exception ex)
        {
            holder.tv_paymentmethod.setText("Payment type : "+data.get(position).getPaymentModel().getPaymentMethod().getName());
        }

        int color =R.color.order_preparing;

        holder.tv_trackdriver.setVisibility(View.GONE);

        try {

            String deliveryPatnerStatus = data.get(position).getOrderStatus().getDeliveryPatnerStatus();
            boolean delBoyStatus = (deliveryPatnerStatus == null || deliveryPatnerStatus.isEmpty() || deliveryPatnerStatus.equals("null"));

            if (!delBoyStatus && deliveryPatnerStatus.equals("500") && deliveryPatnerStatus.equals("501"))
                holder.tv_trackdriver.setVisibility(View.VISIBLE);

            String statusID = Utility.getCurrentOrderStatus(data.get(position).getOrderStatus());


            String status = data.get(position).getOrderStatus().getStatus();
            switch (statusID) {
                case "1":
                case "9"://received
                    color = R.color.order_received;
                    break;
                case "11":
                case "3"://Order has been Preparing
                    color = R.color.order_preparing;
                    break;
                case "4"://ready to pickup
                    color = R.color.outfor_delivery;
                    break;
                case "12":
                    color = R.color.outfor_delivery;
                    break;
                case "5":
                    color = R.color.picked_up;
                    break;
                case "16":
                case "17": //rejected
                    color = R.color.order_cancelled;
                    break;
                case "500":
                    status = "Rejected By Delivery Patner";
                    data.get(position).getOrderStatus().setStatus("Rejected By Delivery Patner");
                    color = R.color.order_rejected;
                    holder.tv_trackdriver.setVisibility(View.GONE);
                    break;
                case "501":
                    status = "Received";
                    data.get(position).getOrderStatus().setStatus("Received");
                    color = R.color.order_received;
                    holder.tv_trackdriver.setVisibility(View.GONE);
                    break;
                case "502":
                    status = "Accepted";
                    data.get(position).getOrderStatus().setStatus("Accepted");
                    holder.tv_trackdriver.setVisibility(View.VISIBLE);
                    color = R.color.order_accepted;
                    break;
                case "503":
                    status = "Cancelled";
                    data.get(position).getOrderStatus().setStatus("Cancelled");
                    color = R.color.order_cancelled;
                    break;
                case "504":
                    status = "Reached to pickup point";
                    data.get(position).getOrderStatus().setStatus("Reached to pickup point");
                    holder.tv_trackdriver.setVisibility(View.VISIBLE);
                    color = R.color.dp_reached_pickup_point;
                    break;
                case "505":
                    status = "Delivery Patner Picked the order";
                    data.get(position).getOrderStatus().setStatus("Delivery Patner Picked the order");
                    holder.tv_trackdriver.setVisibility(View.VISIBLE);
                    color = R.color.outfor_delivery;
                    break;
                case "506":
                    status = "Delivery Patner Reached to delivery point";
                    data.get(position).getOrderStatus().setStatus("Delivery Patner Reached to delivery point");
                    holder.tv_trackdriver.setVisibility(View.VISIBLE);
                    color = R.color.dp_reached_delivery_point;
                    break;
                case "507":
                    status = "Delivery is on hold";
                    data.get(position).getOrderStatus().setStatus("Delivery is on hold");
                    break;
                case "508":
                    status = "Delivered";
                    data.get(position).getOrderStatus().setStatus("Delivered");
                    holder.tv_trackdriver.setVisibility(View.GONE);
                    color = R.color.order_completed;
                    break;
                case "603":
                    status = "Returned";
                    data.get(position).getOrderStatus().setStatus("Returned");
                    holder.tv_trackdriver.setVisibility(View.GONE);
                    color = R.color.order_cancelled;
                    break;
                default:
                    status = "Received";
                    data.get(position).getOrderStatus().setStatus("Received");
                    break;
            }
            /*if (data.get(position).getOrderStatus().getId().equalsIgnoreCase("4")||
                    data.get(position).getOrderStatus().getId().equalsIgnoreCase("12")){
                holder.tv_status.setText("Out for delivery");
                color = R.color.green_900;
            }else
             */
            {
                holder.tv_status.setText("" + status);
            }

        }catch (NullPointerException e) {
            holder.tv_status.setText(data.get(position).getOrderStatus().getStatus());
        }


        holder.layout_order_background.setBackgroundColor(context.getResources().getColor(color));


        String currentString = data.get(position).getCreated_at();
        String[] separated = currentString.split(" ");
        String date=separated[0];
        String time=separated[1];

        String[] datespilt=date.split("-");
        int datest=Integer.parseInt(datespilt[2]);
        int month=Integer.parseInt(datespilt[1]);
        int year=Integer.parseInt(datespilt[0]);

        String[] timespilt=time.split(":");
        String hour=timespilt[0];
        String minitu=timespilt[1];
        String sec=timespilt[2];

        Calendar calendar=Calendar.getInstance();

        calendar.set(year, month, datest,
                Integer.parseInt(hour), Integer.parseInt(minitu), Integer.parseInt(sec));


        String text= getFormattedDate(calendar.getTimeInMillis());
        holder.tv_cerate.setText(""+text);
        // holder.tv_cerate.setText(""+orderslist.get(position).getCreated_at());

       /* if (isnullboolean(orderslist.get(position).getOrder_delivery_otp())){
            // holder.layout_otp.setVisibility(View.VISIBLE);
            holder.tv_otp.setText(orderslist.get(position).getOrder_delivery_otp());
        }else {
            // holder.layout_otp.setVisibility(View.GONE);
            holder.tv_otp.setText("NA");
        }*/


       /* int sts = orderModel.getOrderStatus().getStatus();
        String sts_str = "";
        if (sts == 0) {
            sts_str = "Order Rejected";
            holder.order_status_card.setCardBackgroundColor(Color.RED);
        } else if (sts == 1) {
            sts_str = "Order Received";

        } else if (sts == 2) {
            sts_str = "Order Accepted";
            holder.order_status_card.setCardBackgroundColor(Color.GREEN);
        } else if (sts == 3) {
            sts_str = "Order Preparing";
            holder.order_status_card.setCardBackgroundColor(Color.GRAY);
        } else if (sts == 4) {
            sts_str = "Out For Delivery";
            holder.order_status_card.setCardBackgroundColor(Color.BLUE);
        } else if (sts == 5) {
            sts_str = "On the way";
            holder.order_status_card.setCardBackgroundColor(Color.parseColor("#B8860B"));
        } else if (sts == 6) {
            sts_str = "Order Completed";
            holder.order_status_card.setCardBackgroundColor(Color.parseColor("#228B22"));
        } else if (sts == 7) {
            sts_str = "Order Cancelled";
            holder.order_status_card.setCardBackgroundColor(Color.RED);
        }*/


     //   holder.order_status.setText(sts_str);
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("order_id", orderModel.getId());
                intent.putExtra("status", data.get(position).getOrderStatus().getStatus());

                context.startActivity(intent);
            }
        });
        holder.tv_trackdriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, TrackOrderActivity.class);
                i.putExtra("order_id", orderModel.getId());
                //i.putExtra("orderdetails", (Serializable) orderModel);
                context.startActivity(i);
            }
        });


    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_orderid,tv_trackdriver,tv_status,total_amount,tv_paymentmethod,tv_cerate,ordered_person_name, order_payment_type, order_sub_items, order_items, order_total, order_status, tvViewDetails;
        CardView card_view;
        private final LinearLayout layout_order_background;

        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_orderid = itemView.findViewById(R.id.tv_orderid);
            tv_status = itemView.findViewById(R.id.tv_status);
            total_amount = itemView.findViewById(R.id.total_amount);
            tv_paymentmethod = itemView.findViewById(R.id.tv_paymentmethod);
            tv_cerate = itemView.findViewById(R.id.tv_cerate);
            ordered_person_name = itemView.findViewById(R.id.ordered_person_name);
            order_payment_type = itemView.findViewById(R.id.order_payment_type);
            order_sub_items = itemView.findViewById(R.id.order_sub_items);
            order_items = itemView.findViewById(R.id.order_items);
            order_total = itemView.findViewById(R.id.order_total);
            order_status = itemView.findViewById(R.id.order_status);
            card_view = itemView.findViewById(R.id.card_view);
            tvViewDetails = itemView.findViewById(R.id.tvViewDetails);
            layout_order_background = itemView.findViewById(R.id.layout_order_background);
            tv_trackdriver = itemView.findViewById(R.id.tv_trackdriver);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }
    public String getFormattedDate(long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();

        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "EEE, MMM d, h:mm aa";
        final long HOURS = 60 * 60 * 60;
      //  System.out.println("aaaaaaaa check "+now.get(Calendar.DATE) +"  "+smsTime.get(Calendar.DATE));

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "Today " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday " + DateFormat.format(timeFormatString, smsTime);
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            smsTime.set(Calendar.MONTH,smsTime.get(Calendar.MONTH)-1);
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MMM dd yyyy, h:mm aa", smsTime).toString();
        }
    }
}
