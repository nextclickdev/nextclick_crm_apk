package com.nextclick.crm.orders.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.OrdersAdapter;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.OrderAdapter;
import com.nextclick.crm.orders.model.DeliveryJob;
import com.nextclick.crm.orders.model.OrderHistoryModel;
import com.nextclick.crm.orders.model.OrderStatus;
import com.nextclick.crm.orders.model.PaymentMethod;
import com.nextclick.crm.orders.model.PaymentModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Constants.Constants.USER_TOKEN;


public class OrderHistoryDataFragment extends Fragment {


    private final OrdersHistoryFragment ordersHistoryFragment;
    private RecyclerView orders_recycler;
    private Context mContext;
    PreferenceManager preferenceManager;
    private View root;
    private String token;
    private final String orders_url;
    private String start_date_str;
    private String end_date_str;
    private final OrdersAdapter ordersAdapter = null;
    private OrderAdapter ordershistoryAdapter = null;
    TextView emptyView;
    private ArrayList<OrderHistoryModel> orderslist;
    private CustomDialog mCustomDialog;
    String status;
    String year,month,day;
    private TextView start_date, end_date,get;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    private boolean isViewCreated =false;

    public OrderHistoryDataFragment(OrdersHistoryFragment ordersHistoryFragment,String orders_url, String start_date_str, String end_date_str,String status) {
        // Required empty public constructor
        this.ordersHistoryFragment=ordersHistoryFragment;
        this.orders_url = orders_url;
        this.start_date_str = start_date_str;
        this.end_date_str = end_date_str;
        System.out.println("aaaaaaaaaa startdate end date   "+start_date_str +"  "+end_date_str+" status  "+status);
        this.status = status;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_order_data, container, false);

        initView();
      //  if (getActivity() != null) {
           // getOrders();

      //  }
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datepicker(0);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((fromyear.equals("")) || (frommonth.equals("")) || (fromday.equals(""))){
                    Toast.makeText(mContext, "Please select startdate", Toast.LENGTH_SHORT).show();
                }else {
                    datepicker(1);
                }
            }
        });

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  int currentitem=orders_view_pager.getCurrentItem();
              //  AddTabsToFragments(currentitem);
                getOrders(start_date_str,end_date_str,status);
            }
        });
        return root;
    }
    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        start_date.setText(day+"-"+month+"-"+year);
        end_date.setText(day+"-"+month+"-"+year);
        start_date_str=year+"-"+month+"-"+day;
        end_date_str=year+"-"+month+"-"+day;

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
      /* int cHour = calander.get(Calendar.Ho);
        int cMinute = calander.get(Calendar.MINUTE);
        int cSecond = calander.get(Calendar.SECOND);
*/
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // initView();
    }

    private void initView() {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        orders_recycler = root.findViewById(R.id.orders_recycler);
        emptyView= root.findViewById(R.id.empty_view);
        start_date = root.findViewById(R.id.start_date);
        end_date = root.findViewById(R.id.end_date);
        get = root.findViewById(R.id.apply);
        orderslist=new ArrayList<>();
        mCustomDialog=new CustomDialog(mContext);
        //getDate();-while creating itself passing dates
        System.out.println("aaaaaaaaaa  orderhistort  "+start_date_str+"  "+end_date_str+"   status "+status);

        if(ordersHistoryFragment!=null)
        {
            start_date_str=ordersHistoryFragment.startdate;
            end_date_str=ordersHistoryFragment.enddate;
        }

        getOrders(start_date_str,end_date_str,status);
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            //  System.out.println("aaaaaa currentdate  "+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+"  "+c.get(Calendar.YEAR));
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    start_date_str=fromyear+"-"+frommonth+"-"+fromday;
                    //   System.out.println("aaaaaaaa  startdate   "+startdate);
                    //start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    start_date.setText(""+start_date_str);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    end_date_str=toyear+"-"+tomonth+"-"+today;
                    //   System.out.println("aaaaaaaa  enddate   "+enddate);

                    // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+end_date_str);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
            c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
            //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
            int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("aaaaaaaaaa  orderhistort resume " + start_date_str + "  " + end_date_str);
        if (isViewCreated)
            getOrders(start_date_str, end_date_str, status);
    }

    public void getOrders(String startdate, String enddate, String status){
        orderslist.clear();
        Map<String, String> uploadMap = new HashMap<>();

        uploadMap.put("start_date", startdate);
        uploadMap.put("end_date", enddate);

        if(status.equals("500"))
        {
            uploadMap.put("status", "0");
            uploadMap.put("delivery_boy_status", ""+500);
        }
        else
            uploadMap.put("status", ""+status);

        if (status.isEmpty()||status.equalsIgnoreCase("")) {
            uploadMap.put("delivery_boy_status", "" + 500);
        }

        JSONObject json = new JSONObject(uploadMap);

        System.out.println("aaaaaaaaa request"+ json +"  url  "+orders_url);
        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, orders_url,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        mCustomDialog.dismiss();
                        isViewCreated = true;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            System.out.println("aaaaaaaa jsonobject  "+ jsonObject);
                            boolean status=jsonObject.getBoolean("status");
                            int http_code=jsonObject.getInt("http_code");

                            if (status){
                                orderslist.clear();
                                String message=jsonObject.getString("message");
                                JSONArray jsonArray1=jsonObject.getJSONArray("data");
                                for (int i=0;i<jsonArray1.length();i++){
                                    JSONObject orderobject=jsonArray1.getJSONObject(i);
                                    OrderHistoryModel orderHistoryModel=new OrderHistoryModel();
                                    orderHistoryModel.setId(orderobject.getString("id"));
                                    orderHistoryModel.setTrack_id(orderobject.getString("track_id"));
                                    orderHistoryModel.setPreparation_time(orderobject.getString("preparation_time"));
                                    orderHistoryModel.setShipping_address_id(orderobject.getString("shipping_address_id"));
                                    orderHistoryModel.setDelivery_mode_id(orderobject.getString("delivery_mode_id"));
                                    orderHistoryModel.setTotal(orderobject.getString("total"));
                                    orderHistoryModel.setDelivery_fee(orderobject.getString("delivery_fee"));
                                    orderHistoryModel.setPayment_id(orderobject.getString("payment_id"));
                                    orderHistoryModel.setMessage(orderobject.getString("message"));
                                    orderHistoryModel.setCreated_user_id(orderobject.getString("created_user_id"));
                                    orderHistoryModel.setVendor_user_id(orderobject.getString("vendor_user_id"));
                                    orderHistoryModel.setCreated_at(orderobject.getString("created_at"));
                                    orderHistoryModel.setUpdated_at(orderobject.getString("updated_at"));
                                    orderHistoryModel.setDelivery_job_status(orderobject.getString("delivery_job_status"));



                                    JSONObject paymentobj=orderobject.getJSONObject("payment");
                                    PaymentModel paymentModel=new PaymentModel();
                                    paymentModel.setId(paymentobj.getString("id"));
                                    paymentModel.setTxn_id(paymentobj.getString("txn_id"));
                                    paymentModel.setAmount(paymentobj.getString("amount"));
                                    paymentModel.setCreated_at(paymentobj.getString("created_at"));
                                    paymentModel.setMessage(paymentobj.getString("message"));
                                    paymentModel.setStatus(paymentobj.getString("status"));
                                    paymentModel.setPayment_method_id(paymentobj.getString("payment_method_id"));

                                    PaymentMethod paymentMethod=new PaymentMethod();
                                    JSONObject paymentmethodobj=paymentobj.getJSONObject("payment_method");
                                    paymentMethod.setId(paymentmethodobj.getString("id"));
                                    paymentMethod.setName(paymentmethodobj.getString("name"));
                                    paymentMethod.setDescription(paymentmethodobj.getString("description"));

                                    paymentModel.setPaymentMethod(paymentMethod);
                                    orderHistoryModel.setPaymentModel(paymentModel);


                                    try{
                                        JSONObject oederstatusobj=orderobject.getJSONObject("order_status");
                                        OrderStatus orderStatus=new OrderStatus();
                                        orderStatus.setId(oederstatusobj.getString("id"));
                                        orderStatus.setDelivery_mode_id(oederstatusobj.getString("delivery_mode_id"));
                                        orderStatus.setStatus(oederstatusobj.getString("status"));
                                        orderStatus.setSerial_number(oederstatusobj.getString("serial_number"));

                                        try {
                                            if(orderobject.has("delivery_job_status")) {
                                                orderStatus.setDeliveryPatnerStatus(orderobject.getString("delivery_job_status"));
                                            }
                                        }
                                        catch (Exception ex)
                                        {

                                        }


                                        try{
                                            JSONObject deliveryobj=orderobject.getJSONObject("delivery_job");
                                            JSONObject rejectreason=deliveryobj.getJSONObject("rejected_reason");
                                            String reason=rejectreason.getString("reason");
                                            orderStatus.setStatus(""+reason);
                                        }catch (Exception e){

                                        }

                                        try{
                                            JSONObject delivery_object=orderobject.getJSONObject("delivery_job");
                                            DeliveryJob deliveryJob=new DeliveryJob();
                                            deliveryJob.setId(delivery_object.getString("id"));
                                            deliveryJob.setJob_id(delivery_object.getString("job_id"));
                                            deliveryJob.setRating(delivery_object.getString("rating"));
                                            deliveryJob.setFeedback(delivery_object.getString("feedback"));
                                            deliveryJob.setJob_type(delivery_object.getString("job_type"));
                                            deliveryJob.setDelivery_boy_user_id(delivery_object.getString("delivery_boy_user_id"));
                                            deliveryJob.setStatus(delivery_object.getString("status"));
                                            deliveryJob.setRejected_reason_id(delivery_object.getString("rejected_reason_id"));
                                            deliveryJob.setRejected_reason(delivery_object.getString("rejected_reason"));
                                            deliveryJob.setFirst_name(delivery_object.getJSONObject("delivery_boy").getString("first_name"));
                                            deliveryJob.setLast_name(delivery_object.getJSONObject("delivery_boy").getString("last_name"));
                                            deliveryJob.setPhone(delivery_object.getJSONObject("delivery_boy").getString("phone"));
                                            deliveryJob.setUnique_id(delivery_object.getJSONObject("delivery_boy").getString("unique_id"));
                                            orderHistoryModel.setDeliveryJob(deliveryJob);
                                        }catch (Exception e){

                                        }

                                        orderHistoryModel.setOrderStatus(orderStatus);

                                    }catch(JSONException  e){
                                        System.out.println("aaaaaaaa orderstatus "+e.getMessage());

                                    }

                                    orderslist.add(orderHistoryModel);
                                }

                                ordershistoryAdapter = new OrderAdapter(mContext, orderslist);

                                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

                                    @Override
                                    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                        LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                            private static final float SPEED = 300f;// Change this value (default=25f)

                                            @Override
                                            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                return SPEED / displayMetrics.densityDpi;
                                            }

                                        };
                                        smoothScroller.setTargetPosition(position);
                                        startSmoothScroll(smoothScroller);
                                    }

                                };
                                orders_recycler.setLayoutManager(layoutManager);
                                orders_recycler.setAdapter(ordershistoryAdapter);
                                if (orderslist.isEmpty()) {
                                    System.out.println("aaaaaaaa list empty");
                                    orders_recycler.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                }
                                else {
                                    System.out.println("aaaaaaaa list non empty");
                                    orders_recycler.setVisibility(View.VISIBLE);
                                    emptyView.setVisibility(View.GONE);
                                }

                            }

                        } catch (JSONException e) {
                            orders_recycler.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                          //  Toast.makeText(mContext, "No orders found", Toast.LENGTH_SHORT).show();
                            System.out.println("aaaaaaaaaa   catch " + e);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                orders_recycler.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                mCustomDialog.dismiss();
              //  Toast.makeText(mContext, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        })
        {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaaaa token  "+preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}