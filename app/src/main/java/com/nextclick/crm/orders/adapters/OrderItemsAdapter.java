package com.nextclick.crm.orders.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.orders.activity.OrderDetailsActivity;
import com.nextclick.crm.orders.model.ProductDetailsModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder>  {

    List<ProductDetailsModel> data;
    Context context;
    int whichposition;
    String deliverydstatus;

    public OrderItemsAdapter(Context mContext, ArrayList<ProductDetailsModel> productlist,
                             int whichposition,String deliverydstatus) {
        this.context = mContext;
        this.data = productlist;
        this.whichposition = whichposition;
        this.deliverydstatus = deliverydstatus;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.order_products, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        ProductDetailsModel orderItems = data.get(position);

        String productName = orderItems.getItemModel().getName().substring(0, 1).toUpperCase() +
                orderItems.getItemModel().getName().substring(1);

        if (whichposition==1){
            holder.layout_main.setBackground(context.getResources().getDrawable(R.drawable.background_blue_line));
            holder.checkbox_product.setVisibility(View.VISIBLE);
            holder.layout_discounts.setVisibility(View.GONE);
            holder.checkbox_product.setChecked(orderItems.isCheckstatus());
        }else{
            holder.checkbox_product.setVisibility(View.GONE);
            holder.layout_discounts.setVisibility(View.VISIBLE);
        }

        holder.tv_pname.setText(productName);
        try{
            holder.tv_pdescription.setText(orderItems.getItemModel().getVarientModel().getSectionItemModel().getName()+"");
        }catch (NullPointerException e){

        }
        try{
            String promoamount=UIMsgs.isNull(orderItems.getPromocode_discount());
            if (promoamount.isEmpty()||promoamount.equalsIgnoreCase("") ){
                holder.tv_promodiscount.setVisibility(View.GONE);
            }else {
                holder.tv_promodiscount.setTextColor(context.getResources().getColor(R.color.orange));
                holder.tv_promodiscount.setText("Promo "+context.getResources().getString(R.string.Rs)+ UIMsgs.isNull(orderItems.getPromocode_discount()));
            }

        } catch (NullPointerException e){

        }
        try{
            Glide.with(context)
                    .load(orderItems.getItemModel().getImagelist().get(0).getImage())
                    .placeholder(R.drawable.loader_gif)
                    .into(holder.img_product);
        }catch (IndexOutOfBoundsException | NullPointerException e ){

        }



        try{
            if (deliverydstatus.equalsIgnoreCase("601")||deliverydstatus.equalsIgnoreCase("602")||deliverydstatus.equalsIgnoreCase("603") ){
                if (orderItems.getItemModel().getVarientModel().getReturn_available().equalsIgnoreCase("1")){
                    holder.layout_return.setVisibility(View.VISIBLE);
                }else{
                    holder.layout_return.setVisibility(View.GONE);
                }
            }else{
                holder.layout_return.setVisibility(View.GONE);
            }
        }catch (Exception e){
            holder.layout_return.setVisibility(View.GONE);

        }

        if (orderItems.getStatus().equalsIgnoreCase("4")){
            holder.layout_reject.setVisibility(View.VISIBLE);
            holder.tv_reject.setText("Rejected");
        }else{
            holder.layout_reject.setVisibility(View.GONE);
        }
        holder.tv_quantity.setText("Quantity : "+orderItems.getQty()+"");

        holder.tv_subtotal.setText("Sub Total : ₹"+orderItems.getSub_total()+"");

        if (orderItems.getDiscount().equalsIgnoreCase("0")) {
            holder.tv_discount.setVisibility(View.GONE);
        }else {
            holder.tv_discount.setVisibility(View.VISIBLE);
            holder.tv_discount.setText("Discount : ₹"+orderItems.getDiscount()+"");
        }

        holder.tv_tax.setText("Tax : \u20B9" +orderItems.getTax()+"");
        double promotionamt=0.0;

        try {

            double weight=Double.parseDouble(orderItems.getItemModel().getVarientModel().getSectionItemModel().getWeight());
            holder.tv_weiht.setText((weight/1000)+ " kgs");
        }
        catch (Exception ex) {
            holder.tv_weiht.setText(orderItems.getItemModel().getVarientModel().getSectionItemModel().getWeight());
        }
        try{
            promotionamt=Double.parseDouble(orderItems.getPromotion_banner_discount());
        }catch (NullPointerException | NumberFormatException e){

        }

        try{
            double totalamt=Double.parseDouble(orderItems.getTotal())-Double.parseDouble(orderItems.getPromocode_discount())-promotionamt;
            holder.tv_totalprice.setText("Total : ₹"+totalamt+"");
        }catch (NullPointerException | NumberFormatException e){
            System.out.println("aaaaaaa catch  "+e.getMessage());
            double totalamt=Double.parseDouble(orderItems.getTotal());
            holder.tv_totalprice.setText("Total : ₹"+totalamt+"");
        }

        try{
            String promotionid=UIMsgs.isNull(orderItems.getPromotion_banner_discount_type_id());
            if (!promotionid.equalsIgnoreCase("")|| !promotionid.isEmpty()){
                holder.layout_offer.setVisibility(View.VISIBLE);
                if (promotionid.equalsIgnoreCase("1")){
                    holder.layout_offer.setVisibility(View.GONE);
                    holder.tv_promotion_discount.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setText("Extra ₹ "+orderItems.getPromotion_banner_discount()+" off on this product");
                }else if (promotionid.equalsIgnoreCase("2")){
                    holder.layout_offer.setVisibility(View.GONE);
                    holder.tv_promotion_discount.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setText("Extra ₹ "+orderItems.getPromotion_banner_discount()+" off on this product");
                }
                else if (promotionid.equalsIgnoreCase("3")){
                    String offer_producy_qty=UIMsgs.isNull(orderItems.getOffer_product_qty());
                    holder.layout_offer.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setVisibility(View.GONE);
                    //  holder.tv_quantity.setText("Quantity : "+(Integer.parseInt(orderItems.getQty())+Integer.parseInt(orderItems.getOffer_product_qty()))+"");
                    holder.tv_offer_product.setText("Buy 1 Get 1 Offer Product");
                    holder.tv_offerproductqty.setText("Offer quantity : "+orderItems.getOffer_product_qty());
                    holder.tv_offerproductname.setText("");
                    try {
                        Glide.with(context)
                                .load(orderItems.getItemModel().getImagelist().get(0).getImage())
                                .placeholder(R.drawable.no_image)
                                .into(holder.img_offerproduct);
                    } catch (IndexOutOfBoundsException e) {
                    }
                }else if (promotionid.equalsIgnoreCase("4")){
                    holder.layout_offer.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setVisibility(View.GONE);
                    holder.tv_offerproductname.setText(""+orderItems.getSetOfferitemmodel().getName()+"( "+orderItems.getSetOfferitemmodel().getVarientModel().getSectionItemModel().getName()+" )");
                    try {
                        Glide.with(context)
                                .load(orderItems.getSetOfferitemmodel().getImagelist().get(0).getImage())
                                .placeholder(R.drawable.no_image)
                                .into(holder.img_offerproduct);
                    } catch (IndexOutOfBoundsException e) {
                    }
                    holder.tv_offerproductqty.setText("Offer quantity : "+orderItems.getOffer_product_qty());

                }else if (promotionid.equalsIgnoreCase("5")){
                    holder.layout_offer.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setVisibility(View.GONE);
                    holder.tv_offerproductname.setText(""+orderItems.getSetOfferitemmodel().getName()+"( "+orderItems.getSetOfferitemmodel().getVarientModel().getSectionItemModel().getName()+" )");
                    try {
                        Glide.with(context)
                                .load(orderItems.getSetOfferitemmodel().getImagelist().get(0).getImage())
                                .placeholder(R.drawable.no_image)
                                .into(holder.img_offerproduct);
                    } catch (IndexOutOfBoundsException e) {
                    }
                    holder.tv_offerproductqty.setText("Offer quantity : "+orderItems.getOffer_product_qty());

                }else if (promotionid.equalsIgnoreCase("6")){
                    holder.layout_offer.setVisibility(View.VISIBLE);
                    holder.tv_promotion_discount.setVisibility(View.GONE);
                    holder.tv_offerproductname.setText(""+orderItems.getSetOfferitemmodel().getName()+"( "+orderItems.getSetOfferitemmodel().getVarientModel().getSectionItemModel().getName()+" )");
                    try {
                        Glide.with(context)
                                .load(orderItems.getSetOfferitemmodel().getImagelist().get(0).getImage())
                                .placeholder(R.drawable.no_image)
                                .into(holder.img_offerproduct);
                    } catch (IndexOutOfBoundsException e) {
                    }
                    holder.tv_offerproductqty.setText("Offer quantity : "+orderItems.getOffer_product_qty());

                }
                else {
                    holder.tv_promotion_discount.setVisibility(View.GONE);
                    holder.layout_offer.setVisibility(View.GONE);
                }
            }else {
                holder.tv_promotion_discount.setVisibility(View.GONE);
                holder.layout_offer.setVisibility(View.GONE);
            }
        }catch (NullPointerException | NumberFormatException e){
            holder.layout_offer.setVisibility(View.GONE);
            holder.tv_promotion_discount.setVisibility(View.GONE);
        }


        holder.checkbox_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (orderItems.isCheckstatus()){
                    orderItems.setCheckstatus(false);
                    holder.checkbox_product.setChecked(false);
                    ((OrderDetailsActivity)context).checkboxselect(0);
                }else{
                    orderItems.setCheckstatus(true);
                    holder.checkbox_product.setChecked(true);
                    ((OrderDetailsActivity)context).checkboxselect(1);
                }

            }
        });
       /* holder.checkbox_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                orderItems.setCheckstatus(isChecked);
                ((OrderDetailsActivity)context).checkboxselect();
            }
        });*/

    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}

    public void setchange(ArrayList<ProductDetailsModel> productlist) {
        this.data=productlist;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_weiht,tv_reject,tv_offerproductname,tv_promotion_discount,tv_offerproductqty,tv_offer_product,tv_pname,tv_promodiscount,tv_pdescription,tv_subtotal,tv_discount,tv_tax,tv_quantity,tv_totalprice;
        ImageView img_product,img_offerproduct;
        private final LinearLayout layout_offer;
        private final LinearLayout layout_discounts;
        private final LinearLayout layout_reject;
        private final LinearLayout layout_return;
        private final RelativeLayout layout_main;
        private final CheckBox checkbox_product;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_product = itemView.findViewById(R.id.img_product);
            tv_pname = itemView.findViewById(R.id.tv_pname);
            tv_pdescription = itemView.findViewById(R.id.tv_pdescription);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_subtotal = itemView.findViewById(R.id.tv_subtotal);
            tv_discount = itemView.findViewById(R.id.tv_discount);
            tv_tax = itemView.findViewById(R.id.tv_tax);
            tv_totalprice = itemView.findViewById(R.id.tv_totalprice);
            tv_promodiscount = itemView.findViewById(R.id.tv_promodiscount);
            layout_offer = itemView.findViewById(R.id.layout_offer);
            layout_reject = itemView.findViewById(R.id.layout_reject);

            tv_offer_product = itemView.findViewById(R.id.tv_offer_product);
            tv_offerproductqty = itemView.findViewById(R.id.tv_offerproductqty);
            tv_offerproductname = itemView.findViewById(R.id.tv_offerproductname);
            img_offerproduct = itemView.findViewById(R.id.img_offerproduct);
            tv_promotion_discount = itemView.findViewById(R.id.tv_promotion_discount);
            layout_main = itemView.findViewById(R.id.layout_main);
            checkbox_product = itemView.findViewById(R.id.checkbox_product);
            layout_discounts = itemView.findViewById(R.id.layout_discounts);
            tv_reject = itemView.findViewById(R.id.tv_reject);
            layout_return = itemView.findViewById(R.id.layout_return);
            tv_weiht = itemView.findViewById(R.id.tv_weiht);


            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }


}
