package com.nextclick.crm.orders.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.nextclick.crm.Common.Adapters.ServicesAdapter;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.dialogs.DatePickerResponse;
import com.nextclick.crm.dialogs.MyDatePickerDialog;
import com.nextclick.crm.orders.activity.OrderDetailsActivity;
import com.nextclick.crm.orders.model.OrderHistoryModel;
import com.google.android.material.tabs.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.nextclick.crm.Config.Config.ORDERHISTORY;


public class OrdersHistoryFragment extends Fragment implements OrderDetailsActivity.FragmentInterface,
        DatePickerResponse {

    private View root;
    private TabLayout tabLayout;
    private ViewPager orders_view_pager;
    private Context mContext;
    private TextView start_date, end_date;
    private TextView get;
    private ArrayList<OrderHistoryModel> orderslist;
    private PreferenceManager preferenceManager;
    private String fromday="",frommonth="",fromyear="",today="",tomonth="",toyear="";
    public String startdate="",enddate="";
    String year,month,day;
    private ServicesAdapter servicesAdapter;

    public OrdersHistoryFragment() {

      /*  Order Received,
        Order Rejected,
        Order Accepted,
        Order Preparing,
        Out For Delivery,
                On the way,
        Order Completed,
        Order Cancelled*/
    }

    public OrdersHistoryFragment(Context mContext) {

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.orders_fragments, container, false);
        initView();

       /* FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {

                            return;
                        }


                        String token = task.getResult().getToken();
                        String msg = getString(R.string.fcm_token, token);
                        System.out.println("aaaaaaa token  "+token);
                        System.out.println("aaaaaaa msg  "+msg);

                    }
                });
*/
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datepicker(0);
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((fromyear.equals("")) || (frommonth.equals("")) || (fromday.equals(""))){
                    Toast.makeText(mContext, "please select startdate", Toast.LENGTH_SHORT).show();
                }else {
                    datepicker(1);
                }
            }
        });

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentitem=orders_view_pager.getCurrentItem();
                AddTabsToFragments(currentitem);
            }
        });
        return root;
    }

    public void getDate(){
        Calendar calander = Calendar.getInstance();
        int mday = calander.get(Calendar.DAY_OF_MONTH);
        int cMonth = calander.get(Calendar.MONTH) + 1;
        year =""+ calander.get(Calendar.YEAR);
        if ((cMonth) <= 9) {
            month = 0 + "" + (cMonth);
        } else {
            month = "" + (cMonth);
        }  if (mday <= 9) {
            day = 0 + "" + mday;
        } else {
            day = "" + mday;
        }
        start_date.setText(day+"-"+month+"-"+year);
        end_date.setText(day+"-"+month+"-"+year);
        startdate=year+"-"+month+"-"+day;
        enddate=year+"-"+month+"-"+day;

        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calander.getTime());
      /* int cHour = calander.get(Calendar.Ho);
        int cMinute = calander.get(Calendar.MINUTE);
        int cSecond = calander.get(Calendar.SECOND);
*/
    }

    public void datepicker(final int i){
        final Calendar c;
        final int mYear;
        final int mMonth;
        final int mDay;
        if (i==0){
            c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
          //  System.out.println("aaaaaa currentdate  "+c.get(Calendar.DAY_OF_MONTH)+" "+c.get(Calendar.MONTH)+"  "+c.get(Calendar.YEAR));
        }else {
            c = Calendar.getInstance();
            c.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
        }


        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (i==0){
                    if ((monthOfYear+1) <= 9) {
                        frommonth = 0 + "" + (monthOfYear+1);
                    } else {
                        frommonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        fromday = 0 + "" + dayOfMonth;
                    } else {
                        fromday = "" + dayOfMonth;
                    }
                    fromyear=""+year;
                    startdate=fromyear+"-"+frommonth+"-"+fromday;
                 //   System.out.println("aaaaaaaa  startdate   "+startdate);
                    //start_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    start_date.setText(""+startdate);
                }else {
                    if ((monthOfYear+1) <= 9) {
                        tomonth = 0 + "" + (monthOfYear+1);
                    } else {
                        tomonth = "" + (monthOfYear+1);
                    }  if (dayOfMonth <= 9) {
                        today = 0 + "" + dayOfMonth;
                    } else {
                        today = "" + dayOfMonth;
                    }
                    toyear=""+year;
                    enddate=toyear+"-"+tomonth+"-"+today;
                 //   System.out.println("aaaaaaaa  enddate   "+enddate);

                   // end_date.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    end_date.setText(""+enddate);
                }
            }
        }, mYear, mMonth, mDay);
        if (i==0){
            Calendar c1=Calendar.getInstance();
             c1.set((mYear),mMonth,mDay);
            datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());
          //  end_date.setText("Choose Date");
        }else {
            Calendar c1=Calendar.getInstance();
            Calendar c2=Calendar.getInstance();
           int mYear1 = c2.get(Calendar.YEAR);
            int mMonth1 = c2.get(Calendar.MONTH);
            int mDay1 = c2.get(Calendar.DAY_OF_MONTH);

            c2.set((mYear1),mMonth1,mDay1);
            c1.set(Integer.parseInt(fromyear),(Integer.parseInt(frommonth)-1),Integer.parseInt(fromday));
            datePickerDialog.getDatePicker().setMinDate(c1.getTimeInMillis());
            datePickerDialog.getDatePicker().setMaxDate(c2.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    private void updateLabel(TextView editText) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
       // editText.setText(sdf.format(myCalendar.getTime()));
    }

    private void initView() {
        mContext = getActivity();
        tabLayout = root.findViewById(R.id.tabLayout);
        orders_view_pager = root.findViewById(R.id.orders_view_pager);
        orders_view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (servicesAdapter != null)
                    servicesAdapter.selectedIndex(position);
            }
        });

        start_date = root.findViewById(R.id.start_date);
        end_date = root.findViewById(R.id.end_date);
        get = root.findViewById(R.id.apply);
        getDate();
        AddTabsToFragments(0);
        tabLayout.setupWithViewPager(orders_view_pager);
        preferenceManager=new PreferenceManager(mContext);
    }

    @Override
    public void onResume() {
        super.onResume();
      /*  getDate();

        AddTabsToFragments(0);
        tabLayout.setupWithViewPager(orders_view_pager);*/
    }


    private void AddTabsToFragments(int currentitem) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        String start_date_str = start_date.getText().toString().trim();
        String end_date_str = end_date.getText().toString().trim();
     //   System.out.println("aaaaaaa startdate "+startdate+" enddate  "+enddate);
      //  System.out.println("aaaaaaa start_date_str "+start_date_str+" start_date_str  "+end_date_str);
       // start_date_str = "2021-04-01";
       // end_date_str = "2021-04-01";
        if(orders_view_pager != null) {
          //  System.out.println("aaaaaaaaaa check4444");
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"0"), mContext.getString(R.string.all_orders));
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"3,11"),mContext.getString(R.string.ongoing_orders) );
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"4,12"), mContext.getString(R.string.out_for_delivery));
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"16"),mContext.getString(R.string.rejected_orders) );
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"17"), mContext.getString(R.string.cancelled_orders));
            viewPagerAdapter.addFrag(new OrderHistoryDataFragment(this,ORDERHISTORY, startdate, enddate,"19"), "Delivery boy rejected");
            orders_view_pager.setAdapter(viewPagerAdapter);
            orders_view_pager.setCurrentItem(currentitem);


        }else {
            orders_view_pager.setCurrentItem(0);
            System.out.println("aaaaaaaaaa check11111");
        }
        if(tabLayout != null){
         //   System.out.println("aaaaaaaaaa check333333");
            tabLayout.setupWithViewPager(orders_view_pager);
        }else {
          //  System.out.println("aaaaaaaaaa check22222");
        }


    }


    @Override
    public void sendDataMethod(boolean check) {
        System.out.println("aaaaaaaaaa check status changes "+check);
        //if (check){
           /* getDate();
            AddTabsToFragments(0);
            tabLayout.setupWithViewPager(orders_view_pager);*/
      //  }

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fm/*, int behavior*/) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void setViewHolder(ServicesAdapter servicesAdapter) {
        this.servicesAdapter=servicesAdapter;
    }


    public void doAction(String id) {
        if(orders_view_pager==null)
            return;
        switch (id) {
            case "2001":
                orders_view_pager.setCurrentItem(0);
                break;
            case "2002":
                orders_view_pager.setCurrentItem(1);
                break;
            case "2003":
                orders_view_pager.setCurrentItem(2);
                break;
            case "2004":
                orders_view_pager.setCurrentItem(3);
                break;
            case "2005":
                orders_view_pager.setCurrentItem(4);
                break;
            case "2006":
                orders_view_pager.setCurrentItem(5);
                break;
        }
    }


    public void onCalendarAction(FragmentManager supportFragmentManager) {
        MyDatePickerDialog dialog=new MyDatePickerDialog();
        //dialog.showDialog(supportFragmentManager,this,getActivityContext().getString(R.string.date_range));
        dialog.showStartAndEndDatePickers(mContext,this);
    }

    @Override
    public Context getActivityContext() {
        return mContext;
    }

    @Override
    public void onDateSelected(String startDate, String endDate) {
        this.startdate=startDate;
        this.enddate=endDate;
        int currentitem=orders_view_pager.getCurrentItem();
        AddTabsToFragments(currentitem);
    }
}