package com.nextclick.crm.orders.model;

public class OrderStatus {
    String id,delivery_mode_id,status,serial_number;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDelivery_mode_id() {
        return delivery_mode_id;
    }

    public void setDelivery_mode_id(String delivery_mode_id) {
        this.delivery_mode_id = delivery_mode_id;
    }

    public String getStatus() {
        int _id = Integer.parseInt(id);
        if (_id == 1 || _id == 9)
            return "Order is Placed";
        else if (_id == 3 || _id == 11)
            return "Order has been Preparing";
        else if (_id == 4)
            return "Ready to Pick Up";
        else if (_id == 5)
            return "Picked Up";
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    String deliveryPatnerStatus;
    public void setDeliveryPatnerStatus(String deliveryPatnerStatus) {
        this.deliveryPatnerStatus=deliveryPatnerStatus;
    }
    public String getDeliveryPatnerStatus() {
        return deliveryPatnerStatus;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }
}


