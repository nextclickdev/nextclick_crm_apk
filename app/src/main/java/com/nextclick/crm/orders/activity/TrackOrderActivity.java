package com.nextclick.crm.orders.activity;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Common.Models.AssignedUserAddress;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.DirectionListener;
import com.nextclick.crm.Helpers.UIHelpers.DirectionObject;
import com.nextclick.crm.Helpers.UIHelpers.LocationDetailUtil;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.orders.model.LocationObject;
import com.nextclick.crm.orders.model.OrderDetailsmodel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.nextclick.crm.Config.Config.ORDERDETAILS;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;

public class
TrackOrderActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    //OrderHistoryModel orderHistoryModel;
    private Context mContext;
    private CustomDialog mCustomDialog;
    private PreferenceManager preferenceManager;
    Marker prevMarker = null, homeMarker;
    String orderId;

    private boolean canShowMapView;
    AssignedUserAddress vendorAddress;
    private String mapType;

    Polyline mPolyline;
    String delivery_partner_user_id = "", job_id;
    private OrderDetailsmodel orderDetailsmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_order);


        orderId = getIntent().getStringExtra("order_id");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mContext=TrackOrderActivity.this;
        preferenceManager=new PreferenceManager(mContext);
        mCustomDialog = new CustomDialog(mContext);

        //orderHistoryModel= (OrderHistoryModel) getIntent().getSerializableExtra("orderdetails");
        getOrders();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationTimer();
    }

    Timer repeatLocationTask;
    private void startLocationTimer() {
        cancelLocationTimer();

        if (delivery_partner_user_id != null && !delivery_partner_user_id.isEmpty()) {
            repeatLocationTask = new Timer();
            int repeatInterval = 15000;//60000; // 1 min
            repeatLocationTask.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    getDeliveryBoyCurrentLocation();
                }
            }, 0, repeatInterval);
        }
    }
    private void cancelLocationTimer() {
        if (repeatLocationTask != null)
            repeatLocationTask.cancel();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (canShowMapView) {
            if (vendorAddress != null)
                showVendorLocation(vendorAddress.getLocation());
        }
    }
    private void showtracking(LocationObject restLocationObject) {
        canShowMapView = false;
        if (homeMarker ==null ||  restLocationObject == null
                || restLocationObject.getlongitude() == null || restLocationObject.getlongitude().equals("null")) {
            return;
        }
        LatLng restLatLong = new LatLng(Double.valueOf(restLocationObject.getLatitude()), Double.valueOf(restLocationObject.getlongitude()));

        if (prevMarker != null)
            prevMarker.setVisible(false);
        prevMarker = addMarkerObject(restLatLong, "Delivery Patner",R.drawable.motor_pin);//vendorAddress.getName()
        prevMarker.showInfoWindow();

        LatLng custLatLong = new LatLng(Double.valueOf(vendorAddress.getLocation().getLatitude()), Double.valueOf(vendorAddress.getLocation().getlongitude()));


        DirectionListener shopDistanceListener = new DirectionListener() {
            @Override
            public void onDirectionResult(DirectionObject result) {
                if (result != null) {
                    if (result.getLineOptions() != null) {
                        if (mPolyline != null) {
                            mPolyline.remove();
                        }
                        mPolyline = mMap.addPolyline(result.getLineOptions());
                        System.out.println("aaaaaaaaaa radius  "+result.getTravelDistance());
                        ZoomToPoints(custLatLong, restLatLong);

                        String[] separated = result.getTravelDistance().split(" ");

                        if (Double.parseDouble(separated[0])<=2){
                            //   addCircle(custLatLong,Double.parseDouble(separated[0]));
                        }
                    }
                }
            }
        };
        LocationDetailUtil.getTravelDistanceFromCurrentLocation(this, restLatLong, shopDistanceListener,
                custLatLong);
    }


    private void showVendorLocation(LocationObject custLocationObject) {
        canShowMapView = false;
        if (mMap ==null ||  custLocationObject == null
                || custLocationObject.getlongitude() == null || custLocationObject.getlongitude().equals("null")) {
            return;
        }
        LatLng custLatLong = new LatLng(Double.valueOf(custLocationObject.getLatitude()), Double.valueOf(custLocationObject.getlongitude()));

        if (homeMarker == null)
            homeMarker = addMarkerObject(custLatLong, vendorAddress.getName(),
                    R.drawable.ic_baseline_location_on_24);
        CameraPosition position = new CameraPosition.Builder()
                .target(custLatLong)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
    }
    private void ZoomToPoints(LatLng custLatLong, LatLng restLatLong) {
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(custLatLong);
            builder.include(restLatLong);
            LatLngBounds bounds = builder.build();

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                public void onCancel() {
                }

                public void onFinish() {
                    CameraUpdate zout = CameraUpdateFactory.zoomBy(-1.0f);//-3.0f
                    mMap.animateCamera(zout);
                }
            });
        } catch (Exception ex) {
            Log.e("failed in map", ex.getMessage());
        }
    }

    private Marker addMarkerObject(LatLng latLng, String Address, int id) {

        Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(Address)
                .icon(BitmapFromVector(getApplicationContext(), id)));
        /*CameraPosition position = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15.0f)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));*/
        return marker;
    }
    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);
        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    private void getDeliveryBoyCurrentLocation() {
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("delivery_partner_user_id", delivery_partner_user_id);
        uploadMap.put("job_id", job_id);

        JSONObject json = new JSONObject(uploadMap);
        System.out.println("GET_DELIVERY_BOY_LOCATION request" + json);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.GET_DELIVERY_BOY_LOCATION,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "GET_DELIVERY_BOY_LOCATION: " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                boolean finishedJob = false;
                                int _status = 0;
                                if (dataobj.has("job")) {
                                    JSONObject job = dataobj.getJSONObject("job");

                                    _status = job.getInt("status");

                                }
                                if (!finishedJob && dataobj.has("current_location")) {
                                    JSONObject locationObject = dataobj.getJSONObject("current_location");

                                    LocationObject vendorLocation = new LocationObject();
                                    vendorLocation.setLocationID(locationObject.getString("id"));
                                    vendorLocation.setLatitude(locationObject.getString("latitude"));
                                    vendorLocation.setLongitude(locationObject.getString("longitude"));
                                    vendorLocation.setAddress(locationObject.getString("address"));
                                    showtracking(vendorLocation);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private Double shoplatitude, shoplongitude;
    public void gotoshop() {
        if (shoplongitude != 0.0 && shoplongitude != 0.0) {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + shoplatitude +
                    "," + shoplongitude + "&mode=l");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    private AssignedUserAddress getAssignedUserAddress(JSONObject shippingobj) {
        AssignedUserAddress assignedUserAddress = new AssignedUserAddress();
        try {
            assignedUserAddress.setId(shippingobj.getString("id"));
            if (shippingobj.has("phone"))
                assignedUserAddress.setPhone(shippingobj.getString("phone"));
            else
                assignedUserAddress.setPhone("");

            if (shippingobj.has("email"))
                assignedUserAddress.setEmail(shippingobj.getString("email"));
            assignedUserAddress.setName(shippingobj.getString("name"));
            if (shippingobj.has("landmark"))
                assignedUserAddress.setLandmark(shippingobj.getString("landmark"));
            if (shippingobj.has("address"))
                assignedUserAddress.setAddress(shippingobj.getString("address"));
            assignedUserAddress.setLocation_id(shippingobj.getString("location_id"));
            if (shippingobj.has("vendor_user_id"))
                assignedUserAddress.setVendorUserID(shippingobj.getString("vendor_user_id"));
            if (shippingobj.has("unique_id"))
                assignedUserAddress.setUniqueID(shippingobj.getString("unique_id"));


            if (shippingobj.has("cover_image"))
                assignedUserAddress.setCoverImage(shippingobj.getString("cover_image"));

            if (shippingobj.has("location")) {
                JSONObject locationObject = shippingobj.getJSONObject("location");

                LocationObject vendorLocation = new LocationObject();
                vendorLocation.setLocationID(locationObject.getString("id"));
                vendorLocation.setLatitude(locationObject.getString("latitude"));
                vendorLocation.setLongitude(locationObject.getString("longitude"));
                //vendorLocation.setAddress(locationObject.getString("address"));
                try {
                    shoplatitude = Double.parseDouble(locationObject.getString("latitude"));
                    shoplongitude = Double.parseDouble(locationObject.getString("longitude"));
                } catch (NumberFormatException e) {

                }

                if (assignedUserAddress.getAddress() == null || assignedUserAddress.getAddress().equals("null")
                        || assignedUserAddress.getAddress().equals(null))
                    assignedUserAddress.setAddress(locationObject.getString("address"));

                vendorLocation.setAddress(assignedUserAddress.getAddress());

                assignedUserAddress.setLocation(vendorLocation);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return assignedUserAddress;
    }


    Integer CurrentStatus = -1;
    public void getOrders() {

        mCustomDialog.show();

        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("order_id", orderId);
        JSONObject json = new JSONObject(uploadMap);
        System.out.println("aaaaaaaaa request" + json);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ORDERDETAILS,
                new Response.Listener<String>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(String response) {
                        CurrentStatus = -1;
                        mCustomDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("VolleyResponse", "order_details: " + response);
                            System.out.println("aaaaaaaa jsonobject  " + jsonObject);
                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                String message = jsonObject.getString("message");
                                JSONObject dataobj = jsonObject.getJSONObject("data");

                                orderDetailsmodel = new OrderDetailsmodel();
                                vendorAddress = null;

                                try {
                                    if (dataobj.has("vendor")) {
                                        //vendor accpted the order
                                        JSONObject vendorAddressObj = dataobj.getJSONObject("vendor");
                                        vendorAddress = getAssignedUserAddress(vendorAddressObj);
                                    }

                                } catch (JSONException e1) {
                                }

                                try {
                                    Object aObj = null;
                                    if (dataobj.has("delivery_job"))
                                        aObj = dataobj.get("delivery_job");
                                    if (aObj instanceof JSONObject) {
                                        JSONObject delivery_job = (JSONObject) aObj;
                                        job_id = (delivery_job.getString("id"));
                                        if (delivery_job.has("delivery_boy")) {
                                            JSONObject delivery_boy = delivery_job.getJSONObject("delivery_boy");
                                            delivery_partner_user_id = (delivery_boy.getString("id"));
                                        }
                                    }
                                } catch (JSONException e1) {
                                }

                                startLocationTimer();

                                if (mMap == null) {
                                    //map is not initialised yet, so based on this flag after map is loaded then vendor details can shown on map
                                    canShowMapView = true;
                                } else if(vendorAddress!=null)
                                    showVendorLocation(vendorAddress.getLocation());
                            }
                        } catch (JSONException ex) {
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mCustomDialog.dismiss();
                Toast.makeText(mContext, "" + error.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("aaaaaaaaaa  error  " + error.getMessage());
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + preferenceManager.getString(USER_TOKEN));

                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return json.toString() == null ? null : json.toString().getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}