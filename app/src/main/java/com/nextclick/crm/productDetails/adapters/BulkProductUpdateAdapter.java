package com.nextclick.crm.productDetails.adapters;

import static com.nextclick.crm.Config.Config.BRANDS;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.productDetails.activities.BulkProductUpdateActivity;
import com.nextclick.crm.productDetails.model.BulkProducts;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BulkProductUpdateAdapter extends RecyclerView.Adapter<BulkProductUpdateAdapter.ViewHolder> {


    List<BulkProducts> data;
    Context mContext;

    public BulkProductUpdateAdapter(Context mContext, List<BulkProducts> itemPojos) {
        this.mContext = mContext;
        this.data = itemPojos;
    }


    @NonNull
    @Override
    public BulkProductUpdateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.product_bulk_update, parent, false);
        return new BulkProductUpdateAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BulkProducts bulkProducts =data.get(position);

        holder.product_name.setText(bulkProducts.getName());
        holder.product_desc.setText(Html.fromHtml(bulkProducts.getDescription()));
        /*try{
            holder.item_type_spinner.setSelection(bulkProducts.getItemSelection());
            holder.layout_itemtype.setVisibility(View.VISIBLE);
        }catch (Exception e){
            holder.layout_itemtype.setVisibility(View.GONE);
        }*/
        holder.productUpdateAdapter.setrefresh(bulkProducts.getProductList(),1);


        if(bulkProducts.getProductImages()!=null && bulkProducts.getProductImages().size()>0)
        {
            Picasso.get()
                    .load(bulkProducts.getProductImages().get(0).getImage())
                    .error(R.drawable.ic_default_place_holder)
                    .placeholder(R.drawable.ic_default_place_holder)
                    .into(holder.upload_image);
        }

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void RefreshData(ArrayList<BulkProducts> productaddlist) {
       this.data=productaddlist;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private LinearLayout layout_itemtype;
        TextView product_name,product_desc;
        private Spinner category_spinner,menu_spinner,brands_spinner;
        AutoCompleteTextView item_type_spinner;
        private RecyclerView recycle_views;
        ArrayList<ProductAdd> productaddlist;
        private final int REQUEST_CAMERA = 0;
        private final int SELECT_FILE = 1;
        private String userChoosenTask,product_id,previousActivity;
        private String product_name_str;
        private String product_desc_str;
        private final String base64image="";
        private String token;
        private String sub_cat_id = null;
        private String menu_id = null;
        private final String brand_id=null;
        private String product_price_str;
        private String product_quantity_str;
        private String product_discount_str;
        private String product_option_str;
        private ArrayList<String> categoriesList;
        private ArrayList<String> categoryIDList;
        private final ArrayList<String> taxCategoryList =null;
        private final ArrayList<String> taxCategoryIDList=null;
        private ArrayList<String> catIDList;
        private ArrayList<String> menusList = new ArrayList<>();
        private ArrayList<String> menuIDList = new ArrayList<>();
        private ArrayList<String> brandlist = new ArrayList<>();
        private ArrayList<String> brandIDList = new ArrayList<>();
        private final int status_type = 1;
        private final int productAvailability = 1;
        private boolean isImageSelected;
        private int currentposition1, sectionid;
        private VaraintUpdateAdapter productUpdateAdapter;
        private ArrayList<ImagesModel> imageslist;
        private ArrayList<String> itemtypelist = new ArrayList<>();
        ImageView upload_image;

        @SuppressLint("ResourceAsColor")
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            init(itemView);
            prepareSpinnerDetails();
        }

        public void init(View itemView){
            upload_image=itemView.findViewById(R.id.upload_image);
            product_name=itemView.findViewById(R.id.product_name);
            product_desc=itemView.findViewById(R.id.product_desc);
            category_spinner=itemView.findViewById(R.id.category_spinner);
            menu_spinner=itemView.findViewById(R.id.menu_spinner);
            recycle_views=itemView.findViewById(R.id.recycle_views);
            brands_spinner=itemView.findViewById(R.id.brands_spinner);
            layout_itemtype=itemView.findViewById(R.id.layout_itemtype);
            item_type_spinner=itemView.findViewById(R.id.item_type_spinner);

            product_name.setEnabled(false);
            product_name.setClickable(false);
            product_desc.setEnabled(false);
            product_desc.setClickable(false);


            recycle_views.setLayoutManager(new GridLayoutManager(mContext,1));

            productaddlist=new ArrayList<>();

            //currentposition=getIntent().getIntExtra("currentposition",0);
            productUpdateAdapter=new VaraintUpdateAdapter(mContext,productaddlist);
            recycle_views.setAdapter(productUpdateAdapter);


            itemtypelist=new ArrayList<>();
            itemtypelist.add("Veg");
            itemtypelist.add("Non Veg");
            itemtypelist.add("Others");
            //ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, itemtypelist);
            //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_spinner_item, itemtypelist);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            item_type_spinner.setAdapter(adapter);
           // item_type_spinner.setText(itemtypelist.get(2));


            item_type_spinner.setKeyListener(null);
            item_type_spinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //item_type_spinner.setText("");
                    item_type_spinner.showDropDown();
                }
            });
        }

        @Override
        public void onClick(View v) {

        }

        private void prepareSpinnerDetails() {

            category_spinner.setClickable(false);
            category_spinner.setEnabled(false);
            menu_spinner.setClickable(false);
            menu_spinner.setEnabled(false);
            brands_spinner.setClickable(false);
            brands_spinner.setEnabled(false);

            category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        sub_cat_id = categoryIDList.get(position - 1);
                        LoadingDialog.loadDialog(mContext);
                        getMenus(sub_cat_id);
                        LoadingDialog.dialog.dismiss();
                    } else {
                        sub_cat_id = null;
                        menu_id = null;
                        menuIDList.clear();
                        menusList.clear();
                        menu_spinner.setAdapter(null);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        menu_id = menuIDList.get(position - 1);
                        if(catIDList.size()> position-1)
                            sub_cat_id = catIDList.get(position - 1);
                        // LoadingDialog.loadDialog(mContext);
                        //  getBrands(sub_cat_id);
                        //  LoadingDialog.dialog.dismiss();
                    } else {
                        menu_id = null;
                        sub_cat_id = null;
                        menu_id = null;
                        menuIDList.clear();
                        menusList.clear();
                        menu_spinner.setAdapter(null);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            brands_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* try{
                    if (!brands_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        brand_id = brandIDList.get(position - 1);
                    } else {
                        brand_id = null;
                        brands_spinner.setAdapter(null);
                    }
                }catch (ArrayIndexOutOfBoundsException e){

                }*/


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        private void getCategories() {

            Map<String, String> searchMap = new HashMap<>();
            searchMap.put("q", "");
            final String data = new JSONObject(searchMap).toString();

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("cat_res", response);

                            if (response != null) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            categoriesList = new ArrayList<>();
                                            categoryIDList = new ArrayList<>();
                                            catIDList = new ArrayList<>();
                                            categoriesList.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {

                                                JSONObject categoryObject = dataArray.getJSONObject(i);
                                                categoryIDList.add(categoryObject.getString("id"));
                                                categoriesList.add(categoryObject.getString("name"));
                                                catIDList.add(categoryObject.getString("cat_id"));

                                            }

                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                    android.R.layout.simple_spinner_item, categoriesList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            category_spinner.setAdapter(adapter);


                                        }
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(mContext, MAINTENANCE);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            UIMsgs.showToast(mContext, OOPS);
                            System.out.println("aaaaaaa  111 "+error.getMessage());
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN, token);
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }

      /*    private void getTaxes() {

            Map<String, String> searchMap = new HashMap<>();
            searchMap.put("q", "");
            final String data = new JSONObject(searchMap).toString();

            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_TAXES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("tax_res", response);
                            if (response != null) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        JSONArray dataArray = jsonObject.getJSONArray("data");
                                        if (dataArray.length() > 0) {
                                            taxCategoryList = new ArrayList<>();
                                            taxCategoryIDList = new ArrayList<>();
                                            taxCategoryList.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject categoryObject = dataArray.getJSONObject(i);
                                                taxCategoryIDList.add(categoryObject.getString("id"));
                                                taxCategoryList.add(categoryObject.getString("name"));
                                            }
                                            updateTaxListToProductList();
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(mContext, MAINTENANCE);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            UIMsgs.showToast(mContext, OOPS);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }

      private void updateTaxListToProductList() {
            if (taxCategoryList != null && productaddlist != null && productaddlist.size() > 0) {
                for (int i = 0; i < productaddlist.size(); i++) {
                    ProductAdd product = productaddlist.get(i);
                    product.setTaxesList(taxCategoryList,taxCategoryIDList);
                }
            }
        }*/

        private void getMenus(String sub_cat_id) {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("menu_res", response);

                            if (response != null) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                        if (dataArray.length() > 0) {
                                            menusList = new ArrayList<>();
                                            menuIDList = new ArrayList<>();
                                            menusList.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject categoryObject = dataArray.getJSONObject(i);
                                                menusList.add(categoryObject.getString("name"));
                                                menuIDList.add(categoryObject.getString("id"));
                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            menu_spinner.setAdapter(adapter);

                                            try {
                                                if (menu_id != null) {
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < menuIDList.size(); i++) {
                                                            if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                                menu_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(mContext, MAINTENANCE);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("aaaaaaa  444 "+error.getMessage());
                            UIMsgs.showToast(mContext, OOPS);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN, token);
                    return map;
                }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }
        private void getBrands(String sub_cat_id) {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, BRANDS + sub_cat_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("brands_res", response);

                            if (response != null) {
                                try {

                                    JSONObject jsonObject = new JSONObject(response);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        // JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                        JSONObject dataArray = jsonObject.getJSONObject("data").getJSONObject("menus");
                                        if (dataArray.length() > 0) {
                                            brandlist = new ArrayList<>();
                                            brandIDList = new ArrayList<>();
                                            brandlist.add("Select");
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject categoryObject = dataArray.getJSONObject(""+i);
                                                brandlist.add(categoryObject.getString("name"));
                                                brandIDList.add(categoryObject.getString("id"));
                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            brands_spinner.setAdapter(adapter);

                                            try {
                                                if (menu_id != null) {
                                                    try {
                                                        Thread thread = new Thread();
                                                        Thread.sleep(1000);
                                                        for (int i = 0; i < menuIDList.size(); i++) {
                                                            if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                                menu_spinner.setSelection((i + 1), true);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                UIMsgs.showToast(mContext, MAINTENANCE);
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("aaaaaaa  555 "+error.getMessage());
                            UIMsgs.showToast(mContext, OOPS);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN, token);
                    return map;
                }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);

        }
    }
}
