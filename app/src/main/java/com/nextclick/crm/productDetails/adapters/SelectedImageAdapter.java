package com.nextclick.crm.productDetails.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.imagesselect.FullImageActivity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class SelectedImageAdapter extends RecyclerView.Adapter<SelectedImageAdapter.ViewHolder>{

    public boolean collapseCancelButton = false;
    Context context;
    ArrayList<ImagesModel> stringArrayList;

    public SelectedImageAdapter(Context context, ArrayList<ImagesModel> stringArrayList) {
        this.context = context;
        this.stringArrayList = stringArrayList;
    }

    @Override
    public  ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.selected_multiple_images, viewGroup, false);
        return new ViewHolder(view);
    }
    @Override
    public  void onBindViewHolder(ViewHolder holder, final int position) {

        Glide.with(context)
                .load(stringArrayList.get(position).getImage())
                .placeholder(R.drawable.product_add_image)
                .centerCrop()
                .into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stringArrayList.get(position).getImage() == null)
                    ((ProductAddOrUpdate) context).uploadNewImage();
                else
                    context.startActivity(new Intent(context, FullImageActivity.class).putExtra("image", stringArrayList.get(position).getImage()));
            }
        });
        if(collapseCancelButton || stringArrayList.get(position).getImage() == null) {
            holder.layout_Cancel_Option.setVisibility(View.GONE);
        }
        else {
            holder.layout_Cancel_Option.setVisibility(View.VISIBLE);
            holder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ProductAddOrUpdate) context).deleteimage(stringArrayList.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    public void refresh(ArrayList<ImagesModel> imagelist) {
        this.stringArrayList=imagelist;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image,img_delete;
        LinearLayout layout_Cancel_Option;
        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            img_delete = itemView.findViewById(R.id.img_delete);
            layout_Cancel_Option = itemView.findViewById(R.id.layout_Cancel_Option);
        }
    }
}