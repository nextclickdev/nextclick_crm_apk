package com.nextclick.crm.productDetails.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.fragments.ApprovedFragment;
import com.nextclick.crm.productDetails.fragments.CatologProductsFragment;
import com.nextclick.crm.productDetails.fragments.MyProductsFragment;
import com.nextclick.crm.productDetails.fragments.PendingFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    MainProductpage mainProductpage;
    String catalogue_count,inventory_instock_count,inventory_outofstock_count,pendig_count,
            approved_count;


    public ViewPagerAdapter(
            @NonNull FragmentManager fm, MainProductpage mainProductpage)
    {
        super(fm);
        this.mainProductpage=mainProductpage;
    }

    public ViewPagerAdapter(FragmentManager supportFragmentManager, MainProductpage mainProductpage, String catalogue_count, String inventory_instock_count, String inventory_outofstock_count, String pendig_count, String approved_count) {
        super(supportFragmentManager);
        this.catalogue_count=catalogue_count;
        this.inventory_instock_count=inventory_instock_count;
        this.inventory_outofstock_count=inventory_outofstock_count;
        this.pendig_count=pendig_count;
        this.approved_count=approved_count;
    }

    @NonNull
    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = null;
        if (position == 0)
            fragment = new MyProductsFragment(mainProductpage,inventory_instock_count,inventory_outofstock_count);
        else if (position == 1)
            fragment = new CatologProductsFragment(mainProductpage);
        else if (position == 2)
            fragment = new ApprovedFragment(mainProductpage);
        else if (position == 3)
            fragment = new PendingFragment(mainProductpage);

        return fragment;
    }

    @Override
    public int getCount()
    {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        String title = null;
        if (position == 0)
            title = "My Inventory";
        else if (position == 1)
            title = "Catalogue("+catalogue_count+")";
        else if (position == 2)
            title = "Approved("+approved_count+")";
        else if (position == 3)
            title = "Pending("+pendig_count+")";
        System.out.println("aaaaaaaaaaa title  "+title);
        return title;
    }
}
