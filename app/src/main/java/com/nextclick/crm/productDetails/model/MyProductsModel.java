package com.nextclick.crm.productDetails.model;

import java.util.ArrayList;

public class MyProductsModel {
    String id,sub_cat_id,menu_id,brand_id,product_code,name,desc,quantity,price,discount,item_type,sounds_like,created_user_id,
            updated_user_id,created_at,updated_at,deleted_at,availability,status;

    ArrayList<ItemImages> imageslist;
    ArrayList<VendorVarients> varientlist;
    Menusmodel menusmodel;
    Menusmodel subcategory;
    Menusmodel brandmodel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getSounds_like() {
        return sounds_like;
    }

    public void setSounds_like(String sounds_like) {
        this.sounds_like = sounds_like;
    }

    public String getCreated_user_id() {
        return created_user_id;
    }

    public void setCreated_user_id(String created_user_id) {
        this.created_user_id = created_user_id;
    }

    public String getUpdated_user_id() {
        return updated_user_id;
    }

    public void setUpdated_user_id(String updated_user_id) {
        this.updated_user_id = updated_user_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ItemImages> getImageslist() {
        return imageslist;
    }

    public void setImageslist(ArrayList<ItemImages> imageslist) {
        this.imageslist = imageslist;
    }

    public ArrayList<VendorVarients> getVarientlist() {
        return varientlist;
    }

    public void setVarientlist(ArrayList<VendorVarients> varientlist) {
        this.varientlist = varientlist;
    }

    public Menusmodel getMenusmodel() {
        return menusmodel;
    }

    public void setMenusmodel(Menusmodel menusmodel) {
        this.menusmodel = menusmodel;
    }

    public Menusmodel getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Menusmodel subcategory) {
        this.subcategory = subcategory;
    }

    public Menusmodel getBrandmodel() {
        return brandmodel;
    }

    public void setBrandmodel(Menusmodel brandmodel) {
        this.brandmodel = brandmodel;
    }
}
