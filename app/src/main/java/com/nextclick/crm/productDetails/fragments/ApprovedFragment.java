package com.nextclick.crm.productDetails.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.dialogs.CategoryMenuFilter;
import com.nextclick.crm.interfaces.CategoryMenuSelection;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.adapters.ProductsAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ALLPRODUCTLIST;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.APPROVEDSTATUS;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class ApprovedFragment extends Fragment implements View.OnClickListener , CategoryMenuSelection {

    View view;
    private RecyclerView recycle_catelogue;
    private TextView tvError;
    private EditText etSearch;
    private ImageView ivSearch;
    private Button apply_;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private Spinner filetr_menu_spinner, filter_category_spinner;
    private ProductsAdapter productsAdapter;
    private Context mContext;
    private boolean isScrolled;
    private CustomDialog mCustomDialog;
    private boolean isScrolling = false;
    private ArrayList<ProductModel> productModelsList = new ArrayList<>();
    private LinearLayoutManager layoutManager;
    private PreferenceManager preferenceManager;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int count = 0;
    private int totalItems;
    private int page_no = 1;
    private int currentItems;
    private int scrollOutItems;

    private String token = "";
    private String menu_id = "";
    private String sub_cat_id = "";
    private String enteredText = "";

    MainProductpage mainProductpage;
    private int previousOffset;
    boolean isViewCrated;
    private LinearLayout layout_stock;
    TextView tv_add_product;
    public ApprovedFragment(MainProductpage mainProductpage) {
        // Required empty public constructor
        this.mainProductpage=mainProductpage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_catolog_products, container, false);
        init();
        prepareSpinnerItemsClicks();


        if (!isViewCrated || Utility.RefreshApprovedProducts || productModelsList.size() == 0) {
            Utility.RefreshApprovedProducts = false;
            page_no=1;
            getProducts(1);
        }
        getCategories();
        adapterSetter();
        isViewCrated = true;

        return view;
    }

    public void init(){
        mContext = getActivity();
        recycle_catelogue=view.findViewById(R.id.recycle_catelogue);
        tvError=view.findViewById(R.id.tvError);
        apply_ = view.findViewById(R.id.apply_p);
        etSearch = view.findViewById(R.id.etSearch);
        ivSearch = view.findViewById(R.id.ivSearch);
        ImageView ivFilter = view.findViewById(R.id.ivFilter);
        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCategoryMenuFilter();
            }
        });
        filetr_menu_spinner = view.findViewById(R.id.filetr_menu_spinner);
        filter_category_spinner = view.findViewById(R.id.filter_category_spinner);
        layout_stock = view.findViewById(R.id.layout_stock);
        layout_stock.setVisibility(View.GONE);

        tv_add_product = view.findViewById(R.id.tv_add_product);
        tv_add_product.setOnClickListener(this);
        mCustomDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        apply_.setOnClickListener(this);
        ivSearch.setOnClickListener(this);

        mSwipeRefreshLayout =view.findViewById(R.id.swipeToRefresh);
        // Configure the refreshing colors
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page_no=1;
                getProducts(1);
            }
        });

        recycle_catelogue.setLayoutManager(new GridLayoutManager(getContext(),1));
        NestedScrollView nestedSV = view.findViewById(R.id.idNestedSV);
        nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int dy, int oldScrollX, int oldScrollY) {
                // on scroll change we are checking when users scroll as bottom.
                if (dy == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                    // loadingPB.setVisibility(View.VISIBLE);
                    currentItems = layoutManager.getChildCount();
                    totalItems = layoutManager.getItemCount();
                    scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                    if ((currentItems + scrollOutItems == totalItems)) {
                        isScrolled = true;
                        isScrolling = false;
                        count++;
                        boolean isContinue = true;
                        if (dy >= previousOffset)
                            ++page_no;
                        else if (page_no > 1)//min range
                        {
                            //--page_no;
                            isContinue = false;
                        }
                        else {
                            isContinue = false;//comment this if you encounter any problem
                        }
                        previousOffset = dy;
                        if (isContinue)
                            getProducts(0);
                    }
                }
            }
        });
      /*  recycle_catelogue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolled = true;
                    isScrolling = false;
                    count++;
                    boolean isContinue = true;
                    if (dy > previousOffset)
                        ++page_no;
                    else if (page_no > 1)//min range
                    {
                        //--page_no;
                        isContinue = false;
                    }
                    else {
                        isContinue = false;//comment this if you encounter any problem
                    }
                    previousOffset = dy;
                    if (isContinue)
                        getProducts(0);
                }
            }
        });*/
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                enteredText = charSequence.toString();

               /* if (enteredText.isEmpty()) {
                    page_no = 1;
                    count = 0;
                    recycle_catelogue.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    adapterSetter();
                    getProducts(0);
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private void prepareSpinnerItemsClicks() {
        filter_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filter_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    menusList.clear();
                    menuIDList.clear();
                    filetr_menu_spinner.setAdapter(null);
                    getMenus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void adapterSetter() {
        productsAdapter = new ProductsAdapter(ApprovedFragment.this,mContext, productModelsList,2, mainProductpage);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        recycle_catelogue.setLayoutManager(layoutManager);
        recycle_catelogue.setAdapter(productsAdapter);
    }
    public void getProducts(int delete) {
        if (delete==1){
            productModelsList.clear();
        }
        mCustomDialog.show();
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put("page_no", page_no + "");
        dataMap.put("q", enteredText);
        dataMap.put("menu_id", menu_id);
        dataMap.put("shop_by_cat_id", sub_cat_id);
        dataMap.put("status", APPROVEDSTATUS);
        final String data = new JSONObject(dataMap).toString();
        System.out.println("aaaaaaaa  data approved "+data);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ALLPRODUCTLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("aaaaaaaaaaaa  dailog 11 "+response);
                        mCustomDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (response != null) {
                            Log.d("Product_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    try {
                                      //  JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        Object aObj = dataObject.get("result");
                                        if (!(aObj instanceof JSONArray) || aObj instanceof JSONObject) {
                                            if (page_no > 1)
                                                page_no--;
                                            System.out.println("getProducts responsed failed - ApprovedProducs");
                                            setViewVisibility();
                                            return;
                                        }
                                        JSONArray dataArray =(JSONArray) aObj;
                                        if (dataArray.length() > 0) {
                                            listIsFull();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject productObject = dataArray.getJSONObject(i);
                                                ProductModel productModel = new ProductModel();
                                                productModel.setId(productObject.getString("id"));
                                                productModel.setSub_cat_id(productObject.getString("sub_cat_id"));
                                                productModel.setMenu_id(productObject.getString("menu_id"));
                                                productModel.setProduct_code(productObject.getString("product_code"));
                                                productModel.setName(productObject.getString("name"));
                                                productModel.setDesc(Html.fromHtml(productObject.getString("desc")).toString());
                                                //  productModel.setQuantity(productObject.getInt("quantity"));
                                                //  productModel.setPrice(productObject.getInt("price"));
                                                //  productModel.setDiscount(productObject.getInt("discount"));
                                                productModel.setItem_type(productObject.getString("item_type"));
                                                productModel.setStatus(productObject.getInt("status"));
                                                System.out.println("aaaaaaaaaaaa Myinventory cat1 "+productObject.getString("myinventory"));
                                                productModel.setMyinventory(productObject.getString("myinventory"));
                                               // productModel.setProduct_image(productObject.getString("product_image"));
                                                ArrayList<ImagesModel> imageslist=new ArrayList<>();
                                                try {
                                                    JSONArray jsonArray=productObject.getJSONArray("item_images");
                                                    for (int k=0;k<jsonArray.length();k++){
                                                        JSONObject jsonObject1=jsonArray.getJSONObject(k);
                                                        ImagesModel imagesModel=new ImagesModel();
                                                        imagesModel.setId(jsonObject1.getString("id"));
                                                        imagesModel.setImage(jsonObject1.getString("image"));
                                                        imageslist.add(imagesModel);
                                                    }
                                                    productModel.setImagelist(imageslist);
                                                }catch (Exception e){

                                                }

                                                productModel.setAvailabilityStatus(productObject.getString("status"));
                                                try {
                                                    productModel.setCategoryName(productObject.getJSONObject("sub_category").getString("name"));
                                                    productModel.setMenu_name(productObject.getJSONObject("menu").getString("name"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                productModelsList.add(productModel);
                                                productsAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            if (!isScrolled) {
                                                listIsEmpty();
                                            }
                                        }
                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch 1 "+e.getMessage());
                                        e.printStackTrace();
                                        if (!isScrolled) {
                                            listIsEmpty();
                                        }
                                        if (count == 0) {
                                          //  UIMsgs.showToast(mContext, "Products Not Available");
                                        } else {
                                           // UIMsgs.showToast(mContext, "You've reached the end");
                                        }
                                    }
                                } else {
                                    if (!isScrolled) {
                                        listIsEmpty();
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 22  "+e.getMessage());
                                e.printStackTrace();
                                if (!isScrolled) {
                                    listIsEmpty();
                                }
                            }
                            finally {
                                setViewVisibility();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            if (!isScrolled) {
                                listIsEmpty();
                            }
                            setViewVisibility();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                        if (!isScrolled) {
                            listIsEmpty();
                        }
                        setViewVisibility();
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                System.out.println("aaaaaaaa token  "+token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void setViewVisibility() {
        if(productModelsList.size()>0)
        {
            tvError.setVisibility(View.GONE);
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            tvError.setVisibility(View.VISIBLE);
            mSwipeRefreshLayout.setVisibility(View.GONE);
        }
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
        recycle_catelogue.setVisibility(View.VISIBLE);
    }
    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        recycle_catelogue.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_add_product:
                Intent i=new Intent(mContext, ProductAddOrUpdate.class);
                i.putExtra("currentposition",2);
                startActivity(i);
                break;
            case R.id.apply_p:
                if (sub_cat_id.equalsIgnoreCase("select")||sub_cat_id.isEmpty()){
                    Toast.makeText(getContext(), "Please select Category", Toast.LENGTH_SHORT).show();
                }else if (menu_id.equalsIgnoreCase("")||menu_id.isEmpty()){
                    Toast.makeText(getContext(), "Please select menu", Toast.LENGTH_SHORT).show();
                }else {
                    page_no = 1;
                    count = 0;
                    recycle_catelogue.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    getProducts(1);
                    adapterSetter();
                }

                break;
            case R.id.ivSearch:
                page_no = 1;
                count = 0;
                if (enteredText.isEmpty()||enteredText.equalsIgnoreCase("")){
                    Toast.makeText(getContext(), "Please enter search text", Toast.LENGTH_SHORT).show();
                }else {
                    recycle_catelogue.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    adapterSetter();
                    productModelsList.clear();
                    getProducts(1);
                }

                break;
        }
    }
    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("menu_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filetr_menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("aaaaaa catch manu "+e.getMessage());
                                UIMsgs.showToast(mContext, "No menus available for the selected menu");
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error menu "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filter_category_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaa catch cat "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error cat "+error.getMessage());
                       // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }



    private void openCategoryMenuFilter() {
        new CategoryMenuFilter().showMenuCategoryFilter(mContext,this);
    }

    @Override
    public void onCategoryMenuSelection(String sub_cat_id, String menu_id) {
        this.sub_cat_id=sub_cat_id;
        this.menu_id=menu_id;
        page_no = 1;
        count = 0;
        previousOffset = 0;
        recycle_catelogue.setAdapter(null);
        productModelsList = new ArrayList<>();
        getProducts(1);
        adapterSetter();
    }
}