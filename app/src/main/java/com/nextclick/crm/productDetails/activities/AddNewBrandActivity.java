package com.nextclick.crm.productDetails.activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Adapters.MultiImagesAdapter;
import com.nextclick.crm.ShopNowModule.Fragments.HomeActivityFragments.MyProductsFragment;
import com.nextclick.crm.ShopNowModule.Models.MultiImageModel;
import com.nextclick.crm.Utilities.PreferenceManager;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static androidx.core.content.ContextCompat.checkSelfPermission;
import static com.nextclick.crm.Config.Config.BRANDS_CREATE;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class AddNewBrandActivity extends Activity {
    private EditText brand_name,brand_desc,catgeory_name;
    private ImageView brand_image,tv_addbannerimage;
    private Context mcontext;
    private String userChoosenTask,brand64image;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private static final int CAMERA_REQUEST = 8;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;
    Button submit;
    PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_brand);
        brand_name=findViewById(R.id.brand_name);
        brand_desc=findViewById(R.id.brand_desc);
        catgeory_name=findViewById(R.id.catgeory_name);
        brand_image=findViewById(R.id.brand_image);
        tv_addbannerimage=findViewById(R.id.tv_addbannerimage);
        submit=findViewById(R.id.submit);
        mcontext=AddNewBrandActivity.this;
        preferenceManager=new PreferenceManager(mcontext);
        catgeory_name.setEnabled(false);
        catgeory_name.setClickable(false);
        catgeory_name.setText(preferenceManager.getString("VendorCategoryname"));
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (brand_name.getText().toString().isEmpty()){
                    Toast.makeText(mcontext, "Please enter Brand name", Toast.LENGTH_SHORT).show();
                }else{
                    if (brand_desc.getText().toString().isEmpty()){
                        Toast.makeText(mcontext, "Please enter Brand description", Toast.LENGTH_SHORT).show();
                    }else{
                        if (brand64image.isEmpty()||brand64image.equalsIgnoreCase("")){
                            Toast.makeText(mcontext, "Please select Brand image", Toast.LENGTH_SHORT).show();
                        }else{
                            createbrand();
                        }
                    }
                }

            }
        });
        tv_addbannerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean selecting = selectImage();
                if (selecting) {

                }
            }
        });
    }
    private boolean selectImage() {
        final CharSequence[] items =
                {
                        "Choose from Library",
                        "Open Camera",
                        "Cancel"
                };

        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(mcontext);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }
    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

                brand_image.setImageBitmap(photo);
                Bitmap bitmap = ((BitmapDrawable) brand_image.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                //coverImageBase64 =basse64Converter(cover_image);

                byte[] byteArray = baos.toByteArray();
                brand64image =resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));
        }else if (requestCode == SELECT_FILE)
            onSelectFromGalleryResult(data);
    }
    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);

            brand_image.setImageBitmap(bm);

            Bitmap bitmap = ((BitmapDrawable) brand_image.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();
            brand64image = resizeBase64Image(Base64.encodeToString(byteArray, Base64.DEFAULT));

    }

    public void createbrand(){
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("name", brand_name.getText().toString().trim());
        uploadMap.put("desc", brand_desc.getText().toString().trim());
        uploadMap.put("image", brand64image);

       String data = new JSONObject(uploadMap).toString();
        System.out.println("aaaaaaaaaaa request brand  "+ data);
        LoadingDialog.loadDialog(mcontext);
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BRANDS_CREATE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaa brands response "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mcontext, "Created Successfully");

                                } else {
                                    UIMsgs.showToast(mcontext, "Something went wrong");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mcontext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mcontext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}