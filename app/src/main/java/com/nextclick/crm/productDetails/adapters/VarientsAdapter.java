package com.nextclick.crm.productDetails.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.productDetails.fragments.BottomVarientsFragment;
import com.nextclick.crm.productDetails.fragments.MyProductsFragment;
import com.nextclick.crm.productDetails.model.VendorVarients;
import com.bumptech.glide.Glide;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.UPDATEVARIENT;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class VarientsAdapter extends RecyclerView.Adapter<VarientsAdapter.ViewHolder>  {

    ArrayList<VendorVarients> vendorVarientslist;
    Context context;
    private final PreferenceManager preferenceManager;
    ProductModel productModel;
    MyProductsFragment myProductsFragment;
    BottomVarientsFragment bottomVarientsFragment;
    String stocktype;

    public VarientsAdapter(Context context, ArrayList<VendorVarients> vendorVarientslist,
                           ProductModel productModel, MyProductsFragment myProductsFragment,
                           BottomVarientsFragment bottomVarientsFragment,String stocktype) {
        this.context=context;
        this.vendorVarientslist=vendorVarientslist;
        this.productModel=productModel;
        this.myProductsFragment=myProductsFragment;
        this.bottomVarientsFragment=bottomVarientsFragment;
        this.stocktype=stocktype;
        preferenceManager=new PreferenceManager(context);
    }


    @NonNull
    @Override
    public VarientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.product_varientlayout, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        return new VarientsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VarientsAdapter.ViewHolder holder, int position) {

        VendorVarients productsModel = vendorVarientslist.get(position);

            try{
                String name =productsModel.getSection_item_name().substring(0, 1).toUpperCase() + productsModel.getSection_item_name().substring(1);
                holder.item_name.setText(name);
            }catch (IndexOutOfBoundsException e){
                System.out.println("aaaaaaaa   "+e.getMessage());
            }
        Glide.with(context)
                .load(productModel.getImagelist().get(0).getImage())
                .error(R.drawable.ic_default_place_holder)
                .placeholder(R.drawable.ic_default_place_holder)
                .into(holder.img_product);
            holder.item_price.setText(""+context.getResources().getString(R.string.Rs)+" "+productsModel.getPrice());
        holder.tv_quantity.setText(""+productsModel.getStock());

        holder.tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int stock=Integer.parseInt(holder.tv_quantity.getText().toString().trim());
                if (stock<=0){

                }else {
                    stock=stock-1;
                }
                holder.tv_quantity.setText(""+stock);
            }
        });
        holder.tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tv_quantity.setText(""+(Integer.parseInt(holder.tv_quantity.getText().toString())+1));
            }
        });
        holder.tv_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setsaveVarient(productsModel,holder.tv_quantity.getText().toString());
            }
        });

    }

    @Override
    public int getItemCount() {
        return vendorVarientslist.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {return position;}


    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView item_name;
        private final TextView tv_minus;
        private final TextView tv_plus;
        private final TextView tv_update;
        private final TextView item_price;
        private final EditText tv_quantity;
        private final ImageView img_product;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            item_name=itemView.findViewById(R.id.item_name);
            tv_quantity=itemView.findViewById(R.id.tv_quantity);
            tv_minus=itemView.findViewById(R.id.tv_minus);
            tv_plus=itemView.findViewById(R.id.tv_plus);
            tv_update=itemView.findViewById(R.id.tv_update);
            img_product=itemView.findViewById(R.id.img_product);
            item_price=itemView.findViewById(R.id.item_price);
        }
    }
    public void setsaveVarient(VendorVarients productsModel, String s) {
        String tax_id;
        try{
            if (productsModel.getTaxid().isEmpty()){
                tax_id="1";
            }else {
                tax_id=productsModel.getTaxid();
            }
        }catch (Exception e){
            tax_id="1";
        }

        System.out.println("aaaaaaaaaaa  update product");
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("variant_id", productsModel.getId());
        uploadMap.put("price", productsModel.getPrice());
        uploadMap.put("stock", "" + s);
        uploadMap.put("discount", productsModel.getDiscount());
        uploadMap.put("status", "" + productsModel.getStatus());
        uploadMap.put("weight", "" + productsModel.getWeight());

        uploadMap.put("tax_id", "" + tax_id);

        uploadMap.put("option_name",  productsModel.getSection_item_name());
        JSONObject json = new JSONObject(uploadMap);

        String data = json.toString();
        System.out.println("aaaaaaaa  data  " + data);
        createOrUpdateCategory(data,productsModel,s);
    }
    private void createOrUpdateCategory(final String data, VendorVarients productsModel,String stock) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATEVARIENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            //  Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  jsonobject  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    bottomVarientsFragment.dismiss();
                                    System.out.println("aaaaaaaaaaa  update product status ");
                                    Toast.makeText(context, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                  //  myProductsFragment.getProducts(1,""+stocktype);
                                    myProductsFragment.changeproduct(productsModel,stock,stocktype);
                                } else {
                                    UIMsgs.showToast(context, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                System.out.println("aaaaaaa  666 "+error.getMessage());
                UIMsgs.showToast(context, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                System.out.println("aaaaaa token  "+preferenceManager.getString(USER_TOKEN));
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
}
