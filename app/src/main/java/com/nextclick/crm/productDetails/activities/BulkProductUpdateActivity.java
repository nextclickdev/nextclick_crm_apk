package com.nextclick.crm.productDetails.activities;

import static com.nextclick.crm.Config.Config.ADDPRODUCTTOMYLIST;
import static com.nextclick.crm.Config.Config.BRANDS;
import static com.nextclick.crm.Config.Config.BULKUPDATEVARIENT;
import static com.nextclick.crm.Config.Config.PRODUCT_D;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Config.Config.UPDATEVARIENT;
import static com.nextclick.crm.Config.Config.VENDOR_VARIANTS_BULK;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.SupportActivity;
import com.nextclick.crm.productDetails.adapters.BulkProductUpdateAdapter;
import com.nextclick.crm.productDetails.adapters.ProductUpdateAdapter;
import com.nextclick.crm.productDetails.adapters.SelectedImageAdapter;
import com.nextclick.crm.productDetails.fragments.ApprovedFragment;
import com.nextclick.crm.productDetails.fragments.CatologProductsFragment;
import com.nextclick.crm.productDetails.model.BulkProducts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class BulkProductUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    PreferenceManager preferenceManager;
    CustomDialog customDialog;
    Context mContext;
    private String product_ids;

    private ImageView back_image,upload_image;
    private Button gotohomepage;
    /*
    private LinearLayout layout_itemtype;
    TextInputEditText product_name,product_desc;
    private Spinner category_spinner,menu_spinner,brands_spinner,item_type_spinner;
    //ArrayList<ProductAdd> productaddlist;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask,product_id,previousActivity;
    private String product_name_str, product_desc_str, base64image="",
             sub_cat_id = null, menu_id = null,brand_id=null, product_price_str, product_quantity_str, product_discount_str,product_option_str;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> catIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<String> brandlist = new ArrayList<>();
    private ArrayList<String> brandIDList = new ArrayList<>();
    private int status_type = 1;
    private int productAvailability = 1;
    private boolean isImageSelected;*/
    private ArrayList<String> taxCategoryList =null;
    private ArrayList<String> taxCategoryIDList=null;
    private SelectedImageAdapter selectedImageAdapter;
    private RecyclerView recycle_views;
    private BulkProductUpdateAdapter productUpdateAdapter;
    ArrayList<BulkProducts> bulkProductList;
    private ArrayList<ImagesModel> imageslist;
    private final ArrayList<String> itemtypelist = new ArrayList<>();
    private RecyclerView images_recycle;
    String token,previousActivity;
    private int currentposition, sectionid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_product_update);

        mContext= BulkProductUpdateActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        customDialog=new CustomDialog(BulkProductUpdateActivity.this);

        if(getIntent().hasExtra("product_ids"))
        {
            product_ids =getIntent().getStringExtra("product_ids");
        }

        init();
        getBulkProductVaraints();
    }

    public void init(){
        mContext= BulkProductUpdateActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        customDialog=new CustomDialog(mContext);

        upload_image=findViewById(R.id.upload_image);
        images_recycle=findViewById(R.id.images_recycle);
        back_image=findViewById(R.id.back_image);
        gotohomepage=findViewById(R.id.gotohomepage);
        recycle_views=findViewById(R.id.recycle_products);
        recycle_views.setLayoutManager(new GridLayoutManager(this,1));

        bulkProductList=new ArrayList<>();
        back_image.setOnClickListener(this::onClick);
        gotohomepage.setOnClickListener(this::onClick);
        //currentposition=getIntent().getIntExtra("currentposition",0);
        productUpdateAdapter=new BulkProductUpdateAdapter(BulkProductUpdateActivity.this,bulkProductList);
        recycle_views.setAdapter(productUpdateAdapter);


        imageslist=new ArrayList<>();
        selectedImageAdapter = new SelectedImageAdapter(this, imageslist);
        selectedImageAdapter.collapseCancelButton= true;
        images_recycle.setAdapter(selectedImageAdapter);

        try {
            //Thread thread = new Thread();
            getTaxes();
            //thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    private void getTaxes() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_TAXES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("tax_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        taxCategoryList = new ArrayList<>();
                                        taxCategoryIDList = new ArrayList<>();
                                        taxCategoryList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            taxCategoryIDList.add(categoryObject.getString("id"));
                                            taxCategoryList.add(categoryObject.getString("name"));
                                        }
                                        updateTaxListToProductList();
                                    }
                                }
                                else
                                {
                                   UIMsgs.showToast(mContext, "tax list is empty");
                                }

                            } catch (Exception e) {
                                UIMsgs.showToast(mContext, "tax list is empty "+e.getMessage());
                                e.printStackTrace();
                                getTaxes();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    private void getBulkProductVaraints() {
        customDialog.show();
        HashMap<String,Object> hashmap=new HashMap<>();

        JSONArray servicesjsonArray = new JSONArray();
        for (int i=0;i<product_ids.split(",").length;i++){
            servicesjsonArray.put(product_ids.split(",")[i]);
        }
        hashmap.put("products",servicesjsonArray);


        String dataurl = new JSONObject(hashmap).toString();
        System.out.println("aaaaaaaaa  itemid "+VENDOR_VARIANTS_BULK+dataurl);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_VARIANTS_BULK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaa VENDOR_VARIANTS_BULK  " + jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    //JSONArray dataArray = jsonObject.getJSONArray("data");


                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("data");


                                    imageslist = new ArrayList<>();
                                    bulkProductList.clear();
                                    for (int i = 0; i < dataArray.length(); i++) {
                                        //ideally
                                        /*
                                            imageslist = new ArrayList<>();
                                            productaddlist.clear();
                                         */
                                        JSONObject dataObject = dataArray.getJSONObject(i);
                                        addProduct(dataObject);
                                    }
                                    updateTaxListToProductList();
                                    productUpdateAdapter.RefreshData(bulkProductList);


                                    if (imageslist.size() == 0) {
                                        images_recycle.setVisibility(View.GONE);
                                        upload_image.setVisibility(View.VISIBLE);
                                    } else {
                                        images_recycle.setVisibility(View.VISIBLE);
                                        upload_image.setVisibility(View.GONE);
                                        selectedImageAdapter.refresh(imageslist);
                                    }
                                }
                            } catch (JSONException e) {

                                Toast.makeText(BulkProductUpdateActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                                System.out.println("aaaaaaaaaa   catch " + e);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.cancel();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataurl == null ? null : dataurl.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void addProduct(JSONObject dataObject) {
        BulkProducts bulkProduct =new BulkProducts();

        try {
            int productStatus = dataObject.getInt("status");
            bulkProduct.setName(dataObject.getString("name"));
            bulkProduct.setDescription(dataObject.getString("desc"));
            try{
                bulkProduct.setItemSelection(Integer.parseInt(dataObject.getString("item_type"))-1);
            }catch (Exception e){
            }

            JSONArray sectionsarray = dataObject.getJSONArray("sections");
            JSONObject sectionobject = sectionsarray.getJSONObject(0);
            Integer sectionid = sectionobject.getInt("id");

            try {
                JSONArray imagesArray = dataObject.getJSONArray("item_images");
                ArrayList<ImagesModel> productImages =new ArrayList<>();
                for (int i = 0; i < imagesArray.length(); i++) {
                    JSONObject jsonObject1 = imagesArray.getJSONObject(i);
                    String id = jsonObject1.getString("id");
                    // String image=jsonObject1.getString("image");
                    System.out.println("aaaaaaa image " + id + " " + jsonObject1.getString("image"));
                    ImagesModel imagesModel = new ImagesModel();
                    imagesModel.setId(id);
                    imagesModel.setImage(jsonObject1.getString("image"));
                    imageslist.add(imagesModel);
                    productImages.add(imagesModel);
                }
                bulkProduct.addProdutImages(productImages);

            } catch (Exception e) {
                System.out.println("aaaaaa catch img " + e.getMessage());
            }


            JSONArray sectionarray = dataObject.getJSONArray("section_items");


            ArrayList<ProductAdd> productaddlist = new ArrayList<>();
            for (int i = 0; i < sectionarray.length(); i++) {
                JSONObject jsonObject1 = sectionarray.getJSONObject(i);


                ProductAdd productAdd = new ProductAdd();
                productAdd.setPrice(jsonObject1.getString("price"));
                productAdd.setSizecolor(jsonObject1.getString("name"));//section_item_name
                productAdd.setId(jsonObject1.getString("id"));
                productAdd.setItem_id(jsonObject1.getString("item_id"));
                productAdd.setStatus(jsonObject1.getInt("status"));

                productAdd.setSection_id(sectionid);

                if(jsonObject1.has("sku"))
                    productAdd.setSku(jsonObject1.getString("sku"));
                else
                    productAdd.setSku("0");


                if(jsonObject1.has("discount"))
                    productAdd.setDiscount(jsonObject1.getString("discount"));
                else
                    productAdd.setDiscount("0");

                if(jsonObject1.has("stock"))
                    productAdd.setStock(jsonObject1.getString("stock"));
                else
                    productAdd.setStock("0");
                if(jsonObject1.has("weight"))
                    productAdd.setWeight(jsonObject1.getString("weight"));
                else
                    productAdd.setWeight("0");



                try{
                    productAdd.setReturn_available(jsonObject1.getInt("return_available"));
                }catch (Exception e){
                    productAdd.setReturn_available(0);
                }

                try {
                    //if (jsonObject1.getString("tax_id") != null)
                    // productAdd.setSelectedTaxID(jsonObject1.getString("tax_id"));
                    JSONObject taxObject = jsonObject1.getJSONObject("tax");
                    if (taxObject != null) {
                        productAdd.setSelectedTaxID(taxObject.getString("id"));
                        productAdd.setSelectedTaxTypeID(taxObject.getString("type_id"));

                        //JSONObject taxTypeObject = taxObject.getJSONObject("tax_type");
                    }

                } catch (Exception ex) {

                }
                try{
                    JSONObject returnobj=dataObject.getJSONObject("return_policy");
                    productAdd.setReturn_id(returnobj.getString("id"));
                    productAdd.setReturn_days(returnobj.getString("return_days"));
                    productAdd.setReturn_terms_conditions(returnobj.getString("terms_conditions"));

                }catch (Exception e){

                }
                productaddlist.add(productAdd);
                //   }
            }


            bulkProduct.addProductList(productaddlist);
            /*
            try {
                menu_id = dataObject.getJSONObject("menu").getString("id");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread thread = new Thread();
                thread.sleep(1000);
                for (int i = 0; i < categoryIDList.size(); i++) {
                    if (dataObject.getJSONObject("sub_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                        sub_cat_id = dataObject.getJSONObject("sub_category").getString("id");
                        category_spinner.setSelection((i + 1), true);
                        getMenus(sub_cat_id);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                brand_id = dataObject.getString("brand_id");
                Thread thread = new Thread();
                thread.sleep(1000);
                // for (int i = 0; i < catIDList.size(); i++) {
                //   if (dataObject.getJSONObject("brand").getString("id").equalsIgnoreCase(catIDList.get(i))) {
                brand_id = dataObject.getJSONObject("brand").getString("id");
                String brand_name = dataObject.getJSONObject("brand").getString("name");
                brandlist.add(brand_name);
                brandIDList.add(brand_id);
                // getBrands(catIDList.get(i));
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, brandlist);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                brands_spinner.setAdapter(adapter);
                //  }
                //   }
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            //getreturnpolicy(bulkProduct.getProductList()); -this should come from api
        }
        catch (Exception ex)
        {

        }

        bulkProductList.add(bulkProduct);
    }

    private void updateTaxListToProductList() {
        if(taxCategoryList != null && bulkProductList!=null && bulkProductList.size()>0) {
            for(int k=0;k<bulkProductList.size();k++) {
                ArrayList<ProductAdd> productaddlist =bulkProductList.get(k).getProductList();
                if (productaddlist != null && productaddlist.size() > 0) {
                    System.out.println("updateTaxListToProductList  passed");
                    for (int i = 0; i < productaddlist.size(); i++) {
                        ProductAdd product = productaddlist.get(i);
                        product.setTaxesList(taxCategoryList, taxCategoryIDList);
                    }
                }
                else
                    System.out.println("updateTaxListToProductList  failed");
            }
        }
        else {
            System.out.println("updateTaxListToProductList failed in first if, taxlist is empty "+(taxCategoryList != null)+" bulkProductList is empty "+(taxCategoryList != null));
           // System.out.println("updateTaxListToProductList  failed "+ taxCategoryList!=null ? true: false);
         //  System.out.println("updateTaxListToProductList  failed"+ bulkProductList!=null? true: false);
        }
    }


    private void createOrUpdateCategory(final String data) {
        System.out.println("aaaaaaa request  BULKUPDATEVARIENT  "+data);

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BULKUPDATEVARIENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            //  Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  BULKUPDATEVARIENT  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    System.out.println("aaaaaaaaaaa  update product status ");
                                    Toasty.success(mContext, "The product(s) has been successfully added to your inventory", Toast.LENGTH_SHORT).show();
                                    //uma
                                   // getProductToUpdate(product_id);
                                    //Intent i=new Intent(BulkProductUpdateActivity.this,MainProductpage.class);
                                    //startActivity(i);
                                    Utility.check_product_page=true;
                                    finish();
                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                LoadingDialog.dialog.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                System.out.println("aaaaaaa  666 "+error.getMessage());
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaa tokrn  "+token);
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    /*public void getreturnpolicy(ArrayList<ProductAdd> productaddlist){
        String url=Config.GET_REURNS_DATA+"?sub_cat_id="+sub_cat_id+"&menu_id="+menu_id;
        System.out.println("aaaaaaaa url  "+url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("return_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                    for (int i=0;i<productaddlist.size();i++){
                                        productaddlist.get(i).setReturn_id(jsonObject1.getString("id"));
                                        productaddlist.get(i).setReturn_days(jsonObject1.getString("return_days"));
                                        productaddlist.get(i).setReturn_terms_conditions(jsonObject1.getString("terms_conditions"));
                                    }
                                    String days=jsonObject1.getString("return_days");
                                    String terms_conditions=jsonObject1.getString("terms_conditions");
                                    String return_id=jsonObject1.getString("id");
                                    productUpdateAdapter.RefreshData(bulkProductList);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                productUpdateAdapter.RefreshData(bulkProductList);
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }*/
    public void setreturn(boolean check) {
        if (check){

        }else{

        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.gotohomepage:


                postBulkStockUpdate();

               /* Intent i=new Intent(BulkProductUpdateActivity.this,MainProductpage.class);
                startActivity(i);
                finish();*/
                break;
            case R.id.back_image:
                if(previousActivity!=null && previousActivity.equals("OrdersFragment")) {
                    /*Intent intent = new Intent(BulkProductUpdateActivity.this, OrdersFragment.class);//OrdersFragment
                    startActivity(intent);*/
                    getFragmentManager().popBackStack();
                }
                else {
                    if(Constants.Use_ActivityDesign) {
                        Intent intent = new Intent(BulkProductUpdateActivity.this, MainProductpage.class);
                        intent.putExtra("currentposition", currentposition);
                        startActivity(intent);
                    }
                    else
                    {
                        getFragmentManager().popBackStack();
                    }
                }
                finish();
                break;
            case R.id.banner_image:
                //selectImage();
                break;

        }
    }

    private void postBulkStockUpdate() {
        System.out.println("aaaaaaaaaaa  update product");

        HashMap<String, Object> hashmap = new HashMap<>();

        JSONArray servicesjsonArray = new JSONArray();
        boolean checkerror=false;
        for (int i = 0; i < bulkProductList.size(); i++) {
            try {

                ArrayList<ProductAdd> productList = bulkProductList.get(i).getProductList();

                for (int j = 0; j < productList.size(); j++) {

                    JSONObject uploadMap = new JSONObject();
                    ProductAdd productAdd = productList.get(j);

                    if (productAdd.getPrice().isEmpty()||productAdd.getPrice().startsWith("0")){
                        String sourceString = "Please enter valid price, it should not start with 0 or empty of <b>" + bulkProductList.get(i).getName() + "</b> " ;
                      //  mytextview.setText(Html.fromHtml(sourceString));

                        Toast.makeText(mContext, Html.fromHtml(sourceString), Toast.LENGTH_SHORT).show();
                        checkerror=true;
                        break;

                    }else{
                        if (productAdd.getStock().isEmpty()||productAdd.getStock().startsWith("0")){
                            String sourceString = "Please enter valid Stock, it should not start with 0 or empty of <b>" + bulkProductList.get(i).getName() + "</b> " ;

                            Toast.makeText(mContext, Html.fromHtml(sourceString), Toast.LENGTH_SHORT).show();
                          //  Toast.makeText(mContext, "Please enter valid Stock, it should not start with 0 or empty of "+bulkProductList.get(i).getName(), Toast.LENGTH_SHORT).show();
                            checkerror=true;
                     break;
                        }else{
                            String tax_id;
                            try{
                                if (productAdd.getSelectedTaxID() ==null || productAdd.getSelectedTaxID().isEmpty()){
                                    tax_id="1";
                                }else {
                                    tax_id=productAdd.getSelectedTaxID();
                                }
                            }catch (Exception e){
                                tax_id="1";
                            }

                            if (tax_id != null) {
                                //mandatory param so no tax no update item
                                uploadMap.put("tax_id", productAdd.getSelectedTaxID());


                                //uploadMap.put("variant_id", productAdd.getId()); --old api

                                //new ai
                                uploadMap.put("item_id", productAdd.getItem_id());
                                uploadMap.put("section_id", productAdd.getSection_id());
                                uploadMap.put("section_item_id", productAdd.getId());//getSection_item_id
                                //

                                uploadMap.put("price", productAdd.getPrice());
                                uploadMap.put("stock", "" + productAdd.getStock());
                                uploadMap.put("discount", productAdd.getDiscount());
                                uploadMap.put("weight", "" + productAdd.getWeight());

                                uploadMap.put("status", "" + productAdd.getStatus());
                                uploadMap.put("return_id", "" + productAdd.getReturn_id());
                                uploadMap.put("return_available", "" + productAdd.getReturncheck());

                       /* if (productAdd.getOptionlist() != null) {
                            StringBuilder str = new StringBuilder("");
                            for (String option : productAdd.getOptionlist()) {
                                if (!option.isEmpty())
                                    str.append(option).append(",");
                            }
                            if (str.length() > 0) {
                                uploadMap.put("option_name", str.substring(0, str.length() - 1));
                            }
                        }*/
                                servicesjsonArray.put(uploadMap);
                            }
                        }
                    }



                }

            } catch (JSONException e) {
                System.out.println("aaaaaaaaa catch 1  " + e.getMessage());
                e.printStackTrace();
            }
            if (checkerror){
                break;
            }
        }
        if(servicesjsonArray.length()>0) {
            if (bulkProductList.size()==servicesjsonArray.length()){
                hashmap.put("variants", servicesjsonArray);

                JSONObject json = new JSONObject(hashmap);

                String data = json.toString();
                System.out.println("aaaaaaaa  data  " + data);
                createOrUpdateCategory(data);
            }

        }
        else
        {
           // Toast.makeText(BulkProductUpdateActivity.this, "Please update the tax", Toast.LENGTH_SHORT).show();
        }
    }
}