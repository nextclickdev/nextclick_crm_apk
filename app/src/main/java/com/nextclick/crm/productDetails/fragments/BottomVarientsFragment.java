package com.nextclick.crm.productDetails.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.productDetails.adapters.VarientsAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SuppressLint("ValidFragment")
public class BottomVarientsFragment extends BottomSheetDialogFragment {

    private Context context;
    private RecyclerView varient_recycleview;
    private CustomDialog customDialog;
    String sub_cat_id,menuid,vendoruserid,brand_id,id,search="pass",minimum_price="";
    private TextView tv_productname,tv_price,tv_add,tv_cancel;
    ProductModel productModel;
    private VarientsAdapter varientsAdapter;
    MyProductsFragment myProductsFragment;
    private final String stocktype;

    public BottomVarientsFragment(Context context, ProductModel productModel, MyProductsFragment myProductsFragment,String stocktype) {
        this.context=context;
        this.productModel=productModel;
        this.myProductsFragment=myProductsFragment;
        this.stocktype=stocktype;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style)
    {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.bottomvarientlayout, null);
        context = contentView.getContext();
        dialog.setContentView(contentView);
        //tv_title.setText(getString(R.string.app_name)); R.style.AppBottomSheetDialogTheme

        varient_recycleview=dialog.findViewById(R.id.varient_recycleview);
        tv_productname=dialog.findViewById(R.id.tv_productname);
        tv_price=dialog.findViewById(R.id.tv_price);
        tv_add=dialog.findViewById(R.id.tv_add);
        tv_cancel=dialog.findViewById(R.id.tv_cancel);

        varient_recycleview.setLayoutManager(new GridLayoutManager(context,1));


        varientsAdapter=new VarientsAdapter(context,productModel.getVendorVarientslist(),productModel,myProductsFragment,BottomVarientsFragment.this,
                stocktype);
        varient_recycleview.setAdapter(varientsAdapter);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        int maxHeight = (int) (height*0.44); //custom height of bottom sheet

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((BottomSheetBehavior) behavior).setPeekHeight(maxHeight);  //changed default peek height of bottom sheet

        if (behavior != null && behavior instanceof BottomSheetBehavior)
        {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
            {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState)
                {
                    String state = "";
                    switch (newState)
                    {
                        case BottomSheetBehavior.STATE_DRAGGING: {
                            //imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "DRAGGING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_SETTLING: {
                            // imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "SETTLING";
                            break;
                        }
                        case BottomSheetBehavior.STATE_EXPANDED: {
                            // imgBtnClose.setVisibility(View.VISIBLE);
                            state = "EXPANDED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_COLLAPSED: {
                            //imgBtnClose.setVisibility(View.INVISIBLE);
                            state = "COLLAPSED";
                            break;
                        }
                        case BottomSheetBehavior.STATE_HIDDEN: {
                            // imgBtnClose.setVisibility(View.INVISIBLE);
                            dismiss();
                            state = "HIDDEN";
                            break;
                        }
                    }
                    Log.i("BottomSheetFrag", "onStateChanged: "+ state);
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        }

        tv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

  /*  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.bottomvarientlayout, container, false);
        varient_recycleview=view.findViewById(R.id.varient_recycleview);
        tv_productname=view.findViewById(R.id.tv_productname);

        varient_recycleview.setLayoutManager(new GridLayoutManager(context,1));

        varientlist=new ArrayList<VarientModel>();
         varientsAdapter=new VarientsAdapter(context,varientlist);
        varient_recycleview.setAdapter(varientsAdapter);
        productsFetcher();
        return view;

    }*/
}
