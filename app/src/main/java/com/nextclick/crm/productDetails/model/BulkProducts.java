package com.nextclick.crm.productDetails.model;

import android.text.Spanned;

import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;

import java.util.ArrayList;

public class BulkProducts {

    private ArrayList<ImagesModel> productImages;
    ArrayList<ProductAdd> productaddlist;


    public void addProductList(ArrayList<ProductAdd> productaddlist) {
        this.productaddlist=productaddlist;
    }
    public ArrayList<ProductAdd> getProductList()
    {
        return productaddlist;
    }

    public void addProdutImages(ArrayList<ImagesModel> productImages) {
        this.productImages=productImages;
    }
    public ArrayList<ImagesModel> getProductImages()
    {
        return productImages;
    }

    String name,desc;
    Integer item_type;
    public void setName(String name) {
        this.name=name;
    }
    public String getName()
    {
        return name;
    }

    public void setDescription(String desc) {
        this.desc=desc;
    }
    public String getDescription()
    {
        return desc;
    }

    public void setItemSelection(int item_type) {
        this.item_type=item_type;
    }
    public Integer getItemSelection()
    {
        return item_type;
    }
}
