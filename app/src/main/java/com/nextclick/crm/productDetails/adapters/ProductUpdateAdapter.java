package com.nextclick.crm.productDetails.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.productDetails.activities.ProductUpdateActivity;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductUpdateAdapter extends RecyclerView.Adapter<ProductUpdateAdapter.ViewHolder> {

    List<ProductAdd> data;
    Context context;
    int whrecome;

    public ProductUpdateAdapter(Context activity, List<ProductAdd> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
    }


    @NonNull
    @Override
    public ProductUpdateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.product_update, parent, false);

        return new ProductUpdateAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductUpdateAdapter.ViewHolder holder, final int position) {


        if (data.get(position).getStatus()==1){
            holder.sw_status.setChecked(true);
            holder.status_radio.check(R.id.active);
        }else {
            holder.sw_status.setChecked(false);
            holder.status_radio.check(R.id.inactive);
        }
        holder.checkbox_return.setChecked(data.get(position).getReturn_available() == 1);

            holder.tv_option.setText(context.getString(R.string.option)+ " "+(position+1));
            holder.et_size.setText(""+data.get(position).getSizecolor());
            //   holder.et_option.setText(data.get(position).getOptional());
            holder.et_qty.setText(""+data.get(position).getStock());
            holder.et_price.setText(""+data.get(position).getPrice());
            if(data.get(position).getReturn_days()!=null && !data.get(position).getReturn_days().isEmpty())
            holder.checkbox_return.setText(""+data.get(position).getReturn_days()+" days Returnable");
            else
                holder.checkbox_return.setVisibility(View.GONE);

            holder.et_discount.setText(""+data.get(position).getDiscount());
        if (data.get(position).getWeight() != null && !data.get(position).getWeight().isEmpty()) {
            holder.et_weight.setText(""+data.get(position).getWeight());
        }else {
            holder.et_weight.setText("");
        }

        try{
            Double sellingprice=((Double.parseDouble(data.get(position).getPrice()) / 100.0f) * Double.parseDouble(data.get(position).getDiscount()));
            holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+(Double.parseDouble(data.get(position).getPrice()) -sellingprice ),2));

           // holder.et_sellingprice.setText(""+(Double.parseDouble(data.get(position).getPrice()) -sellingprice ));

        }catch (NumberFormatException e){

            try{
                holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+holder.et_price.getText().toString() ,2));

            }catch (NumberFormatException e1){
                holder.et_sellingprice.setText("₹ "+holder.et_price.getText().toString());

            }

           // holder.et_sellingprice.setText( holder.et_price.getText().toString());
        }

        holder.et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    Double sellingprice=(((Double.parseDouble(holder.et_price.getText().toString()) / 100.0f) * Double.parseDouble(s.toString())));
                  //  holder.et_sellingprice.setText(""+(Double.parseDouble(holder.et_price.getText().toString())-sellingprice));
                    holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+(Double.parseDouble(holder.et_price.getText().toString())-sellingprice) ,2));

                }catch (NumberFormatException e){
                    holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+holder.et_price.getText().toString() ,2));

                  //  holder.et_sellingprice.setText(""+holder.et_price.getText().toString());
                }

            }
        });
        holder.et_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try{
                    Double sellingprice=(((Double.parseDouble(s.toString()) / 100.0f) * Double.parseDouble(holder.et_discount.getText().toString())));
                   // holder.et_sellingprice.setText(""+(Double.parseDouble(s.toString())-sellingprice));
                    holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+(Double.parseDouble(s.toString())-sellingprice) ,2));

                }catch (NumberFormatException e){
                    try{
                        if (s.length()!=0){
                            holder.et_sellingprice.setText("₹ "+ Utility.roundFloat(""+ s,2));
                        }
                    }catch (NumberFormatException e1){

                    }

                  //  holder.et_sellingprice.setText(""+s.toString());
                }

            }
        });

        holder.categoriesList= data.get(position).getTaxCategoryList();
        holder.categoryIDList=data.get(position).getTaxCategoryIDList();

        if( holder.categoryIDList!=null) {
            ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_item, holder.categoriesList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.category_spinner.setAdapter(categoryAdapter);

            try {
                if (data.get(position).getSelectedTaxTypeID() != null) {
                    int selCategory = holder.categoryIDList.indexOf(data.get(position).getSelectedTaxTypeID());
                    holder.category_spinner.setSelection((selCategory + 1), true);
                }
            }
            catch (Exception ex)
            {

            }
        }
        if (data.get(position).getSelectedTaxID() != null && !data.get(position).getSelectedTaxID().isEmpty()) {
            holder.menu_id= data.get(position).getSelectedTaxID();
        }

        holder.save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (data.get(position).getStatus()==1){
                        String price=holder.et_price.getText().toString().trim();
                        String stock=holder.et_qty.getText().toString().trim();
                        String discount=holder.et_discount.getText().toString().trim();
                        if (price.startsWith("0") || price.isEmpty()){
                            Toast.makeText(context, "Please enter valid price, it should not start with 0 or empty", Toast.LENGTH_SHORT).show();
                        }else {
                            if (stock.startsWith("0") || stock.isEmpty()){
                                Toast.makeText(context, "Please enter valid Stock, it should not start with 0 or empty", Toast.LENGTH_SHORT).show();
                            }else {
                           /* if (discount.startsWith("0")){
                                Toast.makeText(context, "Please enter valid discount", Toast.LENGTH_SHORT).show();
                            }else {*/
                                if (holder.category_spinner.getSelectedItemPosition()==0){
                                    Toast.makeText(context, "Please select Tax", Toast.LENGTH_SHORT).show();
                                }else{
                                    if (holder.menu_spinner.getSelectedItemPosition()==0){
                                        Toast.makeText(context, "Please select Taxes", Toast.LENGTH_SHORT).show();
                                    }else {
                                        data.get(position).setDiscount(holder.et_discount.getText().toString());
                                        data.get(position).setPrice(holder.et_price.getText().toString());
                                        data.get(position).setStock(holder.et_qty.getText().toString());
                                       if (holder.checkbox_return.isChecked()){
                                           data.get(position).setReturncheck(1);
                                       }else {
                                           data.get(position).setReturncheck(0);
                                       }

                                        ((ProductUpdateActivity)context).setsaveVarient(data.get(position));
                                    }

                                    }

                                //  }
                            }
                        }
                    }else {
                        String price=holder.et_price.getText().toString().trim();
                        String stock=holder.et_qty.getText().toString().trim();
                        String discount=holder.et_discount.getText().toString().trim();
                       /* if (price.startsWith("0")){
                            Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                        }else {
                            if (stock.startsWith("0")){
                                Toast.makeText(context, "Please enter valid Stock", Toast.LENGTH_SHORT).show();
                            }else {*/

                                data.get(position).setDiscount(holder.et_discount.getText().toString());
                                data.get(position).setPrice(holder.et_price.getText().toString());
                                data.get(position).setStock(holder.et_qty.getText().toString());
                        if (holder.checkbox_return.isChecked()){
                            data.get(position).setReturncheck(1);
                        }else {
                            data.get(position).setReturncheck(0);
                        }
                                ((ProductUpdateActivity)context).setsaveVarient(data.get(position));

                         //   }
                       // }
                    }


                }
            });

        holder.checkbox_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.get(position).getReturn_available()==1){
                    android.app.AlertDialog.Builder alertDialogBuilder = new
                            android.app.AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage(""+Html.fromHtml(data.get(position).getReturn_terms_conditions()));
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.setPositiveButton("Reject", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            holder.checkbox_return.setChecked(false);
                            data.get(position).setReturncheck(0);
                        }
                    });alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            holder.checkbox_return.setChecked(true);
                            data.get(position).setReturncheck(1);
                        }
                    });
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }else if(data.get(position).getReturn_terms_conditions() != null && !data.get(position).getReturn_terms_conditions().isEmpty()){
                    android.app.AlertDialog.Builder alertDialogBuilder = new
                            android.app.AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage(""+Html.fromHtml(data.get(position).getReturn_terms_conditions()));
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            holder.checkbox_return.setChecked(true);
                            data.get(position).setReturncheck(1);
                        }
                    });alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            holder.checkbox_return.setChecked(false);
                            data.get(position).setReturncheck(0);
                        }
                    });
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }
        });

       /* holder.checkbox_return.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (terms_conditions.isEmpty()){

               }else {
                   System.out.println("aaaaaaa check checkbox click");
                   android.app.AlertDialog.Builder alertDialogBuilder = new
                           android.app.AlertDialog.Builder(context);
                   alertDialogBuilder.setMessage(""+terms_conditions);
                   alertDialogBuilder.setCancelable(true);
                   alertDialogBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.cancel();
                           holder.checkbox_return.setChecked(true);
                       }
                   });alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           dialog.cancel();
                           holder.checkbox_return.setChecked(false);
                       }
                   });
                   android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                   alertDialog.show();
               }

               */
        /* if (isChecked){
                    ((ProductUpdateActivity)context).setreturn(isChecked);
                }*//*
            }
        });*/
       /* holder. et_discount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                data.get(position).setDiscount(holder.et_discount.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
       holder.et_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // data.get(getAdapterPosition()).setPrice(et_price.getText().toString());
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.toString().startsWith("0"))
                {
                    holder.et_price.setText("");
                    Toast.makeText(context, "Please enter valid price", Toast.LENGTH_SHORT).show();
                }else {
                    data.get(position).setPrice(holder.et_price.getText().toString());
                }
            }
        }); holder.et_qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                data.get(position).setStock(holder.et_qty.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

*/
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setrefresh(ArrayList<ProductAdd> productaddlist,int whrecome) {
        this.data=productaddlist;
        this.whrecome=whrecome;
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextInputEditText et_size,et_price,et_qty,et_discount,et_weight,et_sellingprice;

        private final Spinner category_spinner;
        private final Spinner menu_spinner;
        TextView tv_option;
        Switch sw_status;
         Button save;
         RadioGroup status_radio;
         CheckBox checkbox_return;

         String sub_cat_id = null, menu_id = null;
        ArrayList<String> categoriesList= new ArrayList<>();
        private ArrayList<String> categoryIDList= new ArrayList<>();
        private ArrayList<String> menusList = new ArrayList<>();
        private ArrayList<String> menuIDList = new ArrayList<>();

        ArrayAdapter<String>  categoryAdapter;

        @SuppressLint("ResourceAsColor")
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            et_size = itemView.findViewById(R.id.et_size);
            et_discount = itemView.findViewById(R.id.et_discount);
           // et_option = itemView.findViewById(R.id.et_option);
            et_price = itemView.findViewById(R.id.et_price);
            et_weight = itemView.findViewById(R.id.et_weight);
            et_qty = itemView.findViewById(R.id.et_qty);
            tv_option = itemView.findViewById(R.id.tv_option);
            save = itemView.findViewById(R.id.save);
            status_radio = itemView.findViewById(R.id.status_radio);
            category_spinner = itemView.findViewById(R.id.category_spinner);
            menu_spinner = itemView.findViewById(R.id.menu_spinner);
            et_sellingprice = itemView.findViewById(R.id.et_sellingprice);
            checkbox_return = itemView.findViewById(R.id.checkbox_return);



            categoriesList = new ArrayList<>();
            //categoriesList.add("Select");

            //categoryIDList.add("1");
           // categoriesList.add("GST");

           /* categoryAdapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_item, categoriesList);
            categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            category_spinner.setAdapter(categoryAdapter);

            ArrayAdapter<String> menuAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, menusList);
            menuAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            menu_spinner.setAdapter(menuAdapter);*/



            sw_status = itemView.findViewById(R.id.sw_status);

            et_size.setEnabled(false);
            et_size.setClickable(false);

            et_weight.setEnabled(false);
            et_weight.setClickable(false);

            et_sellingprice.setEnabled(false);
            et_sellingprice.setClickable(false);

        //    et_size.setTextColor(R.color.black);
            status_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch(checkedId){
                        case R.id.active:
                            data.get(getAdapterPosition()).setStatus(1);
                            // do operations specific to this selection
                            break;
                        case R.id.inactive:
                            data.get(getAdapterPosition()).setStatus(2);
                            // do operations specific to this selection
                            break;

                    }

                }
            });

            sw_status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b){
                        data.get(getAdapterPosition()).setStatus(1);
                    }else {
                        data.get(getAdapterPosition()).setStatus(2);
                    }

                }
            });
            et_size.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setSizecolor(et_size.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            /*et_option.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    data.get(getAdapterPosition()).setOptional(et_option.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });*/
            et_weight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    //  data.get(getAdapterPosition()).setWeight(et_weight.getText().toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(editable.toString().startsWith("0"))
                    {
                        et_weight.setText("");
                       // Toast.makeText(context, "Please enter valid weight", Toast.LENGTH_SHORT).show();
                    }else {
                        data.get(getAdapterPosition()).setWeight(et_weight.getText().toString());
                    }
                }
            });
            category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        sub_cat_id = categoryIDList.get(position - 1);
                        getMenus(sub_cat_id);
                    } else {
                        sub_cat_id = null;
                        menu_id = null;
                        menuIDList.clear();
                        menusList.clear();
                       // menu_spinner.setAdapter(null);
                    }
                }

                private void getMenus(String sub_cat_id) {
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_TAX_BY_ID + sub_cat_id,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("menu_res", response);

                                    if (response != null) {
                                        try {

                                            JSONObject jsonObject = new JSONObject(response);
                                            boolean status = jsonObject.getBoolean("status");
                                            if (status) {
                                                JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("taxes");
                                                if (dataArray.length() > 0) {
                                                    menusList = new ArrayList<>();
                                                    menuIDList = new ArrayList<>();
                                                    menusList.add("Select");
                                                    for (int i = 0; i < dataArray.length(); i++) {
                                                        JSONObject categoryObject = dataArray.getJSONObject(i);//type_id,id,tax,rate
                                                        menusList.add(categoryObject.getString("tax")/* + ","+categoryObject.getString("rate")*/);
                                                        menuIDList.add(categoryObject.getString("id"));
                                                       // menuIDList.add(categoryObject.getString("type_id"));
                                                    }
                                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, menusList);
                                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                                    menu_spinner.setAdapter(adapter);

                                                    try {
                                                        if (menu_id != null) {
                                                            try {
                                                                Thread thread = new Thread();
                                                                Thread.sleep(1000);
                                                                for (int i = 0; i < menuIDList.size(); i++) {
                                                                    if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                                        menu_spinner.setSelection((i + 1), true);
                                                                    }
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> map = new HashMap<>();
                            map.put("Content-Type", "application/json");
                            return map;
                        }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(stringRequest);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        menu_id = menuIDList.get(position - 1);
                        data.get(getAdapterPosition()).setSelectedTaxID(menu_id);
                    } else {
                        menu_id = null;
                        data.get(getAdapterPosition()).setSelectedTaxID(menu_id);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

}
