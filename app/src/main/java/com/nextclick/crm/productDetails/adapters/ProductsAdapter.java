package com.nextclick.crm.productDetails.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.activities.ProductUpdateActivity;
import com.nextclick.crm.productDetails.fragments.ApprovedFragment;
import com.nextclick.crm.productDetails.fragments.BottomVarientsFragment;
import com.nextclick.crm.productDetails.fragments.CatologProductsFragment;
import com.nextclick.crm.productDetails.fragments.MyProductsFragment;
import com.nextclick.crm.productDetails.fragments.PendingFragment;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Transformation;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ADDPRODUCTTOMYLIST;
import static com.nextclick.crm.Config.Config.PRODUCT_C;
import static com.nextclick.crm.Config.Config.PRODUCT_D;
import static com.nextclick.crm.Config.Config.PRODUCT_U;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> implements Filterable {

    private final Context context;
    private final Transformation transformation;
    ProductAdapterListener listener;

    private final List<ProductModel> data;
    private final List<ProductModel> data_full;

    private String token;
    private final int currentposition;
    //MainProductpage mainProductpage;
    PreferenceManager preferenceManager;
    CatologProductsFragment catologProductsFragment;
    ApprovedFragment approvedFragment;
    CustomDialog customDialog;
    PendingFragment pendingFragment;
    MyProductsFragment myProductsFragment;
    String stocktype;
    public ProductsAdapter(Context context, List<ProductModel> itemPojos, int currentposition) {
        this.context = context;
        this.data = itemPojos;
        this.currentposition = currentposition;
       // this.mainProductpage = mainProductpage;
        preferenceManager=new PreferenceManager(context);
        customDialog=new CustomDialog(context);

        data_full = new ArrayList<>(itemPojos);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }


    public ProductsAdapter(CatologProductsFragment catologProductsFragment, Context mContext, ArrayList<ProductModel> productModelsList,
                           int i, MainProductpage mainProductpage,ProductAdapterListener listener) {
        this(catologProductsFragment,mContext,productModelsList,i,mainProductpage);
        this.listener=listener;
        selectedItems = new SparseBooleanArray();
    }

    public ProductsAdapter(CatologProductsFragment catologProductsFragment, Context mContext, ArrayList<ProductModel> productModelsList,
                           int i, MainProductpage mainProductpage) {
        this.context = mContext;
        this.data = productModelsList;
        this.catologProductsFragment = catologProductsFragment;
        this.currentposition = i;
        //this.mainProductpage = mainProductpage;
        preferenceManager=new PreferenceManager(context);
        customDialog=new CustomDialog(context);

        data_full = new ArrayList<>(productModelsList);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    public ProductsAdapter(ApprovedFragment approvedFragment, Context mContext, ArrayList<ProductModel> productModelsList,
                           int i, MainProductpage mainProductpage) {
        this.context = mContext;
        this.data = productModelsList;
        this.approvedFragment = approvedFragment;
        this.currentposition = i;
        //this.mainProductpage = mainProductpage;
        preferenceManager=new PreferenceManager(context);
        customDialog=new CustomDialog(context);

        data_full = new ArrayList<>(productModelsList);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    public ProductsAdapter(PendingFragment pendingFragment, Context mContext, ArrayList<ProductModel> productModelsList,
                           int i, MainProductpage mainProductpage) {
        this.context = mContext;
        this.data = productModelsList;
        this.pendingFragment = pendingFragment;
        this.currentposition = i;
        //this.mainProductpage = mainProductpage;
        preferenceManager=new PreferenceManager(context);
        customDialog=new CustomDialog(context);

        data_full = new ArrayList<>(productModelsList);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }

    public ProductsAdapter(MyProductsFragment myProductsFragment, Context mContext, ArrayList<ProductModel> productModelsList,
                           int i, MainProductpage mainProductpage,String stocktype) {
        this.context = mContext;
        this.data = productModelsList;
        this.myProductsFragment = myProductsFragment;
        this.stocktype = stocktype;
        this.currentposition = i;
        //this.mainProductpage = mainProductpage;
        preferenceManager=new PreferenceManager(context);
        customDialog=new CustomDialog(context);

        data_full = new ArrayList<>(productModelsList);

        transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(5)
                .borderColor(Color.parseColor("#00000000"))
                .borderWidthDp(1)
                .oval(false)
                .build();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_product_item_new, parent, false);
        token = "Bearer " +new PreferenceManager(context).getString(USER_TOKEN);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ProductModel productModel = data.get(position);

      //  String productName = productModel.getName();
        String productName = productModel.getName().substring(0, 1).toUpperCase() + productModel.getName().substring(1);
        String productDescription = productModel.getDesc().substring(0, 1).toUpperCase()
                + productModel.getDesc().substring(1);



       // String productDescription = productModel.getDesc();
        int productPrice = productModel.getPrice();
        int discountPrice = productModel.getDiscount();
        String menuName = productModel.getMenu_name();
        try{
            String productImageUrl = productModel.getImagelist().get(0).getImage();
            System.out.println("aaaaaaaaaa  imgurl  "+productImageUrl);
            if (productImageUrl != null) {
                if (!productImageUrl.isEmpty()) {
                   /* Picasso.get()
                            .load(productImageUrl)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .transform(transformation)
                            .into(holder.ivProductImage);*/

                    Glide.with(context)
                            .load(productImageUrl)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(holder.ivProductImage);
                } else {
                  /*  Picasso.get()
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .transform(transformation)
                            .into(holder.ivProductImage);*/

                    Glide.with(context)
                            .load(R.drawable.ic_default_place_holder)
                            .error(R.drawable.ic_default_place_holder)
                            .placeholder(R.drawable.ic_default_place_holder)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(holder.ivProductImage);
                }
            } else {
               /* Picasso.get()
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .transform(transformation)
                        .into(holder.ivProductImage);*/

                Glide.with(context)
                        .load(R.drawable.ic_default_place_holder)
                        .error(R.drawable.ic_default_place_holder)
                        .placeholder(R.drawable.ic_default_place_holder)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(holder.ivProductImage);
            }

        }catch (NullPointerException e){
            System.out.println("aaaaaa  catch adapter "+e.getMessage());
        }

        String categoryName = productModel.getCategoryName();
        String status = productModel.getAvailabilityStatus();

        holder.tvMenu.setText(menuName);
        holder.tvCategory.setText(categoryName);
        holder.tvProductName.setText(productName);
        holder.tvProductDescription.setText(Html.fromHtml(productDescription));
        holder.tvPrice.setText(context.getString(R.string.price_symbol) + " " + productPrice);
        holder.tvDiscount.setText(context.getString(R.string.price_symbol) + " " + discountPrice);



        if (currentposition==0){
            holder.layout_menu.setVisibility(View.GONE);
            holder.layout_category.setVisibility(View.GONE);
            holder.product_availability.setVisibility(View.GONE);
            holder.layout_stock.setVisibility(View.VISIBLE);
            try{
                holder.tv_varient_name.setText(""+productModel.getVendorVarientslist().get(0).getStock());
                int min =Integer.parseInt(productModel.getVendorVarientslist().get(0).getStock());
                for (int k=0;k<productModel.getVendorVarientslist().size();k++){
                    if (Integer.parseInt(productModel.getVendorVarientslist().get(k).getStock()) < min) {
                        min = Integer.parseInt(productModel.getVendorVarientslist().get(k).getStock());
                    }
                }
            }catch (NullPointerException e){

            }

            if (preferenceManager.getString("is_admin").equalsIgnoreCase("true")){
                holder.ivDelete.setVisibility(View.VISIBLE);
                holder.iv_edit.setVisibility(View.VISIBLE);
                holder.iv_add.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
            }else {

                holder.iv_edit.setVisibility(View.VISIBLE);
                holder.status_radio.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
                holder.iv_add.setVisibility(View.GONE);
            }

        }else if (currentposition==1){
            holder.layout_stock.setVisibility(View.GONE);
            holder.product_availability.setVisibility(View.GONE);
            if (preferenceManager.getString("is_admin").equalsIgnoreCase("true")){
                holder.ivDelete.setVisibility(View.VISIBLE);
                holder.iv_edit.setVisibility(View.VISIBLE);
                holder.iv_add.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
            }else {
                holder.iv_edit.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
              //  holder.iv_add.setVisibility(View.VISIBLE);
                try{
                    if (productModel.getMyinventory().equalsIgnoreCase("0")){
                        holder.iv_add.setVisibility(View.VISIBLE);
                    }else{
                        holder.iv_add.setVisibility(View.GONE);
                    }
                }catch (NullPointerException e){

                }
            }
        }else if (currentposition==2){
            holder.layout_stock.setVisibility(View.GONE);
            holder.product_availability.setVisibility(View.GONE);
            if (preferenceManager.getString("is_admin").equalsIgnoreCase("true")){
                holder.ivDelete.setVisibility(View.GONE);
                holder.iv_edit.setVisibility(View.GONE);
                holder.iv_add.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
            }else {
                holder.iv_edit.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.GONE);
                System.out.println("aaaaaaaaa  invewntory  "+productModel.getMyinventory());
             //   holder.iv_add.setVisibility(View.VISIBLE);
                try{
                    if (productModel.getMyinventory().equalsIgnoreCase("0")){
                        holder.iv_add.setVisibility(View.VISIBLE);
                    }else{
                        holder.iv_add.setVisibility(View.GONE);
                    }
                }catch (NullPointerException e){

                }
            }

        } else {
            holder.layout_stock.setVisibility(View.GONE);
            holder.product_availability.setVisibility(View.GONE);
            if (preferenceManager.getString("is_admin").equalsIgnoreCase("true")){
                holder.ivDelete.setVisibility(View.GONE);
                holder.iv_edit.setVisibility(View.GONE);
                holder.iv_add.setVisibility(View.GONE);
                holder.status_radio.setVisibility(View.GONE);
            }else {
                holder.iv_edit.setVisibility(View.VISIBLE);
                holder.status_radio.setVisibility(View.GONE);
                holder.ivDelete.setVisibility(View.VISIBLE);
                holder.iv_add.setVisibility(View.GONE);
            }
        }
        if (status.equalsIgnoreCase("1")) {
            holder.status_radio.check(R.id.active);
            holder.tvProductAvailability.setText(context.getString(R.string.in_stock));
            holder.tvProductAvailability.setTextColor(Color.parseColor("#1B5E20"));

        } else {
            holder.status_radio.check(R.id.inactive);
            holder.tvProductAvailability.setText(context.getString(R.string.out_of_stock));
            holder.tvProductAvailability.setTextColor(Color.parseColor("#B71C1C"));
        }

        if(this.listener!=null)
            applyIconAnimation(holder, position);

        holder.layout_stock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(productModel.getVendorVarientslist()!=null && productModel.getVendorVarientslist().size()>0) {
                    if(context instanceof ShopNowHomeActivity) {
                        BottomVarientsFragment bottomVarientsFragment = new BottomVarientsFragment(context, productModel, myProductsFragment, stocktype);
                        bottomVarientsFragment.show(((ShopNowHomeActivity) context).getSupportFragmentManager(), bottomVarientsFragment.getTag());
                    }
                    //com.nextclick.crm.productDetails.activities.MainProductpage cannot be cast to com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity
                }
                else
                    UIMsgs.showToast(context, "No Varaints are found.");

            }
        });

        holder.status_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.active:
                       // int pos=getLayoutPosition();
                       // ProductModel productModel = data.get(position);
                        statusChange(data, position, data.get(position).getId(),1);
                        break;
                    case R.id.inactive:
                       // int pos1=getLayoutPosition();
                       // ProductModel productModel1 = data.get(position);
                        statusChange(data, position, data.get(position).getId(),2);
                        break;

                }

            }
        });

        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("aaaaaaaaaaa  id delete ");
                showDeleteAlertDialog(position);
            }
        });
        holder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProductModel productModel = data.get(position);
                if (currentposition==0){
                    Intent intent = new Intent(context, ProductUpdateActivity.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                    //  intent.putExtra("type", "u");
                    intent.putExtra("currentposition",currentposition);
                    context.startActivity(intent);
                   //mainProductpage.finish();
                }else {
                    Intent intent = new Intent(context, ProductAddOrUpdate.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                    intent.putExtra("type", "u");
                    intent.putExtra("currentposition",currentposition);
                    context.startActivity(intent);
                  //  mainProductpage.finish();
                }
            }
        });
        holder.iv_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ProductModel productModel = data.get(position);
                addProduct(data,position, productModel.getId());
            }
        });
    }

    private void deleteProduct(final List<ProductModel> data, final int position, String id) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String deleteProductUrl = PRODUCT_D + id;
        System.out.println("aaaaaaaaa  delete url "+deleteProductUrl);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, deleteProductUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaa delete response  "+ response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(context, "Deleted");
                                    data.remove(position);
                                    notifyDataSetChanged();
                                } else {
                                    UIMsgs.showToast(context, "Unable to delete. Please try later");
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaaa  delete catch "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaaaa  delete error  "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void clearSelections() {
        //reverseAllAnimations = true;
        selectedItems.clear();
        notifyDataSetChanged();
    }

    private static int currentSelectedIndex = -1;
    private SparseBooleanArray selectedItems;

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public String getSelectedItems() {
        String result="";
        for(int i=0;i<selectedItems.size();i++)
        {
            if (selectedItems.valueAt(i)) {
                ProductModel productModel = data.get(selectedItems.keyAt(i));
                result += (productModel.getId()+",");
            }
        }
        if(result.endsWith(","))
            result=result.substring(0,result.length()-1);
        return result;
    }


    public void toggleSelection(int pos) {
        if (selectedItems == null)
            return;
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
            // animationItemsIndex.delete(pos);
        } else {
            if (data.get(pos).getMyinventory() != null && data.get(pos).getMyinventory().equals("0")) {
                selectedItems.put(pos, true);
                // animationItemsIndex.put(pos, true);
            }
        }
        notifyItemChanged(pos);
    }
    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    private void applyIconAnimation(ViewHolder holder, int position) {
        if(selectedItems ==null)
            return;
        if (selectedItems.get(position, false)) {
            if (currentSelectedIndex == position) {
               // FlipAnimator.flipView(mContext, holder.iconBack, holder.iconFront, true);
                //resetCurrentIndex();
            }

            holder.iv_add.setVisibility(View.GONE);

            holder.layout_root.setBackgroundColor(Color.parseColor("#f7b733"));//F26B35
        } else {
            holder.layout_root.setBackground(null);

            if (data.get(position).getMyinventory().equalsIgnoreCase("0")){
                holder.iv_add.setVisibility(View.VISIBLE);
            }else{
                holder.iv_add.setVisibility(View.GONE);
            }
        }
    }

    public void resetAnimationIndex() {
      //  reverseAllAnimations = false;
     //   animationItemsIndex.clear();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ProductModel> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(data_full);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (ProductModel model : data_full) {
                    if (model.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            try {
                data.clear();
                data.addAll((List) results.values);
                notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void setchanged(String stock_type) {
        this.stocktype=stock_type;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener ,View.OnLongClickListener {

        private final ImageView ivProductImage;
        LinearLayout iv_edit,iv_add,ivDelete,layout_category,layout_menu,layout_root;
        private final TextView tvProductName;
        private final TextView tv_varient_name;
        private final TextView tvProductDescription;
        private final TextView tvCategory;
        private final TextView tvMenu;
        private final TextView tvPrice;
        private final TextView tvDiscount;
        private final TextView tvProductAvailability;
        private final Switch sw_status;
        RadioGroup status_radio;
        LinearLayout product_availability,layout_stock;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layout_root= itemView.findViewById(R.id.layout_root);
            tvMenu = itemView.findViewById(R.id.tvMenu);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            ivDelete = itemView.findViewById(R.id.btn_delete);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvCategory = itemView.findViewById(R.id.tvCategory);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            ivProductImage = itemView.findViewById(R.id.ivProductImage);
            tvProductDescription = itemView.findViewById(R.id.tvProductDescription);
            tvProductAvailability = itemView.findViewById(R.id.tvProductAvailability);
            iv_edit = itemView.findViewById(R.id.btn_edit);
            iv_add = itemView.findViewById(R.id.btn_add);
            sw_status = itemView.findViewById(R.id.sw_status);
            status_radio = itemView.findViewById(R.id.status_radio);
            tv_varient_name = itemView.findViewById(R.id.tv_varient_name);
            layout_stock = itemView.findViewById(R.id.layout_stock);
            layout_category = itemView.findViewById(R.id.layout_category);
            layout_menu = itemView.findViewById(R.id.layout_menu);


            product_availability = itemView.findViewById(R.id.product_availability);

            ivDelete.setOnClickListener(this);
            itemView.setOnClickListener(this);
            iv_add.setOnClickListener(this);
            iv_edit.setOnClickListener(this);

            itemView.setOnLongClickListener(this);
        }


        @Override
        public boolean onLongClick(View view) {
            if(listener!=null) {
                listener.onRowLongClicked(getAdapterPosition());
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
            }
            return true;
        }

        @Override
        public void onClick(View view) {
            int id = view.getId();

            if (id == R.id.ivDelete) {
                System.out.println("aaaaaaaaaaa  id delete "+id);
                showDeleteAlertDialog(getLayoutPosition());
            } else if (id==R.id.iv_edit){
                System.out.println("aaaaaaaaaaa  id editclick "+id);
                final ProductModel productModel = data.get(getLayoutPosition());
                if (currentposition==0){
                    Intent intent = new Intent(context, ProductUpdateActivity.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                  //  intent.putExtra("type", "u");
                    intent.putExtra("currentposition",currentposition);
                    context.startActivity(intent);
                   // mainProductpage.finish();
                }else {
                    Intent intent = new Intent(context, ProductAddOrUpdate.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                    intent.putExtra("type", "u");
                    intent.putExtra("currentposition",currentposition);
                    context.startActivity(intent);
                   // mainProductpage.finish();
                }

            }else if (id==R.id.iv_add){
                System.out.println("aaaaaaaaaaa  id addclick "+id);
                final ProductModel productModel = data.get(getLayoutPosition());
                addProduct(data, getLayoutPosition(), productModel.getId());
            }
            /*else {
                final ProductModel productModel = data.get(getLayoutPosition());
                if (currentposition==0){
                    Intent intent = new Intent(context, ProductUpdateActivity.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                    //  intent.putExtra("type", "u");
                    intent.putExtra("currentposition",currentposition);
                    context.startActivity(intent);
                    mainProductpage.finish();
                }else {
                    System.out.println("aaaaaaaaa  update click");
                    Intent intent = new Intent(context, ProductAddOrUpdate.class);
                    //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);
                    intent.putExtra("product_id", productModel.getId());
                    intent.putExtra("type", "u");
                    intent.putExtra("currentposition", currentposition);
                    context.startActivity(intent);
                    mainProductpage.finish();
                }
            }*/
        }

    /*    @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ProductModel productModel = data.get(getLayoutPosition());
            int status = productModel.getStatus();
            if (isChecked) {
                if (status == 2) {
                    productModel.setStatus(1);
                    serviceCallToUpdateStatus(productModel.getId(), 1);
                }
            } else {
                if (status == 1) {
                    productModel.setStatus(2);
                    serviceCallToUpdateStatus(productModel.getId(), 2);
                }
            }
        }
    }*/

        private void serviceCallToUpdateStatus(String productId, int status) {
            LoadingDialog.loadDialog(context);
            Map<String, String> uploadMap = new HashMap<>();
            uploadMap.put("id", productId);
            uploadMap.put("status", String.valueOf(status));
            String url = PRODUCT_C;
            final String jsonObject = new JSONObject(uploadMap).toString();
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response != null) {
                                Log.d("asd", response);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    LoadingDialog.dialog.dismiss();
                    UIMsgs.showToast(context, OOPS);
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put(AUTH_TOKEN, token);
                    return map;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return jsonObject.getBytes(StandardCharsets.UTF_8);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }


    }
    private void showDeleteAlertDialog(final int position) {
        final ProductModel productModel = data.get(position);
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage("Are you sure to remove..?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteProduct(data, position, productModel.getId());
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    private void addProduct(final List<ProductModel> data, final int position, String id) {
        customDialog.show();
        HashMap<String,String> hashmap=new HashMap<>();
        hashmap.put("item_id",id);
       String dataurl = new JSONObject(hashmap).toString();
        System.out.println("aaaaaaaaa  itemid "+ADDPRODUCTTOMYLIST+dataurl);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String deleteProductUrl = PRODUCT_D + id;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADDPRODUCTTOMYLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            customDialog.cancel();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaa added  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {

                                    if (currentposition==1 || currentposition==2){
                                        Intent intent = new Intent(context, ProductUpdateActivity.class);
                                        //  Intent intent = new Intent(context, CreateAndUpdateProductActivity.class);

                                        intent.putExtra("product_id", ""+id);
                                        //  intent.putExtra("type", "u");
                                        intent.putExtra("currentposition",currentposition);
                                        context.startActivity(intent);
                                        catologProductsFragment.getProducts(1);

                                    }else {
                                        approvedFragment.getProducts(1);
                                    }

                                    UIMsgs.showToast(context, "Product has been added to your Inventory.");
                                    Utility.RefreshMyProducts=true;
                                  //  data.remove(position);
                                  //  notifyDataSetChanged();
                                } else {

                                    UIMsgs.showToast(context, "Product has been already added to your Inventory.");
                                    /*//testing
                                    Intent intent = new Intent(context, ProductUpdateActivity.class);
                                    intent.putExtra("product_id", ""+id);
                                    intent.putExtra("currentposition",currentposition);
                                    context.startActivity(intent);*/
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            customDialog.cancel();
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                customDialog.cancel();
                System.out.println("aaaaaaaa  error "+error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return dataurl == null ? null : dataurl.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void statusChange(final List<ProductModel> data, final int position, String id,int active) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        String statuschange = PRODUCT_U + id;
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("availability", ""+active);
        JSONObject json = new JSONObject(uploadMap);
        String datajson = json.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, statuschange,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(context, "Status Updated");
                                    notifyDataSetChanged();
                                  //  myProductsFragment.getProducts(1);
                                } else {
                                    UIMsgs.showToast(context, "Unable to Update status. Please try later");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return datajson == null ? null : datajson.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public interface ProductAdapterListener {
        void onIconClicked(int position);

        void onIconImportantClicked(int position);

        void onProductRowClicked(int position);

        void onRowLongClicked(int position);
    }
}

