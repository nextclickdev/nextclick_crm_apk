package com.nextclick.crm.productDetails.model;

public class Shopbycategeries {
    String id,cat_id,type,name,desc,product_type_widget_status,image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProduct_type_widget_status() {
        return product_type_widget_status;
    }

    public void setProduct_type_widget_status(String product_type_widget_status) {
        this.product_type_widget_status = product_type_widget_status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
