package com.nextclick.crm.productDetails.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.productDetails.adapters.ViewPagerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.nextclick.crm.productDetails.model.MyProductsModel;
import com.nextclick.crm.productDetails.model.VendorVarients;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MYPRODUCTLIST;
import static com.nextclick.crm.Config.Config.VENDOR_PRODUCTS_COUNT;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class MainProductpage extends AppCompatActivity implements View.OnClickListener {

    private TabLayout tabLayout;
    private ImageView ivBackArrow;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    private TextView tv_headername;
    private FloatingActionButton add_product_fab;
    private int currentposition=0;
    public static int viewpagercurrentposion=0;
    private CustomDialog mCustomDialog;
    private PreferenceManager preferenceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_productpage);
     //   getSupportActionBar().hide();
        initializeUi();
        initializeListeners();


        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to products activity");
        }

        getProductscount();

    }
    public void getProductscount() {

        mCustomDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(MainProductpage.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, VENDOR_PRODUCTS_COUNT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        mCustomDialog.dismiss();
                        if (response != null) {
                            Log.d("Productscount_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  myproducts "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");

                                if (status) {
                                    try {
                                        //JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        String catalogue_count=dataObject.getString("catalogue_count");
                                        String inventory_instock_count=dataObject.getString("inventory_instock_count");
                                        String inventory_outofstock_count=dataObject.getString("inventory_outofstock_count");
                                        String pendig_count=dataObject.getString("pendig_count");
                                        String approved_count=dataObject.getString("approved_count");

                                        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),MainProductpage.this,catalogue_count,
                                                inventory_instock_count,inventory_outofstock_count,pendig_count,approved_count);
                                        viewPager.setAdapter(viewPagerAdapter);
                                        viewPager.setCurrentItem(currentposition);

                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch myproduct  "+e.getMessage());
                                        e.printStackTrace();
                                    }
                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(MainProductpage.this, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomDialog.dismiss();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, preferenceManager.getString("USER_TOKEN"));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void initializeUi() {
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        ivBackArrow = findViewById(R.id.ivBackArrow);
        tv_headername = findViewById(R.id.tv_headername);
        add_product_fab = findViewById(R.id.add_product_fab);
        mCustomDialog=new CustomDialog(MainProductpage.this);
        preferenceManager=new PreferenceManager(MainProductpage.this);
        viewPager = findViewById(R.id.viewPager);

        currentposition=getIntent().getIntExtra("currentposition",0);

        tabLayout.setupWithViewPager(viewPager);





        tv_headername.setText("Products");


    }

    private void initializeListeners() {
        ivBackArrow.setOnClickListener(this);
        add_product_fab.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ivBackArrow:
                onBackPressed();
                break;
            case R.id.add_product_fab:
                Intent i=new Intent(MainProductpage.this, ProductAddOrUpdate.class);
                i.putExtra("currentposition",viewPager.getCurrentItem());
                startActivity(i);
                finish();
                break;
            default:
                break;
        }
    }
}