package com.nextclick.crm.productDetails.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Config.Config;
import com.nextclick.crm.Constants.Constants;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductAdd;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.Utilities.mixpanel.MyMixPanel;
import com.nextclick.crm.productDetails.adapters.ProductUpdateAdapter;
import com.nextclick.crm.productDetails.adapters.SelectedImageAdapter;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.BRANDS;
import static com.nextclick.crm.Config.Config.PRODUCTDETAILS;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Config.Config.UPDATEVARIENT;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class ProductUpdateActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView back_image,upload_image;
    private Button gotohomepage;
    private LinearLayout layout_itemtype;
    TextInputEditText product_name,product_desc;
    private Spinner category_spinner,menu_spinner,brands_spinner,item_type_spinner;
    private RecyclerView images_recycle;
    private SelectedImageAdapter selectedImageAdapter;
    private RecyclerView recycle_views;
    ArrayList<ProductAdd> productaddlist;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private String userChoosenTask,product_id,previousActivity;
    private String product_name_str, product_desc_str, base64image="",
            token, sub_cat_id = null, menu_id = null,brand_id=null, product_price_str, product_quantity_str, product_discount_str,product_option_str;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> taxCategoryList =null;
    private ArrayList<String> taxCategoryIDList=null;
    private ArrayList<String> catIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private ArrayList<String> brandlist = new ArrayList<>();
    private ArrayList<String> brandIDList = new ArrayList<>();
    private int status_type = 1;
    private Context mContext;
    PreferenceManager preferenceManager;
    private final int productAvailability = 1;
    private boolean isImageSelected;
    private int currentposition, sectionid;
    private ProductUpdateAdapter productUpdateAdapter;
    private ArrayList<ImagesModel> imageslist;
    private ArrayList<String> itemtypelist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_product_update_new);
      //  Objects.requireNonNull(getSupportActionBar()).hide();

        init();
        prepareDetails();
        prepareSpinnerDetails();




        if (MyMixPanel.isMixPanelSupport) {
            MyMixPanel.logEvent("Vendor navigated to update the product");
        }
        previousActivity=getIntent().getStringExtra("previous_activity");
    }
    private CustomDialog customDialog;

    public void init(){
        mContext= ProductUpdateActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        customDialog=new CustomDialog(mContext);

        back_image=findViewById(R.id.back_image);
        gotohomepage=findViewById(R.id.gotohomepage);
       // banner_image=findViewById(R.id.banner_image);
        product_name=findViewById(R.id.product_name);
        product_desc=findViewById(R.id.product_desc);
        category_spinner=findViewById(R.id.category_spinner);
        menu_spinner=findViewById(R.id.menu_spinner);
        recycle_views=findViewById(R.id.recycle_views);
        brands_spinner=findViewById(R.id.brands_spinner);
        layout_itemtype=findViewById(R.id.layout_itemtype);
        item_type_spinner=findViewById(R.id.item_type_spinner);

        images_recycle = findViewById(R.id.images_recycle);
        upload_image=findViewById(R.id.upload_image);

        product_name.setEnabled(false);
        product_name.setClickable(false);
        product_desc.setEnabled(false);
        product_desc.setClickable(false);


        recycle_views.setLayoutManager(new GridLayoutManager(this,1));

        productaddlist=new ArrayList<>();
        back_image.setOnClickListener(this::onClick);
        gotohomepage.setOnClickListener(this::onClick);
       // banner_image.setOnClickListener(this::onClick);
        currentposition=getIntent().getIntExtra("currentposition",0);
        productUpdateAdapter=new ProductUpdateAdapter(ProductUpdateActivity.this,productaddlist);
        recycle_views.setAdapter(productUpdateAdapter);

        selectedImageAdapter = new SelectedImageAdapter(this, imageslist);
        selectedImageAdapter.collapseCancelButton= true;
        images_recycle.setAdapter(selectedImageAdapter);

        try {
            Thread thread = new Thread();
            getCategories();
            getTaxes();
            /*thread.sleep(1000);
            getMenus();*/
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        images_recycle.setLayoutManager(layoutManager);

        itemtypelist=new ArrayList<>();
        itemtypelist.add("Veg");
        itemtypelist.add("Non Veg");
        itemtypelist.add("Others");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, itemtypelist);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        item_type_spinner.setAdapter(adapter);
        item_type_spinner.setSelection(2);

    }
    private void prepareDetails() {
        try {
            String statusText = getIntent().getStringExtra("type");
            System.out.println("aaaaaaaa type "+statusText);
          //  if (statusText != null) {

                    status_type = 2;
                    LoadingDialog.loadDialog(mContext);
                     product_id=getIntent().getStringExtra("product_id");
                    getProductToUpdate(product_id);

          //  }
        } catch (Exception e) {
            System.out.println("aaaaaaaa cathc "+e.getMessage());
            e.printStackTrace();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.gotohomepage:
                /*if(previousActivity!=null && previousActivity.equals("OrdersFragment")) {
                    getFragmentManager().popBackStack();
                }
                else {
                    if(Constants.Use_ActivityDesign) {
                        Intent intent = new Intent(ProductUpdateActivity.this, MainProductpage.class);
                        intent.putExtra("currentposition", currentposition);
                        startActivity(intent);
                    }
                    else
                    {
                        getFragmentManager().popBackStack();
                    }
                }*/
               /* Intent i=new Intent(ProductUpdateActivity.this,MainProductpage.class);
                startActivity(i);*/
                Utility.check_product_page=true;
                finish();
                break;
            case R.id.back_image:
              /*  if(previousActivity!=null && previousActivity.equals("OrdersFragment")) {
                    *//*Intent intent = new Intent(ProductUpdateActivity.this, OrdersFragment.class);//OrdersFragment
                    startActivity(intent);*//*
                    getFragmentManager().popBackStack();
                }
                else {
                    if(Constants.Use_ActivityDesign) {
                        Intent intent = new Intent(ProductUpdateActivity.this, MainProductpage.class);
                        intent.putExtra("currentposition", currentposition);
                        startActivity(intent);
                    }
                    else
                    {
                        getFragmentManager().popBackStack();
                    }
                }*/
                Utility.check_product_page=true;
                finish();
                break;
            case R.id.banner_image:
                selectImage();
                break;

        }
    }
    private void prepareSpinnerDetails() {

        category_spinner.setClickable(false);
        category_spinner.setEnabled(false);
        menu_spinner.setClickable(false);
        menu_spinner.setEnabled(false);
        brands_spinner.setClickable(false);
        brands_spinner.setEnabled(false);
        item_type_spinner.setClickable(false);
        item_type_spinner.setEnabled(false);

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    LoadingDialog.loadDialog(mContext);
                    getMenus(sub_cat_id);
                    LoadingDialog.dialog.dismiss();
                } else {
                    sub_cat_id = null;
                    menu_id = null;
                    menuIDList.clear();
                    menusList.clear();
                    menu_spinner.setAdapter(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SuspiciousIndentation")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                    if(catIDList.size()> position-1)
                    sub_cat_id = catIDList.get(position - 1);
                   // LoadingDialog.loadDialog(mContext);
                  //  getBrands(sub_cat_id);
                  //  LoadingDialog.dialog.dismiss();
                } else {
                    menu_id = null;
                    sub_cat_id = null;
                    menu_id = null;
                    menuIDList.clear();
                    menusList.clear();
                    menu_spinner.setAdapter(null);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        brands_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               /* try{
                    if (!brands_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                        brand_id = brandIDList.get(position - 1);
                    } else {
                        brand_id = null;
                        brands_spinner.setAdapter(null);
                    }
                }catch (ArrayIndexOutOfBoundsException e){

                }*/


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void getCategories() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("cat_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        catIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {

                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                            catIDList.add(categoryObject.getString("cat_id"));

                                        }

                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        category_spinner.setAdapter(adapter);


                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                        System.out.println("aaaaaaa  111 "+error.getMessage());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getTaxes() {

        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Config.GET_TAXES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("tax_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        taxCategoryList = new ArrayList<>();
                                        taxCategoryIDList = new ArrayList<>();
                                        taxCategoryList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            taxCategoryIDList.add(categoryObject.getString("id"));
                                            taxCategoryList.add(categoryObject.getString("name"));
                                        }
                                        updateTaxListToProductList();
                                    }
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void updateTaxListToProductList() {
        if (taxCategoryList != null && productaddlist != null && productaddlist.size() > 0) {
            for (int i = 0; i < productaddlist.size(); i++) {
                ProductAdd product = productaddlist.get(i);
                product.setTaxesList(taxCategoryList,taxCategoryIDList);
            }
        }
    }

    private void getProductToUpdate(String product_id) {
        System.out.println("aaaaa productid  " + product_id);
        customDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PRODUCTDETAILS + product_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response != null) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    System.out.println("aaaaaaa jsonobject  " + jsonObject);
                                    boolean status = jsonObject.getBoolean("status");
                                    if (status) {
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        int productStatus = dataObject.getInt("status");
                                   /* if (productStatus == 1) {
                                        switchCompact.setChecked(true);
                                    } else {
                                        switchCompact.setChecked(false);
                                    }*/
                                        product_name.setText(dataObject.getString("name"));
                                        product_desc.setText(Html.fromHtml(dataObject.getString("desc")));
                                        try{

                                            item_type_spinner.setSelection(Integer.parseInt(dataObject.getString("item_type"))-1);
                                            layout_itemtype.setVisibility(View.VISIBLE);
                                        }catch (Exception e){
                                            layout_itemtype.setVisibility(View.GONE);
                                        }

                                   /* Picasso.get()
                                            .load(dataObject.getString("product_image"))
                                            .placeholder(R.drawable.loader_gif)
                                            .into(banner_image);*/
                                        //  product_quantity.setText(dataObject.getString("quantity"));
                                        //   product_desc.setText(Html.fromHtml(dataObject.getString("desc")).toString());
                                        //  product_price.setText(dataObject.getString("price"));

                                        JSONArray sectionsarray = dataObject.getJSONArray("sections");
                                        JSONObject sectionobject = sectionsarray.getJSONObject(0);
                                        sectionid = sectionobject.getInt("id");

                                        imageslist = new ArrayList<>();
                                        try {
                                            JSONArray imagesArray = dataObject.getJSONArray("item_images");
                                            for (int i = 0; i < imagesArray.length(); i++) {
                                                JSONObject jsonObject1 = imagesArray.getJSONObject(i);
                                                String id = jsonObject1.getString("id");
                                                // String image=jsonObject1.getString("image");
                                                System.out.println("aaaaaaa image " + id + " " + jsonObject1.getString("image"));
                                                ImagesModel imagesModel = new ImagesModel();
                                                imagesModel.setId(id);
                                                imagesModel.setImage(jsonObject1.getString("image"));
                                                imageslist.add(imagesModel);
                                           /* Picasso.get()
                                                    .load(jsonObject1.getString("image"))
                                                    .placeholder(R.drawable.loader_gif)
                                                    .into(banner_image);*/
                                            }

                                            if (imageslist.size() == 0) {
                                                images_recycle.setVisibility(View.GONE);
                                                upload_image.setVisibility(View.VISIBLE);
                                            } else {
                                                images_recycle.setVisibility(View.VISIBLE);
                                                upload_image.setVisibility(View.GONE);
                                                selectedImageAdapter.refresh(imageslist);
                                            }
                                        } catch (Exception e) {
                                            System.out.println("aaaaaa catch img " + e.getMessage());
                                        }

                                        JSONArray sectionarray = dataObject.getJSONArray("vendor_product_varinats");
                                        productaddlist.clear();
                                        for (int i = 0; i < sectionarray.length(); i++) {
                                            JSONObject jsonObject1 = sectionarray.getJSONObject(i);

                                       /* if (i==0){
                                            product_price.setText(jsonObject1.getString("price"));
                                            product_option.setText(jsonObject1.getString("name"));
                                            product_option.setText(jsonObject1.getString("variant_id"));
                                        }else {*/
                                            ProductAdd productAdd = new ProductAdd();
                                            productAdd.setPrice(jsonObject1.getString("price"));
                                            productAdd.setSizecolor(jsonObject1.getString("section_item_name"));
                                            productAdd.setId(jsonObject1.getString("id"));
                                            productAdd.setItem_id(jsonObject1.getString("item_id"));
                                            productAdd.setStatus(jsonObject1.getInt("status"));

                                            productAdd.setSku(jsonObject1.getString("sku"));
                                            productAdd.setDiscount(jsonObject1.getString("discount"));
                                            productAdd.setStock(jsonObject1.getString("stock"));
                                            productAdd.setWeight(jsonObject1.getString("weight"));



                                            try{
                                                productAdd.setReturn_available(jsonObject1.getInt("return_available"));
                                            }catch (Exception e){
                                                productAdd.setReturn_available(0);
                                            }

                                            try {
                                                //if (jsonObject1.getString("tax_id") != null)
                                                // productAdd.setSelectedTaxID(jsonObject1.getString("tax_id"));
                                                JSONObject taxObject = jsonObject1.getJSONObject("tax");
                                                if (taxObject != null) {
                                                    productAdd.setSelectedTaxID(taxObject.getString("id"));
                                                    productAdd.setSelectedTaxTypeID(taxObject.getString("type_id"));

                                                    //JSONObject taxTypeObject = taxObject.getJSONObject("tax_type");
                                                }

                                            } catch (Exception ex) {

                                            }
                                            try{
                                                JSONObject returnobj=jsonObject1.getJSONObject("returns");
                                                productAdd.setReturn_id(returnobj.getString("id"));
                                                productAdd.setReturn_days(returnobj.getString("return_days"));
                                                productAdd.setReturn_terms_conditions(returnobj.getString("terms_conditions"));

                                            }catch (Exception e){

                                            }
                                            productaddlist.add(productAdd);
                                            //   }
                                        }

                                        updateTaxListToProductList();

                                        //  product_option.setText(productaddlist.get(0).getSizecolor());

                                  /*  int item_type = dataObject.getInt("item_type");
                                    if (item_type == 1) {
                                        veg_radio_btn.setChecked(true);
                                    }
                                    if (item_type == 2) {
                                        non_veg_radio_btn.setChecked(true);
                                    }*/
                                        try {
                                            menu_id = dataObject.getJSONObject("menu").getString("id");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            Thread thread = new Thread();
                                            Thread.sleep(1000);
                                            for (int i = 0; i < categoryIDList.size(); i++) {
                                                if (dataObject.getJSONObject("sub_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))) {
                                                    sub_cat_id = dataObject.getJSONObject("sub_category").getString("id");
                                                    category_spinner.setSelection((i + 1), true);
                                                    getMenus(sub_cat_id);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            brand_id = dataObject.getString("brand_id");
                                            Thread thread = new Thread();
                                            Thread.sleep(1000);
                                            // for (int i = 0; i < catIDList.size(); i++) {
                                            //   if (dataObject.getJSONObject("brand").getString("id").equalsIgnoreCase(catIDList.get(i))) {
                                            brand_id = dataObject.getJSONObject("brand").getString("id");
                                            String brand_name = dataObject.getJSONObject("brand").getString("name");
                                            brandlist.add(brand_name);
                                            brandIDList.add(brand_id);
                                            // getBrands(catIDList.get(i));
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, brandlist);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            brands_spinner.setAdapter(adapter);
                                            //  }
                                            //   }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        getreturnpolicy();
                                     //   productUpdateAdapter.setrefresh(productaddlist, 1);
                                    } else {
                                        UIMsgs.showToast(mContext, OOPS);
                                        System.out.println("aaaaaaa  222 ");
                                    }


                                } catch (Exception e) {
                                    System.out.println("aaaaaa catch  " + e.getMessage());
                                    System.out.println("aaaaaaa  333 " + e.getMessage());
                                    e.printStackTrace();
                                    UIMsgs.showToast(mContext, OOPS);
                                }

                            } else {
                                UIMsgs.showToast(mContext, MAINTENANCE);
                            }
                        } catch (Exception ex) {
                            UIMsgs.showToast(mContext, ex.getLocalizedMessage());

                        } finally {

                            customDialog.cancel();
                        }

                        // LoadingDialog.dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //LoadingDialog.dialog.dismiss();
                customDialog.cancel();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    private void getMenus(String sub_cat_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("menu_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        menu_spinner.setAdapter(adapter);

                                        try {
                                            if (menu_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    Thread.sleep(1000);
                                                    for (int i = 0; i < menuIDList.size(); i++) {
                                                        if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                            menu_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("NA");
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {

                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa  444 "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getBrands(String sub_cat_id) {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BRANDS + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("brands_res", response);

                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                   // JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    JSONObject dataArray = jsonObject.getJSONObject("data").getJSONObject("menus");
                                    if (dataArray.length() > 0) {
                                        brandlist = new ArrayList<>();
                                        brandIDList = new ArrayList<>();
                                        brandlist.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(""+i);
                                            brandlist.add(categoryObject.getString("name"));
                                            brandIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        brands_spinner.setAdapter(adapter);

                                        try {
                                            if (menu_id != null) {
                                                try {
                                                    Thread thread = new Thread();
                                                    Thread.sleep(1000);
                                                    for (int i = 0; i < menuIDList.size(); i++) {
                                                        if (menu_id.equalsIgnoreCase(menuIDList.get(i))) {
                                                            menu_spinner.setSelection((i + 1), true);
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaaa  555 "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ProductUpdateActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(ProductUpdateActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }
    public String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                base64image=convert(bm);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);
        isImageSelected=true;
        //banner_image.setImageBitmap(bm);
        base64image=convert(bm);

    }
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isImageSelected=true;
        //banner_image.setImageBitmap(thumbnail);
        base64image=convert(thumbnail);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length/1024;

        System.out.println("aaaaaaaaaa  size "+lengthbmp);

    }

    public void setsaveVarient(ProductAdd productAdd) {
        System.out.println("aaaaaaaaaaa  update product");
        Map<String, String> uploadMap = new HashMap<>();
        uploadMap.put("variant_id", productAdd.getId());
        uploadMap.put("price", productAdd.getPrice());
        uploadMap.put("stock", "" + productAdd.getStock());
        uploadMap.put("discount", productAdd.getDiscount());
        uploadMap.put("status", "" + productAdd.getStatus());
        uploadMap.put("weight", "" + productAdd.getWeight());
        uploadMap.put("return_id", "" + productAdd.getReturn_id());
        uploadMap.put("return_available", "" + productAdd.getReturncheck());
        if (productAdd.getSelectedTaxID() != null)
            uploadMap.put("tax_id", "" + productAdd.getSelectedTaxID());
        if (productAdd.getOptionlist() != null) {
            StringBuilder str = new StringBuilder();
            for (String option : productAdd.getOptionlist()) {
                if(!option.isEmpty())
                    str.append(option).append(",");
            }
            if (str.length() > 0) {
                uploadMap.put("option_name",  str.substring(0, str.length() - 1));
            }
        }

        JSONObject json = new JSONObject(uploadMap);

        String data = json.toString();
        System.out.println("aaaaaaaa  data  " + data);
        createOrUpdateCategory(data);
    }

    private void createOrUpdateCategory(final String data) {

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATEVARIENT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                          //  Log.d("cc_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaa  jsonobject  "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    System.out.println("aaaaaaaaaaa  update product status ");
                                    Toast.makeText(mContext, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                  //  UIMsgs.showToast(mContext, "Updated Successfully");
                                 //   getProductToUpdate(product_id);

                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch  "+e.getMessage());
                                LoadingDialog.dialog.cancel();
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("aaaaaaa error  "+error.getMessage());
                System.out.println("aaaaaaa  666 "+error.getMessage());
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                System.out.println("aaaaaaaa tokrn  "+token);
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    public void getreturnpolicy(){
        String url=Config.GET_REURNS_DATA+"?sub_cat_id="+sub_cat_id+"&menu_id="+menu_id;
        System.out.println("aaaaaaaa url  "+url);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("return_res", response);
                        if (response != null) {
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONObject jsonObject1=jsonObject.getJSONObject("data");

                                    for (int i=0;i<productaddlist.size();i++){
                                        productaddlist.get(i).setReturn_id(jsonObject1.getString("id"));
                                        productaddlist.get(i).setReturn_days(jsonObject1.getString("return_days"));
                                        productaddlist.get(i).setReturn_terms_conditions(jsonObject1.getString("terms_conditions"));

                                    }
                                    String days=jsonObject1.getString("return_days");
                                    String terms_conditions=jsonObject1.getString("terms_conditions");
                                    String return_id=jsonObject1.getString("id");
                                    productUpdateAdapter.setrefresh(productaddlist, 1);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                                productUpdateAdapter.setrefresh(productaddlist, 1);

                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    public void setreturn(boolean check) {
        if (check){

        }else{

        }
    }
}