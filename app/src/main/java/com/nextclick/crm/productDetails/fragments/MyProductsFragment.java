package com.nextclick.crm.productDetails.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ProductAddOrUpdate;
import com.nextclick.crm.ShopNowModule.Models.ImagesModel;
import com.nextclick.crm.ShopNowModule.Models.ProductModel;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.dialogs.CategoryMenuFilter;
import com.nextclick.crm.interfaces.CategoryMenuSelection;
import com.nextclick.crm.productDetails.activities.MainProductpage;
import com.nextclick.crm.productDetails.adapters.ProductsAdapter;
import com.nextclick.crm.productDetails.model.MyProductsModel;
import com.nextclick.crm.productDetails.model.VendorVarients;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.MYPRODUCTLIST;
import static com.nextclick.crm.Config.Config.SHOP_BY_CATEGORY_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class MyProductsFragment extends Fragment implements View.OnClickListener, CategoryMenuSelection {

    View view;
    private RecyclerView recycle_catelogue;
    private TextView tvError,tv_instock,tv_outstock;
    private EditText etSearch;
    private ImageView ivSearch,ivFilter;
    private Button apply_;
    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;
    private ArrayList<String> menusList = new ArrayList<>();
    private ArrayList<String> menuIDList = new ArrayList<>();
    private Spinner filetr_menu_spinner, filter_category_spinner;
    private ProductsAdapter productsAdapter;
    private Context mContext;
    private boolean isScrolled;
    private boolean isScrolling = false;
    private ArrayList<ProductModel> productModelsList = new ArrayList<>();

    private LinearLayoutManager layoutManager;
    private PreferenceManager preferenceManager;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private int count = 0;
    private int totalItems;
    private int page_no = 1;
    private int currentItems;
    private int scrollOutItems;
    private CustomDialog  mCustomDialog;
    public static int status_count = 0;
    private String token = "";
    private String menu_id = "";
    private String sub_cat_id = "";
    private String enteredText = "";
    String stock_type="instock";
    MainProductpage mainProductpage;
    boolean isViewCrated;
    private int previousOffset;
    private LinearLayout layout_instock,layout_outstock;
    private boolean checkstock=false,checkproducts=false;
    TextView tv_add_product;
    String inventory_instock_count,inventory_outofstock_count;
    public MyProductsFragment(MainProductpage mainProductpage, String inventory_instock_count, String inventory_outofstock_count) {
        this.mainProductpage=mainProductpage;
        this.inventory_instock_count=inventory_instock_count;
        this.inventory_outofstock_count=inventory_outofstock_count;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_catolog_products, container, false);
        init();
        prepareSpinnerItemsClicks();

        if (!isViewCrated || Utility.RefreshMyProducts || productModelsList.size() == 0) {
            System.out.println("aaaaaaaaa check oncreate ");
            checkproducts=true;
            stock_type="instock";
            Utility.RefreshMyProducts = false;
            page_no=1;
            getProducts(1,stock_type);
        }
       // getCategories();
        adapterSetter();
        isViewCrated = true;

        return view;
    }
    public void init(){
        mContext = getActivity();
        recycle_catelogue=view.findViewById(R.id.recycle_catelogue);

        ivFilter = view.findViewById(R.id.ivFilter);
        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              openCategoryMenuFilter();
            }
        });

        tvError=view.findViewById(R.id.tvError);
        apply_ = view.findViewById(R.id.apply_p);
        etSearch = view.findViewById(R.id.etSearch);
        ivSearch = view.findViewById(R.id.ivSearch);
        filetr_menu_spinner = view.findViewById(R.id.filetr_menu_spinner);
        filter_category_spinner = view.findViewById(R.id.filter_category_spinner);
        layout_instock = view.findViewById(R.id.layout_instock);
        layout_outstock = view.findViewById(R.id.layout_outstock);
        tv_instock = view.findViewById(R.id.tv_instock);
        tv_outstock = view.findViewById(R.id.tv_outstock);

        tv_add_product = view.findViewById(R.id.tv_add_product);
        tv_add_product.setOnClickListener(this);
        mCustomDialog=new CustomDialog(mContext);
        preferenceManager=new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        recycle_catelogue.setLayoutManager(new GridLayoutManager(getContext(),1));

        ivSearch.setOnClickListener(this::onClick);
        apply_.setOnClickListener(this::onClick);

        mSwipeRefreshLayout =view.findViewById(R.id.swipeToRefresh);
        // Configure the refreshing colors
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        /*mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (checkstock){
                    page_no=1;
                    getProducts(1,stock_type);
                }else {
                    page_no=1;
                    getProducts(1,stock_type);
                }

            }
        });*/

        tv_instock.setText(mContext.getResources().getString(R.string.in_stock)+"("+inventory_instock_count+")");
        tv_outstock.setText(mContext.getResources().getString(R.string.out_of_stock)+"("+inventory_outofstock_count+")");

        NestedScrollView nestedSV = view.findViewById(R.id.idNestedSV);
        nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    // on scroll change we are checking when users scroll as bottom.
                    if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) {
                       // loadingPB.setVisibility(View.VISIBLE);
                        currentItems = layoutManager.getChildCount();
                        totalItems = layoutManager.getItemCount();
                        scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                        System.out.println("onScrollChange onScrollChange  "+currentItems + scrollOutItems+"   "+totalItems);
                        if ((currentItems + scrollOutItems == totalItems)) {
                            isScrolled = true;
                            isScrolling = false;
                            count++;
                            boolean isContinue = true;
                            if (scrollY >= previousOffset)
                                ++page_no;
                            else if (page_no > 1)//min range
                            {
                                //--page_no;
                                isContinue = false;
                            }
                            else {
                                isContinue = false;//comment this if you encounter any problem
                            }
                            previousOffset = scrollY;
                            System.out.println("onScrollChange onScrollChange  isContinue "+isContinue+", page_no "+page_no);
                            if (isContinue) {
                                getProducts(0, stock_type);
                            }
                        }
                    }
                }
            });

      /*  recycle_catelogue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                System.out.println("aaaaaaaaa currentitem  "+currentItems + scrollOutItems+"   "+totalItems);
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolled = true;
                    isScrolling = false;
                    count++;
                    boolean isContinue = true;
                    if (dy > previousOffset)
                        ++page_no;
                    else if (page_no > 1)//min range
                    {
                        //--page_no;
                        isContinue = false;
                    }
                    else {
                        isContinue = false;//comment this if you encounter any problem
                    }
                    previousOffset = dy;
                    if (isContinue)
                        getProducts(0,stock_type);
                }
            }
        });*/
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                enteredText = charSequence.toString();

               /* if (enteredText.isEmpty()) {
                    page_no = 1;
                    count = 0;
                    recycle_catelogue.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    adapterSetter();
                    getProducts();
                }*/
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        layout_outstock.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                productModelsList.clear();
                productsAdapter.notifyDataSetChanged();
                tv_outstock.setTextColor(mContext.getColor(R.color.white));
                tv_instock.setTextColor(mContext.getColor(R.color.black));
                layout_instock.setBackground(null);
                layout_outstock.setBackground(null);
                layout_instock.setBackgroundColor(mContext.getColor(R.color.gray));
               // layout_outstock.setBackgroundColor(mContext.getColor(R.color.colorPrimary));
                layout_outstock.setBackgroundDrawable(mContext.getDrawable(R.drawable.blue_gradient_bg));
                checkstock=true;
                page_no=1;
                stock_type="outofstock";
                getProducts(1,stock_type);

            }
        });
        layout_instock.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                tv_outstock.setTextColor(mContext.getColor(R.color.black));
                tv_instock.setTextColor(mContext.getColor(R.color.white));
                layout_instock.setBackground(null);
                layout_outstock.setBackground(null);
                layout_outstock.setBackgroundColor(mContext.getColor(R.color.gray));
                //layout_instock.setBackgroundColor(mContext.getColor(R.color.colorPrimary));
                layout_instock.setBackgroundDrawable(mContext.getDrawable(R.drawable.blue_gradient_bg));
                checkstock=false;
                page_no=1;
                stock_type="instock";
                getProducts(1,stock_type);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (status_count == 1) {
            stock_type="instock";
            getProducts(1,stock_type);
            status_count = 0;
        }*/
        System.out.println("aaaaaaaaaaa check onresume ");
        tv_outstock.setTextColor(mContext.getResources().getColor(R.color.black));
        tv_instock.setTextColor(mContext.getResources().getColor(R.color.white));
        layout_instock.setBackground(null);
        layout_outstock.setBackground(null);
        layout_outstock.setBackgroundColor(mContext.getResources().getColor(R.color.gray));
        //layout_instock.setBackgroundColor(mContext.getColor(R.color.colorPrimary));
        layout_instock.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.blue_gradient_bg));
        checkstock=false;

        if (!checkproducts){

            stock_type="instock";
            System.out.println("aaaaaaaaaaa check onresume 1111  ");
            Utility.RefreshMyProducts = false;
            page_no=1;
            getProducts(1,stock_type);
        }
       // if (!isViewCrated || Utility.RefreshMyProducts || productModelsList.size() == 0) {

      //  }
    }


    private void prepareSpinnerItemsClicks() {
        filter_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filter_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                    menusList.clear();
                    menuIDList.clear();
                    filetr_menu_spinner.setAdapter(null);
                    getMenus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        filetr_menu_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!filetr_menu_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    menu_id = menuIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
    private void adapterSetter() {
        productsAdapter = new ProductsAdapter(MyProductsFragment.this,mContext, productModelsList,0,mainProductpage,stock_type);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                    private static final float SPEED = 300f;// Change this value (default=25f)

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };

        recycle_catelogue.setLayoutManager(layoutManager);
        recycle_catelogue.setAdapter(productsAdapter);
    }
    public void changeproduct(VendorVarients productsModel,String stock,String stocktype){
       /* for (int i=0;i<productModelsList.size();i++){
            for (int j=0;j<productModelsList.get(i).getVendorVarientslist().size();j++){
                if (productModelsList.get(i).getVendorVarientslist().get(j).getId().equalsIgnoreCase(productsModel.getId())){
                    productModelsList.get(i).getVendorVarientslist().get(j).setStock(stock);
                }
            }
        }
         productsAdapter.notifyDataSetChanged();
         */
        page_no=1;
        getProducts(1,stocktype);

    }
    public void getProducts(int i,String stock_type) {
        if (i==1){
            productModelsList.clear();
        }
        System.out.println("aaaaaaaaa page number  "+page_no);
        mCustomDialog.show();
        int limit =10;
        int offset =((page_no-1)* limit);
        Map<String, String> dataMap = new HashMap<>();
        //dataMap.put("page_no", page_no + "");
        dataMap.put("limit",  limit+ "");
        dataMap.put("offset", offset  + "");
        dataMap.put("q", enteredText);
        dataMap.put("menu_id", menu_id);
        dataMap.put("shop_by_cat_id", sub_cat_id);
        dataMap.put("stock_type", stock_type);
       // dataMap.put("status", APPROVEDSTATUS);
        final String data = new JSONObject(dataMap).toString();
        System.out.println("aaaaaaaa  data "+data+" api "+MYPRODUCTLIST);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, MYPRODUCTLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        mCustomDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (response != null) {
                            Log.d("Product_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  myproducts "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");
                                System.out.println("aaaaaa  isScrolled "+isScrolled);
                                if (status) {
                                    try {
                                        //JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("result");
                                        JSONObject dataObject = jsonObject.getJSONObject("data");
                                        Object aObj = dataObject.get("result");
                                        if (!(aObj instanceof JSONArray) || aObj instanceof JSONObject) {
                                            if (page_no > 1)
                                                page_no--;
                                            System.out.println("getProducts responsed failed - MyProducts");
                                            return;
                                        }
                                        JSONArray dataArray =(JSONArray) aObj;
                                        if (dataArray.length() > 0) {
                                            listIsFull();
                                            for (int i = 0; i < dataArray.length(); i++) {
                                                JSONObject productObject = dataArray.getJSONObject(i);

                                                MyProductsModel myProductsModel=new MyProductsModel();

                                              /*  myProductsModel.setId(productObject.getString("id"));
                                                myProductsModel.setSub_cat_id(productObject.getString("sub_cat_id"));
                                                myProductsModel.setMenu_id(productObject.getString("menu_id"));
                                                myProductsModel.setBrand_id(productObject.getString("brand_id"));
                                                myProductsModel.setProduct_code(productObject.getString("product_code"));
                                                myProductsModel.setName(productObject.getString("name"));
                                                myProductsModel.setDesc(productObject.getString("desc"));
                                                myProductsModel.setQuantity(productObject.getString("quantity"));
                                                myProductsModel.setPrice(productObject.getString("price"));
                                                myProductsModel.setDiscount(productObject.getString("discount"));
                                                myProductsModel.setItem_type(productObject.getString("item_type"));
                                                myProductsModel.setSounds_like(productObject.getString("sounds_like"));
                                                myProductsModel.setCreated_user_id(productObject.getString("created_user_id"));
                                                myProductsModel.setUpdated_user_id(productObject.getString("updated_user_id"));
                                                myProductsModel.setUpdated_user_id(productObject.getString("created_at"));
                                                myProductsModel.setUpdated_at(productObject.getString("updated_at"));
*/

                                                ProductModel productModel = new ProductModel();
                                                productModel.setId(productObject.getString("id"));
                                                productModel.setSub_cat_id(productObject.getString("sub_cat_id"));
                                                productModel.setMenu_id(productObject.getString("menu_id"));
                                                productModel.setProduct_code(productObject.getString("product_code"));
                                                productModel.setName(productObject.getString("name"));
                                                productModel.setDesc(Html.fromHtml(productObject.getString("desc")).toString());
                                                //  productModel.setQuantity(productObject.getInt("quantity"));
                                                //  productModel.setPrice(productObject.getInt("price"));
                                                //  productModel.setDiscount(productObject.getInt("discount"));
                                                productModel.setItem_type(productObject.getString("item_type"));
                                                productModel.setStatus(productObject.getInt("status"));
                                              //  productModel.setProduct_image(productObject.getString("product_image"));
                                                ArrayList<ImagesModel> imageslist=new ArrayList<>();
                                                try {
                                                   /* JSONArray jsonArray=productObject.getJSONArray("item_images");
                                                    for (int k=0;k<jsonArray.length();k++){
                                                        JSONObject jsonObject1=jsonArray.getJSONObject(k);
                                                        ImagesModel imagesModel=new ImagesModel();
                                                        imagesModel.setId(jsonObject1.getString("id"));
                                                        imagesModel.setImage(jsonObject1.getString("image"));
                                                        imageslist.add(imagesModel);
                                                    }*/

                                                    String productImage = productObject.getString("image");
                                                    if(productImage!=null)
                                                    {
                                                        ImagesModel imagesModel=new ImagesModel();
                                                        imagesModel.setImage(productImage);
                                                        imageslist.add(imagesModel);
                                                    }

                                                    productModel.setImagelist(imageslist);
                                                }catch (Exception e){

                                                }
                                               // productModel.setAvailabilityStatus(productObject.getString("status"));

                                                try {
                                                    productModel.setAvailabilityStatus(productObject.getString("availability"));
                                                    productModel.setCategoryName(productObject.getJSONObject("sub_category").getString("name"));
                                                    productModel.setMenu_name(productObject.getJSONObject("menu").getString("name"));
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                try{
                                                    ArrayList<VendorVarients> varientslist = new ArrayList<>();
                                                    JSONObject productarray=productObject.getJSONObject("vendor_product_details");
                                                    JSONArray varientsarray=productarray.getJSONArray("vendor_product_varinats");
                                                    for (int j=0;j<varientsarray.length();j++){
                                                        JSONObject varintonj=varientsarray.getJSONObject(j);
                                                        VendorVarients vendorVarients=new VendorVarients();
                                                        vendorVarients.setId(varintonj.getString("id"));
                                                        vendorVarients.setItem_id(varintonj.getString("item_id"));
                                                        vendorVarients.setSection_id(varintonj.getString("section_id"));
                                                        vendorVarients.setSku(varintonj.getString("sku"));
                                                        vendorVarients.setPrice(varintonj.getString("price"));
                                                        vendorVarients.setStock(varintonj.getString("stock"));
                                                        vendorVarients.setDiscount(varintonj.getString("discount"));
                                                        vendorVarients.setList_id(varintonj.getString("list_id"));
                                                        vendorVarients.setStatus(varintonj.getString("status"));
                                                        vendorVarients.setVendor_user_id(varintonj.getString("vendor_user_id"));
                                                        JSONObject sectionobj=varintonj.getJSONObject("section_item_details");

                                                        vendorVarients.setSection_item_name(sectionobj.getString("name"));
                                                        vendorVarients.setWeight(sectionobj.getString("weight"));
                                                        try{
                                                            JSONObject jsonObject1=varintonj.getJSONObject("tax");
                                                            vendorVarients.setTaxid(jsonObject1.getString("id"));
                                                        }catch (Exception e){

                                                        }
                                                        vendorVarients.setVendor_user_id(varintonj.getString("vendor_user_id"));
                                                        varientslist.add(vendorVarients);
                                                    }
                                                    productModel.setVendorVarientslist(varientslist);
                                                }catch (Exception e1){
                                                    System.out.println("aaaaaaaaa catch e1  "+e1.getMessage());
                                                }

                                                productModelsList.add(productModel);
                                                productsAdapter.setchanged(stock_type);
                                                productsAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            if (!isScrolled) {
                                                listIsEmpty();
                                            }
                                        }
                                    } catch (Exception e) {
                                        System.out.println("aaaaaaa catch myproduct  "+e.getMessage()+"  "+isScrolled);
                                        e.printStackTrace();
                                        if (!isScrolled) {
                                            listIsEmpty();
                                        }
                                        if (count == 0) {
                                        //    UIMsgs.showToast(mContext, "Products Not Available");
                                        } else {
                                          //  UIMsgs.showToast(mContext, "You've reached the end");
                                        }
                                    }
                                } else {
                                    if (!isScrolled) {
                                        listIsEmpty();
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                                if (!isScrolled) {
                                    listIsEmpty();
                                }
                            }
                        } else {
                            UIMsgs.showToast(mContext, MAINTENANCE);
                            if (!isScrolled) {
                                listIsEmpty();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mCustomDialog.dismiss();
                        mSwipeRefreshLayout.setRefreshing(false);
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                        if (!isScrolled) {
                            listIsEmpty();
                        }
                    }
                }) {

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void listIsFull() {
        tvError.setVisibility(View.GONE);
        recycle_catelogue.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
    }
    private void listIsEmpty() {
        tvError.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        recycle_catelogue.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_add_product:
                Intent i=new Intent(mContext, ProductAddOrUpdate.class);
                i.putExtra("currentposition",0);
                startActivity(i);
                break;
            case R.id.apply_p:
                if (sub_cat_id.equalsIgnoreCase("select")||sub_cat_id.isEmpty()){
                    Toast.makeText(getContext(), "Please select Category", Toast.LENGTH_SHORT).show();
                }else if (menu_id.equalsIgnoreCase("")||menu_id.isEmpty()){
                    Toast.makeText(getContext(), "Please select menu", Toast.LENGTH_SHORT).show();
                }else {
                    page_no = 1;
                    count = 0;
                    recycle_catelogue.setAdapter(null);
                    productModelsList = new ArrayList<>();
                    getProducts(1,stock_type);
                    adapterSetter();
                }

                break;
            case R.id.ivSearch:
             if (enteredText.length()<3){
                 Toast.makeText(requireContext(),"Please enter minimjum 3 characters",Toast.LENGTH_SHORT).show();
             } else {
                 page_no = 1;
                 count = 0;
                 recycle_catelogue.setAdapter(null);
                 productModelsList = new ArrayList<>();
                 adapterSetter();
                 getProducts(1,stock_type);
             }
                break;
        }
    }
    private void getMenus() {
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R + sub_cat_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("menu_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("menus");
                                    if (dataArray.length() > 0) {
                                        menusList = new ArrayList<>();
                                        menuIDList = new ArrayList<>();
                                        menusList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            menusList.add(categoryObject.getString("name"));
                                            menuIDList.add(categoryObject.getString("id"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, menusList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filetr_menu_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("aaaaaa catch manu "+e.getMessage());
                                UIMsgs.showToast(mContext, "No menus available for the selected menu");
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error menu "+error.getMessage());
                        UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }
    private void getCategories() {
        Map<String, String> searchMap = new HashMap<>();
        searchMap.put("q", "");
        final String data = new JSONObject(searchMap).toString();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SHOP_BY_CATEGORY_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            Log.d("cat_res", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for (int i = 0; i < dataArray.length(); i++) {
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));
                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        filter_category_spinner.setAdapter(adapter);
                                    }
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaa catch cat "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("aaaaaa response empty ");
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("aaaaaa error cat "+error.getMessage());
                       // UIMsgs.showToast(mContext, OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                System.out.println("aaaaaaaa  cat "+data);
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void openCategoryMenuFilter() {
        new CategoryMenuFilter().showMenuCategoryFilter(mContext,this);
    }

    @Override
    public void onCategoryMenuSelection(String sub_cat_id, String menu_id) {
        this.sub_cat_id=sub_cat_id;
        this.menu_id=menu_id;
        page_no = 1;
        count = 0;
        previousOffset = 0;
        recycle_catelogue.setAdapter(null);
        productModelsList = new ArrayList<>();
        getProducts(1,stock_type);
        adapterSetter();
    }
}