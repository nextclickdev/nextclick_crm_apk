package com.nextclick.crm.OnDemandsModule.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nextclick.crm.R;

import java.util.ArrayList;

public class TimingListAdapter extends ArrayAdapter<String> {

    ArrayList<String> openingtimelist = new ArrayList<>();
    ArrayList<String> closingtimelist = new ArrayList<>();
    public TimingListAdapter(Context context, int resource, int id, ArrayList<String> openingtimelist, ArrayList<String> closingtimelist) {
        super(context, resource, id);
        this.openingtimelist = openingtimelist;
        this.closingtimelist = closingtimelist;

    }

    @Override
    public int getCount() {
        return openingtimelist.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.timing_list_supporter, null);
        TextView open = v.findViewById(R.id.in_time);
        open.setText(openingtimelist.get(position));
        TextView close = v.findViewById(R.id.out_time);
        close.setText(closingtimelist.get(position));

        /*TextView remove = (TextView) v.findViewById(R.id.remove);

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/


        return v;

    }
}
