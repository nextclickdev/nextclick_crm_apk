package com.nextclick.crm.OnDemandsModule.Fragments.HomeActivityFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.nextclick.crm.R;
import com.bumptech.glide.Glide;

public class HomeFragment extends Fragment {


    private View rootView;
    private ImageView service_gif;

    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView=inflater.inflate(R.layout.ondemands_home_fragment, container, false);

        service_gif = rootView.findViewById(R.id.service_gif);
        Glide.with(getActivity())
                .load(R.drawable.servicegiffy)
                .into(service_gif);

        return rootView;
    }
}