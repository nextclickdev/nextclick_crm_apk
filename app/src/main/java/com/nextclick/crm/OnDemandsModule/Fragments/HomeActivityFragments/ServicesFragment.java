package com.nextclick.crm.OnDemandsModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.OnDemandsModule.Activities.CreateAndUpdateOnDemandServiceActivity;
import com.nextclick.crm.OnDemandsModule.Adapters.OnDemandsServiceAdapter;
import com.nextclick.crm.OnDemandsModule.Models.OnDemandsServiceModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ODS_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;


public class ServicesFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private View root;
    private RecyclerView ondemands_service_recycler;
    private FloatingActionButton add_ondemands_service_fab;
    private String token;
    private ArrayList<OnDemandsServiceModel> onDemandsServiceModelArrayList;
    private final String searchString = " ";
    private SearchView service_searchview;
    OnDemandsServiceAdapter onDemandsServiceAdapter=null;

    public ServicesFragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_ondemands_services, container, false);
        initView();
        add_ondemands_service_fab.setOnClickListener(this);
        service_searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getCategories(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    onDemandsServiceAdapter.getFilter().filter(newText);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return false;
            }
        });
        return root;
    }

    private void initView() {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        ondemands_service_recycler = root.findViewById(R.id.ondemands_service_recycler);
        add_ondemands_service_fab = root.findViewById(R.id.add_ondemands_service_fab);
        service_searchview = root.findViewById(R.id.service_searchview);
        getCategories(searchString);
    }

    private void getCategories(String s) {

        Map<String,String> searchMap = new HashMap<>();
        searchMap.put("q",s);
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ODS_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ods_res",response);

                        if(response!=null){
                            LoadingDialog.dialog.dismiss();
                            try{

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if(dataArray.length()>0){
                                        onDemandsServiceModelArrayList = new ArrayList<>();
                                        for(int i=0; i<dataArray.length(); i++){
                                            OnDemandsServiceModel serviceModel = new OnDemandsServiceModel();
                                            JSONObject serviceObject = dataArray.getJSONObject(i);
                                            serviceModel.setId(serviceObject.getString("id"));
                                            serviceModel.setName(serviceObject.getString("name"));
                                            serviceModel.setOd_cat_id(serviceObject.getString("od_cat_id"));
                                            serviceModel.setDesc(serviceObject.getString("desc"));
                                            serviceModel.setService_duration(serviceObject.getString("service_duration"));
                                            serviceModel.setPrice(serviceObject.getInt("price"));
                                            try{
                                                serviceModel.setDiscount(serviceObject.getDouble("discount"));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            serviceModel.setStatus(serviceObject.getInt("status"));
                                            try {
                                                serviceModel.setImage(serviceObject.getString("image"));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            onDemandsServiceModelArrayList.add(serviceModel);
                                        }

                                        onDemandsServiceAdapter = new OnDemandsServiceAdapter(mContext, onDemandsServiceModelArrayList);

                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,  LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }

                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }

                                        };
                                        ondemands_service_recycler.setLayoutManager(layoutManager);
                                        ondemands_service_recycler.setAdapter(onDemandsServiceAdapter);


                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_ondemands_service_fab) {
            mContext.startActivity(new Intent(mContext, CreateAndUpdateOnDemandServiceActivity.class));
        }

    }
}