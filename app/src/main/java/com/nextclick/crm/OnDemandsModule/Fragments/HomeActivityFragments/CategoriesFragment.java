package com.nextclick.crm.OnDemandsModule.Fragments.HomeActivityFragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.OnDemandsModule.Activities.CreateAndUpdateOnDemandCategoryActivity;
import com.nextclick.crm.OnDemandsModule.Adapters.OnDemandsCategoryAdapter;
import com.nextclick.crm.OnDemandsModule.Models.OnDemandCategoryModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ODC_R;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;

public class CategoriesFragment extends Fragment implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private View root;
    private RecyclerView ondemands_category_recycler;
    private FloatingActionButton add_ondemands_category_fab;
    private String token;
    private ArrayList<OnDemandCategoryModel> categoryModelsList;
    private final String searchString = " ";
    private SearchView category_searchview;
    OnDemandsCategoryAdapter onDemandsCategoryAdapter=null;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_ondemands_categories, container, false);
        initView();
        add_ondemands_category_fab.setOnClickListener(this);
        category_searchview.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                getCategories(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    onDemandsCategoryAdapter.getFilter().filter(newText);
                }catch (Exception e){
                    e.printStackTrace();
                }
                return false;
            }
        });
        return root;
    }

    private void initView() {
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        ondemands_category_recycler = root.findViewById(R.id.ondemands_category_recycler);
        add_ondemands_category_fab = root.findViewById(R.id.add_ondemands_category_fab);
        category_searchview = root.findViewById(R.id.category_searchview);
        getCategories(searchString);
    }

    private void getCategories(String s) {

        Map<String,String> searchMap = new HashMap<>();
        searchMap.put("q",s);
        final String data = new JSONObject(searchMap).toString();

        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ODC_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("odc_cat_res",response);

                        if(response!=null){
                            LoadingDialog.dialog.dismiss();
                            try{

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if(dataArray.length()>0){
                                        categoryModelsList = new ArrayList<>();
                                        for(int i=0; i<dataArray.length(); i++){
                                            OnDemandCategoryModel categoryModel = new OnDemandCategoryModel();
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryModel.setId(categoryObject.getString("id"));
                                            categoryModel.setName(categoryObject.getString("name"));
                                            categoryModel.setCat_id(categoryObject.getString("cat_id"));

                                            categoryModel.setDesc(categoryObject.getString("desc"));
                                            try {
                                                categoryModel.setImage(categoryObject.getString("image"));
                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                            categoryModelsList.add(categoryModel);
                                        }

                                        onDemandsCategoryAdapter = new OnDemandsCategoryAdapter(mContext, categoryModelsList);

                                        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext,  LinearLayoutManager.VERTICAL, false) {

                                            @Override
                                            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                                                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(mContext) {

                                                    private static final float SPEED = 300f;// Change this value (default=25f)

                                                    @Override
                                                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                                                        return SPEED / displayMetrics.densityDpi;
                                                    }

                                                };
                                                smoothScroller.setTargetPosition(position);
                                                startSmoothScroll(smoothScroller);
                                            }

                                        };
                                        ondemands_category_recycler.setLayoutManager(layoutManager);
                                        ondemands_category_recycler.setAdapter(onDemandsCategoryAdapter);


                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.add_ondemands_category_fab) {
            mContext.startActivity(new Intent(mContext, CreateAndUpdateOnDemandCategoryActivity.class));
        }

    }
}