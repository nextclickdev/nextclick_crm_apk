package com.nextclick.crm.OnDemandsModule.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.OnDemandsModule.Activities.CreateAndUpdateOnDemandServiceActivity;
import com.nextclick.crm.OnDemandsModule.Models.OnDemandsServiceModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ODS_D;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;

public class OnDemandsServiceAdapter extends RecyclerView.Adapter<OnDemandsServiceAdapter.ViewHolder> implements Filterable {

    List<OnDemandsServiceModel> data;
    List<OnDemandsServiceModel> data_full;

    LayoutInflater inflter;
    Context context;
    private String token;




    public OnDemandsServiceAdapter(Context activity, List<OnDemandsServiceModel> itemPojos) {
        this.context = activity;
        this.data = itemPojos;
        data_full = new ArrayList<>(itemPojos);

    }


    @NonNull
    @Override
    public OnDemandsServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.supporter_shop_by_category_and_speciality, parent, false);
        //return new HostelRecentlyAdapter.Business_head_list (itemView);
        token = new PreferenceManager(context).getString(USER_TOKEN);
        return new OnDemandsServiceAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OnDemandsServiceAdapter.ViewHolder holder, final int position) {
        final OnDemandsServiceModel serviceModel = data.get(position);

        holder.sub_cat_name.setText(serviceModel.getName());
        Picasso.get()
                .load(serviceModel.getImage())
                /*.networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                .placeholder(R.drawable.no_image)
                .into(holder.sub_cat_image);


        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CreateAndUpdateOnDemandServiceActivity.class);

                intent.putExtra("service_id", serviceModel.getId());
                intent.putExtra("type", "u");

                context.startActivity(intent);
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setTitle("Alert")
                        .setMessage("Are you sure to remove..?")

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Continue with delete operation


                                deleteCategory(data,position,serviceModel.getId());

                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });



    }

    private void deleteCategory(final List<OnDemandsServiceModel> data, final int position, String id) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ODS_D + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null){
                            try{
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    UIMsgs.showToast(context,"Deleted");
                                    data.remove(position);
                                    notifyDataSetChanged();
                                }else {
                                    UIMsgs.showToast(context,"Unable to delete. Please try later");
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            UIMsgs.showToast(context,MAINTENANCE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {return position;}

    @Override
    public Filter getFilter() {
        return filter;
    }

    private final Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<OnDemandsServiceModel> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length()==0){
                filteredList.addAll(data_full);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(OnDemandsServiceModel model : data_full){
                    if(model.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(model);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {


            data.clear();
            data.addAll((List)results.values);
            notifyDataSetChanged();

        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sub_cat_name;
        ImageView sub_cat_image,delete;
        ConstraintLayout item_layout;
        // MapView vendor_location_map;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            sub_cat_name = itemView.findViewById(R.id.sub_cat_name);
            sub_cat_image = itemView.findViewById(R.id.sub_cat_image);
            item_layout = itemView.findViewById(R.id.item_layout);
            delete = itemView.findViewById(R.id.delete);

            //vendor_location_map = itemView.findViewById(R.id.vendor_map_view);
        }
    }





}
