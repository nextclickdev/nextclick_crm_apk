package com.nextclick.crm.OnDemandsModule.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.LoadingDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.Helpers.UIHelpers.Utility;
import com.nextclick.crm.OnDemandsModule.Adapters.TimingListAdapter;
import com.nextclick.crm.OnDemandsModule.Models.OnDemandCategoryModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.nextclick.crm.Config.Config.ODC_R;
import static com.nextclick.crm.Config.Config.ODS_C;
import static com.nextclick.crm.Config.Config.ODS_R;
import static com.nextclick.crm.Config.Config.ODS_U;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.ValidationMessages.EMPTY_NOT_ALLOWED;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;
import static com.nextclick.crm.Constants.ValidationMessages.OOPS;
import static com.nextclick.crm.Helpers.UIHelpers.UIMsgs.setEditTextErrorMethod;

public class CreateAndUpdateOnDemandServiceActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mContext;
    PreferenceManager preferenceManager;
    private ImageView back_image, service_image;
    private EditText ondemands_service_name, ondemands_service_desc,
            ondemands_service_duration, ondemands_service_price, ondemands_service_discount,
            everyday_opening_time, everyday_closing_time;
    private TextView service_status_type, add_one_more;
    private LinearLayout timings_list_layout;
    private ListView timings_list;
    private Spinner ondemands_category_spinner;
    private Button submit;
    private int status_type = 1;//1- create, 2-update
    private String service_name_str, service_desc_str, service_image_str,
            token,service_duration_str,service_price_str,service_discount_str, sub_cat_id = null;

    public int flag = 0;
    private String userChoosenTask;
    private static final int CAMERA_REQUEST = 8;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 15;
    private final int SELECT_MULTIPLE_FILE = 1;
    int IMG_WIDTH = 400, IMG_HEIGHT = 400;

    private ArrayList<String> categoriesList;
    private ArrayList<String> categoryIDList;

    ArrayList<String> openingTimeList = new ArrayList<String>();
    ArrayList<String> closingTimeList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_update_on_demand_service);
        getSupportActionBar().hide();
        init();

        try {
            String statustext = getIntent().getStringExtra("type");
            if (statustext.equalsIgnoreCase("u")) {
                service_status_type.setText("Update service");
                submit.setText("UPDATE");
                status_type = 2;
                //availabilty.setVisibility(View.VISIBLE);
                Log.d("service_id", getIntent().getStringExtra("service_id")+"----ok");
                getService(getIntent().getStringExtra("service_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        back_image.setOnClickListener(this);
        service_image.setOnClickListener(this);
        submit.setOnClickListener(this);

        everyday_opening_time.setOnClickListener(this);
        everyday_closing_time.setOnClickListener(this);
        add_one_more.setOnClickListener(this);

        timings_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openingTimeList.remove(position);
                closingTimeList.remove(position);
                TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                timings_list.setAdapter(timingListAdapter);
                timingListAdapter.notifyDataSetChanged();
                //Toast.makeText(mContext, timings.size() + "", Toast.LENGTH_SHORT).show();
            }
        });
        ondemands_category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!ondemands_category_spinner.getSelectedItem().toString().equalsIgnoreCase("select")) {
                    sub_cat_id = categoryIDList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    private void init() {
        mContext = CreateAndUpdateOnDemandServiceActivity.this;
        preferenceManager = new PreferenceManager(mContext);
        token = "Bearer " + preferenceManager.getString(USER_TOKEN);
        back_image = findViewById(R.id.back_image);
        ondemands_category_spinner = findViewById(R.id.ondemands_category_spinner);
        service_image = findViewById(R.id.service_image);
        ondemands_service_name = findViewById(R.id.ondemands_service_name);
        ondemands_service_desc = findViewById(R.id.ondemands_service_desc);
        ondemands_service_duration = findViewById(R.id.ondemands_service_duration);
        ondemands_service_price = findViewById(R.id.ondemands_service_price);
        ondemands_service_discount = findViewById(R.id.ondemands_service_discount);
        everyday_opening_time = findViewById(R.id.everyday_opening_time);
        everyday_closing_time = findViewById(R.id.everyday_closing_time);
        service_status_type = findViewById(R.id.service_status_type);
        add_one_more = findViewById(R.id.add_one_more);
        timings_list_layout = findViewById(R.id.timings_list_layout);
        timings_list = findViewById(R.id.timings_list);
        submit = findViewById(R.id.submit);
        try {

            getOnDemandsCategories();
            Thread thread = new Thread();
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void getService(String service_id) {


        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ODS_R + service_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ods_res",response);

                        if(response!=null){
                            LoadingDialog.dialog.dismiss();
                            try{

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONObject dataObject = jsonObject.getJSONObject("data");
                                    ondemands_service_name.setText(dataObject.getString("name"));
                                    ondemands_service_desc.setText(dataObject.getString("desc"));
                                    ondemands_service_duration.setText(dataObject.getString("service_duration"));
                                    ondemands_service_price.setText(dataObject.getString("price"));
                                    ondemands_service_discount.setText(dataObject.getString("discount"));
                                    Picasso.get()
                                            .load(dataObject.getString("image"))
                                            /*.networkPolicy(NetworkPolicy.NO_CACHE)
                                            .memoryPolicy(MemoryPolicy.NO_CACHE)*/
                                            .placeholder(R.drawable.loader_gif)
                                            .into(service_image);

                                    try{
                                        Thread thread = new Thread();
                                        Thread.sleep(1000);
                                        for(int i=0;i<categoryIDList.size();i++){
                                            if(dataObject.getJSONObject("od_category").getString("id").equalsIgnoreCase(categoryIDList.get(i))){
                                                ondemands_category_spinner.setSelection((i+1),true);
                                            }
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }

                                    try {
                                        JSONArray timingsArray = dataObject.getJSONArray("service_timings");
                                        for (int i = 0; i < timingsArray.length(); i++) {

                                            JSONObject timeObject = timingsArray.getJSONObject(i);
                                            openingTimeList.add(timeObject.getString("start_time"));
                                            closingTimeList.add(timeObject.getString("end_time"));

                                        }

                                        timings_list_layout.setVisibility(View.VISIBLE);
                                        ViewGroup.LayoutParams params;
                                        params = timings_list_layout.getLayoutParams();
                                        params.height = openingTimeList.size() * 135;
                                        timings_list_layout.setLayoutParams(params);
                                        TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                                        timings_list.setAdapter(timingListAdapter);
                                        timingListAdapter.notifyDataSetChanged();

                                    }catch (Exception e){

                                    }

                                }


                            }catch (Exception e){
                                LoadingDialog.dialog.dismiss();
                                e.printStackTrace();
                            }
                        }else{
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            /*@Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }


    private void getOnDemandsCategories() {
        Map<String,String> searchMap = new HashMap<>();
        searchMap.put("q"," ");
        final String data = new JSONObject(searchMap).toString();

        //LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ODC_R,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("odc_cat_res",response);

                        if(response!=null){
                            //LoadingDialog.dialog.dismiss();
                            try{

                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if(status){
                                    JSONArray dataArray = jsonObject.getJSONArray("data");
                                    if(dataArray.length()>0){
                                        categoriesList = new ArrayList<>();
                                        categoryIDList = new ArrayList<>();
                                        categoriesList.add("Select");
                                        for(int i=0; i<dataArray.length(); i++){
                                            OnDemandCategoryModel categoryModel = new OnDemandCategoryModel();
                                            JSONObject categoryObject = dataArray.getJSONObject(i);
                                            categoryIDList.add(categoryObject.getString("id"));
                                            categoriesList.add(categoryObject.getString("name"));

                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                                                android.R.layout.simple_spinner_item, categoriesList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        ondemands_category_spinner.setAdapter(adapter);




                                    }
                                }


                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }else{
                            //LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //LoadingDialog.dialog.dismiss();
                        UIMsgs.showToast(mContext,OOPS);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.back_image:
                onBackPressed();
                break;

            case R.id.service_image:
                boolean selecting = selectImage();
                if (selecting) {

                }
                break;

            case R.id.submit:
                if (isValid()) {

                    String url = "";
                    String data = "";


                    if (status_type == 1) {
                        Map<String, Object> uploadMap = new HashMap<>();
                        //uploadMap.put("cat_id", sub_cat_id);
                        uploadMap.put("name", service_name_str);
                        uploadMap.put("desc", service_desc_str);
                        uploadMap.put("od_cat_id", sub_cat_id);
                        uploadMap.put("service_duration", service_duration_str);
                        uploadMap.put("price", service_price_str);
                        uploadMap.put("dscount", service_discount_str);

                        ArrayList<Map<String,Object>> timelist= new ArrayList<>();

                        for (int i = 0; i < openingTimeList.size(); i++) {

                            Map<String, Object> insideTimings = new HashMap<>();
                            insideTimings.put("start_time", openingTimeList.get(i));
                            insideTimings.put("end_time", closingTimeList.get(i));


                            timelist.add(insideTimings);
                        }


                        uploadMap.put("timings", timelist);
                        uploadMap.put("image", service_image_str);

                        url = ODS_C;
                        data = new JSONObject(uploadMap).toString();
                    }
                    if (status_type == 2) {
                        Map<String, Object> uploadMap = new HashMap<>();
                        uploadMap.put("name", service_name_str);
                        uploadMap.put("desc", service_desc_str);
                        uploadMap.put("od_cat_id", sub_cat_id);
                        uploadMap.put("service_duration", service_duration_str);
                        uploadMap.put("price", service_price_str);
                        uploadMap.put("dscount", service_discount_str);

                        ArrayList<Map<String,Object>> timelist= new ArrayList<>();

                        for (int i = 0; i < openingTimeList.size(); i++) {

                            Map<String, Object> insideTimings = new HashMap<>();
                            insideTimings.put("start_time", openingTimeList.get(i));
                            insideTimings.put("end_time", closingTimeList.get(i));


                            timelist.add(insideTimings);
                        }


                        uploadMap.put("timings", timelist);
                        uploadMap.put("image", service_image_str);

                        url = ODS_U;
                        data = new JSONObject(uploadMap).toString();
                    }

                    createOrUpdateService(url, data);
                }
                break;

            case R.id.everyday_opening_time:

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateAndUpdateOnDemandServiceActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_opening_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

                break;
            case R.id.everyday_closing_time:

                Calendar mcurrentTime1 = Calendar.getInstance();
                int hour1 = mcurrentTime1.get(Calendar.HOUR_OF_DAY);
                int minute1 = mcurrentTime1.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker1;
                mTimePicker1 = new TimePickerDialog(CreateAndUpdateOnDemandServiceActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        everyday_closing_time.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour1, minute1, true);//Yes 24 hour time
                mTimePicker1.setTitle("Select Time");
                mTimePicker1.show();

                break;

            case R.id.add_one_more:


                String open = everyday_opening_time.getText().toString().trim();
                String close = everyday_closing_time.getText().toString().trim();
                if (open.length() > 0 && close.length() > 0) {
                    timings_list_layout.setVisibility(View.VISIBLE);

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    Date opentime = null;
                    Date closetime = null;
                    try {
                        opentime = sdf.parse(open);
                        closetime = sdf.parse(close);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long reqOpen = opentime.getTime();
                    long reqClose = closetime.getTime();

                    if (openingTimeList.size() > 0 && closingTimeList.size() > 0) {
                        Date checkertime = null;
                        try {
                            checkertime = sdf.parse(closingTimeList.get(closingTimeList.size() - 1));

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long timechecker = checkertime.getTime();

                        if (reqOpen > timechecker) {

                            openingTimeList.add(open);
                            closingTimeList.add(close);

                        } else {
                            everyday_opening_time.requestFocus();
                            Toast.makeText(mContext, "please choose valid timings", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        openingTimeList.add(open);
                        closingTimeList.add(close);
                        everyday_opening_time.setText("".trim());
                        everyday_opening_time.setHint("00:00");
                        everyday_closing_time.setText("".trim());
                        everyday_closing_time.setHint("00:00");
                    }
                    ViewGroup.LayoutParams params;
                    params = timings_list_layout.getLayoutParams();
                    params.height = openingTimeList.size() * 135;
                    timings_list_layout.setLayoutParams(params);
                    TimingListAdapter timingListAdapter = new TimingListAdapter(getApplicationContext(), R.layout.timing_list_supporter, 1, openingTimeList, closingTimeList);
                    timings_list.setAdapter(timingListAdapter);
                    timingListAdapter.notifyDataSetChanged();


                }
                break;

        }

    }


    private boolean isValid() {
        boolean valid = true;
        service_name_str = ondemands_service_name.getText().toString().trim();
        service_desc_str = ondemands_service_desc.getText().toString().trim();
        service_image_str = basse64Converter(service_image);
        service_duration_str = ondemands_service_duration.getText().toString().trim();
        service_price_str = ondemands_service_price.getText().toString().trim();
        service_discount_str = ondemands_service_discount.getText().toString().trim();
        int name_length, desc_length, image_length,price=0;
        name_length = service_name_str.length();
        desc_length = service_desc_str.length();
        image_length = service_image_str.length();
        try {
            price = Integer.parseInt(service_price_str);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (name_length == 0) {
            setEditTextErrorMethod(ondemands_service_name, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (name_length < 1) {
            setEditTextErrorMethod(ondemands_service_name, "Please provide valid data");
            valid = false;
        } else if (desc_length == 0) {
            setEditTextErrorMethod(ondemands_service_desc, EMPTY_NOT_ALLOWED);
            valid = false;
        } else if (desc_length < 3) {
            setEditTextErrorMethod(ondemands_service_desc, "Please provide valid data");
            valid = false;
        } else if (image_length < 1) {
            UIMsgs.showToast(mContext, "Please provide image");
            valid = false;
        }else if(sub_cat_id==null){
            UIMsgs.showToast(mContext, "Please select category");
            valid = false;
        }else if(price == 0){
            setEditTextErrorMethod(ondemands_service_price, "Please provide valid data");
            valid = false;
        }else if(service_price_str.length()<1){
            setEditTextErrorMethod(ondemands_service_price, "Please provide valid data");
            valid = false;
        }else if(service_duration_str.length()<1){
            setEditTextErrorMethod(ondemands_service_duration, "Please provide valid data");
            valid = false;
        }else if(Double.parseDouble(service_duration_str)<1){
            setEditTextErrorMethod(ondemands_service_duration, "Please provide valid data");
            valid = false;
        }else if(openingTimeList.size()<1){
            UIMsgs.showToast(mContext, "Please provide timings");
            valid = false;
        }

        return valid;

    }

    //Image Selection Start


    private boolean selectImage() {
        final CharSequence[] items = {"Choose from Library", "Open Camera",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CreateAndUpdateOnDemandServiceActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(CreateAndUpdateOnDemandServiceActivity.this);

                if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {    //Calling Gallery Marhod For Images
                        galleryIntent();
                    }

                } else if (items[item].equals("Open Camera")) {
                    userChoosenTask = "Open Camera";
                    if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                    } else {
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);


        }
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            service_image.setImageBitmap(photo);
            Bitmap bitmap = ((BitmapDrawable) service_image.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] byteArray = baos.toByteArray();

            flag = 1;
        }

    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);


        if (image.getHeight() <= 400 && image.getWidth() <= 400) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);

    }

    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
               /* Toast.makeText(mContext, data.getData() + "", Toast.LENGTH_SHORT).show();
                Log.d("Data", data.getData().toString());*/

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bm = Bitmap.createScaledBitmap(bm, 512, 512, false);


        service_image.setImageBitmap(bm);

        Bitmap bitmap = ((BitmapDrawable) service_image.getDrawable()).getBitmap();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArray = baos.toByteArray();

        flag = 1;


    }

    //Calling Openig Gallery For Images
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    private String basse64Converter(ImageView imageView) {
        String base64String = null;
        imageView.buildDrawingCache();
        Bitmap bitmap = imageView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        System.out.println("byte array:" + image);
        base64String = Base64.encodeToString(image, 0);

        return base64String;
    }


    private void createOrUpdateService(String url, final String data) {
        LoadingDialog.loadDialog(mContext);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            LoadingDialog.dialog.dismiss();
                            Log.d("_res",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                boolean status = jsonObject.getBoolean("status");
                                if (status) {
                                    UIMsgs.showToast(mContext, "Created Successfully");
                                    recreate();
                                } else {
                                    UIMsgs.showToast(mContext, jsonObject.getString("data"));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            LoadingDialog.dialog.dismiss();
                            UIMsgs.showToast(mContext, MAINTENANCE);
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LoadingDialog.dialog.dismiss();
                UIMsgs.showToast(mContext, OOPS);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN, token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes(StandardCharsets.UTF_8);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }



}