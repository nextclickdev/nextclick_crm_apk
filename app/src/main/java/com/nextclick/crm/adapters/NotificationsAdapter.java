package com.nextclick.crm.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nextclick.crm.Helpers.UIHelpers.CustomDialog;
import com.nextclick.crm.Helpers.UIHelpers.UIMsgs;
import com.nextclick.crm.R;
import com.nextclick.crm.ShopNowModule.Activities.ShopNowHomeActivity;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.customer_support.TicketsActivity;
import com.nextclick.crm.models.responseModels.notificationDetailsResponse.NotificationDetails;
import com.nextclick.crm.orders.activity.OrderDetailsActivity;
import com.nextclick.crm.subcriptions.SubcriptionsActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.nextclick.crm.Config.Config.NOTIFICATIONS_READ;
import static com.nextclick.crm.Constants.Constants.AUTH_TOKEN;
import static com.nextclick.crm.Constants.Constants.USER_TOKEN;
import static com.nextclick.crm.Constants.Constants.isnullboolean;
import static com.nextclick.crm.Constants.ValidationMessages.MAINTENANCE;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    private final Context context;
    private final boolean isPlanExists;
    private final LinkedList<NotificationDetails> listOFNotificationDetails;
    private final CustomDialog customDialog;
    PreferenceManager preferenceManager;

    public NotificationsAdapter(Context context, LinkedList<NotificationDetails> listOFNotificationDetails, boolean isPlanExists) {
        this.context =context;
        this.customDialog =new CustomDialog(context);
        this.preferenceManager=new PreferenceManager(context);
        this.listOFNotificationDetails = listOFNotificationDetails;
        this.isPlanExists = isPlanExists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notifictaion_items, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationDetails notificationDetails = listOFNotificationDetails.get(position);
        String title = String.valueOf(Html.fromHtml(notificationDetails.getTitle()));
        String message =  String.valueOf(Html.fromHtml(notificationDetails.getMessage()));
        holder.tvTitle.setText(title);
        holder.tvMessage.setText(message);

        if (notificationDetails.getStatus().equalsIgnoreCase("1")){
            holder.layout_notification.setBackgroundColor(context.getResources().getColor(R.color.notification_readcolor));
        }else{
            holder.layout_notification.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

        holder.cardview_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isnullboolean(notificationDetails.getEcom_order_id())){
                    if (notificationDetails.getStatus().equalsIgnoreCase("2")){
                        if (notificationDetails.getNotification_code().equalsIgnoreCase("PROD")){
                            Intent intent = new Intent(context, ShopNowHomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                        } else {
                            Intent i=new Intent(context, OrderDetailsActivity.class);
                            i.putExtra("order_id", notificationDetails.getEcom_order_id());
                            i.putExtra("status", "");

                            context.startActivity(i);
                        }

                    }else{
                        readNotificarion(notificationDetails,0);
                    }

                }else if(notificationDetails.getNotification_code().equalsIgnoreCase("SUBS")){
                    if (notificationDetails.getStatus().equalsIgnoreCase("2")){
                        Intent intent = new Intent(context, SubcriptionsActivity.class);
                        intent.putExtra("isPlanExists", "" + isPlanExists);

                        context.startActivity(intent);
                    }else{
                        readNotificarion(notificationDetails,1);
                    }

                } else if (notificationDetails.getNotification_code().equalsIgnoreCase("CS")){
                    if (notificationDetails.getStatus().equalsIgnoreCase("2")){
                        Intent intent = new Intent(context, TicketsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("id",notificationDetails.getTicket_id());
                        context.startActivity(intent);
                    } else {
                        readNotificarion(notificationDetails,2);
                    }

                }
            }
        });
    }
    public void readNotificarion(NotificationDetails notificationDetails,int checkwhich) {

        System.out.println("aaaaaaaaaa notification id  "+notificationDetails.getId());
        customDialog.show();

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, NOTIFICATIONS_READ+notificationDetails.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        //  LoadingDialog.dialog.dismiss();
                        customDialog.dismiss();
                        if (response != null) {
                            Log.d("notificationread_resp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                System.out.println("aaaaaaaaaaaa  notifications read "+ jsonObject);
                                boolean status = jsonObject.getBoolean("status");

                                if (status) {
                                    if (checkwhich==0){
                                        try {
                                            if (notificationDetails.getNotification_code().equalsIgnoreCase("PROD")){
                                                Intent intent = new Intent(context, ShopNowHomeActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                context.startActivity(intent);
                                            } else {
                                                Intent i=new Intent(context, OrderDetailsActivity.class);
                                                i.putExtra("order_id", notificationDetails.getEcom_order_id());
                                                i.putExtra("status", "");

                                                context.startActivity(i);
                                            }
                                        } catch (Exception e) {
                                            System.out.println("aaaaaaa catch  "+e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }else if(checkwhich==1){
                                        try {
                                            Intent intent = new Intent(context, SubcriptionsActivity.class);
                                            intent.putExtra("isPlanExists", "" + isPlanExists);

                                            context.startActivity(intent);
                                        } catch (Exception e) {
                                            System.out.println("aaaaaaa catch  "+e.getMessage());
                                            e.printStackTrace();
                                        }
                                    } else if (checkwhich==2){
                                        try {
                                            Intent intent = new Intent(context, TicketsActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            intent.putExtra("id",notificationDetails.getTicket_id());
                                            context.startActivity(intent);
                                        } catch (Exception e) {
                                            System.out.println("aaaaaaa catch  "+e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }

                                } else {
                                }
                            } catch (Exception e) {
                                System.out.println("aaaaaaa catch 111  "+e.getMessage());
                                e.printStackTrace();
                            }
                        } else {
                            UIMsgs.showToast(context, MAINTENANCE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        customDialog.dismiss();
                        System.out.println("aaaaaaa error 111  "+error.getMessage());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put(AUTH_TOKEN,"Bearer " + preferenceManager.getString(USER_TOKEN));
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
    @Override
    public int getItemCount() {
        return listOFNotificationDetails.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView tvTitle;
        private final TextView tvMessage;
        private final CardView cardview_main;
        private final LinearLayout layout_notification;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);
            cardview_main = itemView.findViewById(R.id.cardview_main);
            layout_notification = itemView.findViewById(R.id.layout_notification);
        }
    }
}
