package com.nextclick.crm.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextclick.crm.Common.Models.NotificationSoundsModel;
import com.nextclick.crm.R;
import com.nextclick.crm.Utilities.PreferenceManager;
import com.nextclick.crm.activities.NotificationSoundsActivity;

import java.util.ArrayList;

public class NotificationsoundsAdapter extends RecyclerView.Adapter<NotificationsoundsAdapter.ViewHolder> {
     Context context;
      ArrayList<NotificationSoundsModel> listOFNotificationDetails;
    public MediaPlayer mp;
    PreferenceManager preferenceManager;

    public NotificationsoundsAdapter(Context context, ArrayList<NotificationSoundsModel> listOFNotificationDetails) {
        this.context =context;
        this.listOFNotificationDetails =listOFNotificationDetails;
        preferenceManager=new PreferenceManager(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_sounds, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.radio_button.setText("Notification "+position);

        holder.radio_button.setChecked(listOFNotificationDetails.get(position).isIschecked());

        holder.radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mp != null) {
                    mp.stop();
                }

                mp = MediaPlayer.create(context, listOFNotificationDetails.get(position).getSounds());
                mp.setLooping(false);
                mp.setVolume(20, 20);
                mp.start();

                ((NotificationSoundsActivity)context).setSound(position);
            }
        });
       /* holder.radio_button.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mp != null) {
                    mp.stop();
                }

                if (isChecked){

                    mp = MediaPlayer.create(context, listOFNotificationDetails.get(position).getSounds());
                    mp.setLooping(false);
                    mp.setVolume(20, 20);
                    mp.start();

                    ((NotificationSoundsActivity)context).setSound(position);
                }else {
                  //  holder.radio_button.setChecked(false);
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return listOFNotificationDetails.size();

    }

    public void setchange(ArrayList<NotificationSoundsModel> sounds) {
        this.listOFNotificationDetails=sounds;
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
      //  notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private final RadioButton radio_button;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            radio_button = itemView.findViewById(R.id.radio_button);
        }
    }
}
